{-# OPTIONS --prop --rewriting #-}
module gy06_after where

open import Lib
module NB where
  open import NatBool
  open I

  TF : Model {i = lsuc lzero}
  TF = record
    { Ty = Raise (Lift ⊤)
    ; Tm = λ _ → 𝟚
    ; Nat = mk (mk trivi)
    ; Bool = mk (mk trivi)
    ; true = tt
    ; false = ff
    ; ite = λ where tt tr fa → tr
                    ff tr fa → fa
    ; num = λ where zero → ff
                    (suc n) → tt
    ; isZero = λ where tt → ff
                       ff → tt
    ; _+o_ = _∨_
    ; iteβ₁ = λ {A} {u} {v} → refl {x = u}
    ; iteβ₂ = λ {A} {u} {v} → refl {x = v}
    ; isZeroβ₁ = refl {x = tt}
    ; isZeroβ₂ = refl {x = ff}
    ; +β = λ where {zero} {zero} → refl {x = ff}
                   {zero} {suc n} → refl {x = tt}
                   {suc m} {zero} → refl {x = tt}
                   {suc m} {suc n} → refl {x = tt}
    }

  module TF = Model TF

  getType : 𝟚 → Prop
  getType tt = ⊤
  getType ff = ⊥

  true≠false : ¬ (true ≡ false)
  true≠false eq = coep (λ x → getType TF.⟦ x ⟧t) {a₀ = true} eq trivi

  -- true≠isZero : {t : Tm Nat} → ¬ (true ≡ isZero t)
  -- true≠isZero eq = {!   !}


module Int where

  module I where
    postulate
      Z     : Set
      Zero  : Z
      Suc   : Z → Z
      Pred  : Z → Z
      SucPred : (i : Z) → Suc (Pred i) ≡ i
      PredSuc : (i : Z) → Pred (Suc i) ≡ i
  variable
    i' : I.Z

  record Model {ℓ} : Set (lsuc ℓ) where
    field
      Z        : Set ℓ
      Zero     : Z
      Suc      : Z → Z
      Pred     : Z → Z
      SucPred  : (i : Z) → Suc (Pred i) ≡ i
      PredSuc  : (i : Z) → Pred (Suc i) ≡ i

    postulate
      ⟦_⟧      : I.Z → Z
      ⟦Zero⟧   : ⟦ I.Zero    ⟧ ≡ Zero
      ⟦Suc⟧    : ⟦ I.Suc i'  ⟧ ≡ Suc ⟦ i' ⟧
      ⟦Pred⟧   : ⟦ I.Pred i' ⟧ ≡ Pred ⟦ i' ⟧

      {-# REWRITE ⟦Zero⟧ ⟦Suc⟧ ⟦Pred⟧ #-}

  test : Model
  test = record
    { Z = 𝟚
    ; Zero = ff
    ; Suc = λ z → z
    ; Pred = λ z → z
    ; SucPred = λ z → refl {x = z}
    ; PredSuc = λ z → refl {x = z}
    }

  record DepModel {ℓ} : Set (lsuc ℓ) where
    field
      Z        : I.Z → Set ℓ
      Zero     : Z I.Zero
      Suc      : Z i' → Z (I.Suc  i')
      Pred     : Z i' → Z (I.Pred i')
      SucPred  : (i : Z i') → (Z ~) (I.SucPred i') (Suc (Pred i)) i
      PredSuc  : (i : Z i') → (Z ~) (I.PredSuc i') (Pred (Suc i)) i
    postulate
      ⟦_⟧      : (n : I.Z) → Z n
      ⟦Zero⟧   : ⟦ I.Zero     ⟧ ≡ Zero
      ⟦Suc⟧    : ⟦ I.Suc i'   ⟧ ≡ Suc   ⟦ i' ⟧
      ⟦Pred⟧   : ⟦ I.Pred i'  ⟧ ≡ Pred  ⟦ i' ⟧

      {-# REWRITE ⟦Zero⟧ #-}

  open I

  eq-0 : Pred (Suc Zero) ≡ Zero
  eq-0 = PredSuc Zero

  eq-1 : Suc (Suc (Pred Zero)) ≡ Suc Zero
  eq-1 = cong Suc (SucPred Zero)

  eq-2 : Pred (Pred (Suc Zero)) ≡ Suc (Suc (Pred (Pred (Pred Zero))))
  eq-2 =
    Pred (Pred (Suc Zero))
      ≡⟨ cong Pred (PredSuc Zero) ⟩
    Pred Zero
      ≡⟨ sym {A = Z} (SucPred (Pred Zero)) ⟩
    Suc (Pred (Pred Zero))
      ≡⟨ sym {A = Z} (cong Suc (SucPred (Pred (Pred Zero)))) ⟩
    Suc (Suc (Pred (Pred (Pred Zero))))
      ∎

  -- Normalisation

  data Nf  : Set where
    Negative : ℕ → Nf
    Neutral  : Nf
    Positive : ℕ → Nf

  minus-two : Nf
  minus-two = Negative 1

  minus-one : Nf
  minus-one = Negative 0

  plus-one : Nf
  plus-one = Positive 0

  plus-three : Nf
  plus-three = Positive 2

  plus-two : Nf
  plus-two = Positive 1

  plus-four : Nf
  plus-four = Positive 3

  SucNf : Nf → Nf
  SucNf (Negative zero) = Neutral
  SucNf (Negative (suc x)) = Negative x
  SucNf Neutral = Positive 0
  SucNf (Positive x) = Positive (suc x)

  PredNf : Nf → Nf
  PredNf (Negative x) = Negative (suc x)
  PredNf Neutral = Negative 0
  PredNf (Positive zero) = Neutral
  PredNf (Positive (suc x)) = Positive x

  -- Ami kell : SucNf (PredNf (Negative x)) ≡ Negative x
  --            SucNf (Negative (suc x)) ≡ Negative x
  --            Negative x ≡ Negative x

  N : Model {ℓ = lzero}
  N = record
    { Z       = Nf
    ; Zero    = Neutral
    ; Suc     = SucNf
    ; Pred    = PredNf
    ; SucPred = λ where (Negative x) → refl {x = Negative x}
                        Neutral → refl {x = Neutral}
                        (Positive zero) → refl {x = Positive 0}
                        (Positive (suc x)) → refl {x = Positive (suc x)}
    ; PredSuc = λ where (Negative zero) → refl {x = Negative 0}
                        (Negative (suc x)) → refl {x = Negative (suc x)}
                        Neutral → refl {x = Neutral}
                        (Positive x) → refl {x = Positive x}
    }
  module N = Model N

  norm : I.Z → Nf
  norm = N.⟦_⟧

  test-norm = {! norm (I.Pred (I.Pred (I.Suc (I.Pred (I.Pred (I.Pred (I.Suc I.Zero)))))))  !}

  ⌜_⌝ : Nf → Z -- ⌜ = \cul | ⌝ = \cur
  ⌜ Negative zero ⌝ = Pred Zero
  ⌜ Negative (suc x) ⌝ = Pred ⌜ Negative x ⌝
  ⌜ Neutral ⌝ = Zero
  ⌜ Positive zero ⌝ = Suc Zero
  ⌜ Positive (suc x) ⌝ = Suc ⌜ Positive x ⌝

  test-quote = {! ⌜ Negative 5 ⌝  !}

  -- Stability

  stab : (n : Nf) → norm ⌜ n ⌝ ≡ n
  stab (Negative zero) = refl {x = Negative 0}
  stab (Negative (suc x)) = cong PredNf (stab (Negative x))
  stab Neutral = refl {x = Neutral }
  stab (Positive zero) = refl {x = Positive 0}
  stab (Positive (suc x)) = cong SucNf (stab (Positive x))

  -- Ami kell:  norm   ⌜ Negative (suc x) ⌝  ≡ Negative (suc x)
  --            norm   (Pred ⌜ Negative x ⌝) ≡ Negative (suc x)
  --            PredNf (norm ⌜ Negative x ⌝) ≡ Negative (suc x)
  --            PredNf (norm ⌜ Negative x ⌝) ≡ PredNf (Negative x)
  -- Amink van:         norm ⌜ Negative x ⌝  ≡         Negative x

  -- Completeness

  ⌜Suc⌝ : (n : Nf) → ⌜ SucNf n ⌝ ≡ Suc ⌜ n ⌝
  ⌜Suc⌝ (Negative zero) = sym {A = Z} (SucPred Zero)
  ⌜Suc⌝ (Negative (suc x)) = sym {A = Z} (SucPred ⌜ Negative x ⌝)
  ⌜Suc⌝ Neutral = refl {x = Suc Zero}
  ⌜Suc⌝ (Positive x) = refl {x = Suc ⌜ Positive x ⌝}

  ⌜Pred⌝ : (n : Nf) → ⌜ PredNf n ⌝ ≡ Pred ⌜ n ⌝
  ⌜Pred⌝ (Negative x) = refl {x = Pred ⌜ Negative x ⌝}
  ⌜Pred⌝ Neutral = refl {x = Pred Zero}
  ⌜Pred⌝ (Positive zero) = sym {A = Z} (PredSuc Zero)
  ⌜Pred⌝ (Positive (suc x)) = sym {A = Z} (PredSuc ⌜ Positive x ⌝)

  Comp : DepModel {ℓ = lzero}
  Comp = record
    { Z        = λ i → Lift (⌜ norm i ⌝ ≡ i)
    ; Zero     = mk (refl {x = Zero})
    ; Suc      = λ {i'} i → mk (trans {A = Z} (⌜Suc⌝ (norm i')) (cong Suc (un i)))
    ; Pred     = λ {i'} i → mk (trans {A = Z} (⌜Pred⌝ (norm i')) (cong Pred (un i)))
    ; SucPred  = λ _ → mk trivi
    ; PredSuc  = λ _ → mk trivi
    }
  module Comp = DepModel Comp

  -- Amink van (segédlemma)  : (n : Nf) → ⌜ SucNf n ⌝ ≡ Suc ⌜ n ⌝
  -- Amink van (indukció)    : ⌜ norm i' ⌝ ≡ i'
  -- Ami kell                : ⌜ norm (Suc i') ⌝   ≡ Suc i'
  -- Ami kell                : ⌜ SucNf (norm i') ⌝ ≡ Suc i'
  -- segédlemma (norm i')-re : ⌜ SucNf (norm i') ⌝ ≡ Suc ⌜ (norm i') ⌝
  -- indukciós hipotézis `Suc` szerinti `cong`-gal : Suc ⌜ (norm i') ⌝ ≡ Suc i'

  comp : (i : I.Z) → ⌜ norm i ⌝ ≡ i
  comp i = un Comp.⟦ i ⟧


module DefABT where

  module I where
    data Var  : ℕ → Set where
      vz      : ∀{n} → Var (suc n)
      vs      : ∀{n} → Var n → Var (suc n)

    data Tm   : ℕ → Set where
      var     : Var n → Tm n
      def     : Tm  n → Tm (suc n) → Tm n

      true    : ∀ {n} → Tm n
      false   : ∀ {n} → Tm n
      ite     : ∀ {n} → Tm n → Tm n → Tm n → Tm n
      num     : ℕ → Tm n
      isZero  : ∀ {n} → Tm n → Tm n
      _+o_    : ∀ {n} → Tm n → Tm n → Tm n

    v0 : {n : ℕ} → Tm (1 + n)
    v0 = var vz
    v1 : {n : ℕ} → Tm (2 + n)
    v1 = var (vs vz)
    v2 : {n : ℕ} → Tm (3 + n)
    v2 = var (vs (vs vz))
    v3 : {n : ℕ} → Tm (4 + n)
    v3 = var (vs (vs (vs vz)))

  open I


  {- Rewrite the following expressions with De Bruijn notation -}

  -- let x:=1 in x + let y:=x+1 in y + let z:=x+y in (x+z)+(y+x)
  tm-1 : Tm 0
  tm-1 = def (num 1) (v0 +o def (v0 +o (num 1)) (v0 +o def (v1 +o v0) ((v2 +o v0) +o (v1 +o v2))))

  -- (let x:=1 in x) + let y:=1 in y + let z:=x+y in (x+z)+(y+x)
  tm-2 : Tm 1
  tm-2 = (def (num 1) v0) +o def (num 1) (v0 +o def (v1 +o v0) ((v2 +o v0) +o (v1 +o v2)))

  data _<=_ : ℕ → ℕ → Set where
    zero<= : {n : ℕ} → zero <= n
    suc<= : {n m : ℕ} → (n <= m) → (suc n <= suc m)

  -- (let x:=1 in x) + let y:=1 in y + let z:=x+y in (x+z)+(y+x)
  tm-2' : (n : ℕ) → (1 <= n) → Tm n
  tm-2' .(suc _) (suc<= ge) = (def (num 1) v0) +o def (num 1) (v0 +o def (v1 +o v0) ((v2 +o v0) +o (v1 +o v2)))

  tm-wrong : Tm 0
  tm-wrong = def false (v0 +o v0)
