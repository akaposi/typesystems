{-# OPTIONS --prop --rewriting #-}
module gy04_after where

open import Lib
open import Nat using ()

module NatBoolAST where

  module I where
    data Tm   : Set where
      true    : Tm
      false   : Tm
      ite     : Tm → Tm → Tm → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : Tm → Tm → Tm

  record Model {i} : Set (lsuc i) where
    field
      Tm      : Set i
      true    : Tm
      false   : Tm
      ite     : (co : Tm) → (tr : Tm) → (fa : Tm) → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : (l : Tm) → (r : Tm) → Tm

    ⟦_⟧ : I.Tm → Tm
    ⟦ I.true         ⟧ = true
    ⟦ I.false        ⟧ = false
    ⟦ I.ite t t' t'' ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
    ⟦ I.num n        ⟧ = num n
    ⟦ I.isZero t     ⟧ = isZero ⟦ t ⟧
    ⟦ t I.+o t'      ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧

  record DepModel {i} : Set (lsuc i) where
    field
      Tm      : (t : I.Tm) → Set i
      true    : Tm I.true
      false   : Tm I.false
      ite     : ∀{co' tr' fa'} → (co : Tm co') → (tr : Tm tr') → (fa : Tm fa') → Tm (I.ite co' tr' fa')
      num     : (n : ℕ) → Tm (I.num n)
      isZero  : ∀{t'} → (t : Tm t') → Tm (I.isZero t')
      _+o_    : ∀{l' r'} → (l : Tm l') → (r : Tm r') → Tm (l' I.+o r')

    ⟦_⟧ : (t : I.Tm) → Tm t
    ⟦ I.true          ⟧ = true
    ⟦ I.false         ⟧ = false
    ⟦ I.ite t t' t''  ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
    ⟦ I.num n         ⟧ = num n
    ⟦ I.isZero t      ⟧ = isZero ⟦ t ⟧
    ⟦ t I.+o t'       ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧

  open I -- All unqualified terms are from the initial model!

  tm1 : I.Tm
  tm1 = ite true (num 1 +o num 3) (isZero (isZero false))

  {-
          ite             |          ite
        /  |  \           |       /   |  \
       /   |   \          |      /    |   \
     true  +o   isZero    |  isZero   +o  true
       ^  /\        |     |   |       / \
         /  \       |     |   |      /   \
    num 1  num 3  isZero  | isZero num 3 num 1
      ^      ^      |     |   |
                    |     |   |
                  false   | false
                    ^     |
  -}


  -- Count the leaves of a tree

  Leaves : Model {i = lzero}
  Leaves = record
    { Tm     = ℕ
    ; true   = 1
    ; false  = 1
    ; ite    = λ co tr fa → co + tr + fa
    ; num    = λ _ → 1
    ; isZero = λ x → x
    ; _+o_   = λ l r → l + r
    }
  module L = Model Leaves

  leaves-test-1 : L.⟦ false ⟧ ≡ 1
  leaves-test-1 = refl {x = 1}

  leaves-test-2 : L.⟦ num 3 +o (isZero true) ⟧ ≡ 2
  leaves-test-2 = refl {x = 2}

  leaves-test-3 : L.⟦ ite false true (num 5) ⟧ ≡ 3
  leaves-test-3 = refl {x = 3}

  leaves-test-4 : L.⟦ ite true (num 1 +o num 3) (isZero (isZero (true +o false))) ⟧ ≡ 5
  leaves-test-4 = refl {x = 4}


  -- Horizontally mirror a tree (can this be done in NatBoolWT?)

  Flip : Model {i = lzero}
  Flip = record
    { Tm     = Tm
    ; true   = true
    ; false  = false
    ; ite    = λ co tr fa → ite fa tr co
    ; num    = num -- (λ x → num x)
    ; isZero = λ x → isZero x
    ; _+o_   = λ l r → r +o l -- (_+o_ r l)
    }

  module F = Model Flip

  flip-test : I.Tm
  flip-test = {! F.⟦  isZero (ite true (false +o true) (num 3))  ⟧  !}

  flip-test-1 : F.⟦ false ⟧ ≡ false
  flip-test-1 = refl {A = Tm}

  flip-test-2 : F.⟦ num 3 +o (isZero true) ⟧ ≡ (isZero true) +o num 3
  flip-test-2 = refl {A = Tm}

  flip-test-3 : F.⟦ ite false true (num 5) ⟧ ≡ ite (num 5) true false
  flip-test-3 = refl {A = Tm}

  flip-test-4 : F.⟦ ite true (num 1 +o num 3) (isZero (isZero false)) ⟧ ≡ ite (isZero (isZero false)) (num 3 +o num 1) true
  flip-test-4 = refl {A = Tm}


  -- Flipping a tree doesn't change the number of leaves it has

  postulate
    h1 : {a b c d e f : ℕ} → (a ≡ f) → (b ≡ e) → (c ≡ d) → a + b + c ≡ d + e + f
    h2 : {a b c d : ℕ} →     (a ≡ d) → (b ≡ c) →           a + b ≡ c + d

  FlipLeaves : DepModel {i = lzero}
  FlipLeaves = record
    { Tm     = λ t → Lift (L.⟦ t ⟧ ≡ L.⟦ F.⟦ t ⟧ ⟧)
    ; true   = mk (refl {x = 1})
    ; false  = mk (refl {x = 1})
    ; ite    = λ {co'} co tr fa → mk (h1 {a = L.⟦ co' ⟧} (un co) (un tr) (un fa))
    ; num    = λ _ → mk (refl {x = 1})
    ; isZero = λ {t'} ih → ih
    ; _+o_   = λ {l'} {r'} l r → mk (h2 {a = L.⟦ l' ⟧} (un l) (un r))
    }

    -- true
    -- Lift (L.⟦ true ⟧ ≡ L.⟦ F.⟦ true ⟧ ⟧)
    -- Lift (L.⟦ true ⟧ ≡ L.⟦ true ⟧)
    -- Lift (1 ≡ 1)
    -- Lift ⊤

    -- isZero
    -- Amink van: Lift (L.⟦ t ⟧ ≡ L.⟦ F.⟦ t ⟧ ⟧)
    -- Ami kell: Lift (L.⟦ isZero t ⟧ ≡ L.⟦ F.⟦ isZero t ⟧ ⟧)
    --           Lift (L.⟦ t ⟧ ≡ L.⟦ isZero F.⟦ t ⟧ ⟧)
    --           Lift (L.⟦ t ⟧ ≡ L.⟦ F.⟦ t ⟧ ⟧)

    -- _+o_
    --  Amink van: Lift (L.⟦ l' ⟧ ≡ L.⟦ F.⟦ l' ⟧ ⟧)
    --  Amink van: Lift (L.⟦ r' ⟧ ≡ L.⟦ F.⟦ r' ⟧ ⟧)
    --  Ami kell: Lift (L.⟦ l' + r' ⟧ ≡ L.⟦ F.⟦ l' + r' ⟧ ⟧)
    --            Lift (L.⟦ l' + r' ⟧ ≡ L.⟦ F.⟦ r' ⟧ + F.⟦ l' ⟧ ⟧)
    --                  a        +      b   ≡     c          +     d
    --            Lift (L.⟦ l' ⟧ + L.⟦ r' ⟧ ≡ L.⟦ F.⟦ r' ⟧ ⟧ + L.⟦ F.⟦ l' ⟧ ⟧)


-- Well Typed

module NatBoolWT where

  module I where
    data Ty   : Set where
      Nat     : Ty
      Bool    : Ty
    data Tm   : Ty → Set where
      true    : Tm Bool
      false   : Tm Bool
      ite     : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
      num     : ℕ → Tm Nat
      isZero  : Tm Nat → Tm Bool
      _+o_    : Tm Nat → Tm Nat → Tm Nat

  record Model {i j} : Set (lsuc (i ⊔ j)) where
    field
      Ty      : Set i
      Tm      : Ty → Set j
      Nat     : Ty
      Bool    : Ty
      true    : Tm Bool
      false   : Tm Bool
      ite     : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
      num     : ℕ → Tm Nat
      isZero  : Tm Nat → Tm Bool
      _+o_    : Tm Nat → Tm Nat → Tm Nat

    ⟦_⟧T  : I.Ty → Ty
    ⟦_⟧t  : ∀{A} → I.Tm A → Tm ⟦ A ⟧T
    ⟦ I.Nat           ⟧T = Nat
    ⟦ I.Bool          ⟧T = Bool
    ⟦ I.true          ⟧t = true
    ⟦ I.false         ⟧t = false
    ⟦ I.ite t u v     ⟧t = ite ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    ⟦ I.num n         ⟧t = num n
    ⟦ I.isZero t      ⟧t = isZero ⟦ t ⟧t
    ⟦ u I.+o v        ⟧t = ⟦ u ⟧t +o ⟦ v ⟧t

  record DepModel {i j} : Set (lsuc (i ⊔ j)) where
    field
      Ty      :  I.Ty → Set i
      Tm      :  ∀{Aᴵ} → Ty Aᴵ → I.Tm Aᴵ → Set j
      Nat     :  Ty I.Nat
      Bool    :  Ty I.Bool
      true    :  Tm Bool I.true
      false   :  Tm Bool I.false
      ite     :  ∀{Aᴵ tᴵ uᴵ vᴵ}{A : Ty Aᴵ} → Tm Bool tᴵ → Tm A uᴵ → Tm A vᴵ →
                Tm A (I.ite tᴵ uᴵ vᴵ)
      num     :  (n : ℕ) → Tm Nat (I.num n)
      isZero  :  ∀{tᴵ} → Tm Nat tᴵ → Tm Bool (I.isZero tᴵ)
      _+o_    :  ∀{uᴵ vᴵ} → Tm Nat uᴵ → Tm Nat vᴵ → Tm Nat (uᴵ I.+o vᴵ)
    ⟦_⟧T  : (Aᴵ : I.Ty) → Ty Aᴵ
    ⟦_⟧t  : ∀{Aᴵ}(tᴵ : I.Tm Aᴵ) → Tm ⟦ Aᴵ ⟧T tᴵ
    ⟦ I.Nat           ⟧T = Nat
    ⟦ I.Bool          ⟧T = Bool
    ⟦ I.true          ⟧t = true
    ⟦ I.false         ⟧t = false
    ⟦ I.ite tᴵ uᴵ vᴵ  ⟧t = ite ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    ⟦ I.num n         ⟧t = num n
    ⟦ I.isZero tᴵ     ⟧t = isZero ⟦ tᴵ ⟧t
    ⟦ uᴵ I.+o vᴵ      ⟧t = ⟦ uᴵ ⟧t +o ⟦ vᴵ ⟧t

  open I -- All unqualified terms are from the initial model!

  -- Type inference

  data Result (A : Set) : Set where
    Ok : (r : A) → Result A
    Err : (e : ℕ) → Result A

  {-
    Error codes:
    · 1: Non boolean condition
    · 2: Differing branch types
    · 3: Non numeric isZero parameter
    · 4: Non numeric addition parameter
  -}

  -- true

  Infer : NatBoolAST.Model {i = lzero}
  Infer = record
    { Tm      = Result (Σ Ty (λ A → Tm A))
    ; true    = Ok (Bool , true)
    ; false   = Ok (Bool , false)
    ; ite     = {!   !}
    ; num     = λ x → Ok (Nat , num x)
    ; isZero  = λ where
                    (Ok (Nat , tm)) → Ok (Bool , isZero tm)
                    (Ok (Bool , tm)) → Err 3
                    (Err e) → Err e
    ; _+o_    = {!   !}
    }

  module INF = NatBoolAST.Model Infer

  inf-test : Result (Σ Ty (λ T → Tm T))
  inf-test = {! INF.⟦ isZero false ⟧  !}
    where open NatBoolAST.I
