{-# OPTIONS --prop --rewriting #-}
module gy09_after where

open import Lib

module Def where
  open import Def
  open Def.I

  -- Equalities with substitutions

  sub-swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
  sub-swap = (p ⊚ p) ,o q ,o (q [ p ])

  sub-eq-1 : {Γ : Con} → {σ δ : Sub Γ ∙} → σ ≡ δ
  sub-eq-1 {σ = σ} {δ = δ} =
    σ
      ≡⟨ ∙η ⟩
    ε
      ≡⟨ sym {A = Sub _ _} ∙η ⟩
    δ
      ∎

  sub-eq-2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → ε ⊚ σ ≡ ε
  sub-eq-2 {σ = σ} = ∙η

  sub-eq-3 :
    {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
    (σ ,o t) ⊚ δ ≡ (σ ⊚ δ ,o t [ δ ])
  sub-eq-3 {Γ} {Δ} {Θ} {σ} {δ} {A} {t} =
    (σ ,o t) ⊚ δ
      ≡⟨ sym {A = Sub _ _} ▹η ⟩
    ((p ⊚ ((σ ,o t) ⊚ δ)) ,o (q [ (σ ,o t) ⊚ δ ]))
      ≡⟨ cong {A = Sub _ _} (_,o (q [ (σ ,o t) ⊚ δ ])) (sym {A = Sub _ _} ass) ⟩
    (((p ⊚ (σ ,o t)) ⊚ δ) ,o (q [ (σ ,o t) ⊚ δ ]))
      ≡⟨ cong {A = Sub _ _} (λ a → a ⊚ δ ,o (q [ (σ ,o t) ⊚ δ ])) ▹β₁ ⟩
    ((σ ⊚ δ) ,o (q [ (σ ,o t) ⊚ δ ]))
      ≡⟨ cong {A = Tm _ _} ((σ ⊚ δ) ,o_) [∘] ⟩
    ((σ ⊚ δ) ,o ((q [ σ ,o t ]) [ δ ]))
      ≡⟨ cong {A = Tm _ _} (λ a → (σ ⊚ δ) ,o (a [ δ ])) ▹β₂ ⟩
    (σ ⊚ δ ,o t [ δ ])
      ∎

  sub-eq-4 :
    {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ⊚ p ,o q ])
  sub-eq-4 {σ = σ} {t = t} {u = u} =
    (def t u) [ σ ]
      ≡⟨ refl {x = def t u [ σ ]} ⟩
    ((u [ id ,o t ]) [ σ ])
      ≡⟨ sym {A = Tm _ _} [∘] ⟩
    (u [ (id ,o t) ⊚ σ ])
      ≡⟨ cong {A = Sub _ _} (u [_]) (
        ((id ,o t) ⊚ σ)
          ≡⟨ sub-eq-3 ⟩
        ((id ⊚ σ) ,o (t [ σ ]))
          ≡⟨ cong {A = Sub _ _} (_,o (t [ σ ])) (
            (id ⊚ σ)
              ≡⟨ idl ⟩
            σ
              ≡⟨ sym {A = Sub _ _} idr ⟩
            (σ ⊚ id)
              ≡⟨ cong {A = Sub _ _} (σ ⊚_) (sym {A = Sub _ _} ▹β₁) ⟩
            (σ ⊚ (p ⊚ (id ,o (t [ σ ]))))
              ≡⟨ sym {A = Sub _ _} ass ⟩
            ((σ ⊚ p) ⊚ (id ,o (t [ σ ])))
              ∎
          )⟩
        (((σ ⊚ p) ⊚ (id ,o (t [ σ ]))) ,o (t [ σ ]))
          ≡⟨ cong {A = Tm _ _} (((σ ⊚ p) ⊚ (id ,o (t [ σ ]))) ,o_) (sym {A = Tm _ _} ▹β₂) ⟩
        (((σ ⊚ p) ⊚ (id ,o (t [ σ ]))) ,o (q [ id ,o (t [ σ ]) ]))
          ≡⟨ sym {A = Sub _ _} sub-eq-3 ⟩
        (((σ ⊚ p) ,o q) ⊚ (id ,o (t [ σ ])))
          ∎
      )⟩
    (u [ ((σ ⊚ p) ,o q) ⊚ (id ,o (t [ σ ])) ])
      ≡⟨ [∘] ⟩
    ((u [ (σ ⊚ p) ,o q ]) [ id ,o (t [ σ ]) ])
      ≡⟨ refl {x = def (t [ σ ]) (u [ (σ ⊚ p) ,o q ])} ⟩
    def (t [ σ ]) (u [ σ ⊚ p ,o q ])
      ∎

module NatBool where
  open import NatBool using (I ; Model ; DepModel)
  open I

  isZ : ℕ → 𝟚
  isZ zero    = tt
  isZ (suc _) = ff

  St : Model
  St = record
    { Ty        = Set
    ; Tm        = λ A → A
    ; Nat       = ℕ
    ; Bool      = 𝟚
    ; true      = tt
    ; false     = ff
    ; ite       = if_then_else_
    ; num       = λ n → n
    ; isZero    = isZ
    ; _+o_      = _+_
    ; iteβ₁     = λ {A}{u}{v} → refl {x = u}
    ; iteβ₂     = λ {A}{u}{v} → refl {x = v}
    ; isZeroβ₁  = trivi
    ; isZeroβ₂  = trivi
    ; +β        = λ {m}{n} → refl {x = m + n}
    }
  module St = Model St

  ⌜_⌝N : ℕ → Tm Nat
  ⌜_⌝N = num

  ⌜_⌝B : 𝟚 → Tm Bool
  ⌜ tt ⌝B = true
  ⌜ ff ⌝B = false

  ⌜_⌝ : ∀ {A} → St.⟦ A ⟧T → Tm A
  ⌜_⌝ {Nat}  = ⌜_⌝N
  ⌜_⌝ {Bool} = ⌜_⌝B

  ⌜isZero⌝ : ∀{n} → ⌜ isZ n ⌝ ≡ isZero ⌜ n ⌝
  ⌜isZero⌝ {zero}   = sym {A = Tm Bool} isZeroβ₁
  ⌜isZero⌝ {suc n}  = sym {A = Tm Bool} isZeroβ₂

  ⌜+⌝ : ∀{m n} → ⌜ m + n ⌝N ≡ ⌜ m ⌝N +o ⌜ n ⌝N
  ⌜+⌝ {m}{n} = sym {A = Tm Nat} +β

  ⌜ite⌝ : ∀{b A}{u v : St.⟦ A ⟧T} → ⌜_⌝ {A} (if b then u else v) ≡ ite ⌜ b ⌝ ⌜ u ⌝ ⌜ v ⌝
  ⌜ite⌝ {tt}{A} = sym {A = Tm A} iteβ₁
  ⌜ite⌝ {ff}{A} = sym {A = Tm A} iteβ₂

  isZero-Comp : {t : Tm Nat} → ⌜ St.⟦ t ⟧t ⌝N ≡ t → ⌜ isZ St.⟦ t ⟧t ⌝B ≡ isZero t
  isZero-Comp {t = t} eq =
    ⌜ isZ St.⟦ t ⟧t ⌝
      ≡⟨ ⌜isZero⌝ ⟩
    isZero ⌜ St.⟦ t ⟧t ⌝
      ≡⟨ cong isZero eq ⟩
    isZero t
      ∎

  +-Comp : {l r : Tm Nat} → ⌜ St.⟦ l ⟧t ⌝N ≡ l → ⌜ St.⟦ r ⟧t ⌝N ≡ r →
           ⌜ St.⟦ l ⟧t + St.⟦ r ⟧t ⌝N ≡ l +o r
  +-Comp {l = l} {r = r} eql eqr =
    ⌜ St.⟦ l ⟧t + St.⟦ r ⟧t ⌝N
      ≡⟨ ⌜+⌝ ⟩
    ⌜ St.⟦ l ⟧t ⌝ +o ⌜ St.⟦ r ⟧t ⌝
      ≡⟨ cong (_+o ⌜ St.⟦ r ⟧t ⌝) eql ⟩
    (l +o ⌜ St.⟦ r ⟧t ⌝)
      ≡⟨ cong (l +o_) eqr ⟩
    l +o r
      ∎

  ite-Comp : {A : Ty} → {co : Tm Bool} → {tr fa : Tm A} →
             ⌜ St.⟦ co ⟧t ⌝ ≡ co → ⌜ St.⟦ tr ⟧t ⌝ ≡ tr → ⌜ St.⟦ fa ⟧t ⌝ ≡ fa →
             ⌜ if St.⟦ co ⟧t then St.⟦ tr ⟧t else St.⟦ fa ⟧t ⌝ ≡ ite co tr fa
  ite-Comp {A = A} {co = co} {tr = tr} {fa = fa} eqco eqtr eqfa =
    ⌜ if St.⟦ co ⟧t then St.⟦ tr ⟧t else St.⟦ fa ⟧t ⌝
      ≡⟨ ⌜ite⌝ ⟩
    ite ⌜ St.⟦ co ⟧t ⌝ ⌜ St.⟦ tr ⟧t ⌝ ⌜ St.⟦ fa ⟧t ⌝
      ≡⟨ cong {A = Tm _} (λ a → ite {A = A} a ⌜ St.⟦ tr ⟧t ⌝ ⌜ St.⟦ fa ⟧t ⌝) eqco ⟩
    ite co ⌜ St.⟦ tr ⟧t ⌝ ⌜ St.⟦ fa ⟧t ⌝
      ≡⟨ cong {A = Tm _} (λ a → ite co a ⌜ St.⟦ fa ⟧t ⌝) eqtr ⟩
    ite co tr ⌜ St.⟦ fa ⟧t ⌝
      ≡⟨ cong {A = Tm _} (λ a → ite co tr a) eqfa ⟩
    ite co tr fa
      ∎

  Comp : DepModel
  Comp = record
    { Ty        = λ _ → Lift ⊤
    ; Tm        = λ _ t → Lift (⌜ St.⟦ t ⟧t ⌝ ≡ t)
    ; Nat       = _
    ; Bool      = _
    ; num       = λ n → mk (refl {x = num n})
    ; isZero    = λ t → mk (isZero-Comp (un t))
    ; _+o_      = λ l r → mk (+-Comp (un l) (un r))
    ; true      = mk (refl {x = true})
    ; false     = mk (refl {x = false})
    ; ite       = λ co tr fa → mk (ite-Comp (un co) (un tr) (un fa))
    ; isZeroβ₁  = mk trivi
    ; isZeroβ₂  = mk trivi
    ; +β        = mk trivi
    ; iteβ₁     = mk trivi
    ; iteβ₂     = mk trivi
    }
  module Comp = DepModel Comp

  comp : {A : Ty} → (t : Tm A) → ⌜ St.⟦ t ⟧t ⌝ ≡ t
  comp t = un (Comp.⟦ t ⟧t)

  stab : {A : Ty}{t : St.⟦ A ⟧T} → St.⟦_⟧t {Aᴵ = A} ⌜ t ⌝ ≡ t
  stab {Nat}{t} = refl {x = t}
  stab {Bool} {tt} = trivi
  stab {Bool} {ff} = trivi

  -- Extra equalities

  choice𝟚 : (t : Tm Bool) → (Lift (true ≡ t) ⊎ Lift (false ≡ t))
  choice𝟚 t with comp t
  ... | eq with St.⟦ t ⟧t
  ... | tt = ι₁ (mk eq)
  ... | ff = ι₂ (mk eq)

  ite-eq : {A : Ty}{t : Tm Bool}{u : Tm A} → ite t u u ≡ u
  ite-eq {t = t} {u = u} with choice𝟚 t
  ... | ι₁ (mk eq) = coep {A = Tm _} (λ t' → ite t' u u ≡ u) eq iteβ₁
  ... | ι₂ (mk eq) = coep {A = Tm _} (λ t' → ite t' u u ≡ u) eq iteβ₂

  choiceℕ : (t : Tm Nat) → (Σ ℕ (λ n → Lift (num n ≡ t)))
  choiceℕ t with comp t
  ... | eq with St.⟦ t ⟧t
  ... | n = n , (mk eq)

  isZero1+ : {t : Tm Nat} → isZero (num 1 +o t) ≡ false
  isZero1+ {t = t} with choiceℕ t
  ... | n , (mk eq) = coep {A = Tm _} (λ t' → isZero (num 1 +o t') ≡ false) eq 1+n≠0
    where
      1+n≠0 : {n : ℕ} → isZero (num 1 +o num n) ≡ false
      1+n≠0 {n} =
        isZero (num 1 +o num n)
          ≡⟨ cong isZero +β ⟩
        isZero (num (1 + n))
          ≡⟨ isZeroβ₂ ⟩
        false
          ∎

  valid : {i j : Level}{M : Model {i} {j}} → (t : Tm Bool) →
         Lift (Model.true M ≡ Model.⟦_⟧t M t) ⊎ Lift (Model.false M ≡ Model.⟦_⟧t M t)
  valid t = {!   !}

  invalid : {i j : Level}{M : Model {i} {j}} → (t : (Model.Tm M) (Model.Bool M)) →
            Lift (Model.true M ≡ t) ⊎ Lift (Model.false M ≡ t)
  invalid t = {!   !}
