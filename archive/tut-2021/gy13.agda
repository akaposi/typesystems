{-# OPTIONS --prop --rewriting #-}
module gy13 where

open import Lib hiding (ℕ)

module LibExtra where

  data ℕ : Set where
    zero : ℕ
    suc : ℕ → ℕ
  iteℕ : ∀{i}{A : Set i}(u : A)(v : A → A)(t : ℕ) → A
  iteℕ u v zero = u
  iteℕ u v (suc t) = v (iteℕ u v t)

  one two three four five six : ℕ
  one  = suc zero  ; two  = suc one  ; three = suc two
  four = suc three ; five = suc four ; six   = suc five

  infixr 10 _∷_
  data <_> {i}(A : Set i) : Set i where
    <>  : < A >
    _∷_ : A → < A > → < A >
  ite<> : {A B : Set} → B → (A → B → B) → < A > → B
  ite<> b f <> = b
  ite<> b f (a ∷ as) = f a (ite<> b f as)

open LibExtra

module Ind where

  module I where
    infixl 6 _⊚_
    infixl 6 _[_]
    infixl 5 _▹_
    infixl 5 _,o_
    infixr 5 _⇒_
    infixl 5 _$_

    data Ty      : Set where
      _⇒_        : Ty → Ty → Ty
      -- TODO: Unit
      Bool       : Ty
      Nat        : Ty
      List       : Ty → Ty

    data Con     : Set where
      ∙          : Con
      _▹_        : Con → Ty → Con

    postulate
      Sub        : Con → Con → Set
      _⊚_        : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
      ass        : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
      id         : ∀{Γ} → Sub Γ Γ
      idl        : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
      idr        : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

      ε          : ∀{Γ} → Sub Γ ∙
      ∙η         : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

      Tm         : Con → Ty → Set
      _[_]       : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
      [∘]        : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
      [id]       : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
      _,o_       : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
      p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
      q          : ∀{Γ A} → Tm (Γ ▹ A) A
      ▹β₁        : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
      ▹β₂        : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
      ▹η         : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

      lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
      _$_        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
      ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
      ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
      lam[]      : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                  (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
      $[]        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                  (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

      -- TODO: Unit

      true       : ∀{Γ} → Tm Γ Bool
      false      : ∀{Γ} → Tm Γ Bool
      iteBool    : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
      Boolβ₁     : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
      Boolβ₂     : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
      true[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
      false[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
      iteBool[]  : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                  iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])

      zeroo      : ∀{Γ} → Tm Γ Nat
      suco       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
      iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
      Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
      Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                  iteNat u v (suco t) ≡ v [ id ,o iteNat u v t ]
      zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
      suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
      iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                  iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])

      nil        : ∀{Γ A} → Tm Γ (List A)
      cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
      iteList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
      Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → iteList u v nil ≡ u
      Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                  iteList u v (cons t₁ t) ≡ (v [ id ,o t₁ ,o iteList u v t ])
      nil[]      : ∀{Γ A Δ}{γ : Sub Δ Γ} → nil {Γ}{A} [ γ ] ≡ nil {Δ}{A}
      cons[]     : ∀{Γ A}{t₁ : Tm Γ A}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                  (cons t₁ t) [ γ ] ≡ cons (t₁ [ γ ]) (t [ γ ])
      iteList[]  : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                  iteList u v t [ γ ] ≡ iteList (u [ γ ]) (v [ (γ ⊚ p ,o q) ⊚ p ,o q ]) (t [ γ ])

    def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
    def t u = u [ id ,o t ]

    v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
    v0 = q
    v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
    v1 = q [ p ]
    v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
    v2 = q [ p ⊚ p ]
    v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
    v3 = q [ p ⊚ p ⊚ p ]

  variable
    Aᴵ Bᴵ : I.Ty
    Γᴵ Δᴵ Θᴵ : I.Con
    γᴵ δᴵ θᴵ σᴵ : I.Sub Δᴵ Γᴵ
    tᴵ uᴵ vᴵ : I.Tm Γᴵ Aᴵ

  record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
    infixl 6 _⊚_
    infixl 6 _[_]
    infixl 5 _▹_
    infixl 5 _,o_
    infixr 5 _⇒_
    infixl 5 _$_

    field
      Con       : Set i
      Sub       : Con → Con → Set j
      _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
      ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
      id        : ∀{Γ} → Sub Γ Γ
      idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
      idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

      ∙          : Con
      ε         : ∀{Γ} → Sub Γ ∙
      ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

      Ty        : Set k
      Tm        : Con → Ty → Set l
      _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
      [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
      [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
      _▹_        : Con → Ty → Con
      _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
      p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
      q         : ∀{Γ A} → Tm (Γ ▹ A) A
      ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
      ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
      ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

      _⇒_        : Ty → Ty → Ty
      lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
      _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
      ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
      ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
      lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                  (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
      $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                  (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

      -- TODO: Unit
      -- Unit      : Ty
      -- trivial   : ∀{Γ} → Tm Γ Unit
      -- iteUnit   : ∀{Γ A} → Tm Γ A → Tm Γ Unit → Tm Γ A
      -- Unitβ₁    : ∀{Γ A t} → iteUnit {Γ}{A} t trivial ≡ t
      -- trivial[] : ∀{Γ Δ}{γ : Sub Δ Γ} → trivial [ γ ] ≡ trivial
      -- iteUnit[] : ∀{Γ A t u Δ}{γ : Sub Δ Γ} →
      --             iteUnit {Γ}{A} u t [ γ ] ≡ iteUnit (u [ γ ]) (t [ γ ])

      Bool      : Ty
      true      : ∀{Γ} → Tm Γ Bool
      false     : ∀{Γ} → Tm Γ Bool
      iteBool   : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
      Boolβ₁    : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
      Boolβ₂    : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
      true[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
      false[]   : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
      iteBool[] : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                  iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])

      Nat        : Ty
      zeroo      : ∀{Γ} → Tm Γ Nat
      suco       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
      iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
      Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
      Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                  iteNat u v (suco t) ≡ v [ id ,o iteNat u v t ]
      zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
      suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
      iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                  iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])

      List       : Ty → Ty
      nil        : ∀{Γ A} → Tm Γ (List A)
      cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
      iteList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
      Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → iteList u v nil ≡ u
      Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                  iteList u v (cons t₁ t) ≡ (v [ id ,o t₁ ,o iteList u v t ])
      nil[]      : ∀{Γ A Δ}{γ : Sub Δ Γ} → nil {Γ}{A} [ γ ] ≡ nil {Δ}{A}
      cons[]     : ∀{Γ A}{t₁ : Tm Γ A}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                  (cons t₁ t) [ γ ] ≡ cons (t₁ [ γ ]) (t [ γ ])
      iteList[]  : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                  iteList u v t [ γ ] ≡ iteList (u [ γ ]) (v [ (γ ⊚ p ,o q) ⊚ p ,o q ]) (t [ γ ])

    def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
    def t u = u [ id ,o t ]

    v0 : ∀{Γ A}        → Tm (Γ ▹ A) A
    v0 = q
    v1 : ∀{Γ A B}      → Tm (Γ ▹ A ▹ B) A
    v1 = q [ p ]
    v2 : ∀{Γ A B C}    → Tm (Γ ▹ A ▹ B ▹ C) A
    v2 = q [ p ⊚ p ]
    v3 : ∀{Γ A B C D}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
    v3 = q [ p ⊚ p ⊚ p ]

    ⟦_⟧T : I.Ty → Ty
    ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
    -- TODO: Unit
    -- ⟦ I.Unit ⟧T = Unit
    ⟦ I.Bool ⟧T = Bool
    ⟦ I.Nat ⟧T = Nat
    ⟦ I.List A ⟧T = List ⟦ A ⟧T

    ⟦_⟧C : I.Con → Con
    ⟦ I.∙ ⟧C = ∙
    ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

    postulate
      ⟦_⟧S      : I.Sub  Δᴵ  Γᴵ  → Sub  ⟦ Δᴵ ⟧C  ⟦ Γᴵ ⟧C
      ⟦_⟧t      : I.Tm   Γᴵ  Aᴵ  → Tm   ⟦ Γᴵ ⟧C  ⟦ Aᴵ ⟧T
      ⟦∘⟧       : ⟦ γᴵ I.⊚ δᴵ ⟧S            ≡ ⟦ γᴵ ⟧S ⊚ ⟦ δᴵ ⟧S
      ⟦id⟧      : ⟦ I.id {Γᴵ} ⟧S            ≡ id
      ⟦ε⟧       : ⟦ I.ε {Γᴵ} ⟧S             ≡ ε
      ⟦[]⟧      : ⟦ tᴵ I.[ γᴵ ] ⟧t          ≡ ⟦ tᴵ ⟧t [ ⟦ γᴵ ⟧S ]
      ⟦,⟧       : ⟦ γᴵ I.,o tᴵ ⟧S           ≡ ⟦ γᴵ ⟧S ,o ⟦ tᴵ ⟧t
      ⟦p⟧       : ⟦ I.p {Γᴵ}{Aᴵ} ⟧S         ≡ p
      ⟦q⟧       : ⟦ I.q {Γᴵ}{Aᴵ} ⟧t         ≡ q
      {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦[]⟧ ⟦,⟧ ⟦p⟧ ⟦q⟧ #-}

      ⟦lam⟧     : ⟦ I.lam tᴵ ⟧t             ≡ lam ⟦ tᴵ ⟧t
      ⟦$⟧       : ⟦ tᴵ I.$ uᴵ ⟧t            ≡ ⟦ tᴵ ⟧t $ ⟦ uᴵ ⟧t
      {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}

      -- TODO: Unit
      -- ⟦trivial⟧  : ⟦ I.trivial {Γᴵ} ⟧t      ≡ trivial
      -- ⟦iteUnit⟧  : ⟦ I.iteUnit uᴵ tᴵ ⟧t     ≡ iteUnit ⟦ uᴵ ⟧t ⟦ tᴵ ⟧t
      -- {-# REWRITE ⟦trivial⟧ ⟦iteUnit⟧ #-}

      ⟦true⟧     : ⟦ I.true {Γᴵ} ⟧t         ≡ true
      ⟦false⟧    : ⟦ I.false {Γᴵ} ⟧t        ≡ false
      ⟦iteBool⟧  : ⟦ I.iteBool uᴵ vᴵ tᴵ ⟧t  ≡ iteBool ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
      {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦iteBool⟧ #-}

      ⟦zero⟧     : ⟦ I.zeroo {Γᴵ} ⟧t        ≡ zeroo
      ⟦suc⟧      : ⟦ I.suco tᴵ ⟧t           ≡ suco ⟦ tᴵ ⟧t
      ⟦iteNat⟧   : ⟦ I.iteNat uᴵ vᴵ tᴵ ⟧t   ≡ iteNat ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
      {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦iteNat⟧ #-}

      ⟦nil⟧      : ⟦ I.nil {Γᴵ}{Aᴵ} ⟧t      ≡ nil
      ⟦cons⟧     : ⟦ I.cons uᴵ vᴵ ⟧t        ≡ cons ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
      ⟦iteList⟧  : ⟦ I.iteList uᴵ vᴵ tᴵ ⟧t  ≡ iteList ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
      {-# REWRITE ⟦nil⟧ ⟦cons⟧ ⟦iteList⟧ #-}

  St : Model
  St = record
    { Con       = Set
    ; Sub       = λ Δ Γ → Δ → Γ
    ; _⊚_       = λ γ δ θ* → γ (δ θ*)
    ; ass       = λ {Γ}{Δ}{Θ}{Ξ} → refl {A = Ξ → Γ}
    ; id        = λ γ* → γ*
    ; idl       = λ {Γ}{Δ} → refl {A = Δ → Γ}
    ; idr       = λ {Γ}{Δ} → refl {A = Δ → Γ}

    ; ∙         = Lift ⊤
    ; ε         = _
    ; ∙η        = λ {Γ}{σ} → refl {A = Γ → Lift ⊤}

    ; Ty        = Set

    ; Tm        = λ Γ A → Γ → A
    ; _[_]      = λ a γ δ* → a (γ δ*)
    ; [∘]       = λ {Γ}{Δ}{Θ}{A} → refl {A = Θ → A}
    ; [id]      = λ {Γ}{A}{a} → refl {A = Γ → A}
    ; _▹_       = _×_
    ; _,o_      = λ γ t δ* → γ δ* , t δ*
    ; p         = π₁
    ; q         = π₂
    ; ▹β₁       = λ {Γ}{Δ} → refl {A = Δ → Γ}
    ; ▹β₂       = λ {Γ}{Δ}{A} → refl {A = Δ → A}
    ; ▹η        = λ {Γ}{Δ}{A} → refl {A = Δ → Γ × A}

    ; _⇒_       = λ A B → A → B
    ; lam       = λ t γ* α* → t (γ* , α*)
    ; _$_       = λ t u γ* → t γ* (u γ*)
    ; ⇒β        = λ {Γ}{A}{B}{t}{u} → refl {A = Γ → B}
    ; ⇒η        = λ {Γ}{A}{B}{t} → refl {A = Γ → A → B}
    ; lam[]     = λ {Γ}{A}{B}{t}{Δ}{γ} → refl {A = Δ → A → B}
    ; $[]       = λ {Γ}{A}{B}{t}{u}{Δ}{γ} → refl {A = Δ → B}

    -- TODO: Unit
    -- ; Unit      = ?
    -- ; trivial   = ?
    -- ; iteUnit   = ?
    -- ; Unitβ₁    = ?
    -- ; trivial[] = ?
    -- ; iteUnit[] = ?

    ; Bool      = 𝟚
    ; true      = λ _ → tt
    ; false     = λ _ → ff
    ; iteBool   = λ u v t γ* → if t γ* then u γ* else v γ*
    ; Boolβ₁    = λ {Γ}{A} → refl {A = Γ → A}
    ; Boolβ₂    = λ {Γ}{A} → refl {A = Γ → A}
    ; true[]    = λ {Γ}{Δ} → refl {A = Δ → 𝟚}{x = λ _ → tt}
    ; false[]   = λ {Γ}{Δ} → refl {A = Δ → 𝟚}{x = λ _ → ff}
    ; iteBool[] = λ {Γ}{A}{t}{u}{v}{Δ}{γ} → refl {A = Δ → A}

    ; Nat       = ℕ
    ; zeroo     = λ _ → zero
    ; suco      = λ t γ* → suc (t γ*)
    ; iteNat    = λ u v t γ* → iteℕ (u γ*) (λ x → v (γ* , x)) (t γ*)
    ; Natβ₁     = λ {Γ}{A}{u}{v} → refl {A = Γ → A}
    ; Natβ₂     = λ {Γ}{A}{u}{v} → refl {A = Γ → A}
    ; zero[]    = λ {Γ}{Δ}{γ} → refl {A = Δ → ℕ}{x = λ _ → zero}
    ; suc[]     = λ {Γ}{t}{Δ}{γ} → refl {A = Δ → ℕ}{x = λ δ* → suc (t (γ δ*))}
    ; iteNat[]  = λ {Γ}{A}{u}{v}{t}{Δ}{γ} → refl {A = Δ → A}

    ; List      = <_>
    ; nil       = λ _ → <>
    ; cons      = λ u v γ* → u γ* ∷ v γ*
    ; iteList   = λ u v t γ* → ite<> (u γ*) (λ x y → v ((γ* , x) , y)) (t γ*)
    ; Listβ₁    = λ {Γ}{A}{B}{u}{v} → refl {A = Γ → B}
    ; Listβ₂    = λ {Γ}{A}{B}{u}{v}{a}{t} → refl {A = Γ → B}
    ; nil[]     = λ {Γ}{A}{Δ}{γ} → refl {A = Δ → < A >}
    ; cons[]    = λ {Γ}{A}{u}{v}{Δ}{γ} → refl {A = Δ → < A >}
    ; iteList[] = λ {Γ}{A}{B}{u}{v}{t}{Δ}{γ} → refl {A = Δ → B}
    }
  module St = Model St

open Ind
open I

eval : {A : Ty} → Tm ∙ A → St.⟦ A ⟧T
eval t = St.⟦ t ⟧t (mk trivi)

{-
module _ where


  -- isZero

  isZero : {Γ : Con} → Tm Γ (Nat ⇒ Bool)
  isZero = {!   !}

  isZero-test-1 : eval isZero zero ≡ tt
  isZero-test-1 = tt ∎

  isZero-test-2 : eval isZero one ≡ ff
  isZero-test-2 = ff ∎

  isZero-test-3 : eval isZero six ≡ ff
  isZero-test-3 = ff ∎


  -- plus

  plus : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
  plus = {!   !}

  plus-test-1 : eval plus one one ≡ two
  plus-test-1 = refl {A = ℕ}

  plus-test-2 : eval plus two three ≡ five
  plus-test-2 = refl {A = ℕ}

  plus-test-3 : eval plus zero four ≡ four
  plus-test-3 = refl {A = ℕ}


  -- times

  times : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
  times = {!   !}

  times-test-1 : eval times one five ≡ five
  times-test-1 = refl {A = ℕ}

  times-test-2 : eval times two three ≡ six
  times-test-2 = refl {A = ℕ}

  times-test-3 : eval times zero four ≡ zero
  times-test-3 = refl {A = ℕ}


  -- length

  length : {Γ : Con}{A : Ty} → Tm Γ (List A ⇒ Nat)
  length = {!   !}

  length-test-1 : eval (length $ nil {A = Nat}) ≡ zero
  length-test-1 = refl {A = ℕ}

  length-test-2 : eval (length $ (cons trivial nil)) ≡ one
  length-test-2 = refl {A = ℕ}

  length-test-3 : eval (length $ (cons false (cons true nil))) ≡ two
  length-test-3 = refl {A = ℕ}


  -- concat

  concat : {Γ : Con}{A : Ty} → Tm Γ (List A ⇒ List A ⇒ List A)
  concat = {!   !}

  concat-test-1 : eval (concat $ nil {A = Bool} $ nil {A = Bool}) ≡ <>
  concat-test-1 = refl {A = < 𝟚 >}

  concat-test-2 : eval (concat $ nil $ (cons trivial nil)) ≡ mk trivi ∷ <>
  concat-test-2 = refl {A = < Lift ⊤ >}

  concat-test-3 : eval (concat $ (cons zeroo nil) $ (cons (suco zeroo) nil)) ≡
                  zero ∷ one ∷ <>
  concat-test-3 = refl {A = < ℕ >}


module _ where

  record LEq (A B : Ty) : Set where
    field
      f : {Γ : Con} → Tm Γ (A ⇒ B)
      g : {Γ : Con} → Tm Γ (B ⇒ A)

  record Iso (A B : Ty) : Set where
    field
      f : {Γ : Con} → Tm Γ (A ⇒ B)
      g : {Γ : Con} → Tm Γ (B ⇒ A)
      id₁ : (a : St.⟦ A ⟧T) → eval (lam (g $ (f $ q))) a ≡ a
      id₂ : (b : St.⟦ B ⟧T) → eval (lam (f $ (g $ q))) b ≡ b

  Nat↔UnitList : LEq Nat (List Unit)
  Nat↔UnitList = record
    { f = {!   !}
    ; g = {!   !}
    }
  open LEq Nat↔UnitList

  fg : (a : St.⟦ Nat ⟧T) → eval (lam (g $ (f $ q))) a ≡ a
  fg n = {!   !}

  gf : (b : St.⟦ List Unit ⟧T) → eval (lam (f $ (g $ q))) b ≡ b
  gf ul = {!   !}

  Nat≅UnitList : Iso Nat (List Unit)
  Nat≅UnitList = {!   !}
-}