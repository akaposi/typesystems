{-# OPTIONS --prop --rewriting #-}
module gy10 where

open import Lib

module _ where
  open import STT
  open import STT_lemmas
  open I

  constIsConst : {Γ : Con}{A B : Ty}{t : Tm Γ B}{u v : Tm Γ A} →
                lam (t [ p ]) $ u ≡ lam (t [ p ]) $ v
  constIsConst = {!   !}

  -- Internalzie addition and verify some test cases!

  add : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
  add = {!   !}

  add-test-1 : add $ num 0 $ num 1 ≡ num {∙} 1
  add-test-1 = {!   !}

  add-test-2 : add $ num 2 $ num 3 ≡ num {∙} 5
  add-test-2 = {!   !}

  -- Define function composition externally and internally as well!

  comp : {Γ : Con}{A B C : Ty} → Tm Γ (B ⇒ C) → Tm Γ (A ⇒ B) → Tm Γ (A ⇒ C)
  comp = {!   !}

  comp' : {Γ : Con}{A B C : Ty} → Tm Γ ((B ⇒ C) ⇒ (A ⇒ B) ⇒ (A ⇒ C))
  comp' = {!   !}

  -- Define generic internalization and externalization!

  internalize : {Γ : Con}{A B : Ty} → (Tm Γ A → Tm Γ B) → Tm Γ (A ⇒ B)
  internalize = {!   !}

  externalize : {Γ : Con}{A B : Ty} → Tm Γ (A ⇒ B) → (Tm Γ A → Tm Γ B)
  externalize = {!   !}

  -- Prove the elimination rules of the internalized `isZero`!

  isZero' : {Γ : Con} → Tm Γ (Nat ⇒ Bool)
  isZero' = internalize isZero

  isZero'β₁ : {Γ : Con} → isZero' $ num 0 ≡ true {Γ}
  isZero'β₁ = {!   !}

  isZero'β₂ : {Γ : Con}{n : ℕ} → isZero' $ num (1 + n) ≡ false {Γ}
  isZero'β₂ = {!   !}

  -- Define boolean negation internally and prove its elimination rules!

  not : {Γ : Con} → Tm Γ (Bool ⇒ Bool)
  not = {!   !}

  notβ₁ : {Γ : Con} → not $ true ≡ false {Γ}
  notβ₁ = {!   !}

  notβ₂ : {Γ : Con} → not $ false ≡ true {Γ}
  notβ₂ = {!   !}

module _ where
  module I where
    infixl 6 _⊚_
    infixl 6 _[_]
    infixl 5 _▹_
    infixl 5 _,o_
    infixr 5 _⇒_
    infixl 5 _$_

    data Ty     : Set where
      _⇒_       : Ty → Ty → Ty
      Prod      : Ty → Ty → Ty
      Unit      : Ty
      Sum       : Ty → Ty → Ty
      Empty     : Ty

    data Con    : Set where
      ∙         : Con
      _▹_       : Con → Ty → Con

    postulate
      Sub       : Con → Con → Set
      _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
      ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
      id        : ∀{Γ} → Sub Γ Γ
      idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
      idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

      ε         : ∀{Γ} → Sub Γ ∙
      ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

      Tm        : Con → Ty → Set
      _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
      [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
      [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
      _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
      p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
      q         : ∀{Γ A} → Tm (Γ ▹ A) A
      ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
      ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
      ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

      lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
      _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
      ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
      ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
      lam[]     : ∀{Γ Δ A B}{t : Tm (Γ ▹ A) B}{γ : Sub Δ Γ} →
                  (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
      $[]       : ∀{Γ Δ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{γ : Sub Δ Γ} →
                  (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

      pair      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (Prod A B)
      fst       : ∀{Γ A B} → Tm Γ (Prod A B) → Tm Γ A
      snd       : ∀{Γ A B} → Tm Γ (Prod A B) → Tm Γ B
      Prodβ₁    : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst (pair u v) ≡ u
      Prodβ₂    : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd (pair u v) ≡ v
      Prodη     : ∀{Γ A B}{t : Tm Γ (Prod A B)} → pair (fst t) (snd t) ≡ t
      pair[]    : ∀{Γ Δ A B}{u : Tm Γ A}{v : Tm Γ B}{γ : Sub Δ Γ} →
                  pair u v [ γ ] ≡ pair (u [ γ ]) (v [ γ ])

      trivial   : ∀{Γ} → Tm Γ Unit
      Unitη     : ∀{Γ}{u : Tm Γ Unit} → u ≡ trivial

      inl       : ∀{Γ A B} → Tm Γ A → Tm Γ (Sum A B)
      inr       : ∀{Γ A B} → Tm Γ B → Tm Γ (Sum A B)
      switch    : ∀{Γ A B C} → Tm Γ (Sum A B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C
      Sumβ₁     : ∀{Γ A B C}{t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
                  switch (inl t) u v ≡ u [ id ,o t ]
      Sumβ₂     : ∀{Γ A B C}{t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
                  switch (inr t) u v ≡ v [ id ,o t ]
      inl[]     : ∀{Γ Δ A B}{t : Tm Γ A}{γ : Sub Δ Γ} →
                  (inl {B = B} t) [ γ ] ≡ inl (t [ γ ])
      inr[]     : ∀{Γ Δ A B}{t : Tm Γ B}{γ : Sub Δ Γ} →
                  (inr {A = A} t) [ γ ] ≡ inr (t [ γ ])
      switch[]  : ∀{Γ Δ A B C}{t : Tm Γ (Sum A B)}
                  {u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{γ : Sub Δ Γ} →
                  (switch t u v) [ γ ] ≡
                  switch (t [ γ ]) (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ])

      absurd    : ∀{Γ A} → Tm Γ Empty → Tm Γ A
      absurd[]  : ∀{Γ Δ A}{t : Tm Γ Empty}{γ : Sub Δ Γ} →
                  (absurd {A = A} t) [ γ ] ≡ absurd (t [ γ ])

    def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
    def t u = u [ id ,o t ]

    v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
    v0 = q
    v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
    v1 = q [ p ]
    v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
    v2 = q [ p ⊚ p ]
    v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
    v3 = q [ p ⊚ p ⊚ p ]

    -- Implement the Boolean type!
    -- rot13 encoded hint: Hfr Fhz naq Havg!

    Bool : Ty
    Bool = {!   !}

    true : {Γ : Con} → Tm Γ Bool
    true = {!   !}

    false : {Γ : Con} → Tm Γ Bool
    false = {!   !}

    ite : {Γ : Con}{A : Ty} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    ite = {!   !}

    iteβ₁ : {Γ : Con}{A : Ty} → {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₁ = {!   !}

    iteβ₂ : {Γ : Con}{A : Ty} → {u v : Tm Γ A} → ite false u v ≡ v
    iteβ₂ = {!   !}

    -- Implement the Maybe type!
    -- rot13 encoded hint: Hfr Fhz naq Havg!
    --
    -- Since the name "Maybe" and it's usual constructors "Just" and "Nothing"
    -- are already used in `Lib`, use the following names:
    -- * Maybe   = Optional
    -- * Just    = Some
    -- * Nothing = None

    Optional : Ty → Ty
    Optional = {!   !}

    None : {Γ : Con}{A : Ty} → Tm Γ (Optional A)
    None = {!   !}

    Some : {Γ : Con}{A : Ty} → Tm Γ A → Tm Γ (Optional A)
    Some = {!   !}

    fromOptional : {Γ : Con}{A B : Ty} →
                   (Tm Γ B) → Tm Γ (A ⇒ B) → Tm Γ (Optional A) → Tm Γ B
    fromOptional = {!   !}

    fromOptionalβ₁ : {Γ : Con}{A B : Ty} → (n : Tm Γ B) → (s : Tm Γ (A ⇒ B)) →
                     fromOptional n s (None {A = A}) ≡ n
    fromOptionalβ₁ = {!   !}

    fromOptionalβ₂ : {Γ : Con}{A B : Ty} → (n : Tm Γ B) → (s : Tm Γ (A ⇒ B)) →
                     (t : Tm Γ A) → fromOptional n s (Some t) ≡ s $ t
    fromOptionalβ₂ = {!   !}