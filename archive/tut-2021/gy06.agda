{-# OPTIONS --prop --rewriting #-}
module gy06 where

open import Lib
module NB where
  open import NatBool
  open I

  TF : Model {i = lsuc lzero}
  TF = record
    { Ty = Raise (Lift ⊤)
    ; Tm = λ _ → 𝟚
    ; Nat = mk (mk trivi)
    ; Bool = mk (mk trivi)
    ; true = tt
    ; false = ff
    ; ite = λ where tt tr fa → tr
                    ff tr fa → fa
    ; num = λ where zero → ff
                    (suc n) → tt
    ; isZero = λ where tt → ff
                       ff → tt
    ; _+o_ = _∨_
    ; iteβ₁ = λ {A} {u} {v} → refl {x = u}
    ; iteβ₂ = λ {A} {u} {v} → refl {x = v}
    ; isZeroβ₁ = refl {x = tt}
    ; isZeroβ₂ = refl {x = ff}
    ; +β = λ where {zero} {zero} → refl {x = ff}
                   {zero} {suc n} → refl {x = tt}
                   {suc m} {zero} → refl {x = tt}
                   {suc m} {suc n} → refl {x = tt}
    }

  module TF = Model TF

  getType : 𝟚 → Prop
  getType tt = ⊤
  getType ff = ⊥

  true≠false : ¬ (true ≡ false)
  true≠false eq = coep (λ x → getType TF.⟦ x ⟧t) {a₀ = true} eq trivi

  true≠isZero : {t : Tm Nat} → ¬ (true ≡ isZero t)
  true≠isZero eq = {!   !}


module Int where

  module I where
    postulate
      Z     : Set
      Zero  : Z
      Suc   : Z → Z
      Pred  : Z → Z
      SucPred : (i : Z) → Suc (Pred i) ≡ i
      PredSuc : (i : Z) → Pred (Suc i) ≡ i
  variable
    i' : I.Z

  record Model {ℓ} : Set (lsuc ℓ) where
    field
      Z        : Set ℓ
      Zero     : Z
      Suc      : Z → Z
      Pred     : Z → Z
      SucPred  : (i : Z) → Suc (Pred i) ≡ i
      PredSuc  : (i : Z) → Pred (Suc i) ≡ i

    postulate
      ⟦_⟧      : I.Z → Z
      ⟦Zero⟧   : ⟦ I.Zero    ⟧ ≡ Zero
      ⟦Suc⟧    : ⟦ I.Suc i'  ⟧ ≡ Suc ⟦ i' ⟧
      ⟦Pred⟧   : ⟦ I.Pred i' ⟧ ≡ Pred ⟦ i' ⟧

      {-# REWRITE ⟦Zero⟧ ⟦Suc⟧ ⟦Pred⟧ #-}

  record DepModel {ℓ} : Set (lsuc ℓ) where
    field
      Z        : I.Z → Set ℓ
      Zero     : Z I.Zero
      Suc      : Z i' → Z (I.Suc  i')
      Pred     : Z i' → Z (I.Pred i')
      SucPred  : (i : Z i') → (Z ~) (I.SucPred i') (Suc (Pred i)) i
      PredSuc  : (i : Z i') → (Z ~) (I.PredSuc i') (Pred (Suc i)) i
    postulate
      ⟦_⟧      : (n : I.Z) → Z n
      ⟦Zero⟧   : ⟦ I.Zero     ⟧ ≡ Zero
      ⟦Suc⟧    : ⟦ I.Suc i'   ⟧ ≡ Suc   ⟦ i' ⟧
      ⟦Pred⟧   : ⟦ I.Pred i'  ⟧ ≡ Pred  ⟦ i' ⟧

      {-# REWRITE ⟦Zero⟧ #-}

  open I

  eq-0 : Pred (Suc Zero) ≡ Zero
  eq-0 = {!   !}

  eq-1 : Suc (Suc (Pred Zero)) ≡ Suc Zero
  eq-1 = {!   !}

  eq-2 : Pred (Pred (Suc Zero)) ≡ Suc (Suc (Pred (Pred (Pred Zero))))
  eq-2 =
    Pred (Pred (Suc Zero))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    Suc (Suc (Pred (Pred (Pred Zero))))
      ∎

  -- Normalisation

  data Nf  : Set where
    Negative : ℕ → Nf
    Neutral  : Nf
    Positive : ℕ → Nf

  SucNf : Nf → Nf
  SucNf n = {!   !}

  PredNf : Nf → Nf
  PredNf n = {!   !}

  N : Model {ℓ = lzero}
  N = record
    { Z       = {!   !}
    ; Zero    = {!   !}
    ; Suc     = {!   !}
    ; Pred    = {!   !}
    ; SucPred = {!   !}
    ; PredSuc = {!   !}
    }
  module N = Model N

  norm : I.Z → Nf
  norm = N.⟦_⟧

  test-norm = {! norm (I.Pred (I.Pred (I.Suc (I.Pred (I.Pred (I.Pred (I.Suc I.Zero)))))))  !}

  ⌜_⌝ : Nf → Z
  ⌜ n ⌝ = {!   !}

  test-quote = {! ⌜ Negative 5 ⌝  !}

  -- Stability

  stab : (n : Nf) → norm ⌜ n ⌝ ≡ n
  stab n = {!   !}

  -- Completeness

  ⌜Suc⌝ : (n : Nf) → ⌜ SucNf n ⌝ ≡ Suc ⌜ n ⌝
  ⌜Suc⌝ n = {!   !}

  ⌜Pred⌝ : (n : Nf) → ⌜ PredNf n ⌝ ≡ Pred ⌜ n ⌝
  ⌜Pred⌝ n = {!   !}

  Comp : DepModel {ℓ = lzero}
  Comp = record
    { Z        = λ i → Lift {!   !}
    ; Zero     = mk {!   !}
    ; Suc      = λ {i'} i → mk {!   !}
    ; Pred     = λ {i'} i → mk {!   !}
    ; SucPred  = λ _ → mk trivi
    ; PredSuc  = λ _ → mk trivi
    }
  module Comp = DepModel Comp

  comp : (i : I.Z) → ⌜ norm i ⌝ ≡ i
  comp i = un Comp.⟦ i ⟧


module DefABT where

  module I where
    data Var  : ℕ → Set where
      vz      : ∀{n} → Var (suc n)
      vs      : ∀{n} → Var n → Var (suc n)

    data Tm   : ℕ → Set where
      var     : Var n → Tm n
      def     : Tm  n → Tm (suc n) → Tm n

      true    : ∀ {n} → Tm n
      false   : ∀ {n} → Tm n
      ite     : ∀ {n} → Tm n → Tm n → Tm n → Tm n
      num     : ℕ → Tm n
      isZero  : ∀ {n} → Tm n → Tm n
      _+o_    : ∀ {n} → Tm n → Tm n → Tm n

    v0 : {n : ℕ} → Tm (1 + n)
    v0 = var vz
    v1 : {n : ℕ} → Tm (2 + n)
    v1 = var (vs vz)
    v2 : {n : ℕ} → Tm (3 + n)
    v2 = var (vs (vs vz))
    v3 : {n : ℕ} → Tm (4 + n)
    v3 = var (vs (vs (vs vz)))

  open I


  {- Rewrite the following expressions with De Bruijn notation -}

  -- let x:=1 in x + let y:=x+1 in y + let z:=x+y in (x+z)+(y+x)
  tm-1 : Tm {!   !}
  tm-1 = {!   !}

  -- (let x:=1 in x) + let y:=1 in y + let z:=x+y in (x+z)+(y+x)
  tm-2 : Tm {!   !}
  tm-2 = {!   !}

  -- (let x:=1 in x + let y:=x+1 in y) + let z:=1 in z+z
  tm-3 : Tm 0
  tm-3 = {!   !}

  -- (let x:=1 in x) + (let y:=1 in y) + let z:=1 in z+z
  tm-4 : Tm 0
  tm-4 = {!   !}

  -- let x:=(isZero true) in (ite x 0 x)
  tm-5 : Tm 0
  tm-5 = {!   !}


  {- Rewrite the following expressions with variable names -}

  --
  t-1 : Tm 0
  t-1 = def (num 1 +o num 2) (v0 +o v0) +o def (num 3 +o num 4) (v0 +o v0)

  --
  t-1' : Tm 0
  t-1' = def (num 1 +o num 2) ((v0 +o v0) +o def (num 3 +o num 4) (v1 +o v0))

  --
  t-2 : Tm 0
  t-2 = def true (v0 +o def v0 (v0 +o v1))

  --
  t-3 : Tm 0
  t-3 = def true (def false (ite v0 v0 v1))

  --
  t-4 : Tm 0
  t-4 = true +o def true (false +o def v0 (v1 +o v0))

  --
  t-5 : Tm 0
  t-5 = def true (def false (def true (def false ((v0 +o v1) +o (v2 +o v3)))))
