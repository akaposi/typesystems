{-# OPTIONS --prop --rewriting #-}
module gy09 where

open import Lib

module Def where
  open import Def
  open Def.I

  -- Equalities with substitutions

  sub-swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
  sub-swap = {!   !}

  sub-eq-1 : {Γ : Con} → {σ δ : Sub Γ ∙} → σ ≡ δ
  sub-eq-1 {σ = σ} {δ = δ} =
    σ
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    δ
      ∎

  sub-eq-2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → ε ⊚ σ ≡ ε
  sub-eq-2 = {!  !}

  sub-eq-3 :
    {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
    (σ ,o t) ⊚ δ ≡ (σ ⊚ δ ,o t [ δ ])
  sub-eq-3 {Γ} {Δ} {Θ} {σ} {δ} {A} {t} =
    (σ ,o t) ⊚ δ
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    (σ ⊚ δ ,o t [ δ ])
      ∎

  sub-eq-4 :
    {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ⊚ p ,o q ])
  sub-eq-4 {σ = σ} {t = t} {u = u} =
    (def t u) [ σ ]
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    def (t [ σ ]) (u [ σ ⊚ p ,o q ])
      ∎

module NatBool where
  open import NatBool using (I ; Model ; DepModel)
  open I

  isZ : ℕ → 𝟚
  isZ zero    = tt
  isZ (suc n) = ff

  St : Model
  St = record
    { Ty        = Set
    ; Tm        = λ A → A
    ; Nat       = ℕ
    ; Bool      = 𝟚
    ; true      = tt
    ; false     = ff
    ; ite       = if_then_else_
    ; num       = λ n → n
    ; isZero    = isZ
    ; _+o_      = _+_
    ; iteβ₁     = λ {A}{u}{v} → refl {x = u}
    ; iteβ₂     = λ {A}{u}{v} → refl {x = v}
    ; isZeroβ₁  = trivi
    ; isZeroβ₂  = trivi
    ; +β        = λ {m}{n} → refl {x = m + n}
    }
  module St = Model St

  ⌜_⌝N : ℕ → Tm Nat
  ⌜_⌝N = num

  ⌜_⌝B : 𝟚 → Tm Bool
  ⌜ tt ⌝B = true
  ⌜ ff ⌝B = false

  ⌜_⌝ : ∀ {A} → St.⟦ A ⟧T → Tm A
  ⌜_⌝ {Nat}  = ⌜_⌝N
  ⌜_⌝ {Bool} = ⌜_⌝B

  ⌜isZero⌝ : ∀{n} → ⌜ isZ n ⌝ ≡ isZero ⌜ n ⌝
  ⌜isZero⌝ {zero}   = sym {A = Tm Bool} isZeroβ₁
  ⌜isZero⌝ {suc n}  = sym {A = Tm Bool} isZeroβ₂

  ⌜+⌝ : ∀{m n} → ⌜ m + n ⌝N ≡ ⌜ m ⌝N +o ⌜ n ⌝N
  ⌜+⌝ {m}{n} = sym {A = Tm Nat} +β

  ⌜ite⌝ : ∀{b A}{u v : St.⟦ A ⟧T} → ⌜_⌝ {A} (if b then u else v) ≡ ite ⌜ b ⌝ ⌜ u ⌝ ⌜ v ⌝
  ⌜ite⌝ {tt}{A} = sym {A = Tm A} iteβ₁
  ⌜ite⌝ {ff}{A} = sym {A = Tm A} iteβ₂

  isZero-Comp : {t : Tm Nat} → ⌜ St.⟦ t ⟧t ⌝N ≡ t → ⌜ isZ St.⟦ t ⟧t ⌝B ≡ isZero t
  isZero-Comp {t = t} eq =
    ⌜ isZ St.⟦ t ⟧t ⌝B
      ≡⟨ {!   !} ⟩
    isZero t
      ∎

  +-Comp : {l r : Tm Nat} → ⌜ St.⟦ l ⟧t ⌝N ≡ l → ⌜ St.⟦ r ⟧t ⌝N ≡ r →
           ⌜ St.⟦ l ⟧t + St.⟦ r ⟧t ⌝N ≡ l +o r
  +-Comp {l = l} {r = r} eql eqr =
    ⌜ St.⟦ l ⟧t + St.⟦ r ⟧t ⌝N
      ≡⟨ {!   !} ⟩
    l +o r
      ∎

  ite-Comp : {A : Ty} → {co : Tm Bool} → {tr fa : Tm A} →
             ⌜ St.⟦ co ⟧t ⌝ ≡ co → ⌜ St.⟦ tr ⟧t ⌝ ≡ tr → ⌜ St.⟦ fa ⟧t ⌝ ≡ fa →
             ⌜ if St.⟦ co ⟧t then St.⟦ tr ⟧t else St.⟦ fa ⟧t ⌝ ≡ ite co tr fa
  ite-Comp {co = co} {tr = tr} {fa = fa} eqco eqtr eqfa =
    ⌜ if St.⟦ co ⟧t then St.⟦ tr ⟧t else St.⟦ fa ⟧t ⌝
      ≡⟨ {!   !} ⟩
    ite co tr fa
      ∎

  Comp : DepModel
  Comp = record
    { Ty        = λ _ → Lift ⊤
    ; Tm        = λ _ t → Lift (⌜ St.⟦ t ⟧t ⌝ ≡ t)
    ; Nat       = _
    ; Bool      = _
    ; num       = λ n → mk (refl {x = num n})
    ; isZero    = λ t → mk (isZero-Comp (un t))
    ; _+o_      = λ l r → mk (+-Comp (un l) (un r))
    ; true      = mk (refl {x = true})
    ; false     = mk (refl {x = false})
    ; ite       = λ co tr fa → mk (ite-Comp (un co) (un tr) (un fa))
    ; isZeroβ₁  = mk trivi
    ; isZeroβ₂  = mk trivi
    ; +β        = mk trivi
    ; iteβ₁     = mk trivi
    ; iteβ₂     = mk trivi
    }
  module Comp = DepModel Comp

  comp : {A : Ty} → (t : Tm A) → ⌜ St.⟦ t ⟧t ⌝ ≡ t
  comp t = un (Comp.⟦ t ⟧t)

  stab : {A : Ty}{t : St.⟦ A ⟧T} → St.⟦_⟧t {Aᴵ = A} ⌜ t ⌝ ≡ t
  stab {Nat}{t} = refl {x = t}
  stab {Bool} {tt} = trivi
  stab {Bool} {ff} = trivi

  -- Extra equalities

  choice𝟚 : (t : Tm Bool) → (Lift (true ≡ t) ⊎ Lift (false ≡ t))
  choice𝟚 t = {!   !}

  ite-eq : {A : Ty}{t : Tm Bool}{u : Tm A} → ite t u u ≡ u
  ite-eq {t = t} {u = u} = {!   !}

  choiceℕ : (t : Tm Nat) → (Σ ℕ (λ n → Lift (num n ≡ t)))
  choiceℕ t = {!   !}

  isZero1+ : {t : Tm Nat} → isZero (num 1 +o t) ≡ false
  isZero1+ {t = t} = {!   !}