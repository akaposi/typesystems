{-# OPTIONS --prop --rewriting #-}
module gy10_after where

open import Lib

module _ where
  open import STT
  open import STT_lemmas
  open I

  constIsConst : {Γ : Con}{A B : Ty}{t : Tm Γ B}{u v : Tm Γ A} →
                 lam (t [ p ]) $ u ≡ lam (t [ p ]) $ v
  constIsConst {t = t} {u = u} {v = v} =
    (lam (t [ p ]) $ u)
      ≡⟨ ⇒β ⟩ -- \=>
    ((t [ p ]) [ id ,o u ])
      ≡⟨ sym-[∘] ⟩
    (t [ p ⊚ (id ,o u) ])
      ≡⟨ cong {A = Sub _ _} (λ a → t [ a ]) ▹β₁ ⟩
    (t [ id ])
    --   ≡⟨ [id] ⟩
    -- t
    --   ≡⟨ sym {A = Tm _ _} [id] ⟩
    -- (t [ id ])
      ≡⟨ cong {A = Sub _ _} (λ a → t [ a ]) (sym {A = Sub _ _} ▹β₁) ⟩
    (t [ p ⊚ (id ,o v) ])
      ≡⟨ [∘] ⟩
    ((t [ p ]) [ id ,o v ])
      ≡⟨ sym {A = Tm _ _} ⇒β ⟩
    (lam (t [ p ]) $ v)
      ∎

  -- Internalzie addition and verify some test cases!

  -- _+_ : ℕ → ℕ → ℕ
  add : {Γ : Con} → Tm Γ (Nat ⇒ (Nat ⇒ Nat))
  add = lam (lam (v1 +o v0))

  add-test-1 : (add $ num 0) $ num 1 ≡ num {∙} 1
  add-test-1 =
    ((lam (lam ((q [ p ]) +o q)) $ num 0) $ num 1)
      ≡⟨ cong-$₁ (
        (lam (lam ((q [ p ]) +o q)) $ num 0)
          ≡⟨ ⇒β ⟩
        (lam ((q [ p ]) +o q) [ id ,o num 0 ])
          ≡⟨ lam[] ⟩
        lam (((q [ p ]) +o q) [ ((id ,o num 0) ⊚ p) ,o q ])
          ≡⟨ cong-lam (
            (((q [ p ]) +o q) [ ((id ,o num 0) ⊚ p) ,o q ])
              ≡⟨ +[] ⟩
            (((q [ p ]) [ ((id ,o num 0) ⊚ p) ,o q ]) +o (q [ ((id ,o num 0) ⊚ p) ,o q ]))
              ≡⟨ cong-+₂ ▹β₂ ⟩
            (((q [ p ]) [ ((id ,o num 0) ⊚ p) ,o q ]) +o q)
              ≡⟨ cong-+₁ sym-[∘] ⟩
            ((q [ p ⊚ (((id ,o num 0) ⊚ p) ,o q) ]) +o q)
              ≡⟨ cong-+₁ (cong-[]₂ ▹β₁) ⟩
            ((q [ (id ,o num 0) ⊚ p ]) +o q)
              ≡⟨ cong-+₁ (cong-[]₂ ,∘) ⟩
            ((q [ (id ⊚ p) ,o (num 0 [ p ]) ]) +o q)
              ≡⟨ cong-+₁ ▹β₂ ⟩
            ((num 0 [ p ]) +o q)
              ≡⟨ cong-+₁ num[] ⟩
            (num 0 +o q) ∎
          )⟩
        lam (num 0 +o q)
          ∎
      )⟩
    (lam (num 0 +o q) $ num 1)
      ≡⟨ ⇒β ⟩
    ((num 0 +o q) [ id ,o num 1 ])
      ≡⟨ +[] ⟩
    ((num 0 [ id ,o num 1 ]) +o (q [ id ,o num 1 ]))
      ≡⟨ cong-+₂ ▹β₂ ⟩
    ((num 0 [ id ,o num 1 ]) +o num 1)
      ≡⟨ cong-+₁ num[] ⟩
    (num 0 +o num 1)
      ≡⟨ +β ⟩
    num 1
      ∎

  -- add-test-2 : add $ num 2 $ num 3 ≡ num {∙} 5
  -- add-test-2 = {!   !}

  -- Define function composition externally and internally as well!

  comp : {Γ : Con}{A B C : Ty} → Tm Γ (B ⇒ C) → Tm Γ (A ⇒ B) → Tm Γ (A ⇒ C)
  comp f g = lam (f [ p ] $ (g [ p ] $ v0))

  comp' : {Γ : Con}{A B C : Ty} → Tm Γ ((B ⇒ C) ⇒ (A ⇒ B) ⇒ (A ⇒ C))
  comp' = lam (lam (lam (v2 $ (v1 $ v0))))

  -- Define generic internalization and externalization!

  extFun : Con → Ty → Ty → Set
  extFun Γ A B = Tm Γ A → Tm Γ B

  intFun : Con → Ty → Ty → Set
  intFun Γ A B = Tm Γ (A ⇒ B)

  internalize : {Γ : Con}{A B : Ty} → ({Δ : Con} → extFun Δ A B) → intFun Γ A B
  internalize f = lam (f v0)

  externalize : {Γ : Con}{A B : Ty} → ({Δ : Con} → intFun Δ A B) → extFun Γ A B
  externalize f a = f $ a

  -- Prove the elimination rules of the internalized `isZero`!

  isZero' : {Γ : Con} → Tm Γ (Nat ⇒ Bool)
  isZero' = internalize isZero

  isZero'β₁ : {Γ : Con} → isZero' $ num 0 ≡ true {Γ}
  isZero'β₁ =
    (lam (isZero q) $ num zero)
      ≡⟨ ⇒β ⟩
    (isZero q [ id ,o num zero ])
      ≡⟨ isZero[] ⟩
    isZero (q [ id ,o num zero ])
      ≡⟨ cong-isZero ▹β₂ ⟩
    isZero (num zero)
      ≡⟨ isZeroβ₁ ⟩
    true
      ∎

  isZero'β₂ : {Γ : Con}{n : ℕ} → isZero' $ num (1 + n) ≡ false {Γ}
  isZero'β₂ {n = n} =
    (lam (isZero q) $ num (suc n))
      ≡⟨ ⇒β ⟩
    (isZero q [ id ,o num (suc n) ])
      ≡⟨ isZero[] ⟩
    isZero (q [ id ,o num (suc n) ])
      ≡⟨ cong-isZero ▹β₂ ⟩
    isZero (num (suc n))
      ≡⟨ isZeroβ₂ ⟩
    false
      ∎

  -- Define boolean negation internally and prove its elimination rules!

  not : {Γ : Con} → Tm Γ (Bool ⇒ Bool)
  not = lam (ite v0 false true)

  notβ₁ : {Γ : Con} → not $ true ≡ false {Γ}
  notβ₁ =
    (lam (ite q false true) $ true)
      ≡⟨ ⇒β ⟩
    (ite q false true [ id ,o true ])
      ≡⟨ ite[] ⟩
    ite (q [ id ,o true ]) (false [ id ,o true ]) (true [ id ,o true ])
      ≡⟨ cong-ite₁ ▹β₂ ⟩
    ite true (false [ id ,o true ]) (true [ id ,o true ])
      ≡⟨ iteβ₁ ⟩
    (false [ id ,o true ])
      ≡⟨ false[] ⟩
    false
      ∎

  notβ₂ : {Γ : Con} → not $ false ≡ true {Γ}
  notβ₂ =
    (lam (ite q false true) $ false)
      ≡⟨ ⇒β ⟩
    (ite q false true [ id ,o false ])
      ≡⟨ ite[] ⟩
    ite (q [ id ,o false ]) (false [ id ,o false ]) (true [ id ,o false ])
      ≡⟨ cong-ite₁ ▹β₂ ⟩
    ite false (false [ id ,o false ]) (true [ id ,o false ])
      ≡⟨ iteβ₂ ⟩
    (true [ id ,o false ])
      ≡⟨ true[] ⟩
    true
      ∎
