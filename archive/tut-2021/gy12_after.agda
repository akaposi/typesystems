{-# OPTIONS --prop --rewriting #-}
module gy12_after where

module _ where
  open import Agda.Primitive


  -- Bool

  data 𝟚 : Set where
    tt ff : 𝟚

  data ℕ : Set where
    zero : ℕ
    suc : ℕ → ℕ

  record BoolModel {i} : Set (lsuc i) where
    field
      A     : Set i
      True  : A
      False : A

    ⟦_⟧ : 𝟚 → A
    ⟦ tt ⟧ = True
    ⟦ ff ⟧ = False

  testBoolModel : BoolModel
  testBoolModel = record { A = ℕ ; True = suc (zero) ; False = zero }

  test = {! ⟦ tt ⟧  !} where open BoolModel testBoolModel

  -- iteBool :          Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
  iteBool : {A : Set} →      A →      A →      𝟚    →      A
  iteBool {A = A} t f b = ⟦ b ⟧
    where open BoolModel (record { A = A ; True = t ; False = f })

  not : 𝟚 → 𝟚
  not tt = ff
  not ff = tt

  test' = {! iteBool (suc (suc (suc zero))) (suc zero) tt !}

  -- Nat


  record NatModel {i} : Set (lsuc i) where
    field
      A    : Set i
      Zero : A
      Suc  : A → A

    ⟦_⟧ : ℕ → A
    ⟦ zero  ⟧ = Zero
    ⟦ suc n ⟧ = Suc ⟦ n ⟧


  -- iteNat :          Tm Γ A → Tm (Γ ▹ A)  A  → Tm Γ Nat → Tm Γ A
  iteNat : {A : Set} →      A →        (A → A) →      ℕ   →      A
  iteNat {A = A} z s n = ⟦ n ⟧
    where open NatModel (record { A = A ; Zero = z ; Suc = s })

  -- suc (suc (suc zero))
  -- not (not (not tt)) = ff

  test'' = {! iteNat tt not (suc (suc (suc zero)))  !}


  -- Maybe

  data Maybe {i}(A : Set i) : Set i where
    Nothing  : Maybe A
    Just     : A → Maybe A

  record MaybeModel {i}(A : Set i) : Set (lsuc i) where
    field
      B    : Set i
      None : B
      Some : A → B

    ⟦_⟧ : (Maybe A) → B
    ⟦ Nothing ⟧ = None
    ⟦ Just a  ⟧ = Some a

    -- MaybeModel 𝟚
    -- ===
    -- None
    -- Some tt \___ Inclusion
    -- Some ff /

  -- iteMaybe :            Tm Γ B → Tm Γ (A ⇒ B) → Tm Γ (Maybe A) → Tm Γ B
  iteMaybe : {A B : Set} →      B →      (A → B) →       Maybe A  →      B
  iteMaybe {A = A} {B = B} n s m = ⟦ m ⟧
    where open MaybeModel (record { B = B ; None = n ; Some = s })


  -- Lists

  data <_> {i}(A : Set i) : Set i where
    <>  : < A >
    _∷_ : A → < A > → < A >

  record ListModel {i}{A : Set} : Set (lsuc i) where
    field
      B    : Set i
      Nil  : B
      Cons : A → B → B

    ⟦_⟧ : < A > → B
    ⟦ <>     ⟧ = Nil
    ⟦ a ∷ as ⟧ = Cons a ⟦ as ⟧

  -- iteList :            Tm Γ B → Tm (Γ ▹ A ▹ B)  B  → Tm Γ (List A) → Tm Γ B
  iteList : {A B : Set} →      B →        (A → B → B) →        < A >  →      B
  iteList {A = A} {B = B} n c l = ⟦ l ⟧
    where open ListModel (record { B = B ; Nil = n ; Cons = c })


module _ where
  open import Lib
  open import Fin
  open I hiding (caseo ; absurd)

  caseo : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ (A +o B) → Tm Γ C
  caseo l r s = I.caseo l r [ id ,o s ]

  absurd : {Γ : Con}{A : Ty} → Tm Γ Empty → Tm Γ A
  absurd e = I.absurd [ id ,o e ]

  module Boolean where
    Bool : Ty
    Bool = Unit +o Unit

    true : {Γ : Con} → Tm Γ Bool
    true = inl trivial

    false : {Γ : Con} → Tm Γ Bool
    false = inr trivial

    ite : {Γ : Con}{A : Ty} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    ite b u v = caseo (u [ p ]) (v [ p ]) b
  open Boolean

  -- Logical equivalence

  record LEq (A B : Ty) : Set where
    field
      f : {Γ : Con} → Tm Γ (A ⇒ B)
      g : {Γ : Con} → Tm Γ (B ⇒ A)
  open LEq

  -- Isomorphism

  record Iso (A B : Ty) : Set where
    field
      f : {Γ : Con} → Tm Γ (A ⇒ B)
      g : {Γ : Con} → Tm Γ (B ⇒ A)
      id₁ : {Γ : Con} → lam {Γ} (f $ (g $ q)) ≡ lam q
      id₂ : {Γ : Con} → lam {Γ} (g $ (f $ q)) ≡ lam q


  -- Sum

  -- a + 0 = a
  sumUnitRight : {A : Ty} → LEq (A +o Empty) A
  sumUnitRight = record
    { f = lam (caseo v0 (absurd v0) v0)
    ; g = lam (inl v0)
    }

  sumFlip : {Γ : Con}{A B : Ty} →
            Tm Γ ((A +o B) ⇒ (B +o A))
  sumFlip = lam (caseo (inr v0) (inl v0) v0)

  -- a + b = b + a
  sumCommute : {A B : Ty} → LEq (A +o B) (B +o A)
  sumCommute {A = A} {B = B} = record
    { f = lam (caseo (inr {A = B} v0) (inl v0) v0)
    ; g = lam (caseo (inr {A = A} v0) (inl v0) v0)
    -- { f = sumFlip
    -- ; g = sumFlip
    }

  -- (a + b) + c = a + (b + c)
  sumAssoc : {A B C : Ty} → LEq ((A +o B) +o C) (A +o (B +o C))
  sumAssoc = record
    { f = lam {!   !}
    ; g = lam {!   !}
    }


  -- Prod

  -- a * 0 = 0
  prodNullRight : {A : Ty} → LEq (A ×o Empty) Empty
  prodNullRight = record
    { f = lam {!   !}
    ; g = lam {!   !}
    }

  -- a * 1 = a
  prodUnitRight : {A : Ty} → LEq (A ×o Unit) A
  prodUnitRight = record
    { f = lam {!   !}
    ; g = lam {!   !}
    }

  -- a * b = b * a
  prodCommute : {A B : Ty} → LEq (A ×o B) (B ×o A)
  prodCommute = record
    { f = lam {!   !}
    ; g = lam {!   !}
    }

  -- (a * b) * c = a * (b * c)
  prodAssoc : {A B C : Ty} → LEq ((A ×o B) ×o C) (A ×o (B ×o C))
  prodAssoc = record
    { f = lam {!   !}
    ; g = lam {!   !}
    }


  -- Sum and Prod

  -- (a + b) * c = (a * c) + (b * c)
  distrib : {A B C : Ty} → LEq ((A +o B) ×o C) ((A ×o C) +o (B ×o C))
  distrib = record
    { f = lam {!   !}
    ; g = lam {!   !}
    }


  -- Functions

  -- a ^ 0 = 1
  powerNull : {A : Ty} → LEq (Empty ⇒ A) Unit
  powerNull = record
    { f = lam {!   !}
    ; g = lam {!   !}
    }

  -- a ^ 1 = a
  powerUnit : {A : Ty} → LEq (Unit ⇒ A) A
  powerUnit = record
    { f = lam {!   !}
    ; g = lam {!   !}
    }

  -- a ^ 2 = a * a
  square : {A : Ty} → LEq (Bool ⇒ A) (A ×o A)
  square = record
    { f = lam {!   !}
    ; g = lam {!   !}
    }

  -- (a + b) ^ 2 = a^2 + 2*a*b + b^2
  squareOfSum : {A B : Ty} → LEq
                  (Bool ⇒ (A +o B))
                  ((Bool ⇒ A) +o (Bool ×o A ×o B) +o (Bool ⇒ B))
  squareOfSum = record
    { f = lam (caseo
        (caseo
          (inl (inl (lam (ite v0 v2 v1))))
          (inl (inr ⟨ ⟨ true , v1 ⟩ , v0 ⟩))
        (v1 $ false))
        (caseo
          (inl (inr ⟨ ⟨ false , v0 ⟩ , v1 ⟩))
          ((inr (lam (ite v0 v2 v1))))
        (v1 $ false))
        (v0 $ true))
    ; g = lam (lam (caseo (caseo
        (inl (v0 $ v2))
        (ite (fst (fst v0)) (inl (snd (fst v0))) (inr (snd v0))) v0)
        (inr (v0 $ v1)) v1))
    }