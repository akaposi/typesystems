https://bitbucket.org/akaposi/typesystems/src/master/src/main.pdf

_⇒_ : Ty → Ty → Ty
konstr: lam : Tm (Γ▹A) B → Tm Γ (A ⇒ B)
destr: _$_ : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
β: lam t $ u = t[id,u]
η: lam (t[p] $ q) = t                λ x.(t$x) = t                       λ x → t x
  t    : Tm Γ (A ⇒ B)                t : Tm Γ (A⇒B)
  t[p] : Tm (Γ▹A) (A⇒B)              t : Tm (Γ▹x:A) (A⇒B)
  t[p] $ q : Tm (Γ▹A) B              t $ x : Tm (Γ▹x:A) B
  lam (t[p] $ q) : Tm Γ (A ⇒ B)      λ x . (t $ x) : Tm Γ (A ⇒ B)

lam (lam (v1 $ v0)) = lam (lam (q[p] $ q)) = lam q = lam v0

-----------------------------------------------------------------------------

nyelvnek van modellje: értelmes nyelv, konzisztens nyelv
nyelvnek van "konstruktív" modellje = nyelvnek van modellje Agdában, akkor az egy progamozási nyelv

Def nyelv - standard modell: legegyszerűbb értelmezése a nyelvnek (ebbe való kiértékelés: metacirkuláris interpetáció, kiértékelés)

(Γ:I.Con)  ⟦Γ⟧ : Set
A :I.Ty    ⟦A⟧ : Set
t : Tm Γ A    ⟦t⟧ : ⟦Γ⟧ → ⟦A⟧
⟦∙⟧ := ⊤
⟦Nat⟧ := ℕ
t : Tm ∙ Nat  ⟦t⟧ : ⊤ → ℕ    ⟦t⟧ trivi : ℕ
⟦Γ▹A⟧ := ⟦Γ⟧ × ⟦A⟧

⟦_⟧ : I.Ty → Set
⟦A I.⇒ B⟧ := ⟦A⟧ → ⟦B⟧
⟦lam t⟧ := λ γ* a* → t (γ* , a*)
           ^ : ⟦Γ⟧ → ⟦A⟧ → ⟦B⟧
           ⟦t⟧ : ⟦Γ⟧×⟦A⟧ → ⟦B⟧       -- lam : Tm (Γ▹A) B → Tm Γ (A ⇒ B)
⟦t$u⟧   := λ γ* → ⟦t⟧ γ (⟦u⟧ γ)
           ^ : ⟦Γ⟧ → ⟦B⟧
    ⟦t⟧ : ⟦Γ⟧ → ⟦A⟧ → ⟦B⟧
    ⟦u⟧ : ⟦Γ⟧ → ⟦A⟧
⟦β⟧ : ⟦lam t $ u⟧ = λ γ* → ⟦t⟧ (γ*,⟦u⟧ γ*) = ⟦t[id,u]⟧
⟦η⟧
lam[]
$[]

MAJD: normalizálás (volt NatBool nyelvre (változók nélkül))
MAJD: típuskikövetkeztetés (volt NatBool nyelvre (változók nélkül)) -- kétirányú
        NatBoolAST -> NatBool

---------------------------------------------------------------------------------

Véges típusokat (Fin - finite types)

Szorzat, összeg, speciális eset lesz pl. a Bool

Prod : Ty → Ty → Ty   (bináris szorzat)    Prod A B = A × B , A ⊗ B, ...
konstr: pair : Tm Γ A → Tm Γ B → Tm Γ (Prod A B)
destr:  fst  : Tm Γ (Prod A B) → Tm Γ A
        snd  : Tm Γ (Prod A B) → Tm Γ B
szam:   fst (pair a b) = a
szam:   snd (pair a b) = b
egyedi: pair (fst t) (snd t) = t           t : Tm Γ (Prod A B)
pair[] : pair t [ γ ] = pair (t [γ])
fst[]  : fst t [ γ ] = fst (t[γ])         (fst[] és snd[] következik pair[])
snd[]

Tm Γ (Prod A B) ≅ (Tm Γ A × Tm Γ B)

(X ≅ Y) = (f:X→Y)×(g:Y→X)×(x=g(f x))×
          (y=f(g y))

Mire jók? Rekord: Prod Nat Bool = record { fst : Nat , snd : Bool}

Bool × Nat × Nat × Nat = 4x-es szorzat

nulláris szorzat típus

Tm Γ (Prod3 A B C) ≅ Tm Γ A × Tm Γ B × Tm Γ C
Tm Γ (Prod2 A B)   ≅ Tm Γ A × Tm Γ B
Tm Γ (Prod1 A)     ≅ Tm Γ A
Tm Γ Prod0         ≅ Unit

----------------------------------------------------------------

Unit (void, (), {*}, ⊤, One, 𝟙, 1)
  Unit    : Ty
  trivial : Tm Γ Unit
  nincs destruktor
  Unitη   : trivial = t                   t : Tm Γ Unit
  (trivial {Γ})[γ] = trivial {Δ}

----------------------------------------------------------------

Bináris összeg:

Sum : Ty → Ty → Ty             Sum A B = A+B = A⊎B = Either A B = (C osztály, A és a B a C-nek alosztálya), Enum 4-féle lehet: Sum (Sum ((Sum Unit) Unit) Unit) Unit  =   Unit+Unit+Unit+Unit  = 1+1+1+1 = 4
konstr: inl : Tm Γ A → Tm Γ (Sum A B)
        inr : Tm Γ B → Tm Γ (Sum A B)
destr:  case : Tm Γ (Sum A B) → Tm (Γ▹A) C → Tm (Γ▹B) C → Tm Γ C
szamit: case (inl a) u v = u[id,a]
        case (inr b) u v = v[id,b]
inl[] : inl t [γ] = inl (t[γ])
inr[]
case[] : case t u v [γ] = case (t[γ]) (u[γ∘p,q]) (v[γ∘p,q])
                  (lam t[γ] = lam (t[γ∘p,q]))

Összes véges típus megvan:
Empty <- nulla elemű
Unit <- egyelemű
Sum Unit Unit <- kételemű
Sum (Sum Unit Unit) Unit <- háromelemű típus
...
  Empty : Ty
  nincs konstruktor
  absurd : Tm Γ Empty → Tm Γ A
  nincs se szamitasi, se egyedisegi szabalya
  absurd[] : absurd t [γ] = absurd (t[γ])

-----------------------------------------------------------------

Összes véges típus. Bool  := Sum Unit Unit,
                    true  := inl trivial
                    false := inr trivial
                    ite t u v := case t (u[p]) (v[p])   : Tm Γ C
                     t : Tm Γ Bool, u,v:Tm Γ C  u[p]:Tm (Γ▹Unit) C
                    ite true u v =
                    case (inl trivial) (u[p]) (v[p]) =
                    u[p][id,trivial] = u[id] = u

Maybe A := Sum A Unit   (Optional, kivétel)

------------------------------------------------------------------

standard modell:

Prod : Set → Set → Set
Prod A B := A × B
pair a b := λ γ* . (a γ* , b γ*)
            ^ : Γ → (A × B)
fst t γ* := π₁ (t γ*)
snd t γ* := π₂ (t γ*)
β,η szabályok teljesülnek

Unit := ⊤

Sum A B := A ⊎ B
inl     := "ι₁"
inr     := "ι₂"

Empty := ⊥
absurd := "exfalso"

A standard modellben több egyenlőség van, mint a szintaxisban:

lam (if v0 then true else false) ≠ lam v0   nem egyenlő a szintaxisban, nehéz ezt bebizonyítani
  de a standard modellben egyenlők

Con = Set = Ty
Sub Γ Δ = Tm Γ Δ
Tm ∙ (Prod Unit A)   =   Sub ∙ (∙ ▹ A)

--------------------------------------------------------------

Prod (Prod A B) C        (A×B)×C
Prod A (Prod B C)        A×(B×C)

Tm Γ ((A×B)×C) ≅ Tm Γ (A×(B×C))

Bool⇒Bool    <- "4 eleme van" -- mukodes szempontjabol
  lam v0                              ugyanugy mukodik, mint
  lam (if v0 then true else false)
        if true  then u else v = u
        if false then u else v = v
Bool×Bool    <- 4 eleme van     (pair true true, pair true false, ...)

akarhanyszoros aritasu rekord tipus (általános rekord típus):
Record : (n : ℕ) → (Fin n → Ty) → Ty
                   (Ty ^ n) = Ty × Ty × Ty × ... × Ty <- n-szer
mkRecord : ((i : Fin n) → Tm Γ (As i)) → Tm Γ (Record n As)

Record zero    As := Unit
Record (suc n) As := Record n As' × (As n)
                        As' := As, de elfelejti az utolsot

mkRecord-ot pair-el megadhatjuk
