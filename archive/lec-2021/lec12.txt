Tree : Ty
leaf : Tm Γ Tree
node2 : Tm Γ Tree → Tm Γ Tree → Tm Γ Tree
node3 : Tm Γ Tree → Tm Γ Tree → Tm Γ Tree → Tm Γ Tree
iteTree : {C:Ty} → Tm Γ C → Tm (Γ▹C▹C) C →
  Tm (Γ▹C▹C▹C) C →
  Tm Γ Tree → Tm Γ C
iteTree {C} l n2 n3 leaf = l
iteTree {C} l n2 n3 (node2 u v) =
  n2[id,iteTree l n2 n3 u,iteTree l n2 n3 v]
iteTree {C} l n2 n3 (node3 u v w) =
  n3[id,iteTree l n2 n3 u,
        iteTree l n2 n3 v,
        iteTree l n2 n3 w]

Nat : Ty
zero : Tm Γ Nat
suc  : Tm (Γ▹Nat) Nat
iteNat : {C} → Tm Γ C → Tm (Γ▹C) C → Tm Γ Nat → Tm Γ C
iteNat u v zero = u
iteNat u v (suc t) = v[id,iteNat u v t]


induktiv tipus
- konstruktorok
- eliminator: barmely C-re, ha C-bol felepitunk egy konstruktorok-formaju strukturat, akkor az induktiv tipusbol el tudunk menni C-be


Tree : Ty
leaf : Tm (Γ▹Bool) Tree
node : Tm (Γ▹Nat⇒Tree) Tree
C : Ty
leaf : Tm (Γ▹Bool) C
node : Tm (Γ▹Nat⇒C) C

--------------------------------------

Tree : Ty
leaf : Tm (Γ▹Bool) Tree
node : Tm (Γ▹Nat) Tree

iteTree :
  Tm (Γ▹Bool) C →
  Tm Γ (Nat⇒C) →
  Tm Γ Tree → Tm Γ C	

case : Tm Γ (A+B) → Tm (Γ▹A) C → Tm (Γ▹B) C → Tm Γ C

--------------------------

Ty : Set           TmBool : Set
Tm : Ty → Set      TmNat  : Set
Bool : Ty          TmNat⇒Nat : Set
Nat : Ty           TmBool⇒Bool⇒Nat : Set..

-------------------------------------------------------

Induktív típusokról még:

+,×,Empty,One
  <- ezek is megadhatók induktív típusokként
  A+Empty nem ≅ A     A+Empty ↔ A
  nincs η 
_×_ : Ty → Ty → Ty
_,_ : Tm Γ A → Tm Γ B → Tm Γ (A×B)
fst : Tm Γ (A×B) → Tm Γ A
snd :                   B
β₁  : fst (a ,o b)=a       \
β₂  : snd (a ,o b) = b     / (a,b)=fst(a ,o b),snd(a ,o b)
η   : fst w,snd w = w
-- fst,snd : Tm Γ (A×B) ≅ Tm Γ A × Tm Γ B : _,o_
                    w   →  fst w    snd w
             fst w , snd w  ←

                 ~ _[p]$q : Tm Γ (A⇒B) ≅ Tm (Γ▹A) B : lam

pair : Tm Γ A → Tm Γ B → Tm Γ (A×B)
       Tm (Γ▹A▹B) (A×B)
ite× : {C:Ty} → Tm (Γ▹A▹B) C → Tm Γ (A×B) → Tm Γ C
ite× t (pair u v) = t[id,u,v]

T : Ty
con : Tm Γ T → Tm Γ T

---------------------------------------------

ℕ     Tm ∙ Nat
ℕ→ℕ   Tm ∙ (Nat ⇒ Nat) ≅ ℕ  Gödel-kódolás   (Skolem paradoxon)


ℕ ≅ ℕ × ℕ                ℚ  ℤ
0   0,0   1,0  2,0
1   0,1   1,1  2,0
2   0,2   1,2  ...
3   0,3   1,3

ℕ ≠ ℕ → 𝟚 = 𝟚^ℕ = P(ℕ)
Cantor-féle átlós metszés

0  001101010101010
1  000000000000000
2  001111111111100
3  000110010011000
4  ...
5  ...
nincs benne:
   1100....

----------------------------------------------------------

Koinduktív típusok           Induktív
destruktorokkal              konstruktorokkal
végtelen                     véges
gen,anamorf:hogyan tudok     ite,rec,fold,cata: hogyan lehet szétszedni
  konstruálni egy elemét

Stream : Ty → Ty
head   : Tm Γ (Stream A) → A          Tm (Γ▹Stream A) A
tail   : Tm Γ (Stream A) → Tm Γ (Stream A)
gen    : Tm (Γ▹C) A → Tm (Γ▹C) C → Tm Γ C → Tm Γ Stream
Streamβ₁ : head (gen u v t) = u[id,t]
Streamβ₂ : tail (gen u v t) = gen u v (v[id,t])
head[], tail[], gen[]
  A : seed típusa

[0,1,2,3,4,5,6,...]

gen {Nat} v0 (suco v0) zeroo : Tm ∙ (Stream Nat)

head (tail (tail (gen v0 (suco v0) zeroo))) =
head (tail (gen v0 (suco v0) (suc zeroo))) =
head (gen v0 (suco v0) (suc (suc zeroo))) =
v0[id,suc (suc zeroo)] = suc (suc zero)

------------------------------------------------------

Mach  : Ty
des   : Tm Γ Mach → Tm Γ Bool → Tm Γ (Nat × Mach)
gen   : Tm (Γ▹C▹Bool) (Nat×C) → Tm Γ C → Tm Γ Mach
Machβ : des (gen u t) v =
  (fst(u[id,t,v]), gen u (snd((u[id,t,v]))))

gen {Nat}
  (if v0 then suco v1 else v1,if v0 then suco v1 else v1)
  zeroo

true
          0
false
          1
true
          1
false
          2
false
          3
true
          3

Bool ⇒ (Nat × (Bool ⇒ (Nat × (Bool ⇒ Nat × ...))))

Mach : Ty
put  : Tm Γ Mach → Tm Γ Nat → Tm Γ Mach
get  : Tm Γ Mach → Tm Γ Nat
gen  : Tm (Γ▹C▹Nat) C → Tm (Γ▹C) Nat → Tm Γ C → Tm Γ Mach

