{-# OPTIONS --prop --rewriting #-}

module lec06 where

open import Lib

{-

Változók, kötés
------------------------------------------------------------

- korábban: Nat, Bool
- extra feature: változó kötés
- lokális definíció (informálisan):

   let x := exp₁ in exp₂

   példa:
     let x := 0 in x + x
     let y := x + x in if z then y else x

   informálisan:
    kötés, szabad/kötött változók, láthatóság (scope), változók átnevezése

   - nyelvben van változó mint szinaktikus elem
   - minden kötött változó hivatkozik egyértelműen egy pontra a szintaxisfában
            szabad változók: nincs kötve sehol a változó, viszont szerepel
                             egy kifejezésben

   let x := 0 in x + x                    - kötött változó: x, nincs szabad változó
   let y := x + x in if z then y else x   - kötött: y, szabad: x és z


   - átnevezés: kötött változókat átnevezzük, akkor nem változik a program
     jelentése

   (let x := 0 in x + x) = (let y := 0 in y + y)

   legyen változónevek halmaza:
     Var : Set        x, y, z : Var

   absztrakt szintaxisfa szintjén a "let" specifikációja:
     let : Var → Tm → Tm → Tm

   rename:
     ∀ (v, v' : Var). (let v := exp₁ in exp₂) = (let v' := exp₁ in exp₂[v ↦ v'])

   helyettesítés jelölése:

     exp₁[x ↦ exp₂]      exp₁-ben x szabad előfordulásait exp₂-re cserélem

   kérdés:
     (let v₀ := exp₁ in exp₂) [v₁ ↦ exp₃]
        = let v₀ := exp₁[v₁ ↦ exp₃] in exp₂[v₁ ↦ exp₃]

     Nem jó: mivel lehet, hogy exp₃-ban a v₀ szabad változó
     példa:
          (let x := 0 in y + x)[y ↦ x + 100]
        = let x := 0 in x + 100 + x

   tényleges implementáció:

     (let v₀ := exp₁ in exp₂) [v₁ ↦ exp₃]
     = ha v₀ szabad exp₃-ban:
          - legyen "v₂" egy új/friss változó, ami nem szabad semmilyen expᵢ-ben
          - exp₃' := exp₃[v₀ ↦ v₂]
          - eredmény:
             let v₀ := exp₁[v₁ ↦ exp₃] in exp₂[v₁ ↦ exp₃']
          "capture-avoiding substitution"

   nevek és átnevezések matematikai elmélete:
      "nominal sets"  (érdeklődőknek)

   Alternatíva:
     - *nincsenek* változónevek
     - változók legyen "pointer"-ek, amik kötési pontra mutatnak
     - reprezentáció: természetes számok
     - De Bruijn konvenció, két verzió

   De Bruijn index:

     változó: a változó előfordulási pontjától kifelé haladva N-edik kötésre mutat
     példa:
          Nevek                       De Bruijn index
       let x := true in x + x     let true in v0 + v0

       let x := 5 in              let 5 in
       let y := 10 in             let 10 in
       x + y                      v1 + v0

       let x := 5 in              let 5 in
       let y := x + x in          let v0 + v0 in
       y + 10                     v0 + 10

   De Bruijn szint ("level"):
       Scope-ban kívülről befelé számoljuk a kötéseket

       let x := true in x + x     let true in v0 + v0

       let x := 5 in              let 5 in
       let y := 10 in             let 10 in
       x + y                      v0 + v1

       let x := 5 in              let 5 in
       let y := x + x in          let v0 + v0 in
       y + 10                     v1 + 10

  De Bruijn reprezentációra igaz:
    - nem kell foglalkozni átnevezéssel (mert nincsenek nevek)
-}

-- De Bruijn váltók formális megadása
--   első körben: nem típusozott AST-k, viszont "well-scoped",
--   nem írhatunk olyan változót, ami kimutat a scope-ból

module WellScopedSyntax where

  -- zero : ℕ         0
  -- suc  : ℕ → ℕ     1+_

  -- változók halmaza, N méretű scope-ban
  --   (N-darab lehetséges változó van N mérető scope-ban)
  -- (De Bruijn index)
  data Var : ℕ → Set where
    -- nemüres környeztben van csak v0!
    v0   : ∀ {n} → Var (suc n)

    -- ha van változó N méretű környezetben, akkor van
    -- N+1 méretű környezetben is
    vsuc : ∀ {n} → Var n → Var (suc n)

  -- példák:

  ex0 : Var 0
  ex0 = {!!}   -- nincs

  ex1 : Var 1
  ex1 = v0

  -- ex2 : Var 1
  -- ex2 = vsuc {!!}

  -- rövidítések:

  v1 : ∀ {n} → Var (suc (suc n))
  v1 = vsuc v0

  v2 : ∀ {n} → Var (suc (suc (suc n)))
  v2 = vsuc v1

  v3 : ∀ {n} → Var (suc (suc (suc (suc n))))
  v3 = vsuc v2

  v4 : ∀ {n} → Var (5 + n)
  v4 = vsuc v3

  -- Var n értékei: v0 .. v(n-1)

  ------------------------------------------------------------

  -- kifejezés N darab lehetséges szabad változóval
  data Tm : ℕ → Set where
    var : ∀ {n} → Var n → Tm n

    -- "let" kifejezés:
    def : ∀ {n} → Tm n → Tm (suc n) → Tm n

    true   : ∀ {n} → Tm n
    false  : ∀ {n} → Tm n
    ite    : ∀ {n} → Tm n → Tm n → Tm n → Tm n
    num    : ∀ {n} → ℕ → Tm n
    isZero : ∀ {n} → Tm n → Tm n
    _+Tm_  : ∀ {n} → Tm n → Tm n → Tm n

  -- példák:
  -- let x := true in x + x
  t1 : ∀ {n} → Tm n
  t1 = def true (var v0 +Tm var v0)

  -- feltételezve: x, y, z szabad változók
  -- let a := x + x + y in a + z
  -- el kell dönteni, hogy x, y, z milyen sorrendben van a környeztben
  -- legyen x, y, z
  --        v2 v1 v0
  t2 : Tm 3
  t2 = def (var v2 +Tm (var v2 +Tm var v1))
           (var v0 +Tm var v1)

  -- kérdés: milyen érdekes függvények vannak Tm-re
  -- t : Tm n

  -- true : Tm 0
  -- t : Tm 0    ez a "t" kifejezés használható legyen N-méretű környezetben

  -- "lemma": minden kifejezés ami valid N-környezetben, az valid M+N méretű
  -- környezetben is
  wk : ∀ {n} → Tm n → Tm (suc n)
  wk (var x) = var (vsuc x)      -- "shift"
  wk (def t u) = def (wk t) (wk u)
  wk true = true
  wk false = false
  wk (ite t u v) = ite (wk t) (wk u) (wk v)
  wk (num n) = num n
  wk (isZero t) = isZero (wk t)
  wk (t +Tm u) = wk t +Tm wk u


-- Jól típusozott változók
--------------------------------------------------------------------------------

data Ty : Set where
  Bool : Ty
  Nat  : Ty

-- környezet: típusok listája, megadja, hogy melyik változónak
-- mi a típusa
-- "Con" context rövidítése
data Con : Set where
  ◇   : Con             -- üres környezet
  _▷_ : Con → Ty → Con  -- jobb oldalon kiegészített környezet

infixl 3 _▷_

con1 : Con
con1 = ((◇ ▷ Bool) ▷ Bool) ▷ Bool
    -- ◇ ▷ Bool ▷ Bool ▷ Bool

con2 : Con
con2 = ◇ ▷ Bool ▷ Bool ▷ Bool ▷ Nat ▷ Nat

-- Var Γ A : Γ-ba mutató A-típusú változók típusa
data Var : Con → Ty → Set where
  v0   : ∀ {Γ A} → Var (Γ ▷ A) A
  vsuc : ∀ {Γ A B} → Var Γ A → Var (Γ ▷ B) A

-- példa:

varEx1 : Var con1 Bool
varEx1 = v0             -- legjobboldaibb

varEx1' : Var con1 Bool
varEx1' = vsuc v0       -- középső

varEx1'' : Var con1 Bool
varEx1'' = vsuc (vsuc v0)

-- Γ ▷ A ▷ B ▷ C ▷ D
--     v3  v2  v1  v0

v1 : ∀ {Γ A B} → Var (Γ ▷ A ▷ B) A
v1 = vsuc v0

v2 : ∀ {Γ A B C} → Var (Γ ▷ A ▷ B ▷ C) A
v2 = vsuc v1

v3 : ∀ {Γ A B C D} → Var (Γ ▷ A ▷ B ▷ C ▷ D) A
v3 = vsuc v2

data Tm : Con → Ty → Set where
  var : ∀ {Γ A} → Var Γ A → Tm Γ A

         -- van +1 A típusú változó a környezetben
         --                         ↓↓↓
  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▷ A) B → Tm Γ B

  true   : ∀ {Γ} → Tm Γ Bool
  false  : ∀ {Γ} → Tm Γ Bool
  ite    : ∀ {Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
  num    : ∀ {Γ} → ℕ → Tm Γ Nat
  isZero : ∀ {Γ} → Tm Γ Nat → Tm Γ Bool
  _+Tm_  : ∀ {Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat


-- példák:

-- let x := true in x + x   -- nem működik, rossz a változó típusa
t1 : Tm ◇ Nat
t1 = def true {!v0 +Tm v0!}

-- let x := 5 in
-- let y := x + x in
-- y + 10
t2 : Tm ◇ Nat
t2 = def (num 5)
    (def (var v0 +Tm var v0)
    (var v0 +Tm num 10))

-- házi:
wk : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▷ B) A
wk = {!!}

-- (miért "wk")
-- "weakening" (logikából jön)
