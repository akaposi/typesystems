{-# OPTIONS --cubical --guardedness #-}

module Nat where

open import Lib
open import Agda.Primitive
open import Cubical.Data.Nat
open import Cubical.Core.Everything hiding (Sub)
open import Cubical.Foundations.Prelude hiding (Sub)
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Isomorphism
open import Cubical.Foundations.Equiv
open import Cubical.Foundations.Transport

data Nat : Type where
  Zero : Nat
  Suc  : Nat → Nat

⟦_⟧ : ℕ → Nat
⟦ zero ⟧ = Zero
⟦ suc n ⟧ = Suc ⟦ n ⟧

m : Nat → ℕ
m Zero = 1
m (Suc x) = suc (suc (m x))

testM0 : m ⟦ 0 ⟧  ≡ 1
testM1 : m ⟦ 1 ⟧  ≡ 3
testM2 : m ⟦ 2 ⟧  ≡ 5

testM0 = refl
testM1 = refl
testM2 = refl

a : Nat → (ℕ → ℕ)
a Zero = id
a (Suc n) = suc ∘ a n

testA0 : a ⟦ 0 ⟧  ≡ λ n → n
testA1 : a ⟦ 1 ⟧  ≡ suc
testA2 : a ⟦ 2 ⟧  ≡ suc ∘ suc
testA3 : a ⟦ 3 ⟧  ≡ suc ∘ suc ∘ suc

testA0 = refl
testA1 = refl
testA2 = refl
testA3 = refl

_+'_ : ℕ → ℕ → ℕ
_+'_ = a ∘ ⟦_⟧

test1+3 : 1 +' 3 ≡ 4
test3+2 : 3 +' 2 ≡ 5

test1+3 = refl
test3+2 = refl

data DNat : ℕ → Type where
  DZero : DNat zero
  DSuc  : ∀{n} → DNat n → DNat (suc n)

D⟦_⟧ : (n : ℕ) → DNat n
D⟦ zero ⟧ = DZero
D⟦ suc n ⟧ = DSuc D⟦ n ⟧

Ass : (n o : ℕ) → {m : ℕ} → (DNat m) → (m +' n) +' o ≡ m +' (n +' o)
Ass n o DZero = refl
Ass n o (DSuc dm) = cong suc (Ass n o dm)

ass : (m n o : ℕ) → (m +' n) +' o ≡ m +' (n +' o)
ass m n o = Ass n o D⟦ m ⟧
