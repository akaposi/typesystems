{-# OPTIONS --cubical --guardedness #-}

module Def where

open import Lib hiding (id)

infixl 6 _⊚_
infixl 5 _▹_
infixl 6 _[_]
infixl 6 _[_]'
infixl 5 _,o_

data Ty   : Type where
  Nat     : Ty
  Bool    : Ty

NatNotBool : ¬ (Nat ≡ Bool)
NatNotBool h = subst T h 0
  where
  T : Ty → Type
  T Nat  = ℕ
  T Bool = ⊥

discreteTy : Discrete Ty
discreteTy Nat Nat = yes refl
discreteTy Nat Bool = no NatNotBool
discreteTy Bool Nat = no (NatNotBool ∘ sym)
discreteTy Bool Bool = yes refl

TySet : isSet Ty
TySet = Discrete→isSet discreteTy

data Con  : Type where
  ◆       : Con
  _▹_     : Con → Ty → Con

inj▹ : ∀(c₁)(c₂)(t₁)(t₂) → c₁ ▹ t₁ ≡ c₂ ▹ t₂ → (c₁ ≡ c₂) × (t₁ ≡ t₂)
inj▹ c₁ c₂ t₁ t₂ h = subst T h (refl , refl)
  where
  T : Con → Type
  T (c ▹ r) = (c₁ ≡ c) × (t₁ ≡ r)
  T ◆ = ⊥

◆Not▹ : ∀(c)(t) → ¬ (◆ ≡ c ▹ t)
◆Not▹ c t h = subst T h trivial
  where
  T : Con → Type
  T ◆ = ⊤
  T (c ▹ t) = ⊥

discreteCon : Discrete Con
discreteCon ◆ ◆ = yes refl
discreteCon ◆ (d ▹ x) = no (◆Not▹ d x)
discreteCon (c ▹ x) ◆ = no (◆Not▹ c x ∘ sym)
discreteCon (c ▹ t) (d ▹ r) with discreteCon c d
...| no p = no (p ∘ π₁ ∘ inj▹ c d t r)
...| yes p with discreteTy t r
...| yes q = yes (cong2 _▹_ p q)
...| no q = no (q ∘ π₂ ∘ inj▹ c d t r)

ConSet : isSet Con
ConSet = Discrete→isSet discreteCon

data Sub : Con → Con → Type
data Tm : Con → Ty → Type

_[_]' : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
q' : ∀{Γ A} → Tm (Γ ▹ A) A

data Sub where
  ε         : ∀{Γ} → Sub Γ ◆
  ◆η        : ∀{Γ}{σ : Sub Γ ◆} → σ ≡ ε

  _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
  
  id        : ∀{Γ} → Sub Γ Γ
  idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
  idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ
  
  p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
  _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
  ▹βSub     : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
  ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → (((p ⊚ γa) ,o (q' [ γa ]')) ≡ γa)
  SubSet    : ∀{Γ Δ} → isSet (Sub Δ Γ)

data Tm where
  q          : ∀{Γ A} → Tm (Γ ▹ A) A
  _[_]       : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
  [∘]        : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
  [id]       : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
  ▹βTm       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → (q [ γ ,o t ]) ≡ t

  true       : ∀{Γ} → Tm Γ Bool
  false      : ∀{Γ} → Tm Γ Bool
  ite        : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
  iteβtrue   : ∀{Γ A u v} → ite {Γ}{A} true u v ≡ u
  iteβfalse  : ∀{Γ A u v} → ite {Γ}{A} false u v ≡ v
  true[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
  false[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
  ite[]      : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} → (ite {Γ}{A} t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])

  num        : ∀{Γ} → ℕ → Tm Γ Nat
  isZero     : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
  _+o_       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
  isZeroβ0   : ∀{Γ} → isZero (num {Γ} 0) ≡ true
  isZeroβsuc : ∀{Γ n} → isZero (num {Γ} (1 + n)) ≡ false
  +β         : ∀{Γ m n} → num {Γ} m +o num n ≡ num (m + n)
  num[]      : ∀{Γ n Δ}{γ : Sub Δ Γ} → num n [ γ ] ≡ num n
  isZero[]   : ∀{Γ t Δ}{γ : Sub Δ Γ} → isZero t [ γ ] ≡ isZero (t [ γ ])
  +[]        : ∀{Γ u v Δ}{γ : Sub Δ Γ} → (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])
  TmSet      : ∀{Γ A} → isSet (Tm Γ A)

q' = q
_[_]' = _[_]

-- let ... in ...
def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
def t u = u [ id ,o t ]

v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
v0 = q

v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
v1 = q [ p ]

v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = q [ p ⊚ p ]

v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = q [ p ⊚ p ⊚ p ]

▹η' : ∀{Γ A} → (p ,o q) ≡ (id {Γ ▹ A})
▹η' = 
  p ,o q 
  ≡⟨ cong2 (_,o_) (sym idr) (sym [id]) ⟩ 
  p ⊚ id ,o q [ id ] 
  ≡⟨ ▹η ⟩
  id
  ∎

,⊚ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
  (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
,⊚ {γ = γ}{t = t}{δ = δ} =
  (γ ,o t) ⊚ δ
    ≡⟨ sym ▹η ⟩
  (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
    ≡⟨ cong2 (_,o_) (sym ass) [∘] ⟩
  ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
    ≡⟨ cong2 (_,o_) (cong (_⊚ δ) ▹βSub) (cong (_[ δ ]) ▹βTm) ⟩
  γ ⊚ δ ,o t [ δ ]
    ∎

^∘ : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{Θ}{δ : Sub Θ Δ}{t : Tm Θ A} →
  (γ ⊚ p ,o q) ⊚ (δ ,o t) ≡ (γ ⊚ δ ,o t)
^∘ {Γ}{Δ}{γ}{A}{Θ}{δ}{t} =
  (γ ⊚ p ,o q) ⊚ (δ ,o t)
    ≡⟨ ,⊚ ⟩
  (γ ⊚ p ⊚ (δ ,o t) ,o q [ δ ,o t ])
    ≡⟨ cong (λ x → (x ,o q [ δ ,o t ])) ass ⟩
  (γ ⊚ (p ⊚ (δ ,o t)) ,o q [ δ ,o t ])
    ≡⟨ cong (λ x → (γ ⊚ x ,o q [ δ ,o t ])) ▹βSub ⟩
  (γ ⊚ δ ,o q [ δ ,o t ])
    ≡⟨ cong (λ x → γ ⊚ δ ,o x) ▹βTm ⟩
  (γ ⊚ δ ,o t)
    ∎

pt : ∀{Γ} → def false (ite q true (isZero (ite q (num 0) (num 1)))) ≡ false {Γ}
pt =
  def false (ite q true (isZero (ite q (num 0) (num 1))))
    ≡⟨ refl ⟩
  ite q true (isZero (ite q (num 0) (num 1))) [ id ,o false ]
    ≡⟨ ite[] ⟩
  ite (q [ id ,o false ]) (true [ id ,o false ]) (isZero (ite q (num 0) (num 1)) [ id ,o false ])
    ≡⟨ cong
          (λ x → ite x (true [ id ,o false ]) (isZero (ite q (num 0) (num 1)) [ id ,o false ]))
          ▹βTm ⟩
  ite false (true [ id ,o false ]) (isZero (ite q (num 0) (num 1)) [ id ,o false ])
    ≡⟨ iteβfalse ⟩
  isZero (ite q (num 0) (num 1)) [ id ,o false ]
    ≡⟨ isZero[] ⟩
  isZero (ite q (num 0) (num 1) [ id ,o false ])
    ≡⟨ cong isZero ite[] ⟩
  isZero (ite (q [ id ,o false ]) (num 0 [ id ,o false ]) (num 1 [ id ,o false ]))
    ≡⟨ cong
          (λ x → isZero (ite x (num 0 [ id ,o false ]) (num 1 [ id ,o false ])))
          ▹βTm ⟩
  isZero (ite false (num 0 [ id ,o false ]) (num 1 [ id ,o false ]))
    ≡⟨ cong isZero iteβfalse ⟩
  isZero (num 1 [ id ,o false ])
    ≡⟨ cong isZero num[] ⟩
  isZero (num 1)
    ≡⟨ isZeroβsuc ⟩
  false
    ∎

+o-comm : ∀{Γ n m} → num n +o num m ≡ num m +o num {Γ} n
+o-comm {Γ} {n} {m} = 
  (num n +o num m) 
  ≡⟨ +β ⟩ 
  num (n + m) 
  ≡⟨ cong num (+-comm n m) ⟩ 
  num (m + n) 
  ≡⟨ sym +β ⟩ 
  (num m +o num n) 
  ∎

St⟦_⟧T : Ty → Type
St⟦ Nat ⟧T = ℕ
St⟦ Bool ⟧T = 𝟚
 
discreteSt⟦_⟧T : ∀(t) → Discrete St⟦ t ⟧T
discreteSt⟦ Nat ⟧T = discreteℕ
discreteSt⟦ Bool ⟧T = _≟_

St⟦_⟧C : Con → Type
St⟦ ◆ ⟧C  = ⊤
St⟦ c ▹ x ⟧C = St⟦ c ⟧C × St⟦ x ⟧T

discrete⊤ : Discrete ⊤
discrete⊤ trivial trivial = yes refl

discreteSt⟦_⟧C : ∀(c) → Discrete St⟦ c ⟧C
discreteSt⟦ ◆ ⟧C = discrete⊤
discreteSt⟦ c ▹ x ⟧C = discreteΣ discreteSt⟦ c ⟧C λ _ → discreteSt⟦ x ⟧T

St⟦_⟧CSet : ∀(Δ) → isSet St⟦ Δ ⟧C
St⟦ Δ ⟧CSet = Discrete→isSet discreteSt⟦ Δ ⟧C

St⟦_⟧TSet : ∀(t) → isSet St⟦ t ⟧T
St⟦ t ⟧TSet = Discrete→isSet discreteSt⟦ t ⟧T

St⟦_⟧S : ∀{Γ Δ} → Sub Γ Δ → St⟦ Γ ⟧C → St⟦ Δ ⟧C
St⟦_⟧t : ∀{Γ A} → Tm Γ A → St⟦ Γ ⟧C → St⟦ A ⟧T

St⟦_⟧S {Γ} {.◆} ε γ = trivial
St⟦_⟧S {Γ} {.◆} (◆η i) γ = trivial
St⟦_⟧S {Γ} {Δ} (f ⊚ g) γ = St⟦ f ⟧S (St⟦ g ⟧S γ)
St⟦_⟧S {Γ} {Δ} (ass {_} {_} {_} {_} {γ'} {δ} {θ} i) γ = 
  St⟦ γ' ⟧S (St⟦ δ ⟧S (St⟦ θ ⟧S γ))
St⟦_⟧S {Γ} {.Γ} id γ = γ
St⟦_⟧S {Γ} {Δ} (idl {_} {_} {δ} i) γ = St⟦ δ ⟧S γ
St⟦_⟧S {Γ} {Δ} (idr {_} {_} {δ} i) γ = St⟦ δ ⟧S γ
St⟦_⟧S {.(Δ ▹ _)} {Δ} p γ = π₁ γ
St⟦_⟧S {Γ} {.(_ ▹ _)} (s ,o x) γ = St⟦ s ⟧S γ , St⟦ x ⟧t γ
St⟦_⟧S {Γ} {Δ} (▹βSub {_} {_} {_} {δ} {_} i) γ = St⟦ δ ⟧S γ
St⟦_⟧S {Γ} {.(_ ▹ _)} (▹η {γa = γa} i) γ = St⟦ γa ⟧S γ
St⟦_⟧S {Γ} {Δ} (SubSet s s₁ x y i i₁) γ = isSet→isSet' St⟦ Δ ⟧CSet
 (λ i₁ → St⟦ x i₁ ⟧S γ)
 (λ i₁ → St⟦ y i₁ ⟧S γ)
 (λ i → St⟦ s ⟧S γ)
 (λ i → St⟦ s₁ ⟧S γ) i i₁

St⟦_⟧t {.(_ ▹ A)} {A} q γ = π₂ γ
St⟦_⟧t {Γ} {A} (t [ x ]) γ = St⟦ t ⟧t (St⟦ x ⟧S γ)
St⟦_⟧t {Γ} {A} ([∘] {t = t} {γ = γ'} {δ = δ} i) γ = St⟦ t ⟧t (St⟦ γ' ⟧S (St⟦ δ ⟧S γ))
St⟦_⟧t {Γ} {A} ([id] {t = t} i) γ = St⟦ t ⟧t γ
St⟦_⟧t {Γ} {A} (▹βTm {t = t} i) γ = St⟦ t ⟧t γ
St⟦_⟧t {Γ} {.Bool} true γ = tt
St⟦_⟧t {Γ} {.Bool} false γ = ff
St⟦_⟧t {Γ} {A} (ite t t₁ t₂) γ = if St⟦ t ⟧t γ then St⟦ t₁ ⟧t γ else St⟦ t₂ ⟧t γ
St⟦_⟧t {Γ} {A} (iteβtrue {u = u} i) γ = St⟦ u ⟧t γ
St⟦_⟧t {Γ} {A} (iteβfalse {v = v} i) γ = St⟦ v ⟧t γ
St⟦_⟧t {Γ} {.Bool} (true[] i) γ = tt
St⟦_⟧t {Γ} {.Bool} (false[] i) γ = ff
St⟦_⟧t {Γ} {A} (ite[] {_} {_} {t} {u} {v} {Δ} {δ} i) γ = if St⟦ t ⟧t (St⟦ δ ⟧S γ) then St⟦ u ⟧t (St⟦ δ ⟧S γ) else St⟦ v ⟧t (St⟦ δ ⟧S γ)
St⟦_⟧t {Γ} {.Nat} (num x) γ = x
St⟦_⟧t {Γ} {.Bool} (isZero t) γ = isZeroℕ (St⟦ t ⟧t γ)
St⟦_⟧t {Γ} {.Nat} (t +o t₁) γ = St⟦ t ⟧t γ + St⟦ t₁ ⟧t γ
St⟦_⟧t {Γ} {.Bool} (isZeroβ0 i) γ = tt
St⟦_⟧t {Γ} {.Bool} (isZeroβsuc i) γ = ff
St⟦_⟧t {Γ} {.Nat} (+β {m = m} {n = n} i) γ = m + n
St⟦_⟧t {Γ} {.Nat} (num[] {n = n} i) γ = n
St⟦_⟧t {Γ} {.Bool} (isZero[] {t = t} {γ = γ'} i) γ = isZeroℕ (St⟦ t ⟧t (St⟦ γ' ⟧S γ))
St⟦_⟧t {Γ} {.Nat} (+[] {u = u} {v = v} {γ = γ'} i) γ = St⟦ u ⟧t (St⟦ γ' ⟧S γ) + St⟦ v ⟧t (St⟦ γ' ⟧S γ)
St⟦_⟧t {Γ} {A} (TmSet t t₁ x y i i₁) γ = isSet→isSet' St⟦ A ⟧TSet
 (λ i₁ → St⟦ x i₁ ⟧t γ)
 (λ i₁ → St⟦ y i₁ ⟧t γ)
 (λ i → St⟦ t ⟧t γ)
 (λ i → St⟦ t₁ ⟧t γ) i i₁

-- norm : ⌜ norm tm ⌝ ≡ tm

-- a predicate on closed terms for each type
_ᴾT : (A : Ty) → Tm ◆ A → Type
(Nat ᴾT) t = Σ ℕ λ n → t ≡ num n
(Bool ᴾT) t = (t ≡ true) ⊎ (t ≡ false)

-- a predicate on closed substitutions (iteration of the predicate for each type in the context)
_ᴾC : (Γ : Con) → Sub ◆ Γ → Type
(◆ ᴾC) γ = ⊤
((Γ ▹ A) ᴾC) γ = (Γ ᴾC) (p ⊚ γ) × (A ᴾT) (q [ γ ])

-- fundamental lemmas
fls : ∀{Γ Δ} → (γ : Sub Δ Γ) → ∀{δ₀} → (Δ ᴾC) δ₀ → (Γ ᴾC) (γ ⊚ δ₀)
flt : ∀{Γ A} → (a : Tm Γ A) → ∀{γ₀} → (Γ ᴾC) γ₀ → (A ᴾT) (a [ γ₀ ])
fls ε δ₀ = _
fls (◆η i) δ₀ = _
fls {Γ} (γ ⊚ δ) {θ₀} θ₀ᴾ = transport (cong (Γ ᴾC) (sym (ass {γ = γ}{δ = δ}{θ = θ₀}))) (fls γ (fls δ θ₀ᴾ))
fls (ass i) δ₀ = {!!}
fls (id {Γ}) γ₀ᴾ = transport (cong (Γ ᴾC) (sym idl)) γ₀ᴾ
fls (idl {Γ}{Δ}{γ} i) {δ₀} δ₀ᴾ = {!!}
{-
hcomp
  (λ j → λ { (i = i1) → sym (substComposite (Γ ᴾC) (sym idl) (sym ass) (fls γ δ₀ᴾ)) ∙ cong (λ e → transport (λ j → (Γ ᴾC) (e j)) (fls γ δ₀ᴾ)) (λ j → SubSet (((λ i₁ → idl (~ i₁)) ∙ (λ i₁ → ass (~ i₁))) j) (idl (~ j) ⊚ δ₀)) ; (i = i0) → fls γ δ₀ᴾ })
  (transport-filler (λ i → (Γ ᴾC) (idl (~ i) ⊚ δ₀)) (fls γ δ₀ᴾ) (~ i))


subst (Γ ᴾC) (λ i₁ → ass (~ i₁)) (subst (Γ ᴾC) (λ i₁ → idl (~ i₁)) (fls γ δ₀ᴾ))
                                                                                   sym (substComposite (Γ ᴾC) (sym idl) (sym ass) (fls γ δ₀ᴾ))
subst (Γ ᴾC) ((λ i₁ → idl (~ i₁)) ∙ (λ i₁ → ass (~ i₁))) (fls γ δ₀ᴾ)
                                                                                   =
transport (λ j → (Γ ᴾC) (((λ i₁ → idl (~ i₁)) ∙ (λ i₁ → ass (~ i₁))) j)) (fls γ δ₀ᴾ)
                                                                                   cong (λ e → transport (λ j → (Γ ᴾC) (e j)) (fls γ δ₀ᴾ))
                                                                                        (λ j → SubSet (((λ i₁ → idl (~ i₁)) ∙ (λ i₁ → ass (~ i₁))) j) (idl (~ j) ⊚ δ₀))
transport (λ j → (Γ ᴾC) (idl (~ j) ⊚ δ₀)) (fls γ δ₀ᴾ)
                                                                                   transport-filler (λ i → (Γ ᴾC) (idl (~ i) ⊚ δ₀)) (fls γ δ₀ᴾ) (~ i)
fls γ δ₀ᴾ
-}
fls (idr {Γ}{Δ}{γ} i) {δ₀} δ₀ᴾ = {!!}
fls p {γa₀} γa₀ᴾ = π₁ γa₀ᴾ
fls (γ ,o t) δ₀ᴾ = (transport (cong (_ ᴾC) (sym ▹βSub ∙ cong (p ⊚_) (sym ,⊚))) (fls γ δ₀ᴾ)) , transport {!!} (flt t δ₀ᴾ)
fls (▹βSub i) δ₀ = {!!}
fls (▹η i) δ₀ = {!!}
fls (SubSet γ γ₁ x y i i₁) δ₀ = {!!}

flt q γa₀ᴾ = π₂ γa₀ᴾ
flt (t [ γ ]) δ₀ᴾ = {!!}
flt ([∘] i) γ₀ = {!!}
flt ([id] i) γ₀ = {!!}
flt (▹βTm i) γ₀ = {!!}
flt true _ = ι₁ true[]
flt false _ = {!!}
flt (ite t t₁ t₂) {γ₀} γ₀ᴾ with flt t γ₀ᴾ
... | ι₁ e = transport
  (cong (_ ᴾT) (sym iteβtrue) ∙ cong (λ z → (_ ᴾT) (ite z (t₁ [ γ₀ ]) (t₂ [ γ₀ ]))) (sym e) ∙ cong (_ ᴾT) (sym ite[]))
  (flt t₁ γ₀ᴾ)
... | ι₂ e = {!!}
flt (iteβtrue i) γ₀ = {!!}
flt (iteβfalse i) γ₀ = {!!}
flt (true[] i) γ₀ = {!!}
flt (false[] i) γ₀ = {!!}
flt (ite[] i) γ₀ = {!!}
flt (num n) _ = n , num[]
flt (isZero t) γ₀ᴾ with flt t γ₀ᴾ
... | zero , e = ι₁ (isZero[] ∙ cong isZero e ∙ isZeroβ0)
... | suc n , e = ι₂ (isZero[] ∙ cong isZero e ∙ isZeroβsuc)
flt (t +o t') γ₀ᴾ with flt t γ₀ᴾ | flt t' γ₀ᴾ
... | (m , e) | (m' , e') = m + m' , +[] ∙ cong₂ _+o_ e e' ∙ +β
flt (isZeroβ0 i) γ₀ = {!!}
flt (isZeroβsuc i) γ₀ = {!!}
flt (+β i) γ₀ = {!!}
flt (num[] i) γ₀ = {!!}
flt (isZero[] i) γ₀ = {!!}
flt (+[] i) γ₀ = {!!}
flt (TmSet t t₁ x y i i₁) γ₀ = {!!}

canonBool : (t : Tm ◆ Bool) → (t ≡ true) ⊎ (t ≡ false)
canonBool t = transport (cong (λ t → (t ≡ true) ⊎ (t ≡ false)) [id]) (flt t {id} _)

-- ide nehany peldat, amikor tenylegesen kiszamolom termekrol, hogy ok mik

