{-# OPTIONS --cubical --safe #-}
module Tree.Properties where

open import Cubical.Core.Everything
open import Cubical.Foundations.GroupoidLaws
open import Cubical.Foundations.HLevels
open import Cubical.Foundations.Prelude
open import Cubical.Data.Empty as ⊥
open import Cubical.Data.Nat
open import Cubical.Data.Sigma
open import Cubical.Data.Unit
open import Cubical.Relation.Nullary

open import Tree.Base

module TreePath {ℓ} {A : Type ℓ} where

  Cover : Tree A → Tree A → Type ℓ
  Cover (leaf l) (leaf l₁) = l ≡ l₁
  Cover (leaf l) (node s s₁) = Lift ⊥
  Cover (node t t₁) (leaf l) = Lift ⊥
  Cover (node t l) (node s r) = Cover t s × Cover l r
  
  reflCode : ∀ t → Cover t t
  reflCode (leaf l) = refl
  reflCode (node t s) = reflCode t , reflCode s
  
  encode : ∀ t s → (p : t ≡ s) → Cover t s
  encode t _ = J (λ s _ → Cover t s) (reflCode t)

  encodeRefl : ∀ t → encode t t refl ≡ reflCode t
  encodeRefl t = JRefl (λ s _ → Cover t s) (reflCode t)
  
  decode : ∀ t s → Cover t s → t ≡ s
  decode (leaf l) (leaf l₁) c = cong leaf c
  decode (node t t₁) (node s s₁) (p , c) = cong₂ node (decode t s p) (decode t₁ s₁ c)
  
  decodeRefl : ∀ t → decode t t (reflCode t) ≡ refl
  decodeRefl (leaf l) = refl
  decodeRefl (node t s) = λ i j → node (decodeRefl t i j) (decodeRefl s i j)
  
  decodeEncode : ∀ t s → (p : t ≡ s) → decode t s (encode t s p) ≡ p
  decodeEncode t _ =
    J (λ s p → decode t s (encode t s p) ≡ p)
      (cong (decode t t) (encodeRefl t) ∙ decodeRefl t)
  
  isOfHLevelCover : (n : HLevel) (p : isOfHLevel (suc (suc n)) A)
    (t s : Tree A) → isOfHLevel (suc n) (Cover t s)
  isOfHLevelCover n p (leaf l) (leaf l₁) = p l l₁
  isOfHLevelCover n p (leaf l) (node s s₁) =
    isOfHLevelLift (suc n) (isProp→isOfHLevelSuc n isProp⊥)
  isOfHLevelCover n p (node t t₁) (leaf l) = 
    isOfHLevelLift (suc n) (isProp→isOfHLevelSuc n isProp⊥)
  isOfHLevelCover n p (node t t₁) (node s s₁) = 
    isOfHLevelΣ (suc n) (isOfHLevelCover n p t s) λ _ → isOfHLevelCover n p t₁ s₁

isOfHLevelTree : ∀ {ℓ} (n : HLevel) {A : Type ℓ}
  → isOfHLevel (suc (suc n)) A → isOfHLevel (suc (suc n)) (Tree A)
isOfHLevelTree n ofLevel xs ys =
  isOfHLevelRetract (suc n)
    (TreePath.encode xs ys)
    (TreePath.decode xs ys)
    (TreePath.decodeEncode xs ys)
    (TreePath.isOfHLevelCover n ofLevel xs ys)
  
isSetTree : ∀{ℓ}{A : Type ℓ} → isSet A → isSet (Tree A)
isSetTree = isOfHLevelTree 0

leafNotnode : ∀{ℓ}{A : Type ℓ}(x : A)(y z : Tree A) → ¬ (leaf x ≡ node y z)
leafNotnode {ℓ} {A} x y z h = lower (subst T h tt*)
  where
  T : Tree A → Type ℓ
  T (leaf _) = Lift Unit
  T (node _ _) = Lift ⊥

leaf-inj : ∀{ℓ}{A : Type ℓ}(x y : A) → leaf x ≡ leaf y → x ≡ y
leaf-inj {ℓ} {A} x y h = subst T h refl
  where
  T : Tree A → Type ℓ
  T (leaf a) = x ≡ a
  {-# CATCHALL #-}
  T _        = Lift ⊥

node-inj : ∀{ℓ}{A : Type ℓ}(a b c d : Tree A) → node a c ≡ node b d → (a ≡ b) × (c ≡ d)
node-inj {ℓ} {A} a b c d h = subst T h (refl , refl)
  where
  T : Tree A → Type ℓ
  T (node x y) = (a ≡ x) × (c ≡ y)
  {-# CATCHALL #-}
  T _        = Lift ⊥

discreteTree : ∀{ℓ}{A : Type ℓ} → Discrete A → Discrete (Tree A)
discreteTree a (leaf l) (leaf l₁) with a l l₁ 
...| no pr  = no λ x → pr (leaf-inj l l₁ x)
...| yes pr = yes (cong leaf pr)
discreteTree a (leaf l) (node y y₁) = no (leafNotnode l y y₁)
discreteTree a (node x x₁) (leaf l) = no λ y → leafNotnode l x x₁ (sym y)
discreteTree a (node x x₁) (node y y₁) with discreteTree a x y | discreteTree a x₁ y₁
...| no pr  | _      = no λ nr → pr (fst (node-inj x y x₁ y₁ nr))
...| yes pr | no qr  = no λ nr → qr (snd (node-inj x y x₁ y₁ nr))
...| yes pr | yes qr = yes (cong₂ node pr qr)