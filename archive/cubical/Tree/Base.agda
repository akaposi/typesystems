{-# OPTIONS --cubical --safe #-}
module Tree.Base where

open import Cubical.Core.Everything

private
  variable
    ℓ ℓA ℓB : Level
    A : Type ℓA
    B : Type ℓB

data Tree {ℓ} (A : Type ℓ) : Type ℓ where
  leaf : (l : A) → Tree A
  node : (ll : Tree A) → (rr : Tree A) → Tree A

caseTree : (l n : B) → Tree A → B
caseTree l n (leaf x) = l
caseTree l n (node ll rr) = n 

map-Tree : (A → B) → Tree A → Tree B
map-Tree f (leaf l) = leaf (f l)
map-Tree f (node ll rr) = node (map-Tree f ll) (map-Tree f rr)

rec : (A → B) → (B → B → B) → Tree A → B
rec f g (leaf l) = f l
rec f g (node ll rr) = g (rec f g ll) (rec f g rr)

elim : ∀{A : Type ℓA}(B : Tree A → Type ℓB)(f : (a : A) → B (leaf a))(g : (ll rr : Tree A) → B (node ll rr)) → (t : Tree A) → B t
elim B f g (leaf l) = f l
elim B f g (node ll rr) = g ll rr