{-# OPTIONS --cubical --guardedness #-}

module NatBoolAST where

open import Lib

infixl 6 _+o_
  
data Tm   : Type where
  true    : Tm
  false   : Tm
  ite     : Tm → Tm → Tm → Tm
  num     : ℕ → Tm
  isZero  : Tm → Tm
  _+o_    : Tm → Tm → Tm

height : Tm → ℕ
height true       = 0
height false      = 0
height (ite t t' t'') = 1 + max (height t) (max (height t') (height t''))
height (num n)    = 0
height (isZero t) = 1 + height t
height (t +o t')  = 1 + max (height t) (height t')

evalHeight : height (ite (isZero (num 0 +o num 1)) false (isZero (num 0))) ≝ 3
evalHeight =
  height  (ite    (isZero (num 0 +o num 1))                 false               (isZero (num 0)         )) ≡≡
  1 + max (height (isZero (num 0 +o num 1)))                (max (height false) (height (isZero (num 0)))) ≡≡
  1 + max (1 + height (num 0 +o num 1))                     (max (height false) (height (isZero (num 0)))) ≡≡
  1 + max (1 + (1 + max (height (num 0)) (height (num 1)))) (max (height false) (height (isZero (num 0)))) ≡≡
  1 + max (1 + (1 + max 0                0               )) (max (height false) (height (isZero (num 0)))) ≡≡
  1 + max (1 + (1 + 0                                    )) (max (height false) (height (isZero (num 0)))) ≡≡
  1 + max 2                                                 (max (height false) (height (isZero (num 0)))) ≡≡
  1 + max 2                                                 (max 0              (height (isZero (num 0)))) ≡≡
  1 + max 2                                                 (max 0              (1 +    height  (num 0) )) ≡≡
  1 + max 2                                                 (max 0              (1 +    0               )) ≡≡
  1 + max 2                                                 (max 0              1                        ) ≡≡
  1 + max 2                                                 1                                              ≡≡
  1 + 2                                                                                                    ≡≡
  3                                                                                                        ∎∎

tm : Tm → 𝟚
tm true       = tt
tm false      = ff
tm (ite t t' t'') = tm t or tm t' or tm t''
tm (num n)    = ff
tm (isZero t) = tm t
tm (t +o t')  = tm t or tm t'

evalT : tm (ite (isZero (num 0 +o num 1)) false (isZero (num 0))) ≝ ff
evalT = 
  tm (ite (isZero (num 0 +o num 1))       false       (isZero (num 0))) ≡≡
  tm (     isZero (num 0 +o num 1)) or tm false or tm (isZero (num 0) ) ≡≡
  tm  (num 0   +o    num 1 )        or ff       or tm (num 0)           ≡≡ 
  (tm (num 0) or tm (num 1))        or ff       or ff                   ≡≡ 
  (ff         or ff               ) or ff       or ff ≡≡
  ff ∎∎

evalT' : tm (ite true (num 0) false) ≝ tt
evalT' =
  tm (ite true    (num 0)       false)   ≡≡ 
  tm true      or tm (num 0) or tm false ≡≡ 
  tt           or ff         or ff       ≡≡ 
  tt ∎∎

-- Proving that true doesn't equal to false in the syntax.
tf : Tm → 𝟚
tf true  = tt
tf false = ff
{-# CATCHALL #-}
tf _     = ff

true≠false : ¬ (true ≡ false)
true≠false true≡false = true≢false (cong tf true≡false)

-- Semantics with true = false
triv : Tm → ⊤
triv tm = trivial

trivtrue=trivfalse : triv true ≡ triv false
trivtrue=trivfalse = refl

-- Dependent terms instead of dependent model
data DTm : Tm → Type where
  dtrue   : DTm true
  dfalse  : DTm false
  dite    : ∀{t u v} → DTm t → DTm u → DTm v → DTm (ite t u v)
  dnum    : (n : ℕ) → DTm (num n)
  disZero : ∀{t} → DTm t → DTm (isZero t)
  _d+o_   : ∀{t u} → DTm t → DTm u → DTm (t +o u)

D⟦_⟧ : (t : Tm) → DTm t
D⟦ true ⟧ = dtrue
D⟦ false ⟧ = dfalse
D⟦ ite t t₁ t₂ ⟧ = dite D⟦ t ⟧ D⟦ t₁ ⟧ D⟦ t₂ ⟧
D⟦ num x ⟧ = dnum x
D⟦ isZero t ⟧ = disZero D⟦ t ⟧
D⟦ t +o t₁ ⟧ = D⟦ t ⟧ d+o D⟦ t₁ ⟧

countLeaves : Tm → ℕ
countLeaves true       = 1
countLeaves false      = 1
countLeaves (ite t t' t'') = countLeaves t + countLeaves t' + countLeaves t''
countLeaves (num n)    = 1
countLeaves (isZero t) = countLeaves t
countLeaves (t +o t')  = countLeaves t + countLeaves t'

countLeaves' : ∀{t} → DTm t → ℕ
countLeaves' dtrue = 0
countLeaves' dfalse = 0
countLeaves' (dite t t' t'') = 2 + countLeaves' t + countLeaves' t' + countLeaves' t''
countLeaves' (dnum n) = 0
countLeaves' (disZero t) = countLeaves' t
countLeaves' (t d+o t') = 1 + countLeaves' t + countLeaves' t'

leaves : ∀(t) → countLeaves t ≡ countLeaves' (D⟦ t ⟧) + 1
leaves true = refl
leaves false = refl
leaves (ite t t₁ t₂) =
  countLeaves t + countLeaves t₁ + countLeaves t₂
  ≡⟨ cong (λ x → x + countLeaves t₁ + countLeaves t₂) (leaves t) ⟩
  countLeaves' D⟦ t ⟧ + 1 + countLeaves t₁ + countLeaves t₂
  ≡⟨ cong (λ x → countLeaves' D⟦ t ⟧ + 1 + x + countLeaves t₂) (leaves t₁) ⟩
  countLeaves' D⟦ t ⟧ + 1 + (countLeaves' D⟦ t₁ ⟧ + 1) + countLeaves t₂
  ≡⟨ cong (λ x → countLeaves' D⟦ t ⟧ + 1 + (countLeaves' D⟦ t₁ ⟧ + 1) + x) (leaves t₂) ⟩
  countLeaves' D⟦ t ⟧ + 1 + (countLeaves' D⟦ t₁ ⟧ + 1) + (countLeaves' D⟦ t₂ ⟧ + 1)
  ≡⟨ cong (λ x → x + (countLeaves' D⟦ t₁ ⟧ + 1) + (countLeaves' D⟦ t₂ ⟧ + 1)) (+-comm (countLeaves' D⟦ t ⟧) 1) ⟩
  suc (countLeaves' D⟦ t ⟧ + (countLeaves' D⟦ t₁ ⟧ + 1) + (countLeaves' D⟦ t₂ ⟧ + 1))
  ≡⟨ cong (λ x → suc (countLeaves' D⟦ t ⟧ + x + (countLeaves' D⟦ t₂ ⟧ + 1))) (+-comm (countLeaves' D⟦ t₁ ⟧) 1) ⟩
  suc (countLeaves' D⟦ t ⟧ + suc (countLeaves' D⟦ t₁ ⟧) + (countLeaves' D⟦ t₂ ⟧ + 1))
  ≡⟨ cong (λ x → suc (x + (countLeaves' D⟦ t₂ ⟧ + 1))) (+-suc (countLeaves' D⟦ t ⟧) (countLeaves' D⟦ t₁ ⟧)) ⟩
  suc (suc (countLeaves' D⟦ t ⟧ + countLeaves' D⟦ t₁ ⟧ + (countLeaves' D⟦ t₂ ⟧ + 1)))
  ≡⟨ cong (λ x → suc (suc x)) (+-assoc (countLeaves' D⟦ t ⟧ + countLeaves' D⟦ t₁ ⟧) (countLeaves' D⟦ t₂ ⟧) 1) ⟩
  suc (suc (countLeaves' D⟦ t ⟧ + countLeaves' D⟦ t₁ ⟧ + countLeaves' D⟦ t₂ ⟧ + 1)) ∎
leaves (num x) = refl
leaves (isZero t) = leaves t
leaves (t +o t₁) = 
  countLeaves t + countLeaves t₁
  ≡⟨ cong (_+ countLeaves t₁) (leaves t) ⟩
  countLeaves' D⟦ t ⟧ + 1 + countLeaves t₁
  ≡⟨ cong ((countLeaves' D⟦ t ⟧ + 1) +_ ) (leaves t₁) ⟩
  countLeaves' D⟦ t ⟧ + 1 + (countLeaves' D⟦ t₁ ⟧ + 1)
  ≡⟨ cong (_+ (countLeaves' D⟦ t₁ ⟧ + 1)) (+-comm (countLeaves' D⟦ t ⟧) 1) ⟩
  suc (countLeaves' D⟦ t ⟧ + (countLeaves' D⟦ t₁ ⟧ + 1))
  ≡⟨ cong suc (+-assoc (countLeaves' D⟦ t ⟧) (countLeaves' D⟦ t₁ ⟧) 1) ⟩
  suc (countLeaves' D⟦ t ⟧ + countLeaves' D⟦ t₁ ⟧ + 1) ∎ 