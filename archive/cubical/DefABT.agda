{-# OPTIONS --cubical --guardedness #-}
module DefABT where

open import Lib

data Var  : ℕ → Type where
  vz      : ∀{n} → Var (suc n)
  vs      : ∀{n} → Var n → Var (suc n)

data Tm   : ℕ → Type where
  var     : ∀{n} → Var n → Tm n
  def     : ∀{n} → Tm n → Tm (suc n) → Tm n
  
  true    : ∀ {n} → Tm n
  false   : ∀ {n} → Tm n
  ite     : ∀ {n} → Tm n → Tm n → Tm n → Tm n
  num     : ∀ {n} → ℕ → Tm n
  isZero  : ∀ {n} → Tm n → Tm n
  _+o_    : ∀ {n} → Tm n → Tm n → Tm n

v0 : {n : ℕ} → Tm (1 + n)
v0 = var vz

v1 : {n : ℕ} → Tm (2 + n)
v1 = var (vs vz)

v2 : {n : ℕ} → Tm (3 + n)
v2 = var (vs (vs vz))

v3 : {n : ℕ} → Tm (4 + n)
v3 = var (vs (vs (vs vz)))

v : (k : ℕ) → {n : ℕ} → Tm (suc k + n)
v k {n} = var (h k)
  where
    h : (m : ℕ) → Var (suc m + n)
    h 0 = vz
    h (suc m) = vs (h m)

t : Tm 0
t = def (num 1 +o num 2) ((v0 +o v0) +o (def (num 3 +o num 4) (v1 +o v0)))

t' : Tm 0
t' = (def (num 1 +o num 2) (v0 +o v0)) +o (def (num 3 +o num 4) (v0 +o v0))
