{-# OPTIONS --cubical --guardedness #-}

module STT where

open import Lib hiding (id)

infixl 6 _⊚_
infixl 6 _[_]
infixl 6 _[_]'
infixl 5 _▹_
infixl 5 _,o_
infixr 5 _⇒_
infixl 5 _$_
infixl 7 _+o_

data Ty   : Type where
  Nat     : Ty
  Bool    : Ty
  _⇒_     : (a : Ty) → (b : Ty) → Ty

inj⇒ : ∀(p)(q)(r)(s) → p ⇒ q ≡ r ⇒ s → (p ≡ r) × (q ≡ s)
inj⇒ p q r s h = subst T h (refl , refl)
  where
  T : Ty → Type
  T (a ⇒ b) = (p ≡ a) × (q ≡ b)
  {-# CATCHALL #-}
  T _ = ⊥

NatNotBool : ¬ (Nat ≡ Bool)
NatNotBool h = subst T h 0
  where
  T : Ty → Type
  T Nat  = ℕ
  {-# CATCHALL #-}
  T _ = ⊥

NatNot⇒ : ∀(p)(q) → ¬ (Nat ≡ p ⇒ q)
NatNot⇒ p q h = subst T h 0
  where
  T : Ty → Type
  T Nat = ℕ
  {-# CATCHALL #-}
  T _ = ⊥

BoolNot⇒ : ∀(p)(q) → ¬ (Bool ≡ p ⇒ q)
BoolNot⇒ p q h = subst T h tt
  where
  T : Ty → Type
  T Bool = 𝟚
  {-# CATCHALL #-}
  T _ = ⊥

discreteTy : Discrete Ty
discreteTy Nat Nat = yes refl
discreteTy Nat Bool = no NatNotBool
discreteTy Nat (a ⇒ b) = no (NatNot⇒ a b)
discreteTy Bool Nat = no (NatNotBool ∘ sym)
discreteTy Bool Bool = yes refl
discreteTy Bool (a ⇒ b) = no (BoolNot⇒ a b)
discreteTy (a ⇒ b) Nat = no (NatNot⇒ a b ∘ sym)
discreteTy (a ⇒ b) Bool = no (BoolNot⇒ a b ∘ sym)
discreteTy (a ⇒ b) (c ⇒ d) with discreteTy a c
...| no p = no λ z → p (π₁ (inj⇒ a b c d z))
...| yes p with discreteTy b d
...| no q = no λ z → q (π₂ (inj⇒ a b c d z))
...| yes q = yes (cong2 _⇒_ p q)

TySet : isSet Ty
TySet = Discrete→isSet discreteTy

data Con  : Type where
  ◆       : Con
  _▹_     : (Γ : Con) → (t : Ty) → Con

inj▹ : ∀(c₁)(c₂)(t₁)(t₂) → c₁ ▹ t₁ ≡ c₂ ▹ t₂ → (c₁ ≡ c₂) × (t₁ ≡ t₂)
inj▹ c₁ c₂ t₁ t₂ h = subst T h (refl , refl)
  where
  T : Con → Type
  T (c ▹ r) = (c₁ ≡ c) × (t₁ ≡ r)
  T ◆ = ⊥

◆Not▹ : ∀(c)(t) → ¬ (◆ ≡ c ▹ t)
◆Not▹ c t h = subst T h trivial
  where
  T : Con → Type
  T ◆ = ⊤
  T (c ▹ t) = ⊥

discreteCon : Discrete Con
discreteCon ◆ ◆ = yes refl
discreteCon ◆ (d ▹ x) = no (◆Not▹ d x)
discreteCon (c ▹ x) ◆ = no (◆Not▹ c x ∘ sym)
discreteCon (c ▹ t) (d ▹ r) with discreteCon c d
...| no p = no (p ∘ π₁ ∘ inj▹ c d t r)
...| yes p with discreteTy t r
...| yes q = yes (cong2 _▹_ p q)
...| no q = no (q ∘ π₂ ∘ inj▹ c d t r)

ConSet : isSet Con
ConSet = Discrete→isSet discreteCon

data Sub : Con → Con → Type
data Tm : Con → Ty → Type

_[_]' : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
q' : ∀{Γ A} → Tm (Γ ▹ A) A

data Sub where
  ε         : ∀{Γ} → Sub Γ ◆
  ◆η        : ∀{Γ}{σ : Sub Γ ◆} → σ ≡ ε

  _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
  
  id        : ∀{Γ} → Sub Γ Γ
  idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
  idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ
  
  p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
  _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
  ▹βSub     : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
  ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → (((p ⊚ γa) ,o (q' [ γa ]')) ≡ γa)
  SubSet    : ∀{Γ Δ} → isSet (Sub Δ Γ)

data Tm where
  q          : ∀{Γ A} → Tm (Γ ▹ A) A
  _[_]       : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
  [∘]        : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
  [id]       : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
  ▹βTm       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → (q [ γ ,o t ]) ≡ t

  true       : ∀{Γ} → Tm Γ Bool
  false      : ∀{Γ} → Tm Γ Bool
  ite        : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
  iteβtrue   : ∀{Γ A u v} → ite {Γ}{A} true u v ≡ u
  iteβfalse  : ∀{Γ A u v} → ite {Γ}{A} false u v ≡ v
  true[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
  false[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
  ite[]      : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} → (ite {Γ}{A} t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])

  num        : ∀{Γ} → ℕ → Tm Γ Nat
  isZero     : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
  _+o_       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
  isZeroβ0   : ∀{Γ} → isZero (num {Γ} 0) ≡ true
  isZeroβsuc : ∀{Γ n} → isZero (num {Γ} (1 + n)) ≡ false
  +β         : ∀{Γ m n} → num {Γ} m +o num n ≡ num (m + n)
  num[]      : ∀{Γ n Δ}{γ : Sub Δ Γ} → num n [ γ ] ≡ num n
  isZero[]   : ∀{Γ t Δ}{γ : Sub Δ Γ} → isZero t [ γ ] ≡ isZero (t [ γ ])
  +[]        : ∀{Γ u v Δ}{γ : Sub Δ Γ} → (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])
  lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  _$_        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
  ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} → (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
  $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]
  TmSet      : ∀{Γ A} → isSet (Tm Γ A)

q' = q
_[_]' = _[_]

def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
def t u = u [ id ,o t ]

v0 : ∀{Γ A}        → Tm (Γ ▹ A) A
v0 = q

v1 : ∀{Γ A B}      → Tm (Γ ▹ A ▹ B) A
v1 = q [ p ]

v2 : ∀{Γ A B C}    → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = q [ p ⊚ p ]

v3 : ∀{Γ A B C D}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = q [ p ⊚ p ⊚ p ]

▹η' : ∀{Γ A} → p ,o q ≡ id {Γ ▹ A}
▹η' {Γ} {A} =
  p ,o q
    ≡⟨ sym (cong2 _,o_ idr [id]) ⟩
  p ⊚ id ,o q [ id ]
    ≡⟨ ▹η ⟩
  id
    ∎

,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
  (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
  (γ ,o t) ⊚ δ
    ≡⟨ sym ▹η ⟩
  (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
    ≡⟨ cong2 _,o_ (sym ass) [∘] ⟩
  ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
    ≡⟨ cong2 _,o_ (cong (_⊚ δ) ▹βSub) (cong (_[ δ ]) ▹βTm) ⟩
  γ ⊚ δ ,o t [ δ ]
    ∎

^∘ : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{Θ}{δ : Sub Θ Δ}{t : Tm Θ A} →
  (γ ⊚ p ,o q) ⊚ (δ ,o t) ≡ (γ ⊚ δ ,o t)
^∘ {Γ}{Δ}{γ}{A}{Θ}{δ}{t} =
  (γ ⊚ p ,o q) ⊚ (δ ,o t)
    ≡⟨ ,∘ ⟩
  (γ ⊚ p ⊚ (δ ,o t) ,o q [ δ ,o t ])
    ≡⟨ cong (λ x → (x ,o q [ δ ,o t ])) ass ⟩
  (γ ⊚ (p ⊚ (δ ,o t)) ,o q [ δ ,o t ])
    ≡⟨ cong (λ x → (γ ⊚ x ,o q [ δ ,o t ])) ▹βSub ⟩
  (γ ⊚ δ ,o q [ δ ,o t ])
    ≡⟨ cong (λ x → γ ⊚ δ ,o x) ▹βTm ⟩
  (γ ⊚ δ ,o t)
    ∎

  module Examples where

  add2 : Tm ◆ (Nat ⇒ Nat)
  add2 = lam (v0 +o num 2) -- λx.x+2

  isZero' : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
  isZero' = lam (isZero v0) -- λx.isZero x

  isZero'β0 : ∀{Γ} → isZero' {Γ} $ num 0 ≡ true
  isZero'β0 =
    isZero' $ num 0
      ≡⟨ refl ⟩
    lam (isZero v0) $ num 0
      ≡⟨ ⇒β ⟩
    isZero v0 [ id ,o num 0 ]
      ≡⟨ isZero[] ⟩
    isZero (v0 [ id ,o num 0 ])
      ≡⟨ cong isZero ▹βTm ⟩
    isZero (num 0)
      ≡⟨ isZeroβ0 ⟩
    true
      ∎
  
  constTrue : {A : Ty} → Tm ◆ (A ⇒ Bool)
  constTrue = lam true -- λx.true

  constIsConst : ∀{Γ A B}{t : Tm Γ B}{u : Tm Γ A} → lam (t [ p ]) $ u ≡ t
  constIsConst {t = t}{u = u} =
    lam (t [ p ]) $ u
      ≡⟨ ⇒β ⟩
    t [ p ] [ id ,o u ]
      ≡⟨ sym [∘] ⟩
    t [ p ⊚ (id ,o u) ]
      ≡⟨ cong (t [_]) ▹βSub ⟩
    t [ id ]
      ≡⟨ [id] ⟩
    t
      ∎

  curried : Tm ◆ (Nat ⇒ (Bool ⇒ Nat))
  curried = lam (lam (v1 +o ite v0 (num 1) (num 2))) -- λ x y . x + if y then 1 else 2

  higher : Tm ◆ ((Bool ⇒ Nat) ⇒ Nat)
  higher = lam (v0 $ isZero (v0 $ true)) -- λ f . f $ (isZero (f $ true))
{-
module Examples where
  open I
\end{code}
The function that adds 2 to its input is given as follows.
\begin{code}
  add2 : Tm ∙ (Nat ⇒ Nat)
  add2 = lam (v0 +o num 2) -- λx.x+2
\end{code}
Note that \verb$isZero$ does not have a function type (in fact, we introduced \verb$isZero$ before
\verb$_⇒_$). But we can define a function which acts like \verb$isZero$ as follows.
\begin{code}
  isZero' : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
  isZero' = lam (isZero v0) -- λx.isZero x
\end{code}
It obeys a computation rule similar to \verb$isZeroβ₁$ which is admissible:
\begin{code}
  isZero'β₁ : ∀{Γ} → isZero' {Γ} $ num 0 ≡ true
  isZero'β₁ =
    isZero' $ num 0
      ≡⟨ refl {x = isZero' $ num 0} ⟩
    lam (isZero v0) $ num 0
      ≡⟨ ⇒β ⟩
    isZero v0 [ id ,o num 0 ]
      ≡⟨ isZero[] ⟩
    isZero (v0 [ id ,o num 0 ])
      ≡⟨ cong isZero ▹β₂ ⟩
    isZero (num 0)
      ≡⟨ isZeroβ₁ ⟩
    true
      ∎
\end{code}
\begin{exe}[compulsory]
Prove the other computation rule:
{\normalfont
\begin{code}
  isZero'β₂ : ∀{Γ} → isZero' {Γ} $ num 1 ≡ false
\end{code}
\begin{code}[hide]
  isZero'β₂ = exercisep
\end{code}
}
\end{exe}
The function that constantly returns true is the following. Its domain type can be anything.
\begin{code}
  constTrue : Tm ∙ (Aᴵ ⇒ Bool)
  constTrue = lam true -- λx.true
\end{code}
A constant function is really constant:
\begin{code}
  constIsConst : ∀{Γ A B}{t : Tm Γ B}{u : Tm Γ A} → lam (t [ p ]) $ u ≡ t
  constIsConst {t = t}{u = u} =
    lam (t [ p ]) $ u
      ≡⟨ ⇒β ⟩
    t [ p ] [ id ,o u ]
      ≡⟨ sym {A = Tm _ _} [∘] ⟩
    t [ p ⊚ (id ,o u) ]
      ≡⟨ cong {A = Sub _ _} (t [_]) ▹β₁  ⟩
    t [ id ]
      ≡⟨ [id] ⟩
    t
      ∎
\end{code}
Functions with multiple parameters can be defined using the so-called
\emph{currying}. A function whose input is a \verb$Nat$ and a \verb$Bool$ and whose output is a \verb$Nat$ is the same
as a function whose domain is \verb$Nat$ and whose codomain is \verb$Bool ⇒ Nat$:
\begin{code}
  curried : Tm ∙ (Nat ⇒ (Bool ⇒ Nat))
  curried = lam (lam (v1 +o ite v0 (num 1) (num 2))) -- λ x y . x + if y then 1 else 2
\end{code}
The type of the following term also contains two arrows but it is parenthesised differently. It is a higher-order function, where the domain type is a function itself.
\begin{code}
  higher : Tm ∙ ((Bool ⇒ Nat) ⇒ Nat)
  higher = lam (v0 $ isZero (v0 $ true)) -- λ f . f $ (isZero (f $ true))
\end{code}
\begin{exe}[compulsory]
Define the function \verb$apply3 : Tm ∙ ((Bool ⇒ Bool) ⇒ Bool ⇒ Bool)$ which applies the function given as the first parameter three times to the boolean given as the second parameter.
\end{exe}

Note the difference between the types of \verb$isZero$ and \verb$isZero'$:
\begin{verbatim}
isZero   : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
isZero'  : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
\end{verbatim}
One is a metatheoretic function relating terms, the other is a function in our object theory. We show that the latter is stronger than the former: the types
\begin{code}[hide]
module ⇒vs→ {i j k l}(M : Model {i}{j}{k}{l})(A B : Model.Ty M) where
  open Model M
\end{code}
\begin{code}
  EXT = Σ (∀ Γ → Tm Γ A → Tm Γ B) λ f → ∀{Γ Δ γ a} → Lift (f Γ a [ γ ] ≡ f Δ (a [ γ ]))
  INT = Tm ∙ (A ⇒ B)
\end{code}
are isomorphic (for any \verb$A$, \verb$B$ in any model).
\begin{code}
  toINT : EXT → INT
  toINT f = lam (π₁ f (∙ ▹ A) q)
  toEXT : INT → EXT
  toEXT t = (λ Γ a → t [ ε ] $ a) , λ {Γ}{Δ}{γ}{a} → mk (
    (t [ ε ] $ a) [ γ ]
                             ≡⟨ $[] ⟩
    t [ ε ] [ γ ] $ a [ γ ]
                             ≡⟨ cong (_$ a [ γ ]) (sym {A = Tm _ _} [∘]) ⟩
    t [ ε ⊚ γ ] $ a [ γ ]
                             ≡⟨ cong (λ x → t [ x ] $ a [ γ ]) ∙η ⟩
    t [ ε ] $ a [ γ ]
                             ∎)
  extRoundtrip : (f : EXT) → toEXT (toINT f) ≡ f
  extRoundtrip f = (λ Γ a →
      lam (π₁ f (∙ ▹ A) q) [ ε ] $ a
                                                    ≡⟨ cong (_$ a) lam[] ⟩
      lam (π₁ f (∙ ▹ A) q [ ε ⊚ p ,o q ]) $ a
                                                    ≡⟨ cong (λ x → lam x $ a) (un (π₂ f)) ⟩
      lam (π₁ f (Γ ▹ A) (q [ ε ⊚ p ,o q ])) $ a
                                                    ≡⟨ cong (λ x → lam (π₁ f (Γ ▹ A) x) $ a) ▹β₂ ⟩
      lam (π₁ f (Γ ▹ A) q) $ a
                                                    ≡⟨ ⇒β ⟩
      π₁ f (Γ ▹ A) q [ id ,o a ]
                                                    ≡⟨ un (π₂ f) ⟩
      π₁ f Γ (q [ id ,o a ])
                                                    ≡⟨ cong (π₁ f Γ) ▹β₂ ⟩
      π₁ f Γ a
                                                    ∎)
    , λ _ _ _ _ → mk trivi
  intRoundtrip : (t : INT) → toINT (toEXT t) ≡ t
  intRoundtrip t = trans {A = Tm _ _}
    (cong (λ γ → lam (t [ γ ] $ q)) (sym {A = Sub _ ∙} ∙η))
    ⇒η
\end{code}
-}