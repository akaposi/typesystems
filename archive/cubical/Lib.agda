{-# OPTIONS --cubical --no-forcing --guardedness #-}

module Lib where

open import Agda.Primitive public
open import Cubical.Core.Everything hiding (Sub) public
open import Cubical.Foundations.Prelude hiding (Sub) public
open import Cubical.Foundations.HLevels public
open import Cubical.Foundations.Isomorphism public
open import Cubical.Foundations.Equiv hiding (transpEquiv) public
open import Cubical.Foundations.Transport public
open import Cubical.Foundations.Function public

-- open import Cubical.Data.Prod public
--   renaming (proj₁ to π₁ ; proj₂ to π₂)
open import Cubical.Data.Unit public
  renaming (Unit to ⊤ ; tt to trivial ; isSetUnit to isSet⊤)
open import Cubical.Data.Nat public
  renaming (isZero to isZeroℕ ; elim to elimℕ)
open import Cubical.Data.Bool public
  renaming (Bool to 𝟚; false to ff; true to tt ; elim to elim𝟚)
open import Cubical.Data.Empty public
  renaming (rec to exfalso ; elim to exfalsoD)
open import Cubical.Data.Sigma public
  renaming (fst to π₁ ; snd to π₂)
  hiding (Sub)
open import Cubical.Data.List public
  renaming (map2 to zipWith ; rev to reverse)
  hiding ([_])
open import Cubical.Data.Maybe public
  renaming (elim to elimMaybe ; rec to recMaybe)
open import Cubical.Data.FinData public
  renaming (elim to elimFin ; snotz to snotzFin ; znots to znotsFin)
open import Cubical.Data.Sum public
  renaming (inl to ι₁ ; inr to ι₂ ; rec to case ; elim to elim⊎ ; map to map⊎)
open import Tree public
  renaming (rec to iteTree ; elim to elimTree)
open import Cubical.Codata.Stream public
  renaming (_,_ to _∷∞_)

open import Cubical.Relation.Nullary public
open import Cubical.Relation.Nullary.DecidablePropositions public

postulate
  exercise : ∀{i}{A : Type i} → A

level : ℕ → Level
level 0 = lzero
level (suc n) = lsuc (level n)

{-
isZeroℕ : ℕ → 𝟚
isZeroℕ zero = tt
isZeroℕ (suc _) = ff
-}

id : ∀ {i} {A : Type i} → A → A
id {i} {A} = idfun A

the : ∀ {i} (A : Type i) → A → A
the = idfun

syntax the A x = x ∶ A

infixr 9 _∘i_

_∘i_ : ∀ {i j k} {A : Type i}{B : A → Type j}{C : ∀ {x} → B x → Type k}
  (f : ∀ {x} (y : B x) → C y)(g : (x : A) → B x)
  (x : A) → C (g x)
(f ∘i g) x = f (g x)

infixr 2 _≡≡_
infix 3 _∎∎
infix 4 _≝_

data _≝_ {i}{A : Type i} : A → A → Type i where
  _∎∎   : (x : A) → x ≝ x
  _≡≡_   : (x : A) → x ≝ x → x ≝ x

ind𝟚 : ∀{i}(P : 𝟚 → Set i) → P tt → P ff → (t : 𝟚) → P t
ind𝟚 P u v tt = u
ind𝟚 P u v ff = v

infix 0 dep-if_then_else_
dep-if_then_else_ : ∀{i}{A : 𝟚 → Set i} → (b : 𝟚) → A tt → A ff → A b
dep-if_then_else_ {A = A} b t f = ind𝟚 A t f b

_∨𝟚_ : 𝟚 → 𝟚 → 𝟚
a ∨𝟚 b = a or b

recℕ : ∀{ℓ}{A : Type ℓ} → A → (A → A) → ℕ → A
recℕ a f n = iter n f a

cong2 : ∀{i j}{A : Type i}{B : Type j}{C : A → B → Type (ℓ-max i j)}
        {a c : A}{b d : B}(f : (x : A) → (y : B) → C x y) →
        (p : a ≡ c) → (q : b ≡ d) →
         PathP (λ i → C (p i) (q i)) (f a b) (f c d)
cong2 f p q i = f (p i) (q i)

cong3 : ∀{i j k}{A : Type i}{B : Type j}{C : Type k}{D : A → B → C → Type (ℓ-max i (ℓ-max j k))}
        {a d : A}{b e : B}{c z : C}(f : (x : A) → (y : B) → (w : C) → D x y w) →
        (p : a ≡ d) → (q : b ≡ e) → (r : c ≡ z) →
         PathP (λ i → D (p i) (q i) (r i)) (f a b c) (f d e z)
cong3 f p q r i = f (p i) (q i) (r i)

iteList : ∀{ℓ ℓ'}{A : Type ℓ}{B : Type ℓ'} → B → (A → B → B) → List A → B
iteList b f xs = foldr f b xs