{-# OPTIONS --cubical --guardedness #-}

module NatBool where

open import Lib

infixl 6 _+o_

data Ty     : Set where
  Nat       : Ty
  Bool      : Ty
  
data Tm : Ty → Set where
  true       : Tm Bool
  false      : Tm Bool
  ite        : ∀{A} → Tm Bool → Tm A → Tm A → Tm A
  num        : ℕ → Tm Nat
  isZero     : Tm Nat → Tm Bool
  _+o_       : Tm Nat → Tm Nat → Tm Nat
  iteβtrue   : ∀{A}{u v : Tm A} → ite true u v ≡ u
  iteβfalse  : ∀{A}{u v : Tm A} → ite false u v ≡ v
  isZeroβ0   : isZero (num 0) ≡ true
  isZeroβsuc : {n : ℕ} → isZero (num (1 + n)) ≡ false
  +β         : {m n : ℕ} → num m +o num n ≡ num (m + n)
  TmSet      : ∀{A} → isSet (Tm A)

ex : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
ex = 
  ite (isZero (num 0 +o num 1)) false (isZero (num 0))
    ≡⟨ cong (λ x → ite (isZero x) false (isZero (num 0))) +β ⟩ 
  ite (isZero (num 1)) false (isZero (num zero)) 
    ≡⟨ cong (λ x → ite x false (isZero (num zero))) isZeroβsuc ⟩ 
  ite false false (isZero (num zero))
    ≡⟨ iteβfalse ⟩ 
  isZero (num 0)
    ≡⟨ isZeroβ0 ⟩ 
  true
  ∎
{-
height : ∀{A} → Tm A → ℕ
height true = 0
height false = 0
height (ite tm tm₁ tm₂) = 1 + max (height tm) (max (height tm₁) (height tm₂))
height (num x) = 0
height (isZero tm) = 1 + height tm
height (tm +o tm₁) = 1 + max (height tm) (height tm₁)
height (iteβ₁ {_} {u} {v} i) = {!   !} -- itt kezd baj lenni, ez nem bizonyítható
height (iteβ₂ i) = {!   !}
height (isZeroβ₁ i) = {!   !}
height (isZeroβ₂ i) = {!   !}
height (+β i) = {!   !}
-}
trivialModel : ∀{A} → Tm A → ⊤
trivialModel (TmSet tm tm₁ x y i i₁) = trivial
trivialModel true = trivial
trivialModel false = trivial
trivialModel (ite tm tm₁ tm₂) = trivial
trivialModel (num x) = trivial
trivialModel (isZero tm) = trivial
trivialModel (tm +o tm₁) = trivial
trivialModel (iteβtrue i) = trivial
trivialModel (iteβfalse i) = trivial
trivialModel (isZeroβ0 i) = trivial
trivialModel (isZeroβsuc i) = trivial
trivialModel (+β i) = trivial

b : ∀{A} → Tm A → 𝟚
b (TmSet tm tm₁ x y i i₁) = isSet→isSet' isSetBool (λ j → b (x j)) (λ j → b (y j)) (λ _ → b tm) (λ _ → b tm₁) i i₁
b true = tt
b false = ff
b (ite tm tm₁ tm₂) = if b tm then b tm₁ else b tm₂
b (num x) = ff
b (isZero (TmSet tm tm₁ x y i i₁)) = isSet→isSet' isSetBool (λ j → b (isZero (x j))) (λ j → b (isZero (y j))) (λ _ → b (isZero tm)) (λ _ → b (isZero tm₁)) i i₁
b (isZero (ite tm tm₁ tm₂)) = if b tm then b (isZero tm₁) else b (isZero tm₂)
b (isZero (num zero)) = tt
b (isZero (num (suc x))) = ff
b (isZero (tm +o tm₁)) = b (isZero tm) and b (isZero tm₁)
b (isZero (iteβtrue {_} {u} {v} i)) = isSet→isSet' isSetBool (λ _ → b (isZero u)) (λ _ → b (isZero u)) (λ _ → b (isZero u)) (λ _ → b (isZero u)) i i
b (isZero (iteβfalse {_} {u} {v} i)) = isSet→isSet' isSetBool (λ _ → b (isZero v)) (λ _ → b (isZero v)) (λ _ → b (isZero v)) (λ _ → b (isZero v)) i i
b (isZero (+β {zero} {zero} i)) = tt
b (isZero (+β {zero} {suc n} i)) = ff
b (isZero (+β {suc m} {zero} i)) = ff
b (isZero (+β {suc m} {suc n} i)) = ff
b (tm +o tm₁) = b tm or b tm₁
b (iteβtrue {_} {u} {v} i) = b u
b (iteβfalse {_} {u} {v} i) = b v
b (isZeroβ0 i) = tt
b (isZeroβsuc i) = ff
b (+β i) = ff

St⟦_⟧T : Ty → Type
St⟦ Nat ⟧T = ℕ
St⟦ Bool ⟧T = 𝟚

St⟦_⟧t : ∀{A} → Tm A → St⟦ A ⟧T
St⟦ true ⟧t = tt
St⟦ false ⟧t = ff
St⟦ ite tm tm₁ tm₂ ⟧t = if St⟦ tm ⟧t then St⟦ tm₁ ⟧t else St⟦ tm₂ ⟧t
St⟦ num x ⟧t = x
St⟦ isZero tm ⟧t = isZeroℕ St⟦ tm ⟧t
St⟦ tm +o tm₁ ⟧t = St⟦ tm ⟧t + St⟦ tm₁ ⟧t
St⟦ iteβtrue {_} {u} {v} i ⟧t = St⟦ u ⟧t
St⟦ iteβfalse {_} {u} {v} i ⟧t = St⟦ v ⟧t
St⟦ isZeroβ0 i ⟧t = tt
St⟦ isZeroβsuc i ⟧t = ff
St⟦ +β {m} {n} i ⟧t = m + n
St⟦_⟧t {Nat} (TmSet {.Nat} tm tm₁ x y i i₁) = isSet→isSet' isSetℕ (λ j → St⟦ x j ⟧t) (λ j → St⟦ y j ⟧t) (λ _ → St⟦ tm ⟧t) (λ _ → St⟦ tm₁ ⟧t) i i₁
St⟦_⟧t {Bool} (TmSet {.Bool} tm tm₁ x y i i₁) = isSet→isSet' isSetBool (λ j → St⟦ x j ⟧t ) (λ j → St⟦ y j ⟧t ) (λ _ → St⟦ tm ⟧t) (λ _ → St⟦ tm₁ ⟧t ) i i₁

eq-0 : true ≡ true
eq-0 = refl

eq-1 : isZero (num 0) ≡ true
eq-1 = isZeroβ0

eq-2 : isZero (num 3 +o num 1) ≡ isZero (num 4)
eq-2 = cong isZero +β

eq-3 : isZero (num 4) ≡ false
eq-3 = isZeroβsuc

eq-4 : isZero (num 3 +o num 1) ≡ false
eq-4 = eq-2 ∙ eq-3

eq-4' : isZero (num 3 +o num 1) ≡ false
eq-4' =
  isZero (num 3 +o num 1)
    ≡⟨ cong isZero +β ⟩ -- \== \< \>
  isZero (num 4)
    ≡⟨ isZeroβsuc ⟩
  false
    ∎ -- \qed

eq-5 : ite false (num 2) (num 5) ≡ num 5
eq-5 = iteβfalse

eq-6 : ite true (isZero (num 0)) false ≡ true
eq-6 = iteβtrue ∙ isZeroβ0

eq-6' : ite true (isZero (num 0)) false ≡ true
eq-6' =
  ite true (isZero (num 0)) false
    ≡⟨ iteβtrue ⟩
  isZero (num 0)
    ≡⟨ isZeroβ0 ⟩
  true
    ∎

eq-7 : num 3 +o num 0 +o num 1 ≡ num 4
eq-7 =
  (num 3 +o num 0 +o num 1)
    ≡⟨ cong (_+o num 1) +β ⟩
  num 3 +o num 1
    ≡⟨ +β ⟩
  num 4
    ∎

eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
eq-8 =
  ite (isZero (num 0)) (num 1 +o num 1) (num 0)
    ≡⟨ cong (λ x → ite x (num 1 +o num 1) (num 0)) isZeroβ0 ⟩
  ite true (num 1 +o num 1) (num 0)
    ≡⟨ iteβtrue ⟩
  num 1 +o num 1
    ≡⟨ +β ⟩
  num 2
    ∎

eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
eq-9 =
  (num 3 +o ite (isZero (num 2)) (num 1) (num 0))
    ≡⟨ cong (λ x → num 3 +o ite x (num 1) (num 0)) isZeroβsuc ⟩
  num 3 +o ite false (num 1) (num zero)
    ≡⟨ cong (num 3 +o_) iteβfalse ⟩
  num 3 +o num zero
    ≡⟨ +β ⟩
  num 3
    ∎

eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
eq-10 =
  (ite false (num 1 +o num 1) (num 0) +o num 0)
    ≡⟨ cong (_+o num 0) iteβfalse ⟩
  num zero +o num zero
    ≡⟨ +β ⟩
  num 0
    ∎

eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
eq-11 =
  ite (isZero (num 0 +o num 1)) false (isZero (num 0))
    ≡⟨ cong (λ x → ite (isZero x) false (isZero (num 0))) +β ⟩
  ite (isZero (num 1)) false (isZero (num zero))
    ≡⟨ cong (λ x → ite x false (isZero (num 0))) isZeroβsuc ⟩
  ite false false (isZero (num zero))
    ≡⟨ iteβfalse ⟩
  isZero (num zero)
    ≡⟨ isZeroβ0 ⟩
  true
    ∎

eq-12 : num 3 +o num 2 ≡ ite true (num 5) (num 1)
eq-12 =
  num 3 +o num 2
    ≡⟨ +β ⟩
  num 5
    ≡⟨ sym iteβtrue ⟩
  ite true (num 5) (num 1)
    ∎

{-
iteβsame : ∀{A t}{u : Tm A} → ite t u u ≡ u
iteβsame {A} {t} {u} = {!   !}
-}

norm : ∀{A} → Tm A → St⟦ A ⟧T
norm = St⟦_⟧t

⌜_⌝N : ℕ → Tm Nat
⌜_⌝N = num

⌜_⌝B : 𝟚 → Tm Bool
⌜ tt ⌝B = true
⌜ ff ⌝B = false

⌜_⌝ : ∀ {A} → St⟦ A ⟧T → Tm A
⌜_⌝ {Nat} = ⌜_⌝N
⌜_⌝ {Bool} = ⌜_⌝B

⌜isZero⌝ : ∀{n} → ⌜ isZeroℕ n ⌝ ≡ isZero ⌜ n ⌝
⌜isZero⌝ {zero}   = sym isZeroβ0
⌜isZero⌝ {suc n}  = sym isZeroβsuc

⌜+⌝ : ∀{m n} → ⌜ m + n ⌝N ≡ ⌜ m ⌝N +o ⌜ n ⌝N
⌜+⌝ {m} {n} = sym +β

⌜ite⌝ : ∀{b A}{u v : St⟦ A ⟧T} → ⌜_⌝ {A} (if b then u else v) ≡ ite ⌜ b ⌝ ⌜ u ⌝ ⌜ v ⌝
⌜ite⌝ {tt}{A} = sym iteβtrue
⌜ite⌝ {ff}{A} = sym iteβfalse

completeness : ∀{A}(t : Tm A) → ⌜ norm t ⌝ ≡ t

completeness true = refl

completeness false = refl

completeness {A} (ite t t₁ t₂) = 
  ⌜ if St⟦ t ⟧t then St⟦ t₁ ⟧t else St⟦ t₂ ⟧t ⌝ 
  ≡⟨ ⌜ite⌝ {St⟦ t ⟧t} {A} {St⟦ t₁ ⟧t} {St⟦ t₂ ⟧t} ⟩ 
  ite  ⌜ St⟦ t ⟧t ⌝ ⌜ St⟦ t₁ ⟧t ⌝ ⌜ St⟦ t₂ ⟧t ⌝
  ≡⟨ cong3 ite (completeness t) (completeness t₁) (completeness t₂) ⟩ 
  ite t t₁ t₂ ∎

completeness (num x) = refl

completeness (isZero t) =
  ⌜ isZeroℕ St⟦ t ⟧t ⌝
  ≡⟨ ⌜isZero⌝ {St⟦ t ⟧t} ⟩
  isZero ⌜ St⟦ t ⟧t ⌝
  ≡⟨ cong isZero (completeness t) ⟩
  isZero t ∎

completeness (t +o t₁) = 
  num (St⟦ t ⟧t + St⟦ t₁ ⟧t) 
  ≡⟨ (⌜+⌝ {St⟦ t ⟧t} {St⟦ t₁ ⟧t}) ⟩ 
  num St⟦ t ⟧t +o num St⟦ t₁ ⟧t
  ≡⟨ cong2 _+o_ (completeness t) (completeness t₁) ⟩ 
  t +o t₁ ∎

completeness (iteβtrue {A} {u} {v} i) j = isSet→isSet' TmSet
  (λ j → (
    ⌜ St⟦ u ⟧t ⌝
    ≡⟨ (λ i₁ → iteβtrue {A} {⌜ St⟦ u ⟧t ⌝} {⌜ St⟦ v ⟧t ⌝} (~ i₁)) ⟩
    ite {A} true ⌜ St⟦ u ⟧t ⌝ ⌜ St⟦ v ⟧t ⌝
    ≡⟨ cong3 ite (λ _ → true) (completeness u) (completeness v) ⟩
    ite {A} true u v ∎) j)
  (λ j → completeness u j)
  (λ i → ⌜ norm (iteβtrue {A} {u} {v} i) ⌝)
  (λ i → iteβtrue {A} {u} {v} i) i j

completeness (iteβfalse {A} {u} {v} i) j = isSet→isSet' TmSet
  (λ j → (
    ⌜ St⟦ v ⟧t ⌝
    ≡⟨ (λ i₁ → iteβfalse {A} {⌜ St⟦ u ⟧t ⌝} {⌜ St⟦ v ⟧t ⌝} (~ i₁)) ⟩
    ite {A} false ⌜ St⟦ u ⟧t ⌝ ⌜ St⟦ v ⟧t ⌝
    ≡⟨ cong3 ite (λ _ → false) (completeness u) (completeness v) ⟩
    ite {A} false u v ∎) j)
  (λ j → completeness v j)
  (λ i → ⌜ norm (iteβfalse {A} {u} {v} i) ⌝)
  (λ i → iteβfalse {A} {u} {v} i) i j

completeness (isZeroβ0 i) j = isSet→isSet' TmSet
  (λ j → (
    true
    ≡⟨ (λ i₁ → isZeroβ0 (~ i₁)) ⟩
    isZero ⌜ 0 ⌝N
    ≡⟨ (λ i₁ → isZero ⌜ norm (num 0) ⌝N) ⟩
    isZero (num 0) ∎) j)
  (λ j → true)
  (λ i → true)
  (λ i → isZeroβ0 i) i j

completeness (isZeroβsuc {n} i) j = isSet→isSet' TmSet
  (λ j → (
    false
    ≡⟨ (λ i₁ → isZeroβsuc {n} (~ i₁)) ⟩
    isZero ⌜ suc n ⌝N
    ≡⟨ (λ i₁ → isZero ⌜ norm (num (suc n)) ⌝N) ⟩
    isZero (num (suc n)) ∎) j)
  (λ j → false)
  (λ i → false)
  (λ i → isZeroβsuc {n} i) i j

completeness (+β {m} {n} i) j = isSet→isSet' TmSet
  (λ j → (
    num (m + n) 
    ≡⟨ ⌜+⌝ ⟩
    num m +o num n
    ≡⟨ cong2 _+o_ (λ _ → ⌜ norm (num m) ⌝N) (λ _ → ⌜ norm (num n) ⌝N) ⟩
    num m +o num n ∎) j)
  (λ j → ⌜ norm (num (m + n)) ⌝N)
  (λ i → ⌜ norm (+β {m} {n} i) ⌝N)
  (λ i → +β {m} {n} i) i j

completeness {A} (TmSet t t₁ x y i i₁) j = isGroupoid→isGroupoid' (isSet→isGroupoid TmSet)
  (λ i₁ j → completeness (x i₁) j)
  (λ i₁ j → completeness (y i₁) j)
  (λ i j  → completeness t j)
  (λ i j  → completeness t₁ j)
  (λ i i₁ → ⌜ norm (TmSet t t₁ x y i i₁) ⌝)
  (λ i i₁ → TmSet t t₁ x y i i₁) i i₁ j

stability : {A : Ty}{t : St⟦ A ⟧T} → norm {A} ⌜ t ⌝ ≡ t
stability {Nat}  {t}  = refl
stability {Bool} {tt} = refl
stability {Bool} {ff} = refl