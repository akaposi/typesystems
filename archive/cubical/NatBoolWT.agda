{-# OPTIONS --cubical --prop --guardedness #-}

module NatBoolWT where

open import Lib

data Ty   : Type where
  Nat     : Ty
  Bool    : Ty

data Tm   : Ty → Type where
  true    : Tm Bool
  false   : Tm Bool
  ite     : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
  num     : ℕ → Tm Nat
  isZero  : Tm Nat → Tm Bool
  _+o_    : Tm Nat → Tm Nat → Tm Nat
  
import NatBoolAST as AST

infer : AST.Tm → Maybe (Σ Ty Tm)
infer AST.true = just (Bool , true)
infer AST.false = just (Bool , false)
infer (AST.ite tm tm₁ tm₂) = iteWT (infer tm) (infer tm₁) (infer tm₂) where
  iteWT : Maybe (Σ Ty Tm) → Maybe (Σ Ty Tm) → Maybe (Σ Ty Tm) → Maybe (Σ Ty Tm)
  iteWT (just (Bool , t)) (just (Nat , u))  (just (Nat , v))  = just (Nat , ite t u v)
  iteWT (just (Bool , t)) (just (Bool , u)) (just (Bool , v)) = just (Bool , ite t u v)
  {-# CATCHALL #-}
  iteWT _                 _                 _                 = nothing
infer (AST.num n) = just (Nat , num n)
infer (AST.isZero tm) = isZeroWT (infer tm) where
  isZeroWT : Maybe (Σ Ty Tm) → Maybe (Σ Ty Tm)
  isZeroWT (just (Nat , t)) = just (Bool , isZero t)
  {-# CATCHALL #-}
  isZeroWT _                = nothing
infer (tm AST.+o tm₁) = infer tm +oWT infer tm₁ where
  _+oWT_ : Maybe (Σ Ty Tm) → Maybe (Σ Ty Tm) → Maybe (Σ Ty Tm)
  _+oWT_ (just (Nat , t)) (just (Nat , u)) = just (Nat , t +o u)
  {-# CATCHALL #-}
  _+oWT_ _                _                = nothing

St⟦_⟧T : Ty → Type
St⟦ Nat ⟧T = ℕ
St⟦ Bool ⟧T = 𝟚

St⟦_⟧t : {A : Ty} → Tm A → St⟦ A ⟧T
St⟦ true ⟧t = tt
St⟦ false ⟧t = ff
St⟦ ite tm tm₁ tm₂ ⟧t = if St⟦ tm ⟧t then St⟦ tm₁ ⟧t else St⟦ tm₂ ⟧t
St⟦ num x ⟧t = x
St⟦ isZero tm ⟧t = isZeroℕ St⟦ tm ⟧t
St⟦ tm +o tm₁ ⟧t = St⟦ tm ⟧t + St⟦ tm₁ ⟧t

norm : ∀{A} → Tm A → St⟦ A ⟧T
norm = St⟦_⟧t

data _⦂_  :  AST.Tm → Ty → Prop where
  true'   :
                               -----------------------
                               AST.true ⦂ Bool
  false'  :
                               -----------------------
                               AST.false ⦂ Bool
  ite'    :  ∀{t u v A} →
                               t ⦂ Bool                  →
                               u ⦂ A                     →
                               v ⦂ A                     →
                               -----------------------
                               AST.ite t u v ⦂ A
  num'    :  ∀{n} →
                               -----------------------
                               AST.num n ⦂ Nat
  isZero' :  ∀{t} →
                               t ⦂ Nat                   →
                               -----------------------
                               AST.isZero t ⦂ Bool
  _+o'_   :  ∀{u v} →
                               u ⦂ Nat                   →
                               v ⦂ Nat                   →
                               -----------------------
                               (u AST.+o v) ⦂ Nat

data DTy : Ty → Type where
  DBool : DTy Bool
  DNat : DTy Nat

data DTm : ∀{A} → DTy A → Tm A → Type where
  dtrue : DTm DBool true
  dfalse : DTm DBool false
  dite : ∀{Aᴵ t u v}{A : DTy Aᴵ} → DTm DBool t → DTm A u → DTm A v → DTm A (ite t u v)
  dnum : (n : ℕ) → DTm DNat (num n)
  dIsZero : ∀{t} → DTm DNat t → DTm DBool (isZero t)
  _d+o_ : ∀{u v} → DTm DNat u → DTm DNat v → DTm DNat (u +o v)

D⟦_⟧T  : (Aᴵ : Ty) → DTy Aᴵ
D⟦_⟧t  : ∀{Aᴵ}(tᴵ : Tm Aᴵ) → DTm D⟦ Aᴵ ⟧T tᴵ
D⟦ Nat           ⟧T = DNat
D⟦ Bool          ⟧T = DBool
D⟦ true          ⟧t = dtrue
D⟦ false         ⟧t = dfalse
D⟦ ite tᴵ uᴵ vᴵ  ⟧t = dite D⟦ tᴵ ⟧t D⟦ uᴵ ⟧t D⟦ vᴵ ⟧t
D⟦ num n         ⟧t = dnum n
D⟦ isZero tᴵ     ⟧t = dIsZero D⟦ tᴵ ⟧t
D⟦ uᴵ +o vᴵ      ⟧t = D⟦ uᴵ ⟧t d+o D⟦ vᴵ ⟧t