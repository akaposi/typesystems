{-# OPTIONS --cubical --guardedness #-}

module DefWT where

open import Lib

data Ty   : Set where
    Nat     : Ty
    Bool    : Ty
    
data Con : Set where
  ◆ : Con
  _▹_ : Con → Ty → Con

infixl 5 _▹_

data Var : Con → Ty → Set where
  vz : ∀{Γ A} → Var (Γ ▹ A) A
  vs : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

data Tm (Γ : Con) : Ty → Set where
  var     : ∀{A} → Var Γ A → Tm Γ A
  def     : ∀{A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B

  true    : Tm Γ Bool
  false   : Tm Γ Bool
  ite     : ∀{A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
  num     : ℕ → Tm Γ Nat
  isZero  : Tm Γ Nat → Tm Γ Bool
  _+o_    : Tm Γ Nat → Tm Γ Nat → Tm Γ Nat

v0 : {Γ : Con}{A : Ty}        → Tm (Γ ▹ A) A
v0 = var vz

v1 : {Γ : Con}{A B : Ty}      → Tm (Γ ▹ A ▹ B) A
v1 = var (vs vz)

v2 : {Γ : Con}{A B C : Ty}    → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = var (vs (vs vz))

v3 : {Γ : Con}{A B C D : Ty}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = var (vs (vs (vs vz)))

St⟦_⟧T : Ty → Type
St⟦ Nat ⟧T = ℕ
St⟦ Bool ⟧T = 𝟚

St⟦_⟧C : Con → Type
St⟦ ◆ ⟧C = ⊤
St⟦ c ▹ x ⟧C = St⟦ c ⟧C × St⟦ x ⟧T

St⟦_⟧v : ∀{Γ A} → Var Γ A → St⟦ Γ ⟧C → St⟦ A ⟧T
St⟦ vz ⟧v = π₂
St⟦ vs v ⟧v γ = St⟦ v ⟧v (π₁ γ)

St⟦_⟧t : ∀{Γ A} → Tm Γ A → St⟦ Γ ⟧C → St⟦ A ⟧T
St⟦ var x ⟧t γ = St⟦ x ⟧v γ
St⟦ def tm tm₁ ⟧t γ = St⟦ tm₁ ⟧t (γ , St⟦ tm ⟧t γ)
St⟦ true ⟧t γ = tt
St⟦ false ⟧t γ = ff
St⟦ ite tm tm₁ tm₂ ⟧t γ = if St⟦ tm ⟧t γ then St⟦ tm₁ ⟧t γ else St⟦ tm₂ ⟧t γ
St⟦ num x ⟧t γ = x
St⟦ isZero tm ⟧t γ = isZeroℕ (St⟦ tm ⟧t γ)
St⟦ tm +o tm₁ ⟧t γ = St⟦ tm ⟧t γ + St⟦ tm₁ ⟧t γ