{-# OPTIONS --cubical --guardedness #-}

module Ind where

open import Lib hiding (id)

infixl 6 _⊚_
infixl 6 _[_]'
infixl 6 _[_]
infixl 5 _▹_
infixl 5 _,o_
infixr 5 _⇒_
infixl 5 _$_

data Ty      : Type where
  _⇒_        : Ty → Ty → Ty
  ⊤ₒ         : Ty
  Bool       : Ty
  Nat        : Ty
  Listₒ      : Ty → Ty
  Treeₒ      : Ty → Ty

data Con     : Type where
  ◆          : Con
  _▹_        : Con → Ty → Con

data Sub : Con → Con → Type
data Tm : Con → Ty → Type

_[_]' : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
q' : ∀{Γ A} → Tm (Γ ▹ A) A

data Sub where
  ε         : ∀{Γ} → Sub Γ ◆
  ◆η        : ∀{Γ}{σ : Sub Γ ◆} → σ ≡ ε

  _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
  
  id        : ∀{Γ} → Sub Γ Γ
  idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
  idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ
  
  p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
  _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
  ▹βSub     : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
  ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → (((p ⊚ γa) ,o (q' [ γa ]')) ≡ γa)
  SubSet    : ∀{Γ Δ} → isSet (Sub Δ Γ)

data Tm where
  q         : ∀{Γ A} → Tm (Γ ▹ A) A
  _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
  [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
  [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
  ▹βTm      : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → (q [ γ ,o t ]) ≡ t

  lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
  ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} → (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
  $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

  trivialₒ  : ∀{Γ} → Tm Γ ⊤ₒ
  ⊤η        : ∀{Γ}{u : Tm Γ ⊤ₒ} → u ≡ trivialₒ
  ite⊤      : ∀{Γ A} → Tm Γ A → Tm Γ ⊤ₒ → Tm Γ A
  ⊤β        : ∀{Γ A t} → ite⊤ {Γ}{A} t trivialₒ ≡ t
  trivial[] : ∀{Γ Δ}{γ : Sub Δ Γ} → trivialₒ [ γ ] ≡ trivialₒ
  ite⊤[]    : ∀{Γ A t u Δ}{γ : Sub Δ Γ} →
                 ite⊤ {Γ}{A} u t [ γ ] ≡ ite⊤ (u [ γ ]) (t [ γ ])
  
  true          : ∀{Γ} → Tm Γ Bool
  false         : ∀{Γ} → Tm Γ Bool
  iteBool       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
  iteBoolβtrue  : ∀{Γ A u v} → iteBool {Γ}{A} true u v ≡ u
  iteBoolβfalse : ∀{Γ A u v} → iteBool {Γ}{A} false u v ≡ v
  true[]        : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
  false[]       : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
  iteBool[]     : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                    iteBool {Γ}{A} t u v [ γ ] ≡ iteBool (t [ γ ]) (u [ γ ]) (v [ γ ])

  zeroₒ      : ∀{Γ} → Tm Γ Nat
  sucₒ       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
  iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
  iteNatβ0   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroₒ ≡ u
  iteNatβsuc : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                iteNat u v (sucₒ t) ≡ v [ id ,o iteNat u v t ]
  zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroₒ [ γ ] ≡ zeroₒ
  suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (sucₒ t) [ γ ] ≡ sucₒ (t [ γ ])
  iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])

  nil          : ∀{Γ A} → Tm Γ (Listₒ A)
  cons         : ∀{Γ A} → Tm Γ A → Tm Γ (Listₒ A) → Tm Γ (Listₒ A)
  iteListₒ      : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (Listₒ A) → Tm Γ B
  iteListβnil  : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → iteListₒ u v nil ≡ u
  iteListβcons : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t : Tm Γ A}{ts : Tm Γ (Listₒ A)} →
                  iteListₒ u v (cons t ts) ≡ (v [ id ,o t ,o iteListₒ u v ts ])
  nil[]        : ∀{Γ A Δ}{γ : Sub Δ Γ} → nil {Γ}{A} [ γ ] ≡ nil {Δ}{A}
  cons[]       : ∀{Γ A}{t : Tm Γ A}{ts : Tm Γ (Listₒ A)}{Δ}{γ : Sub Δ Γ} →
                  (cons t ts) [ γ ] ≡ cons (t [ γ ]) (ts [ γ ])
  iteList[]    : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t : Tm Γ (Listₒ A)}{Δ}{γ : Sub Δ Γ} →
                  iteListₒ u v t [ γ ] ≡ iteListₒ (u [ γ ]) (v [ (γ ⊚ p ,o q) ⊚ p ,o q ]) (t [ γ ])

  leafₒ        : ∀{Γ A} → Tm Γ A → Tm Γ (Treeₒ A)
  nodeₒ        : ∀{Γ A} → Tm Γ (Treeₒ A) → Tm Γ (Treeₒ A) → Tm Γ (Treeₒ A)
  iteTreeₒ     : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm (Γ ▹ B ▹ B) B → Tm Γ (Treeₒ A) → Tm Γ B
  iteTreeβleaf : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{a : Tm Γ A} → iteTreeₒ l n (leafₒ a) ≡ l [ id ,o a ]
  iteTreeβnode : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{ll rr : Tm Γ (Treeₒ A)} →
                 iteTreeₒ l n (nodeₒ ll rr) ≡ n [ id ,o iteTreeₒ l n ll ,o iteTreeₒ l n rr ]
  leaf[]       : ∀{Γ A}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (leafₒ a) [ γ ] ≡ leafₒ (a [ γ ])
  node[]       : ∀{Γ A}{ll rr : Tm Γ (Treeₒ A)}{Δ}{γ : Sub Δ Γ} →
                 (nodeₒ ll rr) [ γ ] ≡ nodeₒ (ll [ γ ]) (rr [ γ ])
  iteTree[]    : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{t : Tm Γ (Treeₒ A)}{Δ}{γ : Sub Δ Γ} →
                 iteTreeₒ l n t [ γ ] ≡ iteTreeₒ (l [ (γ ⊚ p) ,o q ]) (n [ (γ ⊚ p ⊚ p) ,o (q [ p ]) ,o q ]) (t [ γ ])

  TmSet     : ∀{Γ A} → isSet (Tm Γ A)

q' = q
_[_]' = _[_]

def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
def t u = u [ id ,o t ]

v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
v0 = q

v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
v1 = q [ p ]

v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = q [ p ⊚ p ]

v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = q [ p ⊚ p ⊚ p ]

▹η' : ∀{Γ A} → p ,o q ≡ id {Γ ▹ A}
▹η' {Γ}{A} =
  p ,o q
    ≡⟨ sym (cong₂ _,o_ idr [id]) ⟩
  p ⊚ id ,o q [ id ]
    ≡⟨ ▹η ⟩
  id
    ∎

,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
  (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
  (γ ,o t) ⊚ δ
    ≡⟨ sym ▹η ⟩
  (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
    ≡⟨ cong₂ _,o_ (sym ass) [∘] ⟩
  ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
    ≡⟨ cong₂ _,o_ (cong (_⊚ δ) ▹βSub) (cong (_[ δ ]) ▹βTm) ⟩
  γ ⊚ δ ,o t [ δ ]
    ∎

plus : Tm ◆ (Nat ⇒ Nat ⇒ Nat)
plus = lam (lam (iteNat v0 (sucₒ v0) v1))

isZero : Tm ◆ (Nat ⇒ Bool)
isZero = lam (iteNat true false v0)

isnil : {A : Ty} → Tm ◆ (Listₒ A ⇒ Bool)
isnil = lam (iteListₒ true false v0)

concat : {A : Ty} → Tm ◆ (Listₒ A ⇒ Listₒ A ⇒ Listₒ A)
concat = lam (lam (iteListₒ v0 (cons v1 v0) v1))

concatTest : {A : Ty}{x y z : Tm ◆ A} →
    concat $ cons x (cons y nil) $ cons z nil ≡ cons x (cons y (cons z nil))
concatTest {A} {x} {y} {z} = 
  lam (lam (iteListₒ q (cons (q [ p ]) q) (q [ p ]))) $ cons x (cons y nil) $ cons z nil 
  ≡⟨ cong (_$ cons z nil) (⇒β ∙ lam[]) ⟩
  lam (iteListₒ q (cons (q [ p ]) q) (q [ p ]) [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) $ cons z nil
  ≡⟨ ⇒β  ⟩
  iteListₒ q (cons (q [ p ]) q) (q [ p ]) [ (id ,o cons x (cons y nil)) ⊚ p ,o q ] [ id ,o cons z nil ]
  ≡⟨ cong (_[ id ,o cons z nil ]) iteList[] ⟩
  iteListₒ (q [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) (cons (q [ p ]) q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q ]) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]
  ≡⟨ cong (λ a → iteListₒ a (cons (q [ p ]) q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q ]) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]) ▹βTm ⟩
  iteListₒ q (cons (q [ p ]) q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q ]) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]
  ≡⟨ cong (λ a → iteListₒ q a (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]) cons[] ⟩
  iteListₒ q (cons (q [ p ] [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q ]) (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q ])) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]
  ≡⟨ cong (λ a → iteListₒ q (cons a (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q ])) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]) (sym [∘]) ⟩
  iteListₒ q (cons (q [ p ⊚ ((((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q) ]) (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q ])) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]
  ≡⟨ cong (λ a → iteListₒ q (cons (q [ a ]) (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q ])) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]) ▹βSub ⟩
  iteListₒ q (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ,o q ])) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]
  ≡⟨ cong (λ a → iteListₒ q (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) a) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]) ▹βTm ⟩
  iteListₒ q (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q) (q [ p ] [ (id ,o cons x (cons y nil)) ⊚ p ,o q ]) [ id ,o cons z nil ]
  ≡⟨ cong (λ a → iteListₒ q (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q) a [ id ,o cons z nil ]) (sym [∘]) ⟩
  iteListₒ q (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q) (q [ p ⊚ ((id ,o cons x (cons y nil)) ⊚ p ,o q) ]) [ id ,o cons z nil ]
  ≡⟨ cong (λ a → iteListₒ q (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q) (q [ a ]) [ id ,o cons z nil ]) ▹βSub ⟩
  iteListₒ q (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q) (q [ (id ,o cons x (cons y nil)) ⊚ p ]) [ id ,o cons z nil ]
  ≡⟨ iteList[] ⟩
  iteListₒ (q [ id ,o cons z nil ]) (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ]) (q [ (id ,o cons x (cons y nil)) ⊚ p ] [ id ,o cons z nil ])
  ≡⟨ cong (λ a → iteListₒ a (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ]) (q [ (id ,o cons x (cons y nil)) ⊚ p ] [ id ,o cons z nil ])) ▹βTm ⟩
  iteListₒ 
    (cons z nil) 
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])
    (q [ (id ,o cons x (cons y nil)) ⊚ p ] [ id ,o cons z nil ])
  ≡⟨ cong (iteListₒ (cons z nil) (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])) (sym [∘]) ⟩
  iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])
    (q [ (id ,o cons x (cons y nil)) ⊚ p ⊚ (id ,o cons z nil) ])
  ≡⟨ cong (λ a → iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])
    (q [ a ])) ass ⟩
  iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])
    (q [ (id ,o cons x (cons y nil)) ⊚ (p ⊚ (id ,o cons z nil)) ])
  ≡⟨ cong (λ a → iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])
    (q [ (id ,o cons x (cons y nil)) ⊚ a ])) ▹βSub ⟩
  iteListₒ
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])
    (q [ (id ,o cons x (cons y nil)) ⊚ id ])
  ≡⟨ cong (λ a → iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])
    (q [ a ])) idr ⟩
  iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])
    (q [ id ,o cons x (cons y nil) ])
  ≡⟨ cong (iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])) ▹βTm ⟩
  iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ]) q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ])
    (cons x (cons y nil))
  ≡⟨ cong (λ a → iteListₒ 
    (cons z nil)
    a
    (cons x (cons y nil))) cons[] ⟩
  iteListₒ
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ] [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ]) (q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ]))
    (cons x (cons y nil))
  ≡⟨ cong (λ a → iteListₒ
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ] [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ]) a)
    (cons x (cons y nil))) ▹βTm ⟩
  iteListₒ
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ] [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q ]) q)
    (cons x (cons y nil))
  ≡⟨ cong (λ a → iteListₒ
    (cons z nil)
    (cons a q)
    (cons x (cons y nil))) (sym [∘]) ⟩
  iteListₒ
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ p ⊚ (((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q) ]) q)
    (cons x (cons y nil))
  ≡⟨ cong (λ a → iteListₒ
    (cons z nil)
    (cons (q [ a ]) q)
    (cons x (cons y nil))) ass ⟩
  iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ (p ⊚ (((id ,o cons z nil) ⊚ p ,o q) ⊚ p ,o q)) ]) q)
    (cons x (cons y nil))
  ≡⟨ cong (λ a → iteListₒ 
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ a ]) q)
    (cons x (cons y nil))) ▹βSub ⟩
  iteListₒ
    (cons z nil)
    (cons (q [ (((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q) ⊚ (((id ,o cons z nil) ⊚ p ,o q) ⊚ p) ]) q)
    (cons x (cons y nil))
  ≡⟨ cong (λ a → iteListₒ
    (cons z nil)
    (cons a q)
    (cons x (cons y nil))) [∘] ⟩
  iteListₒ
    (cons z nil)
    (cons (q [ ((id ,o cons x (cons y nil)) ⊚ p ,o q) ⊚ p ,o q ] [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ]) q)
    (cons x (cons y nil))
  ≡⟨ cong (λ a → iteListₒ
    (cons z nil)
    (cons (a [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ]) q)
    (cons x (cons y nil))) ▹βTm ⟩
  iteListₒ
    (cons z nil)
    (cons (q [ ((id ,o cons z nil) ⊚ p ,o q) ⊚ p ]) q)
    (cons x (cons y nil))
  ≡⟨ cong (λ a → iteListₒ
    (cons z nil)
    (cons a q)
    (cons x (cons y nil))) [∘] ⟩
  iteListₒ 
    (cons z nil)
    (cons (q [ (id ,o cons z nil) ⊚ p ,o q ] [ p ]) q)
    (cons x (cons y nil))
  ≡⟨ cong (λ a → iteListₒ 
    (cons z nil)
    (cons (a [ p ]) q)
    (cons x (cons y nil))) ▹βTm ⟩
  iteListₒ (cons z nil) (cons (q [ p ]) q) (cons x (cons y nil))
  ≡⟨ iteListβcons ⟩
  cons (q [ p ]) q [ id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil) ]
  ≡⟨ cons[] ⟩
  cons 
    (q [ p ] [ id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil) ])
    (q [ id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil) ])
  ≡⟨ cong (λ a → cons 
    a
    (q [ id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil) ])) (sym [∘]) ⟩
  cons
    (q [ p ⊚ (id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil)) ])
    (q [ id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil) ])
  ≡⟨ cong (λ a → cons
    (q [ a ])
    (q [ id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil) ])) ▹βSub ⟩
  cons 
    (q [ id ,o x ])
    (q [ id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil) ])
  ≡⟨ cong (λ a → cons 
    a
    (q [ id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil) ])) ▹βTm ⟩
  cons x (q [ id ,o x ,o iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil) ])
  ≡⟨ cong (cons x) ▹βTm ⟩
  cons x (iteListₒ (cons z nil) (cons (q [ p ]) q) (cons y nil))
  ≡⟨ cong (cons x) iteListβcons ⟩
  cons x
    (cons (q [ p ]) q [ id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil ])
  ≡⟨ cong (cons x) cons[] ⟩
  cons x
    (cons (q [ p ] [ id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil ]) (q [ id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil ]))
  ≡⟨ cong (λ a → cons x
    (cons a (q [ id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil ]))) (sym [∘]) ⟩
  cons x
    (cons (q [ p ⊚ (id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil) ]) (q [ id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil ]))
  ≡⟨ cong (λ a → cons x
    (cons (q [ a ]) (q [ id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil ]))) ▹βSub ⟩
  cons x
    (cons (q [ id ,o y ]) (q [ id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil ]))
  ≡⟨ cong (λ a → cons x
    (cons a (q [ id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil ]))) ▹βTm ⟩
  cons x (cons y (q [ id ,o y ,o iteListₒ (cons z nil) (cons (q [ p ]) q) nil ]))
  ≡⟨ cong (cons x ∘ cons y) ▹βTm ⟩
  cons x (cons y (iteListₒ (cons z nil) (cons (q [ p ]) q) nil))
  ≡⟨ cong (cons x ∘ cons y) iteListβnil ⟩
  cons x (cons y (cons z nil)) ∎

St⟦_⟧TSet : Ty → hSet (level 0)
St⟦ A ⇒ B ⟧TSet = (π₁ St⟦ A ⟧TSet → π₁ St⟦ B ⟧TSet) , λ a b x y i j aT → isSet→isSet' (π₂ St⟦ B ⟧TSet)
  (λ j → x j aT)
  (λ j → y j aT)
  (λ i → a aT)
  (λ i → b aT) i j
St⟦ ⊤ₒ ⟧TSet = ⊤ , λ {trivial trivial x y i j → trivial}
St⟦ Bool ⟧TSet = 𝟚 , λ a b x y i j → isSet→isSet' isSetBool
  (λ j → x j)
  (λ j → y j)
  (λ i → a)
  (λ i → b) i j
St⟦ Nat ⟧TSet = ℕ , λ a b x y i j → isSet→isSet' isSetℕ
  (λ j → x j)
  (λ j → y j)
  (λ i → a)
  (λ i → b) i j
St⟦ Listₒ A ⟧TSet = List (π₁ St⟦ A ⟧TSet) , isOfHLevelList 0 (π₂ St⟦ A ⟧TSet)
St⟦ Treeₒ A ⟧TSet = Tree (π₁ St⟦ A ⟧TSet) , isOfHLevelTree 0 (π₂ St⟦ A ⟧TSet)

St⟦_⟧CSet : Con → hSet (level 0)
St⟦ ◆ ⟧CSet = ⊤ , λ {trivial trivial x y i j → trivial}
St⟦ Γ ▹ A ⟧CSet = π₁ St⟦ Γ ⟧CSet × π₁ St⟦ A ⟧TSet , λ a b x y i j → isSet→isSet' (π₂ St⟦ Γ ⟧CSet)
  (λ j → π₁ (x j))
  (λ j → π₁ (y j))
  (λ i → π₁ a)
  (λ i → π₁ b) i j , isSet→isSet' (π₂ St⟦ A ⟧TSet)
  (λ j → π₂ (x j))
  (λ j → π₂ (y j))
  (λ i → π₂ a)
  (λ i → π₂ b) i j

St⟦_⟧S : ∀{Γ Δ} → Sub Γ Δ → π₁ St⟦ Γ ⟧CSet → π₁ St⟦ Δ ⟧CSet
St⟦_⟧t : ∀{Γ A} → Tm Γ A → π₁ St⟦ Γ ⟧CSet → π₁ St⟦ A ⟧TSet

St⟦ ε ⟧S γ = trivial
St⟦ ◆η i ⟧S γ = trivial
St⟦ s ⊚ t ⟧S γ = St⟦ s ⟧S (St⟦ t ⟧S γ)
St⟦ ass {γ = γ'} {δ = δ} {θ = θ} i ⟧S γ = St⟦ γ' ⟧S (St⟦ δ ⟧S (St⟦ θ ⟧S γ))
St⟦ id ⟧S γ = γ
St⟦ idl {γ = γ'} i ⟧S γ = St⟦ γ' ⟧S γ
St⟦ idr {γ = γ'} i ⟧S γ = St⟦ γ' ⟧S γ
St⟦ p ⟧S γ = π₁ γ
St⟦ s ,o x ⟧S γ = (St⟦ s ⟧S γ) , (St⟦ x ⟧t γ)
St⟦ ▹βSub {γ = γ'} i ⟧S γ = St⟦ γ' ⟧S γ
St⟦ ▹η {γa = γa} i ⟧S γ = St⟦ γa ⟧S γ
St⟦_⟧S {Γ} {Δ} (SubSet s t x y i j) γ = isSet→isSet' (π₂ St⟦ Δ ⟧CSet)
  (λ j → St⟦ x j ⟧S γ)
  (λ j → St⟦ y j ⟧S γ)
  (λ i → St⟦ s ⟧S γ)
  (λ i → St⟦ t ⟧S γ) i j

St⟦ q ⟧t γ = π₂ γ
St⟦ t [ x ] ⟧t γ = St⟦ t ⟧t (St⟦ x ⟧S γ)
St⟦ [∘] {t = t} {γ'} {δ} i ⟧t γ = St⟦ t ⟧t (St⟦ γ' ⟧S (St⟦ δ ⟧S γ))
St⟦ [id] {t = t} i ⟧t γ = St⟦ t ⟧t γ
St⟦ ▹βTm {t = t} i ⟧t γ = St⟦ t ⟧t γ
St⟦ lam t ⟧t γ a = St⟦ t ⟧t (γ , a)
St⟦ t $ u ⟧t γ = St⟦ t ⟧t γ (St⟦ u ⟧t γ)
St⟦ ⇒β {t = t} {u} i ⟧t γ = St⟦ t ⟧t (γ , St⟦ u ⟧t γ)
St⟦ ⇒η {t = t} i ⟧t γ = St⟦ t ⟧t γ
St⟦ lam[] {t = t} {γ = γ'} i ⟧t γ a = St⟦ t ⟧t (St⟦ γ' ⟧S γ , a)
St⟦ $[] {t = t} {u} {γ = γ'} i ⟧t γ = St⟦ t ⟧t (St⟦ γ' ⟧S γ) (St⟦ u ⟧t (St⟦ γ' ⟧S γ))
St⟦ trivialₒ ⟧t γ = trivial
St⟦ ⊤η i ⟧t γ = trivial
St⟦ ite⊤ t u ⟧t γ = St⟦ t ⟧t γ
St⟦ ⊤β {t = t} i ⟧t γ = St⟦ t ⟧t γ
St⟦ trivial[] i ⟧t γ = trivial
St⟦ ite⊤[] {u = u} {γ = γ'} i ⟧t γ = St⟦ u ⟧t (St⟦ γ' ⟧S γ)
St⟦ true ⟧t γ = tt
St⟦ false ⟧t γ = ff
St⟦ iteBool t u v ⟧t γ = if St⟦ t ⟧t γ then St⟦ u ⟧t γ else St⟦ v ⟧t γ
St⟦ iteBoolβtrue {u = u} i ⟧t γ = St⟦ u ⟧t γ
St⟦ iteBoolβfalse {v = v} i ⟧t γ = St⟦ v ⟧t γ
St⟦ true[] i ⟧t γ = tt
St⟦ false[] i ⟧t γ = ff
St⟦ iteBool[] {t = t} {u} {v} {_} {γ'} i ⟧t γ = if St⟦ t ⟧t (St⟦ γ' ⟧S γ) then St⟦ u ⟧t (St⟦ γ' ⟧S γ) else St⟦ v ⟧t (St⟦ γ' ⟧S γ)
St⟦ zeroₒ ⟧t γ = 0
St⟦ sucₒ t ⟧t γ = suc (St⟦ t ⟧t γ)
St⟦ iteNat u v t ⟧t γ = iter (St⟦ t ⟧t γ) (λ a → St⟦ v ⟧t (γ , a)) (St⟦ u ⟧t γ)
St⟦ iteNatβ0 {u = u} i ⟧t γ = St⟦ u ⟧t γ
St⟦ iteNatβsuc {u = u} {v} {t} i ⟧t γ = St⟦ v ⟧t (γ , iter (St⟦ t ⟧t γ) (λ a → St⟦ v ⟧t (γ , a)) (St⟦ u ⟧t γ))
St⟦ zero[] i ⟧t γ = 0
St⟦ suc[] {t = t} {γ = γ'} i ⟧t γ = suc (St⟦ t ⟧t (St⟦ γ' ⟧S γ))
St⟦ iteNat[] {u = u} {v} {t} {γ = γ'} i ⟧t γ = iter (St⟦ t ⟧t (St⟦ γ' ⟧S γ)) (λ a → St⟦ v ⟧t (St⟦ γ' ⟧S γ , a)) (St⟦ u ⟧t (St⟦ γ' ⟧S γ))
St⟦ nil ⟧t γ = []
St⟦ cons t ts ⟧t γ = St⟦ t ⟧t γ ∷ St⟦ ts ⟧t γ
St⟦ iteListₒ u v t ⟧t γ = foldr (λ a b → St⟦ v ⟧t ((γ , a) , b)) (St⟦ u ⟧t γ) (St⟦ t ⟧t γ)
St⟦ iteListβnil {u = u} i ⟧t γ = St⟦ u ⟧t γ
St⟦ iteListβcons {u = u} {v} {t} {ts} i ⟧t γ = St⟦ v ⟧t ((γ , St⟦ t ⟧t γ) , foldr (λ a b → St⟦ v ⟧t ((γ , a) , b)) (St⟦ u ⟧t γ) (St⟦ ts ⟧t γ))
St⟦ nil[] i ⟧t γ = []
St⟦ cons[] {t = t} {ts} {_} {γ'} i ⟧t γ = St⟦ t ⟧t (St⟦ γ' ⟧S γ) ∷ St⟦ ts ⟧t (St⟦ γ' ⟧S γ)
St⟦ iteList[] {u = u} {v} {t} {_} {γ'} i ⟧t γ = foldr (λ a b → St⟦ v ⟧t ((St⟦ γ' ⟧S γ , a) , b)) (St⟦ u ⟧t (St⟦ γ' ⟧S γ)) (St⟦ t ⟧t (St⟦ γ' ⟧S γ))
St⟦ leafₒ l ⟧t γ = leaf (St⟦ l ⟧t γ)
St⟦ nodeₒ ll rr ⟧t γ = node (St⟦ ll ⟧t γ) (St⟦ rr ⟧t γ)
St⟦ iteTreeₒ u v t ⟧t γ = iteTree (λ a → St⟦ u ⟧t (γ , a)) (λ a b → St⟦ v ⟧t ((γ , a) , b)) (St⟦ t ⟧t γ)
St⟦ iteTreeβleaf {l = l} {_} {a} i ⟧t γ = St⟦ l ⟧t (γ , St⟦ a ⟧t γ)
St⟦ iteTreeβnode {l = l} {n} {ll} {rr} i ⟧t γ = St⟦ n ⟧t 
  ((γ , iteTree (λ a → St⟦ l ⟧t (γ , a)) (λ a b → St⟦ n ⟧t ((γ , a) , b)) (St⟦ ll ⟧t γ))
  , iteTree (λ a → St⟦ l ⟧t (γ , a)) (λ a b → St⟦ n ⟧t ((γ , a) , b)) (St⟦ rr ⟧t γ))
St⟦ leaf[] {a = a} {_} {γ'} i ⟧t γ = leaf (St⟦ a ⟧t (St⟦ γ' ⟧S γ))
St⟦ node[] {ll = ll} {rr} {_} {γ'} i ⟧t γ = node (St⟦ ll ⟧t (St⟦ γ' ⟧S γ)) (St⟦ rr ⟧t (St⟦ γ' ⟧S γ))
St⟦ iteTree[] {l = l} {n} {t} {_} {γ'} i ⟧t γ = iteTree (λ a → St⟦ l ⟧t (St⟦ γ' ⟧S γ , a)) (λ a b → St⟦ n ⟧t ((St⟦ γ' ⟧S γ , a) , b)) (St⟦ t ⟧t (St⟦ γ' ⟧S γ))
St⟦_⟧t {Γ} {A} (TmSet s t x y i j) γ = isSet→isSet' (π₂ St⟦ A ⟧TSet)
  (λ j → St⟦ x j ⟧t γ)
  (λ j → St⟦ y j ⟧t γ)
  (λ i → St⟦ s ⟧t γ)
  (λ i → St⟦ t ⟧t γ) i j

-- These work in any context Γ
true≠false : ¬ (true {◆} ≡ false)
true≠false e = {!   !}

zero≠suc : ∀{t} → ¬ (zeroₒ {◆} ≡ sucₒ t)
zero≠suc e = {!   !}

t≠suct : ∀{t} → ¬ (t ≡ sucₒ {◆} t)
t≠suct {t} e = {!   !}

module Definability where
  
  ⌜_⌝ : ℕ → Tm ◆ Nat
  ⌜ zero ⌝ = zeroₒ
  ⌜ suc n ⌝ = sucₒ ⌜ n ⌝

  Definable : (ℕ → ℕ) → Set
  Definable f = Σ (Tm ◆ (Nat ⇒ Nat)) λ t → (n : ℕ) → (t $ ⌜ n ⌝ ≡ ⌜ f n ⌝)

  definable2* : Definable (recℕ zero (λ x → suc (suc x)))
  definable2* = (lam (iteNat zeroₒ (sucₒ (sucₒ q)) q)) , λ n →
    lam (iteNat zeroₒ (sucₒ (sucₒ q)) q) $ ⌜ n ⌝
        ≡⟨ ⇒β ⟩
    iteNat zeroₒ (sucₒ (sucₒ q)) q [ id ,o ⌜ n ⌝ ]
        ≡⟨ iteNat[] ⟩
    iteNat (zeroₒ [ id ,o ⌜ n ⌝ ]) (sucₒ (sucₒ q) [ (id ,o ⌜ n ⌝) ⊚ p ,o q ]) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (λ x → iteNat x (sucₒ (sucₒ q) [ (id ,o ⌜ n ⌝) ⊚ p ,o q ]) (q [ id ,o ⌜ n ⌝ ])) zero[] ⟩
    iteNat zeroₒ (sucₒ (sucₒ q) [ (id ,o ⌜ n ⌝) ⊚ p ,o q ]) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (λ x → iteNat zeroₒ  x (q [ id ,o ⌜ n ⌝ ])) suc[] ⟩
    iteNat zeroₒ (sucₒ (sucₒ q [ (id ,o ⌜ n ⌝) ⊚ p ,o q ])) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (λ x → iteNat zeroₒ  (sucₒ x) (q [ id ,o ⌜ n ⌝ ])) suc[] ⟩
    iteNat zeroₒ (sucₒ (sucₒ (q [ (id ,o ⌜ n ⌝) ⊚ p ,o q ]))) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (λ x → iteNat zeroₒ  (sucₒ (sucₒ x)) (q [ id ,o ⌜ n ⌝ ])) ▹βTm ⟩
    iteNat zeroₒ (sucₒ (sucₒ q)) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (iteNat zeroₒ  (sucₒ (sucₒ q))) ▹βTm ⟩
    iteNat zeroₒ (sucₒ (sucₒ q)) ⌜ n ⌝
        ≡⟨ sym (⌜recℕ⌝ n) ⟩
    ⌜ recℕ zero (λ x → suc (suc x)) n ⌝
        ∎
    where
      -- TODO: obtain this from normalisation:
      ⌜recℕ⌝ : (n : ℕ) → ⌜ recℕ zero (λ x → suc (suc x)) n ⌝ ≡ iteNat zeroₒ (sucₒ (sucₒ q)) ⌜ n ⌝
      ⌜recℕ⌝ zero = sym iteNatβ0
      ⌜recℕ⌝ (suc n) =
        sucₒ (sucₒ ⌜ iter n (λ z → suc (suc z)) zero ⌝)
        ≡⟨ cong (sucₒ ∘ sucₒ) (⌜recℕ⌝ n) ⟩
        sucₒ (sucₒ (iteNat zeroₒ (sucₒ (sucₒ q)) ⌜ n ⌝))
        ≡⟨ cong (sucₒ ∘ sucₒ) (sym ▹βTm) ⟩
        sucₒ (sucₒ (q [ id ,o iteNat zeroₒ (sucₒ (sucₒ q)) ⌜ n ⌝ ]))
        ≡⟨ cong sucₒ (sym suc[]) ⟩
        sucₒ (sucₒ q [ id ,o iteNat zeroₒ (sucₒ (sucₒ q)) ⌜ n ⌝ ])
        ≡⟨ sym suc[] ⟩
        sucₒ (sucₒ q) [ id ,o iteNat zeroₒ (sucₒ (sucₒ q)) ⌜ n ⌝ ]
        ≡⟨ sym iteNatβsuc ⟩
        iteNat zeroₒ (sucₒ (sucₒ q)) (sucₒ ⌜ n ⌝) ∎

  Definable' : (ℕ → ℕ → ℕ) → Type
  Definable' f = Σ (Tm ◆ (Nat ⇒ Nat ⇒ Nat)) λ t → (m n : ℕ) → (t $ ⌜ m ⌝ $ ⌜ n ⌝ ≡ ⌜ f m n ⌝)

  postulate
    code : Tm ◆ (Nat ⇒ Nat) → ℕ
    eval : ℕ → ℕ → ℕ
    evalProp : (t : Tm ◆ (Nat ⇒ Nat))(n : ℕ) → t $ ⌜ n ⌝ ≡ ⌜ eval (code t) n ⌝

  evalNotDefinable : Definable' eval → ⊥
  evalNotDefinable (t , f) = t≠suct (
     ⌜ eval (code u) (code u) ⌝
       ≡⟨ sym (evalProp u (code u)) ⟩
     u $ ⌜ code u ⌝
       ≡⟨ ⇒β ⟩
     sucₒ (t [ p ] $ q $ q) [ id ,o ⌜ code u ⌝ ]
       ≡⟨ suc[] ⟩
     sucₒ ((t [ p ] $ q $ q) [ id ,o ⌜ code u ⌝ ])
       ≡⟨ cong sucₒ $[] ⟩
     sucₒ (((t [ p ] $ q) [ id ,o ⌜ code u ⌝ ] $ q [ id ,o ⌜ code u ⌝ ]))
       ≡⟨ cong (λ x → sucₒ (x $ q [ id ,o ⌜ code u ⌝ ])) $[] ⟩
     sucₒ ((t [ p ] [ id ,o ⌜ code u ⌝ ] $ q [ id ,o ⌜ code u ⌝ ] $ q [ id ,o ⌜ code u ⌝ ]))
       ≡⟨ cong (λ x → sucₒ ((t [ p ] [ id ,o ⌜ code u ⌝ ] $ x $ x))) ▹βTm ⟩
     sucₒ ((t [ p ] [ id ,o ⌜ code u ⌝ ] $ ⌜ code u ⌝ $ ⌜ code u ⌝))
       ≡⟨ cong (λ x → sucₒ ((x $ ⌜ code u ⌝ $ ⌜ code u ⌝))) (sym [∘]) ⟩
     sucₒ ((t [ p ⊚ (id ,o ⌜ code u ⌝) ] $ ⌜ code u ⌝ $ ⌜ code u ⌝))
       ≡⟨ cong (λ x → sucₒ ((t [ x ] $ ⌜ code u ⌝ $ ⌜ code u ⌝))) ▹βSub ⟩
     sucₒ ((t [ id ] $ ⌜ code u ⌝ $ ⌜ code u ⌝))
       ≡⟨ cong (λ x → sucₒ ((x $ ⌜ code u ⌝ $ ⌜ code u ⌝))) [id] ⟩
     sucₒ ((t $ ⌜ code u ⌝ $ ⌜ code u ⌝))
       ≡⟨ cong sucₒ (f (code u) (code u)) ⟩
     sucₒ ⌜ eval (code u) (code u) ⌝
       ∎)
    where
      u : Tm ◆ (Nat ⇒ Nat)
      u = lam (sucₒ (t [ p ] $ q $ q))
