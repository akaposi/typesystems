{-# OPTIONS --cubical --guardedness #-}

module Int where

open import Lib

data ℤ : Type where
  Zero : ℤ
  Suc  : ℤ → ℤ
  Pred : ℤ → ℤ
  SucPred : (i : ℤ) → Suc (Pred i) ≡ i
  PredSuc : (i : ℤ) → Pred (Suc i) ≡ i
  ℤSet : isSet ℤ

eq1 : Pred (Suc (Pred Zero)) ≡ Pred Zero
eq1 = PredSuc (Pred Zero)

eq2 : Pred (Suc (Suc (Pred Zero))) ≡ Zero
eq2 =
  Pred (Suc (Suc (Pred Zero)))
  ≡⟨ PredSuc (Suc (Pred Zero)) ⟩
  Suc (Pred Zero)
  ≡⟨ SucPred Zero ⟩ 
  Zero ∎

infixl 6 _+ℤ_

_+ℤ_ : ℤ → ℤ → ℤ
Zero +ℤ j = j
Suc i +ℤ j = Suc (i +ℤ j)
Pred i +ℤ j = Pred (i +ℤ j)
SucPred i i₁ +ℤ j = SucPred (i +ℤ j) i₁
PredSuc i i₁ +ℤ j = PredSuc (i +ℤ j) i₁
ℤSet z z₁ x y i j +ℤ k = isSet→isSet' ℤSet
  (λ j → x j +ℤ k)
  (λ j → y j +ℤ k)
  (λ i → z +ℤ k)
  (λ i → z₁ +ℤ k) i j

test+ : {i : ℤ} → Suc (Suc (Pred Zero)) +ℤ i ≡ Suc (Suc (Pred i))
test+ {i} = refl

test+' : {i : ℤ} → Pred (Suc (Suc (Pred Zero))) +ℤ i ≡ Pred (Suc (Suc (Pred i)))
test+' {i} = refl

test+'' : {i : ℤ} → Suc (Suc (Pred Zero)) +ℤ i ≡ Suc i
test+'' {i} = cong (λ x → Suc x +ℤ i) (SucPred Zero)

data ℤNf : Type where
  -Suc   : ℕ → ℤNf
  Zero   : ℤNf
  +Suc   : ℕ → ℤNf

inj-Suc : ∀ {a b : ℕ} → -Suc a ≡ -Suc b → a ≡ b
inj-Suc {a} h = subst T h refl
  where
  T : ℤNf → Type
  T (-Suc x)    = a ≡ x
  T Zero = ⊥
  T (+Suc _) = ⊥

inj+Suc : ∀ {a b : ℕ} → +Suc a ≡ +Suc b → a ≡ b
inj+Suc {a} h = subst T h refl
  where
  T : ℤNf → Type
  T (-Suc x)    = ⊥
  T Zero = ⊥
  T (+Suc x) = a ≡ x

-SucNot+Suc : ∀ (a b : ℕ) → ¬ (-Suc a ≡ +Suc b)
-SucNot+Suc a b h = subst T h 0
  where
  T : ℤNf → Type
  T (-Suc _) = ℕ
  {-# CATCHALL #-}
  T _        = ⊥

+SucNotZero : ∀ (a : ℕ) → ¬ (+Suc a ≡ Zero)
+SucNotZero a h = subst T h 0
  where
  T : ℤNf → Type
  T (+Suc _)    = ℕ
  {-# CATCHALL #-}
  T _ = ⊥

-SucNotZero : ∀ (a : ℕ) → ¬ (-Suc a ≡ Zero)
-SucNotZero a h = subst T h 0
  where
  T : ℤNf → Type
  T (-Suc _)    = ℕ
  {-# CATCHALL #-}
  T _ = ⊥

discreteℤNf : Discrete ℤNf
discreteℤNf (-Suc x) (-Suc y) with discreteℕ x y
...| yes p = yes (cong -Suc p)
...| no p  = no (p ∘ inj-Suc)
discreteℤNf (-Suc x) Zero = no (-SucNotZero x)
discreteℤNf (-Suc x) (+Suc y) = no (-SucNot+Suc x y)
discreteℤNf Zero (-Suc y) = no (-SucNotZero y ∘ sym)
discreteℤNf Zero Zero = yes refl
discreteℤNf Zero (+Suc y) = no (+SucNotZero y ∘ sym)
discreteℤNf (+Suc x) (-Suc y) = no (-SucNot+Suc y x ∘ sym)
discreteℤNf (+Suc x) Zero = no (+SucNotZero x)
discreteℤNf (+Suc x) (+Suc y) with discreteℕ x y
...| yes p = yes (cong +Suc p)
...| no p  = no (p ∘ inj+Suc)

ℤNfSet : isSet ℤNf
ℤNfSet = Discrete→isSet discreteℤNf

minusThree minusFive plusSix : ℤNf
minusThree  = -Suc 2
minusFive   = -Suc 4
plusSix     = +Suc 5

SucNf : ℤNf → ℤNf
SucNf (-Suc zero)      = Zero
SucNf (-Suc (suc n))   = -Suc n
SucNf Zero             = +Suc 0
SucNf (+Suc n)         = +Suc (suc n)

PredNf : ℤNf → ℤNf
PredNf (-Suc n)        = -Suc (suc n)
PredNf Zero            = -Suc 0
PredNf (+Suc zero)     = Zero
PredNf (+Suc (suc n))  = +Suc n

SucNfPredNf : (z : ℤNf) → SucNf (PredNf z) ≡ z
SucNfPredNf (-Suc x) = refl
SucNfPredNf Zero = refl
SucNfPredNf (+Suc zero) = refl
SucNfPredNf (+Suc (suc x)) = refl

PredNfSucNf : (z : ℤNf) → PredNf (SucNf z) ≡ z
PredNfSucNf (-Suc zero) = refl
PredNfSucNf (-Suc (suc x)) = refl
PredNfSucNf Zero = refl
PredNfSucNf (+Suc x) = refl

Nf⟦_⟧ : ℤ → ℤNf
Nf⟦ Zero ⟧ = Zero
Nf⟦ Suc z ⟧ = SucNf Nf⟦ z ⟧
Nf⟦ Pred z ⟧ = PredNf Nf⟦ z ⟧
Nf⟦ SucPred z i ⟧ = SucNfPredNf Nf⟦ z ⟧ i
Nf⟦ PredSuc z i ⟧ = PredNfSucNf Nf⟦ z ⟧ i
Nf⟦ ℤSet z w x y i j ⟧ = isSet→isSet' ℤNfSet
  (λ j → Nf⟦ x j ⟧)
  (λ j → Nf⟦ y j ⟧)
  (λ i → Nf⟦ z ⟧)
  (λ i → Nf⟦ w ⟧) i j

norm : ℤ → ℤNf
norm = Nf⟦_⟧

⌜_⌝ : ℤNf → ℤ
⌜ -Suc zero     ⌝ = Pred Zero
⌜ -Suc (suc n)  ⌝ = Pred ⌜ -Suc n ⌝
⌜ Zero          ⌝ = Zero
⌜ +Suc zero     ⌝ = Suc Zero
⌜ +Suc (suc n)  ⌝ = Suc ⌜ +Suc n ⌝

testnorm :  ⌜ norm (Pred (Pred (Suc (Pred (Pred (Pred (Suc Zero))))))) ⌝ ≡
            Pred (Pred (Pred Zero))
testnorm = refl

stability : (v : ℤNf) → norm ⌜ v ⌝ ≡ v
stability (-Suc zero)     = refl
stability (-Suc (suc n))  = cong PredNf (stability (-Suc n))
stability Zero            = refl
stability (+Suc zero)     = refl
stability (+Suc (suc n))  = cong SucNf (stability (+Suc n))

⌜Suc⌝ : (v : ℤNf) → ⌜ SucNf v ⌝ ≡ Suc ⌜ v ⌝
⌜Suc⌝ (-Suc zero)      = sym (SucPred Zero)
⌜Suc⌝ (-Suc (suc n))   = sym (SucPred ⌜ -Suc n ⌝)
⌜Suc⌝ Zero             = refl
⌜Suc⌝ (+Suc n)         = refl

⌜Pred⌝ : (v : ℤNf) → ⌜ PredNf v ⌝ ≡ Pred ⌜ v ⌝
⌜Pred⌝ (-Suc n)        = refl
⌜Pred⌝ Zero            = refl
⌜Pred⌝ (+Suc zero)     = sym (PredSuc Zero)
⌜Pred⌝ (+Suc (suc n))  = sym (PredSuc ⌜ +Suc n ⌝)

completeness : (z : ℤ) → ⌜ norm z ⌝ ≡ z
completeness Zero = refl
completeness (Suc z) = ⌜Suc⌝ (Nf⟦ z ⟧) ∙ cong Suc (completeness z)
completeness (Pred z) = ⌜Pred⌝ (Nf⟦ z ⟧) ∙ cong Pred (completeness z)
completeness (SucPred z i) = isSet→isSet' ℤSet
  (λ j → (⌜Suc⌝ (PredNf Nf⟦ z ⟧) ∙ (λ i₁ → Suc ((⌜Pred⌝ Nf⟦ z ⟧ ∙ (λ i₂ → Pred (completeness z i₂))) i₁))) j)
  (λ j → completeness z j)
  (λ i → ⌜ norm (SucPred z i) ⌝)
  (λ i → SucPred z i) i
completeness (PredSuc z i) = isSet→isSet' ℤSet
  (λ j → (⌜Pred⌝ (SucNf Nf⟦ z ⟧) ∙ (λ i₁ → Pred ((⌜Suc⌝ Nf⟦ z ⟧ ∙ (λ i₂ → Suc (completeness z i₂))) i₁))) j)
  (λ j → completeness z j)
  (λ i → ⌜ norm (PredSuc z i) ⌝)
  (λ i → PredSuc z i) i
completeness (ℤSet z w x y i j) = isGroupoid→isGroupoid' (isSet→isGroupoid ℤSet)
  (λ j k → completeness (x j) k)
  (λ j k → completeness (y j) k)
  (λ i k → completeness z k)
  (λ i k → completeness w k)
  (λ i j → ⌜ norm (ℤSet z w x y i j) ⌝)
  (λ i j → ℤSet z w x y i j) i j