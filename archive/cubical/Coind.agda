{-# OPTIONS --cubical --guardedness #-}

module Coind where

open import Lib hiding (id ; fst ; snd)

infixl 6 _⊚_
infixl 6 _[_]'
infixl 6 _[_]
infixl 5 _▹_
infixl 5 _,o_
infixr 5 _⇒_
infixl 5 _$_
infixl 7 _×o_
infixl 5 ⟨_,_⟩

data Ty         : Type where
  _⇒_           : Ty → Ty → Ty
  _×o_          : Ty → Ty → Ty
  Bool          : Ty
  Nat           : Ty
  Stream        : Ty → Ty
  Machine       : Ty

data Con        : Type where
  ◆             : Con
  _▹_           : Con → Ty → Con

data Sub : Con → Con → Type
data Tm : Con → Ty → Type

_[_]' : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
q' : ∀{Γ A} → Tm (Γ ▹ A) A

data Sub where
  ε         : ∀{Γ} → Sub Γ ◆
  ◆η        : ∀{Γ}{σ : Sub Γ ◆} → σ ≡ ε

  _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
  
  id        : ∀{Γ} → Sub Γ Γ
  idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
  idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ
  
  p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
  _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
  ▹βSub     : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
  ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → (((p ⊚ γa) ,o (q' [ γa ]')) ≡ γa)
  SubSet    : ∀{Γ Δ} → isSet (Sub Δ Γ)

data Tm where
  q         : ∀{Γ A} → Tm (Γ ▹ A) A
  _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
  [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
  [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
  ▹βTm      : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → (q [ γ ,o t ]) ≡ t

  lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
  ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} → (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
  $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

  ⟨_,_⟩     : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
  fst       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
  snd       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
  ×β₁       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst ⟨ u , v ⟩ ≡ u
  ×β₂       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd ⟨ u , v ⟩ ≡ v
  ×η        : ∀{Γ A B}{t : Tm Γ (A ×o B)} → ⟨ fst t , snd t ⟩ ≡ t
  ,[]       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                ⟨ u , v ⟩ [ γ ] ≡ ⟨ u [ γ ] , v [ γ ] ⟩
  
  true          : ∀{Γ} → Tm Γ Bool
  false         : ∀{Γ} → Tm Γ Bool
  iteBool       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
  iteBoolβtrue  : ∀{Γ A u v} → iteBool {Γ}{A} true u v ≡ u
  iteBoolβfalse : ∀{Γ A u v} → iteBool {Γ}{A} false u v ≡ v
  true[]        : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
  false[]       : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
  iteBool[]     : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                    iteBool {Γ}{A} t u v [ γ ] ≡ iteBool (t [ γ ]) (u [ γ ]) (v [ γ ])

  zeroₒ      : ∀{Γ} → Tm Γ Nat
  sucₒ       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
  iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
  iteNatβ0   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroₒ ≡ u
  iteNatβsuc : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                iteNat u v (sucₒ t) ≡ v [ id ,o iteNat u v t ]
  zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroₒ [ γ ] ≡ zeroₒ
  suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (sucₒ t) [ γ ] ≡ sucₒ (t [ γ ])
  iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])
  head          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ A
  tail          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ (Stream A)
  genStream     : ∀{Γ A C} → Tm (Γ ▹ C) A → Tm (Γ ▹ C) C → Tm Γ C → Tm Γ (Stream A)
  Streamβhead   : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                  head (genStream u v t) ≡ u [ id ,o t ]
  Streamβtail   : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                  tail (genStream u v t) ≡ genStream u v (v [ id ,o t ])
  head[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} →
                  head as [ γ ] ≡ head (as [ γ ])
  tail[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} → tail as [ γ ] ≡ tail (as [ γ ])
  genStream[]   : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C}{Δ}
                  {γ : Sub Δ Γ} →
                  genStream u v t [ γ ] ≡ genStream (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])

  put           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat → Tm Γ Machine
  set           : ∀{Γ} → Tm Γ Machine → Tm Γ Machine
  get           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat
  genMachine    : ∀{Γ C} → Tm (Γ ▹ C ▹ Nat) C → Tm (Γ ▹ C) C → Tm (Γ ▹ C) Nat →
                  Tm Γ C → Tm Γ Machine
  Machineβput   : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                  {t : Tm Γ C}{t' : Tm Γ Nat} →
                  put (genMachine u v w t) t' ≡ genMachine u v w (u [ id ,o t ,o t' ])
  Machineβset   : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                  {t : Tm Γ C} →
                  set (genMachine u v w t) ≡ genMachine u v w (v [ id ,o t ])
  Machineβget   : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                  {t : Tm Γ C} →
                  get (genMachine u v w t) ≡ w [ id ,o t ]
  put[]         : ∀{Γ}{t : Tm Γ Machine}{u : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                  put t u [ γ ] ≡ put (t [ γ ]) (u [ γ ])
  set[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → set t [ γ ] ≡ set (t [ γ ])
  get[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → get t [ γ ] ≡ get (t [ γ ])
  genMachine[]  : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                  {t : Tm Γ C}{Δ}{γ : Sub Δ Γ} →
                  genMachine u v w t [ γ ] ≡
                  genMachine (u [ (γ ⊚ p ,o q) ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) (w [ γ ⊚ p ,o q ]) (t [ γ ]) 

  TmSet     : ∀{Γ A} → isSet (Tm Γ A)

q' = q
_[_]' = _[_]

def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
def t u = u [ id ,o t ]

v0 : ∀{Γ A}        → Tm (Γ ▹ A) A
v0 = q

v1 : ∀{Γ A B}      → Tm (Γ ▹ A ▹ B) A
v1 = q [ p ]

v2 : ∀{Γ A B C}    → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = q [ p ⊚ p ]

v3 : ∀{Γ A B C D}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = q [ p ⊚ p ⊚ p ]

▹η' : ∀{Γ A} → p ,o q ≡ id {Γ ▹ A}
▹η' {Γ}{A} =
  p ,o q
    ≡⟨ sym (cong₂ _,o_ idr [id]) ⟩
  p ⊚ id ,o q [ id ]
    ≡⟨ ▹η ⟩
  id
    ∎

,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
  (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
  (γ ,o t) ⊚ δ
    ≡⟨ sym ▹η ⟩
  (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
    ≡⟨ cong₂ _,o_ (sym ass) [∘] ⟩
  ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
    ≡⟨ cong₂ _,o_ (cong (_⊚ δ) ▹βSub) (cong (_[ δ ]) ▹βTm) ⟩
  γ ⊚ δ ,o t [ δ ]
    ∎

plus : Tm ◆ (Nat ⇒ Nat ⇒ Nat)
plus = lam (lam (iteNat v0 (sucₒ v0) v1))

numbers : Tm ◆ (Stream Nat)
numbers = genStream v0 (sucₒ v0) zeroₒ

testNumbers0 : head numbers ≡ zeroₒ
testNumbers0 =
  head numbers
    ≡⟨ refl ⟩
  head (genStream q (sucₒ q) zeroₒ)
    ≡⟨ Streamβhead ⟩
  q [ id ,o zeroₒ ]
    ≡⟨ ▹βTm ⟩
  zeroₒ
    ∎

testNumbers2 : head (tail (tail numbers)) ≡ sucₒ (sucₒ zeroₒ)
testNumbers2 = 
  head (tail (tail (genStream q (sucₒ q) zeroₒ)))
  ≡⟨ cong (λ x → head (tail x)) Streamβtail ⟩
  head (tail (genStream q (sucₒ q) (sucₒ q [ id ,o zeroₒ ])))
  ≡⟨ cong (λ x → head (tail (genStream q (sucₒ q) x))) suc[] ⟩
  head (tail (genStream q (sucₒ q) (sucₒ (q [ id ,o zeroₒ ]))))
  ≡⟨ cong (λ x → head (tail (genStream q (sucₒ q) (sucₒ x)))) ▹βTm ⟩
  head (tail (genStream q (sucₒ q) (sucₒ zeroₒ)))
  ≡⟨ cong head Streamβtail ⟩
  head (genStream q (sucₒ q) (sucₒ q [ id ,o sucₒ zeroₒ ]))
  ≡⟨ cong (λ x → head (genStream q (sucₒ q) x)) suc[] ⟩
  head (genStream q (sucₒ q) (sucₒ (q [ id ,o sucₒ zeroₒ ])))
  ≡⟨ cong (λ x → head (genStream q (sucₒ q) (sucₒ x))) ▹βTm ⟩
  head (genStream q (sucₒ q) (sucₒ (sucₒ zeroₒ)))
  ≡⟨ Streamβhead ⟩
  q [ id ,o sucₒ (sucₒ zeroₒ) ]
  ≡⟨ ▹βTm ⟩
  sucₒ (sucₒ zeroₒ) ∎

aMachine : Tm ◆ Machine
  aMachine = genMachine ⟨ fst v1 , plus [ ε ] $ snd v1 $ v0 ⟩
    ⟨ iteBool false true (fst v0) , snd v0 ⟩ (snd v0) ⟨ true , zeroo ⟩