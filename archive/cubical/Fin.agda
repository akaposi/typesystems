{-# OPTIONS --cubical --guardedness #-}

module Fin where

open import Lib hiding (id ; fst ; snd)

infixl 6 _⊚_
infixl 6 _[_]
infixl 6 _[_]'
infixl 5 _▹_
infixl 5 _,o_
infixr 5 _⇒_
infixl 5 _$_
infixl 7 _×o_
infixl 5 ⟨_,_⟩
infixl 6 _⊎o_

data Ty     : Type where
  _⇒_       : Ty → Ty → Ty
  _×o_      : Ty → Ty → Ty
  ⊤ₒ        : Ty
  _⊎o_      : Ty → Ty → Ty
  ⊥ₒ        : Ty

data Con    : Type where
  ◆         : Con
  _▹_       : Con → Ty → Con

data Sub : Con → Con → Type
data Tm : Con → Ty → Type

_[_]' : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
q' : ∀{Γ A} → Tm (Γ ▹ A) A

data Sub where
  ε         : ∀{Γ} → Sub Γ ◆
  ◆η        : ∀{Γ}{σ : Sub Γ ◆} → σ ≡ ε

  _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
  
  id        : ∀{Γ} → Sub Γ Γ
  idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
  idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ
  
  p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
  _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
  ▹βSub     : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
  ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → (((p ⊚ γa) ,o (q' [ γa ]')) ≡ γa)
  SubSet    : ∀{Γ Δ} → isSet (Sub Δ Γ)

data Tm where
  q         : ∀{Γ A} → Tm (Γ ▹ A) A
  _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
  [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
  [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
  ▹βTm      : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → (q [ γ ,o t ]) ≡ t

  lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
  ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} → (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
  $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

  ⟨_,_⟩     : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
  fst       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
  snd       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
  ×β₁       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst ⟨ u , v ⟩ ≡ u
  ×β₂       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd ⟨ u , v ⟩ ≡ v
  ×η        : ∀{Γ A B}{t : Tm Γ (A ×o B)} → ⟨ fst t , snd t ⟩ ≡ t
  ,[]       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                ⟨ u , v ⟩ [ γ ] ≡ ⟨ u [ γ ] , v [ γ ] ⟩

  trivialₒ : ∀{Γ} → Tm Γ ⊤ₒ
  ⊤η        : ∀{Γ}{u : Tm Γ ⊤ₒ} → u ≡ trivialₒ

  inl       : ∀{Γ A B} → Tm Γ A → Tm Γ (A ⊎o B)
  inr       : ∀{Γ A B} → Tm Γ B → Tm Γ (A ⊎o B)
  caseₒ    : ∀{Γ A B C} → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm (Γ ▹ A ⊎o B) C
  ⊎βl       : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{t : Tm Γ A} →
              caseₒ u v [ id ,o inl t ] ≡ u [ id ,o t ]
  ⊎βr       : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{t : Tm Γ B} →
              caseₒ u v [ id ,o inr t ] ≡ v [ id ,o t ]
  ⊎η        : ∀{Γ A B C}{t : Tm (Γ ▹ A ⊎o B) C} →
              caseₒ (t [ p ,o inl q ]) (t [ p ,o inr q ]) ≡ t
  inl[]     : ∀{Γ A B}{t : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
              (inl {B = B} t) [ γ ] ≡ inl (t [ γ ])
  inr[]     : ∀{Γ A B}{t : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
              (inr {A = A} t) [ γ ] ≡ inr (t [ γ ])
  case[]    : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{Δ}{γ : Sub Δ Γ} →
              (caseₒ u v) [ γ ⊚ p ,o q ] ≡
              caseₒ (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ])

  absurd    : ∀{Γ A} → Tm (Γ ▹ ⊥ₒ) A
  ⊥η        : ∀{Γ A}{t : Tm (Γ ▹ ⊥ₒ) A} → t ≡ absurd
  
  TmSet      : ∀{Γ A} → isSet (Tm Γ A)

q' = q
_[_]' = _[_]

caseₒ' : ∀{Γ A B C} → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ (A ⊎o B) → Tm Γ C
caseₒ' l r s = caseₒ l r [ id ,o s ]

absurd' : ∀{Γ A} → Tm Γ ⊥ₒ → Tm Γ A
absurd' e = absurd [ id ,o e ]

def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
def t u = u [ id ,o t ]

v0 : ∀{Γ A}        → Tm (Γ ▹ A) A
v0 = q

v1 : ∀{Γ A B}      → Tm (Γ ▹ A ▹ B) A
v1 = q [ p ]

v2 : ∀{Γ A B C}    → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = q [ p ⊚ p ]

v3 : ∀{Γ A B C D}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = q [ p ⊚ p ⊚ p ]

▹η' : ∀{Γ A} → p ,o q ≡ id {Γ ▹ A}
▹η' {Γ}{A} =
  p ,o q
    ≡⟨ sym (cong2 _,o_ idr [id]) ⟩
  p ⊚ id ,o q [ id ]
    ≡⟨ ▹η ⟩
  id
    ∎

,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
  (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
  (γ ,o t) ⊚ δ
    ≡⟨ sym ▹η ⟩
  (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
    ≡⟨ cong2 _,o_ (sym ass) [∘] ⟩
  ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
    ≡⟨ cong2 _,o_ (cong (_⊚ δ) ▹βSub) (cong (_[ δ ]) ▹βTm) ⟩
  γ ⊚ δ ,o t [ δ ]
    ∎

^∘ : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{Θ}{δ : Sub Θ Δ}{t : Tm Θ A} →
  (γ ⊚ p ,o q) ⊚ (δ ,o t) ≡ (γ ⊚ δ ,o t)
^∘ {Γ}{Δ}{γ}{A}{Θ}{δ}{t} =
  (γ ⊚ p ,o q) ⊚ (δ ,o t)
    ≡⟨ ,∘ ⟩
  (γ ⊚ p ⊚ (δ ,o t) ,o q [ δ ,o t ])
    ≡⟨ cong (λ x → (x ,o q [ δ ,o t ])) ass ⟩
  (γ ⊚ (p ⊚ (δ ,o t)) ,o q [ δ ,o t ])
    ≡⟨ cong (λ x → (γ ⊚ x ,o q [ δ ,o t ])) ▹βSub ⟩
  (γ ⊚ δ ,o q [ δ ,o t ])
    ≡⟨ cong (λ x → γ ⊚ δ ,o x) ▹βTm ⟩
  (γ ⊚ δ ,o t)
    ∎
  
fst[] : ∀{Γ Δ A B}{t : Tm Γ (A ×o B)}{γ : Sub Δ Γ} →
    (fst t) [ γ ] ≡ fst (t [ γ ])
fst[] {Γ}{Δ}{A}{B}{t}{γ} =
  fst t [ γ ]
    ≡⟨ sym ×β₁ ⟩
  fst ⟨ fst t [ γ ] , snd t [ γ ] ⟩
    ≡⟨ cong fst (sym ,[]) ⟩
  fst (⟨ fst t , snd t ⟩ [ γ ])
    ≡⟨ cong (λ x → fst (x [ γ ])) ×η ⟩
  fst (t [ γ ]) ∎

snd[] : ∀{Γ Δ A B}{t : Tm Γ (A ×o B)}{γ : Sub Δ Γ} →
  (snd t) [ γ ] ≡ snd (t [ γ ])
snd[] {Γ}{Δ}{A}{B}{t}{γ} =
  snd t [ γ ]
    ≡⟨ sym ×β₂ ⟩
  snd ⟨ fst t [ γ ] , snd t [ γ ] ⟩
    ≡⟨ cong snd (sym ,[]) ⟩
  snd (⟨ fst t , snd t ⟩ [ γ ])
    ≡⟨ cong (λ x → snd (x [ γ ])) ×η ⟩
  snd (t [ γ ]) ∎

trivial[] : ∀{Γ Δ}{γ : Sub Δ Γ} → trivialₒ [ γ ] ≡ trivialₒ
trivial[] = ⊤η

⊎β₁' : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{Δ}{γ : Sub Δ Γ}{t : Tm Δ A} →
  caseₒ u v [ γ ,o inl t ] ≡ u [ γ ,o t ]
⊎β₁' {Γ}{A}{B}{C}{u}{v}{Δ}{γ}{t} =
  caseₒ u v [ γ ,o inl t ]
    ≡⟨ cong (λ x → caseₒ u v [ x ,o inl t ]) (sym idr) ⟩
  caseₒ u v [ (γ ⊚ id ,o inl t) ]
    ≡⟨ cong (caseₒ u v [_]) (sym ^∘)  ⟩
  caseₒ u v [ (γ ⊚ p ,o q) ⊚ (id ,o inl t) ]
    ≡⟨ [∘] ⟩
  caseₒ u v [ γ ⊚ p ,o q ] [ id ,o inl t ]
    ≡⟨ cong (_[ id ,o inl t ]) case[] ⟩
  caseₒ (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) [ id ,o inl t ]
    ≡⟨ ⊎βl ⟩
  u [ γ ⊚ p ,o q ] [ id ,o t ]
    ≡⟨ sym [∘] ⟩
  u [ (γ ⊚ p ,o q) ⊚ (id ,o t) ]
    ≡⟨ cong (u [_]) ^∘ ⟩
  u [ γ ⊚ id ,o t ]
    ≡⟨ cong (λ x → u [ x ,o t ]) idr ⟩
  u [ γ ,o t ]
    ∎

⊎β₂' : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{Δ}{γ : Sub Δ Γ}{t : Tm Δ B} →
  caseₒ u v [ γ ,o inr t ] ≡ v [ γ ,o t ]
⊎β₂' {Γ}{A}{B}{C}{u}{v}{Δ}{γ}{t} =
  caseₒ u v [ γ ,o inr t ]
    ≡⟨ cong (λ x → caseₒ u v [ x ,o inr t ]) (sym idr) ⟩
  caseₒ u v [ (γ ⊚ id ,o inr t) ]
    ≡⟨ cong (caseₒ u v [_]) (sym ^∘)  ⟩
  caseₒ u v [ (γ ⊚ p ,o q) ⊚ (id ,o inr t) ]
    ≡⟨ [∘] ⟩
  caseₒ u v [ γ ⊚ p ,o q ] [ id ,o inr t ]
    ≡⟨ cong (_[ id ,o inr t ]) case[] ⟩
  caseₒ (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) [ id ,o inr t ]
    ≡⟨ ⊎βr ⟩
  v [ γ ⊚ p ,o q ] [ id ,o t ]
    ≡⟨ sym [∘] ⟩
  v [ (γ ⊚ p ,o q) ⊚ (id ,o t) ]
    ≡⟨ cong (v [_]) ^∘ ⟩
  v [ γ ⊚ id ,o t ]
    ≡⟨ cong (λ x → v [ x ,o t ]) idr ⟩
  v [ γ ,o t ]
    ∎

absurd[] : ∀{Γ A}{Δ}{γ : Sub Δ Γ} → absurd {Γ}{A} [ γ ⊚ p ,o q ] ≡ absurd
absurd[] = ⊥η

𝟛o : Ty
𝟛o = ⊤ₒ ⊎o (⊤ₒ ⊎o ⊤ₒ)
three0 three1 three2 : ∀{Γ} → Tm Γ 𝟛o
three0 = inl trivialₒ
three1 = inr (inl trivialₒ)
three2 = inr (inr trivialₒ)

case𝟛 : ∀{Γ A} → Tm Γ 𝟛o → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A
case𝟛 t u v w = caseₒ (u [ p ]) (caseₒ (v [ p ]) (w [ p ])) [ id ,o t ]
Threeβ₀ : ∀{Γ A}{u v w : Tm Γ A} → case𝟛 three0 u v w ≡ u
Threeβ₁ : ∀{Γ A}{u v w : Tm Γ A} → case𝟛 three1 u v w ≡ v
Threeβ₂ : ∀{Γ A}{u v w : Tm Γ A} → case𝟛 three2 u v w ≡ w
Threeβ₀ {Γ}{a}{u}{v}{w} =
  case𝟛 three0 u v w
                          ≡⟨ refl ⟩
  caseₒ (u [ p ]) (caseₒ (v [ p ]) (w [ p ])) [ id ,o inl trivialₒ ]
                          ≡⟨ ⊎βl ⟩
  u [ p ] [ id ,o trivialₒ ]
                          ≡⟨ sym [∘] ⟩
  u [ p ⊚ (id ,o trivialₒ) ]
                          ≡⟨ cong (u [_]) ▹βSub ⟩
  u [ id ]
                          ≡⟨ [id] ⟩
  u
                          ∎

Threeβ₁ = exercise
Threeβ₂ = exercise

plus3 : Tm ◆ (𝟛o ⇒ 𝟛o ⇒ 𝟛o)
plus3 = lam (lam (case𝟛 v1
  {- v1=0 -} v0
  {- v1=1 -} (case𝟛 v0 three1 three2 three0)
  {- v1=2 -} (case𝟛 v0 three2 three0 three1)))

plus3test00 : plus3 $ three0 $ three0 ≡ three0
plus3test11 : plus3 $ three1 $ three1 ≡ three2
plus3test12 : plus3 $ three1 $ three2 ≡ three0
plus3test00 = exercise
plus3test11 = exercise
plus3test12 = exercise

Bool : Ty
Bool = ⊤ₒ ⊎o ⊤ₒ

true : ∀{Γ} → Tm Γ Bool
true = inl trivialₒ

false : ∀{Γ} → Tm Γ Bool
false = inr trivialₒ

ite : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
ite b u v = caseₒ (u [ p ]) (v [ p ]) [ id ,o b ]

iteβtrue : ∀{Γ A}{u v : Tm Γ A} → ite true u v ≡ u
iteβtrue {Γ}{A}{u}{v} =
  ite true u v
    ≡⟨ refl ⟩
  caseₒ (u [ p ]) (v [ p ]) [ id ,o inl trivialₒ ]
    ≡⟨ ⊎βl ⟩
  u [ p ] [ id ,o trivialₒ ]
    ≡⟨ sym [∘] ⟩
  u [ p ⊚ (id ,o trivialₒ) ]
    ≡⟨ cong (u [_]) ▹βSub ⟩
  u [ id ]
    ≡⟨ [id] ⟩
  u
    ∎

iteβfalse : ∀{Γ A}{u v : Tm Γ A} → ite false u v ≡ v
iteβfalse {Γ}{A}{u}{v} =
  ite false u v
    ≡⟨ refl ⟩
  caseₒ (u [ p ]) (v [ p ]) [ id ,o inr trivialₒ ]
    ≡⟨ ⊎βr ⟩
  v [ p ] [ id ,o trivialₒ ]
    ≡⟨ sym [∘] ⟩
  v [ p ⊚ (id ,o trivialₒ) ]
    ≡⟨ cong (v [_]) ▹βSub ⟩
  v [ id ]
    ≡⟨ [id] ⟩
  v
    ∎

Maybeₒ : Ty → Ty
Maybeₒ A = A ⊎o ⊤ₒ

nothingₒ : ∀{Γ A} → Tm Γ (Maybeₒ A)
nothingₒ = inr trivialₒ

justₒ : ∀{Γ A} → Tm Γ A → Tm Γ (Maybeₒ A)
justₒ t = inl t

Finₒ : ℕ → Ty
Finₒ zero    = ⊤ₒ
Finₒ (suc n) = ⊤ₒ ⊎o Finₒ n

Tys : ℕ → Type
Tys zero     = ⊤
Tys (suc n)  = Ty × Tys n

Prod : {n : ℕ} → Tys n → Ty
Prod {zero}  _         = ⊤ₒ
Prod {suc n} (A , As)  = A ×o Prod As

Tms : Con → {n : ℕ} → Tys n → Type
Tms Γ {zero}  _         = ⊤
Tms Γ {suc n} (A , As)  = Tm Γ A × Tms Γ As

tpl : ∀{Γ n}{As : Tys n} → Tms Γ As → Tm Γ (Prod As)
tpl {Γ} {zero}   _         = trivialₒ
tpl {Γ} {suc n}  (t , ts)  = ⟨ t , tpl ts ⟩

{-
-- Cubical.Data.Fin, where Fin is a Σ-type: Fin n = Σ ℕ (λ k → k < n)
-- unusable like this
_!_ : ∀{n} → Tys n → Fin n → Ty
_!_ {zero} t (x , y , pr) = exfalso (snotz (subst (λ n → suc x + y ≡ n) pr (+-comm (suc x) y)))
_!_ {suc zero} (ty , tys) (x , y , pr) = ty
_!_ {suc (suc n)} (ty , tys) (zero , zero , pr) = exfalso (znots (injSuc pr))
_!_ {suc (suc n)} (ty , tys) (zero , suc y , pr) = tys ! (zero , y , injSuc pr)
_!_ {suc (suc n)} (ty , tys) (suc x , y , pr) = tys ! (x , y , injSuc (subst (λ m → m ≡ suc (suc n)) (+-suc y (suc x)) pr))
-}

_!_ : ∀{n} → Tys n → Fin n → Ty
(t , ts) ! zero = t
(t , ts) ! suc i = ts ! i

prj : ∀{Γ n}{As : Tys n}(i : Fin n) → Tm Γ (Prod As) → Tm Γ (As ! i)
prj zero     t = fst t
prj (suc i)  t = prj i (snd t)

record _≅_ (A B : Ty) : Type where
  field
    l→r   : Tm (◆ ▹ A) B
    r→l   : Tm (◆ ▹ B) A
    l→r→l : l→r [ p ,o r→l ] ≡ q
    r→l→r : r→l [ p ,o l→r ] ≡ q

open _≅_
infix 4 _≅_

⊎idr : ∀{A} → A ⊎o ⊥ₒ ≅ A
l→r  (⊎idr {A}) = caseₒ q absurd
r→l  (⊎idr {A}) = inl q
l→r→l (⊎idr {A}) =
  caseₒ q absurd [ p ,o inl q ]
    ≡⟨ ⊎β₁' ⟩
  q [ p ,o q ]
    ≡⟨ ▹βTm ⟩
  q
    ∎
r→l→r (⊎idr {A}) =
  inl q [ p ,o caseₒ q absurd ]
    ≡⟨ inl[] ⟩
  inl (q [ p ,o caseₒ q absurd ])
    ≡⟨ cong inl ▹βTm ⟩
  inl (caseₒ q absurd)
    ≡⟨ sym ⊎η ⟩
  caseₒ (inl (caseₒ q absurd) [ p ,o inl q ]) (inl (caseₒ q absurd) [ p ,o inr q ])
    ≡⟨ cong (λ x → caseₒ x (inl (caseₒ q absurd) [ p ,o inr q ])) inl[] ⟩
  caseₒ (inl (caseₒ q absurd [ p ,o inl q ])) (inl (caseₒ q absurd) [ p ,o inr q ])
    ≡⟨ cong (λ x → caseₒ (inl (caseₒ q absurd [ p ,o inl q ])) x) inl[] ⟩
  caseₒ (inl (caseₒ q absurd [ p ,o inl q ])) (inl (caseₒ q absurd [ p ,o inr q ]))
    ≡⟨ cong (λ x → caseₒ (inl {B = ⊥ₒ} x) (inl (caseₒ q absurd [ p ,o inr q ]))) ⊎β₁' ⟩
  caseₒ (inl (q [ p ,o q ]))                  (inl (caseₒ q absurd [ p ,o inr q ]))
    ≡⟨ cong (λ x → caseₒ (inl {B = ⊥ₒ} x) (inl (caseₒ q absurd [ p ,o inr q ]))) ▹βTm ⟩
  caseₒ (inl q)                               (inl (caseₒ q absurd [ p ,o inr q ]))
    ≡⟨ cong (λ x → caseₒ (inl q) x) ⊥η ⟩
  caseₒ (inl q) absurd
    ≡⟨ cong (λ x → caseₒ (inl q) x) (sym ⊥η) ⟩
  caseₒ (inl q) (inr q)
    ≡⟨ cong (λ x → caseₒ (inl q) x) (sym ▹βTm) ⟩
  caseₒ (inl q) (q [ p ,o inr q ])
    ≡⟨ cong (λ x → caseₒ x (q [ p ,o inr q ])) (sym ▹βTm) ⟩
  caseₒ (q [ p ,o inl q ]) (q [ p ,o inr q ])
    ≡⟨ ⊎η ⟩
  q
    ∎

×Prop : ∀{ℓ}{A B : Type ℓ} → isProp A → isProp B → isProp (A × B)
×Prop pA pB (a , b) (c , d) = ≡-× (pA a c) (pB b d)

inj× : ∀{ℓ ℓ'}{A : Type ℓ}{B : Type ℓ'}(a : A)(b : B)(c : A)(d : B) → (a , b) ≡ (c , d) → (a ≡ c) × (b ≡ d)
inj× {ℓ} {ℓ'} {A} {B} a b c d pr = subst T pr (refl , refl)
  where
  T : (A × B) → Type (ℓ-max ℓ ℓ')
  T (x , y) = (a ≡ x) × (b ≡ y)

×Ση : ∀{ℓ ℓ'}{A : Type ℓ} {B : Type ℓ'} (x : A × B) → x ≡ ((π₁ x) , (π₂ x))
×Ση (x , x₁) = refl

×ΣT : ∀{ℓ ℓ'}{A : Type ℓ} {B : Type ℓ'}{x y : A × B} → x ≡ y → (π₁ x ≡ π₁ y) × (π₂ x ≡ π₂ y)
×ΣT {x = x} {y = y} pr = inj× (π₁ x) (π₂ x) (π₁ y) (π₂ y) pr

St⟦_⟧TSet : Ty → hSet (level 0)
St⟦ A ⇒ B ⟧TSet = (π₁ St⟦ A ⟧TSet → π₁ St⟦ B ⟧TSet) , λ a b x y i j aT → isSet→isSet' (π₂ St⟦ B ⟧TSet)
  (λ j → x j aT)
  (λ j → y j aT)
  (λ i → a aT)
  (λ i → b aT) i j
St⟦ A ×o B ⟧TSet = π₁ St⟦ A ⟧TSet × π₁ St⟦ B ⟧TSet , λ a b x y i j → isSet→isSet' (π₂ St⟦ A ⟧TSet)
  (λ j → π₁ (x j))
  (λ j → π₁ (y j))
  (λ i → π₁ a)
  (λ i → π₁ b) i j , isSet→isSet' (π₂ St⟦ B ⟧TSet)
  (λ j → π₂ (x j))
  (λ j → π₂ (y j))
  (λ i → π₂ a)
  (λ i → π₂ b) i j
St⟦ ⊤ₒ ⟧TSet = ⊤ , λ {trivial trivial x y i j → trivial}
St⟦ A ⊎o B ⟧TSet = π₁ St⟦ A ⟧TSet ⊎ π₁ St⟦ B ⟧TSet , λ a b x y i j → (isSet→isSet' (isSet⊎ (π₂ St⟦ A ⟧TSet) (π₂ St⟦ B ⟧TSet))
  (λ j → x j)
  (λ j → y j)
  (λ i → a)
  (λ i → b) i j)
St⟦ ⊥ₒ ⟧TSet = ⊥ , λ {() () x y i j}

St⟦_⟧CSet : Con → hSet (level 0)
St⟦ ◆ ⟧CSet = ⊤ , λ{trivial trivial x y i j → trivial}
St⟦ Γ ▹ t ⟧CSet = π₁ St⟦ Γ ⟧CSet × π₁ St⟦ t ⟧TSet , λ a b x y i j → isSet→isSet' (π₂ St⟦ Γ ⟧CSet)
  (λ j → π₁ (x j))
  (λ j → π₁ (y j))
  (λ i → π₁ a)
  (λ i → π₁ b) i j , isSet→isSet' (π₂ St⟦ t ⟧TSet)
  (λ j → π₂ (x j))
  (λ j → π₂ (y j))
  (λ i → π₂ a)
  (λ i → π₂ b) i j

St⟦_⟧S : ∀{Γ Δ} → Sub Γ Δ → π₁ St⟦ Γ ⟧CSet → π₁ St⟦ Δ ⟧CSet
St⟦_⟧t : ∀{Γ A} → Tm Γ A → π₁ St⟦ Γ ⟧CSet → π₁ St⟦ A ⟧TSet

St⟦ ε ⟧S γ = trivial
St⟦ ◆η i ⟧S γ = trivial
St⟦ s ⊚ t ⟧S γ = St⟦ s ⟧S (St⟦ t ⟧S γ)
St⟦ ass {γ = γ₁} {δ} {θ} i ⟧S γ = St⟦ γ₁ ⟧S (St⟦ δ ⟧S (St⟦ θ ⟧S γ))
St⟦ id ⟧S γ = γ
St⟦ idl {γ = γ₁} i ⟧S γ = St⟦ γ₁ ⟧S γ
St⟦ idr {γ = γ₁} i ⟧S γ = St⟦ γ₁ ⟧S γ
St⟦ p ⟧S γ = π₁ γ
St⟦ s ,o x ⟧S γ = St⟦ s ⟧S γ , St⟦ x ⟧t γ
St⟦ ▹βSub {γ = γ₁} i ⟧S γ = St⟦ γ₁ ⟧S γ
St⟦ ▹η {γa = γa} i ⟧S γ = St⟦ γa ⟧S γ
St⟦_⟧S {Γ} {Δ} (SubSet s t x y i j) γ = isSet→isSet' (π₂ St⟦ Δ ⟧CSet)
 (λ j → St⟦ x j ⟧S γ)
 (λ j → St⟦ y j ⟧S γ)
 (λ i → St⟦ s ⟧S γ)
 (λ i → St⟦ t ⟧S γ) i j

St⟦ q ⟧t γ = π₂ γ
St⟦ t [ x ] ⟧t γ = St⟦ t ⟧t (St⟦ x ⟧S γ)
St⟦ [∘] {t = t} {γ = γ'} {δ = δ} i ⟧t γ = St⟦ t ⟧t (St⟦ γ' ⟧S (St⟦ δ ⟧S γ))
St⟦ [id] {t = t} i ⟧t γ = St⟦ t ⟧t γ
St⟦ ▹βTm {t = t} i ⟧t γ = St⟦ t ⟧t γ
St⟦ lam t ⟧t γ = λ u → St⟦ t ⟧t (γ , u)
St⟦ t $ u ⟧t γ = St⟦ t ⟧t γ (St⟦ u ⟧t γ)
St⟦ ⇒β {t = t} {u = u} i ⟧t γ = St⟦ t ⟧t (γ , St⟦ u ⟧t γ)
St⟦ ⇒η {t = t} i ⟧t γ = St⟦ t ⟧t γ
St⟦ lam[] {t = t} {γ = γ'} i ⟧t γ = λ u → St⟦ t ⟧t (St⟦ γ' ⟧S γ , u)
St⟦ $[] {t = t} {u = u} {γ = γ'} i ⟧t γ = St⟦ t ⟧t (St⟦ γ' ⟧S γ) (St⟦ u ⟧t (St⟦ γ' ⟧S γ))
St⟦ ⟨ t , u ⟩ ⟧t γ = (St⟦ t ⟧t γ) , (St⟦ u ⟧t γ)
St⟦ fst t ⟧t γ = π₁ (St⟦ t ⟧t γ)
St⟦ snd t ⟧t γ = π₂ (St⟦ t ⟧t γ)
St⟦ ×β₁ {u = u} i ⟧t γ = St⟦ u ⟧t γ
St⟦ ×β₂ {v = v} i ⟧t γ = St⟦ v ⟧t γ
St⟦ ×η {t = t} i ⟧t γ = St⟦ t ⟧t γ
St⟦ ,[] {u = u} {v = v} {γ = γ'} i ⟧t γ = St⟦ u ⟧t (St⟦ γ' ⟧S γ) , St⟦ v ⟧t (St⟦ γ' ⟧S γ)
St⟦ trivialₒ ⟧t γ = trivial
St⟦ ⊤η i ⟧t γ = trivial
St⟦ inl t ⟧t γ = ι₁ (St⟦ t ⟧t γ)
St⟦ inr t ⟧t γ = ι₂ (St⟦ t ⟧t γ)
St⟦ caseₒ t u ⟧t γ = case (λ x → St⟦ t ⟧t (π₁ γ , x)) (λ x → St⟦ u ⟧t (π₁ γ , x)) (π₂ γ)
St⟦ ⊎βl {u = u} {t = t} i ⟧t γ = St⟦ u ⟧t (γ , St⟦ t ⟧t γ)
St⟦ ⊎βr {v = v} {t = t} i ⟧t γ = St⟦ v ⟧t (γ , St⟦ t ⟧t γ)
St⟦ ⊎η {A = A} {B} {C} {t = t} i ⟧t (γ , ι₁ x) = St⟦ t ⟧t (γ , ι₁ x)
St⟦ ⊎η {A = A} {B} {C} {t = t} i ⟧t (γ , ι₂ x) = St⟦ t ⟧t (γ , ι₂ x)
St⟦ inl[] {t = t} {γ = γ'} i ⟧t γ = ι₁ (St⟦ t ⟧t (St⟦ γ' ⟧S γ))
St⟦ inr[] {t = t} {γ = γ'} i ⟧t γ = ι₂ (St⟦ t ⟧t (St⟦ γ' ⟧S γ))
St⟦ case[] {u = u} {v = v} {γ = γ'} i ⟧t γ = case (λ x → St⟦ u ⟧t (St⟦ γ' ⟧S (π₁ γ) , x)) (λ x → St⟦ v ⟧t (St⟦ γ' ⟧S (π₁ γ) , x)) (π₂ γ)
St⟦ TmSet {Γ} {A} t u x y i j ⟧t γ = isSet→isSet' (π₂ St⟦ A ⟧TSet)
  (λ j → St⟦ x j ⟧t γ)
  (λ j → St⟦ y j ⟧t γ)
  (λ i → St⟦ t ⟧t γ)
  (λ i → St⟦ u ⟧t γ) i j 

{-
caseo∘distr : ∀{f u v} → f ∘ caseₒ u v ≡ caseₒ (f ∘ u) (f ∘ v)
caseo∘distr {f} {u} {v} = ?
-}