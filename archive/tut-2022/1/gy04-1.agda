{-# OPTIONS --prop --rewriting #-}

module gy04-1 where

open import Lib
open import NatBoolAST

{-
dependens modellek
-}

L1' : Model
L1' = record
  { Tm     = ℕ
  ; true   = 1
  ; false  = 1
  ; ite    = λ t t' t'' → t + t' + t''
  ; num    = λ _ → 1
  ; isZero = λ t → t
  ; _+o_   = _+_
  }
module L1' = Model L1'
L2' : Model
L2' = record
  { Tm     = ℕ
  ; true   = 0
  ; false  = 0
  ; ite    = λ t t' t'' → 2 + t + t' + t''
  ; num    = λ _ → 0
  ; isZero = λ t → t
  ; _+o_   = λ t t' → 1 + t + t'
  }
module L2' = Model L2'

-- FELADAT: Bizonyítsd, hogy minden szintaxisbeli termre L1 modell ugyanazt csinálja, mint 1 + L2.
L1L2 : DepModel {lzero}
L1L2 = record
  { Tm∙ = λ t → Lift (L1'.⟦ t ⟧ ≡ 1 + L2'.⟦ t ⟧)
  ; true∙ = mk refl
  ; false∙ = mk refl
  ; ite∙ = λ { {t} {u} {v} (mk a) (mk b) (mk c) → mk (
      L1'.⟦ t ⟧ + L1'.⟦ u ⟧ + L1'.⟦ v ⟧
      ≡⟨ cong₃ (λ x y z → x + y + z) a b c ⟩
      suc ((L2'.⟦ t ⟧ + suc L2'.⟦ u ⟧) + suc L2'.⟦ v ⟧)
      ≡⟨ cong (λ x → suc x) +suc ⟩
      suc (suc (L2'.⟦ t ⟧ + suc L2'.⟦ u ⟧) + L2'.⟦ v ⟧)
      ≡⟨ cong (λ x → suc (suc x + L2'.⟦ v ⟧)) +suc ⟩
      suc (suc (suc (L2'.⟦ t ⟧ + L2'.⟦ u ⟧)) + L2'.⟦ v ⟧)
      ∎)}
  ; num∙ = λ _ → mk refl
  ; isZero∙ = λ ih → ih
  ; _+o∙_ = λ { {u} {v} (mk a) (mk b) → mk (
      L1'.⟦ u ⟧ + L1'.⟦ v ⟧
      ≡⟨ cong (λ x → x + L1'.⟦ v ⟧) a ⟩
      suc L2'.⟦ u ⟧ + L1'.⟦ v ⟧
      ≡⟨ cong (λ x → suc L2'.⟦ u ⟧ + x) b ⟩
      suc L2'.⟦ u ⟧ + suc L2'.⟦ v ⟧
      ≡⟨ refl ⟩
      suc (L2'.⟦ u ⟧ + suc L2'.⟦ v ⟧) -- L2'.⟦ u ⟧ + suc L2'.⟦ v ⟧ ≡ suc (L2'.⟦ u ⟧ + L2'.⟦ v ⟧)
      ≡⟨ cong (λ x → suc x) +suc ⟩
      suc (suc (L2'.⟦ u ⟧ + L2'.⟦ v ⟧))
      ∎ ) } -- \== : ≡ | \< : ⟨  \> : ⟩ | \qed : ∎
  }

module L1L2 = DepModel L1L2

twolengths : ∀ t → L1'.⟦ t ⟧ ≡ 1 + L2'.⟦ t ⟧
twolengths t = un L1L2.⟦ t ⟧

module isZeroInjectivity where

  D' : DepModel {lzero}
  D' = record
    { Tm∙     = λ _ → I.Tm
    ; true∙   = I.true
    ; false∙  = I.false
    ; ite∙    = I.ite
    ; num∙    = I.num
    ; isZero∙ = λ {t} t' → t
    ; _+o∙_   = I._+o_
    }
  module D' = DepModel D'

  isZeroInj : ∀{t t'} → I.isZero t ≡ I.isZero t' → t ≡ t'
  isZeroInj e = cong D'.⟦_⟧ e

module isZeroInjective where

  -- Az iniciális modellben most már tudjuk, hogy az isZero injektív. Adj meg egy másik modellt, amelyben szintén injektív!
  M : Model {lzero}
  M = record
    { Tm     = ℕ
    ; true   = {!!}
    ; false  = {!!}
    ; ite    = {!!}
    ; num    = {!!}
    ; isZero = {!!}
    ; _+o_   = {!!}
    }
  open Model M

  inj : ∀ t t' → isZero t ≡ isZero t' → t ≡ t'
  inj = {!!}

module isZeroNotInjective where

  -- Adj meg egy modellt, amelyben az isZero operátor nem injektív!
  M : Model {lzero}
  M = {!   !}
  open Model M

  notInj : ¬ (∀ t t' → isZero t ≡ isZero t' → t ≡ t')
  notInj inj = {!!}

-- FELADAT: mutasd meg, hogy a _+_ operátor mindkét paraméterében injektív! (a módszer ugyanaz, mint isZero-nál)
module +Injectivity where
  open I

  D' : DepModel {lzero}
  D' = {!!}

  +inj : ∀{u₀ u₁ v₀ v₁} → u₀ + v₀ ≡ u₁ + v₁ → Lift (u₀ ≡ u₁) × Lift (v₀ ≡ v₁)
  +inj = {!!}