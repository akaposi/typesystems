{-# OPTIONS --prop --rewriting #-}

module gy03-1 where

open import Lib
open import NatBoolAST hiding (T)

tm1 : I.Tm
tm1 = ite true (_+o_ (num 1) (num 3)) (isZero (isZero false))
  where open I

-- FEL: do all the compulsory exercises from the book!

-- FEL: count the number of trues in a term (exercise 1.10 in the book)
Trues : Model {lzero}
Trues = record 
  { Tm = ℕ
  ; true = 1
  ; false = 0
  ; ite = λ a b c → a + b + c
  ; num = λ _ → 0
  ; isZero = λ n → n
  ; _+o_ = _+_
  }

module testTrues where
  module M = Model Trues

  t : M.Tm
  t = M.true

  t' : M.Tm
  t' = 10

  test1 : M.⟦ I.false I.+o (I.num 1) ⟧ ≡ 0
  test1 = refl
  test2 : M.⟦ tm1 ⟧ ≡ 1
  test2 = refl
  test3 : M.⟦ I.ite I.true I.true I.true ⟧ ≡ 3
  test3 = refl

  -- FEL: adj meg ket kulonbozo termet, melyek ertelmezese 2
  t2 t3 : I.Tm
  t2 = I.true I.+o I.true
  t3 = I.true I.+o I.false I.+o I.true
  t2≠t3 : ¬ (t2 ≡ t3)
  t2≠t3 ()
  testt2 : M.⟦ t2 ⟧ ≡ 2
  testt2 = refl
  testt3 : M.⟦ t3 ⟧ ≡ 2
  testt3 = refl

-- FEL: number of nodes in the tree
N : Model {lzero}
N = record
  { Tm = ℕ
  ; true = 1
  ; false = 1
  ; ite = λ a b c → 1 + a + b + c
  ; num = λ _ → 1
  ; isZero = λ n → 1 + n
  ; _+o_ = λ a b → 1 + a + b
  }

module testN where
  module N = Model N

  f : ℕ → I.Tm
  f 0 = I.true
  f (suc n) = I.isZero (f n)

  flen : (n : ℕ) → N.⟦ f n ⟧ ≡ suc n
  flen zero = refl
  flen (suc n) = cong suc (flen n)
  {- a = b -> cong (_+ 1) -> a + 1 = b + 1 -}

-- FEL: C stilusu interpreter
C : Model {lzero}
C = record
  { Tm = ℕ
  ; true = 1
  ; false = 0
  ; ite = λ {zero n m → m
           ; (suc b) n m → n}
  ; num = λ n → n
  ; isZero = λ {zero → 1
              ; (suc n) → 0}
  ; _+o_ = _+_
  }

module testC where
  module M = Model C

  test1 : M.⟦ I.false ⟧ ≡ 0
  test1 = refl
  test2 : M.⟦ I.true ⟧ ≡ 1
  test2 = refl
  test3 : M.⟦ I.ite (I.num 100) (I.num 3) (I.num 2) ⟧ ≡ 3
  test3 = refl
  test4 : M.⟦ I.ite (I.num 0) (I.num 3) (I.num 2) I.+o I.num 3 ⟧ ≡ 5
  test4 = refl

-- mutasd meg, hogy true ≠ isZero (num 0) a szintaxisban! ehhez adj
-- meg egy modellt, amiben a true tt-re, az isZero (num 0) ff-re
-- ertekelodik!
TN : Model {lzero}
TN = record
  { Tm = 𝟚
  ; true = tt
  ; false = ff
  ; ite = λ _ _ _ → ff
  ; num = λ _ → ff
  ; isZero = λ _ → ff
  ; _+o_ = λ _ _ → ff
  }
true≠isZeronum0 : ¬ (I.true ≡ I.isZero (I.num 0))
true≠isZeronum0 e = tt≠ff (cong TN.⟦_⟧ e)
  where module TN = Model TN

-- nemsztenderd modell (a szintaxis ertelmezese nem rakepzes)
NS : Model {lzero}
NS = record
       { Tm = ℕ
       ; true = 2
       ; false = 0
       ; ite = λ { zero a b → b ; _ a b → a }
       ; num = λ n → n + n
       ; isZero = λ { zero → 2 ; _ → 0 }
       ; _+o_ = _+_
       }
module testNS where
  module NS = Model NS

  -- adj meg egy ℕ-t, amire nem kepez egyik term sem
  n : ℕ
  n = 1

  -- bizonyitsd be, hogy minden szintaktikus term ertelmezese paros szam
  ps : (t : I.Tm) → Σsp ℕ λ m → NS.⟦ t ⟧ ≡ m + m
  ps I.true = 1 , refl
  ps I.false = 0 , refl
  ps (I.ite t t₁ t₂) with NS.⟦ t ⟧
  ... | zero = ps t₂
  ... | suc x = ps t₁
  ps (I.num x) = x , refl
  ps (I.isZero t) with NS.⟦ t ⟧ 
  ... | zero = 1 , refl
  ... | suc x = 0 , refl
  ps (t I.+o t₁) with ps t | ps t₁
  ... | m , pr | n , qr = m + n , 
    (NS.⟦ t ⟧ + NS.⟦ t₁ ⟧ 
    ≡⟨ cong₂ _+_ pr qr ⟩ 
    m + m + (n + n)
    ≡⟨ ass+ {m} ⟩ 
    m + (m + (n + n))
    ≡⟨ cong (m +_) (ass+ {m} ⁻¹) ⟩ 
    m + (m + n + n)
    ≡⟨ cong (m +_) (+comm {m + n}) ⟩ 
    m + (n + (m + n))
    ≡⟨ ass+ {m} ⁻¹ ⟩ 
    m + n + (m + n) ∎) -- KESOBB

-- FEL: add meg a legegyszerubb nemsztenderd modellt!
NS' : Model {lzero}
NS' = record
  { Tm = 𝟚
  ; true = tt
  ; false = tt
  ; ite = λ _ _ _ → tt
  ; num = λ _ → tt
  ; isZero = λ _ → tt
  ; _+o_ = λ _ _ → tt
  }
module testNS' where
  module NS' = Model NS'

  -- indukcio
  D' : DepModel {lzero}
  D' = record
    { Tm∙ = λ t → Lift (NS'.⟦ t ⟧ ≡ tt)
    ; true∙ = mk refl
    ; false∙ = mk refl
    ; ite∙ = λ _ _ _ → mk refl
    ; num∙ = λ _ → mk refl
    ; isZero∙ = λ _ → mk refl
    ; _+o∙_ = λ _ _ → mk refl
    }
  module D' = DepModel D'
  
  ∀tt : (t : I.Tm) → NS'.⟦ t ⟧ ≡ tt
  ∀tt t = un D'.⟦ t ⟧
  
  ns : (Σsp I.Tm λ t → NS'.⟦ t ⟧ ≡ ff) → ⊥
  ns (a , b) = tt≠ff ((b ⁻¹ ◾ (∀tt a)) ⁻¹)

-- FEL: product models
Prod : ∀{i j} → Model {i} → Model {j} → Model {i ⊔ j}
Prod M N = record
  { Tm = M.Tm × N.Tm
  ; true = M.true , N.true
  ; false = M.false , N.false
  ; ite = {!!}
  ; num = λ n → M.num n , N.num n
  ; isZero = λ {(a , b) → M.isZero a , N.isZero b}
  ; _+o_ = {!!}
  }
  where
    module M = Model M
    module N = Model N

L1' : Model
L1' = record
  { Tm     = ℕ
  ; true   = 1
  ; false  = 1
  ; ite    = λ t t' t'' → t + t' + t''
  ; num    = λ _ → 1
  ; isZero = λ t → t
  ; _+o_   = _+_
  }
module L1' = Model L1'
L2' : Model
L2' = record
  { Tm     = ℕ
  ; true   = 0
  ; false  = 0
  ; ite    = λ t t' t'' → 2 + t + t' + t''
  ; num    = λ _ → 0
  ; isZero = λ t → t
  ; _+o_   = λ t t' → 1 + t + t'
  }
module L2' = Model L2'

L1L2 : DepModel {lzero}
L1L2 = {!!}
module L1L2 = DepModel L1L2

twolengths : ∀ t → L1.⟦ t ⟧ ≡ suc L2.⟦ t ⟧
twolengths t = {!!}
