{-# OPTIONS --prop --rewriting #-}

module gy06-1 where

open import Lib

module NatBool-with-equational-theory where

  open import NatBool
  open I

  eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  eq-9 =
    (num 3 +o ite (isZero (num 2)) (num 1) (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 3
      ∎

  eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  eq-10 =
    (ite false (num 1 +o num 1) (num 0) +o num 0)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 0
      ∎

  eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  eq-11 =
    ite (isZero (num 0 +o num 1)) false (isZero (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    true
      ∎

module injectivity where

  module NatBool where
    open import NatBool hiding (St)

    -- standard model
    St : Model
    St = record
      { Ty       = Set
      ; Tm       = λ A → A
      ; Nat      = ℕ
      ; Bool     = 𝟚
      ; true     = {!   !}
      ; false    = {!   !}
      ; ite      = {!   !}
      ; num      = {!!}
      ; isZero   = {!!}
      ; _+o_     = {!!}
      ; iteβ₁    = {!!}
      ; iteβ₂    = {!!}
      ; isZeroβ₁ = {!!}
      ; isZeroβ₂ = {!!}
      ; +β       = {!!}
      }
    module St = Model St

    module equations where
      open St

      -- a standard modellben az eq-0...eq-12 egyenlosegek definicio szerint teljesulnek (a metaelmeletbol kovetkeznek)
      -- pl.
      eq-4 : isZero (num 3 +o num 1) ≡ false
      eq-4 = {!!}

      eq-6 : ite true (isZero (num 0)) false ≡ true
      eq-6 = {!!}

      eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
      eq-7 = {!!}

      eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
      eq-8 = {!!}
      
      eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
      eq-9 = {!!}

    open I

    -- bizonyitsd be, hogy NatBool.I.num injektiv!
    numInj : ∀{m n} → num m ≡ num n → m ≡ n
    numInj e = {!!} -- hasznald az St-be valo kiertekelest!

    -- bizonyitsd be, hogy NatBool.I.isZero nem injektiv!
    notInj : ¬ ((t t' : Tm Nat) → isZero t ≡ isZero t' → t ≡ t')
    notInj e = {!!} -- hasznald a num injektivitasat ill. az isZeroβ₂-t!

module integers where

  open import Int hiding (N; SucNf; PredNf; ⌜_⌝; norm; testnorm; stab; ⌜Suc⌝; ⌜Pred⌝; Comp; comp)
  open I

  one one' : Z
  one  = Suc I.Zero
  one' = Suc (Pred (Suc I.Zero))

  one= : one ≡ one'
  one= = {!   !}
  
  -2' -2'' : Z
  -2'  = Pred (Pred I.Zero)
  -2'' = Pred (Suc (Pred (Suc (Pred (Suc (Pred (Pred I.Zero)))))))

  -2= : -2' ≡ -2''
  -2= = {!   !}

  -- nezd meg, mi az, hogy Model:
  Model' = Model

  -- nezd meg, mik a normal formak:
  Nf' = Nf

  ⌜_⌝ : Nf → I.Z -- \cul : ⌜ | \cur : ⌝
  ⌜ -Suc zero     ⌝ = I.Pred I.Zero
  ⌜ -Suc (suc n)  ⌝ = I.Pred ⌜ -Suc n ⌝
  ⌜ Zero          ⌝ = I.Zero
  ⌜ +Suc zero     ⌝ = I.Suc I.Zero
  ⌜ +Suc (suc n)  ⌝ = I.Suc ⌜ +Suc n ⌝

  SucNf : Nf → Nf
  SucNf x = {!   !}

  PredNf : Nf → Nf
  PredNf x = {!   !}

  -- egy normal formakbol allo modell
  N : Model
  N = record
    { Z       = Nf
    ; Zero    = {!   !}
    ; Suc     = {!   !}
    ; Pred    = {!   !}
    ; SucPred = {!   !}
    ; PredSuc = {!   !}
    }
  module N = Model N

  norm : I.Z → Nf
  norm = N.⟦_⟧

  testnorm1 : norm one ≡ norm one'
  testnorm1 = refl
  testnorm2 : norm -2' ≡ norm -2''
  testnorm2 = refl
  testnorm3 :  ⌜ norm (I.Pred (I.Pred (I.Suc (I.Pred (I.Pred (I.Pred (I.Suc I.Zero))))))) ⌝ ≡
              I.Pred (I.Pred (I.Pred I.Zero))
  testnorm3 = refl

  stab : (v : Nf) → norm ⌜ v ⌝ ≡ v
  stab v = {!   !}

  ⌜Suc⌝ : (v : Nf) → ⌜ SucNf v ⌝ ≡ I.Suc ⌜ v ⌝
  ⌜Suc⌝ v = {!   !}

  ⌜Pred⌝ : (v : Nf) → ⌜ PredNf v ⌝ ≡ I.Pred ⌜ v ⌝
  ⌜Pred⌝ v = {!   !}

  Comp : DepModel
  Comp = record
    { Z∙       = λ i → Lift (⌜ norm i ⌝ ≡ i)
    ; Zero∙    = {!   !}
    ; Suc∙     = {!   !}
    ; Pred∙    = {!   !}
    ; SucPred∙ = {!   !}
    ; PredSuc∙ = {!!}
    }
  module Comp = DepModel Comp

  comp : (i : I.Z) → ⌜ norm i ⌝ ≡ i
  comp i = un (Comp.⟦ i ⟧)
