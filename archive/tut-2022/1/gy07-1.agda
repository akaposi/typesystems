{-# OPTIONS --prop --rewriting #-}

module gy07-1 where

open import Lib

module DefABT where
  open import DefABT

  open I

  {- Rewrite the following expressions with De Bruijn notation -}
  
  {-
     let
     / \
  num 1 +o
        / \
    num 2  x
  -}

  -- let x:=num 1 in num 2 +o x
  -- let (num 1) (num 2 +o v0)

  -- let x:=num 1 +o ite (isZero (num 2)) (num 3) (num 4) in x +o x
  -- let (num 1 +o ite (isZero (num 2)) (num 3) (num 4)) (v0 +o v0)

  -- (let x:=num 2 in let y:=num 1 in x +o y)
  -- let (num 2) (let (num 1) (v1 +o v0))
  
  -- De Bruijn index
  -- (let (num 1) in v0 +o v0)
  -- let x:=num 1 in let y:=num 2 in x +o y
  -- let (num 1) in let (num 2) in v1 +o v0

  -- let => def

  v : (n : ℕ) → ∀{m} → Tm (suc n + m)
  v n = var (v' n) where
    v' : (n : ℕ) → ∀{m} → Var (suc n + m)
    v' zero = vz
    v' (suc n) = vs (v' n)
  
  -- (let x:=num 1 in x +o x)
  -- 
  tm-0 : Tm 0
  tm-0 = def (num 1) (v0 {-v0 x-re hivatkozik, ez az egyetlen változó -} +o v0)

  -- let x:=num 1 in 
  --  x +o let y:=x +o num 1 in
  --    y +o let z:=x +o y in
  --      (x +o z) +o (y +o x)
  tm-1 : Tm 0
  tm-1 = def (num 1) (v0 {- x-re hivatkozik, ezen a ponton csak az létezik -} +o 
    def (v0 {- még mindig csak x létezik -} +o num 1) (v0 {- ezen a ponton már y is létezik, ekkorra már az indexek "arrébb" csúsztak 1-gyel, már y a legutoljára definiált változó, v0 y lesz, v1 pedig x -} +o
      def (v1 {- x -} +o v0 {- y -}) (
        (v2 {- Most már z is létezik, megint minden csúszik, így már v2 lesz x -} +o v0 {- z -}) 
          +o (v1 {- y -} +o v2 {- x -}))))

  -- (let x:=num 1 in x) +o let y:=num 1 in 
  --   y +o let z:=x +o y in 
  --     (x +o z) +o (y +o x)
  tm-2 : Tm 1
  tm-2 = (def (num 1) v0 {- x van definiálva csak -}) +o
    def (num 1) (v0 {- itt x nem létezik, most y van definiálva az a v0-} +o
    def (v1 {- !!! Ezen a ponton y definiálva van, az a v0, x ezen a ponton nem létezik, ez egy SZABAD VÁLTOZÓ és ez arra hivatkozik. -} +o v0 {- y -}) ((v2 {- Most már létezik y és z, így ez megint a SZABAD változóra hivavtkozik, de már v2-ként -} +o v0 {- z -}) +o (v1 {- y -} +o v2 {- megint a szabad változó-} )))

  -- (let x:=num 1 in 
  --      x +o let y:=x +o num 1 in x) +o 
  --    let z:=num 1 in z +o z
  tm-3 : Tm {!   !}
  tm-3 = {!   !}

  -- ((let x:=num 1 in x) +o (let y:=num 1 in y)) +o let z:=num 1 in z +o z
  tm-4 : Tm {!   !}
  tm-4 = {!   !}

  -- let x:=(isZero true) in (ite x 0 x)
  tm-5 : Tm {!   !}
  tm-5 = {!   !}


  {- Rewrite the following expressions with variable names -}

  -- let x:=      1+2 in        (x+x)   + let    y:=3+4      in   y  + y
  t-1 : Tm {!   !}
  t-1 = {!   !}

  -- let x:=1+2 in (x+x) + let y:=3+4 in x + y
  t-1' : Tm {!   !}
  t-1' = {!   !}

  --    let x:=true in x   + let y:=x in y+x
  t-2 : Tm {!   !}
  t-2 = {!   !}

  --   let x:=true in let y:=false in ite y y x
  t-3 : Tm {!   !}
  t-3 = {!   !}

  --   true + let x:= true in false + let y:=x in x+y
  t-4 : Tm {!   !}
  t-4 = {!   !}

   --  let x:=true in let y:=false in let z:=true in let w:=false in (w +o z) +o (y +o x)
  t-5 : Tm {!   !}
  t-5 = {!   !}

  -- exercise 2.6

  zipWith : ∀{n}{A B C : Set} → (A → B → C) → Vec A n → Vec B n → Vec C n
  zipWith _ [] [] = []
  zipWith f (m :: ms) (n :: ns) = f m n :: zipWith f ms ns

  zip+ : ∀{n} → Vec ℕ n → Vec ℕ n → Vec ℕ n
  zip+ = zipWith _+_

  tail : ∀{n}{A : Set} → Vec A (suc n) → Vec A n
  tail (_ :: ms) = ms

  countVars' : ∀{n} → Tm n → Vec ℕ n -- var esetén tudni kell, hogy hova kell számolni.
  countVars' = {!   !}

  ttt : Tm 3
--  ttt = v0
  ttt = (v0 +o v0) +o def v0 (v1 +o v2)

  a = {!countVars' ttt!}

module DefWT where

  open import DefWT
  open I
  -- ◇ \di2     ▹  \t6
  tm-0 : {!   !}
  tm-0 = isZero (ite v2 (v1 +o v0) v1)

  tm-1 : {!   !}
  tm-1 = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  tm-2 : {!   !}
  tm-2 = ite v1 (isZero v0) (isZero v2)

  tm-3 : {!   !}
  tm-3 = ite (ite v2 (isZero v0) v2) v1 v1