{-# OPTIONS --prop --rewriting #-}

module gy11-1 where

open import Lib
open import STT
open I

add2 : Tm ◇ (Nat ⇒ Nat) -- \=>
add2 = lam (v0 +o num 2) -- λx.x+2

and : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
and = lam (lam (ite v1 v0 false)) -- λx y.if x then y else false

and-test1 : and $ true $ true ≡ true
and-test1 =
  and $ true $ true
  ≡⟨ refl ⟩
  (lam (lam (ite (q [ p ]) q false)) $ true) $ true
  ≡⟨ cong (_$ true) ⇒β ⟩
  ((lam (ite (q [ p ]) q false)) [ id ,o true ]) $ true
  ≡⟨ cong (_$ true) lam[] ⟩
  lam (ite (q [ p ]) q false [ (id ,o true) ⊚ p ,o q ]) $ true
  ≡⟨ cong (λ x → lam x $ true) ite[] ⟩
  lam (ite 
    (q [ p ] [ (id ,o true) ⊚ p ,o q ])
    (q [ (id ,o true) ⊚ p ,o q ])
    (false [ (id ,o true) ⊚ p ,o q ]))
  $ true
  ≡⟨ cong₃ (λ x y z → lam (ite x y z) $ true) ([∘] ⁻¹) ▹β₂ false[] ⟩
  lam (ite (q [ p ⊚ ((id ,o true) ⊚ p ,o q) ]) q false) $ true
  ≡⟨ cong (λ x → lam (ite (q [ x ]) q false) $ true) ▹β₁ ⟩
  lam (ite (q [ (id ,o true) ⊚ p ]) q false) $ true
  ≡⟨ cong (λ x → lam (ite x q false) $ true) [∘] ⟩
  lam (ite (q [ id ,o true ] [ p ]) q false) $ true
  ≡⟨ cong (λ x → lam (ite (x [ p ]) q false) $ true) ▹β₂ ⟩
  lam (ite (true [ p ]) q false) $ true
  ≡⟨ cong (λ x → lam (ite x q false) $ true) true[] ⟩
  lam (ite true q false) $ true
  ≡⟨ ⇒β ⟩
  ite true q false [ id ,o true ]
  ≡⟨ ite[] ⟩
  ite (true [ id ,o true ]) (q [ id ,o true ]) (false [ id ,o true ])
  ≡⟨ cong₃ ite true[] ▹β₂ false[] ⟩
  ite true true false
  ≡⟨ iteβ₁ ⟩
  true ∎
  
and-test2 : and $ false $ true ≡ false
and-test2 = {!   !}

{-
and : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
and = lam (lam (ite v1 v0 false))
-}
-- External and
and' : Tm ◇ Bool → Tm ◇ Bool → Tm ◇ Bool
and' = λ x → λ y → ite x y false

and'-test1 : and' true true ≡ true
and'-test1 = iteβ₁

and'-test2 : and' false true ≡ false
and'-test2 = iteβ₂
----------------------------------
neg : Tm ◇ (Bool ⇒ Bool)
neg = lam (ite v0 false true)

neg-test1 : neg $ true ≡ false
neg-test1 = {!!}

neg-test2 : neg $ false ≡ true
neg-test2 = {!!}

infixl 6  _∘o_
_∘o_ : ∀{Γ A B C} → Tm Γ (B ⇒ C) → Tm Γ (A ⇒ B) → Tm Γ (A ⇒ C) -- (b -> c) -> (a -> b) -> a -> c
f ∘o g = lam (f [ p ] $ (g [ p ] $ q)) -- (f ∘ g) := λx.f(g(x))

id' : {A : Set} → A → A
id' = λ x → x

ido : ∀{Γ A} → Tm Γ (A ⇒ A)
ido = lam v0

apply3 : Tm ◇ ((Bool ⇒ Bool) ⇒ Bool ⇒ Bool)
apply3 = lam (q ∘o q ∘o q)

apply3-test : apply3 $ neg $ true ≡ false
apply3-test =
  lam (lam (lam (q [ p ] $ (q [ p ] $ q)) [ p ] $ (q [ p ] $ q))) $ lam (ite q false true) $ true
  ≡⟨ cong (_$ true) ⇒β ⟩
  lam (lam (q [ p ] $ (q [ p ] $ q)) [ p ] $ (q [ p ] $ q)) [ id ,o lam (ite q false true) ] $ true
  ≡⟨ cong (_$ true) lam[] ⟩
  lam
    ((lam (q [ p ] $ (q [ p ] $ q)) [ p ] $ (q [ p ] $ q))
    [ (id ,o lam (ite q false true)) ⊚ p ,o q ])
    $ true
  ≡⟨ cong (λ x → lam x $ true) $[] ⟩
  lam
    (lam (q [ p ] $ (q [ p ] $ q)) [ p ] [ (id ,o lam (ite q false true)) ⊚ p ,o q ]
    $ (q [ p ] $ q) [ (id ,o lam (ite q false true)) ⊚ p ,o q ])
    $ true
  ≡⟨ cong₂ (λ x y → lam (x $ y) $ true) ([∘] ⁻¹) $[] ⟩
  lam
    (lam (q [ p ] $ (q [ p ] $ q))
    [ p ⊚ ((id ,o lam (ite q false true)) ⊚ p ,o q) ] $
    (q [ p ] [ (id ,o lam (ite q false true)) ⊚ p ,o q ] $
    q [ (id ,o lam (ite q false true)) ⊚ p ,o q ]))
    $ true
  ≡⟨ cong₃ (λ x y z → lam ((lam (q [ p ] $ (q [ p ] $ q))) [ x ] $ (y $ z)) $ true) ▹β₁ ([∘] ⁻¹) ▹β₂ ⟩
  lam
    (lam (q [ p ] $ (q [ p ] $ q)) [ (id ,o lam (ite q false true)) ⊚ p ]
    $ (q [ p ⊚ ((id ,o lam (ite q false true)) ⊚ p ,o q) ] $ q))
    $ true
  ≡⟨ cong (λ x → lam
    (lam (q [ p ] $ (q [ p ] $ q)) [ (id ,o lam (ite q false true)) ⊚ p ]
    $ (q [ x ] $ q))
    $ true) ▹β₁ ⟩
  lam
    (lam (q [ p ] $ (q [ p ] $ q)) [ (id ,o lam (ite q false true)) ⊚ p ]
    $ (q [ (id ,o lam (ite q false true)) ⊚ p ] $ q))
    $ true
  ≡⟨ cong₂ (λ x y → lam (x $ (y $ q)) $ true) lam[] [∘] ⟩
  lam
    (lam ((q [ p ] $ (q [ p ] $ q)) [ (id ,o lam (ite q false true)) ⊚ p ⊚ p ,o q ])
    $ (q [ id ,o lam (ite q false true) ] [ p ] $ q))
    $ true
  ≡⟨ cong₂ (λ x y → lam ((lam x) $ ((y [ p ]) $ q)) $ true) $[] ▹β₂ ⟩
  lam
    (lam
      (q [ p ] [ (id ,o lam (ite q false true)) ⊚ p ⊚ p ,o q ] $
      (q [ p ] $ q) [ (id ,o lam (ite q false true)) ⊚ p ⊚ p ,o q ])
    $ (lam (ite q false true) [ p ] $ q))
  $ true
  ≡⟨ cong (λ x → lam (lam (q [ p ] [ (id ,o lam (ite q false true)) ⊚ p ⊚ p ,o q ] $ x) $ (lam (ite q false true) [ p ] $ q) ) $ true) $[] ⟩
  lam
    (lam
     (q [ p ] [ (id ,o lam (ite q false true)) ⊚ p ⊚ p ,o q ] $
      (q [ p ] [ (id ,o lam (ite q false true)) ⊚ p ⊚ p ,o q ] $
       q [ (id ,o lam (ite q false true)) ⊚ p ⊚ p ,o q ]))
     $ (lam (ite q false true) [ p ] $ q))
    $ true
  ≡⟨ cong₃
    (λ x y z →
      lam (lam (x $ (y $ z)) $ (lam (ite q false true) [ p ] $ q)) $
      true)
    ([∘] ⁻¹) ([∘] ⁻¹) ▹β₂ ⟩
  lam
    (lam
     (q [ p ⊚ ((id ,o lam (ite q false true)) ⊚ p ⊚ p ,o q) ] $
      (q [ p ⊚ ((id ,o lam (ite q false true)) ⊚ p ⊚ p ,o q) ] $ q))
     $ (lam (ite q false true) [ p ] $ q))
    $ true
  ≡⟨ cong₂
    (λ x y →
      lam (lam (q [ x ] $ (q [ y ] $ q)) $ (lam (ite q false true) [ p ] $ q)) $ true)
    ▹β₁ ▹β₁ ⟩
  lam
    (lam
     (q [ (id ,o lam (ite q false true)) ⊚ p ⊚ p ] $
      (q [ (id ,o lam (ite q false true)) ⊚ p ⊚ p ] $ q))
     $ (lam (ite q false true) [ p ] $ q))
    $ true
  ≡⟨ cong₂
      (λ x y →
         lam
         (lam (q [ x ] $ (q [ y ] $ q)) $
          (lam (ite q false true) [ p ] $ q))
         $ true)
      ass ass ⟩
  lam
    (lam
     (q [ (id ,o lam (ite q false true)) ⊚ (p ⊚ p) ] $
      (q [ (id ,o lam (ite q false true)) ⊚ (p ⊚ p) ] $ q))
     $ (lam (ite q false true) [ p ] $ q))
    $ true
  ≡⟨ cong₂
      (λ x y →
         lam
         (lam (x $ (y $ q)) $
          (lam (ite q false true) [ p ] $ q))
         $ true)
      [∘] [∘] ⟩
  lam
    (lam
     (q [ id ,o lam (ite q false true) ] [ p ⊚ p ] $
      (q [ id ,o lam (ite q false true) ] [ p ⊚ p ] $ q))
     $ (lam (ite q false true) [ p ] $ q))
    $ true
  ≡⟨ cong₂
      (λ x y →
         lam
         (lam (x [ p ⊚ p ] $ (y [ p ⊚ p ] $ q)) $
          (lam (ite q false true) [ p ] $ q))
         $ true)
      ▹β₂ ▹β₂ ⟩
  lam
    (lam
     (lam (ite q false true) [ p ⊚ p ] $
      (lam (ite q false true) [ p ⊚ p ] $ q))
     $ (lam (ite q false true) [ p ] $ q))
    $ true
  ≡⟨ ⇒β ⟩
  (lam
    (lam (ite q false true) [ p ⊚ p ] $
      (lam (ite q false true) [ p ⊚ p ] $ q))
    $ (lam (ite q false true) [ p ] $ q))
      [ id ,o true ]
  ≡⟨ $[] ⟩
  lam
    (lam (ite q false true) [ p ⊚ p ] $
    (lam (ite q false true) [ p ⊚ p ] $ q))
    [ id ,o true ]
    $ (lam (ite q false true) [ p ] $ q) [ id ,o true ]
    -- nagyon produktív óra vége :(
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !} ∎

apply3-test' : (t : Tm ◇ (Bool ⇒ Bool))(u : Tm ◇ Bool) → apply3 $ t $ u ≡ t $ (t $ (t $ u))
apply3-test' = {!!}

apply3-extra : (t : Tm ◇ (Bool ⇒ Bool))(u : Tm ◇ Bool) → apply3 $ t $ u ≡ t $ u
apply3-extra = {!!}

isZero' : {!   !}
isZero' = {!   !} -- λx.isZero x

isZero'β₁ : ∀{Γ} → isZero' {Γ} $ num 0 ≡ true
isZero'β₁ = {!   !}

isZero'β₂ : ∀{Γ} → isZero' {Γ} $ num 1 ≡ false
isZero'β₂ = {!!}

Four = Bool ⇒ Bool

one two three four : Tm ◇ Four
one = {!!}
two = {!!}
three = {!!}
four = {!   !}

one' two' three' four' : ∀{Γ} → Tm Γ Four
one'   = lam (ite v0 false true)
two'   = lam (ite v0 true false)
three' = lam true
four'   = lam false

four'[] : ∀{Γ Δ}{γ : Sub Δ Γ} → four' [ γ ] ≡ four'
four'[] = {!   !}
-- also for one', two', three'

case' : ∀{Γ A} → Tm Γ Four → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A
case' t a0 a1 a2 a3 = ite (t $ false)
  (ite (t $ true)
    a3
    a2)
  (ite (t $ true)
    a1
    a0)

case'β1 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' one' a1 a2 a3 a4 ≡ a1
case'β1 = {!!}
case'β2 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' two' a1 a2 a3 a4 ≡ a2
case'β2 = {!!}
case'β3 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' three' a1 a2 a3 a4 ≡ a3
case'β3 = {!!}
case'β4 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' four' a1 a2 a3 a4 ≡ a4
case'β4 = {!!}

(num 5 +o ((q [ p ] [ ((id ,o num 4) ⊚ p) ,o q ]) [ id ,o num 5 ]))
