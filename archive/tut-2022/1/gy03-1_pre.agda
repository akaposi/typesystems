{-# OPTIONS --prop --rewriting #-}

module gy03-1 where

open import Lib
open import NatBoolAST hiding (T)

tm1 : I.Tm
tm1 = ite true (_+o_ (num 1) (num 3)) (isZero (isZero false))
  where open I

-- FEL: do all the compulsory exercises from the book!

-- FEL: count the number of trues in a term (exercise 1.10 in the book)
Trues : Model {lzero}
Trues = {!!}

module testTrues where
  module M = Model Trues

  t : M.Tm
  t = M.true

  t' : M.Tm
  t' = 10

  test1 : M.⟦ I.false I.+o (I.num 1) ⟧ ≡ 0
  test1 = refl
  test2 : M.⟦ tm1 ⟧ ≡ 1
  test2 = refl
  test3 : M.⟦ I.ite I.true I.true I.true ⟧ ≡ 3
  test3 = refl

  -- FEL: adj meg ket kulonbozo termet, melyek ertelmezese 2
  t2 t3 : I.Tm
  t2 = {!!}
  t3 = {!!}
  t2≠t3 : ¬ (t2 ≡ t3)
  t2≠t3 ()
  testt2 : M.⟦ t2 ⟧ ≡ 2
  testt2 = refl
  testt3 : M.⟦ t3 ⟧ ≡ 2
  testt3 = refl

-- FEL: number of nodes in the tree
N : Model {lzero}
N = {!!}
module testN where
  module N = Model N

  f : ℕ → I.Tm
  f = {!!}

  flen : (n : ℕ) → N.⟦ f n ⟧ ≡ suc n
  flen n = {!!}

-- FEL: C stilusu interpreter
C : Model {lzero}
C = {!!}

module testC where
  module M = Model C

  test1 : M.⟦ I.false ⟧ ≡ 0
  test1 = refl
  test2 : M.⟦ I.true ⟧ ≡ 1
  test2 = refl
  test3 : M.⟦ I.ite (I.num 100) (I.num 3) (I.num 2) ⟧ ≡ 3
  test3 = refl
  test4 : M.⟦ I.ite (I.num 0) (I.num 3) (I.num 2) I.+o I.num 3 ⟧ ≡ 5
  test4 = refl

-- mutasd meg, hogy true ≠ isZero (num 0) a szintaxisban! ehhez adj
-- meg egy modellt, amiben a true tt-re, az isZero (num 0) ff-re
-- ertekelodik!
TN : Model {lzero}
TN = record
  { Tm = 𝟚
  ; true = {!!}
  ; false = {!!}
  ; ite = {!!}
  ; num = {!!}
  ; isZero = {!!}
  ; _+o_ = {!!}
  }
true≠isZeronum0 : ¬ (I.true ≡ I.isZero (I.num 0))
true≠isZeronum0 e = tt≠ff (cong TN.⟦_⟧ e)
  where module TN = Model TN

-- nemsztenderd modell (a szintaxis ertelmezese nem rakepzes)
NS : Model {lzero}
NS = record
       { Tm = ℕ
       ; true = 2
       ; false = 0
       ; ite = λ { zero a b → b ; _ a b → a }
       ; num = λ n → n + n
       ; isZero = λ { zero → 2 ; _ → 0 }
       ; _+o_ = _+_
       }
module testNS where
  module NS = Model NS

  -- adj meg egy ℕ-t, amire nem kepez egyik term sem
  n : ℕ
  n = {!!}

  -- bizonyitsd be, hogy minden szintaktikus term ertelmezese paros szam
  ps : (t : I.Tm) → Σsp ℕ λ m → NS.⟦ t ⟧ ≡ m + m
  ps = {!!} -- KESOBB

-- FEL: add meg a legegyszerubb nemsztenderd modellt!
NS' : Model {lzero}
NS' = record
  { Tm = 𝟚
  ; true = {!!}
  ; false = {!!}
  ; ite = {!!}
  ; num = {!!}
  ; isZero = {!!}
  ; _+o_ = {!!}
  }
module testNS' where
  module NS' = Model NS'
  b : 𝟚
  b = tt

  -- indukcio
  D : DepModel {lzero}
  D = {!!}
  module D = DepModel D
  
  ∀ff : (t : I.Tm) → NS'.⟦ t ⟧ ≡ ff
  ∀ff t = un D.⟦ t ⟧
  
  ns : (Σsp I.Tm λ t → NS'.⟦ t ⟧ ≡ tt) → ⊥
  ns e = tt≠ff (π₂ e ⁻¹ ◾ ∀ff (π₁ e))

-- FEL: product models
Prod : ∀{i j} → Model {i} → Model {j} → Model {i ⊔ j}
Prod M N = record
  { Tm = M.Tm × N.Tm
  ; true = {!!}
  ; false = {!!}
  ; ite = {!!}
  ; num = {!!}
  ; isZero = {!!}
  ; _+o_ = {!!}
  }
  where
    module M = Model M
    module N = Model N

L1 : Model
L1 = record
  { Tm     = ℕ
  ; true   = 1
  ; false  = 1
  ; ite    = λ t t' t'' → t + t' + t''
  ; num    = λ _ → 1
  ; isZero = λ t → t
  ; _+o_   = _+_
  }
module L1 = Model L1
L2 : Model
L2 = record
  { Tm     = ℕ
  ; true   = 0
  ; false  = 0
  ; ite    = λ t t' t'' → 2 + t + t' + t''
  ; num    = λ _ → 0
  ; isZero = λ t → t
  ; _+o_   = λ t t' → 1 + t + t'
  }
module L2 = Model L2

L1L2 : DepModel {lzero}
L1L2 = {!!}
module L1L2 = DepModel L1L2

twolengths : ∀ t → L1.⟦ t ⟧ ≡ suc L2.⟦ t ⟧
twolengths t = {!!}
