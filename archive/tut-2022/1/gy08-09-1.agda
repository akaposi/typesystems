{-# OPTIONS --prop --rewriting #-}

module gy08-09-1 where

module DefWT where

  open import DefWT
  open I
  -- ◇ \di2 \diw    ▹  \t6 \tw2 \triangleright
  tm-0 : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Bool
  tm-0 = isZero (ite v2 (v1 +o v0) v1)

  tm-1 : {A : Ty} → Tm (◇ ▹ Nat ▹ A) Nat
  tm-1 = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  tm-2 : {Γ : Con} → Tm (Γ ▹ Nat ▹ Bool ▹ Nat) Bool
  tm-2 = ite v1 (isZero v0) (isZero v2)

  tm-3 : ∀{Γ A} → Tm (Γ ▹ Bool ▹ A ▹ Nat) A
  tm-3 = ite (ite v2 (isZero v0) v2) v1 v1

open import Lib
open import Def
open I

-- def (num 2) v0 = v0[v0 ↦ num 2] = num 2
-- def (num 2) (v0 +o v0) = (v0 +o v0)[v0 ↦ num 2] = num 2 +o num 2 = num 4
-- def (num 2) (def (num 1) (v0 +o v1)) = (def (num 1) (v0 +o v1))[v0 ↦ num 2] =
--   (v0 +o v1)[v0 ↦ num 1][v0 ↦ num 2] = ... =
--   (v0 +o v1)[v0 ↦ num 1, v1 ↦ num 2] = num 1 +o num 2 = num 3
-- def (num 2) (def (num 1) (v0 +o v1)) = (def (num 1) (v0 +o v1))[v0 ↦ num 2] =
--   (v0 +o v1)[v0 ↦ num 1][v0 ↦ num 2] =
--   (num 1 +o v0)[v0 ↦ num 2] = num 1 +o num 2 = num 3

-- def (num 2) (def (num 1) (v0 +o v1)) = (def (num 1) (v0 +o v1))[ε ,o num 2] = ...
-- \__________________________________/   \______________________/\__________/
--    : Tm ◇ Nat                         : Tm (◇ ▹ Nat) Nat        : Sub ◇ (◇ ▹ Nat)

-- def t u = u[id ,o t]

-- substitution calculus, simply typed CATEGORY WITH FAMILIES

-- Ty, Con
-- Sub : Con → Con → Set      Sub Γ (◇ ▹ A₀ ▹ A₁ ▹ A₂) = 3 db term, egy A₀ tipusú, egy A₁ tipusú es egy A₂ tipusú
-- (ε ,o a₀ ,o a₁ ,o a₂) : Sub Γ (◇ ▹ A₀ ▹ A₁ ▹ A₂), ha  a₀ : Tm Γ A₀, a₁ : Tm Γ A₁, a₂ : Tm Γ A₂
--        termek sorozata (listája)
-- id : Sub Γ Γ
-- ε  : Sub Γ ◇
-- _,o_ : Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
-- _[_] : Tm Γ A → Sub Δ Γ → Tm Δ A
-- Tm : Con → Ty → Set        Tm Γ A -- A típusú termek, amelyekben Γ-nak megfelelő változók lehetnek
--   ite v0 (num 1 +o v1) v1 : Tm (◇ ▹ Nat ▹ Bool) Nat

-- σ : Sub (◇ ▹ Nat ▹ Bool) (◇ ▹ Bool ▹ Nat ▹ Nat)
-- σ = (ε ,o isZero (num 0 +o v1) ,o v1 ,o ite v0 v1 (num 2))
-- t : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Nat
-- t = v0 +o (v1 +o ite v2 (num 0) (num 1))
-- t [ σ ] : Tm (◇ ▹ Nat ▹ Bool) Nat
-- (v0 +o (v1 +o ite v2 (num 0) (num 1))) [ (ε ,o isZero (num 0 +o v1) ,o v1 ,o ite v0 v1 (num 2)) ] =
--   (ite v0 v1 (num 2))) +o (v1 +o ite (isZero (num 0 +o v1)) (num 0) (num 1))

module ex1 where
  Γ : Con
  Γ = ◇ ▹ Bool

  t : Tm Γ Nat
  t = ite v0 (num 1) (num 0)

  γ : Sub ◇ Γ
  γ = ε ,o true

  t[γ] : t [ γ ] ≡ num 1
  t[γ] =
    (t [ γ ])
    ≡⟨ refl ⟩
    ite q (num 1) (num 0) [ ε ,o true ]
    ≡⟨ ite[] ⟩
    ite (q [ ε ,o true ]) 
      (num 1 [ ε ,o true ])
      (num 0 [ ε ,o true ])
    ≡⟨ cong₃ ite ▹β₂ num[] num[] ⟩
    ite true (num 1) (num 0)
    ≡⟨ iteβ₁ ⟩
    num 1 ∎

  Δ : Con
  Δ = ◇ ▹ Nat ▹ Nat

  δ : Sub Γ Δ
  δ = ε ,o ite v0 (num 1) (num 2) ,o num 2

  u : Tm Δ Nat
  u = v0 +o v1

  u[δ] : u [ δ ] ≡
    num 2 +o ite v0 (num 1) (num 2)
  u[δ] =
    (u [ δ ])
    ≡⟨ refl ⟩
    q +o (q [ p ]) [ ε ,o ite q (num 1) (num 2) ,o num 2 ]
    ≡⟨ +[] ⟩
    (q [ ε ,o ite q (num 1) (num 2) ,o num 2 ])
    +o
    (q [ p ] [ ε ,o ite q (num 1) (num 2) ,o num 2 ])
    ≡⟨ cong (_+o (q [ p ] [ ε ,o ite q (num 1) (num 2) ,o num 2 ])) ▹β₂ ⟩
    num 2 +o (q [ p ] [ ε ,o ite q (num 1) (num 2) ,o num 2 ])
    ≡⟨ cong (num 2 +o_) ([∘] ⁻¹) ⟩
    num 2 +o (q [ p ⊚ (ε ,o ite q (num 1) (num 2) ,o num 2) ])
    ≡⟨ cong (λ x → num 2 +o (q [ x ])) ▹β₁ ⟩
    num 2 +o (q [ ε ,o ite q (num 1) (num 2) ])
    ≡⟨ cong (num 2 +o_) ▹β₂ ⟩
    num 2 +o ite v0 (num 1) (num 2) ∎

module ex2 where
  var-test-1 : (def {◇} true v0) ≡ true
  var-test-1 =
    def true v0 -- v0 = q
    ≡⟨ refl ⟩
    q [ id ,o true ]
    ≡⟨ ▹β₂ ⟩
    true ∎
  
  var-test-2 : (def {◇} true (def false v0)) ≡ false
  var-test-2 =
    def true (def false v0)
    ≡⟨ refl ⟩
    (q [ id ,o false ]) [ id ,o true ]
    ≡⟨ cong _[ id ,o true ] ▹β₂ ⟩
    false [ id ,o true ]
    ≡⟨ false[] ⟩
    false ∎
  
  var-test-3 : (def {◇} true (def false v1)) ≡ true
  var-test-3 =
    def true (def false v1)
    ≡⟨ refl ⟩
    q [ p ] [ id ,o false ] [ id ,o true ]
    ≡⟨ cong _[ id ,o true ] ([∘] ⁻¹) ⟩
    q [ p ⊚ (id ,o false) ] [ id ,o true ]
    ≡⟨ [∘] ⁻¹ ⟩
    q [ p ⊚ (id ,o false) ⊚ (id ,o true) ]
    ≡⟨ cong (λ x → q [ x ⊚ (id ,o true) ]) ▹β₁ ⟩
    q [ id ⊚ (id ,o true) ]
    ≡⟨ cong (q [_]) idl ⟩
    q [ (id ,o true) ]
    ≡⟨ ▹β₂ ⟩
    true ∎
  --               v1 = false  v0 = num 4
  task-sub : Sub ◇ (◇ ▹ Bool ▹ Nat)
  task-sub = ε ,o false ,o num 4
  
  task-eq : (ite v1 (v0 +o v0) v0) [ task-sub ] ≡ num 4
  task-eq = 
    (ite (q [ p ]) (q +o q) q) [ ε ,o false ,o num 4 ]
    ≡⟨ ite[] ⟩
    ite (q [ p ] [ ε ,o false ,o num 4 ])
        (q +o q [ ε ,o false ,o num 4 ])
        (q [ ε ,o false ,o num 4 ])
    ≡⟨ cong₃ ite ([∘] ⁻¹) +[] ▹β₂ ⟩
    ite (q [ p ⊚ (ε ,o false ,o num 4) ])
        ((q [ ε ,o false ,o num 4 ]) +o (q [ ε ,o false ,o num 4 ]))
        (num 4)
    ≡⟨ cong₂ (λ x y → ite x y (num 4))
        (cong (q [_]) ▹β₁)
        (cong₂ _+o_ ▹β₂ ▹β₂) ⟩
    ite (q [ ε ,o false ]) (num 4 +o num 4) (num 4)
    ≡⟨ cong (λ x → ite x (num 4 +o num 4) (num 4)) ▹β₂ ⟩
    ite false (num 4 +o num 4) (num 4)
    ≡⟨ iteβ₂ ⟩
    num 4 ∎
  
  var-eq-4 : def (num 1) (v0 +o v0) ≡ num {◇} 2
  var-eq-4 =
    def (num 1) (v0 +o v0)
    ≡⟨ refl ⟩
    (q +o q) [ id ,o num 1 ]
    ≡⟨ +[] ⟩
    (q [ id ,o num 1 ]) +o (q [ id ,o num 1 ])
    ≡⟨ cong₂ _+o_ ▹β₂ ▹β₂ ⟩
    num 1 +o num 1
    ≡⟨ +β ⟩
    num 2 ∎
  
  var-eq-5 : def false (def (num 2) (ite v1 (num 0) (num 1 +o v0))) ≡ num {◇} 3
  var-eq-5 =
    def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
    ≡⟨ refl ⟩
    ite (q [ p ]) (num 0) (num 1 +o q) [ id ,o num 2 ] [ id ,o false ]
    ≡⟨ cong _[ id ,o false ] ite[] ⟩
    ite
      (q [ p ] [ id ,o num 2 ])
      (num 0 [ id ,o num 2 ])
      ((num 1 +o q) [ id ,o num 2 ])
        [ id ,o false ]
    ≡⟨ cong₃ (λ x y z → ite x y z [ id ,o false ])
      ([∘] ⁻¹)
      num[]
      +[] ⟩
    ite (q [ p ⊚ (id ,o num 2) ]) 
        (num 0)
        ((num 1 [ id ,o num 2 ]) +o (q [ id ,o num 2 ]))
          [ id ,o false ]
    ≡⟨ cong₂ (λ x z → ite (q [ x ]) (num 0) z [ id ,o false ])
        ▹β₁
        (cong₂ _+o_ num[] ▹β₂) ⟩
    ite (q [ id ]) (num 0) (num 1 +o num 2) [ id ,o false ]
    ≡⟨ cong (λ x → ite (q [ id ]) (num 0) x [ id ,o false ]) +β ⟩
    ite (q [ id ]) (num 0) (num 3) [ id ,o false ]
    ≡⟨ ite[] ⟩
    ite (q [ id ] [ id ,o false ]) (num 0 [ id ,o false ]) (num 3 [ id ,o false ])
    ≡⟨ cong₃ ite ([∘] ⁻¹) num[] num[] ⟩
    ite (q [ id ⊚ (id ,o false) ]) (num 0) (num 3)
    ≡⟨ cong (λ x → ite (q [ x ]) (num 0) (num 3)) idl ⟩
    ite (q [ id ,o false ]) (num zero) (num 3)
    ≡⟨ cong (λ x → ite x (num zero) (num 3)) ▹β₂ ⟩
    ite false (num 0) (num 3)
    ≡⟨ iteβ₂ ⟩
    num 3 ∎
  
  -- var-eq-6-9 JÓ GYAKORLÁS A KÖVI KIS ZH-RA!
  var-eq-6 : def (num 0) (def (isZero v0) (ite v0 false true)) ≡ false {◇}
  var-eq-6 =
    def (num 0) (def (isZero v0) (ite v0 false true))
    ≡⟨ {!!} ⟩
    false ∎
  
  var-eq-7 : def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0))) ≡ num {◇} 2
  var-eq-7 =
    def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
    ≡⟨ {!!} ⟩
    num 2 ∎
  
  var-eq-8 : def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0))) ≡ false {◇}
  var-eq-8 =
    def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0)))
    ≡⟨ {!!} ⟩
    false ∎
  
  var-eq-9 : def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0))) ≡ num {◇} 2
  var-eq-9 =
    def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0)))
    ≡⟨ {!!} ⟩
    num 2 ∎
  
  swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
  swap = {!   !}
  
  swapswap : {Γ : Con} → {A B : Ty} → swap ⊚ swap ≡ id {Γ ▹ A ▹ B}
  swapswap {Γ} {A} {B} = {!!}
  
  sub-eq-1 : {Γ : Con} → {σ δ : Sub Γ ◇} → σ ≡ δ
  sub-eq-1 {σ = σ} {δ} = 
    σ
    ≡⟨ ◇η ⟩
    ε
    ≡⟨ ◇η ⁻¹ ⟩
    δ ∎
  
  sub-eq-2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → ε ⊚ σ ≡ ε
  sub-eq-2 {Γ} {Δ} {σ} = {!   !}

  sub-eq-3 :
    {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
    (σ ,o t) ⊚ δ ≡ (σ ⊚ δ ,o t [ δ ])
  sub-eq-3 {Γ} {Δ} {Θ} {σ} {δ} {A} {t} =
    (σ ,o t) ⊚ δ
    ≡⟨ {!!} ⟩
    {!!}
    ≡⟨ {!!} ⟩
    (σ ⊚ δ ,o t [ δ ]) ∎
  
  sub-eq-4 :
    {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ⊚ p ,o q ])
  sub-eq-4 {σ = σ} {t = t} {u = u} =
    (def t u) [ σ ]
    ≡⟨ {!!} ⟩
    {!!}
    ≡⟨ {!!} ⟩
    def (t [ σ ]) (u [ σ ⊚ p ,o q ]) ∎
  
  sub-eq-5 : {Γ Δ : Con}{σ : Sub Γ Δ}{A B : Ty}{u : Tm Γ A}{v : Tm Γ B}
           → (p ⊚ p ,o q ,o q) ⊚ (σ ,o u ,o v) ≡ (σ ,o v ,o v)
  sub-eq-5 {σ = σ} {u = u} {v} = 
    (p ⊚ p ,o q ,o q) ⊚ (σ ,o u ,o v)
    ≡⟨ ▹η ⁻¹ ⟩
    p ⊚ ((p ⊚ p ,o q ,o q) ⊚ (σ ,o u ,o v)) ,o q [ (p ⊚ p ,o q ,o q) ⊚ (σ ,o u ,o v) ]
    ≡⟨ cong₂ _,o_ (ass ⁻¹) [∘] ⟩
    p ⊚ (p ⊚ p ,o q ,o q) ⊚ (σ ,o u ,o v)
    ,o 
    q [ p ⊚ p ,o q ,o q ] [ σ ,o u ,o v ]
    ≡⟨ cong₂ (λ x y → x ⊚ (σ ,o u ,o v) ,o y [ σ ,o u ,o v ]) ▹β₁ ▹β₂ ⟩
    (p ⊚ p ,o q) ⊚ (σ ,o u ,o v) ,o q [ σ ,o u ,o v ]
    ≡⟨ cong (λ x → (p ⊚ p ,o q) ⊚ (σ ,o u ,o v) ,o x) ▹β₂ ⟩
    (p ⊚ p ,o q) ⊚ (σ ,o u ,o v) ,o v
    ≡⟨ cong (_,o v) (▹η ⁻¹) ⟩
    p ⊚ ((p ⊚ p ,o q) ⊚ (σ ,o u ,o v))
    ,o
    q [ (p ⊚ p ,o q) ⊚ (σ ,o u ,o v) ]
    ,o 
    v
    ≡⟨ cong₂ (λ x y → x ,o y ,o v) (ass ⁻¹) [∘] ⟩
    p ⊚ (p ⊚ p ,o q) ⊚ (σ ,o u ,o v)
    ,o
    q [ p ⊚ p ,o q ] [ σ ,o u ,o v ]
    ,o
    v
    ≡⟨ cong₂ (λ x y → x ⊚ (σ ,o u ,o v) ,o y [ (σ ,o u ,o v) ] ,o v) ▹β₁ ▹β₂ ⟩
    (p ⊚ p) ⊚ (σ ,o u ,o v) ,o q [ σ ,o u ,o v ] ,o v
    ≡⟨ cong₂ (λ x y → x ,o y ,o v) ass ▹β₂ ⟩
    p ⊚ (p ⊚ (σ ,o u ,o v)) ,o v ,o v
    ≡⟨ cong (λ x → p ⊚ x ,o v ,o v) ▹β₁ ⟩
    p ⊚ (σ ,o u) ,o v ,o v
    ≡⟨ cong (λ x → x ,o v ,o v) ▹β₁ ⟩
    σ ,o v ,o v ∎