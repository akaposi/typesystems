{-# OPTIONS --prop --rewriting #-}

module gy04-1 where

open import Lib
open import NatBoolAST

{-
dependens modellek
-}

L1 : Model
L1 = record
  { Tm     = ℕ
  ; true   = 1
  ; false  = 1
  ; ite    = λ t t' t'' → t + t' + t''
  ; num    = λ _ → 1
  ; isZero = λ t → t
  ; _+o_   = _+_
  }
module L1 = Model L1
L2 : Model
L2 = record
  { Tm     = ℕ
  ; true   = 0
  ; false  = 0
  ; ite    = λ t t' t'' → 2 + t + t' + t''
  ; num    = λ _ → 0
  ; isZero = λ t → t
  ; _+o_   = λ t t' → 1 + t + t'
  }
module L2 = Model L2

-- FELADAT: Bizonyítsd, hogy minden szintaxisbeli termre L1 modell ugyanazt csinálja, mint 1 + L2.
L1L2 : DepModel {lzero}
L1L2 = ?

module L1L2 = DepModel L1L2

twolengths : ∀ t → L1.⟦ t ⟧ ≡ 1 + L2.⟦ t ⟧
twolengths t = {!!}

module isZeroInjectivity where

  D : DepModel {lzero}
  D = record
    { Tm∙     = {!!}
    ; true∙   = {!!}
    ; false∙  = {!!}
    ; ite∙    = {!!}
    ; num∙    = {!!}
    ; isZero∙ = {!!}
    ; _+o∙_   = {!!}
    }
  module D = DepModel D

  isZeroInj : ∀{t t'} → I.isZero t ≡ I.isZero t' → t ≡ t'
  isZeroInj e = cong D.⟦_⟧ e

module isZeroInjective where

  -- Az iniciális modellben most már tudjuk, hogy az isZero injektív. Adj meg egy másik modellt, amelyben szintén injektív!
  M : Model {lzero}
  M = record
    { Tm     = ℕ
    ; true   = {!!}
    ; false  = {!!}
    ; ite    = {!!}
    ; num    = {!!}
    ; isZero = {!!}
    ; _+o_   = {!!}
    }
  open Model M

  inj : ∀ t t' → isZero t ≡ isZero t' → t ≡ t'
  inj = {!!}

module isZeroNotInjective where

  -- Adj meg egy modellt, amelyben az isZero operátor nem injektív!
  M : Model {lzero}
  M = ?
  open Model M

  notInj : ¬ (∀ t t' → isZero t ≡ isZero t' → t ≡ t')
  notInj inj = {!!}

-- FELADAT: mutasd meg, hogy a _+_ operátor mindkét paraméterében injektív! (a módszer ugyanaz, mint isZero-nál)
module +Injectivity where
  open I

  D : DepModel {lzero}
  D = {!!}

  +inj : ∀{u₀ u₁ v₀ v₁} → u₀ + v₀ ≡ u₁ + v₁ → u₀ ≡ u₁ × v₀ ≡ v₁
  +inj = {!!}