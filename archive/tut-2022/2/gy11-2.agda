{-# OPTIONS --prop --rewriting #-}

module gy11-2 where

open import Lib
open import STT
open I

add2 : Tm ◇ (Nat ⇒ Nat) -- \=> : ⇒
add2 = lam (v0 +o num 2) -- λx.x+2

and : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
and = lam (lam (ite v1 v0 false)) -- λx y.if x then y else false

and-test1 : and $ true $ true ≡ true
and-test1 = 
  and $ true $ true 
  ≡⟨ refl ⟩
  ((lam (lam (ite (q [ p ]) q false))) $ true) $ true
  ≡⟨ cong (_$ true) ⇒β ⟩
  (lam (ite (q [ p ]) q false) [ id ,o true ]) $ true
  ≡⟨ cong (_$ true) lam[] ⟩
  lam (ite (q [ p ]) q false [ (id ,o true) ⊚ p ,o q ]) $ true
  ≡⟨ ⇒β ⟩
  ite (q [ p ]) q false [ (id ,o true) ⊚ p ,o q ] [ id ,o true ]
  ≡⟨ cong (_[ id ,o true ]) ite[] ⟩
  ite (q [ p ] [ (id ,o true) ⊚ p ,o q ])
      (q [ (id ,o true) ⊚ p ,o q ])
      (false [ (id ,o true) ⊚ p ,o q ])
  [ id ,o true ]
  ≡⟨ cong₃ (λ x y z → ite x y z [ id ,o true ]) ([∘] ⁻¹) ▹β₂ false[] ⟩
  ite (q [ p ⊚ ((id ,o true) ⊚ p ,o q) ]) q false [ id ,o true ]
  ≡⟨ cong (λ x → ite (q [ x ]) q false [ id ,o true ]) ▹β₁ ⟩
  ite (q [ (id ,o true) ⊚ p ]) q false [ id ,o true ]
  ≡⟨ cong (λ x → ite x q false [ id ,o true ]) [∘] ⟩
  ite (q [ id ,o true ] [ p ]) q false [ id ,o true ]
  ≡⟨ cong (λ x → ite (x [ p ]) q false [ id ,o true ]) ▹β₂ ⟩
  ite (true [ p ]) q false [ id ,o true ]
  ≡⟨ cong (λ x → ite x q false [ id ,o true ]) true[] ⟩
  ite true q false [ id ,o true ]
  ≡⟨ cong (_[ id ,o true ]) iteβ₁ ⟩
  q [ id ,o true ]
  ≡⟨ ▹β₂ ⟩
  true ∎
  
  
and-test2 : and $ false $ true ≡ false
and-test2 = {!   !}

and'' : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
and'' = lam (ite v0 (lam v0) (lam false))
-- λ x → if x then (λ y → y) else (λ _ → false)

{-
and : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
and = lam (lam (ite v1 v0 false))
-}
-- External and
and' : Tm ◇ Bool → Tm ◇ Bool → Tm ◇ Bool
and' = λ x → λ y → ite x y false

and'-test1 : and' true true ≡ true
and'-test1 = iteβ₁

and'-test2 : and' false true ≡ false
and'-test2 = iteβ₂
----------------------------------
neg : Tm ◇ (Bool ⇒ Bool)
neg = lam (ite v0 false true)

neg-test1 : neg $ true ≡ false
neg-test1 = 
  neg $ true
  ≡⟨ refl ⟩
  lam (ite q false true) $ true
  ≡⟨ ⇒β ⟩
  ite q false true [ id ,o true ]
  ≡⟨ ite[] ⟩
  ite (q [ id ,o true ]) (false [ id ,o true ]) (true [ id ,o true ])
  ≡⟨ cong₃ ite ▹β₂ false[] true[] ⟩
  ite true false true
  ≡⟨ iteβ₁ ⟩
  false ∎

neg-test2 : neg $ false ≡ true
neg-test2 = {!!}

infixl 6  _∘o_
_∘o_ : ∀{Γ A B C} → Tm Γ (B ⇒ C) → Tm Γ (A ⇒ B) → Tm Γ (A ⇒ C) -- (b -> c) -> (a -> b) -> (a -> c)
f ∘o g = lam (f [ p ] $ (g [ p ] $ v0)) -- (f ∘ g) := λx.f(g(x))

ido : ∀{Γ A} → Tm Γ (A ⇒ A)
ido = lam v0

apply3 : Tm ◇ ((Bool ⇒ Bool) ⇒ Bool ⇒ Bool)
apply3 = lam (lam (v1 $ (v1 $ (v1 $ v0))))

apply3-test : apply3 $ neg $ true ≡ false
apply3-test = {!!}

apply3-test' : (t : Tm ◇ (Bool ⇒ Bool))(u : Tm ◇ Bool) → apply3 $ t $ u ≡ t $ (t $ (t $ u))
apply3-test' = {!!}

apply3-extra : (t : Tm ◇ (Bool ⇒ Bool))(u : Tm ◇ Bool) → apply3 $ t $ u ≡ t $ u
apply3-extra = {!!}

isZero' : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
isZero' = lam (isZero v0) -- λx.isZero x

isZero'β₁ : ∀{Γ} → isZero' {Γ} $ num 0 ≡ true
isZero'β₁ =
  lam (isZero q) $ num 0
  ≡⟨ ⇒β ⟩
  isZero q [ id ,o num 0 ]
  ≡⟨ isZero[] ⟩
  isZero (q [ id ,o num 0 ])
  ≡⟨ cong isZero ▹β₂ ⟩
  isZero (num 0)
  ≡⟨ isZeroβ₁ ⟩
  true ∎


isZero'β₂ : ∀{Γ} → isZero' {Γ} $ num 1 ≡ false
isZero'β₂ = 
  lam (isZero q) $ num 1
  ≡⟨ ⇒β ⟩
  isZero q [ id ,o num 1 ]
  ≡⟨ isZero[] ⟩
  isZero (q [ id ,o num 1 ])
  ≡⟨ cong isZero ▹β₂ ⟩
  isZero (num 1)
  ≡⟨ isZeroβ₂ ⟩
  false ∎

Four = Bool ⇒ Bool

one two three four : Tm ◇ Four
one = ido
two = neg
three = lam true
four = lam false

one' two' three' four' : ∀{Γ} → Tm Γ Four
one'   = lam (ite v0 false true)
two'   = lam v0
three' = lam true
four'  = lam false

one'[] : ∀{Γ Δ}{γ : Sub Δ Γ} → one' [ γ ] ≡ one'
one'[] {γ = γ} =
  lam (ite q false true) [ γ ]
  ≡⟨ lam[] ⟩
  lam (ite q false true [ γ ⊚ p ,o q ])
  ≡⟨ cong lam ite[] ⟩
  lam
    (ite (q [ γ ⊚ p ,o q ])
    (false [ γ ⊚ p ,o q ])
    (true [ γ ⊚ p ,o q ]))
  ≡⟨ cong₃ (λ x y z → lam (ite x y z)) ▹β₂ false[] true[] ⟩
  lam (ite q false true) ∎

two'[] : ∀{Γ Δ}{γ : Sub Δ Γ} → two' [ γ ] ≡ two'
two'[] {γ = γ} =
  lam q [ γ ]
  ≡⟨ lam[] ⟩
  lam (q [ γ ⊚ p ,o q ])
  ≡⟨ cong lam ▹β₂ ⟩
  lam q ∎


three'[] : ∀{Γ Δ}{γ : Sub Δ Γ} → three' [ γ ] ≡ three'
three'[] {γ = γ} =
  lam true [ γ ]
  ≡⟨ lam[] ⟩
  lam (true [ γ ⊚ p ,o q ])
  ≡⟨ cong lam true[] ⟩
  lam true ∎
  
four'[] : ∀{Γ Δ}{γ : Sub Δ Γ} → four' [ γ ] ≡ four'
four'[] {γ = γ} =
  lam false [ γ ]
  ≡⟨ lam[] ⟩
  lam (false [ γ ⊚ p ,o q ])
  ≡⟨ cong lam false[] ⟩
  lam false ∎
-- also for one', two', three'

case' : ∀{Γ A} → Tm Γ Four → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A
case' t a0 a1 a2 a3 = ite (t $ false)
  (ite (t $ true)
    a3
    a2)
  (ite (t $ true)
    a1
    a0)

case'β1 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' one' a1 a2 a3 a4 ≡ a3
case'β1 {a1 = a1} {a2} {a3} {a4} = 
  ite (lam (ite q false true) $ false)
    (ite (lam (ite q false true) $ true) a4 a3)
    (ite (lam (ite q false true) $ true) a2 a1)
  ≡⟨ cong₃ (λ x y z → ite x (ite y a4 a3) (ite z a2 a1)) (⇒β ◾ ite[]) (⇒β ◾ ite[]) (⇒β ◾ ite[]) ⟩
  ite
    (ite (q [ id ,o false ]) (false [ id ,o false ]) (true [ id ,o false ]))
    (ite (ite (q [ id ,o true ]) (false [ id ,o true ]) (true [ id ,o true ])) a4 a3)
    (ite (ite (q [ id ,o true ]) (false [ id ,o true ]) (true [ id ,o true ])) a2 a1)
  ≡⟨ cong₃ (λ x y z → ite x (ite y a4 a3) (ite z a2 a1)) (cong₃ ite ▹β₂ false[] true[]) (cong₃ ite ▹β₂ false[] true[]) (cong₃ ite ▹β₂ false[] true[]) ⟩
  ite 
    (ite false false true)
    (ite (ite true false true) a4 a3)
    (ite (ite true false true) a2 a1)
  ≡⟨ cong₃ (λ x y z → ite x (ite y a4 a3) (ite z a2 a1)) iteβ₂ iteβ₁ iteβ₁ ⟩
  ite true (ite false a4 a3) (ite false a2 a1)
  ≡⟨ iteβ₁ ⟩
  ite false a4 a3 
  ≡⟨ iteβ₂ ⟩
  a3  ∎
case'β2 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' two' a1 a2 a3 a4 ≡ a2
case'β2 {a1 = a1} {a2} {a3} {a4} = 
  ite (lam q $ false) (ite (lam q $ true) a4 a3) (ite (lam q $ true) a2 a1)
  ≡⟨ cong₂ (λ x y → ite x (ite (lam q $ true) a4 a3) (ite y a2 a1)) (⇒β ◾ ▹β₂) (⇒β ◾ ▹β₂) ⟩
  ite false (ite (lam q $ true) a4 a3) (ite true a2 a1)
  ≡⟨ iteβ₂ ⟩
  ite true a2 a1
  ≡⟨ iteβ₁ ⟩
  a2 ∎
case'β3 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' three' a1 a2 a3 a4 ≡ a4
case'β3 {a1 = a1} {a2} {a3} {a4} = 
  ite (lam true $ false) (ite (lam true $ true) a4 a3) (ite (lam true $ true) a2 a1)
  ≡⟨ cong₂ (λ x y → ite x (ite y a4 a3) (ite (lam true $ true) a2 a1)) (⇒β ◾ true[]) (⇒β ◾ true[]) ⟩
  ite true (ite true a4 a3) (ite (lam true $ true) a2 a1)
  ≡⟨ iteβ₁ ⟩
  ite true a4 a3
  ≡⟨ iteβ₁ ⟩
  a4 ∎
case'β4 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' four' a1 a2 a3 a4 ≡ a1
case'β4 {a1 = a1} {a2} {a3} {a4} = 
  ite (lam false $ false) (ite (lam false $ true) a4 a3) (ite (lam false $ true) a2 a1)
  ≡⟨ cong₂ (λ x y → ite x (ite (lam false $ true) a4 a3) (ite y a2 a1)) (⇒β ◾ false[]) (⇒β ◾ false[]) ⟩
  ite false (ite (lam false $ true) a4 a3) (ite false a2 a1)
  ≡⟨ iteβ₂ ⟩
  ite false a2 a1
  ≡⟨ iteβ₂ ⟩
  a1 ∎
{-
(Nat ⇒ Bool)           ←       Nat

0 00010100101001001..
1 111010010000000000...
2 000000000000000000...
3 101010101010101010....
4 111111111111111111...
5 1111111111100000011...

új 101100
...
-}