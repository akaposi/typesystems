{-# OPTIONS --prop --rewriting #-}

module gy06-2 where

open import Lib

module NatBool-with-equational-theory where

  open import NatBool
  open I

  eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  eq-9 =
    (num 3 +o ite (isZero (num 2)) (num 1) (num 0))
      ≡⟨ cong (λ x → num 3 +o ite x (num 1) (num 0)) I.isZeroβ₂ ⟩
    (num 3 +o ite false (num 1) (num 0))
      ≡⟨ cong (num 3 +o_) I.iteβ₂ ⟩
    (num 3 +o num 0)
      ≡⟨ I.+β ⟩
    num 3
      ∎

  eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  eq-10 =
    (ite false (num 1 +o num 1) (num 0) +o num 0)
      ≡⟨ cong (_+o num 0) I.iteβ₂ ⟩
    (num 0 +o num 0)
      ≡⟨ I.+β ⟩
    num 0
      ∎

  eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  eq-11 =
    ite (isZero (num 0 +o num 1)) false (isZero (num 0))
      ≡⟨ cong (λ x → ite (isZero x) false (isZero (num 0))) I.+β 
        ◾ 
         cong (λ x → ite x false (isZero (num 0))) I.isZeroβ₂ ⟩
    ite false false (isZero (num 0))
      ≡⟨ I.iteβ₂ ◾ I.isZeroβ₁ ⟩
    true
      ∎

module injectivity where

  module NatBool where
    open import NatBool hiding (St)

    -- standard model
    St : Model
    St = record
      { Ty       = Set
      ; Tm       = λ A → A
      ; Nat      = ℕ
      ; Bool     = 𝟚
      ; true     = tt
      ; false    = ff
      ; ite      = if_then_else_
      ; num      = λ n → n
      ; isZero   = λ {zero → tt
                    ; (suc n) → ff}
      ; _+o_     = _+_
      ; iteβ₁    = refl
      ; iteβ₂    = refl
      ; isZeroβ₁ = refl
      ; isZeroβ₂ = refl
      ; +β       = refl
      }
    module St = Model St

    module equations where
      open St

      -- a standard modellben az eq-0...eq-12 egyenlosegek definicio szerint teljesulnek (a metaelmeletbol kovetkeznek)
      -- pl.
      eq-4 : isZero (num 3 +o num 1) ≡ false
      eq-4 = refl

      eq-6 : ite true (isZero (num 0)) false ≡ true
      eq-6 = refl

      eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
      eq-7 = refl

      eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
      eq-8 = refl
      
      eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
      eq-9 = refl

    open I

    -- bizonyitsd be, hogy NatBool.I.num injektiv!
    numInj : ∀{m n} → num m ≡ num n → m ≡ n
    numInj e = cong St.⟦_⟧t e -- hasznald az St-be valo kiertekelest!

    1≢2 : 1 ≢ 2 -- \==n : ≢
    1≢2 ()

    -- bizonyitsd be, hogy NatBool.I.isZero nem injektiv!
    notInj : ¬ ((t t' : Tm Nat) → isZero t ≡ isZero t' → t ≡ t')
    notInj e = 1≢2 (numInj (e (num 1) (num 2)
      (isZero (num 1)
      ≡⟨ I.isZeroβ₂ ⟩
      false
      ≡⟨ I.isZeroβ₂ ⁻¹ ⟩
      isZero (num 2) ∎))) -- hasznald a num injektivitasat ill. az isZeroβ₂-t!

module integers where

  open import Int hiding (N; SucNf; PredNf; ⌜_⌝; norm; testnorm; stab; ⌜Suc⌝; ⌜Pred⌝; Comp; comp)
  open I
  {-
  data ℤ : Set where
    Zero : ℤ
    Suc : ℤ → ℤ
    Pred : ℤ → ℤ
  SucPred : (z : ℤ) → Suc (Pred z) ≡ z
  PredSuc : (z : ℤ) → Pred (Suc z) ≡ z
  -}
  one one' : Z
  one  = Suc I.Zero
  one' = Suc (Pred (Suc I.Zero))

  one= : one ≡ one'
  one= = cong Suc (PredSuc I.Zero ⁻¹)
  
  one=' : one ≡ one'
  one=' = SucPred (Suc I.Zero) ⁻¹
  
  -2' -2'' : Z
  -2'  = Pred (Pred I.Zero)
  -2'' = Pred (Suc (Pred (Suc (Pred (Suc (Pred (Pred I.Zero)))))))

  -2= : -2' ≡ -2''
  -2= = {!   !}

  -- nezd meg, mi az, hogy Model:
  Model' = Model

  {-
  data ℤNf : Set where
    Zero : ℤNf
    +Suc : ℕ → ℤNf
    -Suc : ℕ → ℤNf
  
  +Suc 0 = 1
  -Suc 0 = -1
  -Suc 10 = -11
  +Suc 12 = 13
  -}
  -- nezd meg, mik a normal formak:
  Nf' = Nf

  ⌜_⌝ : Nf → I.Z -- \cul : ⌜ | \cur : ⌝
  ⌜ -Suc zero     ⌝ = I.Pred I.Zero
  ⌜ -Suc (suc n)  ⌝ = I.Pred ⌜ -Suc n ⌝
  ⌜ Zero          ⌝ = I.Zero
  ⌜ +Suc zero     ⌝ = I.Suc I.Zero
  ⌜ +Suc (suc n)  ⌝ = I.Suc ⌜ +Suc n ⌝

  SucNf : Nf → Nf
  SucNf (-Suc zero) = Nf.Zero
  SucNf (-Suc (suc x)) = -Suc x
  SucNf Nf.Zero = +Suc 0
  SucNf (+Suc x) = +Suc (suc x)

  PredNf : Nf → Nf
  PredNf (-Suc x) = -Suc (suc x)
  PredNf Nf.Zero = -Suc 0
  PredNf (+Suc zero) = Nf.Zero
  PredNf (+Suc (suc x)) = +Suc x

  -- egy normal formakbol allo modell
  N : Model
  N = record
    { Z       = Nf
    ; Zero    = Nf.Zero
    ; Suc     = SucNf
    ; Pred    = PredNf
    ; SucPred = λ {(-Suc x) → refl
                 ; Nf.Zero → refl
                 ; (+Suc zero) → refl
                 ; (+Suc (suc x)) → refl}
    ; PredSuc = λ {(-Suc zero) → refl
                 ; (-Suc (suc x)) → refl
                 ; Nf.Zero → refl
                 ; (+Suc x) → refl}
    }
  module N = Model N

  norm : I.Z → Nf
  norm = N.⟦_⟧

  testnorm1 : norm one ≡ norm one'
  testnorm1 = refl
  testnorm2 : norm -2' ≡ norm -2''
  testnorm2 = refl
  testnorm3 :  ⌜ norm (I.Pred (I.Pred (I.Suc (I.Pred (I.Pred (I.Pred (I.Suc I.Zero))))))) ⌝ ≡
              I.Pred (I.Pred (I.Pred I.Zero))
  testnorm3 = refl

  stab : (v : Nf) → norm ⌜ v ⌝ ≡ v
  stab (-Suc zero) = refl
  stab (-Suc (suc x)) = cong PredNf (stab (-Suc x))
  stab Nf.Zero = refl
  stab (+Suc zero) = refl
  stab (+Suc (suc x)) = cong SucNf (stab (+Suc x))

  ⌜Suc⌝ : (v : Nf) → ⌜ SucNf v ⌝ ≡ I.Suc ⌜ v ⌝
  ⌜Suc⌝ v = {!   !}

  ⌜Pred⌝ : (v : Nf) → ⌜ PredNf v ⌝ ≡ I.Pred ⌜ v ⌝
  ⌜Pred⌝ v = {!   !}

  Comp : DepModel
  Comp = record
    { Z∙       = λ i → Lift (⌜ norm i ⌝ ≡ i)
    ; Zero∙    = {!   !}
    ; Suc∙     = {!   !}
    ; Pred∙    = {!   !}
    ; SucPred∙ = {!   !}
    ; PredSuc∙ = {!!}
    }
  module Comp = DepModel Comp

  comp : (i : I.Z) → ⌜ norm i ⌝ ≡ i
  comp i = un (Comp.⟦ i ⟧)
