{-# OPTIONS --prop --rewriting #-}

module gy10-2 where

open import Lib
open import Def
open I

-- explain Var, Ne, Nf

{-
Var Γ A
  q, q[p], q[p][p], ...

Ne Γ A
  var x, isZero tne, ite tne ane bne, tne + t'ne, tne + num n, num n + tne 

Nf Γ A
  neu t, true, false, num n
-}

-- check what eval, norm and ⌜norm⌝ gives for this term
t : Tm ◇ Bool
t = isZero (ite true (num 1) (num 0))

evalt : Lift ⊤ → 𝟚
evalt = St.⟦ t ⟧t

normt : Nf ◇ Bool
normt = norm t

⌜norm⌝t : Tm ◇ Bool
⌜norm⌝t = ⌜ norm t ⌝Nf

complt : ⌜ norm t ⌝Nf ≡ t
complt = compl t

-- check what eval, norm and ⌜norm⌝ gives for this term
t1 : Tm (◇ ▹ Nat) Bool
t1 = isZero (ite (isZero (num 2 +o num 3)) q (q +o num 2))

evalt1 : Lift ⊤ × ℕ → 𝟚
evalt1 = St.⟦ t1 ⟧t

normt1 : Nf (◇ ▹ Nat) Bool
normt1 = norm t1

⌜norm⌝t1 : Tm (◇ ▹ Nat) Bool
⌜norm⌝t1 = ⌜ norm t1 ⌝Nf

complt1 : ⌜ norm t1 ⌝Nf ≡ t1
complt1 = compl t1

-- how to use completeness
t4 = def (false {◇}) (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
t5 = ite true (num {◇} 3) (num 0)

var-eq-5 : def false (def (num 2) (ite v1 (num 0) (num 1 +o v0))) ≡ num {◇} 3
var-eq-5 =
  def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
  ≡⟨ refl ⟩
  ite (q [ p ]) (num 0) (num 1 +o q) [ id ,o num 2 ] [ id ,o false ]
  ≡⟨ cong _[ id ,o false ] ite[] ⟩
  ite
    (q [ p ] [ id ,o num 2 ])
    (num 0 [ id ,o num 2 ])
    ((num 1 +o q) [ id ,o num 2 ])
      [ id ,o false ]
  ≡⟨ cong₃ (λ x y z → ite x y z [ id ,o false ])
    ([∘] ⁻¹)
    num[]
    +[] ⟩
  ite (q [ p ⊚ (id ,o num 2) ]) 
      (num 0)
      ((num 1 [ id ,o num 2 ]) +o (q [ id ,o num 2 ]))
        [ id ,o false ]
  ≡⟨ cong₂ (λ x z → ite (q [ x ]) (num 0) z [ id ,o false ])
      ▹β₁
      (cong₂ _+o_ num[] ▹β₂) ⟩
  ite (q [ id ]) (num 0) (num 1 +o num 2) [ id ,o false ]
  ≡⟨ cong (λ x → ite (q [ id ]) (num 0) x [ id ,o false ]) +β ⟩
  ite (q [ id ]) (num 0) (num 3) [ id ,o false ]
  ≡⟨ ite[] ⟩
  ite (q [ id ] [ id ,o false ]) (num 0 [ id ,o false ]) (num 3 [ id ,o false ])
  ≡⟨ cong₃ ite ([∘] ⁻¹) num[] num[] ⟩
  ite (q [ id ⊚ (id ,o false) ]) (num 0) (num 3)
  ≡⟨ cong (λ x → ite (q [ x ]) (num 0) (num 3)) idl ⟩
  ite (q [ id ,o false ]) (num zero) (num 3)
  ≡⟨ cong (λ x → ite x (num zero) (num 3)) ▹β₂ ⟩
  ite false (num 0) (num 3)
  ≡⟨ iteβ₂ ⟩
  num 3 ∎

-- prove that two terms are equal using compl
var-eq-5' : t4 ≡ t5
var-eq-5' =
  t4
    ≡⟨ refl ⟩
  ite (q [ p ]) (num 0) (num 1 +o q) [ id ,o num 2 ] [ id ,o false ]
    ≡⟨ compl t4 ⁻¹ ⟩
  ⌜ norm t4 ⌝Nf
    ≡⟨ refl ⟩
  num 3
    ≡⟨ refl ⟩
  ⌜ norm t5 ⌝Nf
    ≡⟨ compl t5 ⟩
  ite true (num 3) (num 0)
    ≡⟨ refl ⟩
  t5
      ∎

novar◇ : ∀{A} → Var ◇ A → ⊥
novar◇ ()

data Tree : Set where
  node : Tree → Tree → Tree

notree : Tree → ⊥
notree (node t₁ t₂) = notree t₁

-- prove that there are no neutral terms in the empty context
-- (pattern match on t)
noneu◇ : ∀{A} → Ne ◇ A → ⊥
noneu◇ (iteNe t₁ x x₁) = noneu◇ t₁
noneu◇ (isZeroNe t₁) = noneu◇ t₁
noneu◇ (t₁ +NeNe t₂) = noneu◇ t₂
noneu◇ (t₁ +NeNf x) = noneu◇ t₁
noneu◇ (x +NfNe t₁) = noneu◇ t₁

exfalso' : {A : Set} → ⊥ → A
exfalso' ()

-- prove that there are only two elements of Bool normal forms in the empty context
case◇BoolNf : (P : Tm ◇ Bool → Set) → P true → P false → (t : Nf ◇ Bool) → P ⌜ t ⌝Nf
case◇BoolNf P pt pf (neu x) with noneu◇ x
... | ()
case◇BoolNf P pt pf trueNf = pt
case◇BoolNf P pt pf falseNf = pf

-- prove that there are only two elements of Bool terms in the empty context (use case◇BoolNf and compl)
-- There will be problems with the type, so it needs to be trasported.
-- Use normalisation on t, it will be normal form, use completeness.
case◇Bool : (P : Tm ◇ Bool → Set) → P true → P false → (t : Tm ◇ Bool) → P t
case◇Bool P pt pf t =  transp P (compl t) (case◇BoolNf P pt pf (norm t))

-- use case◇Bool on t
ite-diag◇ : (t : Tm ◇ Bool)(A : Ty)(u : Tm ◇ A) → ite t u u ≡ u
ite-diag◇ t A u = {!   !}

helper-ex4 : neu (isZeroNe (var (vz {◇}) +NeNf 2)) ≡ falseNf → ⊥
helper-ex4 ()

-- prove that these two terms are not equal in the syntax (hint: they have different normal forms)
ex4 : isZero (q {◇} +o num 2) ≢ false
ex4 e = {!   !}

-- cong
not= : ∀{Γ A}(t1 t2 : Tm Γ A) → ¬ (norm t1 ≡ norm t2) → ¬ (t1 ≡ t2)
not= t1 t2 ne e = {!   !}

-- prove that ite-diag◇ is false in arbitrary contexts -- ite q true true ≠ true
-- we need a counterexample
ite-diag : ((Γ : Con)(t : Tm Γ Bool)(A : Ty)(u : Tm Γ A) → ite t u u ≡ u) → ⊥
ite-diag f = {!   !}

t2 t3 : Tm (◇ ▹ Bool ▹ Nat) Nat
t2 = ite v1 v0 (num 1)
t3 = ite (ite true v1 false) (v0) (num 0 +o num 1)

-- decide if t2 and t3 are equal or not!

t2=t3 : Dec (Lift (t2 ≡ t3))
t2=t3 = {!   !}