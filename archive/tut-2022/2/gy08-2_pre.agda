{-# OPTIONS --prop --rewriting #-}

module gy08-2 where

module DefWT where

  open import DefWT
  open I
  -- ◇ \di2     ▹  \t6
  tm-0 : {!   !}
  tm-0 = isZero (ite v2 (v1 +o v0) v1)

  tm-1 : {!   !}
  tm-1 = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  tm-2 : {!   !}
  tm-2 = ite v1 (isZero v0) (isZero v2)

  tm-3 : {!   !}
  tm-3 = ite (ite v2 (isZero v0) v2) v1 v1

open import Lib
open import Def
open I

-- def (num 2) v0 = v0[v0 ↦ num 2] = num 2
-- def (num 2) (v0 +o v0) = (v0 +o v0)[v0 ↦ num 2] = num 2 +o num 2 = num 4
-- def (num 2) (def (num 1) (v0 +o v1)) = (def (num 1) (v0 +o v1))[v0 ↦ num 2] =
--   (v0 +o v1)[v0 ↦ num 1][v0 ↦ num 2] = ... =
--   (v0 +o v1)[v0 ↦ num 1, v1 ↦ num 2] = num 1 +o num 2 = num 3
-- def (num 2) (def (num 1) (v0 +o v1)) = (def (num 1) (v0 +o v1))[v0 ↦ num 2] =
--   (v0 +o v1)[v0 ↦ num 1][v0 ↦ num 2] =
--   (num 1 +o v0)[v0 ↦ num 2] = num 1 +o num 2 = num 3

-- def (num 2) (def (num 1) (v0 +o v1)) = (def (num 1) (v0 +o v1))[ε ,o num 2] = ...
-- \__________________________________/   \______________________/\__________/
--    : Tm ◇ Nat                         : Tm (◇ ▹ Nat) Nat        : Sub ◇ (◇ ▹ Nat)

-- def t u = u[id ,o t]

-- substitution calculus, simply typed CATEGORY WITH FAMILIES

-- Ty, Con
-- Sub : Con → Con → Set      Sub Γ (◇ ▹ A₀ ▹ A₁ ▹ A₂) = 3 db term, egy A₀ tipusú, egy A₁ tipusú es egy A₂ tipusú
-- (ε ,o a₀ ,o a₁ ,o a₂) : Sub Γ (◇ ▹ A₀ ▹ A₁ ▹ A₂), ha  a₀ : Tm Γ A₀, a₁ : Tm Γ A₁, a₂ : Tm Γ A₂
--        termek sorozata (listája)
-- id : Sub Γ Γ
-- ε  : Sub Γ ◇
-- _,o_ : Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
-- _[_] : Tm Γ A → Sub Δ Γ → Tm Δ A
-- Tm : Con → Ty → Set        Tm Γ A -- A típusú termek, amelyekben Γ-nak megfelelő változók lehetnek
--   ite v0 (num 1 +o v1) v1 : Tm (◇ ▹ Nat ▹ Bool) Nat

-- σ : Sub (◇ ▹ Nat ▹ Bool) (◇ ▹ Bool ▹ Nat ▹ Nat)
-- σ = (ε ,o isZero (num 0 +o v1) ,o v1 ,o ite v0 v1 (num 2))
-- t : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Nat
-- t = v0 +o (v1 +o ite v2 (num 0) (num 1))
-- t [ σ ] : Tm (◇ ▹ Nat ▹ Bool) Nat
-- (v0 +o (v1 +o ite v2 (num 0) (num 1))) [ (ε ,o isZero (num 0 +o v1) ,o v1 ,o ite v0 v1 (num 2)) ] =
--   (ite v0 v1 (num 2))) +o (v1 +o ite (isZero (num 0 +o v1)) (num 0) (num 1))

module ex1 where
  Γ : Con
  Γ = ◇ ▹ Bool

  t : Tm Γ Nat
  t = ite v0 (num 1) (num 0)

  γ : Sub ◇ Γ
  γ = ε ,o true

  t[γ] : t [ γ ] ≡ num 1
  t[γ] =
    (t [ γ ])
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    num 1 ∎

  Δ : Con
  Δ = ◇ ▹ Nat ▹ Nat

  δ : Sub Γ Δ
  δ = ε ,o ite v0 (num 1) (num 2) ,o num 2

  u : Tm Δ Nat
  u = v0 +o v1

  u[δ] : u [ δ ] ≡
    num 2 +o ite v0 (num 1) (num 2)
  u[δ] =
    (u [ δ ])
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    num 2 +o ite v0 (num 1) (num 2) ∎

module ex2 where
  var-test-1 : (def {◇} true v0) ≡ true
  var-test-1 =
    def true v0
    ≡⟨ {!   !} ⟩
    true ∎
  
  var-test-2 : (def {◇} true (def false v0)) ≡ false
  var-test-2 =
    def true (def false v0)
    ≡⟨ {!!} ⟩
    false ∎
  
  var-test-3 : (def {◇} true (def false v1)) ≡ true
  var-test-3 =
    def true (def false v1)
    ≡⟨ {!!} ⟩
    true ∎
  
  task-sub : Sub ◇ (◇ ▹ Bool ▹ Nat)
  task-sub = {!!}
  
  task-eq : (ite v1 (v0 +o v0) v0) [ task-sub ] ≡ num 4
  task-eq = {!!}
  
  var-eq-4 : def (num 1) (v0 +o v0) ≡ num {◇} 2
  var-eq-4 =
    def (num 1) (v0 +o v0)
    ≡⟨ {!!} ⟩
    num 2 ∎
  
  var-eq-5 : def false (def (num 2) (ite v1 (num 0) (num 1 +o v0))) ≡ num {◇} 3
  var-eq-5 =
    def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
    ≡⟨ {!!} ⟩
    num 3 ∎
  
  var-eq-6 : def (num 0) (def (isZero v0) (ite v0 false true)) ≡ false {◇}
  var-eq-6 =
    def (num 0) (def (isZero v0) (ite v0 false true))
    ≡⟨ {!!} ⟩
    false ∎
  
  var-eq-7 : def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0))) ≡ num {◇} 2
  var-eq-7 =
    def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
    ≡⟨ {!!} ⟩
    num 2 ∎
  
  var-eq-8 : def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0))) ≡ false {◇}
  var-eq-8 =
    def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0)))
    ≡⟨ {!!} ⟩
    false ∎
  
  var-eq-9 : def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0))) ≡ num {◇} 2
  var-eq-9 =
    def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0)))
    ≡⟨ {!!} ⟩
    num 2 ∎
  
  swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
  swap = {!!}
  
  swapswap : {Γ : Con} → {A B : Ty} → swap ⊚ swap ≡ id {Γ ▹ A ▹ B}
  swapswap = {!!}
  
  sub-eq-1 : {Γ : Con} → {σ δ : Sub Γ ◇} → σ ≡ δ
  sub-eq-1 = {!!}
  
  sub-eq-2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → ε ⊚ σ ≡ ε
  sub-eq-2 = {!!}

  sub-eq-3 :
    {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
    (σ ,o t) ⊚ δ ≡ (σ ⊚ δ ,o t [ δ ])
  sub-eq-3 {Γ} {Δ} {Θ} {σ} {δ} {A} {t} =
    (σ ,o t) ⊚ δ
    ≡⟨ {!!} ⟩
    {!!}
    ≡⟨ {!!} ⟩
    (σ ⊚ δ ,o t [ δ ]) ∎
  
  sub-eq-4 :
    {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ⊚ p ,o q ])
  sub-eq-4 {σ = σ} {t = t} {u = u} =
    (def t u) [ σ ]
    ≡⟨ {!!} ⟩
    {!!}
    ≡⟨ {!!} ⟩
    def (t [ σ ]) (u [ σ ⊚ p ,o q ]) ∎