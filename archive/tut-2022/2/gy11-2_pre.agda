{-# OPTIONS --prop --rewriting #-}

module gy11-2 where

open import Lib
open import STT
open I

add2 : ?
add2 = ? -- λx.x+2

and : ?
and = ? -- λx y.if x then y else false

and-test1 : and $ true $ true ≡ true
and-test1 = ?
  
and-test2 : and $ false $ true ≡ false
and-test2 = ?

-- External and
and' : Tm ◇ Bool → Tm ◇ Bool → Tm ◇ Bool
and' = ?

and'-test1 : and' true true ≡ true
and'-test1 = {!!}

and'-test2 : and' false true ≡ false
and'-test2 = {!!}
----------------------------------
neg : Tm ◇ (Bool ⇒ Bool)
neg = ?

neg-test1 : neg $ true ≡ false
neg-test1 = {!!}

neg-test2 : neg $ false ≡ true
neg-test2 = {!!}

infixl 6  _∘o_
_∘o_ : ?
f ∘o g = ? -- (f ∘ g) := λx.f(g(x))

ido : ?
ido = ?

apply3 : Tm ◇ ((Bool ⇒ Bool) ⇒ Bool ⇒ Bool)
apply3 = ?

apply3-test : apply3 $ neg $ true ≡ false
apply3-test = {!!}

apply3-test' : (t : Tm ◇ (Bool ⇒ Bool))(u : Tm ◇ Bool) → apply3 $ t $ u ≡ t $ (t $ (t $ u))
apply3-test' = {!!}

apply3-extra : (t : Tm ◇ (Bool ⇒ Bool))(u : Tm ◇ Bool) → apply3 $ t $ u ≡ t $ u
apply3-extra = {!!}

isZero' : ?
isZero' = ? -- λx.isZero x

isZero'β₁ : ∀{Γ} → isZero' {Γ} $ num 0 ≡ true
isZero'β₁ = ?

isZero'β₂ : ∀{Γ} → isZero' {Γ} $ num 1 ≡ false
isZero'β₂ = {!!}

Four = Bool ⇒ Bool

one two three four : Tm ◇ Four
one = {!!}
two = {!!}
three = {!!}
four = ?

one' two' three' four' : ∀{Γ} → Tm Γ Four
one'   = lam (ite v0 false true)
two'   = lam (ite v0 true false)
three' = lam true
four'   = lam false

four'[] : ∀{Γ Δ}{γ : Sub Δ Γ} → four' [ γ ] ≡ four'
four'[] = ?
-- also for one', two', three'

case' : ∀{Γ A} → Tm Γ Four → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A
case' t a0 a1 a2 a3 = ite (t $ false)
  (ite (t $ true)
    a3
    a2)
  (ite (t $ true)
    a1
    a0)

case'β1 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' one' a1 a2 a3 a4 ≡ a1
case'β1 = {!!}
case'β2 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' two' a1 a2 a3 a4 ≡ a2
case'β2 = {!!}
case'β3 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' three' a1 a2 a3 a4 ≡ a3
case'β3 = {!!}
case'β4 : ∀{Γ A}{a1 a2 a3 a4 : Tm Γ A} → case' four' a1 a2 a3 a4 ≡ a4
case'β4 = {!!}
{-
(Nat ⇒ Bool)           ←       Nat

uj101000...

0 00010100101001001..
1 111010010000000000...
2 000000000000000000...
3 101010101010101010....
4 111111111111111111...
5 111111111110000001.1..
...
-}
