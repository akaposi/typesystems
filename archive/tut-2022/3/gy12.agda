{-# OPTIONS --prop --rewriting #-}

module gy12 where

open import Lib

open import Fin -- ⇒ , + , × , Unit , Empty

open I

-- caseo ====>caseo'<====

Bool : Ty
Bool = Unit +o Unit +o Unit
true false : ∀{Γ} → Tm Γ Bool
true = {!!}
false = {!!}
ite : ∀{A Γ} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
ite = {!!}
Boolβ₁ : ∀{A Γ}{u v : Tm Γ A} → ite true u v ≡ u
Boolβ₁ = {!!}
Boolβ₂ : ∀{A Γ}{u v : Tm Γ A} → ite false u v ≡ v
Boolβ₂ = {!!}
true[] : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
true[] = {!!}
false[] : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
false[] = {!!}
ite[] : ∀{A Γ}{t : Tm Γ Bool}{u v : Tm Γ A}{Δ}{γ : Sub Δ Γ} → ite t u v [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])
ite[] = {!!}

module nemMukodik where
  2* : Ty → Ty
  2* A = (A ×o A) +o Unit
  -- fst,snd,⟨_,_⟩ ?
  fst2* : ∀{Γ A} → Tm Γ (2* A) → Tm Γ A
  fst2* = {!!}

module mukodik-e? where
  2* : Ty → Ty
  2* A = (A ×o A) ×o A
  -- fst,snd,⟨_,_⟩,β₁,β₂,η
  fst2* : ∀{Γ A} → Tm Γ (2* A) → Tm Γ A
  fst2* = {!!}
  snd2* : ∀{Γ A} → Tm Γ (2* A) → Tm Γ A
  snd2* = {!!}
  ⟨_,_⟩2* : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ (2* A)
  ⟨_,_⟩2* = {!!}
  β₁ : ∀{Γ A}{a a' : Tm Γ A} → fst2* ⟨ a , a' ⟩2* ≡ a
  β₁ = {!!}
  β₂ : ∀{Γ A}{a a' : Tm Γ A} → fst2* ⟨ a , a' ⟩2* ≡ a
  β₂ = {!!}
  η  : ∀{Γ A}{w : Tm Γ (2* A)} → ⟨ fst2* w , snd2* w ⟩2* ≡ w
  η = ?

rec× : {Γ : Con}{A B C : Ty} → Tm (Γ ▹ A ▹ B) C → Tm Γ (A ×o B) → Tm Γ C
rec× = {!!}
rec×β : {Γ : Con}{A B C : Ty}{t : Tm (Γ ▹ A ▹ B) C}{u : Tm Γ A}{v : Tm Γ B} → rec× t ⟨ u , v ⟩ ≡ t [ id ,o u ,o v ]
rec×β = {!!}
module St = Model St
⟦rec×β⟧ : {Γ : Con}{A B C : Ty}{t : Tm (Γ ▹ A ▹ B) C}{u : Tm Γ A}{v : Tm Γ B} → St.⟦ rec× t ⟨ u , v ⟩ ⟧t ≡ St.⟦ t [ id ,o u ,o v ] ⟧t
⟦rec×β⟧ = refl

Opt : Ty → Ty
Opt = {!!}
none : ∀{Γ A} → Tm Γ (Opt A)
none = {!!}
just : ∀{Γ A} → Tm Γ A → Tm Γ (Opt A)
just = {!!}
caseOpt : ∀{Γ A C} → Tm Γ C → Tm (Γ ▹ A) C → Tm Γ (Opt A) → Tm Γ C
caseOpt = {!!}
-- state and prove the β laws
-- state and prove the substitution laws

module comm-× (A B : Ty) where
  to : Tm ◇ (A ×o B ⇒ B ×o A)
  to = {!!}
  
  from : Tm ◇ (B ×o A ⇒ A ×o B)
  from = {!!}

  fromto : lam (to [ p ] $ (from [ p ] $ q)) ≡ lam q
  fromto = {!!}

  tofrom : lam (from [ p ] $ (to [ p ] $ q)) ≡ lam q
  tofrom = {!!}


module rid-+ (A : Ty) where
  to : Tm ◇ (A +o Empty ⇒ A)
  to = {!!}
  
  from : Tm ◇ (A ⇒ A +o Empty)
  from = {!!}

  fromto : lam (to [ p ] $ (from [ p ] $ q)) ≡ lam q
  fromto = {!!}

  tofrom : lam (from [ p ] $ (to [ p ] $ q)) ≡ lam q
  tofrom = {!!}

module comm-+ (A B : Ty) where
  to : Tm ◇ (A +o B ⇒ B +o A)
  to = {!!}
  
  from : Tm ◇ (B +o A ⇒ A +o B)
  from = {!!}

  fromto : lam (to [ p ] $ (from [ p ] $ q)) ≡ lam q
  fromto = {!!}

  tofrom : lam (from [ p ] $ (to [ p ] $ q)) ≡ lam q
  tofrom = {!!}

module dist+× (A B C : Ty) where
  to : Tm ◇ (A ×o (B +o C) ⇒ A ×o B +o A ×o C)
  to = {!!}
  
  from : Tm ◇ (A ×o B +o A ×o C ⇒ A ×o (B +o C))
  from = {!!}

  fromto : lam (to [ p ] $ (from [ p ] $ q)) ≡ lam q
  fromto = {!!}

  tofrom : lam (from [ p ] $ (to [ p ] $ q)) ≡ lam q
  tofrom = {!!}
