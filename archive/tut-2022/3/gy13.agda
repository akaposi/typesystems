{-# OPTIONS --prop --rewriting #-}

module gy13 where

open import Lib

module Empty where
  open import Fin
  open I
  Emptyη' : ∀{Γ A}(u v : Tm (Γ ▹ Empty) A) → u ≡ v
  Emptyη' u v = Emptyη {t = u} ◾ Emptyη {t = v} ⁻¹

-- con : Tm (Γ ▹ Empty) C ≅ ⊤ : des

open import Ind

open I

{-
5.2
---
Tree3    : Ty
leaf     : Tm Γ Nat → Tm Γ Tree3                                                    Tm (Γ ▹ Nat) Tree3
node     : Tm Γ Bool → Tm Γ Tree3 → Tm Γ Tree3 → Tm Γ Tree3 → Tm Γ Tree3            Tm (Γ ▹ Bool ▹ Tree3 ▹ Tree3 ▹ Tree3) Tree3
iteTree3 : Tm (Γ ▹ Nat) A → Tm (Γ ▹ Bool ▹ A ▹ A ▹ A) A → Tm Γ Tree3 → Tm Γ A
Tree3β₁  : iteTree3 u v (leaf t) = u [ id , t ]
Tree3β₂  : iteTree3 u v (node t t1 t2 t3) = v [ id , t , iteTree3 u v t1 , iteTree3 u v t2 , iteTree3 u v t3 ]
             t1 : Tm Γ Tree3                  ^ : Sub Γ (Γ ▹ Bool ▹ A ▹ A ▹ A)
             ?  : Tm Γ A

sum : Tm ◇ (Tree3 ⇒ Nat)
sum = lam (iteTree3 {Nat} q (q [p∘p] +o q[p] +o q) q)
sum = λx.iteTree3 (y.y) (a b c d.b+c+d) x
sum (leaf y) = y
sum (node a t1 t2 t3) = sum t1+sum t2+sum t3

5.3
---
Tree : Ty
leaf    : Tm Γ Tree
leaf'   : Tm Γ Tree
node2   : Tm Γ Tree → Tm Γ Tree → Tm Γ Tree
node3   : Tm Γ Tree → Tm Γ Tree → Tm Γ Tree → Tm Γ Tree
node3'  : Tm Γ Tree → Tm Γ Tree → Tm Γ Tree → Tm Γ Tree
node3'' : Tm Γ Tree → Tm Γ Tree → Tm Γ Tree → Tm Γ Tree
iteTree : Tm Γ A → Tm Γ A → Tm (Γ ▹ A ▹ A) A → Tm (Γ ▹ A ▹ A ▹ A) A → Tm (Γ ▹ A ▹ A ▹ A) A → Tm (Γ ▹ A ▹ A ▹ A) A → Tm Γ Tree → Tm Γ A

5.4
---
Tree : Ty
leaf : Tm Γ Tree
node : Tm Γ (Nat ⇒ Tree) → Tm Γ Tree
iteTree : Tm Γ A → Tm (Γ ▹ Nat ⇒ A) A → Tm Γ Tree → Tm Γ A

/\              /\\...\\\
 /\               /..\
/\                  /..\


5.5
---
A,B : Ty
Tree A B : Ty
leaf : Tm Γ A → Tm Γ B → Tm Γ (Tree A B)
iteTree : Tm (Γ ▹ A ▹ B) C → Tm Γ (Tree A B) → Tm Γ C
Treeβ : iteTree t (leaf u v) = t [ id , u , v ]

Tree := A × B
leaf u v := ⟨ u , v ⟩
iteTree t t1 := t [ id , fst t1 , snd t2 ]
Treeβ : iteTree t (leaf u v) = t [ id , fst ⟨ u , v ⟩ , snd ⟨ u , v ⟩ ] = t [ id , u , v ]

ha van szorzat tipus => Tree induktiv tipus
                     <=
A × B := Tree A B
⟨ u , v ⟩ := leaf u v
fst : Tm Γ (Tree A B) → Tm Γ A
fst t := iteTree (q[p]) t
snd t := iteTree q      t
β szabalyok OK
η szabaly NEM

Osszeg tipus:
  con : Tm (Γ ▹ A + B) C ≅ Tm (Γ ▹ A) C × Tm (Γ ▹ B) C : des
  con q : Tm (Γ ▹ A) (A+B) × Tm (Γ ▹ B) (A+B)

  con : Tm (Γ ▹ Bool) C ≅ Tm Γ C × Tm Γ C : ite
  con q : Tm Γ Bool × Tm Γ Bool
  β   : ite (con t) = t
  η   : con (ite (u,v)) = (u,v)
  -- Bool, + η szaballyal mukodik
  -- Empty, Nat ha van η szabalya, akkor nincs normalizalas

-}

-- Unit, Bool, Nat, List, Tree

*2+1 : Tm ◇ (Nat ⇒ Nat)
-- *2+1 = lam (iteNat (suco zeroo) (suco (suco q)) q)
-- x ↦ x-ben zero-t 1-re, suc-okat 2 db suc-ra csereljuk
-- x ↦ x-ben zero-t x-re csereljuk, suc-okat suc-ra, utana meg rarakunk egy suc-ot
-- *2+1 = lam (suco (iteNat q (suco q) q))
--     iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
*2+1 = lam (iteNat (suco q) (suco q) q)

test*2+1 : St.⟦ *2+1 $ suco (suco zeroo) ⟧t ≡ St.⟦ suco {◇} (suco (suco (suco (suco zeroo)))) ⟧t
test*2+1 = refl
test*2+1' : St.⟦ *2+1 ⟧t (mk triv) 3 ≡ 7
test*2+1' = refl

plus : Tm ◇ (Nat ⇒ Nat ⇒ Nat)
plus = lam (lam (iteNat (q [ p ]) (suco q) q))

testplus : St.⟦ plus ⟧t (mk triv) 3 4 ≡ 7
testplus = refl
testplus' : St.⟦ plus ⟧t (mk triv) 30 40 ≡ 70
testplus' = refl

prod : Tm ◇ (Nat ⇒ Nat ⇒ Nat)
prod = lam (lam (iteNat zeroo (plus [ ε ] $ q [ p ] [ p ] $ q) q))
-- prod x (suc y) = x * (1+y) = x + x * y = x + q = q[p][p]+q
-- prod x y = q
-- x = q [ p ] [ p ]
-- y = ?

testprod : St.⟦ prod ⟧t (mk triv) 3 4 ≡ 12
testprod = refl
testprod' : St.⟦ prod ⟧t (mk triv) 6 6 ≡ 36
testprod' = refl

Nat^2 : Ty
Nat^2 = Bool ⇒ Nat -- Nat × Nat ≅ Bool ⇒ Nat
fst : ∀{Γ} → Tm Γ Nat^2 → Tm Γ Nat
fst t = t $ true
snd : ∀{Γ} → Tm Γ Nat^2 → Tm Γ Nat
snd t = t $ false
⟨_,_⟩ : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat^2
⟨ u , v ⟩ = lam (iteBool (u [ p ]) (v [ p ]) q)
⟦Nat^2β₁⟧ : ∀{Γ}{u v : Tm Γ Nat} → St.⟦ fst ⟨ u , v ⟩ ⟧t ≡ St.⟦ u ⟧t
⟦Nat^2β₁⟧ = refl
⟦Nat^2β₂⟧ : ∀{Γ}{u v : Tm Γ Nat} → St.⟦ snd ⟨ u , v ⟩ ⟧t ≡ St.⟦ v ⟧t
⟦Nat^2β₂⟧ = refl
{-
fac : Tm ◇ (Nat ⇒ Nat)
fac = {!!}

testfac : St.⟦ fac ⟧t (mk triv) 5 ≡ 120
testfac = refl
-}
fib : Tm ◇ (Nat ⇒ Nat)
fib = lam (snd (iteNat ⟨ zeroo , suco zeroo ⟩ ⟨ snd q , plus [ ε ] $ fst q $ snd q ⟩ q))
-- fib 0 = 0
-- fib 1 = 1
-- fib (suc (suc n)) = fib n + fib (suc n)

-- (0,1) -> (1,1)
-- (1,1) -> (1,2)
-- (1,2) -> (2,3)
-- (2,3) -> (3,5)
-- (a,b) -> (b,a+b)

testfib : St.⟦ fib ⟧t (mk triv) 6 ≡ 13
testfib = refl
{-
concat : ∀{A} → Tm ◇ (I.List A ⇒ I.List A ⇒ I.List A)
concat = {!!}

testconcat : St.⟦ concat {Nat} $ cons zeroo nil $ cons (suco zeroo) (cons (suco (suco zeroo)) nil) ⟧t ≡ St.⟦ cons {◇} zeroo (cons (suco zeroo) (cons (suco (suco zeroo)) nil)) ⟧t
testconcat = {!!}

{-
Define the recursor for natural numbers with the help of the iterator. It does not need to (and will not) satisfy recNatβ2
-}
-}
