{-# OPTIONS --prop --rewriting #-}

module gy10 where

open import Lib

open import Def

open I

-- explain Var, Ne, Nf

-- check what eval, norm and ⌜norm⌝ gives for this term
t : Tm ◇ Bool
t = isZero (ite true (num 1) (num 0))

evalt : Lift ⊤ → 𝟚
evalt = St.⟦ t ⟧t

normt : Nf ◇ Bool
normt = norm t

⌜norm⌝t : Tm ◇ Bool
⌜norm⌝t = ⌜ norm t ⌝Nf

complt : ⌜ norm t ⌝Nf ≡ t
complt = compl t

-- check what eval, norm and ⌜norm⌝ gives for this term
t1 : Tm (◇ ▹ Nat) Bool
t1 = isZero (ite (isZero (num 2 +o num 3)) q (q +o num 2))

evalt1 : Lift ⊤ × ℕ → 𝟚
evalt1 = St.⟦ t1 ⟧t

normt1 : Nf (◇ ▹ Nat) Bool
normt1 = norm t1

⌜norm⌝t1 : Tm (◇ ▹ Nat) Bool
⌜norm⌝t1 = ⌜ norm t1 ⌝Nf

complt1 : ⌜ norm t1 ⌝Nf ≡ t1
complt1 = compl t1

-- how to use completeness
t4 = def (false {◇}) (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
t5 = ite true (num {◇} 3) (num 0)

-- prove that two terms are equal using compl
var-eq-5 : t4 ≡ t5
var-eq-5 =
  t4
      ≡⟨ {!!} ⟩
  ⌜ norm t4 ⌝Nf
      ≡⟨ {!!} ⟩
  t5
      ∎

-- terms in the empty context

novar◇ : ∀{A} → Var ◇ A → ⊥
novar◇ t = {!!}

-- analogy
data Tree : Set where
  node : Tree → Tree → Tree

notree : Tree → ⊥
notree t = {!!}

-- prove that there are no neutral terms in the empty context (pattern match on t)
noneu◇ : ∀{A} → Ne ◇ A → ⊥
noneu◇ t = {!!}

-- prove that there are only two elements of Bool normal forms in the empty context
case◇BoolNf : (P : Tm ◇ Bool → Set) → P true → P false → (t : Nf ◇ Bool) → P ⌜ t ⌝Nf
case◇BoolNf P pt pf t = {!!}

-- prove that there are only two elements of Bool terms in the empty context (use case◇BoolNf, transp, compl)
case◇Bool : (P : Tm ◇ Bool → Set) → P true → P false → (t : Tm ◇ Bool) → P t
case◇Bool P pt pf t = {!!}

-- use case◇Bool (and Lift)
ite-diag◇ : (t : Tm ◇ Bool)(A : Ty)(u : Tm ◇ A) → ite t u u ≡ u
ite-diag◇ t A u = {!!}

helper-ex4 : neu (isZeroNe (var (vz {◇}) +NeNf 2)) ≡ falseNf → ⊥
helper-ex4 ()

-- prove that these two terms are not equal in the syntax (hint: they have different normal forms)
ex4 : ¬ (isZero (q {◇} +o num 2) ≡ false)
ex4 = {!!}

-- if the normal forms are different, so are the terms
not= : ∀{Γ A}(t1 t2 : Tm Γ A) → ¬ (norm t1 ≡ norm t2) → ¬ (t1 ≡ t2)
not= t1 t2 ne e = {!!}

-- prove that ite-diag◇ is false in arbitrary contexts
ite-diag : ((Γ : Con)(t : Tm Γ Bool)(A : Ty)(u : Tm Γ A) → ite t u u ≡ u) → ⊥
ite-diag f = {!!}

t2 t3 : Tm (◇ ▹ Bool ▹ Nat) Nat
t2 = ite v1 v0 (num 1)
t3 = ite (ite true v1 false) (v0) (num 0 +o num 1)

-- decide if t2 and t3 are equal or not!

t2=t3 : t2 ≡ t3
t2=t3 = extract (t2 ≟Tm t3)

-- this is how we prove equalities from now on!
