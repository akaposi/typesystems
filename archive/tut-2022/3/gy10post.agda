{-# OPTIONS --prop --rewriting #-}

module gy10post where

open import Lib

open import Def

open I

-- explain Var, Ne, Nf

{-
Z, zero, suc, pred, sucpred, predsuc     zero, suc(suc(...(suc zero))), pred(pred(...(pred zero)))

NatBool valtozok nem voltak:
Nf Bool = 𝟚
Nf Nat  = ℕ

Def: NatBool + valtozok + definiciok

true , false,   (isZero v0) = ite true (isZero v0) false

Var Γ A
  q, q[p], q[p][p], ...

Ne Γ A
  var x, isZero tne, ite tne ane bne, tne + t'ne, tne + num n, num n + tne 

Nf Γ A
  neu t, true, false, num n
-}

-- check what eval, norm and ⌜norm⌝ gives for this term
t : Tm ◇ Bool
t = isZero (ite true (num 1) (num 0))

evalt : Lift ⊤ → 𝟚
evalt = St.⟦ t ⟧t -- = λ _ → ff

normt : Nf ◇ Bool
normt = norm t -- = falseNf

⌜norm⌝t : Tm ◇ Bool
⌜norm⌝t = ⌜ norm t ⌝Nf -- = false

complt : ⌜ norm t ⌝Nf ≡ t
complt = {!!}

t4 = def (false {◇}) (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
t5 = num {◇} 3

var-eq-5 : t4 ≡ t5
var-eq-5 =
  t4
      ≡⟨ compl t4 ⁻¹ ⟩
  num 3
      ≡⟨ compl t5 ⟩
  t5
      ∎

-- check what eval, norm and ⌜norm⌝ gives for this term
t1 : Tm (◇ ▹ Nat) Bool
t1 = isZero (ite (isZero (num 2 +o num 3)) q (q +o num 2))

evalt1 : Lift ⊤ × ℕ → 𝟚
evalt1 = St.⟦ t1 ⟧t

normt1 : Nf (◇ ▹ Nat) Bool
normt1 = norm t1

⌜norm⌝t1 : Tm (◇ ▹ Nat) Bool
⌜norm⌝t1 = ⌜ norm t1 ⌝Nf

complt1 : ⌜ norm t1 ⌝Nf ≡ t1
complt1 = compl t1

-- terms in the empty context

novar◇ : ∀{A} → Var ◇ A → ⊥
novar◇ ()

data Tree : Set where
  node : Tree → Tree → Tree

notree : Tree → ⊥
notree (node t₁ t₂) = notree t₁

-- prove that there are no neutral terms in the empty context (pattern match on t)
noneu◇ : ∀{A} → Ne ◇ A → ⊥
noneu◇ (iteNe t₁ x x₁) = noneu◇ t₁
noneu◇ (isZeroNe t₁) = noneu◇ t₁
noneu◇ (t₁ +NeNe t₂) = noneu◇ t₁
noneu◇ (t₁ +NeNf x) = noneu◇ t₁
noneu◇ (x +NfNe t₁) = noneu◇ t₁

-- prove that there are only two elements of Bool normal forms in the empty context
case◇BoolNf : (P : Tm ◇ Bool → Set) → P true → P false → (t : Nf ◇ Bool) → P ⌜ t ⌝Nf
case◇BoolNf P pt pf (neu x) = exfalso (noneu◇ x)
case◇BoolNf P pt pf trueNf = pt
case◇BoolNf P pt pf falseNf = pf

-- prove that there are only two elements of Bool terms in the empty context (use case◇BoolNf and compl)
case◇Bool : (P : Tm ◇ Bool → Set) → P true → P false → (t : Tm ◇ Bool) → P t
case◇Bool P pt pf t =  transp P (compl t) (case◇BoolNf P pt pf (norm t))

-- use case◇Bool
ite-diag◇ : (t : Tm ◇ Bool)(A : Ty)(u : Tm ◇ A) → ite t u u ≡ u
ite-diag◇ t A u = un (case◇Bool (λ t → Lift (ite t u u ≡ u)) (mk iteβ₁) (mk iteβ₂) t)

helper-ex4 : neu (isZeroNe (var (vz {◇}) +NeNf 2)) ≡ falseNf → ⊥
helper-ex4 ()

-- prove that these two terms are not equal in the syntax (hint: they have different normal forms)
ex4 : ¬ (isZero (q {◇} +o num 2) ≡ false)
ex4 e with (cong norm e)
... | ()

not= : ∀{Γ A}(t1 t2 : Tm Γ A) → ¬ (norm t1 ≡ norm t2) → ¬ (t1 ≡ t2)
not= t1 t2 ne e = ne (cong norm e)

-- prove that ite-diag◇ is false in arbitrary contexts -- ite q true true ≠ true
ite-diag : ((Γ : Con)(t : Tm Γ Bool)(A : Ty)(u : Tm Γ A) → ite t u u ≡ u) → ⊥
ite-diag f with cong norm (f (◇ ▹ Bool) q Bool true)
... | ()
{-
ite-diag f = neq (cong norm (f (◇ ▹ Bool) q Bool true)) 
  where
    neq : neu (iteNe (var (vz {◇})) trueNf trueNf) ≡ trueNf → ⊥
    neq ()
-}
t2 t3 : Tm (◇ ▹ Bool ▹ Nat) Nat
t2 = ite v1 v0 (num 1)
t3 = ite (ite true v1 false) (v0) (num 0 +o num 1)

-- decide if t2 and t3 are equal or not!

t2=t3 : Dec (Lift (t2 ≡ t3))
t2=t3 = t2 ≟Tm t3
