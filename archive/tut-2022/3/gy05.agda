{-# OPTIONS --prop --rewriting #-}

module gy05 where

open import Lib

-- ez a gyakorlat 5 perccel rovidebb

-- to Ambrus: Az 5 percedet ledolgoztam, újra 0-ban vagy.

-- megbeszelni HF-t, kiszh-t, ha van ra szukseg

module type-inference where

  import RazorAST
  open import RazorWT
  open RazorWT.I

  -- tipuskikovetkeztetes primitiv hibauzenetekkel

  data Result (A : Set) : Set where
    Ok : (r : A) → Result A
    Err : (e : ℕ) → Result A

  {-
    Error codes:
    · 1: Non boolean condition
    · 2: Differing branch types
    · 3: Non numeric isZero parameter
    · 4: Non numeric addition parameter
  -}

  Infer : RazorAST.Model {lzero}
  Infer = record
    { Tm      = Result (Σ Ty (λ A → Tm A)) -- WT-beli Ty, WT-beli Tm
    ; true    = Ok (Bool , true)
    ; false   = Ok (Bool , false)
    ; ite     = λ { (Ok (Nat , b)) tr fa → Err 1
                  ; (Ok (Bool , b)) (Ok (Nat , tr)) (Ok (Nat , fa)) → Ok (Nat , ite b tr fa)
                  ; (Ok (Bool , b)) (Ok (Bool , tr)) (Ok (Bool , fa)) → Ok (Bool , ite b tr fa)
                  ; (Ok (Bool , b)) (Ok _) (Ok _) → Err 2
                  ; (Ok (Bool , b)) (Ok r) (Err e) → Err e
                  ; (Ok (Bool , b)) (Err e) (Ok r) → Err e
                  ; (Ok (Bool , b)) (Err e) (Err e₁) → Err e
                  ; (Err e) tr fa → Err e  }
    ; num     = λ n → Ok (Nat , num n)
    ; isZero  = λ { (Ok (Nat , tm)) → Ok (Bool , isZero tm)
                  ; (Ok (Bool , tm)) → Err 3
                  ; (Err e) → Err e }
    ; _+o_    = λ { (Ok (Nat , tm)) (Ok (Nat , tm₁)) → Ok (Nat , tm +o tm₁)
                  ; (Ok _) (Ok _) → Err 4
                  ; (Ok r) (Err e) → Err e
                  ; (Err e) (Ok r) → Err e
                  ; (Err e) (Err e₁) → Err e}
    }

  module INF = RazorAST.Model Infer

  inf-test : Result (Σ Ty (λ T → Tm T))
  inf-test = INF.⟦ I'.isZero I'.false ⟧ -- itt lehet tesztelni
    where open RazorAST.I

  -- Standard model

  Standard : Model {lsuc lzero} {lzero}
  Standard = record
    { Ty      = Set
    ; Tm      = λ A → A
    ; Nat     = ℕ
    ; Bool    = 𝟚
    ; true    = tt
    ; false   = ff
    ; ite     = if_then_else_
    ; num     = λ n → n
    ; isZero  = λ { zero → tt
                  ; (suc n) → ff }
    ; _+o_    = _+_
    }
  module STD = Model Standard

  eval : {A : Ty} → Tm A → {!   !}
  eval = {!   !}

  typeOfINF : Result (Σ Ty (λ A → Tm A)) → Set
  typeOfINF r = {!   !}

  run : (t : RazorAST.I.Tm) → {!   !}
  run t = {!   !}
  
module Razor-with-equational-theory where

  open import Razor
  open I

  eq-0 : true ≡ true
  eq-0 = refl

  eq-1 : isZero (num 0) ≡ true
  eq-1 = I.isZeroβ₁

  eq-2 : isZero (num 3 +o num 1) ≡ isZero (num 4)
  eq-2 = cong (λ x → isZero x) I.+β

  eq-3 : isZero (num 4) ≡ false
  eq-3 = I.isZeroβ₂

  eq-4 : isZero (num 3 +o num 1) ≡ false
  eq-4 = eq-2 ◾ eq-3

  eq-4'' : isZero (num 3 +o num 1) ≡ false
  eq-4'' = cong (λ x → isZero x) I.+β ◾ I.isZeroβ₂

  eq-4' : isZero (num 3 +o num 1) ≡ false
  eq-4' =
    isZero (num 3 +o num 1)
      ≡⟨ cong isZero I.+β ⟩ -- \== \< \>
    isZero (num 4)
      ≡⟨ I.isZeroβ₂ ⟩
    false
      ∎ -- \qed

  eq-5 : ite false (num 2) (num 5) ≡ num 5
  eq-5 = I.iteβ₂

  eq-6 : ite true (isZero (num 0)) false ≡ true
  eq-6 = I.iteβ₁ ◾ I.isZeroβ₁

  eq-6' : ite true (isZero (num 0)) false ≡ true
  eq-6' =
    ite true (isZero (num 0)) false
      ≡⟨ cong (λ x → ite true x false) I.isZeroβ₁ ⟩
    ite true true false
      ≡⟨ I.iteβ₁ ⟩
    true
      ∎

  eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
  eq-7 =
    ((num 3 +o num 0) +o num 1)
      ≡⟨ cong (λ x → x +o num 1) I.+β ⟩
    (num 3 +o num 1)
      ≡⟨ I.+β ⟩
    num 4
      ∎

  eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0 +o num 1) ≡ num 2
  eq-8 =
    ite (isZero (num 0)) (num 1 +o num 1) (num 0 +o num 1)
      ≡⟨ cong (λ x → ite x (num 1 +o num 1) (num 0 +o num 1)) I.isZeroβ₁ ⟩
    ite true (num 1 +o num 1) (num 0 +o num 1)
      ≡⟨ I.iteβ₁ ⟩
    (num 1 +o num 1)
      ≡⟨ I.+β ⟩
    num 2
      ∎

  eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  eq-9 =
    (num 3 +o ite (isZero (num 2)) (num 1) (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 3
      ∎

  eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  eq-10 =
    (ite false (num 1 +o num 1) (num 0) +o num 0)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 0
      ∎

  eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  eq-11 =
    ite (isZero (num 0 +o num 1)) false (isZero (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    true
      ∎

  eq-12 : num 3 +o num 2 ≡ ite true (num 5) (num 1)
  eq-12 =
    num 3 +o num 2
      ≡⟨ I.+β ⟩
    num 5
      ≡⟨ I.iteβ₁ ⁻¹ ⟩
    ite true (num 5) (num 1)
      ∎  

module injectivity where

  -- bizonyitsd be, hogy RazorWT.I.isZero injektiv!
  module RazorWT where
    open import RazorWT
    open I

    D : DepModel
    D = record
      { Ty∙     = λ _ → Lift ⊤
      ; Bool∙   = _
      ; Nat∙    = _
      ; Tm∙     = λ _ _ → Tm Nat
      ; true∙   = {!!}
      ; false∙  = {!!}
      ; ite∙    = {!!}
      ; num∙    = {!!}
      ; isZero∙ = {!!}
      ; _+o∙_   = {!!}
      }
    module D = DepModel D

    isZeroInj : ∀{t t' : Tm Nat} → isZero t ≡ isZero t' → t ≡ t'
    isZeroInj e = cong D.⟦_⟧t e

  module Razor where
    open import Razor hiding (St)

    -- standard model
    St : Model
    St = record
      { Ty       = Set
      ; Tm       = λ X → X
      ; Nat      = ℕ
      ; Bool     = 𝟚
      ; true     = tt
      ; false    = ff
      ; ite      = λ b a a' → if b then a else a'
      ; num      = {!!}
      ; isZero   = {!!}
      ; _+o_     = {!!}
      ; iteβ₁    = {!!}
      ; iteβ₂    = {!!}
      ; isZeroβ₁ = {!!}
      ; isZeroβ₂ = {!!}
      ; +β       = {!!}
      }
    module St = Model St

    module equations where
      open St

      -- a standard modellben az eq-0...eq-12 egyenlosegek definicio szerint teljesulnek (a metaelmeletbol kovetkeznek)
      -- pl.
      eq-4 : isZero (num 3 +o num 1) ≡ false
      eq-4 = {!!}

      eq-6 : ite true (isZero (num 0)) false ≡ true
      eq-6 = {!!}

      eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
      eq-7 = {!!}

      eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
      eq-8 = {!!}
      
      eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
      eq-9 = {!!}

      -- stb.
       
    open I

    -- bizonyitsd be, hogy Razor.I.num injektiv!
    numInj : ∀{m n} → num m ≡ num n → m ≡ n
    numInj e = {!!} -- hasznald az St-be valo kiertekelest!

    -- bizonyitsd be, hogy Razor.I.isZero nem injektiv!
    notInj : ¬ ((t t' : Tm Nat) → isZero t ≡ isZero t' → t ≡ t')
    notInj e = {!!} -- hasznald a num injektivitasat ill. az isZeroβ₂-t!
