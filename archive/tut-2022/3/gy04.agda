{-# OPTIONS --prop --rewriting #-}

module gy04 where

open import Lib
open import RazorAST hiding (L1; L2)

{-
dependens modellek, tipuskikovetkeztetes (utobbi hazi feladat is volt)
-}

L1 : Model
L1 = record
  { Tm     = ℕ
  ; true   = 1
  ; false  = 1
  ; ite    = λ t t' t'' → t + t' + t''
  ; num    = λ _ → 1
  ; isZero = λ t → t
  ; _+o_   = _+_
  }
module L1 = Model L1
L2 : Model
L2 = record
  { Tm     = ℕ
  ; true   = 0
  ; false  = 0
  ; ite    = λ t t' t'' → 2 + t + t' + t''
  ; num    = λ _ → 0
  ; isZero = λ t → t
  ; _+o_   = λ t t' → 1 + t + t'
  }
module L2 = Model L2

-- FEL (lasd eloadas)
L1L2 : DepModel {lzero}
L1L2 = record
         { Tm∙ = λ t → Lift (L1.⟦ t ⟧ ≡ 1 + L2.⟦ t ⟧)
         ; true∙ = mk refl
         ; false∙ = {!!}
         ; ite∙ = {!!}
         ; num∙ = {!!}
         ; isZero∙ = {!!}
         ; _+o∙_ = λ {u} (mk u∙){v} (mk v∙) → mk (
             L1.⟦ u I.+o v ⟧
                                               ≡⟨ refl ⟩   -- \==  \<   \>
             L1.⟦ u ⟧ + L1.⟦ v ⟧
                                               ≡⟨ cong (_+ L1.⟦ v ⟧) u∙  ⟩ -- cong f : a ≡ a' → f a ≡ f a'
             1 + L2.⟦ u ⟧ + L1.⟦ v ⟧
                                               ≡⟨ {!v∙!} ⟩
             ((1 + L2.⟦ u ⟧) + 1) + L2.⟦ v ⟧
                                               ≡⟨ cong (_+ L2.⟦ v ⟧) (ass+ {1}{_}{1} ⁻¹) ⟩   -- \^-  \^1
             (1 + (L2.⟦ u ⟧ + 1)) + L2.⟦ v ⟧
                                               ≡⟨ cong (λ z → 1 + z + L2.⟦ v ⟧) (+comm {L2.⟦ u ⟧}{1}) ⟩
             (1 + (1 + L2.⟦ u ⟧)) + L2.⟦ v ⟧
                                               ≡⟨ refl ⟩
             2 + (L2.⟦ u ⟧ + L2.⟦ v ⟧)
                                               ≡⟨ refl ⟩   -- \==  \<   \>
             1 + (L2.⟦ u I.+o v ⟧)
                                               ∎           -- \qed
           )
         }
module L1L2 = DepModel L1L2

-- HF: ezt befejezni
-- lasd meg az eload

-- FEL
twolengths : ∀ t → L1.⟦ t ⟧ ≡ 1 + L2.⟦ t ⟧
twolengths t = {!!}

-- https://ngnghm.github.io

f f' : (x y : ℕ) → (2 + x) + y ≡ 2 + (x + y)
f = λ x y → refl
f' = λ x y → ass+ {2}{x}{y} ⁻¹
{-
ass+ {2} =
cong suc (ass+ {1}) =
cong suc (cong suc (ass+ {0})) =
cong suc (cong suc refl) =
cong suc refl =
refl

cong : ∀{ℓ}{A : Set ℓ}{ℓ'}{B : Set ℓ'}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl

ass+ : ∀{m n o} → (m + n) + o ≡ m + (n + o)
ass+ {zero} = refl
ass+ {suc m} = cong suc (ass+ {m})
-}

f=f' : _≡_ {A = Lift ((x y : ℕ) → (2 + x) + y ≡ 2 + (x + y))} (mk f) (mk f')
f=f' = refl
