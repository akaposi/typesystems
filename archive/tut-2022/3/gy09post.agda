{-# OPTIONS --prop --rewriting #-}

module gy09post where

-- ez a gyakorlat 12 perccel rovidebb, 20.30-kor kiszh-t irunk

open import Lib

open import Def

open I

var-test-1 : _≡_ {A = Tm ◇ Bool} (def true v0) true
var-test-1 =
  def true v0
    ≡⟨ ▹β₂ ⟩
  true
    ∎

var-test-2 : _≡_ {A = Tm ◇ Bool} (def true (def false v0)) false
var-test-2 =
  def true (def false v0) -- let x:=true in let y:= false in y
    ≡⟨ refl ⟩
  (q [ id ,o false ]) [ id ,o true ]
    ≡⟨ cong (λ z → z [ id ,o true ]) ▹β₂ ⟩
  false [ id ,o true ]
    ≡⟨ false[] ⟩
  false
    ∎

var-test-3 : _≡_ {A = Tm ◇ Bool} (def true (def false v1)) true
var-test-3 =
  def true (def false v1)  -- let x:=true in let y:= false in x
    ≡⟨ refl ⟩
  (q [ p ] [ id ,o false ]) [ id ,o true ]
    ≡⟨ cong (_[ id ,o true ]) ([∘] ⁻¹) ⟩
  (q [ p ⊚ (id ,o false) ]) [ id ,o true ]
    ≡⟨ cong (λ z → q [ z ] [ id ,o true ]) ▹β₁ ⟩
  (q [ id ]) [ id ,o true ]
    ≡⟨ cong (_[ id ,o true ]) [id] ⟩
  q [ id ,o true ]
    ≡⟨ ▹β₂ ⟩
  true
    ∎

task-sub : Sub ◇ (◇ ▹ Bool ▹ Nat)
task-sub = ε ,o false ,o num 4

task-eq : (ite v1 (v0 +o v0) v0) [ task-sub ] ≡ num 4
task-eq = ite[] ◾ (cong (λ z → ite z (q +o q [ ε ,o false ,o num 4 ]) (q [ ε ,o false ,o num 4 ])) ([∘] ⁻¹ ◾ (cong (q [_]) ▹β₁ ◾ ▹β₂)) ◾ iteβ₂ ◾ ▹β₂)

var-eq-4 : def (num 1) (v0 +o v0) ≡ num {◇} 2
var-eq-4 =
  def (num 1) (v0 +o v0)
    ≡⟨ +[] ⟩
  (q [ id ,o num 1 ]) +o (q [ id ,o num 1 ])
    ≡⟨ cong (λ z → z +o z) ▹β₂ ⟩
  num 1 +o num 1
    ≡⟨ +β ⟩
  num 2
    ∎
