{-# OPTIONS --prop --rewriting #-}

module gy06post where

open import Lib

-- ez a gyakorlat 5 perccel rovidebb

-- to Ambrus: Az 5 percedet ledolgoztam, újra 0-ban vagy.

-- megbeszelni HF-t, kiszh-t, ha van ra szukseg

module NatBool-with-equational-theory where

  open import NatBool
  open I

  eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  eq-9 =
    (num 3 +o ite (isZero (num 2)) (num 1) (num 0))
      ≡⟨ cong (λ z → num 3 +o ite z (num 1) (num 0)) I.isZeroβ₂ ⟩
    ( num 3 +o ite false (num 1) (num 0))
      ≡⟨ cong (num 3 +o_) I.iteβ₂ ⟩
    ( num 3 +o num 0)
      ≡⟨ I.+β ⟩
    num 3
      ∎

  eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  eq-10 =
    (ite false (num 1 +o num 1) (num 0) +o num 0)
      ≡⟨ cong (_+o num 0) I.iteβ₂ ⟩
    (num 0 +o num 0)
      ≡⟨ I.+β ⟩
    num 0
      ∎

  eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  eq-11 =
    ite (isZero (num 0 +o num 1)) false (isZero (num 0))
      ≡⟨ cong (λ z → ite (isZero z) false (isZero (num 0))) I.+β ⟩
    ite (isZero (num 1)) false (isZero (num 0))
      ≡⟨ cong (λ z → ite z false (isZero (num 0))) I.isZeroβ₂ ⟩
    ite false false (isZero (num 0))
      ≡⟨ I.iteβ₂ ⟩
    isZero (num 0)
      ≡⟨ I.isZeroβ₁ ⟩
    true
      ∎

module injectivity where

  -- bizonyitsd be, hogy NatBoolWT.I.isZero injektiv!
  module NatBoolWT where
    open import NatBoolWT
    open I

    -- cel:
    -- D.⟦_⟧t : Tm Nat → Tm Nat
    -- D.⟦ isZero ⟧t = t
    -- D.⟦ _ ⟧t = num 0

    D : DepModel
    D = record
      { Ty∙     = λ _ → Lift ⊤
      ; Bool∙   = _
      ; Nat∙    = _
      ; Tm∙     = λ _ _ → Tm Nat
      ; true∙   = num 0
      ; false∙  = num 0
      ; ite∙    = λ _ _ _ → num 0
      ; num∙    = λ _ → num 0
      ; isZero∙ = λ {t} _ → t
      ; _+o∙_   = λ _ _ → num 0
      }
    module D = DepModel D

    isZeroInj : ∀{t t' : Tm Nat} → isZero t ≡ isZero t' → t ≡ t'
    isZeroInj e = cong D.⟦_⟧t e

  module NatBool where
    open import NatBool hiding (St)

    -- standard model
    St : Model
    St = record
      { Ty       = Set
      ; Tm       = λ X → X
      ; Nat      = ℕ
      ; Bool     = 𝟚
      ; true     = tt
      ; false    = ff
      ; ite      = λ b a a' → if b then a else a'
      ; num      = λ n → n
      ; isZero   = λ { zero → tt ; (suc _) → ff }
      ; _+o_     = _+_
      ; iteβ₁    = refl
      ; iteβ₂    = refl
      ; isZeroβ₁ = refl
      ; isZeroβ₂ = refl
      ; +β       = refl
      }
    module St = Model St

    module equations where
      open St

      -- a standard modellben az eq-0...eq-12 egyenlosegek definicio szerint teljesulnek (a metaelmeletbol kovetkeznek)
      -- pl.
      eq-4 : isZero (num 3 +o num 1) ≡ false
      eq-4 = refl

      eq-6 : ite true (isZero (num 0)) false ≡ true
      eq-6 = refl

      eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
      eq-7 = refl

      eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
      eq-8 = refl
      
      eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
      eq-9 = refl

    open I

    -- bizonyitsd be, hogy NatBool.I.num injektiv!
    numInj : ∀{m n} → num m ≡ num n → m ≡ n
    numInj e = cong St.⟦_⟧t e -- hasznald az St-be valo kiertekelest!

    1≠2 : 1 ≡ 2 → ⊥
    1≠2 ()
  
    -- bizonyitsd be, hogy NatBool.I.isZero nem injektiv!
    notInj : ¬ ((t t' : Tm Nat) → isZero t ≡ isZero t' → t ≡ t')
    notInj e = 1≠2 (numInj (e (num 1) (num 2) (I.isZeroβ₂ ◾ I.isZeroβ₂ ⁻¹))) -- hasznald a num injektivitasat ill. az isZeroβ₂-t!

module integers where

  -- ennek a resznek a megoldasaiert last Int.lagda a jegyzetben!

  open import Int hiding (N; SucNf; PredNf; ⌜_⌝; norm; testnorm; stab; ⌜Suc⌝; ⌜Pred⌝; Comp; comp)
  open I

  one one' : Z
  one  = Suc I.Zero
  one' = Suc (Pred (Suc I.Zero))

  one= : one ≡ one'
  one= = cong Suc (PredSuc I.Zero ⁻¹)
  
  -2' -2'' : Z
  -2'  = Pred (Pred I.Zero)
  -2'' = Pred (Suc (Pred (Suc (Pred (Suc (Pred (Pred I.Zero)))))))

  -2= : -2' ≡ -2''
  -2= = cong Pred (SucPred _ ⁻¹ ◾ SucPred _ ⁻¹) ◾ PredSuc _ ⁻¹

  -- nezd meg, mi az, hogy Model:
  Model' = Model

  -- nezd meg, mik a normal formak:
  Nf' = Nf

  ⌜_⌝ : Nf → I.Z
  ⌜ -Suc zero     ⌝ = I.Pred I.Zero
  ⌜ -Suc (suc n)  ⌝ = I.Pred ⌜ -Suc n ⌝
  ⌜ Zero          ⌝ = I.Zero
  ⌜ +Suc zero     ⌝ = I.Suc I.Zero
  ⌜ +Suc (suc n)  ⌝ = I.Suc ⌜ +Suc n ⌝

  SucNf : Nf → Nf
  SucNf = {!!}

  PredNf : Nf → Nf
  PredNf = {!!}

  -- egy normal formakbol allo modell
  N : Model
  N = record
    { Z       = Nf
    ; Zero    = Nf.Zero
    ; Suc     = SucNf
    ; Pred    = PredNf
    ; SucPred = λ { (-Suc n) → {!!} ; Zero → {!!} ; (+Suc n) → {!!} }
    ; PredSuc = λ { (-Suc n) → {!!} ; Zero → {!!} ; (+Suc n) → {!!} }
    }
  module N = Model N

  norm : I.Z → Nf
  norm = N.⟦_⟧

  testnorm1 : norm one ≡ norm one'
  testnorm1 = refl
  testnorm2 : norm -2' ≡ norm -2''
  testnorm2 = refl
  testnorm3 :  ⌜ norm (I.Pred (I.Pred (I.Suc (I.Pred (I.Pred (I.Pred (I.Suc I.Zero))))))) ⌝ ≡
              I.Pred (I.Pred (I.Pred I.Zero))
  testnorm3 = refl

  stab : (v : Nf) → norm ⌜ v ⌝ ≡ v
  stab = {!!}

  ⌜Suc⌝ : (v : Nf) → ⌜ SucNf v ⌝ ≡ I.Suc ⌜ v ⌝
  ⌜Suc⌝ = {!!}

  ⌜Pred⌝ : (v : Nf) → ⌜ PredNf v ⌝ ≡ I.Pred ⌜ v ⌝
  ⌜Pred⌝ = {!!}

  Comp : DepModel
  Comp = record
    { Z∙       = λ i → Lift (⌜ norm i ⌝ ≡ i)
    ; Zero∙    = {!!}
    ; Suc∙     = {!!}
    ; Pred∙    = {!!}
    ; SucPred∙ = {!!}
    ; PredSuc∙ = {!!}
    }
  module Comp = DepModel Comp

  comp : (i : I.Z) → ⌜ norm i ⌝ ≡ i
  comp i = un (Comp.⟦ i ⟧)
