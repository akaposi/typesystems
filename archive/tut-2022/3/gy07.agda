{-# OPTIONS --prop --rewriting #-}

module gy07 where

open import Lib

-- jegyzetben megcsinalni kozosen 2.1, 2.2, 2.3 feladatokat

module DefABT where
  open import DefABT

  open I

  {- Rewrite the following expressions with De Bruijn notation -}

  -- let x:=num 1 in num 2 + x = num 2 +o num 1 = num (2 + 1) = num 3
  -- let x:=num 1 +o ite (isZero (num 2)) (num 3) (num 4) in x + x = ...
  -- (let x:=num 1 in x + x) == (let y:=num 1 in y + y)
  -- (let x:=num 1 in x + x) == (let y:=num 1 in y + y)
  -- De Bruijn index (Automath)
  -- (let (num 1) in v0 + v0)
  -- let x:=num 1 in let y:=num 2 in x + y
  -- let (num 1) in let (num 2) in v1 + v0
{-
      let
    /    \
 num 1    let      =    
         /   \
      num 2   +o             +o   
             /  \           /  \  
            v1   v0     num 1   num 2

for (i := 1 to 10) {         for (k := 1 to 10) { 
   x := x + i           =       x := x + k
}                            }                    

(λ x . x)  = (λ y.y + (y + 2))

         2
f = λ m. ∫  1/x² + m  dx
         0

kotes, kotes hataskore, szabad, kotott valtozok
-}

  -- let => def

  -- (let x:=num 1 in x + x)
  -- (let (num 1) in v0 + v0)
  tm-0 : Tm 0
  tm-0 = def (num 1) (v0 +o v0)

  -- let x:=1 in      x  + let y:=x+1       in y + let z:=x+y     in (x+z)+(y+x)
  tm-1 : Tm 0
  tm-1 = def (num 1) (v0 +o def (v0 +o num 1) (v0 +o def (v1 +o v0) ((v2 +o v0) +o (v1 +o v2))))
  {-
    def
num 1     +o
        v0    def
            +o     +o
           v0 ... v0
  -}

  -- (let x:=1 in x) + let y:=1 in y + let z:=x+y in (x+z)+(y+x)
  tm-2 : Tm 1
  tm-2 = def (num 1) v0 +o def (num 1) (v0 +o def (v1 +o v0) ((v2 +o v0) +o (v1 +o v2)))

  -- (let x:=1 in x + let y:=x+1 in y) + let z:=1 in z+z
  tm-3 : Tm 0
  tm-3 = def (num 1) (v0 +o def (v0 +o num 1) v0) +o def (num 1) (v0 +o v0)

  -- (let x:=1 in x + let y:=x+1 in x) + let z:=1 in z+z
  tm-3' : Tm 0
  tm-3' = def (num 1) (v0 +o def (v0 +o num 1) v1) +o def (num 1) (v0 +o v0)

  -- ((let x:=1 in x) + (let y:=1 in y)) + let z:=1 in z+z
  tm-4 : Tm 0
  tm-4 = (def (num 1) v0 +o def (num 1) v0) +o def (num 1) (v0 +o v0)

  -- let x:=(isZero true) in (ite x 0 x)
  tm-5 : Tm 0
  tm-5 = def (isZero true) (ite v0 (num 0) v0)


  {- Rewrite the following expressions with variable names -}

  -- let x:=      1+2 in        (x+x)   + let    y:=3+4      in   y  + y
  t-1 : Tm 0
  t-1 = def (num 1 +o num 2) (v0 +o v0) +o def (num 3 +o num 4) (v0 +o v0)

  --     let x:=1+2         in (x+x)      +  let y:=3+4        in  x   + y
  t-1' : Tm 0
  t-1' = def (num 1 +o num 2) ((v0 +o v0) +o def (num 3 +o num 4) (v1 +o v0))

  -- let x:=1 in let x:=2 in x
  -- def (num 1) in (def (num 2) in v0)

  --    let x:=true in x   + let y:=x in y+x
  t-2 : Tm 0
  t-2 = def true      (v0 +o def v0    (v0 +o v1))

  --   let x:=true in let y:=false in ite y y x
  t-3 : Tm 0
  t-3 = def true (def false (ite v0 v0 v1))

  --   true + let x:= true in false + let y:=x in x+y
  t-4 : Tm 0
  t-4 = true +o def true (false +o def v0 (v1 +o v0))

   --  let x:=true in let y:=false in let z:=true in let w:=false in (w +o z) +o (y +o x)
  t-5 : Tm 0
  t-5 = def true (def false (def true (def false ((v0 +o v1) +o (v2 +o v3)))))

  -- exercise 2.6

  zeros : ∀{n} → Vec ℕ n
  zeros {zero} = []
  zeros {suc n} = 0 :: zeros {n}

  zip+ : ∀{n} → Vec ℕ n → Vec ℕ n → Vec ℕ n
  zip+ [] [] = []
  zip+ (m :: ms) (n :: ns) = (m + n) :: zip+ ms ns

  countVarsv : ∀{n} → Var n → Vec ℕ n
  countVarsv vz = 1 :: zeros
  countVarsv (vs x) = 0 :: countVarsv x

  tail : ∀{n} → Vec ℕ (suc n) → Vec ℕ n
  tail (m :: ms) = ms

  countVars' : ∀{n} → Tm n → Vec ℕ n
  countVars' (var x) = countVarsv x
  countVars' (def t₁ t₂) = zip+ (tail (countVars' t₂)) (countVars' t₁)
  countVars' true = zeros
  countVars' false = zeros
  countVars' (ite t₁ t₂ t₃) = {!!}
  countVars' (num n) = zeros
  countVars' (isZero t) = countVars' t
  countVars' (t₁ +o t₂) = zip+ (countVars' t₁) (countVars' t₂)

  ttt : Tm 3
--  ttt = v0
  ttt = (v0 +o v0) +o def v0 (v1 +o v2)

  a = {!countVars' ttt!}

module DefWT where

  open import DefWT
  open I
  -- ◇ \di2     ▹  \t6
  tm-0 : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Bool
  tm-0 = isZero (ite v2 (v1 +o v0) v1)

  tm-1 : ∀{Γ}{A : Ty} → Tm (Γ ▹ Nat ▹ A) Nat
  tm-1 = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  tm-2 : Tm (◇ ▹ Nat ▹ Bool ▹ Nat) Bool
  tm-2 = ite v1 (isZero v0) (isZero v2)

  tm-3 : {A : Ty} → Tm (◇ ▹ Bool ▹ A ▹ Nat) A
  tm-3 = ite (ite v2 (isZero v0) v2) v1 v1

