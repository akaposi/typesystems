{-# OPTIONS --prop --rewriting #-}

module gy09 where

-- ez a gyakorlat 12 perccel rovidebb

open import Lib

open import Def

open I

var-test-1 : _≡_ {A = Tm ◇ Bool} (def true v0) true
var-test-1 =
  def true v0
    ≡⟨ {!!} ⟩
  true
    ∎

var-test-2 : _≡_ {A = Tm ◇ Bool} (def true (def false v0)) false
var-test-2 =
  def true (def false v0)
    ≡⟨ {!!} ⟩
  false
    ∎

var-test-3 : _≡_ {A = Tm ◇ Bool} (def true (def false v1)) true
var-test-3 =
  def true (def false v1)
    ≡⟨ {!!} ⟩
  true
    ∎

task-sub : Sub ◇ (◇ ▹ Bool ▹ Nat)
task-sub = {!!}

task-eq : (ite v1 (v0 +o v0) v0) [ task-sub ] ≡ num 4
task-eq = {!!}

var-eq-4 : def (num 1) (v0 +o v0) ≡ num {◇} 2
var-eq-4 =
  def (num 1) (v0 +o v0)
    ≡⟨ {!!} ⟩
  num 2
    ∎

var-eq-5 : def false (def (num 2) (ite v1 (num 0) (num 1 +o v0))) ≡ num {◇} 3
var-eq-5 =
  def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
    ≡⟨ {!!} ⟩
  num 3
    ∎

var-eq-6 : def (num 0) (def (isZero v0) (ite v0 false true)) ≡ false {◇}
var-eq-6 =
  def (num 0) (def (isZero v0) (ite v0 false true))
    ≡⟨ {!!} ⟩
  false
    ∎

var-eq-7 : def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0))) ≡ num {◇} 2
var-eq-7 =
  def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
    ≡⟨ {!!} ⟩
  num 2
    ∎

var-eq-8 : def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0))) ≡ false {◇}
var-eq-8 =
  def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0)))
    ≡⟨ {!!} ⟩
  false
    ∎

var-eq-9 : def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0))) ≡ num {◇} 2
var-eq-9 =
  def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0)))
    ≡⟨ {!!} ⟩
  num 2
    ∎

swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
swap = {!!}

swapswap : {Γ : Con} → {A B : Ty} → swap ⊚ swap ≡ id {Γ ▹ A ▹ B}
swapswap = {!!}

sub-eq-1 : {Γ : Con} → {σ δ : Sub Γ ◇} → σ ≡ δ
sub-eq-1 = {!!}

sub-eq-2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → ε ⊚ σ ≡ ε
sub-eq-2 = {!!}

sub-eq-3 :
  {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
  (σ ,o t) ⊚ δ ≡ (σ ⊚ δ ,o t [ δ ])
sub-eq-3 {Γ} {Δ} {Θ} {σ} {δ} {A} {t} =
  (σ ,o t) ⊚ δ
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  (σ ⊚ δ ,o t [ δ ])
    ∎

sub-eq-4 :
  {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
  (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ⊚ p ,o q ])
sub-eq-4 {σ = σ} {t = t} {u = u} =
  (def t u) [ σ ]
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  def (t [ σ ]) (u [ σ ⊚ p ,o q ])
    ∎

-- kovetkezo gyakorlat 12 perccel rovidebb
