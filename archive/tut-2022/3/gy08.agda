{-# OPTIONS --prop --rewriting #-}

module gy08 where

open import Lib

open import Def

open I

-- def (num 2) v0 = num 2
-- def (num 2) (v0 +o v0) = num 2 +o num 2
-- def (num 2) (v0 +o v0) = (v0 +o v0)[v0 ↦ num 2]
-- def (num 2) (def (num 1) (v0 +o v1)) = (def (num 1) (v0 +o v1))[v0 ↦ num 2] =
--   (v0 +o v1)[v0 ↦ num 1][v0 ↦ num 2]  = ... = (v0 +o v1)[v0 ↦ num 1, v1 ↦ num 2] = num 1 + num 2

-- def (num 2) (def (num 1) (v0 +o v1)) = (def (num 1) (v0 +o v1))[ε ,o num 2] = ...
-- \__________________________________/   \______________________/\__________/
--    : Tm ◇ Nat                         : Tm (◇ ▹ Nat) Nat        : Sub ◇ (◇ ▹ Nat)

-- def t u = u[id ,o t]

-- substitution calculus, simply typed CATEGORY WITH FAMILIES

-- Ty, Con
-- Sub : Con → Con → Set      Sub Γ (◇ ▹ A₀ ▹ A₁ ▹ A₂) = 3 db term, egy A₀ tipusu, egy A₁ tipusu es egy A₂ tipusu
-- (ε ,o a₀ ,o a₁ ,o a₂) : Sub Γ (◇ ▹ A₀ ▹ A₁ ▹ A₂), ha  a₀ : Tm Γ A₀, a₁ : Tm Γ A₁, a₂ : Tm Γ A₂
--        termek sorozata ((heterogen) listaja)
-- id : Sub Γ Γ
-- ε  : Sub Γ ◇
-- _,o_ : Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
-- _[_] : Tm Γ A → Sub Δ Γ → Tm Δ A
-- Tm : Con → Ty → Set        Tm Γ A -- A tipusu termek, melyekben Γ-nak megfelelo valtozok lehetnek
--   ite v0 (num 1 +o v1) v1 : Tm (◇ ▹ Nat ▹ Bool) Nat

-- σ : Sub (◇ ▹ Nat ▹ Bool) (◇ ▹ Bool ▹ Nat ▹ Nat)
-- σ = (ε ,o isZero (num 0 +o v1) ,o v1 ,o ite v0 v1 (num 2))
-- t : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Nat
-- t = v0 +o (v1 +o ite v2 (num 0) (num 1))
-- t [ σ ] : Tm (◇ ▹ Nat ▹ Bool) Nat
-- (v0 +o (v1 +o ite v2 (num 0) (num 1))) [ (ε ,o isZero (num 0 +o v1) ,o v1 ,o ite v0 v1 (num 2)) ] =
--   (ite v0 v1 (num 2))) +o (v1 +o ite (isZero (num 0 +o v1)) (num 0) (num 1))

module ex1 where
  Γ : Con
  Γ = ◇ ▹ Bool

  t : Tm Γ Nat
  t = ite v0 (num 1) (num 0)

  γ : Sub ◇ Γ
  γ = ε ,o true

  t[γ] : t [ γ ] ≡ num 1
  t[γ] =
    (t [ γ ])
      ≡⟨ refl ⟩
    ite q (num 1) (num 0) [ ε ,o true ]
      ≡⟨ ite[] ⟩
    ite (q [ ε ,o true ])
        (num 1 [ ε ,o true ])
        (num 0 [ ε ,o true ])
      ≡⟨ cong
        (λ z → ite z (num 1 [ ε ,o true ]) (num 0 [ ε ,o true ]))
        ▹β₂ ⟩
    ite true
        (num 1 [ ε ,o true ])
        (num 0 [ ε ,o true ])
      ≡⟨ iteβ₁ ⟩
   (num 1 [ ε ,o true ])
      ≡⟨ num[] ⟩
    num 1
      ∎

  Δ : Con
  Δ = ◇ ▹ Nat ▹ Nat

  δ : Sub Γ Δ
  δ = ε ,o ite v0 (num 1) (num 2) ,o num 2

  u : Tm Δ Nat
  u = v0 +o v1

  u[δ] : u [ δ ] ≡
    num 2 +o ite v0 (num 1) (num 2)
  u[δ] =
    (u [ δ ])
      ≡⟨ refl ⟩
    (v0 +o v1 [ δ ])
      ≡⟨ +[] ⟩
    (v0 [ δ ]) +o (v1 [ δ ])
      ≡⟨ cong (_+o (v1 [ δ ])) ▹β₂ ⟩
    num 2 +o (v1 [ δ ])
      ≡⟨ refl ⟩
    num 2 +o (q [ p ] [ δ ])
      ≡⟨ cong (num 2 +o_) ([∘] ⁻¹) ⟩
    num 2 +o (q [ p ⊚ δ ])
      ≡⟨ cong (λ z → num 2 +o (q [ z ])) ▹β₁ ⟩
    num 2 +o (q [ ε ,o ite v0 (num 1) (num 2) ])
      ≡⟨ cong (num 2 +o_) ▹β₂ ⟩
    num 2 +o ite v0 (num 1) (num 2)
      ∎
