{-# OPTIONS --prop --rewriting #-}

module gy11 where

open import Lib

open import STT

open I

add2 : Tm ◇ (Nat ⇒ Nat)
add2 = lam (v0 +o num 2) -- λx.x+2
-- ◾ \sq5
and : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
and = lam (lam (ite v1 v0 false)) -- λx y.if x then y else false
and-test1 : and $ true $ true ≡ true
and-test1 =
  cong (_$ true) (⇒β ◾  lam[]) ◾
  ⇒β ◾
  [∘] ⁻¹
  ◾ cong (ite v1 v0 false [_]) {((id ,o true) ⊚ p ,o q) ⊚ (id ,o true)}
    (,∘ ◾ cong₂ _,o_ (ass ◾ (cong ((id ,o true) ⊚_) ▹β₁ ◾ idr)) ▹β₂) ◾
  ite[] ◾
  (cong₂ (λ x y → ite x y (false [ id ,o true ,o true ]))
         ([∘] ⁻¹ ◾ (cong (q [_]) ▹β₁ ◾ ▹β₂))
    ▹β₂ ◾
  iteβ₁)
and-test2 : and $ false $ true ≡ false
and-test2 = {!!}

and' : Tm ◇ Bool → Tm ◇ Bool → Tm ◇ Bool
and' = λ u → λ v → ite u v false
and'-test1 : and' true true ≡ true
and'-test1 = {!!}
and'-test2 : and' false true ≡ false
and'-test2 = {!!}

neg : Tm ◇ (Bool ⇒ Bool)
neg = lam (ite v0 false true)
neg-test1 : neg $ true ≡ false
neg-test1 = {!!}
neg-test2 : neg $ false ≡ true
neg-test2 = {!!}

infixl 6  _∘o_
_∘o_ : ∀{Γ A B C} → Tm Γ (B ⇒ C) → Tm Γ (A ⇒ B) → Tm Γ (A ⇒ C)
t ∘o t' = lam (t [ p ] $ (t' [ p ] $ v0)) -- (t ∘ t') := λx.t(t'(x))

ido : ∀{Γ A} → Tm Γ (A ⇒ A)
ido = lam v0

apply3 : Tm ◇ ((Bool ⇒ Bool) ⇒ Bool ⇒ Bool)
apply3 = lam (lam ((v1 ∘o v1 ∘o v1) $ v0))
apply3-test : apply3 $ neg $ true ≡ false
apply3-test = {!!}
apply3-test' : (t : Tm ◇ (Bool ⇒ Bool))(u : Tm ◇ Bool) → apply3 $ t $ u ≡ t $ (t $ (t $ u))
apply3-test' = {!!}
apply3-extra : (t : Tm ◇ (Bool ⇒ Bool))(u : Tm ◇ Bool) → apply3 $ t $ u ≡ t $ u
apply3-extra = {!!}

isZero' : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
isZero' = lam (isZero v0) -- λx.isZero x

isZero'β₁ : ∀{Γ} → isZero' {Γ} $ num 0 ≡ true
isZero'β₁ =
  isZero' $ num 0
    ≡⟨ refl ⟩
  lam (isZero v0) $ num 0
    ≡⟨ ⇒β ⟩
  isZero v0 [ id ,o num 0 ]
    ≡⟨ isZero[] ⟩
  isZero (v0 [ id ,o num 0 ])
    ≡⟨ cong isZero ▹β₂ ⟩
  isZero (num 0)
    ≡⟨ isZeroβ₁ ⟩
  true
    ∎

isZero'β₂ : ∀{Γ} → isZero' {Γ} $ num 1 ≡ false
isZero'β₂ = {!!}

Four = Bool ⇒ Bool

zer one two three : Tm ◇ Four
zer = {!!}
one = {!!}
two = {!!}
three = {!!}

zer' one' two' three' : ∀{Γ} → Tm Γ Four
zer'   = lam false
one'   = lam (ite v0 false true)
two'   = lam (ite v0 true false)
three' = lam true
zer'[] : ∀{Γ Δ}{γ : Sub Δ Γ} → zer' [ γ ] ≡ zer'
zer'[] = lam[] ◾ cong lam false[]
-- also for one', two', three'

case' : ∀{Γ A} → Tm Γ Four → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A
case' t a0 a1 a2 a3 = ite (t $ false)
  (ite (t $ true)
    a3
    a2)
  (ite (t $ true)
    a1
    a0)
case'β0 : ∀{Γ A}{a0 a1 a2 a3 : Tm Γ A} → case' zer' a0 a1 a2 a3 ≡ a0
case'β0 = {!!}
case'β1 : ∀{Γ A}{a0 a1 a2 a3 : Tm Γ A} → case' one' a0 a1 a2 a3 ≡ a1
case'β1 = {!!}
case'β2 : ∀{Γ A}{a0 a1 a2 a3 : Tm Γ A} → case' two' a0 a1 a2 a3 ≡ a2
case'β2 = {!!}
case'β3 : ∀{Γ A}{a0 a1 a2 a3 : Tm Γ A} → case' three' a0 a1 a2 a3 ≡ a3
case'β3 = {!!}
{-
(Nat ⇒ Bool)           ←       Nat

uj101000...

0 00010100101001001..
1 111010010000000000...
2 000000000000000000...
3 101010101010101010....
4 111111111111111111...
5 111111111110000001.1..
...
-}
