{-# OPTIONS --prop --rewriting #-}

module hf04-4 where

open import Lib
open import RazorAST

-- Bizonyítsd be, hogy a szintaxisban true +o num 1 ≠ num 1 +o true
M1 : Model {lzero}
M1 = ?

module M1 = Model M1

-- Írd fel az állítás helyes típusát, majd a modellt
-- használva bizonyítsd az állítást.
-- Szokásos módon a tt≠ff függvényt szükséges használni.
m1 : ?
m1 e = ?

-----------------------------------------
-- Bizonyítsd be, hogy a szintaxisban az ite mindhárom paraméterében injektív.
-- Ehhez egy olyan függőmodellt szükséges definiálni, amelyben nem
-- csak egy darab I.Tm-et adunk vissza, hanem hármat.
-- Rendezett párokat a sima × (\times = \x = ×) típussal lehet készíteni.
-- Konstruktora ugyanúgy a _,_.
-- A destruktorai π₁ és π₂, amelyek a rendre a rendezett pár első, illetve második komponensét adják vissza.
-- Az × egy jobbra kötő művelet, tehát A × B × C = A × (B × C)
-- A _,_ épp ezért szintén jobbra köt.

IteInj : DepModel {lzero}
IteInj = ?

module IteInj = DepModel IteInj

-- Írd fel az állítás helyes típusát úgy, hogy egyszerre lehessen bizonyítani
-- mindhárom szükséges egyenlőséget.
iteInj : ?
iteInj e = ?

-----------------------------------------
-- Adott az alábbi modell:

M2 : Model {lzero}
M2 = record
  { Tm = ℕ
  ; true = 1
  ; false = 1
  ; ite = λ x y z → 1 + x + y + z
  ; num = λ _ → 1
  ; isZero = suc
  ; _+o_ = λ x y → 1 + x + y
  }

module M2 = Model M2

-- Bizonyítsuk be, hogy mindig LÉTEZIK olyan természetes szám, amelyhez
-- 1-et hozzáadva a term kiértékelését kapjuk.
-- Létezést a Σsp típussal lehet felírni, ez egy függő rendezett pár.
-- Első paramétere mindig a típus, hogy mi létezik.
-- A második paramétere pedig egy függvény, amely eredménye egy Prop-ba képző állítás kell legyen.
-- (Ebben a feladatban a Prop-ba képző állítás az egyenlőség lesz.)
M2D : DepModel {lzero}
M2D = ?
-----------------------------------------
-- Keresős feladat:

-- Ezúttal ugyanúgy a korábbi modellről bizonyítsd, hogy soha nem fog 0-t
-- eredményül adni.

-- A függvény, amelyet érdemes lesz használni, az a Lib-ben is megtalálható
-- zero≠suc vagy suc≠zero függvény attól függően, hogy hol mi jön ki, mit kell bizonyítani.

M2D' : DepModel {lzero}
M2D' = ?

-----------------------------------------
-- Definiálj egy modellt, amely segítségével egy szintaktikus kifejezésből
-- minden I.isZero-t kivágunk a fából, minden mást meghagyunk.

M3 : DepModel {lzero}
M3 = ?

module M3 = DepModel M3

M3-test1 : M3.⟦ I.isZero I.false ⟧ ≡ I.false
M3-test1 = refl
M3-test2 : M3.⟦ I.true ⟧ ≡ I.true
M3-test2 = refl
M3-test3 : M3.⟦ I.ite (I.isZero I.true) (I.num 0 I.+o I.num 1) (I.num 1 I.+o I.isZero (I.num 1)) ⟧
         ≡ I.ite I.true (I.num 0 I.+o I.num 1) (I.num 1 I.+o I.num 1)
M3-test3 = refl
M3-test4 : M3.⟦ I.ite (I.num 10 I.+o I.num 2) (I.ite (I.num 3) I.true I.false) (I.false I.+o I.num 5) ⟧
         ≡ I.ite (I.num 10 I.+o I.num 2) (I.ite (I.num 3) I.true I.false) (I.false I.+o I.num 5)
M3-test4 = refl
M3-test5 : M3.⟦ I.isZero (I.isZero (I.num 2)) ⟧ ≡ I.num 2
M3-test5 = refl
M3-test6 : M3.⟦ I.ite (I.isZero (I.num 0)) (I.num 1) (I.num 2) I.+o (I.isZero (I.isZero (I.num 3) I.+o I.isZero (I.num 6))) ⟧ ≡ I.ite (I.num 0) (I.num 1) (I.num 2) I.+o (I.num 3 I.+o I.num 6)
M3-test6 = refl

-----------------------------------------
-- Adott az alábbi modell:

M4 : Model {lzero}
M4 = record
  { Tm = ℕ
  ; true = 2
  ; false = 0
  ; ite = λ x y z → z
  ; num = λ n → n + n
  ; isZero = λ x → x
  ; _+o_ = λ x y → 2 + x + y
  }

module M4 = Model M4

-- Bizonyítsd be, hogy minden termhez létezik egy KONKRÉT szám, amelyet a modell nem képes visszaadni.
-- (Tehát nem kell Σsp-t használni, hanem csak ≢ valami szám.)
-- A metaelméletben tudjuk, hogy suc injektív, ezt a sucinj függvény bizonyítja.

M4D : DepModel {lzero}
M4D = ?

-----------------------------------------
-- Adott az alábbi modell:

M5 : Model {lzero}
M5 = record
  { Tm = ℕ
  ; true = 1
  ; false = 0
  ; ite = λ x y z → suc z
  ; num = λ n → n + n
  ; isZero = 4 +_
  ; _+o_ = λ x y → 2 + x + y
  }

module M5 = Model M5

-- Bizonyítsd be, hogy minden termhez létezik egy szám, amelyet a modell nem képes visszaadni.
-- A számnak nem feltétlenül kell minden esetben azonosnak lenniük.
-- Ehhez érdemes Σsp-t használni.

M5D : DepModel {lzero}
M5D = ?

-----------------------------------------
-- Adott az alábbi modell:

M6 : DepModel {lzero}
M6 = record
  { Tm∙ = λ _ → ℕ
  ; true∙ = 10
  ; false∙ = 5
  ; ite∙ = λ _ _ _ → 0
  ; num∙ = λ n → 5 * n
  ; isZero∙ = λ x → 5 + x
  ; _+o∙_ = λ x y → x
  }

module M6 = DepModel M6

-- Bizonyítsd be, hogy minden term kiértékelése M6-ban 5-tel osztható számot eredményez.

M6D : DepModel {lzero}
M6D = ?

-----------------------------------------
-- Adott az alábbi két modell:

M7₁ : Model {lzero}
M7₁ = record
  { Tm = 𝟚
  ; true = tt
  ; false = ff
  ; ite = if_then_else_
  ; num = λ {zero → ff
           ; (suc n) → tt}
  ; isZero = λ x → not (not (not x))
  ; _+o_ = λ x y → not x ∧ not y
  }

module M7₁ = Model M7₁

M7₂ : Model {lzero}
M7₂ = record
  { Tm = 𝟚
  ; true = tt
  ; false = ff
  ; ite = if_then_else_
  ; num = λ {zero → ff
           ; (suc n) → tt}
  ; isZero = not
  ; _+o_ = λ x y → not (x ∨ y)
  }

module M7₂ = Model M7₂

-- Bizonyítsd be, hogy a két modell valójában ugyanazt csinálja.
-- Az isZero és +o bizonyításokban érdemes segédfüggvényt definiálni
-- egy megfelelő term mintaillesztésének érdekében.
-- (Vagy lehet a nem rekordos módszert használni és akkor van with.)
M7D : DepModel {lzero}
M7D = ?

------------------------------------------
-- NAGYON HOSSZÚ FELADAT! De érdekes koncepciót mutat be. Legalább elkezdeni érdemes.
-- Előfordulhat olyan "sima" interpretáció is, amire az egyszerű modell kevés, ez egy ilyen szeretne lenni.
-- Ebben a feladatban szeretnénk minél pontosabb interpretációt adni.
-- Megközelítőleg a WT standard modelljét emulálja; a típusellenőrzés itt szemantikában van benne.
-- És borzasztó hosszú megírni szemantikában.

-- Definiáld azt a modellt, amely megpróbál kiértékelni kifejezéseket, ha azok típushelyesek.
-- Nem kell hozzá WT (de órán majd kiderül, hogy WT-ben sokkal egyszerűbb és rövidebb).
-- true és false egyértelműen 𝟚 típusúak.
-- num-ok egyértelműen ℕ típusúak.
-- ite lehet 𝟚 vagy ℕ, attól függ, hogy a paraméterekben mi van.
--   A rövidség kedvéért lehet különböző típusú érték a két ágon, illetve hibás érték lehet egy ágon, ha a másikkal kell kezdenie valamit.
-- isZero: a paramétere ℕ, az eredménye 𝟚, ellenőrzi, hogy a szám 0-e, ekkor tt az eredmény, máskor ff, de lehet, hogy korábban már hibás eredményünk volt, ekkor hibánk van továbbra is.
-- +o: A két paramétere ℕ, az eredménye szintén ℕ, összeadja a számokat, szintén lehetett hiba korábban.

M8 : DepModel {lzero}
M8 = ?

module M8 = DepModel M8

M8-test1 : M8.⟦ I.true ⟧ ≡ tt
M8-test1 = refl
M8-test2 : M8.⟦ I.num 2 ⟧ ≡ 2
M8-test2 = refl
M8-test3 : M8.⟦ I.num 2 I.+o I.true ⟧ ≡ nothing
M8-test3 = refl
M8-test4 : M8.⟦ I.ite I.true (I.num 0) (I.num 1) ⟧ ≡ just (ι₂ 0)
M8-test4 = refl
M8-test5 : M8.⟦ I.ite (I.isZero (I.num 1)) (I.num 0 I.+o I.num 2) I.false ⟧ ≡ just (ι₁ ff)
M8-test5 = refl
M8-test6 : M8.⟦ I.isZero (I.isZero (I.num 0)) ⟧ ≡ nothing
M8-test6 = refl
M8-test7 : M8.⟦ I.isZero (I.num 0) ⟧ ≡ just tt
M8-test7 = refl
M8-test8 : M8.⟦ (I.num 4 I.+o I.num 5) I.+o (I.num 2 I.+o I.num 3) ⟧ ≡ just 14
M8-test8 = refl
M8-test9 : M8.⟦ (I.ite (I.isZero I.true) (I.num 3 I.+o I.true) I.true) I.+o (I.num 2 I.+o I.false) ⟧ ≡ nothing
M8-test9 = refl
M8-test10 : M8.⟦ (I.ite (I.isZero (I.num 1)) (I.num 3 I.+o I.true) (I.num 1)) I.+o (I.num 2 I.+o I.num 3) ⟧ ≡ just 6
M8-test10 = refl
M8-test11 : M8.⟦ I.ite (I.ite (I.isZero (I.num 2 I.+o I.num 0)) (I.num 0) I.true) (I.num 2) (I.true I.+o I.false) ⟧ ≡ just (ι₂ 2)
M8-test11 = refl
M8-test12 : M8.⟦ I.num 0 I.+o (I.num 0 I.+o I.isZero (I.num 0)) ⟧ ≡ nothing
M8-test12 = refl
M8-test13 : M8.⟦ I.num 0 I.+o (I.num 0 I.+o I.num 0) ⟧ ≡ just 0
M8-test13 = refl