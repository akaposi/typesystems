{-# OPTIONS --prop --rewriting #-}

module gy13-4 where

open import Lib
open import Coind.Syntax

num : ∀{Γ} → ℕ → Tm Γ Nat
num zero = zeroo
num (suc n) = suco (num n)

[0→∞] : ∀{Γ} → Tm Γ (Stream Nat)
[0→∞] = ?

test-1 : head (tail [0→∞]) ≡ (num {◇} 1)
test-1 = 
  ?
  ≡⟨ ? ⟩
  ? ∎

repeat : ∀{Γ A} → Tm Γ (A ⇒ Stream A)
repeat = ?

test-2 : head (tail (tail (repeat $ true))) ≡ true {◇}
test-2 =
  ?
  ≡⟨ ? ⟩
  ? ∎
