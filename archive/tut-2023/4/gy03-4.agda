{-# OPTIONS --prop --rewriting #-}

module gy03-4 where

open import Lib
open import RazorAST hiding (D)

-- mutasd meg, hogy true ≠ isZero (num 0) a szintaxisban! ehhez adj
-- meg egy modellt, amiben a true tt-re, az isZero (num 0) ff-re
-- ertekelodik!
TN : Model {lzero}
TN = record
  { Tm = 𝟚
  ; true = tt
  ; false = ff
  ; ite = λ _ _ _ → ff
  ; num = λ _ → ff
  ; isZero = λ _ → ff
  ; _+o_ = λ _ _ → ff
  }
true≠isZeronum0 : ¬ (I.true ≡ I.isZero (I.num 0)) -- \[[ = ⟦ ; \]] = ⟧
true≠isZeronum0 e = tt≠ff (cong (λ x → TN.⟦ x ⟧) e)
  where module TN = Model TN

-- nemsztenderd modell (a szintaxis ertelmezese nem rakepzes)
NS : Model {lzero}
NS = record
       { Tm = ℕ
       ; true = 2
       ; false = 0
       ; ite = λ { zero a b → b ; (suc _) a b → a }
       ; num = λ n → n + n
       ; isZero = λ { zero → 2 ; (suc _) → 0 }
       ; _+o_ = _+_
       }
module testNS where
  module NS = Model NS

  -- adj meg egy ℕ-t, amire nem kepez egyik term sem
  d : ℕ
  d = 1

  -- bizonyitsd be, hogy minden szintaktikus term ertelmezese paros szam
  ps : (t : I.Tm) → Σsp ℕ λ m → NS.⟦ t ⟧ ≡ m + m
  ps I.true = 1 , refl
  ps I.false = 0 , refl
  ps (I.ite b t f) with NS.⟦ b ⟧
  ... | zero = ps f
  ... | suc n = ps t
  ps (I.num n) = n , refl
  ps (I.isZero t) with NS.⟦ t ⟧
  ... | zero = 1 , refl
  ... | suc x = 0 , refl
  ps (x I.+o y) with ps x | ps y
  ... | n , p1 | k , p2 = {!   !} , {!   !}

-- FEL: add meg a legegyszerubb nemsztenderd modellt!
NS' : Model {lzero}
NS' = record
  { Tm = 𝟚
  ; true = tt
  ; false = tt
  ; ite = λ _ _ _ → tt
  ; num = λ _ → tt
  ; isZero = λ _ → tt
  ; _+o_ = λ _ _ → tt
  }
module testNS' where
  module NS' = Model NS'
  b : 𝟚
  b = ff

  -- indukcio
  D : DepModel {lzero}
  D = record
        { Tm∙ = λ t → Lift (NS'.⟦ t ⟧ ≡ tt)
        ; true∙ = mk refl
        ; false∙ = mk refl
        ; ite∙ = λ _ _ _ → mk refl
        ; num∙ = λ _ → mk refl
        ; isZero∙ = λ _ → mk refl
        ; _+o∙_ = λ _ _ → mk refl
        }
  module D = DepModel D
  
  ∀tt : (t : I.Tm) → NS'.⟦ t ⟧ ≡ tt
  ∀tt t = un D.⟦ t ⟧
  
  ns : (Σsp I.Tm λ t → NS'.⟦ t ⟧ ≡ ff) → ⊥
  ns e = tt≠ff ((π₂ e ⁻¹ ◾ ∀tt (π₁ e)) ⁻¹)

-- FEL: product models
Prod : ∀{i j} → Model {i} → Model {j} → Model {i ⊔ j}
Prod M N = record
  { Tm = M.Tm × N.Tm
  ; true = {!!}
  ; false = {!!}
  ; ite = {!!}
  ; num = {!!}
  ; isZero = {!!}
  ; _+o_ = {!!}
  }
  where
    module M = Model M
    module N = Model N

L1' : Model
L1' = record
  { Tm     = ℕ
  ; true   = 1
  ; false  = 1
  ; ite    = λ t t' t'' → t + t' + t''
  ; num    = λ _ → 1
  ; isZero = λ t → t
  ; _+o_   = _+_
  }
module L1' = Model L1'
L2' : Model
L2' = record
  { Tm     = ℕ
  ; true   = 0
  ; false  = 0
  ; ite    = λ t t' t'' → 2 + t + t' + t''
  ; num    = λ _ → 0
  ; isZero = λ t → t
  ; _+o_   = λ t t' → 1 + t + t'
  }
module L2' = Model L2'

L1L2' : DepModel {lzero}
L1L2' = {!!}
module L1L2' = DepModel L1L2'

twolengths : ∀ t → L1'.⟦ t ⟧ ≡ suc L2'.⟦ t ⟧ -- \== = ≡
twolengths t = {!!}
