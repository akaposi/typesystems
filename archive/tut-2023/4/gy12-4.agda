{-# OPTIONS --prop --rewriting #-}

module gy12-4 where

open import Lib
open import Ind.Syntax
open import Ind.Model
open import Ind
open Standard hiding (iteList)

eval : {A : Ty} → Tm ◇ A → St.⟦ A ⟧T
eval t = St.⟦ t ⟧t (mk triv)

num : ∀{Γ} → ℕ → Tm Γ Nat
num zero = zeroo
num (suc n) = suco (num n)

-- isZero

isZero : {Γ : Con} → Tm Γ (Nat ⇒ Bool)
isZero = lam (iteNat true false v0)

isZero-test-1 : eval isZero 0 ≡ tt
isZero-test-1 = tt ∎

isZero-test-2 : eval isZero 1 ≡ ff
isZero-test-2 = ff ∎

isZero-test-3 : eval isZero 6 ≡ ff
isZero-test-3 = ff ∎

isZeroβ₁ : ∀{Γ} → isZero $ num 0 ≡ true {Γ}
isZeroβ₁ = 
  lam (iteNat true false (var vz)) $ zeroo
  ≡⟨ ⇒β ⟩
  iteNat true false (var vz) [ ⟨ zeroo ⟩ ]
  ≡⟨ iteNat[] ⟩
  iteNat (true [ ⟨ zeroo ⟩ ]) (false [ ⟨ zeroo ⟩ ⁺ ]) (var vz [ ⟨ zeroo ⟩ ])
  ≡⟨ cong₃ iteNat true[] false[] vz[⟨⟩] ⟩
  iteNat true false zeroo
  ≡⟨ Natβ₁ ⟩
  true ∎



isZeroβ₂ : ∀{Γ n} → isZero $ num (1 + n) ≡ false {Γ}
isZeroβ₂ {n = n} = {!   !}

-- plus

plus : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
plus = lam (lam (iteNat v0 (suco v0) v1))

plus-test-1 : eval plus 1 1 ≡ 2
plus-test-1 = refl {A = ℕ}

plus-test-2 : eval plus 2 3 ≡ 5
plus-test-2 = refl {A = ℕ}

plus-test-3 : eval plus 0 4 ≡ 4
plus-test-3 = refl {A = ℕ}

plusβ : ∀{n m} → plus $ num n $ num m ≡ num {◇} (n + m)
plusβ = {!   !}
-- times

times : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
times = {!   !}

times-test-1 : eval times 1 5 ≡ 5
times-test-1 = refl {A = ℕ}

times-test-2 : eval times 2 3 ≡ 6
times-test-2 = refl {A = ℕ}

times-test-3 : eval times zero 4 ≡ zero
times-test-3 = refl {A = ℕ}


-- length

length : {Γ : Con}{A : Ty} → Tm Γ (Ty.List A ⇒ Nat)
length = lam (iteList (num 0) (suco v0) v0)

length-test-1 : eval (length $ nil {A = Nat}) ≡ zero
length-test-1 = refl {A = ℕ}

length-test-2 : eval (length $ (cons trivial nil)) ≡ 1
length-test-2 = refl {A = ℕ}

length-test-3 : eval (length $ (cons false (cons true nil))) ≡ 2
length-test-3 = refl {A = ℕ}

length-biz : ∀{Γ} → length $ cons false (cons true nil) ≡ num {Γ} 2
length-biz =
  lam (iteList zeroo (suco (var vz)) (var vz)) $ cons false (cons true nil)
  ≡⟨ ⇒β ⟩
  iteList zeroo (suco (var vz)) (var vz) [ ⟨ cons false (cons true nil) ⟩ ]
  ≡⟨ iteList[] ⟩
  iteList (zeroo [ ⟨ cons false (cons true nil) ⟩ ])
          (suco (var vz) [ (⟨ cons false (cons true nil) ⟩ ⁺) ⁺ ])
          (var vz [ ⟨ cons false (cons true nil) ⟩ ])
  ≡⟨ cong₃ iteList zero[] (suc[] ◾ cong suco vz[⁺]) vz[⟨⟩] ⟩
  iteList zeroo (suco (var vz)) (cons false (cons true nil))
  ≡⟨ Listβ₂ ⟩
  suco (var vz) [ ⟨ false ⟩ ⁺ ] [ ⟨ iteList zeroo (suco (var vz)) (cons true nil) ⟩ ]
  ≡⟨ cong _[ ⟨ iteList zeroo (suco (var vz)) (cons true nil) ⟩ ] (suc[] ◾ cong suco vz[⁺]) ⟩
  suco (var vz) [ ⟨ iteList zeroo (suco (var vz)) (cons true nil) ⟩ ]
  ≡⟨ suc[] ◾ cong suco vz[⟨⟩] ⟩
  suco (iteList zeroo (suco (var vz)) (cons true nil))
  ≡⟨ cong suco (
      iteList zeroo (suco (var vz)) (cons true nil) 
      ≡⟨ Listβ₂ ⟩
      suco (var vz) [ ⟨ true ⟩ ⁺ ] [ ⟨ iteList zeroo (suco (var vz)) nil ⟩ ]
      ≡⟨ cong (λ x → suco (var vz) [ ⟨ true ⟩ ⁺ ] [ ⟨ x ⟩ ]) Listβ₁ ⟩
      suco (var vz) [ ⟨ true ⟩ ⁺ ] [ ⟨ zeroo ⟩ ]
      ≡⟨ cong _[ ⟨ num 0 ⟩ ] (suc[] ◾ cong suco vz[⁺]) ⟩
      suco (var vz) [ ⟨ zeroo ⟩ ]
      ≡⟨ suc[] ◾ cong suco vz[⟨⟩] ⟩
      num 1 ∎) ⟩
  num 2 ∎

-- concat

++ : {Γ : Con}{A : Ty} → Tm Γ (Ty.List A ⇒ Ty.List A ⇒ Ty.List A)
++ = {!   !}

++-test-1 : eval (++ $ nil {A = Bool} $ nil {A = Bool}) ≡ []
++-test-1 = refl

++-test-2 : eval (++ $ nil $ (cons trivial nil)) ≡ mk triv ∷ []
++-test-2 = refl

++-test-3 : eval (++ $ (cons zeroo nil) $ (cons (suco zeroo) nil)) ≡
                zero ∷ 1 ∷ []
++-test-3 = refl

{-
Nat≅UnitList : Nat ≅ (Ty.List Unit)
Nat≅UnitList = {!   !}
-}