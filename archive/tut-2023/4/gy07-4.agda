{-# OPTIONS --prop --rewriting #-}

module gy07-4 where

open import Lib

module DefABT where
  open import DefABT

  open I

  v : (n : ℕ) → ∀{m} → Tm (suc n + m)
  v n = var (v' n) where
    v' : (n : ℕ) → ∀{m} → Var (suc n + m)
    v' zero = vz
    v' (suc n) = vs (v' n)


  -- (let x:=num 1 in x) +o let y:=num 1 in y +o let z:=x +o a +o y in (x +o z) +o (y +o x)
  tm-2 : Tm 41
  tm-2 = (def (num 1) v0) +o def (num 1) (v0 +o def (v 40 +o v 21 +o v0) ((v 41 +o v0) +o (v1 +o v 41)))

module DefWT where

  open import DefWT
  open I
  -- \di2 = \diw = ◇ 
  -- \t6 = \tw2 = ▹  
  tm-0 : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Bool
  tm-0 = isZero (ite v2 (v1 +o v0) v1)

  -- \GG = Γ
  tm-0' : {Γ : Con} → Tm (Γ ▹ Bool ▹ Nat ▹ Nat) Bool
  tm-0' = isZero (ite v2 (v1 +o v0) v1)

  tm-0'' : {Γ : Con}{A : Ty} → Tm (Γ ▹ Bool ▹ A ▹ Nat ▹ Nat) Bool
  tm-0'' = isZero (ite v3 (v1 +o v0) v1)

  tm-1 : {Γ : Con}{A : Ty} → Tm (Γ ▹ Nat ▹ A) Nat
  tm-1 = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  tm-1' : {Γ : Con} → Tm (Γ ▹ Nat ▹ Nat) Nat
  tm-1' = def (v1 +o num 5) (ite (isZero v0) v1 (num 0))

  tm-2 : ∀{Γ} → Tm (Γ ▹ Nat ▹ Bool ▹ Nat) Bool
  tm-2 = ite v1 (isZero v0) (isZero v2)

  tm-3 : ∀{Γ A} → Tm (Γ ▹ Bool ▹ A ▹ Nat) A
  tm-3 = ite (ite v2 (isZero v0) v2) v1 v1

  tm-3' : ∀{Γ A B} → Tm (Γ ▹ A ▹ B ▹ Bool ▹ A ▹ Nat) A
  tm-3' = ite (ite v2 (isZero v0) v2) v1 v4

  tm-4 : ∀{Γ} → Tm (Γ ▹ Bool ▹ Bool ▹ Nat) Bool
  tm-4 = ite v1 (ite v1 v2 v2) (isZero v0)

  tm-5 : ∀{Γ A} → Tm (Γ ▹ Nat ▹ A) Nat
  tm-5 = def (v1 +o num 10) (ite (isZero v0) (v2 +o num 20) v0)

  tm-6 : ∀{Γ} → Tm (Γ ▹ Nat ▹ Nat) Bool
  tm-6 = isZero (def (isZero v1) (ite v0 v1 v2))

  tm-7 : ∀{Γ} → Tm (Γ ▹ Nat ▹ Nat ▹ Nat) Nat
  tm-7 = v1 +o def (def (num 2 +o v0) (v0 +o v1 +o v2)) (v2 +o v3) +o num 10

module Substitutions where
  import RazorWT
  import DefWT
  module NB = RazorWT.I
  module D = DefWT.I
  open D
  
  NB→D : RazorWT.Model
  NB→D = record
    { Ty     = D.Ty
    ; Tm     = D.Tm D.◇
    ; Nat    = D.Nat
    ; Bool   = D.Bool
    ; true   = D.true
    ; false  = D.false
    ; ite    = D.ite
    ; num    = D.num
    ; isZero = D.isZero
    ; _+o_   = D._+o_
    }

  module NBTD = RazorWT.Model NB→D

  data NBCon : Set where
    NB◇ : NBCon
    _NB▹_ : NBCon → NB.Ty → NBCon

  {-# DISPLAY NB◇ = D.◇ #-}
  {-# DISPLAY _NB▹_ = D._▹_ #-}

  infixl 5 _,o_
  data NBEnv : NBCon → Set where
    ε   : NBEnv NB◇
    _,o_ : {Γ : NBCon} {A : NB.Ty} → NBEnv Γ → NB.Tm A → NBEnv (Γ NB▹ A)

  D→NB : DefWT.Model {k = lzero}
  D→NB = record
    { Ty     = NB.Ty
    ; Nat    = NB.Nat
    ; Bool   = NB.Bool
    ; Con    = NBCon
    ; ◇      = NB◇
    ; _▹_    = _NB▹_
    ; Var    = λ Γ A → NBEnv Γ → NB.Tm A
    ; vz     = λ where (_ ,o a) → a
    ; vs     = λ where v (e ,o _) → v e
    ; Tm     = λ Γ A → NBEnv Γ → NB.Tm A
    ; var    = λ v → v
    ; def    = λ t f e → f (e ,o t e)
    ; true   = λ _ → NB.true
    ; false  = λ _ → NB.false
    ; ite    = λ co tr fa e → NB.ite (co e) (tr e) (fa e)
    ; num    = λ n _ → NB.num n
    ; isZero = λ t e → NB.isZero (t e)
    ; _+o_   = λ l r e → l e NB.+o r e
    }

  module DTNB = DefWT.Model D→NB

  norm : {A : D.Ty} → D.Tm D.◇ A → RazorWT.St.⟦ (DTNB.⟦ A ⟧T) ⟧T
  norm t = RazorWT.St.⟦ DTNB.⟦ t ⟧t ε ⟧t

  eval : {Γ : D.Con} {A : D.Ty} → D.Tm Γ A → NBEnv DTNB.⟦ Γ ⟧C → RazorWT.St.⟦ (DTNB.⟦ A ⟧T) ⟧T
  eval t e = RazorWT.St.⟦ DTNB.⟦ t ⟧t e ⟧t

  Env : D.Con → Set
  Env Γ = NBEnv DTNB.⟦ Γ ⟧C

  tm-8 : ∀{Γ} → Tm (Γ ▹ Nat ▹ Nat ▹ Nat) Nat
  tm-8 = def 
          (v1 +o num 10) 
          (ite 
            (isZero v0) 
            (def 
              (isZero v1) 
              (v2 +o v1 +o v4)) 
            (def 
              v0 
              (v0 +o v1 +o v2)
            +o v3))
  -- \Ge = ε
  env : Env (◇ ▹ Nat ▹ Nat ▹ Nat)
  env = ε ,o NB.num 0 ,o NB.num 2 ,o NB.num 6

  test-tm-8 : eval (tm-8 {◇}) env ≡ 30
  test-tm-8 = refl