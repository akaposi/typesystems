{-# OPTIONS --prop --rewriting #-}

module hf05-4 where

open import Lib

module WT where
  open import RazorWT

  -- Definiáld azt a modellt, amely kiszámolja egy fa magasságát.
  -- Az {lsuc lzero} azért kell, hogy meg lehessen mondani, hogy
  -- a típusokat miként interpretálom a metanyelvben.
  -- Mivel Set-be szeretnénk, ezért a Ty : Set₁ kell, hogy teljesüljön, ez lesz az lsuc lzero
  height' : Model {lsuc lzero} {lzero}
  height' = ?

  module H = Model height'
  
  height-test0 : H.Tm H.Nat ≡ ℕ
  height-test0 = refl
  height-test0' : H.Tm H.Bool ≡ ℕ
  height-test0' = refl
  height-test1 : H.⟦ I.num 9 ⟧t ≡ 0
  height-test1 = refl
  height-test2 : H.⟦ I.ite I.true (I.num 9 I.+o I.num 10) (I.num 0) ⟧t ≡ 2
  height-test2 = refl
  height-test3 : H.⟦ I.ite (I.isZero (I.num 0)) (I.num 0 I.+o I.num 4 I.+o I.num 2) (I.num 3) 
                     I.+o
                     I.ite 
                        (I.isZero (I.num 1 I.+o I.num 3))
                        (I.num 0) 
                        (I.num 9 I.+o I.num 10 I.+o I.num 2 I.+o I.num 0) ⟧t ≡ 5
  height-test3 = refl
  height-test4 : H.⟦ I.num 9 I.+o I.num 2 I.+o I.num 3 ⟧t ≡ 2
  height-test4 = refl

  -- Definiálj egy modellt, amely segítségével bizonyítható, hogy
  -- a szintaxisban isZero (num 0) ≠ true és isZero (num 0) ≠ false
  not-true-false : Model {lsuc lzero} {lzero}
  not-true-false = ?

  module NTF = Model not-true-false
  
  3bools : let x = I.isZero (I.num 0) in Lift (x ≢ I.true) × Lift (x ≢ I.false)
  3bools = {!   !}

module WT-equality where

  open import Razor
  open I

  eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  eq-10 =
    (ite false (num 1 +o num 1) (num 0) +o num 0)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 0
      ∎

  eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  eq-11 =
    ite (isZero (num 0 +o num 1)) false (isZero (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    true
      ∎

  eq-12 : num 3 +o num 2 ≡ ite true (num 5) (num 1)
  eq-12 =
    num 3 +o num 2
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    ite true (num 5) (num 1)
      ∎  

  eq-15 : num 9 +o num 5 ≡ num 5 +o num 9
  eq-15 = {!   !}

  eq-16 : isZero (num 3 +o num 4) ≡ isZero (num 1 +o num 6)
  eq-16 = {!   !}

  eq-17 : ite true (num 1 +o num 2) (num 3) ≡ ite false (num 4) (num 1 +o num 2)
  eq-17 = {!   !}

  -- Ezen három bizonyítás az implicit paraméterek nélkül sárgulni fognak.
  -- Ahol sárga ott azt jelenti, hogy agda nem tudta kitalálni az implicit paramétereket,
  -- tehát nekünk kell explicit átadni. Ahogy fel vannak véve a paraméterek a sirály zárójelek
  -- között, ugyanúgy kell implicit paramétereket is explicit átadni.

  -- a számok metanyelvből vannak, kell a metanyelvbeli bizonyítás hozzá
  idr+o : ∀{n} → num n +o num 0 ≡ num n
  idr+o {n} = {!   !}

  -- szintén
  +o-comm : ∀{m n} → num m +o num n ≡ num n +o num m
  +o-comm {m} {n} = {!   !}

  -- szintén
  ass+o : ∀{m n k} → num m +o num n +o num k ≡ num m +o (num n +o num k)
  ass+o {m} {n} = {!   !}

  eq-18 : (num 5 +o num 6) +o num 7 ≡ num 5 +o (num 6 +o num 7)
  eq-18 = {!   !}

  eq-19 : ite 
            (ite (isZero (num 10)) false (isZero (num 1 +o num 0)))
            (num 3 +o num 7)
            (num 5 +o (num 9 +o num 6))
          ≡
          ite
            (isZero (num 0))
            (num 14)
            (num 1 +o num 2)
          +o
          ite
            (isZero (num 1))
            (num 7)
            (num 3 +o num 3)
  eq-19 = {!   !}

  eq-20 : ite 
            (isZero (num 1 +o num 2))
            (ite false (isZero (num 0)) (isZero (num 0 +o num 1)))
            (ite (isZero (num 0)) true (isZero (num 1 +o num 9))) 
          ≡ 
          isZero (ite 
            (isZero (num 0 +o num 0 +o num 0))
            (ite true (num 0 +o num 0) (num 10))
            (ite (isZero (num 231)) (num 1 +o num 8) (num 1 +o num 9)))
  eq-20 = {!  !}