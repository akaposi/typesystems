{-# OPTIONS --prop --rewriting #-}

module hf08-4sol where

open import Lib
open import Def
open import Def.Syntax

var-eq-6 : def (num 0) (def (isZero v0) (ite v0 false true)) ≡ false {◇}
var-eq-6 =
  def (num 0) (def (isZero v0) (ite v0 false true))
  ≡⟨ cong _[ ⟨ num 0 ⟩ ] ite[] ⟩
  ite (var vz [ ⟨ isZero (var vz) ⟩ ])
      (false [ ⟨ isZero (var vz) ⟩ ])
      (true [ ⟨ isZero (var vz) ⟩ ])
    [ ⟨ num zero ⟩ ]
  ≡⟨ cong₃ (λ x y z → ite x y z [ ⟨ num zero ⟩ ]) vz[⟨⟩] false[] true[] ⟩
  ite (isZero (var vz)) false true [ ⟨ num zero ⟩ ]
  ≡⟨ ite[] ⟩
  ite (isZero (var vz) [ ⟨ num 0 ⟩ ]) (false [ ⟨ num 0 ⟩ ]) (true [ ⟨ num 0 ⟩ ])
  ≡⟨ cong₃ ite (isZero[] ◾ cong isZero vz[⟨⟩]) false[] true[] ⟩
  ite (isZero (num zero)) false true
  ≡⟨ cong (λ x → ite x false true) isZeroβ₁ ⟩
  ite true false true
  ≡⟨ iteβ₁ ⟩
  false ∎

var-eq-7 : def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0))) ≡ num {◇} 2
var-eq-7 =
  def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
  ≡⟨ cong (λ x → v0 +o num 1 +o x [ ⟨ num 1 ⟩ ]) ite[] ⟩
  var vz +o num 1 +o ite (isZero (var vz) [ ⟨ var vz ⟩ ]) (num 1 [ ⟨ var vz ⟩ ]) (num 0 [ ⟨ var vz ⟩ ]) [ ⟨ num 1 ⟩ ]
  ≡⟨ cong₃ (λ x y z → v0 +o num 1 +o ite x y z [ ⟨ num 1 ⟩ ]) (isZero[] ◾ cong isZero vz[⟨⟩]) num[] num[] ⟩
  var vz +o num 1 +o ite (isZero (var vz)) (num 1) (num 0) [ ⟨ num 1 ⟩ ]
  ≡⟨ +[] ⟩
  (var vz +o num 1 [ ⟨ num 1 ⟩ ]) +o (ite (isZero (var vz)) (num 1) (num zero) [ ⟨ num 1 ⟩ ])
  ≡⟨ cong₂ _+o_ +[] ite[] ⟩
  (var vz [ ⟨ num 1 ⟩ ]) +o (num 1 [ ⟨ num 1 ⟩ ]) +o
  ite (isZero (var vz) [ ⟨ num 1 ⟩ ]) (num 1 [ ⟨ num 1 ⟩ ]) (num zero [ ⟨ num 1 ⟩ ])
  ≡⟨ cong₃ (λ x y z → x +o y +o z) vz[⟨⟩] num[] (cong₃ ite (isZero[] ◾ cong isZero vz[⟨⟩]) num[] num[]) ⟩
  num 1 +o num 1 +o ite (isZero (num 1)) (num 1) (num 0)
  ≡⟨ cong₂ (λ x y → x +o ite y (num 1) (num 0)) +β isZeroβ₂ ⟩
  num 2 +o ite false (num 1) (num 0)
  ≡⟨ cong (num 2 +o_) iteβ₂ ⟩
  num 2 +o num 0
  ≡⟨ +β ⟩
  num 2 ∎

var-eq-8 : def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0))) ≡ false {◇}
var-eq-8 =
  def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0)))
  ≡⟨ cong (λ x → x [ ⟨ num 0 ⟩ ] [ ⟨ false ⟩ ]) (isZero[] ◾ cong isZero vz[⟨⟩]) ⟩
  isZero (ite (var (vs vz)) (var vz) (var vz +o num 1)) [ ⟨ num zero ⟩ ] [ ⟨ false ⟩ ]
  ≡⟨ cong _[ ⟨ false ⟩ ] isZero[] ◾ cong (λ x → isZero x [ ⟨ false ⟩ ]) ite[] ⟩
  isZero (ite (var (vs vz) [ ⟨ num 0 ⟩ ]) (var vz [ ⟨ num 0 ⟩ ]) (var vz +o num 1 [ ⟨ num 0 ⟩ ])) [ ⟨ false ⟩ ]
  ≡⟨ cong₃ (λ x y z → isZero (ite x y z) [ ⟨ false ⟩ ]) vs[⟨⟩] vz[⟨⟩] +[] ⟩
  isZero (ite (var vz) (num 0) ((var vz [ ⟨ num 0 ⟩ ]) +o (num 1 [ ⟨ num 0 ⟩ ]))) [ ⟨ false ⟩ ]
  ≡⟨ cong₂ (λ x y → isZero (ite (var vz) (num 0) (x +o y)) [ ⟨ false ⟩ ]) vz[⟨⟩] num[] ⟩
  isZero (ite (var vz) (num 0) (num 0 +o num 1)) [ ⟨ false ⟩ ]
  ≡⟨ cong (λ x → isZero (ite (var vz) (num 0) x) [ ⟨ false ⟩ ]) +β ⟩
  isZero (ite (var vz) (num 0) (num 1)) [ ⟨ false ⟩ ]
  ≡⟨ isZero[] ◾ cong isZero ite[] ⟩
  isZero (ite (var vz [ ⟨ false ⟩ ]) (num 0 [ ⟨ false ⟩ ]) (num 1 [ ⟨ false ⟩ ]))
  ≡⟨ cong₃ (λ x y z → isZero (ite x y z)) vz[⟨⟩] num[] num[] ⟩
  isZero (ite false (num 0) (num 1))
  ≡⟨ cong isZero iteβ₂ ⟩
  isZero (num 1)
  ≡⟨ isZeroβ₂ ⟩
  false ∎

var-eq-9 : def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0))) ≡ num {◇} 2
var-eq-9 =
  def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0)))
  ≡⟨ (cong (_[ ⟨ num 0 ⟩ ])) ite[] ⟩
  ite (isZero (var vz) [ ⟨ num 1 +o var vz ⟩ ])
      (var (vs vz) [ ⟨ num 1 +o var vz ⟩ ])
      (var (vs vz) +o num 1 +o var vz [ ⟨ num 1 +o var vz ⟩ ])
    [ ⟨ num zero ⟩ ]
  ≡⟨ cong₃ (λ x y z → ite x y z [ ⟨ num 0 ⟩ ]) (isZero[] ◾ cong isZero vz[⟨⟩]) vs[⟨⟩] +[] ⟩
  ite (isZero (num 1 +o var vz))
      (var vz)
      ((var (vs vz) +o num 1 [ ⟨ num 1 +o var vz ⟩ ]) +o (var vz [ ⟨ num 1 +o var vz ⟩ ]))
    [ ⟨ num 0 ⟩ ]
  ≡⟨ cong₂ (λ x y → ite (isZero (num 1 +o v0)) v0 (x +o y) [ ⟨ num 0 ⟩ ]) +[] vz[⟨⟩] ⟩
  ite (isZero (num 1 +o var vz))
      (var vz)
      ((var (vs vz) [ ⟨ num 1 +o var vz ⟩ ]) +o (num 1 [ ⟨ num 1 +o var vz ⟩ ]) +o (num 1 +o var vz))
    [ ⟨ num 0 ⟩ ]
  ≡⟨ cong₂ (λ x y → ite (isZero (num 1 +o v0)) v0 (x +o y +o (num 1 +o v0)) [ ⟨ num 0 ⟩ ]) vs[⟨⟩] num[] ⟩
  ite (isZero (num 1 +o var vz)) (var vz) (var vz +o num 1 +o (num 1 +o var vz)) [ ⟨ num 0 ⟩ ]
  ≡⟨ ite[] ⟩
  ite (isZero (num 1 +o var vz) [ ⟨ num zero ⟩ ])
      (var vz [ ⟨ num zero ⟩ ])
      (var vz +o num 1 +o (num 1 +o var vz) [ ⟨ num zero ⟩ ])
  ≡⟨ cong₃ ite (isZero[] ◾ cong isZero +[]) vz[⟨⟩] (+[] ◾ cong₂ _+o_ +[] +[]) ⟩
  ite (isZero ((num 1 [ ⟨ num zero ⟩ ]) +o (var vz [ ⟨ num zero ⟩ ])))
      (num zero)
      ((var vz [ ⟨ num zero ⟩ ]) +o (num 1 [ ⟨ num zero ⟩ ]) +o ((num 1 [ ⟨ num zero ⟩ ]) +o (var vz [ ⟨ num zero ⟩ ])))
  ≡⟨ cong₃ (λ x y z → ite (isZero (x +o y)) (num 0) z) num[] vz[⟨⟩] (cong₂ _+o_ (cong₂ _+o_ vz[⟨⟩] num[]) (cong₂ _+o_ num[] vz[⟨⟩])) ⟩
  ite (isZero (num 1 +o num 0)) (num 0) (num 0 +o num 1 +o (num 1 +o num 0))
  ≡⟨ cong₃ (λ x y z → ite (isZero x) (num 0) (y +o z)) +β +β +β ⟩
  ite (isZero (num 1)) (num 0) (num 1 +o num 1)
  ≡⟨ cong₂ (λ x y → ite x (num 0) y) isZeroβ₂ +β ⟩
  ite false (num 0) (num 2)
  ≡⟨ iteβ₂ ⟩
  num 2 ∎

var-eq-10 : ite v0 v1 v2 +o ite (isZero v1) v2 (num 2) [ ⟨ true ⟩ ] [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ] ≡ num {◇} 5
var-eq-10 =
  ite (var vz) (var (vs vz)) (var (vs (vs vz))) +o
  ite (isZero (var (vs vz))) (var (vs (vs vz))) (num 2)
    [ ⟨ true ⟩ ]
    [ ⟨ num 3 ⟩ ]
    [ ⟨ num 10 ⟩ ]
  ≡⟨ cong (λ x → x [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ]) +[] ⟩
  (ite (var vz) (var (vs vz)) (var (vs (vs vz))) [ ⟨ true ⟩ ]) +o
  (ite (isZero (var (vs vz))) (var (vs (vs vz))) (num 2) [ ⟨ true ⟩ ])
    [ ⟨ num 3 ⟩ ]
    [ ⟨ num 10 ⟩ ]
  ≡⟨ cong₂ (λ x y → x +o y [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ]) ite[] ite[] ⟩
  ite (var vz [ ⟨ true ⟩ ]) (var (vs vz) [ ⟨ true ⟩ ]) (var (vs (vs vz)) [ ⟨ true ⟩ ])
  +o
  ite (isZero (var (vs vz)) [ ⟨ true ⟩ ]) (var (vs (vs vz)) [ ⟨ true ⟩ ]) (num 2 [ ⟨ true ⟩ ])
    [ ⟨ num 3 ⟩ ]
    [ ⟨ num 10 ⟩ ]
  ≡⟨ cong₃ (λ x y z → ite x y z +o ite (isZero (var (vs vz)) [ ⟨ true ⟩ ]) (var (vs (vs vz)) [ ⟨ true ⟩ ]) (num 2 [ ⟨ true ⟩ ]) [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ]) vz[⟨⟩] vs[⟨⟩] vs[⟨⟩] ⟩
  ite true (var vz) (var (vs vz)) +o ite (isZero (var (vs vz)) [ ⟨ true ⟩ ]) (var (vs (vs vz)) [ ⟨ true ⟩ ]) (num 2 [ ⟨ true ⟩ ]) [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ]
  ≡⟨ cong₃ (λ x y z → x +o ite y (var (vs (vs vz)) [ ⟨ true ⟩ ]) z [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ]) iteβ₁ isZero[] num[] ⟩
  var vz +o ite (isZero (var (vs vz) [ ⟨ true ⟩ ])) (var (vs (vs vz)) [ ⟨ true ⟩ ]) (num 2) [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ]
  ≡⟨ cong₂ (λ x y → var vz +o ite (isZero x) y (num 2) [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ]) vs[⟨⟩] vs[⟨⟩] ⟩
  var vz +o ite (isZero (var vz)) (var (vs vz)) (num 2) [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ]
  ≡⟨ cong _[ ⟨ num 10 ⟩ ] +[] ⟩
  (var vz [ ⟨ num 3 ⟩ ]) +o (ite (isZero (var vz)) (var (vs vz)) (num 2) [ ⟨ num 3 ⟩ ]) [ ⟨ num 10 ⟩ ]
  ≡⟨ cong₂ (λ x y → x +o y [ ⟨ num 10 ⟩ ]) vz[⟨⟩] ite[] ⟩
  num 3 +o ite (isZero (var vz) [ ⟨ num 3 ⟩ ]) (var (vs vz) [ ⟨ num 3 ⟩ ]) (num 2 [ ⟨ num 3 ⟩ ]) [ ⟨ num 10 ⟩ ]
  ≡⟨ cong₃ (λ x y z → num 3 +o ite x y z [ ⟨ num 10 ⟩ ]) (isZero[] ◾ cong isZero vz[⟨⟩]) vs[⟨⟩] num[] ⟩
  num 3 +o ite (isZero (num 3)) (var vz) (num 2) [ ⟨ num 10 ⟩ ]
  ≡⟨ cong (λ x → num 3 +o x [ ⟨ num 10 ⟩ ]) (cong (λ x → ite x v0 (num 2)) isZeroβ₂ ◾ iteβ₂) ⟩
  num 3 +o num 2 [ ⟨ num 10 ⟩ ]
  ≡⟨ cong _[ ⟨ num 10 ⟩ ] +β ⟩
  num 5 [ ⟨ num 10 ⟩ ]
  ≡⟨ num[] ⟩
  num 5 ∎

-- nem kell megijedni a ⁺-tól, szerepelhet, nem izgat senkit, mert az ite "rossz" ágán van.
var-eq-11 : ite (def (num 0) (isZero v0)) (def (v0 +o num 1) (num 3 +o v0 +o v1)) (v0 +o v1 [ ⟨ num 5 ⟩ ⁺ ]) [ ⟨ num 2 ⟩ ] ≡ num {◇} 8
var-eq-11 =
  ite (isZero (var vz) [ ⟨ num zero ⟩ ])
      (num 3 +o var vz +o var (vs vz) [ ⟨ var vz +o num 1 ⟩ ])
      (var vz +o var (vs vz) [ ⟨ num 5 ⟩ ⁺ ])
    [ ⟨ num 2 ⟩ ]
  ≡⟨ cong (λ x → ite x
      (num 3 +o var vz +o var (vs vz) [ ⟨ var vz +o num 1 ⟩ ])
      (var vz +o var (vs vz) [ ⟨ num 5 ⟩ ⁺ ])
    [ ⟨ num 2 ⟩ ]) (isZero[] ◾ cong isZero vz[⟨⟩]) ⟩
  ite (isZero (num 0)) 
      (num 3 +o var vz +o var (vs vz) [ ⟨ var vz +o num 1 ⟩ ])
      (var vz +o var (vs vz) [ ⟨ num 5 ⟩ ⁺ ])
    [ ⟨ num 2 ⟩ ]
  ≡⟨ cong _[ ⟨ num 2 ⟩ ] (cong (λ x → ite x (num 3 +o var vz +o var (vs vz) [ ⟨ var vz +o num 1 ⟩ ]) (var vz +o var (vs vz) [ ⟨ num 5 ⟩ ⁺ ])) isZeroβ₁ ◾ iteβ₁) ⟩
  num 3 +o var vz +o var (vs vz) [ ⟨ var vz +o num 1 ⟩ ] [ ⟨ num 2 ⟩ ]
  ≡⟨ cong _[ ⟨ num 2 ⟩ ] +[] ⟩
  (num 3 +o var vz [ ⟨ var vz +o num 1 ⟩ ]) +o (var (vs vz) [ ⟨ var vz +o num 1 ⟩ ]) [ ⟨ num 2 ⟩ ]
  ≡⟨ cong₂ (λ x y → x +o y [ ⟨ num 2 ⟩ ]) +[] vs[⟨⟩] ⟩
  (num 3 [ ⟨ var vz +o num 1 ⟩ ]) +o (var vz [ ⟨ var vz +o num 1 ⟩ ]) +o var vz [ ⟨ num 2 ⟩ ]
  ≡⟨ cong₂ (λ x y → x +o y +o v0 [ ⟨ num 2 ⟩ ]) num[] vz[⟨⟩] ⟩
  num 3 +o (var vz +o num 1) +o var vz [ ⟨ num 2 ⟩ ]
  ≡⟨ +[] ⟩
  (num 3 +o (var vz +o num 1) [ ⟨ num 2 ⟩ ]) +o (var vz [ ⟨ num 2 ⟩ ])
  ≡⟨ cong₂ _+o_ +[] vz[⟨⟩] ⟩
  (num 3 [ ⟨ num 2 ⟩ ]) +o (var vz +o num 1 [ ⟨ num 2 ⟩ ]) +o num 2
  ≡⟨ cong₂ (λ x y → x +o y +o num 2) num[] +[] ⟩
  num 3 +o ((var vz [ ⟨ num 2 ⟩ ]) +o (num 1 [ ⟨ num 2 ⟩ ])) +o num 2
  ≡⟨ cong₂ (λ x y → num 3 +o (x +o y) +o num 2) vz[⟨⟩] num[] ⟩
  num 3 +o (num 2 +o num 1) +o num 2
  ≡⟨ cong (λ x → num 3 +o x +o num 2) +β ⟩
  num 3 +o num 3 +o num 2
  ≡⟨ cong (_+o num 2) +β ⟩
  num 6 +o num 2
  ≡⟨ +β ⟩
  num 8 ∎

var-eq-12 : v0 +o num 1 [ ⟨ v0 {◇ ▹ Nat} ⟩ ] ≡ v0 [ ⟨ v0 +o num 1 ⟩ ]
var-eq-12 = 
  v0 +o num 1 [ ⟨ v0 ⟩ ]
  ≡⟨ +[] ⟩
  (v0 [ ⟨ v0 ⟩ ]) +o (num 1 [ ⟨ v0 ⟩ ])
  ≡⟨ cong₂ _+o_ vz[⟨⟩] num[] ⟩
  v0 +o num 1
  ≡⟨ vz[⟨⟩] ⁻¹ ⟩
  v0 [ ⟨ v0 +o num 1 ⟩ ] ∎