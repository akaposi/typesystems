{-# OPTIONS --prop --rewriting #-}

module hf09-4 where

open import Lib
open import Def
open import Def.Syntax

var-eq-14 : v1 [ ⟨ num 2 ⟩ ⁺ ] ≡ num {◇ ▹ Bool} 2
var-eq-14 =
  ?
  ≡⟨ ? ⟩
  ? ∎

var-eq-15 : v2 [ ⟨ num 10 ⟩ ⁺ ⁺ ] ≡ num {◇ ▹ Bool ▹ Nat} 10
var-eq-15 =
  ?
  ≡⟨ ? ⟩
  ? ∎

var-eq-16 : isZero {◇ ▹ Nat ▹ Nat ▹ Bool} (v1 +o v2) [ ⟨ num 0 ⟩ ⁺ ] [ ⟨ num 2 ⟩ ⁺ ] ≡ ite v1 false true [ ⟨ true ⟩ ⁺ ]
var-eq-16 =
  ?
  ≡⟨ ? ⟩
  ? ∎

var-eq-17 : isZero {◇ ▹ Nat ▹ Nat ▹ Nat} v1 [ ⟨ v0 +o num 0 ⟩ ⁺ ] [ ⟨ num 2 ⟩ ⁺ ]
          ≡ isZero v0 [ ⟨ num 1 +o v1 ⟩ ] [ ⟨ num 0 ⟩ ⁺ ]
var-eq-17 =
  ?
  ≡⟨ ? ⟩
  ? ∎