{-# OPTIONS --prop --rewriting #-}

module gy13-4 where

open import Lib
open import Coind.Syntax

num : ∀{Γ} → ℕ → Tm Γ Nat
num zero = zeroo
num (suc n) = suco (num n)

[0→∞] : ∀{Γ} → Tm Γ (Stream Nat)
[0→∞] = genStream v0 (suco v0) (num 0)

test-1 : head (tail [0→∞]) ≡ (num {◇} 1)
test-1 = 
  head (tail (genStream (var vz) (suco (var vz)) zeroo))
  ≡⟨ cong head Streamβ₂ ⟩
  head (genStream (var vz) (suco (var vz)) (suco (var vz) [ ⟨ zeroo ⟩ ]))
  ≡⟨ Streamβ₁ ⟩
  var vz [ ⟨ suco (var vz) [ ⟨ zeroo ⟩ ] ⟩ ]
  ≡⟨ vz[⟨⟩] ⟩
  suco (var vz) [ ⟨ zeroo ⟩ ]
  ≡⟨ suc[] ⟩
  suco (var vz [ ⟨ zeroo ⟩ ])
  ≡⟨ cong suco vz[⟨⟩] ⟩
  num 1 ∎

repeat : ∀{Γ A} → Tm Γ (A ⇒ Stream A)
repeat = lam (genStream v0 v0 v0)

test-2 : head (tail (tail (repeat $ true))) ≡ true {◇}
test-2 =
  head (tail (tail (lam (genStream (var vz) (var vz) (var vz)) $ true)))
  ≡⟨ cong (λ x → head (tail (tail x))) (⇒β ◾ genStream[] ◾ cong₃ genStream vz[⁺] vz[⁺] vz[⟨⟩]) ⟩
  head (tail (tail (genStream (var vz) (var vz) true)))
  ≡⟨ cong (λ x → head (tail x)) Streamβ₂ ⟩
  head (tail (genStream (var vz) (var vz) (var vz [ ⟨ true ⟩ ])))
  ≡⟨ cong head Streamβ₂ ⟩
  head (genStream (var vz) (var vz) (var vz [ ⟨ var vz [ ⟨ true ⟩ ] ⟩ ]))
  ≡⟨ Streamβ₁ ⟩
  var vz [ ⟨ var vz [ ⟨ var vz [ ⟨ true ⟩ ] ⟩ ] ⟩ ]
  ≡⟨ vz[⟨⟩] ⟩
  var vz [ ⟨ var vz [ ⟨ true ⟩ ] ⟩ ]
  ≡⟨ vz[⟨⟩] ⟩
  var vz [ ⟨ true ⟩ ]
  ≡⟨ vz[⟨⟩] ⟩
  true ∎

halfStream : ∀{Γ A} → Tm Γ (Stream A ⇒ Stream A)
halfStream = lam (genStream (head v0) (tail (tail v0)) v0)

test-4 : head (tail (halfStream $ [0→∞])) ≡ num {◇} 2
test-4 =
  head (tail
  (lam (genStream (head (var vz)) (tail (tail (var vz))) (var vz)) $
  genStream (var vz) (suco (var vz)) zeroo))
  ≡⟨ cong (λ x → head (tail x)) ⇒β ⟩
  head (tail
  (genStream (head (var vz)) (tail (tail (var vz))) (var vz) [
  ⟨ genStream (var vz) (suco (var vz)) zeroo ⟩ ]))
  ≡⟨ cong (λ x → head (tail x)) genStream[] ⟩
  head (tail
  (genStream
    (head (var vz) [ ⟨ genStream (var vz) (suco (var vz)) zeroo ⟩ ⁺ ])
    (tail (tail (var vz)) [ ⟨ genStream (var vz) (suco (var vz)) zeroo ⟩ ⁺ ])
    (var vz [ ⟨ genStream (var vz) (suco (var vz)) zeroo ⟩ ])))
  ≡⟨ cong₃ (λ x y z → head (tail (genStream x y z))) (head[] ◾ cong head vz[⁺]) (tail[] ◾ cong tail tail[] ◾ cong (λ x → tail (tail x)) vz[⁺]) vz[⟨⟩] ⟩
  head (tail
  (genStream (head (var vz)) (tail (tail (var vz)))
  (genStream (var vz) (suco (var vz)) zeroo)))
  ≡⟨ cong head Streamβ₂ ⟩
  head (genStream (head (var vz)) (tail (tail (var vz))) (tail (tail (var vz))
    [ ⟨ genStream (var vz) (suco (var vz)) zeroo ⟩ ]))
  ≡⟨ Streamβ₁ ⟩
  head (var vz) [ ⟨ tail (tail (var vz)) [ ⟨ genStream (var vz) (suco (var vz)) zeroo ⟩ ] ⟩ ]
  ≡⟨ head[] ◾ cong head vz[⟨⟩] ⟩
  head (tail (tail (var vz)) [ ⟨ genStream (var vz) (suco (var vz)) zeroo ⟩ ])
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !} ∎
-- Befejezni otthon.

-- Machine
-- Szeretnék egy olyan gépet, van rajta 3 gomb,
-- 1. gomb, növeli a tárolt értéket 1-gyel
-- 2. gomb, visszaállítja a belső értéket 0-ra
-- 3. gomb, "kiírja a kijelzőre" a jelenlegi számot.
-- genMachine : Tm (Γ ▹ C) C → Tm (Γ ▹ C) C → Tm (Γ ▹ C) Nat → Tm Γ C → Tm Γ Machine

addOne : Tm ◇ Machine
addOne = genMachine (suco v1) (num 0) v0 (num 0)