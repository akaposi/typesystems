{-# OPTIONS --prop --rewriting #-}

module gy05-4 where

open import Lib

module type-inference where

  import  RazorAST
  open import RazorWT
  open RazorWT.I

  -- tipuskikovetkeztetes primitiv hibauzenetekkel

  data Result (A : Set) : Set where
    Ok : (r : A) → Result A
    Err : (e : ℕ) → Result A

  {-
    Error codes:
    · 1: Non boolean condition
    · 2: Differing branch types
    · 3: Non numeric isZero parameter
    · 4: Non numeric addition parameter
  -}

  Infer : RazorAST.Model {lzero}
  Infer = record
    { Tm      = Result (Σ Ty (λ A → Tm A)) -- WT-beli Ty, WT-beli Tm
    ; true    = {!   !}
    ; false   = {!   !}
    ; ite     = {!   !}
    ; num     = {!   !}
    ; isZero  = {!   !}
    ; _+o_    = {!   !}
    }

  module INF = RazorAST.Model Infer

  inf-test : Result (Σ Ty (λ T → Tm T))
  inf-test = {! INF.⟦ I'.isZero I'.false ⟧  !} -- itt lehet tesztelni
    where open RazorAST.I

  -- Standard model

  Standard : Model {lsuc lzero} {lzero}
  Standard = record
    { Ty      = {!   !}
    ; Tm      = {!   !}
    ; Nat     = {!   !}
    ; Bool    = {!   !}
    ; true    = {!   !}
    ; false   = {!   !}
    ; ite     = {!   !}
    ; num     = {!   !}
    ; isZero  = {!   !}
    ; _+o_    = {!   !}
    }
  module STD = Model Standard

  eval : {A : Ty} → Tm A → {!   !}
  eval = {!   !}

  typeOfINF : Result (Σ Ty (λ A → Tm A)) → Set
  typeOfINF r = {!   !}

  run : (t : RazorAST.I.Tm) → {!   !}
  run t = {!   !}
  
module NatBool-with-equational-theory where

  open import RazorBool
  open I

  eq-0 : true ≡ true
  eq-0 = {!   !}

  eq-1 : isZero (num 0) ≡ true
  eq-1 = {!   !}

  eq-2 : isZero (num 3 +o num 1) ≡ isZero (num 4)
  eq-2 = {!   !}

  eq-3 : isZero (num 4) ≡ false
  eq-3 = {!   !}

  eq-4 : isZero (num 3 +o num 1) ≡ false
  eq-4 = {!   !}

  eq-4' : isZero (num 3 +o num 1) ≡ false
  eq-4' =
    isZero (num 3 +o num 1)
      ≡⟨ {!   !} ⟩ -- \== \< \>
    {!   !}
      ≡⟨ {!   !} ⟩
    false
      ∎ -- \qed

  eq-5 : ite false (num 2) (num 5) ≡ num 5
  eq-5 = {!   !}

  eq-6 : ite true (isZero (num 0)) false ≡ true
  eq-6 = {!   !}

  eq-6' : ite true (isZero (num 0)) false ≡ true
  eq-6' =
    ite true (isZero (num 0)) false
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    true
      ∎

  eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
  eq-7 =
    ((num 3 +o num 0) +o num 1)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 4
      ∎

  eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
  eq-8 =
    ite (isZero (num 0)) (num 1 +o num 1) (num 0)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 2
      ∎

  eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  eq-9 =
    (num 3 +o ite (isZero (num 2)) (num 1) (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 3
      ∎

  eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  eq-10 =
    (ite false (num 1 +o num 1) (num 0) +o num 0)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 0
      ∎

  eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  eq-11 =
    ite (isZero (num 0 +o num 1)) false (isZero (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    true
      ∎

  eq-12 : num 3 +o num 2 ≡ ite true (num 5) (num 1)
  eq-12 =
    num 3 +o num 2
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    ite true (num 5) (num 1)
      ∎  