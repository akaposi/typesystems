{-# OPTIONS --prop --rewriting #-}

module hf12-1 where

open import Lib
open import Ind.Syntax
open import Ind.Model
open import Ind
open Standard using (St; _∷_; []) renaming (List to StList ; Tree to StTree)

eval : {A : Ty} → Tm ◇ A → St.⟦ A ⟧T
eval t = St.⟦ t ⟧t (mk triv)

-- Szintaktikus cukorka num-ra:
num : ∀{Γ} → ℕ → Tm Γ Nat
num zero    = zeroo
num (suc n) = suco (num n)

-- FELADAT: Bizonyítsd be, hogy a num[] szabály teljesül!
num[] : ∀{Γ Δ n}{σ : Sub Δ Γ} → num n [ σ ] ≡ num n
num[] = {!   !}

-- GYAKORLATRÓL FELADAT: isZeroβ₂
isZero : {Γ : Con} → Tm Γ (Nat ⇒ Bool)
isZero = lam (iteNat true false v0)

isZeroβ₂ : ∀{n} → isZero $ num (1 + n) ≡ false {◇}
isZeroβ₂ {n} =
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !} ∎

-- GYAKORLATRÓL FELADAT: plusβ
plus : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
plus = lam (lam (iteNat v0 (suco v0) v1))

-- Segítség: A bizonyításban a num-ban lévő tetszőleges szám miatt kell mintailleszteni.
plusβ : ∀{Γ n m} → plus $ num n $ num m ≡ num {Γ} (n + m)
plusβ {n = n} {m} = {!   !}
  
idl+o : ∀{Γ n} → plus $ num 0 $ num n ≡ num {Γ} n
idl+o {n = n} = {!   !}

idr+o : ∀{Γ n} → plus $ num n $ num 0 ≡ num {Γ} n
idr+o {n = n} = {!   !}

-- GYAKORLATRÓL FELADAT: times, Definiáld azt a függvényt, amely két számot összeszoroz.
times : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
times = {!   !}

times-test-1 : eval times 1 5 ≡ 5
times-test-1 = refl

times-test-2 : eval times 2 3 ≡ 6
times-test-2 = refl

times-test-3 : eval times zero 4 ≡ zero
times-test-3 = refl

times-test-4 : eval times 12 43 ≡ 516
times-test-4 = refl

-- FELADAT: Definiáld a hatványozás függvényét.
pow : ∀{Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
pow = {!   !}

pow-test-1 : eval pow 2 5 ≡ 32
pow-test-1 = refl

pow-test-2 : eval pow 3 3 ≡ 27
pow-test-2 = refl

pow-test-3 : eval pow 0 0 ≡ 1
pow-test-3 = refl

pow-test-4 : eval pow 0 5 ≡ 0
pow-test-4 = refl

pow-test-5 : eval pow 7 4 ≡ 2401
pow-test-5 = refl

-- FELADAT: Definiáld azt a függvényt az objektumnyelvben, amely
--          egy listányi számot összead.
sumList : ∀{Γ} → Tm Γ (List Nat ⇒ Nat)
sumList = {!   !}

sumList-test-1 : let l = eval sumList (1 ∷ 4 ∷ 3 ∷ []) in l ≡ 8
sumList-test-1 = refl

sumList-test-2 : let l = eval sumList (5 ∷ 3 ∷ 10 ∷ 9 ∷ 0 ∷ []) in l ≡ 27
sumList-test-2 = refl

sumList-test-3 : let l = eval sumList [] in l ≡ 0
sumList-test-3 = refl

sumList-test-4 : let l = eval sumList (0 ∷ []) in l ≡ 0
sumList-test-4 = refl

sumList-test-5 : let l = eval sumList (10 ∷ 90 ∷ 11 ∷ 23 ∷ 45 ∷ 120 ∷ 87 ∷ []) in l ≡ 386
sumList-test-5 = refl

-- FELADAT: Bizonyítsd be az alábbi állítást a szintaxisban!
sumList-proof : ∀{Γ} → sumList $ (cons (num 2) (cons (num 3) (cons (num 1) nil))) ≡ num {Γ} 6
sumList-proof =
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !} ∎

-- NEHÉZ FELADAT: Definiáld a listák másik iterátorát, amely bal oldalról hajtja végre a műveletet, nem jobbról, mint az iteList.
iteListL : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ B ▹ A) B → Tm Γ (List A) → Tm Γ B
iteListL = {!   !}

foldl-test-1 : eval (iteListL (num 2) (pow $ v1 $ v0) (cons (num 3) (cons (num 4) nil))) ≡ 4096
foldl-test-1 = refl

-- Bizonyítsd az iteListL elimináló szabályait.
ListLβ₁ : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A) B} → iteListL u v nil ≡ u
ListLβ₁ = {!   !}

ListLβ₂ : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
               iteListL u v (cons t₁ t) ≡ iteListL (v [ ⟨ u ⟩ ⁺ ] [ ⟨ t₁ ⟩ ]) v t
ListLβ₂ = {!   !}

iteListL[]  : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A) B}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
               iteListL u v t [ γ ] ≡ iteListL (u [ γ ]) (v [ γ ⁺ ⁺  ]) (t [ γ ])
iteListL[] = ?

-- GYAKORLATRÓL FELADAT: Definiáld azt a függvényt, amely két listát összefűz.
++ : {Γ : Con}{A : Ty} → Tm Γ (List A ⇒ List A ⇒ List A)
++ = {!   !}

++-test-1 : eval (++ $ nil {A = Bool} $ nil {A = Bool}) ≡ []
++-test-1 = refl

++-test-2 : eval (++ $ nil $ (cons trivial nil)) ≡ mk triv ∷ []
++-test-2 = refl

++-test-3 : eval (++ $ (cons zeroo nil) $ (cons (suco zeroo) nil)) ≡
                zero ∷ 1 ∷ []
++-test-3 = refl

-- FELADAT: Definiáld azt a függvényt, amely listák listáját összefűzi
concat : ∀{Γ A} → Tm Γ (List (List A) ⇒ List A)
concat = {!   !}

concat-test-1 : eval (concat {A = Nat}) ((1 ∷ 2 ∷ []) ∷ (3 ∷ []) ∷ [] ∷ (4 ∷ 5 ∷ 6 ∷ []) ∷ []) ≡ 1 ∷ 2 ∷ 3 ∷ 4 ∷ 5 ∷ 6 ∷ []
concat-test-1 = refl

concat-test-2 : eval (concat {A = Nat}) ((2 ∷ []) ∷ (30 ∷ []) ∷ (1 ∷ 20 ∷ []) ∷ []) ≡ 2 ∷ 30 ∷ 1 ∷ 20 ∷ []
concat-test-2 = refl

-- FELADAT: Definiáld azt a függvényt, amely egy fát a preorder bejárásnak megfelelően alakít át listává.
preorder : ∀{Γ A} → Tm Γ (Tree A ⇒ List A)
preorder = {!   !}