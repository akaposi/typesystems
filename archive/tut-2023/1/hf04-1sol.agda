{-# OPTIONS --prop --rewriting #-}

module hf04-1sol where

open import Lib
open import RazorAST

-- Bizonyítsd be, hogy a szintaxisban true +o num 1 ≠ num 1 +o true
M1 : Model {lzero}
M1 = record
  { Tm = 𝟚
  ; true = tt
  ; false = ff
  ; ite = λ _ _ _ → ff
  ; num = λ _ → ff
  ; isZero = λ _ → ff
  ; _+o_ = λ x _ → x
  }

module M1 = Model M1

-- Írd fel az állítás helyes típusát, majd a modellt
-- használva bizonyítsd az állítást.
-- Szokásos módon a tt≠ff függvényt szükséges használni.
m1 : I.true I.+o I.num 1 ≢ I.num 1 I.+o I.true
m1 e = tt≠ff (cong M1.⟦_⟧ e)

-----------------------------------------
-- Bizonyítsd be, hogy a szintaxisban az ite mindhárom paraméterében injektív.
-- Ehhez egy olyan függőmodellt szükséges definiálni, amelyben nem
-- csak egy darab I.Tm-et adunk vissza, hanem hármat.
-- Rendezett párokat a sima × (\times = \x = ×) típussal lehet készíteni.
-- Konstruktora ugyanúgy a _,_.
-- A destruktorai π₁ és π₂, amelyek a rendre a rendezett pár első, illetve második komponensét adják vissza.
-- Az × egy jobbra kötő művelet, tehát A × B × C = A × (B × C)
-- A _,_ épp ezért szintén jobbra köt.

IteInj : DepModel {lzero}
IteInj = record
  { Tm∙ = λ _ → I.Tm × I.Tm × I.Tm
  ; true∙ = I.true , I.true , I.true
  ; false∙ = I.true , I.true , I.true
  ; ite∙ = λ where {t} _ {u} _ {v} _ → t , u , v
  ; num∙ = λ _ → I.true , I.true , I.true
  ; isZero∙ = λ _ → I.true , I.true , I.true
  ; _+o∙_ = λ _ _ → I.true , I.true , I.true
  }

module IteInj = DepModel IteInj

-- Írd fel az állítás helyes típusát úgy, hogy egyszerre lehessen bizonyítani
-- mindhárom szükséges egyenlőséget.
iteInj : ∀{t u v t' u' v'} → I.ite t u v ≡ I.ite t' u' v' → Lift (t ≡ t') × Lift (u ≡ u') × Lift (v ≡ v')
iteInj e = mk (cong (λ x → π₁ IteInj.⟦ x ⟧) e) , mk (cong (λ x → π₁ (π₂ IteInj.⟦ x ⟧)) e) , mk (cong (λ x → π₂ (π₂ IteInj.⟦ x ⟧)) e)

-----------------------------------------
-- Adott az alábbi modell:

M2 : Model {lzero}
M2 = record
  { Tm = ℕ
  ; true = 1
  ; false = 1
  ; ite = λ x y z → 1 + x + y + z
  ; num = λ _ → 1
  ; isZero = suc
  ; _+o_ = λ x y → 1 + x + y
  }

module M2 = Model M2

-- Bizonyítsuk be, hogy mindig LÉTEZIK olyan természetes szám, amelyhez
-- 1-et hozzáadva a term kiértékelését kapjuk.
-- Létezést a Σsp típussal lehet felírni, ez egy függő rendezett pár.
-- Első paramétere mindig a típus, hogy mi létezik.
-- A második paramétere pedig egy függvény, amely eredménye egy Prop-ba képző állítás kell legyen.
-- (Ebben a feladatban a Prop-ba képző állítás az egyenlőség lesz.)

{-
M2D : DepModel {lzero}
M2D = record
  { Tm∙ = λ t → Σsp ℕ (λ m → M2.⟦ t ⟧ ≡ suc m)
  ; true∙ = zero , refl
  ; false∙ = zero , refl
  ; ite∙ = λ where {t} (n , ih₁) {u} (k , ih₂) {v} (i , ih₃) → suc n + suc k + suc i , 
                    (cong suc (cong₃ (λ x y z → x + y + z) ih₁ ih₂ ih₃))
  ; num∙ = λ n → zero , refl
  ; isZero∙ = λ ih → suc (π₁ ih) , cong suc (π₂ ih)
  ; _+o∙_ = λ where {u} (n , ih₁) {v} (k , ih₂) → suc n + suc k , cong suc (cong₂ _+_ ih₁ ih₂)
  }
-}

M2D : DepModel {lzero}
DepModel.Tm∙ M2D t = Σsp ℕ λ x → M2.⟦ t ⟧ ≡ x + 1 
DepModel.true∙ M2D = 0 , refl
DepModel.false∙ M2D = 0 , refl
DepModel.ite∙ M2D {t} a {u} b {v} c = {!   !} , {!   !}
DepModel.num∙ M2D _ = 0 , refl
DepModel.isZero∙ M2D {t} y = {!   !}
DepModel._+o∙_ M2D = {!   !}
-----------------------------------------
-- Keresős feladat:

-- Ezúttal ugyanúgy a korábbi modellről bizonyítsd, hogy soha nem fog 0-t
-- eredményül adni.

-- A függvény, amelyet érdemes lesz használni, az a Lib-ben is megtalálható
-- zero≠suc vagy suc≠zero függvény attól függően, hogy hol mi jön ki, mit kell bizonyítani.

M2D' : DepModel {lzero}
M2D' = record
  { Tm∙ = λ t → Lift (M2.⟦ t ⟧ ≢ 0)
  ; true∙ = mk suc≠zero
  ; false∙ = mk suc≠zero
  ; ite∙ = λ _ _ _ → mk suc≠zero
  ; num∙ = λ n → mk suc≠zero
  ; isZero∙ = λ _ → mk suc≠zero
  ; _+o∙_ = λ _ _ → mk suc≠zero
  }

-----------------------------------------
-- Definiálj egy modellt, amely segítségével egy szintaktikus kifejezésből
-- minden I.isZero-t kivágunk a fából, minden mást meghagyunk.

M3 : DepModel {lzero}
M3 = record
  { Tm∙ = λ _ → I.Tm
  ; true∙ = I.true
  ; false∙ = I.false
  ; ite∙ = λ x y z → I.ite x y z
  ; num∙ = I.num
  ; isZero∙ = λ t → t
  ; _+o∙_ = λ x y → x I.+o y
  }

module M3 = DepModel M3

M3-test1 : M3.⟦ I.isZero I.false ⟧ ≡ I.false
M3-test1 = refl
M3-test2 : M3.⟦ I.true ⟧ ≡ I.true
M3-test2 = refl
M3-test3 : M3.⟦ I.ite (I.isZero I.true) (I.num 0 I.+o I.num 1) (I.num 1 I.+o I.isZero (I.num 1)) ⟧
         ≡ I.ite I.true (I.num 0 I.+o I.num 1) (I.num 1 I.+o I.num 1)
M3-test3 = refl
M3-test4 : M3.⟦ I.ite (I.num 10 I.+o I.num 2) (I.ite (I.num 3) I.true I.false) (I.false I.+o I.num 5) ⟧
         ≡ I.ite (I.num 10 I.+o I.num 2) (I.ite (I.num 3) I.true I.false) (I.false I.+o I.num 5)
M3-test4 = refl
M3-test5 : M3.⟦ I.isZero (I.isZero (I.num 2)) ⟧ ≡ I.num 2
M3-test5 = refl
M3-test6 : M3.⟦ I.ite (I.isZero (I.num 0)) (I.num 1) (I.num 2) I.+o (I.isZero (I.isZero (I.num 3) I.+o I.isZero (I.num 6))) ⟧ ≡ I.ite (I.num 0) (I.num 1) (I.num 2) I.+o (I.num 3 I.+o I.num 6)
M3-test6 = refl

-----------------------------------------
-- Adott az alábbi modell:

M4 : Model {lzero}
M4 = record
  { Tm = ℕ
  ; true = 2
  ; false = 0
  ; ite = λ x y z → z
  ; num = λ n → n + n
  ; isZero = λ x → x
  ; _+o_ = λ x y → 2 + x + y
  }

module M4 = Model M4

-- Bizonyítsd be, hogy minden termhez létezik egy KONKRÉT szám, amelyet a modell nem képes visszaadni.
-- (Tehát nem kell Σsp-t használni, hanem csak ≢ valami szám.)
-- A metaelméletben tudjuk, hogy suc injektív, ezt a sucinj függvény bizonyítja.

M4D : DepModel {lzero}
M4D = record
  { Tm∙ = λ t → Lift (M4.⟦ t ⟧ ≢ 1)
  ; true∙ = mk λ e → suc≠zero (sucinj e)
  ; false∙ = mk zero≠suc
  ; ite∙ = λ _ _ x → x
  ; num∙ = λ {zero → mk zero≠suc
            ; (suc zero) → mk (λ e → suc≠zero (sucinj e))
            ; (suc (suc n)) → mk (λ e → suc≠zero (sucinj e))}
  ; isZero∙ = λ x → x
  ; _+o∙_ = λ _ _ → mk λ e → suc≠zero (sucinj e)
  }

-----------------------------------------
-- Adott az alábbi modell:

M5 : Model {lzero}
M5 = record
  { Tm = ℕ
  ; true = 1
  ; false = 0
  ; ite = λ x y z → suc z
  ; num = λ n → n + n
  ; isZero = 4 +_
  ; _+o_ = λ x y → 2 + x + y
  }

module M5 = Model M5

-- Bizonyítsd be, hogy minden termhez létezik egy szám, amelyet a modell nem képes visszaadni.
-- A számnak nem feltétlenül kell minden esetben azonosnak lenniük.
-- Ehhez érdemes Σsp-t használni.
{-
M5D : DepModel {lzero}
M5D = record
  { Tm∙ = λ t → Σsp ℕ (λ n → M5.⟦ t ⟧ ≢ n)
  ; true∙ = 0 , suc≠zero
  ; false∙ = 1 , zero≠suc
  ; ite∙ = λ _ _ _ → 0 , suc≠zero
  ; num∙ = λ {zero → 1 , zero≠suc
            ; (suc n) → 0 , suc≠zero}
  ; isZero∙ = λ _ → 0 , suc≠zero
  ; _+o∙_ = λ _ _ → 0 , suc≠zero
  }
-}

M5D : DepModel {lzero}
M5D = record
  { Tm∙ = λ x → Σsp ℕ λ n → ¬ ( M5.⟦ x ⟧ ≡ n )
  ; true∙ = ℕ.zero , suc≠zero
  ; false∙ = 1 , λ x → zero≠suc x
  ; ite∙ = λ {a} x {b} x₁ {c} x₂ → {!   !} , {!   !}
  ; num∙ = λ n → n + n + 1 , λ x → {!   !}
  ; isZero∙ = λ x → {!   !}
  ; _+o∙_ = λ x x₁ → {!   !}
  }
-----------------------------------------
-- Adott az alábbi modell:

M6 : DepModel {lzero}
M6 = record
  { Tm∙ = λ _ → ℕ
  ; true∙ = 10
  ; false∙ = 5
  ; ite∙ = λ _ _ _ → 0
  ; num∙ = λ n → 5 * n
  ; isZero∙ = λ x → 5 + x
  ; _+o∙_ = λ x y → x
  }

module M6 = DepModel M6

-- Bizonyítsd be, hogy minden term kiértékelése M6-ban 5-tel osztható számot eredményez.

M6D : DepModel {lzero}
M6D = record
  { Tm∙ = λ t → Σsp ℕ (λ n → M6.⟦ t ⟧ ≡ 5 * n)
  ; true∙ = 2 , refl
  ; false∙ = 1 , refl
  ; ite∙ = λ _ _ _ → 0 , refl
  ; num∙ = λ n → n , refl
  ; isZero∙ = λ {(n , p) → suc n , (cong (5 +_) p ◾ 
                                   +suc {4 + n} ⁻¹ ◾ +suc {3 + n} ⁻¹ ◾ +suc {2 + n} ⁻¹ ◾ +suc {1 + n} ⁻¹ ◾
                                   (cong (suc n +_) (+suc {3 + n} ⁻¹ ◾ +suc {2 + n} ⁻¹ ◾ +suc {1 + n} ⁻¹)) ◾
                                   (cong (λ x → suc n + (suc n + x)) (+suc {2 + n} ⁻¹ ◾ +suc {1 + n} ⁻¹)) ◾
                                   (cong (λ x → suc n + (suc n + (suc n + x))) (+suc {1 + n} ⁻¹)))}
  ; _+o∙_ = λ x _ → x
  }

-----------------------------------------
-- Adott az alábbi két modell:

M7₁ : Model {lzero}
M7₁ = record
  { Tm = 𝟚
  ; true = tt
  ; false = ff
  ; ite = if_then_else_
  ; num = λ {zero → ff
           ; (suc n) → tt}
  ; isZero = λ x → not (not (not x))
  ; _+o_ = λ x y → not x ∧ not y
  }

module M7₁ = Model M7₁

M7₂ : Model {lzero}
M7₂ = record
  { Tm = 𝟚
  ; true = tt
  ; false = ff
  ; ite = if_then_else_
  ; num = λ {zero → ff
           ; (suc n) → tt}
  ; isZero = not
  ; _+o_ = λ x y → not (x ∨ y)
  }

module M7₂ = Model M7₂

-- Bizonyítsd be, hogy a két modell valójában ugyanazt csinálja.
M7D : DepModel {lzero}
M7D = record
  { Tm∙ = λ t → Lift (M7₁.⟦ t ⟧ ≡ M7₂.⟦ t ⟧)
  ; true∙ = mk refl
  ; false∙ = mk refl
  ; ite∙ = λ where {t} (mk ih₁) {u} (mk ih₂) {v} (mk ih₃) → mk (cong₃ if_then_else_ ih₁ ih₂ ih₃)
  ; num∙ = λ {zero → mk refl
            ; (suc n) → mk refl}
  ; isZero∙ = λ where {t} (mk ih₁) → mk (cong (λ x → if if if x then ff else tt then ff else tt then ff else tt) ih₁ ◾ f M7₂.⟦ t ⟧)
  ; _+o∙_ = λ where {u} (mk ih₁) {v} (mk ih₂) → mk (cong₂ (λ x y → if if x then ff else tt then if y then ff else tt else ff) ih₁ ih₂ ◾ g M7₂.⟦ u ⟧ M7₂.⟦ v ⟧)
  } where
  f : (b : 𝟚) → (if if if b then ff else tt then ff else tt then ff else tt)
    ≡ (if b then ff else tt)
  f ff = refl
  f tt = refl

  g : (b c : 𝟚) → (if if b then ff else tt then if c then ff else tt else ff) ≡ (if if b then tt else c then ff else tt)
  g ff c = refl
  g tt c = refl

------------------------------------------
-- NAGYON HOSSZÚ FELADAT! De érdekes koncepciót mutat be. Legalább elkezdeni érdemes.
-- Előfordulhat olyan "sima" interpretáció is, amire az egyszerű modell kevés, ez egy ilyen szeretne lenni.
-- Ebben a feladatban szeretnénk minél pontosabb interpretációt adni.
-- Megközelítőleg a WT standard modelljét emulálja; a típusellenőrzés itt szemantikában van benne.
-- És borzasztó hosszú megírni szemantikában.

-- Definiáld azt a modellt, amely megpróbál kiértékelni kifejezéseket, ha azok típushelyesek.
-- Nem kell hozzá WT (de órán majd kiderül, hogy WT-ben sokkal egyszerűbb és rövidebb).
-- true és false egyértelműen 𝟚 típusúak.
-- num-ok egyértelműen ℕ típusúak.
-- ite lehet 𝟚 vagy ℕ, attól függ, hogy a paraméterekben mi van.
--   A rövidség kedvéért lehet különböző típusú érték a két ágon, illetve hibás érték lehet egy ágon, ha a másikkal kell kezdenie valamit.
-- isZero: a paramétere ℕ, az eredménye 𝟚, ellenőrzi, hogy a szám 0-e, ekkor tt az eredmény, máskor ff, de lehet, hogy korábban már hibás eredményünk volt, ekkor hibánk van továbbra is.
-- +o: A két paramétere ℕ, az eredménye szintén ℕ, összeadja a számokat, szintén lehetett hiba korábban.

M8 : DepModel {lzero}
M8 = record
  { Tm∙ = λ {I.true → 𝟚
           ; I.false → 𝟚
           ; (I.ite _ _ _) → Maybe (𝟚 ⊎ ℕ)
           ; (I.num _) → ℕ
           ; (I.isZero _) → Maybe 𝟚
           ; (_ I.+o _) → Maybe ℕ}
  ; true∙ = tt
  ; false∙ = ff
  ; ite∙ = λ where {I.true} ih₁ {I.true}        ih₂ {v} ih₃      → just (ι₁ ih₂)
                   {I.true} ih₁ {I.false}       ih₂ {v} ih₃      → just (ι₁ ih₂)
                   {I.true} ih₁ {I.ite u u₁ u₂} ih₂ {v} ih₃      → ih₂
                   {I.true} ih₁ {I.num x}       ih₂ {v} ih₃      → just (ι₂ ih₂)
                   {I.true} ih₁ {I.isZero u}    nothing {v} ih₃  → nothing
                   {I.true} ih₁ {I.isZero u}    (just x) {v} ih₃ → just (ι₁ x)
                   {I.true} ih₁ {u I.+o u₁}     nothing {v} ih₃  → nothing
                   {I.true} ih₁ {u I.+o u₁}     (just x) {v} ih₃ → just (ι₂ x)
                   {I.false} ih₁ {u} ih₂ {I.true} ih₃            → just (ι₁ ih₃)
                   {I.false} ih₁ {u} ih₂ {I.false} ih₃           → just (ι₁ ih₃)
                   {I.false} ih₁ {u} ih₂ {I.ite v v₁ v₂} ih₃     → ih₃
                   {I.false} ih₁ {u} ih₂ {I.num x} ih₃           → just (ι₂ ih₃)
                   {I.false} ih₁ {u} ih₂ {I.isZero v} nothing    → nothing
                   {I.false} ih₁ {u} ih₂ {I.isZero v} (just x)   → just (ι₁ x)
                   {I.false} ih₁ {u} ih₂ {v I.+o v₁} nothing     → nothing
                   {I.false} ih₁ {u} ih₂ {v I.+o v₁} (just x)    → just (ι₂ x)
                   {I.ite t t₁ t₂} nothing {u} ih₂ {v} ih₃ → nothing
                   {I.ite t t₁ t₂} (just (ι₁ ff)) {u} ih₂ {I.true} ih₃ → just (ι₁ ih₃)
                   {I.ite t t₁ t₂} (just (ι₁ ff)) {u} ih₂ {I.false} ih₃ → just (ι₁ ih₃)
                   {I.ite t t₁ t₂} (just (ι₁ ff)) {u} ih₂ {I.ite v v₁ v₂} ih₃ → ih₃
                   {I.ite t t₁ t₂} (just (ι₁ ff)) {u} ih₂ {I.num x} ih₃ → just (ι₂ ih₃)
                   {I.ite t t₁ t₂} (just (ι₁ ff)) {u} ih₂ {I.isZero v} nothing → nothing
                   {I.ite t t₁ t₂} (just (ι₁ ff)) {u} ih₂ {I.isZero v} (just x) → just (ι₁ x)
                   {I.ite t t₁ t₂} (just (ι₁ ff)) {u} ih₂ {v I.+o v₁} nothing → nothing
                   {I.ite t t₁ t₂} (just (ι₁ ff)) {u} ih₂ {v I.+o v₁} (just x) → just (ι₂ x)
                   {I.ite t t₁ t₂} (just (ι₁ tt)) {I.true} ih₂ {v} ih₃ → just (ι₁ ih₂)
                   {I.ite t t₁ t₂} (just (ι₁ tt)) {I.false} ih₂ {v} ih₃ → just (ι₁ ih₂)
                   {I.ite t t₁ t₂} (just (ι₁ tt)) {I.ite u u₁ u₂} ih₂ {v} ih₃ → ih₂
                   {I.ite t t₁ t₂} (just (ι₁ tt)) {I.num x} ih₂ {v} ih₃ → just (ι₂ ih₂)
                   {I.ite t t₁ t₂} (just (ι₁ tt)) {I.isZero u} nothing {v} ih₃ → nothing
                   {I.ite t t₁ t₂} (just (ι₁ tt)) {I.isZero u} (just x) {v} ih₃ → just (ι₁ x)
                   {I.ite t t₁ t₂} (just (ι₁ tt)) {u I.+o u₁} nothing {v} ih₃ → nothing
                   {I.ite t t₁ t₂} (just (ι₁ tt)) {u I.+o u₁} (just x) {v} ih₃ → just (ι₂ x)
                   {I.ite t t₁ t₂} (just (ι₂ x)) {u} ih₂ {v} ih₃ → nothing
                   {I.num x} ih₁ {u} ih₂ {v} ih₃ → nothing
                   {I.isZero t} nothing {u} ih₂ {v} ih₃ → nothing
                   {I.isZero t} (just ff) {u} ih₂ {I.true} ih₃ → just (ι₁ ih₃)
                   {I.isZero t} (just ff) {u} ih₂ {I.false} ih₃ → just (ι₁ ih₃)
                   {I.isZero t} (just ff) {u} ih₂ {I.ite v v₁ v₂} ih₃ → ih₃
                   {I.isZero t} (just ff) {u} ih₂ {I.num x} ih₃ → just (ι₂ ih₃)
                   {I.isZero t} (just ff) {u} ih₂ {I.isZero v} nothing → nothing
                   {I.isZero t} (just ff) {u} ih₂ {I.isZero v} (just x) → just (ι₁ x)
                   {I.isZero t} (just ff) {u} ih₂ {v I.+o v₁} nothing → nothing
                   {I.isZero t} (just ff) {u} ih₂ {v I.+o v₁} (just x) →  just (ι₂ x)
                   {I.isZero t} (just tt) {I.true} ih₂ {v} ih₃ → just (ι₁ ih₂)
                   {I.isZero t} (just tt) {I.false} ih₂ {v} ih₃ → just (ι₁ ih₂)
                   {I.isZero t} (just tt) {I.ite u u₁ u₂} ih₂ {v} ih₃ → ih₂
                   {I.isZero t} (just tt) {I.num x} ih₂ {v} ih₃ → just (ι₂ ih₂)
                   {I.isZero t} (just tt) {I.isZero u} nothing {v} ih₃ → nothing
                   {I.isZero t} (just tt) {I.isZero u} (just x) {v} ih₃ → just (ι₁ x)
                   {I.isZero t} (just tt) {u I.+o u₁} nothing {v} ih₃ → nothing
                   {I.isZero t} (just tt) {u I.+o u₁} (just x) {v} ih₃ → just (ι₂ x)
                   {t I.+o t₁} ih₁ {u} ih₂ {v} ih₃ → nothing
  ; num∙ = λ n → n
  ; isZero∙ = λ where {I.true} ih → nothing
                      {I.false} ih → nothing
                      {I.ite t t₁ t₂} nothing → nothing
                      {I.ite t t₁ t₂} (just (ι₁ x)) → nothing
                      {I.ite t t₁ t₂} (just (ι₂ zero)) → just tt
                      {I.ite t t₁ t₂} (just (ι₂ (suc x))) → just ff
                      {I.num x} zero → just tt
                      {I.num x} (suc ih) → just ff
                      {I.isZero t} ih → nothing
                      {t I.+o t₁} nothing → nothing
                      {t I.+o t₁} (just zero) → just tt
                      {t I.+o t₁} (just (suc x)) → just ff
  ; _+o∙_ = λ where {I.true} ih₁ {v} ih₂ → nothing
                    {I.false} ih₁ {v} ih₂ → nothing
                    {I.ite u u₁ u₂} nothing {v} ih₂ → nothing
                    {I.ite u u₁ u₂} (just (ι₁ x)) {v} ih₂ → nothing
                    {I.ite u u₁ u₂} (just (ι₂ x)) {I.true} ih₂ → nothing
                    {I.ite u u₁ u₂} (just (ι₂ x)) {I.false} ih₂ → nothing
                    {I.ite u u₁ u₂} (just (ι₂ x)) {I.ite v v₁ v₂} nothing → nothing
                    {I.ite u u₁ u₂} (just (ι₂ x)) {I.ite v v₁ v₂} (just (ι₁ y)) → nothing
                    {I.ite u u₁ u₂} (just (ι₂ x)) {I.ite v v₁ v₂} (just (ι₂ y)) → just (x + y)
                    {I.ite u u₁ u₂} (just (ι₂ x)) {I.num x₁} ih₂ → just (x + ih₂)
                    {I.ite u u₁ u₂} (just (ι₂ x)) {I.isZero v} ih₂ → nothing
                    {I.ite u u₁ u₂} (just (ι₂ x)) {v I.+o v₁} nothing → nothing
                    {I.ite u u₁ u₂} (just (ι₂ x)) {v I.+o v₁} (just y) → just (x + y)
                    {I.num x} ih₁ {I.true} ih₂ → nothing
                    {I.num x} ih₁ {I.false} ih₂ → nothing
                    {I.num x} ih₁ {I.ite v v₁ v₂} nothing → nothing
                    {I.num x} ih₁ {I.ite v v₁ v₂} (just (ι₁ y)) → nothing
                    {I.num x} ih₁ {I.ite v v₁ v₂} (just (ι₂ y)) → just (x + y)
                    {I.num x} ih₁ {I.num x₁} ih₂ → just (ih₁ + ih₂)
                    {I.num x} ih₁ {I.isZero v} ih₂ → nothing
                    {I.num x} ih₁ {v I.+o v₁} nothing → nothing
                    {I.num x} ih₁ {v I.+o v₁} (just y) → just (ih₁ + y)
                    {I.isZero u} ih₁ {v} ih₂ → nothing
                    {u I.+o u₁} nothing {v} ih₂ → nothing
                    {u I.+o u₁} (just x) {I.true} ih₂ → nothing
                    {u I.+o u₁} (just x) {I.false} ih₂ → nothing
                    {u I.+o u₁} (just x) {I.ite v v₁ v₂} nothing → nothing
                    {u I.+o u₁} (just x) {I.ite v v₁ v₂} (just (ι₁ y)) → nothing
                    {u I.+o u₁} (just x) {I.ite v v₁ v₂} (just (ι₂ y)) → just (x + y)
                    {u I.+o u₁} (just x) {I.num x₁} ih₂ → just (x + ih₂)
                    {u I.+o u₁} (just x) {I.isZero v} ih₂ → nothing
                    {u I.+o u₁} (just x) {v I.+o v₁} nothing → nothing
                    {u I.+o u₁} (just x) {v I.+o v₁} (just y) → just (x + y)
  }

module M8 = DepModel M8

M8-test1 : M8.⟦ I.true ⟧ ≡ tt
M8-test1 = refl
M8-test2 : M8.⟦ I.num 2 ⟧ ≡ 2
M8-test2 = refl
M8-test3 : M8.⟦ I.num 2 I.+o I.true ⟧ ≡ nothing
M8-test3 = refl
M8-test4 : M8.⟦ I.ite I.true (I.num 0) (I.num 1) ⟧ ≡ just (ι₂ 0)
M8-test4 = refl
M8-test5 : M8.⟦ I.ite (I.isZero (I.num 1)) (I.num 0 I.+o I.num 2) I.false ⟧ ≡ just (ι₁ ff)
M8-test5 = refl
M8-test6 : M8.⟦ I.isZero (I.isZero (I.num 0)) ⟧ ≡ nothing
M8-test6 = refl
M8-test7 : M8.⟦ I.isZero (I.num 0) ⟧ ≡ just tt
M8-test7 = refl
M8-test8 : M8.⟦ (I.num 4 I.+o I.num 5) I.+o (I.num 2 I.+o I.num 3) ⟧ ≡ just 14
M8-test8 = refl
M8-test9 : M8.⟦ (I.ite (I.isZero I.true) (I.num 3 I.+o I.true) I.true) I.+o (I.num 2 I.+o I.false) ⟧ ≡ nothing
M8-test9 = refl
M8-test10 : M8.⟦ (I.ite (I.isZero (I.num 1)) (I.num 3 I.+o I.true) (I.num 1)) I.+o (I.num 2 I.+o I.num 3) ⟧ ≡ just 6
M8-test10 = refl
M8-test11 : M8.⟦ I.ite (I.ite (I.isZero (I.num 2 I.+o I.num 0)) (I.num 0) I.true) (I.num 2) (I.true I.+o I.false) ⟧ ≡ just (ι₂ 2)
M8-test11 = refl
M8-test12 : M8.⟦ I.num 0 I.+o (I.num 0 I.+o I.isZero (I.num 0)) ⟧ ≡ nothing
M8-test12 = refl
M8-test13 : M8.⟦ I.num 0 I.+o (I.num 0 I.+o I.num 0) ⟧ ≡ just 0
M8-test13 = refl