{-# OPTIONS --prop --rewriting #-}

module gy02-1 where

open import Lib

-- NatBool nyelv AST szinten

open import RazorAST hiding (T)

  {-
          ite
        /  |  \
       /   |   \
     true  +o   isZero
          /\        |
         /  \       |
    num 1  num 3  isZero
                    |
                    |
                  false
  -}
tm1 : I.Tm
tm1 = ite true (num 1 +o num 3) (isZero (isZero false))
  where open I
  
-- more exercises from the book

t1 : I.Tm
t1 = I.isZero (I.isZero (I.isZero I.true))

t1-test : height t1 ≡ 3
t1-test = refl

t2 : I.Tm
t2 = I.ite I.true (I.num 1 I.+o I.num 3) (I.isZero (I.isZero I.false))

t2-test-1 : height t2 ≡ 3
t2-test-1 = refl

t2-test-2 : ¬ (t1 ≡ t2)
t2-test-2 ()

-- ujradefinialni height-ot modell segitsegevel


-- C-c C-r : refine
Height' : Model {lzero}
Height' = record
  { Tm = ℕ -- \bN
  ; true = 0
  ; false = 0
  ; ite = λ x y z → suc (max x (max y z))
  ; num = λ _ → 0 -- \Gl = \lambda = λ ; \rightarrow = \r = \-> = →
  ; isZero = λ x → suc x
  ; _+o_ = λ x y → suc (max x y)
  }

module TreeHeight = Model Height'

-- C-c C-n: kifejezés normalizálása
-- \[[ = ⟦
-- \]] = ⟧
-- modell: Count the number of trues in a term

Trues : Model {lzero}
Model.Tm     Trues       = ℕ
Model.true   Trues       = 1
Model.false  Trues       = 0
Model.ite    Trues x y z = x + y + z
Model.num    Trues _     = 0
Model.isZero Trues x     = x
Model._+o_   Trues x y   = x + y

module Trues = Model Trues

module testTrues where
  module M = Model Trues

  -- kulonbseg modell-beli es szintaktikus termek kozott
  t : M.Tm
  t = M.true

  t' : M.Tm
  t' = 10

  test1 : M.⟦ I.false I.+o (I.num 1) ⟧ ≡ 0
  test1 = refl
  test2 : M.⟦ tm1 ⟧ ≡ 1
  test2 = refl
  test3 : M.⟦ I.ite I.true I.true I.true ⟧ ≡ 3
  test3 = refl


-- C stilusu interpreter

C : Model {lzero}
C = record
  { Tm = ℕ
  ; true = 1
  ; false = 0
  ; ite = λ where zero    t f → f
                  (suc b) t f → t
  ; num = λ x → x
  ; isZero = λ where zero    → 1
                     (suc _) → 0
  ; _+o_ = _+_
  }

module testC where
  module M = Model C
  open I

  test1 : M.⟦ false ⟧ ≡ 0
  test1 = refl
  test2 : M.⟦ true ⟧ ≡ 1
  test2 = refl
  test3 : M.⟦ ite (num 100) (num 3) (num 2) ⟧ ≡ 3
  test3 = refl
  test4 : M.⟦ ite (num 0) (num 3) (num 2) +o num 3 ⟧ ≡ 5
  test4 = refl

-- ird le az alabbi nyelveket data-val!

-- T ::= op0 | op1 T | op2 T T | op3 T T T | op4 T T T T

data T : Set where
  op0 : T
  op1 : T → T
  op2 : T → T → T
  op3 : T → T → T → T
  op4 : T → T → T → T → T

-- A ::= a | fb B
-- B ::= fa A

-- V ::= vzero | vsuc V
-- E ::= num N | E < E | E = E | var V
-- C ::= V := E | while E S | if E then S else S
-- S ::= empty | C colon S

