{-# OPTIONS --prop --rewriting #-}

module hf12-1sol where

open import Lib
open import Ind.Syntax
open import Ind.Model
open import Ind
open Standard using (St; _∷_; []) renaming (List to StList ; Tree to StTree)

eval : {A : Ty} → Tm ◇ A → St.⟦ A ⟧T
eval t = St.⟦ t ⟧t (mk triv)

-- Szintaktikus cukorka num-ra:
num : ∀{Γ} → ℕ → Tm Γ Nat
num zero    = zeroo
num (suc n) = suco (num n)

-- FELADAT: Bizonyítsd be, hogy a num[] szabály teljesül!
num[] : ∀{Γ Δ n}{σ : Sub Δ Γ} → num n [ σ ] ≡ num n
num[] {n = zero} = zero[]
num[] {n = suc n} = suc[] ◾ cong suco num[]

-- GYAKORLATRÓL FELADAT: isZeroβ₂
isZero : {Γ : Con} → Tm Γ (Nat ⇒ Bool)
isZero = lam (iteNat true false v0)

isZeroβ₂ : ∀{n} → isZero $ num (1 + n) ≡ false {◇}
isZeroβ₂ {n} =
  lam (iteNat true false (var vz)) $ suco (num n)
  ≡⟨ ⇒β ⟩
  iteNat true false (var vz) [ ⟨ suco (num n) ⟩ ]
  ≡⟨ iteNat[] ⟩
  iteNat (true [ ⟨ suco (num n) ⟩ ]) (false [ ⟨ suco (num n) ⟩ ⁺ ]) (var vz [ ⟨ suco (num n) ⟩ ])
  ≡⟨ cong₃ iteNat true[] false[] vz[⟨⟩] ⟩
  iteNat true false (suco (num n))
  ≡⟨ Natβ₂ ⟩
  false [ ⟨ iteNat true false (num n) ⟩ ]
  ≡⟨ false[] ⟩
  false ∎

-- GYAKORLATRÓL FELADAT: plusβ
plus : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
plus = lam (lam (iteNat v0 (suco v0) v1))

plusβ : ∀{Γ n m} → plus $ num n $ num m ≡ num {Γ} (n + m)
plusβ {n = zero} {m} =
  lam (lam (iteNat (var vz) (suco (var vz)) (var (vs vz)))) $ zeroo $ num m
  ≡⟨ cong (_$ num m) (
    lam (lam (iteNat (var vz) (suco (var vz)) (var (vs vz)))) $ zeroo
    ≡⟨ ⇒β ⟩
    lam (iteNat (var vz) (suco (var vz)) (var (vs vz))) [ ⟨ zeroo ⟩ ]
    ≡⟨ lam[] ⟩
    lam (iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ zeroo ⟩ ⁺ ])
    ≡⟨ cong lam iteNat[] ⟩
    lam (iteNat (var vz [ ⟨ zeroo ⟩ ⁺ ]) (suco (var vz) [ (⟨ zeroo ⟩ ⁺) ⁺ ]) (var (vs vz) [ ⟨ zeroo ⟩ ⁺ ]))
    ≡⟨ cong lam (cong₃ iteNat vz[⁺] (suc[] ◾ cong suco vz[⁺]) (vs[⁺] ◾ cong _[ p ] vz[⟨⟩] ◾ zero[])) ⟩
    lam (iteNat (var vz) (suco (var vz)) zeroo)
    ≡⟨ cong lam Natβ₁ ⟩
    lam (var vz) ∎) ⟩
  lam (var vz) $ num m
  ≡⟨ ⇒β ⟩
  var vz [ ⟨ num m ⟩ ]
  ≡⟨ vz[⟨⟩] ⟩
  num m ∎
plusβ {n = suc n} {m} =
  lam (lam (iteNat (var vz) (suco (var vz)) (var (vs vz)))) $ suco (num n) $ num m
  ≡⟨ cong (_$ num m) (
    lam (lam (iteNat (var vz) (suco (var vz)) (var (vs vz)))) $ suco (num n)
    ≡⟨ ⇒β ⟩
    lam (iteNat (var vz) (suco (var vz)) (var (vs vz))) [ ⟨ suco (num n) ⟩ ]
    ≡⟨ lam[] ⟩
    lam (iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco (num n) ⟩ ⁺ ])
    ≡⟨ cong lam iteNat[] ⟩
    lam (iteNat (var vz [ ⟨ suco (num n) ⟩ ⁺ ]) (suco (var vz) [ (⟨ suco (num n) ⟩ ⁺) ⁺ ]) (var (vs vz) [ ⟨ suco (num n) ⟩ ⁺ ]))
    ≡⟨ cong lam (cong₃ iteNat vz[⁺] (suc[] ◾ cong suco vz[⁺]) (vs[⁺] ◾ cong _[ p ] vz[⟨⟩] ◾ suc[] ◾ cong suco num[])) ⟩
    lam (iteNat (var vz) (suco (var vz)) (suco (num n)))
    ≡⟨ cong lam (
      iteNat (var vz) (suco (var vz)) (suco (num n))
      ≡⟨ Natβ₂ ⟩
      suco (var vz) [ ⟨ iteNat (var vz) (suco (var vz)) (num n) ⟩ ]
      ≡⟨ suc[] ⟩
      suco (var vz [ ⟨ iteNat (var vz) (suco (var vz)) (num n) ⟩ ])
      ≡⟨ cong suco vz[⟨⟩] ⟩
      suco (iteNat (var vz) (suco (var vz)) (num n)) ∎
    ) ⟩
    lam (suco (iteNat (var vz) (suco (var vz)) (num n))) ∎) ⟩
  lam (suco (iteNat (var vz) (suco (var vz)) (num n))) $ num m
  ≡⟨ ⇒β ⟩
  suco (iteNat (var vz) (suco (var vz)) (num n)) [ ⟨ num m ⟩ ]
  ≡⟨ suc[] ⟩
  suco (iteNat (var vz) (suco (var vz)) (num n) [ ⟨ num m ⟩ ])
  ≡⟨ cong suco (
    iteNat (var vz) (suco (var vz)) (num n) [ ⟨ num m ⟩ ]
    ≡⟨ ⇒β ⁻¹ ⟩
    lam (iteNat (var vz) (suco (var vz)) (num n)) $ num m
    ≡⟨ cong (_$ num m) (
      lam (iteNat (var vz) (suco (var vz)) (num n))
      ≡⟨ cong lam (cong₃ iteNat (vz[⁺] ⁻¹) ((suc[] ◾ cong suco vz[⁺]) ⁻¹) ((vs[⁺] ◾ (cong _[ p ] vz[⟨⟩] ◾ num[])) ⁻¹)) ⟩
      lam (iteNat (var vz [ ⟨ num n ⟩ ⁺ ]) (suco (var vz) [ (⟨ num n ⟩ ⁺) ⁺ ]) (var (vs vz) [ ⟨ num n ⟩ ⁺ ]))
      ≡⟨ cong lam (iteNat[] ⁻¹) ⟩
      lam (iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ num n ⟩ ⁺ ])
      ≡⟨ lam[] ⁻¹ ⟩
      lam (iteNat (var vz) (suco (var vz)) (var (vs vz))) [ ⟨ num n ⟩ ]
      ≡⟨ ⇒β ⁻¹ ⟩
      lam (lam (iteNat (var vz) (suco (var vz)) (var (vs vz)))) $ num n ∎) ⟩
    lam (lam (iteNat (var vz) (suco (var vz)) (var (vs vz)))) $ num n $ num m ∎) ⟩
  suco (lam (lam (iteNat (var vz) (suco (var vz)) (var (vs vz)))) $ num n $ num m)
  ≡⟨ cong suco (plusβ {n = n} {m}) ⟩
  suco (num (n + m)) ∎

idl+o : ∀{Γ n} → plus $ num 0 $ num n ≡ num {Γ} n
idl+o {n = n} = plusβ {n = 0}

idr+o : ∀{Γ n} → plus $ num n $ num 0 ≡ num {Γ} n
idr+o {n = n} = plusβ {n = n} ◾ cong num idr+

-- GYAKORLATRÓL FELADAT: times, Definiáld azt a függvényt, amely két számot összeszoroz.
times : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
times = lam (lam (iteNat (num 0) (iteNat v0 (suco v0) v1) v1))

times-test-1 : eval times 1 5 ≡ 5
times-test-1 = refl

times-test-2 : eval times 2 3 ≡ 6
times-test-2 = refl

times-test-3 : eval times zero 4 ≡ zero
times-test-3 = refl

times-test-4 : eval times 12 43 ≡ 516
times-test-4 = refl

-- FELADAT: Definiáld a hatványozás függvényét.
pow : ∀{Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
pow = lam (lam (iteNat (num 1) (iteNat (num 0) (iteNat v0 (suco v0) v1) v2) v0))

pow-test-1 : eval pow 2 5 ≡ 32
pow-test-1 = refl

pow-test-2 : eval pow 3 3 ≡ 27
pow-test-2 = refl

pow-test-3 : eval pow 0 0 ≡ 1
pow-test-3 = refl

pow-test-4 : eval pow 0 5 ≡ 0
pow-test-4 = refl

pow-test-5 : eval pow 7 4 ≡ 2401
pow-test-5 = refl

-- FELADAT: Definiáld azt a függvényt az objektumnyelvben, amely
--          egy listányi számot összead.
sumList : ∀{Γ} → Tm Γ (List Nat ⇒ Nat)
sumList = lam (iteList (num 0) (iteNat v0 (suco v0) v1) v0)

sumList-test-1 : let l = eval sumList (1 ∷ 4 ∷ 3 ∷ []) in l ≡ 8
sumList-test-1 = refl

sumList-test-2 : let l = eval sumList (5 ∷ 3 ∷ 10 ∷ 9 ∷ 0 ∷ []) in l ≡ 27
sumList-test-2 = refl

sumList-test-3 : let l = eval sumList [] in l ≡ 0
sumList-test-3 = refl

sumList-test-4 : let l = eval sumList (0 ∷ []) in l ≡ 0
sumList-test-4 = refl

sumList-test-5 : let l = eval sumList (10 ∷ 90 ∷ 11 ∷ 23 ∷ 45 ∷ 120 ∷ 87 ∷ []) in l ≡ 386
sumList-test-5 = refl

-- FELADAT: Bizonyítsd be az alábbi állítást a szintaxisban!
sumList-proof : ∀{Γ} → sumList $ (cons (num 2) (cons (num 3) (cons (num 1) nil))) ≡ num {Γ} 6
sumList-proof =
  lam (iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) (var vz))
  $ cons (num 2) (cons (num 3) (cons (num 1) nil))
  ≡⟨ ⇒β ⟩
  iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) (var vz) [ ⟨ cons (num 2) (cons (num 3) (cons (num 1) nil)) ⟩ ]
  ≡⟨ iteList[] ⟩
  iteList (zeroo [ ⟨ cons (num 2) (cons (num 3) (cons (num 1) nil)) ⟩ ])
          (iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ cons (num 2) (cons (num 3) (cons (num 1) nil)) ⟩ ⁺ ⁺ ])
          (var vz [ ⟨ cons (num 2) (cons (num 3) (cons (num 1) nil)) ⟩ ])
  ≡⟨ cong₃ iteList zero[] (iteNat[] ◾ cong₃ iteNat vz[⁺] (suc[] ◾ cong suco vz[⁺]) (vs[⁺] ◾ (cong _[ p ] vz[⁺] ◾ [p]))) vz[⟨⟩] ⟩
  iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) (cons (num 2) (cons (num 3) (cons (num 1) nil)))
  ≡⟨ Listβ₂ ⟩
  iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco (suco zeroo) ⟩ ⁺ ] [ ⟨ iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) (cons (num 3) (cons (num 1) nil)) ⟩ ]
  ≡⟨ cong _[ ⟨ iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) (cons (num 3) (cons (num 1) nil)) ⟩ ] (
    iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco (suco zeroo) ⟩ ⁺ ]
    ≡⟨ iteNat[] ⟩
    iteNat (var vz [ ⟨ suco (suco zeroo) ⟩ ⁺ ])
           (suco (var vz) [ (⟨ suco (suco zeroo) ⟩ ⁺) ⁺ ])
           (var (vs vz) [ ⟨ suco (suco zeroo) ⟩ ⁺ ])
    ≡⟨ cong₃ iteNat vz[⁺] (suc[] ◾ cong suco vz[⁺]) (vs[⁺] ◾ (cong _[ p ] vz[⟨⟩] ◾ num[])) ⟩
    iteNat (var vz) (suco (var vz)) (num 2)
    ≡⟨ Natβ₂ ⟩
    suco (var vz) [ ⟨ iteNat (var vz) (suco (var vz)) (suco zeroo) ⟩ ]
    ≡⟨ cong (λ x → suco (var vz) [ ⟨ x ⟩ ]) Natβ₂ ⟩
    suco (var vz) [ ⟨ suco (var vz) [ ⟨ iteNat (var vz) (suco (var vz)) zeroo ⟩ ] ⟩ ]
    ≡⟨ cong (λ x → suco (var vz) [ ⟨ suco (var vz) [ ⟨ x ⟩ ] ⟩ ]) Natβ₁ ⟩
    suco (var vz) [ ⟨ suco (var vz) [ ⟨ var vz ⟩ ] ⟩ ]
    ≡⟨ suc[] ◾ cong suco vz[⟨⟩] ⟩
    suco (suco (var vz) [ ⟨ var vz ⟩ ])
    ≡⟨ cong suco (suc[] ◾ cong suco vz[⟨⟩]) ⟩
    suco (suco (var vz)) ∎) ⟩
  suco (suco (var vz)) [ ⟨ iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) (cons (num 3) (cons (num 1) nil)) ⟩ ]
  ≡⟨ suc[] ◾ cong suco (suc[] ◾ cong suco vz[⟨⟩]) ⟩
  suco (suco (iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) (cons (num 3) (cons (num 1) nil))))
  ≡⟨ cong (λ x → suco (suco x)) (
    iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) (cons (num 3) (cons (num 1) nil))
    ≡⟨ Listβ₂ ⟩
    iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco (suco (suco zeroo)) ⟩ ⁺ ] [ ⟨ iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) (cons (suco zeroo) nil) ⟩ ]
    ≡⟨ cong (λ x → iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco (suco (suco zeroo)) ⟩ ⁺ ] [ ⟨ x ⟩ ]) Listβ₂ ⟩
    iteNat (var vz) (suco (var vz)) (var (vs vz))
      [ ⟨ suco (suco (suco zeroo)) ⟩ ⁺ ]
      [ ⟨ iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco zeroo ⟩ ⁺ ] [ ⟨ iteList zeroo (iteNat (var vz) (suco (var vz)) (var (vs vz))) nil ⟩ ] ⟩ ]
    ≡⟨ cong (λ x → iteNat (var vz) (suco (var vz)) (var (vs vz))
      [ ⟨ suco (suco (suco zeroo)) ⟩ ⁺ ]
      [ ⟨ iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco zeroo ⟩ ⁺ ] [ ⟨ x ⟩ ] ⟩ ]) Listβ₁ ⟩
    iteNat (var vz) (suco (var vz)) (var (vs vz)) 
      [ ⟨ suco (suco (suco zeroo)) ⟩ ⁺ ]
      [ ⟨ iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco zeroo ⟩ ⁺ ] [ ⟨ zeroo ⟩ ] ⟩ ]
    ≡⟨ cong (λ x → iteNat (var vz) (suco (var vz)) (var (vs vz)) 
      [ ⟨ suco (suco (suco zeroo)) ⟩ ⁺ ] [ ⟨ x ⟩ ]) (
        iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco zeroo ⟩ ⁺ ] [ ⟨ zeroo ⟩ ]
        ≡⟨ ⇒β ⁻¹ ⟩
        lam (iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco zeroo ⟩ ⁺ ]) $ zeroo
        ≡⟨ cong (_$ num 0) ((⇒β ◾ lam[]) ⁻¹) ⟩
        plus $ num 1 $ num 0
        ≡⟨ plusβ ⟩
        num 1 ∎) ⟩
    iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco (suco (suco zeroo)) ⟩ ⁺ ] [ ⟨ suco zeroo ⟩ ]
    ≡⟨ ⇒β ⁻¹ ⟩
    lam (iteNat (var vz) (suco (var vz)) (var (vs vz)) [ ⟨ suco (suco (suco zeroo)) ⟩ ⁺ ]) $ suco zeroo
    ≡⟨ cong (_$ num 1) ((⇒β ◾ lam[]) ⁻¹) ⟩
    plus $ num 3 $ num 1
    ≡⟨ plusβ ⟩
    num 4 ∎) ⟩
  num 6 ∎

-- NEHÉZ FELADAT: Definiáld a listák másik iterátorát, amely bal oldalról hajtja végre a műveletet, nem jobbról, mint az iteList.
{-
iteListL' : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ B ▹ A) B → Tm Γ (List A) → Tm Γ B
iteListL' {Γ} {A} {B} b f l = (iteList {Γ ▹ B} {A} {B} v0 {!   !} {!   !}) [ ⟨ b ⟩ ]
-}

iteListL : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ B ▹ A) B → Tm Γ (List A) → Tm Γ B
iteListL {Γ} {A} {B} b f l = iteList {Γ} {A} {B ⇒ B} (lam v0) (lam (v1 $ ((lam (lam f) [ p ] [ p ] [ p ] $ v0 $ v2)))) l $ b

foldl-test-1 : eval (iteListL (num 2) (pow $ v1 $ v0) (cons (num 3) (cons (num 4) nil))) ≡ 4096
foldl-test-1 = refl

-- Bizonyítsd az iteListL elimináló szabályait.
ListLβ₁ : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A) B} → iteListL u v nil ≡ u
ListLβ₁ = {!   !}

ListLβ₂ : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
               iteListL u v (cons t₁ t) ≡ iteListL (v [ ⟨ u ⟩ ⁺ ] [ ⟨ t₁ ⟩ ]) v t
ListLβ₂ = {!   !}

-- GYAKORLATRÓL FELADAT: Definiáld azt a függvényt, amely két listát összefűz.
++ : {Γ : Con}{A : Ty} → Tm Γ (List A ⇒ List A ⇒ List A)
++ = lam (lam (iteList v0 (cons v1 v0) v1))

++-test-1 : eval (++ $ nil {A = Bool} $ nil {A = Bool}) ≡ []
++-test-1 = refl

++-test-2 : eval (++ $ nil $ (cons trivial nil)) ≡ mk triv ∷ []
++-test-2 = refl

++-test-3 : eval (++ $ (cons zeroo nil) $ (cons (suco zeroo) nil)) ≡
                zero ∷ 1 ∷ []
++-test-3 = refl

-- FELADAT: Definiáld azt a függvényt, amely listák listáját összefűzi
concat : ∀{Γ A} → Tm Γ (List (List A) ⇒ List A)
concat = lam (iteList nil (++ $ v1 $ v0) v0)

concat-test-1 : eval (concat {A = Nat}) ((1 ∷ 2 ∷ []) ∷ (3 ∷ []) ∷ [] ∷ (4 ∷ 5 ∷ 6 ∷ []) ∷ []) ≡ 1 ∷ 2 ∷ 3 ∷ 4 ∷ 5 ∷ 6 ∷ []
concat-test-1 = refl

concat-test-2 : eval (concat {A = Nat}) ((2 ∷ []) ∷ (30 ∷ []) ∷ (1 ∷ 20 ∷ []) ∷ []) ≡ 2 ∷ 30 ∷ 1 ∷ 20 ∷ []
concat-test-2 = refl

-- FELADAT: Definiáld azt a függvényt, amely egy fát a preorder bejárásnak megfelelően alakít át listává.
preorder : ∀{Γ A} → Tm Γ (Tree A ⇒ List A)
preorder = {!   !}