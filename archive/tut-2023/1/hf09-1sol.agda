{-# OPTIONS --prop --rewriting #-}

module hf09-1sol where

open import Lib
open import Def
open import Def.Syntax

var-eq-14 : v1 [ ⟨ num 2 ⟩ ⁺ ] ≡ num {◇ ▹ Bool} 2
var-eq-14 =
  var (vs vz) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ vs[⁺] ⟩
  var vz [ ⟨ num 2 ⟩ ] [ p ]
  ≡⟨ cong _[ p ] vz[⟨⟩] ⟩
  num 2 [ p ]
  ≡⟨ num[] ⟩
  num 2 ∎

var-eq-15 : v2 [ ⟨ num 10 ⟩ ⁺ ⁺ ] ≡ num {◇ ▹ Bool ▹ Nat} 10
var-eq-15 =
  var (vs (vs vz)) [ (⟨ num 10 ⟩ ⁺) ⁺ ]
  ≡⟨ vs[⁺] ⟩
  var (vs vz) [ ⟨ num 10 ⟩ ⁺ ] [ p ]
  ≡⟨ cong _[ p ] vs[⁺] ⟩
  var vz [ ⟨ num 10 ⟩ ] [ p ] [ p ]
  ≡⟨ cong (λ x → x [ p ] [ p ]) vz[⟨⟩] ⟩
  num 10 [ p ] [ p ]
  ≡⟨ cong _[ p ] num[] ◾ num[] ⟩
  num 10 ∎

var-eq-16 : isZero {◇ ▹ Nat ▹ Nat ▹ Bool} (v1 +o v2) [ ⟨ num 0 ⟩ ⁺ ] [ ⟨ num 2 ⟩ ⁺ ] ≡ ite v1 false true [ ⟨ true ⟩ ⁺ ]
var-eq-16 =
  isZero (v1 +o v2) [ ⟨ num 0 ⟩ ⁺ ] [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ cong _[ ⟨ num 2 ⟩ ⁺ ] isZero[] ⟩
  isZero (v1 +o v2 [ ⟨ num zero ⟩ ⁺ ]) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ cong (λ x → isZero x [ ⟨ num 2 ⟩ ⁺ ]) +[] ⟩
  isZero ((v1 [ ⟨ num zero ⟩ ⁺ ]) +o (v2 [ ⟨ num zero ⟩ ⁺ ])) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ cong₂ (λ x y → isZero (x +o y) [ ⟨ num 2 ⟩ ⁺ ]) vs[⁺] vs[⁺] ⟩
  isZero ((v0 [ ⟨ num zero ⟩ ] [ p ]) +o (v1 [ ⟨ num zero ⟩ ] [ p ])) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ cong₂ (λ x y → isZero (x +o y) [ ⟨ num 2 ⟩ ⁺ ]) (cong _[ p ] vz[⟨⟩] ◾ num[]) (cong _[ p ] vs[⟨⟩] ◾ [p]) ⟩
  isZero (num zero +o v1) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ isZero[] ⟩
  isZero (num zero +o v1 [ ⟨ num 2 ⟩ ⁺ ])
  ≡⟨ cong isZero +[] ⟩
  isZero ((num zero [ ⟨ num 2 ⟩ ⁺ ]) +o (var (vs vz) [ ⟨ num 2 ⟩ ⁺ ]))
  ≡⟨ cong isZero (cong₂ _+o_ num[] (vs[⁺] ◾ cong _[ p ] vz[⟨⟩] ◾ num[])) ⟩
  isZero (num zero +o num 2)
  ≡⟨ cong isZero +β ⟩
  isZero (num 2)
  ≡⟨ isZeroβ₂ ⟩
  false
  ≡⟨ iteβ₁ ⁻¹ ⟩
  ite true false true
  ≡⟨ cong₃ ite (true[] ⁻¹) (false[] ⁻¹) (true[] ⁻¹) ⟩
  ite (true [ p ]) (false [ ⟨ true ⟩ ⁺ ]) (true [ ⟨ true ⟩ ⁺ ])
  ≡⟨ cong (λ x → ite (x [ p ]) (false [ ⟨ true ⟩ ⁺ ]) (true [ ⟨ true ⟩ ⁺ ])) (vz[⟨⟩] ⁻¹) ⟩
  ite (v0 [ ⟨ true ⟩ ] [ p ]) (false [ ⟨ true ⟩ ⁺ ]) (true [ ⟨ true ⟩ ⁺ ])
  ≡⟨ cong (λ x → ite x (false [ ⟨ true ⟩ ⁺ ]) (true [ ⟨ true ⟩ ⁺ ])) (vs[⁺] ⁻¹) ⟩
  ite (v1 [ ⟨ true ⟩ ⁺ ]) (false [ ⟨ true ⟩ ⁺ ]) (true [ ⟨ true ⟩ ⁺ ])
  ≡⟨ ite[] ⁻¹ ⟩
  ite v1 false true [ ⟨ true ⟩ ⁺ ] ∎

var-eq-17 : isZero {◇ ▹ Nat ▹ Nat ▹ Nat} v1 [ ⟨ v0 +o num 0 ⟩ ⁺ ] [ ⟨ num 2 ⟩ ⁺ ]
          ≡ isZero v0 [ ⟨ num 1 +o v1 ⟩ ] [ ⟨ num 0 ⟩ ⁺ ]
var-eq-17 =
  isZero v1 [ ⟨ v0 +o num 0 ⟩ ⁺ ] [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ cong _[ ⟨ num 2 ⟩ ⁺ ] isZero[] ⟩
  isZero (v1 [ ⟨ v0 +o num zero ⟩ ⁺ ]) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ cong (λ x → isZero x [ ⟨ num 2 ⟩ ⁺ ]) vs[⁺] ⟩
  isZero (v0 [ ⟨ v0 +o num zero ⟩ ] [ p ]) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ cong (λ x → isZero (x [ p ]) [ ⟨ num 2 ⟩ ⁺ ]) vz[⟨⟩] ⟩
  isZero (v0 +o num zero [ p ]) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ cong (λ x → isZero x [ ⟨ num 2 ⟩ ⁺ ]) +[] ⟩
  isZero ((v0 [ p ]) +o (num 0 [ p ])) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ cong₂ (λ x y → isZero (x +o y) [ ⟨ num 2 ⟩ ⁺ ]) [p] num[] ⟩
  isZero (v1 +o num 0) [ ⟨ num 2 ⟩ ⁺ ]
  ≡⟨ isZero[] ◾ cong isZero +[] ⟩
  isZero ((v1 [ ⟨ num 2 ⟩ ⁺ ]) +o (num 0 [ ⟨ num 2 ⟩ ⁺ ]))
  ≡⟨ cong₂ (λ x y → isZero (x +o y)) vs[⁺] num[] ⟩
  isZero ((v0 [ ⟨ num 2 ⟩ ] [ p ]) +o num 0)
  ≡⟨ cong (λ x → isZero (x +o num 0)) (cong _[ p ] vz[⟨⟩] ◾ num[]) ⟩
  isZero (num 2 +o num 0)
  ≡⟨ cong isZero +β ⟩
  isZero (num 2)
  ≡⟨ isZeroβ₂ ⟩
  false
  ≡⟨ isZeroβ₂ ⁻¹ ⟩
  isZero (num 1)
  ≡⟨ cong isZero (+β ⁻¹) ⟩
  isZero (num 1 +o num zero)
  ≡⟨ cong (λ x → isZero (num 1 +o x)) (num[] ⁻¹ ◾ cong _[ p ] (vz[⟨⟩] ⁻¹)) ⟩
  isZero (num 1 +o (v0 [ ⟨ num 0 ⟩ ] [ p ]))
  ≡⟨ cong₂ (λ x y → isZero (x +o y)) (num[] ⁻¹) (vs[⁺] ⁻¹) ⟩
  isZero ((num 1 [ ⟨ num 0 ⟩ ⁺ ]) +o (v1 [ ⟨ num 0 ⟩ ⁺ ]))
  ≡⟨ cong isZero (+[] ⁻¹) ◾ isZero[] ⁻¹ ⟩
  isZero (num 1 +o v1) [ ⟨ num 0 ⟩ ⁺ ]
  ≡⟨ cong (λ x → isZero x [ ⟨ num 0 ⟩ ⁺ ]) (vz[⟨⟩] ⁻¹) ⟩
  isZero (v0 [ ⟨ num 1 +o v1 ⟩ ]) [ ⟨ num 0 ⟩ ⁺ ]
  ≡⟨ cong _[ ⟨ num 0 ⟩ ⁺ ] (isZero[] ⁻¹) ⟩
  isZero v0 [ ⟨ num 1 +o v1 ⟩ ] [ ⟨ num 0 ⟩ ⁺ ] ∎