{-# OPTIONS --prop --rewriting #-}

module gy13-1 where

open import Lib
open import Coind.Syntax

num : ∀{Γ} → ℕ → Tm Γ Nat
num zero = zeroo
num (suc n) = suco (num n)

[0→∞] : ∀{Γ} → Tm Γ (Stream Nat)
[0→∞] = genStream v0 (suco v0) zeroo

test-1 : head (tail [0→∞]) ≡ (num {◇} 1)
test-1 = 
  head (tail (genStream (var vz) (suco (var vz)) zeroo))
  ≡⟨ cong head Streamβ₂ ⟩
  head (genStream (var vz) (suco (var vz)) (suco (var vz) [ ⟨ zeroo ⟩ ]))
  ≡⟨ Streamβ₁ ⟩
  var vz [ ⟨ suco (var vz) [ ⟨ zeroo ⟩ ] ⟩ ]
  ≡⟨ vz[⟨⟩] ⟩
  suco (var vz) [ ⟨ zeroo ⟩ ]
  ≡⟨ suc[] ⟩
  suco (var vz [ ⟨ zeroo ⟩ ])
  ≡⟨ cong suco vz[⟨⟩] ⟩
  num 1 ∎

repeat : ∀{Γ A} → Tm Γ (A ⇒ Stream A)
repeat = lam (genStream v0 v0 v0)

test-2 : head (tail (tail (repeat $ true))) ≡ true {◇}
test-2 =
  head (tail (tail (lam (genStream (var vz) (var vz) (var vz)) $ true)))
  ≡⟨ cong (λ x → head (tail (tail x))) (⇒β ◾ genStream[]) ⟩
  head (tail (tail (genStream (var vz [ ⟨ true ⟩ ⁺ ]) (var vz [ ⟨ true ⟩ ⁺ ]) (var vz [ ⟨ true ⟩ ]))))
  ≡⟨ cong (λ x → head (tail (tail x))) (cong₃ genStream vz[⁺] vz[⁺] vz[⟨⟩]) ⟩
  head (tail (tail (genStream (var vz) (var vz) true)))
  ≡⟨ cong (λ x → head (tail x)) Streamβ₂ ⟩
  head (tail (genStream (var vz) (var vz) (var vz [ ⟨ true ⟩ ])))
  ≡⟨ cong head Streamβ₂ ⟩
  head (genStream (var vz) (var vz) (var vz [ ⟨ var vz [ ⟨ true ⟩ ] ⟩ ]))
  ≡⟨ Streamβ₁ ⟩
  var vz [ ⟨ var vz [ ⟨ var vz [ ⟨ true ⟩ ] ⟩ ] ⟩ ]
  ≡⟨ vz[⟨⟩] ⟩
  var vz [ ⟨ var vz [ ⟨ true ⟩ ] ⟩ ]
  ≡⟨ vz[⟨⟩] ⟩
  var vz [ ⟨ true ⟩ ]
  ≡⟨ vz[⟨⟩] ⟩
  true ∎


{-
Machine with a +1 button, a get button, zero out button
genMachine : Tm (Γ ▹ C) C → Tm (Γ ▹ C) Nat → Tm (Γ ▹ C) C → Tm Γ C → Tm Γ Machine

addOne : Tm ◇ Machine
addOne = genMachine (suco v1) (num 0) v0 (num 0)

test-3 :  addOne
-}