{-
Definiáld a mul2 függvényt (STT-ben), amely egy számot megszoroz 2-vel. Bizonyítsd az alábbi két állítást:
mul2-test1 : mul2 $ (num 3) ≡ num 6
mul2-test2 : def mul2 (v0 $ num 4) ≡ num 8
Definiáld a chi függvényt, amely egy Bool-t átalakít egy számra úgy, hogy ha a Bool értéke true, akkor 1-re, ha false, akkor 0-ra. (Programozás elméletből és programozás módszertanból ismert függvény).
Bizonyítsd az alábbi állításokat:
chi-test1 : chi $ (isZero (num 0)) ≡ num 1
chi-test2 : def (isZero (num 1)) (chi $ v0) ≡ num 0
Definiáld az == függvényt Bool-okra; ellenőrzi, hogy két Bool megegyezik-e.
Bizonyítsd az alábbi állításokat:
eq-test1 : == $ false $ true ≡ false
eq-test2 : def (== $ true) (v0 $ true) ≡ true
eq-test3 : def (== $ false) (def (isZero (num 1)) (v1 $ v0)) ≡ true
Definiáld a depends függvényt, amely egy Bool alapján eldönti, hogy egy számot megszoroz 2-vel (ha a Bool true), vagy egy számhoz hozzáad 2-t (ha a Bool false).
Bizonyítsd az alábbi állításokat:
depends-test1 : def (isZero (num 1)) (depends $ v0 $ num 4) ≡ num 6
depends-test2 : def (isZero (num 0)) (depends $ v0 $ num 4) ≡ num 8
depends-test3 : def (depends $ false) (depends $ true $ (v0 $ num 3)) ≡ num 10
-}