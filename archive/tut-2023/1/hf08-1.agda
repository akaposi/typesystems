{-# OPTIONS --prop --rewriting #-}

module hf08-1 where

open import Lib
open import Def
open import Def.Syntax

-- var-eq-12-t ÉRDEMES megnézni, mert bár visszafelé egyenlőséget ugyan
-- nem mutattam ebben a világban, a koncepció már ismerős és Razor-ben mutattam.
-- Ugyanazt kell csinálni, csak az egyenlőségek száma több.

var-eq-6 : def (num 0) (def (isZero v0) (ite v0 false true)) ≡ false {◇}
var-eq-6 =
  def (num 0) (def (isZero v0) (ite v0 false true))
  ≡⟨ {!   !} ⟩
  false ∎

var-eq-7 : def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0))) ≡ num {◇} 2
var-eq-7 =
  def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
  ≡⟨ {!   !} ⟩
  num 2 ∎

var-eq-8 : def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0))) ≡ false {◇}
var-eq-8 =
  def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0)))
  ≡⟨ {!    !} ⟩
  false ∎

var-eq-9 : def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0))) ≡ num {◇} 2
var-eq-9 =
  def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0)))
  ≡⟨ {!   !} ⟩
  num 2 ∎

var-eq-10 : ite v0 v1 v2 +o ite (isZero v1) v2 (num 2) [ ⟨ true ⟩ ] [ ⟨ num 3 ⟩ ] [ ⟨ num 10 ⟩ ] ≡ num {◇} 5
var-eq-10 = {!   !}

var-eq-11 : ite (def (num 0) (isZero v0)) (def (v0 +o num 1) (num 3 +o v0 +o v1)) (v0 +o v1 [ ⟨ num 5 ⟩ ⁺ ]) [ ⟨ num 2 ⟩ ] ≡ num {◇} 8
var-eq-11 = {!   !}

var-eq-12 : v0 +o num 1 [ ⟨ v0 {◇ ▹ Nat} ⟩ ] ≡ v0 [ ⟨ v0 +o num 1 ⟩ ]
var-eq-12 = {!   !}