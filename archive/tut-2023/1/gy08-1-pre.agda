{-# OPTIONS --prop --rewriting #-}

module gy08-1 where

open import Lib
open import Def
open import Def.Syntax

module ex1 where
  t : Tm (◇ ▹ Bool) Nat
  t = ite (v 0) (num 1) (num 0)

  γ : Sub ◇ (◇ ▹ Bool)
  γ = ⟨ true ⟩ 

  t[γ] : t [ γ ] ≡ num 1
  t[γ] =
    (t [ γ ])
    ≡⟨ refl ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    num 1 ∎
  
  δ' : Sub (◇ ▹ Bool) (◇ ▹ Bool ▹ Nat) 
  δ' = ⟨ ite (v 0) (num 1) (num 2) ⟩

  δ : Sub (◇ ▹ Bool ▹ Nat) (◇ ▹ Bool ▹ Nat ▹ Nat)
  δ = ⟨ num 2 ⟩

  u : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Nat
  u = v 0 +o v 1

  u[δ][δ'] : u [ δ ] [ δ' ] ≡ num 2 +o ite (v 0) (num 1) (num 2)
  u[δ][δ'] =
    (u [ δ ] [ δ' ])
    ≡⟨ refl ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    num 2 +o ite (v 0) (num 1) (num 2) ∎

module ex2 where
  var-test-1 : (def {◇} true v0) ≡ true
  var-test-1 =
    def true v0
    ≡⟨ {!   !} ⟩
    true ∎
  
  var-test-2 : (def {◇} true (def false v0)) ≡ false
  var-test-2 =
    def true (def false v0)
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    false ∎
  
  var-test-3 : (def {◇} true (def false v1)) ≡ true
  var-test-3 =
    def true (def false v1)
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    true ∎

  task-eq : (ite v1 (v0 +o v0) v0) [ {!   !} ] [ {!   !} ] ≡ num {◇} 4
  task-eq =
    (ite v1 (v0 +o v0) v0) [ {!   !} ] [ {!   !} ]
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    num 4 ∎
  
  var-eq-4 : def (num 1) (v0 +o v0) ≡ num {◇} 2
  var-eq-4 =
    def (num 1) (v0 +o v0)
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    {!   !}
    ≡⟨ {!   !} ⟩
    num 2 ∎
  
  var-eq-5 : def false (def (num 2) (ite v1 (num 0) (num 1 +o v0))) ≡ num {◇} 3
  var-eq-5 =
    def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
    ≡⟨ {!   !} ⟩
    num 3 ∎
  
  var-eq-6 : def (num 0) (def (isZero v0) (ite v0 false true)) ≡ false {◇}
  var-eq-6 =
    def (num 0) (def (isZero v0) (ite v0 false true))
    ≡⟨ {!   !} ⟩
    false ∎
  
  var-eq-7 : def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0))) ≡ num {◇} 2
  var-eq-7 =
    def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
    ≡⟨ {!   !} ⟩
    num 2 ∎
  
  var-eq-8 : def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0))) ≡ false {◇}
  var-eq-8 =
    def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0)))
    ≡⟨ {!    !} ⟩
    false ∎
  
  var-eq-9 : def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0))) ≡ num {◇} 2
  var-eq-9 =
    def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0)))
    ≡⟨ {!   !} ⟩
    num 2 ∎