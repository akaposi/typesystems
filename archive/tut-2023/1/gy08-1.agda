{-# OPTIONS --prop --rewriting #-}

module gy08-1 where

open import Lib
open import Def
open import Def.Syntax

module ex1 where
  t : Tm (◇ ▹ Bool) Nat
  t = ite (v 0) (num 1) (num 0)

  γ : Sub ◇ (◇ ▹ Bool)
  γ = ⟨ true ⟩

  t[γ] : t [ γ ] ≡ num 1
  t[γ] =
    (t [ γ ])
    ≡⟨ refl ⟩
    (ite v0 (num 1) (num 0)) [ ⟨ true ⟩ ]
    ≡⟨ ite[] ⟩
    ite (v0 [ ⟨ true ⟩ ]) (num 1 [ ⟨ true ⟩ ]) (num 0 [ ⟨ true ⟩ ])
    ≡⟨ cong (λ x → ite x (num 1 [ ⟨ true ⟩ ]) (num 0 [ ⟨ true ⟩ ])) vz[⟨⟩] ⟩
    ite true (num 1 [ ⟨ true ⟩ ]) (num zero [ ⟨ true ⟩ ])
    ≡⟨ iteβ₁ ⟩
    num 1 [ ⟨ true ⟩ ]
    ≡⟨ num[] ⟩
    num 1 ∎
  
  δ' : Sub (◇ ▹ Bool) (◇ ▹ Bool ▹ Nat) 
  δ' = ⟨ ite (v 0) (num 1) (num 2) ⟩

  δ : Sub (◇ ▹ Bool ▹ Nat) (◇ ▹ Bool ▹ Nat ▹ Nat)
  δ = ⟨ num 2 ⟩

  u : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Nat
  u = v 0 +o v 1

  u[δ][δ'] : u [ δ ] [ δ' ] ≡ num 2 +o ite (v 0) (num 1) (num 2)
  u[δ][δ'] =
    (u [ δ ] [ δ' ])
    ≡⟨ refl ⟩
    ((var vz +o var (vs vz)) [ ⟨ num 2 ⟩ ]) [ ⟨ ite (var vz) (num 1) (num 2) ⟩ ]
    ≡⟨ (cong (_[ ⟨ ite (var vz) (num 1) (num 2) ⟩ ])) +[] ⟩
    (var vz [ ⟨ num 2 ⟩ ]) +o (var (vs vz) [ ⟨ num 2 ⟩ ])
      [ ⟨ ite (var vz) (num 1) (num 2) ⟩ ]
    ≡⟨ +[] ⟩ -- ezzel szívatjuk magunkat.
    (var vz [ ⟨ num 2 ⟩ ] [ ⟨ ite (var vz) (num 1) (num 2) ⟩ ]) +o
    (var (vs vz) [ ⟨ num 2 ⟩ ] [ ⟨ ite (var vz) (num 1) (num 2) ⟩ ])
    ≡⟨ cong₂ (λ x y → (x [ ⟨ ite (var vz) (num 1) (num 2) ⟩ ]) +o (y [ ⟨ ite (var vz) (num 1) (num 2) ⟩ ]))
      vz[⟨⟩] vs[⟨⟩] ⟩
    (num 2 [ ⟨ ite (var vz) (num 1) (num 2) ⟩ ]) +o
    (var vz [ ⟨ ite (var vz) (num 1) (num 2) ⟩ ])
    ≡⟨ cong₂ _+o_ num[] vz[⟨⟩] ⟩
    num 2 +o ite (v 0) (num 1) (num 2) ∎

module ex2 where
  var-test-1 : (def {◇} true v0) ≡ true
  var-test-1 =
    def true v0
    ≡⟨ vz[⟨⟩] ⟩
    true ∎
  
  var-test-2 : (def {◇} true (def false v0)) ≡ false
  var-test-2 =
    def true (def false v0)
    ≡⟨ cong _[ ⟨ true ⟩ ] vz[⟨⟩] ⟩
    false [ ⟨ true ⟩ ]
    ≡⟨ false[] ⟩
    false ∎
  
  var-test-3 : (def {◇} true (def false v1)) ≡ true
  var-test-3 =
    def true (def false v1)
    ≡⟨ cong _[ ⟨ true ⟩ ] vs[⟨⟩] ⟩
    var vz [ ⟨ true ⟩ ]
    ≡⟨ vz[⟨⟩] ⟩
    true ∎

  task-eq : (ite v1 (v0 +o v0) v0) [ ⟨ num 4 ⟩ ] [ ⟨ false ⟩ ] ≡ num {◇} 4
  task-eq =
    (ite v1 (v0 +o v0) v0) [ ⟨ num 4 ⟩ ] [ ⟨ false ⟩ ]
    ≡⟨ cong _[ ⟨ false ⟩ ] ite[] ⟩
    ite (var (vs vz) [ ⟨ num 4 ⟩ ]) 
        (var vz +o var vz [ ⟨ num 4 ⟩ ])
        (var vz [ ⟨ num 4 ⟩ ]) [ ⟨ false ⟩ ]
    ≡⟨ cong₂ (λ x y → ite x (var vz +o var vz [ ⟨ num 4 ⟩ ]) y [ ⟨ false ⟩ ])
      vs[⟨⟩] vz[⟨⟩] ⟩
    ite (var vz) (var vz +o var vz [ ⟨ num 4 ⟩ ]) (num 4) [ ⟨ false ⟩ ]
    ≡⟨ ite[] ⟩
    ite (var vz [ ⟨ false ⟩ ])
        (var vz +o var vz [ ⟨ num 4 ⟩ ] [ ⟨ false ⟩ ])
        (num 4 [ ⟨ false ⟩ ])
    ≡⟨ cong₂ (λ x y → ite x (var vz +o var vz [ ⟨ num 4 ⟩ ] [ ⟨ false ⟩ ]) y)
       vz[⟨⟩] num[] ⟩
    ite false (var vz +o var vz [ ⟨ num 4 ⟩ ] [ ⟨ false ⟩ ]) (num 4)
    ≡⟨ iteβ₂ ⟩
    num 4 ∎
  
  var-eq-4 : def (num 1) (v0 +o v0) ≡ num {◇} 2
  var-eq-4 =
    def (num 1) (v0 +o v0)
    ≡⟨ +[] ⟩
    (var vz [ ⟨ num 1 ⟩ ]) +o (var vz [ ⟨ num 1 ⟩ ])
    ≡⟨ cong₂ _+o_ vz[⟨⟩] vz[⟨⟩] ⟩
    num 1 +o num 1
    ≡⟨ +β ⟩
    num 2 ∎
  
  var-eq-5 : def false (def (num 2) (ite v1 (num 0) (num 1 +o v0))) ≡ num {◇} 3
  var-eq-5 =
    def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
    ≡⟨ cong _[ ⟨ false ⟩ ] ite[] ⟩
    ite (var (vs vz) [ ⟨ num 2 ⟩ ])
        (num zero [ ⟨ num 2 ⟩ ])
        (num 1 +o var vz [ ⟨ num 2 ⟩ ])
      [ ⟨ false ⟩ ]
    ≡⟨ cong₂ (λ x y → ite x (num zero [ ⟨ num 2 ⟩ ]) y [ ⟨ false ⟩ ]) vs[⟨⟩] +[] ⟩
    ite (var vz) 
        (num zero [ ⟨ num 2 ⟩ ])
        ((num 1 [ ⟨ num 2 ⟩ ]) +o (var vz [ ⟨ num 2 ⟩ ]))
      [ ⟨ false ⟩ ]
    ≡⟨ ite[] ⟩
    ite (var vz [ ⟨ false ⟩ ])
        (num zero [ ⟨ num 2 ⟩ ] [ ⟨ false ⟩ ])
        ((num 1 [ ⟨ num 2 ⟩ ]) +o (var vz [ ⟨ num 2 ⟩ ]) [ ⟨ false ⟩ ])
    ≡⟨ cong₂ (λ x y → ite x (num zero [ ⟨ num 2 ⟩ ] [ ⟨ false ⟩ ]) y) vz[⟨⟩] +[] ⟩
    ite false 
        (num zero [ ⟨ num 2 ⟩ ] [ ⟨ false ⟩ ])
        ((num 1 [ ⟨ num 2 ⟩ ] [ ⟨ false ⟩ ]) +o
          (var vz [ ⟨ num 2 ⟩ ] [ ⟨ false ⟩ ]))
    ≡⟨ iteβ₂ ⟩
    (num 1 [ ⟨ num 2 ⟩ ] [ ⟨ false ⟩ ]) +o (var vz [ ⟨ num 2 ⟩ ] [ ⟨ false ⟩ ])
    ≡⟨ cong₂ _+o_ (cong _[ ⟨ false ⟩ ] num[] ◾ num[]) (cong _[ ⟨ false ⟩ ] vz[⟨⟩] ◾ num[]) ⟩
    num 1 +o num 2
    ≡⟨ +β ⟩
    num 3 ∎
  
  var-eq-6 : def (num 0) (def (isZero v0) (ite v0 false true)) ≡ false {◇}
  var-eq-6 =
    def (num 0) (def (isZero v0) (ite v0 false true))
    ≡⟨ {!   !} ⟩
    false ∎
  
  var-eq-7 : def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0))) ≡ num {◇} 2
  var-eq-7 =
    def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
    ≡⟨ {!   !} ⟩
    num 2 ∎
  
  var-eq-8 : def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0))) ≡ false {◇}
  var-eq-8 =
    def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0)))
    ≡⟨ {!    !} ⟩
    false ∎
  
  var-eq-9 : def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0))) ≡ num {◇} 2
  var-eq-9 =
    def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0)))
    ≡⟨ {!   !} ⟩
    num 2 ∎