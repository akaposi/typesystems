{-# OPTIONS --prop --rewriting #-}

module gy05-1 where

open import Lib

module type-inference where

  import RazorAST
  open import RazorWT
  open RazorWT.I

  -- tipuskikovetkeztetes primitiv hibauzenetekkel

  data Result (A : Set) : Set where
    Ok : (r : A) → Result A
    Err : (e : ℕ) → Result A

  {-
    Error codes:
    · 1: Non boolean condition
    · 2: Differing branch types
    · 3: Non numeric isZero parameter
    · 4: Non numeric addition parameter
  -}
  
  Infer : RazorAST.Model {lzero}
  Infer = record
    { Tm      = Result (Σ Ty (λ A → Tm A)) -- WT-beli Ty, WT-beli Tm
    ; true    = Ok (Bool , true)
    ; false   = Ok (Bool , false)
    ; ite     = λ where (Ok (Nat , snd)) t f → Err 1
                        (Ok (Bool , b  )) (Ok (Nat , t)) (Ok (Nat , f)) → Ok (Nat , ite b t f)
                        (Ok (Bool , snd)) (Ok (Nat , _)) (Ok (Bool , _)) → Err 2
                        (Ok (Bool , snd)) (Ok (Bool , _)) (Ok (Nat , _)) → Err 2
                        (Ok (Bool , b )) (Ok (Bool , t)) (Ok (Bool , f)) → Ok (Bool , ite b t f)
                        (Ok (Bool , snd)) (Ok r) (Err e) → Err e
                        (Ok (Bool , snd)) (Err e) (Ok r) → Err e
                        (Ok (Bool , snd)) (Err e) (Err e₁) → Err e
                        (Err e) t f → Err e
    ; num     = λ n → Ok (Nat , num n)
    ; isZero  = λ where (Ok (Nat , n)) → Ok (Bool , isZero n)
                        (Ok (Bool , b)) → Err 3
                        (Err e) → Err e
    ; _+o_    = λ where (Ok (Nat , n)) (Ok (Nat , m)) → Ok (Nat , n +o m)
                        (Ok (Nat , _)) (Ok (Bool , _)) → Err 4
                        (Ok (Bool , _)) (Ok (Nat , _)) → Err 4
                        (Ok (Bool , _)) (Ok (Bool , _)) → Err 4
                        (Ok r) (Err e) → Err e
                        (Err e) (Ok r) → Err e
                        (Err e) (Err e₁) → Err e
    }

  module INF = RazorAST.Model Infer

  inf-test : Result (Σ Ty (λ T → Tm T))
  inf-test = {! INF.⟦ I'.isZero (I'.num 9) ⟧  !} -- itt lehet tesztelni
    where open RazorAST.I

  -- Standard model

  Standard : Model {lsuc lzero} {lzero}
  Standard = record
    { Ty      = Set
    ; Tm      = λ A → A
    ; Nat     = ℕ
    ; Bool    = 𝟚
    ; true    = tt -- true : Tm Bool -> true : Bool -> true : 𝟚
    ; false   = ff
    ; ite     = if_then_else_
    ; num     = λ n → n
    ; isZero  = λ where zero → tt
                        (suc n) → ff
    ; _+o_    = _+_
    }
  module STD = Model Standard

  eval : {A : Ty} → Tm A → STD.⟦ A ⟧T
  eval t = STD.⟦ t ⟧t

  typeOfINF : Result (Σ Ty (λ A → Tm A)) → Set
  typeOfINF (Ok (t , _)) = STD.⟦ t ⟧T
  typeOfINF (Err e) = Lift ⊥

  run : (t : RazorAST.I.Tm) → Result (typeOfINF INF.⟦ t ⟧)
  run t with INF.⟦ t ⟧
  ... | Ok (_ , tm) = Ok (STD.⟦ tm ⟧t)
  ... | Err e = Err e
  
-- WT-ben:
-- isZero (num 0) ≟ true
-- I.isZero (I.num 0) ≠ I.true
-- I.isZero (I.num 0) ≠ I.false

-- height modell definiálható WT-ben.

module NatBool-with-equational-theory where

  open import Razor
  open I

  eq-0 : true ≡ true
  eq-0 = refl

  eq-1 : isZero (num 0) ≡ true
  eq-1 = isZeroβ₁

  eq-1' : true ≡ isZero (num 0)
  eq-1' = isZeroβ₁ ⁻¹ -- \^- = ⁻ ; \^1 = ¹

  eq-2 : isZero (num 3 +o num 1) ≡ isZero (num 4)
  eq-2 = cong isZero +β

  eq-3 : isZero (num 4) ≡ false
  eq-3 = isZeroβ₂

  eq-4 : isZero (num 3 +o num 1) ≡ false
  eq-4 = eq-2 ◾ eq-3

  eq-4' : isZero (num 3 +o num 1) ≡ false
  eq-4' =
    isZero (num 3 +o num 1)
      ≡⟨ eq-2 ⟩ -- \== \< \>
    isZero (num 4)
      ≡⟨ eq-3 ⟩
    false
      ∎ -- \qed

  eq-5 : ite false (num 2) (num 5) ≡ num 5
  eq-5 = iteβ₂

  eq-6 : ite true (isZero (num 0)) false ≡ true
  eq-6 = iteβ₁ ◾ isZeroβ₁

  eq-6' : ite true (isZero (num 0)) false ≡ true
  eq-6' =
    ite true (isZero (num 0)) false
      ≡⟨ iteβ₁ ⟩
    isZero (num 0)
      ≡⟨ isZeroβ₁ ⟩
    true
      ∎

  eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
  eq-7 =
    ((num 3 +o num 0) +o num 1)
      ≡⟨ cong (_+o num 1) +β ⟩
    (num 3 +o num 1)
      ≡⟨ +β ⟩
    num 4
      ∎

  eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
  eq-8 =
    ite (isZero (num 0)) (num 1 +o num 1) (num 0)
      ≡⟨ cong (λ x → ite x (num 1 +o num 1) (num 0)) isZeroβ₁ ⟩
    ite true (num 1 +o num 1) (num zero)
      ≡⟨ iteβ₁ ⟩
    (num 1 +o num 1)
      ≡⟨ +β ⟩
    num 2
      ∎

  eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  eq-9 =
    (num 3 +o ite (isZero (num 2)) (num 1) (num 0))
      ≡⟨ cong (λ x → num 3 +o ite x (num 1) (num 0)) isZeroβ₂ ⟩
    (num 3 +o ite false (num 1) (num zero))
      ≡⟨ cong (num 3 +o_) iteβ₂ ⟩
    (num 3 +o num 0)
      ≡⟨ +β ⟩
    num 3
      ∎

  eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  eq-10 =
    (ite false (num 1 +o num 1) (num 0) +o num 0)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 0
      ∎

  eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  eq-11 =
    ite (isZero (num 0 +o num 1)) false (isZero (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    true
      ∎

  eq-12 : num 3 +o num 2 ≡ ite true (num 5) (num 1)
  eq-12 =
    num 3 +o num 2
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    ite true (num 5) (num 1)
      ∎ 