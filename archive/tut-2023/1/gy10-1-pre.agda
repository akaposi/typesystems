{-# OPTIONS --prop --rewriting #-}

module gy10-1 where

open import Lib
open import STT.Syntax
open import STT

add2 : ?
add2 = ? -- λx.x+2

and : ?
and = ? -- λx y.if x then y else false

and-test1 : and $ true $ true ≡ true
and-test1 = ?
  
and-test2 : and $ false $ true ≡ false
and-test2 = ?

-- External and
and' : Tm ◇ Bool → Tm ◇ Bool → Tm ◇ Bool
and' = ?

and'-test1 : and' true true ≡ true
and'-test1 = {!!}

and'-test2 : and' false true ≡ false
and'-test2 = {!!}

neg : Tm ◇ (Bool ⇒ Bool)
neg = ?

neg-test1 : neg $ true ≡ false
neg-test1 = {!!}

neg-test2 : neg $ false ≡ true
neg-test2 = {!!}

infixr 50 _⊚_
-- external definition
_⊚_ : {Γ : Con}{A B C : Ty} → Tm Γ (B ⇒ C) → Tm Γ (A ⇒ B) → Tm Γ (A ⇒ C)
f ⊚ g = {!   !}

-- internal definition
∘o : {Γ : Con}{A B C : Ty} → Tm Γ ((B ⇒ C) ⇒ (A ⇒ B) ⇒ (A ⇒ C))
∘o = {!   !}

-- Define generic internalization and externalization!

extFun : Con → Ty → Ty → Set
extFun Γ A B = Tm Γ A → Tm Γ B

intFun : Con → Ty → Ty → Set
intFun Γ A B = Tm Γ (A ⇒ B)

internalize : {Γ : Con}{A B : Ty} → ({Δ : Con} → extFun Δ A B) → intFun Γ A B
internalize f = {!   !}

externalize : {Γ : Con}{A B : Ty} → ({Δ : Con} → intFun Δ A B) → extFun Γ A B
externalize f a = {!   !}

-- Prove the elimination rules of the internalized `isZero`!

isZeroInternal : {Γ : Con} → Tm Γ (Nat ⇒ Bool)
isZeroInternal = {!   !}

isZero'β₁ : {Γ : Con} → isZeroInternal $ num 0 ≡ true {Γ}
isZero'β₁ = 
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !} ∎

isZero'β₂ : {Γ : Con}{n : ℕ} → isZeroInternal $ num (1 + n) ≡ false {Γ}
isZero'β₂ {n = n} =
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !}
  ≡⟨ {!   !} ⟩
  {!   !} ∎