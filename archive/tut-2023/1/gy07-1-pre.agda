{-# OPTIONS --prop --rewriting #-}

module gy07-1 where

open import Lib

module DefABT where
  open import DefABT

  open I

  v : (n : ℕ) → ∀{m} → Tm (suc n + m)
  v n = var (v' n) where
    v' : (n : ℕ) → ∀{m} → Var (suc n + m)
    v' zero = vz
    v' (suc n) = vs (v' n)


  -- (let x:=num 1 in x) +o let y:=num 1 in y +o let z:=x +o y in (x +o z) +o (y +o x)
  tm-2 : Tm {!   !}
  tm-2 = {!   !}

module DefWT where

  open import DefWT
  open I
  -- ◇ \di2     ▹  \t6
  tm-0 : {!   !}
  tm-0 = isZero (ite v2 (v1 +o v0) v1)

  tm-1 : {!   !}
  tm-1 = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  tm-2 : {!   !}
  tm-2 = ite v1 (isZero v0) (isZero v2)

  tm-3 : {!   !}
  tm-3 = ite (ite v2 (isZero v0) v2) v1 v1

  tm-4 : {!   !}
  tm-4 = ite v1 (ite v1 v2 v2) (isZero v0)

  tm-5 : {!   !}
  tm-5 = def (v1 +o num 10) (ite (isZero v0) (v2 +o num 20) v0)

  tm-6 : {!   !}
  tm-6 = isZero (def (isZero v1) (ite v0 v1 v2))

  tm-7 : {!   !}
  tm-7 = v1 +o def (def (num 2 +o v0) (v0 +o v1 +o v2)) (v2 +o v3) +o num 10

  tm-8 : {!   !}
  tm-8 = def (v1 +o num 10) (ite (isZero v0) (def (isZero v1) (v2 +o v1 +o v4)) (def v0 (v0 +o v1 +o v2) +o v3))