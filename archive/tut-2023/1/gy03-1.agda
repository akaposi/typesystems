{-# OPTIONS --prop --rewriting #-}

module gy03-1 where

open import Lib
open import RazorAST hiding (D)

-- mutasd meg, hogy true ≠ isZero (num 0) a szintaxisban! ehhez adj
-- meg egy modellt, amiben a true tt-re, az isZero (num 0) ff-re
-- ertekelodik!
TN : Model {lzero}
TN = record
  { Tm = 𝟚
  ; true = tt
  ; false = ff
  ; ite = λ _ _ _ → ff
  ; num = λ _ → ff
  ; isZero = λ _ → ff
  ; _+o_ = λ _ _ → ff
  }
true≠isZeronum0 : ¬ (I.true ≡ I.isZero (I.num 0))
true≠isZeronum0 e = tt≠ff (cong TN.⟦_⟧ e) -- \neq = ≠
  where module TN = Model TN

-- nemsztenderd modell (a szintaxis ertelmezese nem rakepzes)
NS : Model {lzero}
NS = record
       { Tm = ℕ
       ; true = 2
       ; false = 0
       ; ite = λ { zero a b → b ; (suc _) a b → a }
       ; num = λ n → n + n
       ; isZero = λ { zero → 2 ; (suc _) → 0 }
       ; _+o_ = _+_
       }
module testNS where
  module NS = Model NS

  -- adj meg egy ℕ-t, amire nem kepez egyik term sem
  d : ℕ
  d = 1

  -- bizonyitsd be, hogy minden szintaktikus term ertelmezese paros szam
  ps : (t : I.Tm) → Σsp ℕ λ m → NS.⟦ t ⟧ ≡ m + m
  ps I.true = 1 , refl
  ps I.false = 0 , refl
  ps (I.ite b t f) = {!   !}
  ps (I.num n) = n , refl
  ps (I.isZero t) with ps t
  ... | zero , p = 1 , cong NS.isZero p
  ... | suc n , p = 0 , cong NS.isZero p
  ps (x I.+o y) with ps x | ps y
  ... | n | m = {!   !}

-- FEL: add meg a legegyszerubb nemsztenderd modellt!
-- 
NS' : Model {lzero}
NS' = record
  { Tm = 𝟚 -- Lift ⊤
  ; true = ff
  ; false = ff
  ; ite = λ _ _ _ → ff
  ; num = λ _ → ff
  ; isZero = λ _ → ff
  ; _+o_ = λ _ _ → ff
  }
module testNS' where
  module NS' = Model NS'
  b : 𝟚
  b = tt

  -- indukcio
  D : DepModel {lzero}
  D = record
    { Tm∙ = λ t → Lift (NS'.⟦ t ⟧ ≡ ff)
    ; true∙ = mk refl
    ; false∙ = mk refl
    ; ite∙ = λ _ _ _ → mk refl
    ; num∙ = λ _ → mk refl
    ; isZero∙ = λ _ → mk refl
    ; _+o∙_ = λ _ _ → mk refl
    }
  module D = DepModel D
  
  ∀ff : (t : I.Tm) → NS'.⟦ t ⟧ ≡ ff
  ∀ff t = un D.⟦ t ⟧
  
  ns : (Σsp I.Tm λ t → NS'.⟦ t ⟧ ≡ tt) → ⊥
  ns e = tt≠ff (π₂ e ⁻¹ ◾ ∀ff (π₁ e))

-- FEL: product models
Prod : ∀{i j} → Model {i} → Model {j} → Model {i ⊔ j}
Prod M N = record
  { Tm = M.Tm × N.Tm
  ; true = {!!}
  ; false = {!!}
  ; ite = {!!}
  ; num = {!!}
  ; isZero = {!!}
  ; _+o_ = {!!}
  }
  where
    module M = Model M
    module N = Model N

L1' : Model
L1' = record
  { Tm     = ℕ
  ; true   = 1
  ; false  = 1
  ; ite    = λ t t' t'' → t + t' + t''
  ; num    = λ _ → 1
  ; isZero = λ t → t
  ; _+o_   = _+_
  }
module L1' = Model L1'
L2' : Model
L2' = record
  { Tm     = ℕ
  ; true   = 0
  ; false  = 0
  ; ite    = λ t t' t'' → 2 + t + t' + t''
  ; num    = λ _ → 0
  ; isZero = λ t → t
  ; _+o_   = λ t t' → 1 + t + t'
  }
module L2' = Model L2'

L1L2' : DepModel {lzero}
L1L2' = record
  { Tm∙ = λ t → Lift (L1'.⟦ t ⟧ ≡ suc L2'.⟦ t ⟧)
  ; true∙ = mk refl
  ; false∙ = mk refl
  ; ite∙ = {!   !}
  ; num∙ = λ _ → mk refl
  ; isZero∙ = λ x → x
  -- \== = ≡ ; \< = ⟨ ; \> = ⟩ ; \qed = ∎
  ; _+o∙_ = λ where {u} (mk p1) {v} (mk p2) → mk (
                     {!   !} 
                     ≡⟨ {!   !} ⟩
                     {!   !} ∎)
  }
module L1L2' = DepModel L1L2'

twolengths : ∀ t → L1'.⟦ t ⟧ ≡ suc L2'.⟦ t ⟧
twolengths t = {!!}
