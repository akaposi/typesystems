{-# OPTIONS --prop --rewriting #-}

module hf06-1 where

open import Lib
open import DefABT

open I

-- GYAK FÁJLBAN
-- (let x:=num 1 in x +o x)
tm-0 : Tm 0
tm-0 = ?

-- GYAK FÁJLBAN
-- let x:=num 1 in 
--  x +o let y:=x +o num 1 in
--    y +o let z:=x +o y in
--      (x +o z) +o (y +o x)
tm-1 : Tm 0
tm-1 = ?

-- GYAK FÁJLBAN
-- (let x:=num 1 in 
--      x +o let y:=x +o num 1 in x) +o 
--    let z:=num 1 in z +o z
tm-3 : Tm 0
tm-3 = ?

-- GYAK FÁJLBAN
-- ((let x:=num 1 in x) +o (let y:=num 1 in y)) +o let z:=num 1 in z +o z
tm-4 : Tm 0
tm-4 = ?

-- GYAK FÁJLBAN
-- let x:=(isZero true) in (ite x 0 x)
tm-5 : Tm 0
tm-5 = ?

-- GYAK FÁJLBAN
-- let x:= 1+2 in (x+x) + let y:=3+4 in y + y
t-1 : Tm 0
t-1 = ?

-- GYAK FÁJLBAN
-- let x:=1+2 in (x+x) + let y:=3+4 in x + y
t-1' : Tm 0
t-1' = ?

-- GYAK FÁJLBAN
-- let x:=true in x + let y:=x in y+x
t-2 : Tm 0
t-2 = ?

-- GYAK FÁJLBAN
-- let x:=true in let y:=false in ite y y x
t-3 : Tm 0
t-3 = ?

-- GYAK FÁJLBAN
-- true + let x:= true in false + let y:=x in x+y
t-4 : Tm 0
t-4 = ?

-- GYAK FÁJLBAN
--  let x:=true in let y:=false in let z:=true in let w:=false in (w +o z) +o (y +o x)
t-5 : Tm 0
t-5 = ?