{-# OPTIONS --prop --rewriting #-}

{-
bitbucket.org/akaposi/typesystems

Követelmények:
ZH nincs!
+/- van: 12 db, 0-1 pont, ebből lesz gyakorlati jegy.

6 - 7.4999...  : 2
7.5 - 8.999... : 3
9 - 10.499...  : 4
10.5 - 12      : 5

Hiányzás max. 3.

Beadandó: nincs!
Házi feladatok: nem kötelező, gyakorlásra szolgálnak.
-}
{-
Emacs key bindings (C = Ctrl, M = Alt):
C-x C-f : create or open a file
C-x C-w : save (write) file
C-x C-c : close Emacs
C-space : start selecting text
M-w : Copy
C-w : Cut
C-y : Paste
Agda-mode key bindings:
C-\       : Switch Agda mode on-off
C-c C-l   : Typecheck
C-c C-n   : Normalise (use definitions as much as possible)
C-c C-d   : Deduce type
C-c C-,   : Goal type and context (variants: C-u C-u C-c C-, and C-u C-c C-, where you normalise or do not normalise the goal at all)
C-c C-.   : Goal type and context + inferred type of current expr
C-c C-SPC : Fill goal
C-c C-x = : Describe character at point
-}
module gy01-1 where

open import Lib

-- Booleans

-- data 𝟚 : Set where
--   tt ff : 𝟚

-- \b2 = 𝟚
example-bool : 𝟚
example-bool = tt

another-bool : 𝟚
another-bool = ff

-- idb

idb : 𝟚 → 𝟚
idb x = x

-- \== = ≡
idb-test-1 : idb tt ≡ tt
idb-test-1 = refl

idb-test-2 : idb ff ≡ ff
idb-test-2 = refl

-- \rightarrow = → 
idb' : 𝟚 → 𝟚
idb' ff = ff
idb' tt = tt

-- \lambda = \Gl = λ
idb-test-3 : (λ x → idb x) ≡ (λ x → x)
idb-test-3 = refl

{-
-- Ezt nem lehet refl-lel bizonyítani
idb'-test : (λ x → idb' x) ≡ (λ x → x)
idb'-test = {!  !}
-}

-- neg
-- mintaillesztés: C-c C-c
neg : 𝟚 → 𝟚
neg ff = tt
neg tt = ff

neg-test-1 : neg tt ≡ ff
neg-test-1 = refl

neg-test-2 : neg ff ≡ tt
neg-test-2 = refl

-- How many 𝟚 → 𝟚 functions are there?
-- Van 2 lehetséges bemenetem és minden bemenethez 2 lehetséges kimenet
-- 2² = 4
-- or

or : 𝟚 → 𝟚 → 𝟚
or ff b = b
or tt b = tt

or-test-1 : or tt tt ≡ tt
or-test-1 = refl

or-test-2 : or tt ff ≡ tt
or-test-2 = refl

or-test-3 : or ff tt ≡ tt
or-test-3 = refl

or-test-4 : or ff ff ≡ ff
or-test-4 = refl

-- and
-- C-c C-r : refine
-- mintaillesztés lambdában where-rel:
and : 𝟚 → 𝟚 → 𝟚
and = λ where ff b → ff
              tt b → b

and-test-1 : and tt tt ≡ tt
and-test-1 = refl

and-test-2 : and tt ff ≡ ff
and-test-2 = refl

and-test-3 : and ff tt ≡ ff
and-test-3 = refl

and-test-4 : and ff ff ≡ ff
and-test-4 = refl

-- xor
-- mintaillesztés kapcsos zárójellel:
xor : 𝟚 → 𝟚 → 𝟚
xor = λ {ff b → b
       ; tt b → neg b}

xor-test-1 : xor tt tt ≡ ff
xor-test-1 = refl

xor-test-2 : xor tt ff ≡ tt
xor-test-2 = refl

xor-test-3 : xor ff tt ≡ tt
xor-test-3 = refl

xor-test-4 : xor ff ff ≡ ff
xor-test-4 = refl

-- Natural numbers
{-
data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ
-}

-- 2 = suc (suc zero)
-- \bN = ℕ
addTwo : ℕ → ℕ
addTwo = λ x → suc (suc x)

addTwo-test-1 : addTwo 0 ≡ 2
addTwo-test-1 = refl

addTwo-test-2 : addTwo 3 ≡ 5
addTwo-test-2 = refl

if'_then_else_ : {A : Set} → 𝟚 → A → A → A
if' ff then t else f = f
if' tt then t else f = t

_*2 : ℕ → ℕ
zero *2 = 0
suc n *2 = addTwo (n *2)

_*2+1 : ℕ → ℕ
n *2+1 = suc (n *2)

*2+1-test-1 : 3 *2+1 ≡ 7
*2+1-test-1 = refl

plus : ℕ → ℕ → ℕ
plus zero y = y
plus (suc x) y = suc (plus x y)

plus-idl : (n : ℕ) → plus 0 n ≡ n
plus-idl n = refl

plus-idr : (n : ℕ) → plus n 0 ≡ n
plus-idr zero = refl
plus-idr (suc n) = cong suc (plus-idr n)

-- 𝟚 → 𝟚 → 𝟚 Hány darab ilyen függvény van?
--     ₂
-- (2²)  = 16
plus' : ℕ → ℕ → ℕ
plus' x y = {!   !}

-- NatBool nyelv AST szinten
{-
open import NatBoolAST

t1 : I.Tm
t1 = {!!}

t1-test : height t1 ≡ 3
t1-test = refl

t2 : I.Tm
t2 = {!!}

t2-test-1 : height t2 ≡ 3
t2-test-1 = refl

t2-test-2 : ¬ (t1 ≡ t2)
t2-test-2 ()

-- ird le ezeket data-val!

-- T ::= op0 | op1 T | op2 T T | op3 T T T | op4 T T T T

-- A ::= a | fb B
-- B ::= fa A

-- V ::= vzero | vsuc V
-- E ::= num N | E < E | E = E | var V
-- C ::= V := E | while E S | if E then S else S
-- S ::= empty | C colon S

-- kovetkezo: NatBoolAST modelleket megadni.
-}