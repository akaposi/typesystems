{-# OPTIONS --prop --rewriting #-}

module gy10-5 where

open import Lib

open import STT hiding (and; or; neg; isZero'; add'; ite'; id; comp; idl; idr; ass)
open import STT.Syntax

and : ∀ {Γ} → Tm Γ (Bool ⇒ Bool ⇒ Bool)
and = lam (lam (ite v1 v0 false))

and-true-true : and {◇} $ true $ true ≡ true -- \diw or \di2
and-true-true =
  and $ true $ true
    ≡⟨ refl ⟩
  (lam (lam (ite v1 v0 false)) $ true) $ true
  -- (λ x. λ y. ite x y false) $ true $ true
    ≡⟨ cong (λ x → x $ true) ⇒β ⟩
  (lam (ite v1 v0 false)) [ ⟨ true ⟩ ] $ true
    ≡⟨ cong (λ x → x $ true) lam[] ⟩
  lam (ite v1 v0 false [ ⟨ true ⟩ ⁺ ]) $ true
    ≡⟨ cong (λ x → lam x $ true) ite[] ⟩
  lam (ite (v1 [ ⟨ true ⟩ ⁺ ]) (v0 [ ⟨ true ⟩ ⁺ ]) (false [ ⟨ true ⟩ ⁺ ])) $ true
    ≡⟨ cong₃ (λ x y z → lam (ite x y z) $ true) vs[⁺] vz[⁺] false[] ⟩
  lam (ite (v0 [ ⟨ true ⟩ ] [ p ]) v0 false) $ true
    ≡⟨ cong (λ x → lam (ite (x [ p ]) v0 false) $ true) vz[⟨⟩] ⟩
  lam (ite (true [ p ]) v0 false) $ true
    ≡⟨ cong (λ x → lam (ite x v0 false) $ true) true[] ⟩
  lam (ite true v0 false) $ true
  -- (λ y. ite true y false) $ true
    ≡⟨ cong (λ x → lam x $ true) iteβ₁ ⟩
  lam v0 $ true
  -- (λ y. y) $ true
    ≡⟨ ⇒β ⟩
  v0 [ ⟨ true ⟩ ]
    ≡⟨ vz[⟨⟩] ⟩
  true
    ∎

-- Exercise 3.3 (compulsory). Prove
apply2x-ex : apply2x $ add2 $ num 3 ≡ num 7
apply2x-ex =
  apply2x $ add2 $ num 3
    ≡⟨ refl ⟩
  (lam (lam (v1 $ (v1 $ v0))) $ lam (v0 +o num 2)) $ num 3
  -- (λ x. λ y. x $ (x $ y)) $ (λ z. z +o num 2) $ num 3
    ≡⟨ cong (λ x → x $ num 3) ⇒β ⟩
  lam (v1 $ (v1 $ v0)) [ ⟨ lam (v0 +o num 2) ⟩ ] $ num 3
    ≡⟨ cong (λ x → x $ num 3) lam[] ⟩
  lam ((v1 $ (v1 $ v0)) [ ⟨ lam (v0 +o num 2) ⟩ ⁺ ]) $ num 3
    ≡⟨ cong (λ x → lam x $ num 3) $[] ⟩
  lam (v1 [ ⟨ lam (v0 +o num 2) ⟩ ⁺ ] $ (v1 $ v0) [ ⟨ lam (v0 +o num 2) ⟩ ⁺ ]) $ num 3
    ≡⟨ cong (λ x → lam (v1 [ ⟨ lam (v0 +o num 2) ⟩ ⁺ ] $ x) $ num 3) $[] ⟩
  lam
    ( v1 [ ⟨ lam (v0 +o num 2) ⟩ ⁺ ]
      $ (v1 [ ⟨ lam (v0 +o num 2) ⟩ ⁺ ] $ v0 [ ⟨ lam (v0 +o num 2) ⟩ ⁺ ]))
    $ num 3
    ≡⟨
      cong₂ (λ (x : Tm _ (Nat ⇒ Nat)) y → lam (x $ (x $ y)) $ num 3)
        ( vs[⁺]
        ◾ cong (λ x → x [ p ]) vz[⟨⟩]
        ◾ lam[]
        ◾ cong lam +[]
        ◾ cong₂ (λ x y → lam (x +o y)) vz[⁺] num[])
        vz[⁺] ⟩
  lam ((lam (v0 +o num 2)) $ (lam (v0 +o num 2) $ v0)) $ num 3
  -- (λ y. (λ z. z +o num 2) $ ((λ w. w +o num 2) $ y)) $ num 3
    ≡⟨ {!   !} ⟩
  (lam (v0 +o num 2)) $ (lam (v0 +o num 2) $ num 3)
  -- (λ z. z +o num 2) $ ((λ w. w +o num 2) $ num 3)
    ≡⟨ {!   !} ⟩
  (lam (v0 +o num 2)) $ (num 3 +o num 2)
  -- (λ z. z +o num 2) $ (num 3 +o num 2)
    ≡⟨ {!   !} ⟩
  (lam (v0 +o num 2)) $ num 5
  -- (λ z. z +o num 2) $ num 5
    ≡⟨ {!   !} ⟩
  num 5 +o num 2
    ≡⟨ {!   !} ⟩
  num 7
    ∎

-- Exercise 3.5 (compulsory). Define the identity function and function
-- composition, and show the following equations relating them.
id : ∀{Γ A} → Tm Γ (A ⇒ A)
id = lam v0

comp : ∀{Γ A B C} → Tm Γ ((B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
comp = lam (lam (lam (v2 $ (v1 $ v0))))

idl : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → comp $ id $ t ≡ t
idl {t = t} =
  comp $ id $ t
    ≡⟨ refl ⟩
  (lam (lam (lam (v2 $ (v1 $ v0)))) $ lam v0) $ t
    ≡⟨ {!   !} ⟩
  lam (lam (lam v0 $ (v1 $ v0))) $ t
    ≡⟨ {!   !} ⟩
  lam (lam v0 $ (t [ p ] $ v0))
    ≡⟨ {!   !} ⟩
  lam (t [ p ] $ v0) -- λ x. t $ x    -- (λ x. t $ x) $ a = t $ a
    ≡⟨ ⇒η ⁻¹ ⟩
  t -- t $ a
    ∎

-- Exercise 3.8 (compulsory). How many different elements does
-- Tm ⋄ (Bool ⇒ Bool) have? Define them!
{-
lam v0
lam (ite v0 false true)
lam true
lam false

lam (ite v0 true false)
lam (ite (ite v0 true false) true false)
-}

-- Exercise 3.9 (compulsory).
-- Compare the sets Tm ⋄ (Bool ⇒ Bool) and (Tm ⋄ Bool → Tm ⋄ Bool).
{-
f : Tm ◇ Bool → Tm ◇ Bool
f x = x

f x = ite x false true

f x = true

f x = false
-}

-- Exercise 3.10 (compulsory). Internalise isZero, that is, define isZero' as
-- below. Similarly, internalise the functions _+o_ and ite.
isZero' : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
isZero' = lam (isZero v0)

add' : ∀{Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
add' = lam (lam (v1 +o v0))

ite' : ∀{Γ A} → Tm Γ (Bool ⇒ A ⇒ A ⇒ A)
ite' = lam (lam (lam (ite v2 v1 v0)))

-- Externalise `neg` and `and`.
neg : ∀ {Γ} → Tm Γ (Bool ⇒ Bool)
neg = lam (ite v0 false true)

neg' : ∀{Γ} → Tm Γ Bool → Tm Γ Bool
neg' x = neg $ x

{-
and : ∀ {Γ} → Tm Γ (Bool ⇒ Bool ⇒ Bool)
and = lam (lam (ite v1 v0 false))
-}

and' : ∀{Γ} → Tm Γ Bool → Tm Γ Bool → Tm Γ Bool
and' x y = and $ x $ y

-- Exercise 3.12. What are the normal form of the following terms?
t1' : Σsp (Nf (◇ ▹ Nat ⇒ Bool) (Nat ⇒ Bool)) λ v → ⌜ v ⌝Nf ≡ v0
t1' =
  lamNf (neuBool (varNe (vs vz) $Ne neuNat (varNe vz))) ,
  (cong (λ x → lam (x $ var vz)) ([p] ⁻¹) ◾ ⇒η ⁻¹)
