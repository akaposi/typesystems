{-# OPTIONS --prop --rewriting #-}

module gy10-5 where

open import Lib

open import STT hiding (and; or; neg; isZero'; add'; ite'; id; comp; idl; idr; ass)
open import STT.Syntax

neg : ∀ {Γ} → Tm Γ (Bool ⇒ Bool)
neg = lam (ite v0 false true)

and : ∀ {Γ} → Tm Γ (Bool ⇒ Bool ⇒ Bool)
and = lam (lam (ite v1 v0 false))

and-true-true : and $ true $ true ≡ true
and-true-true =
  and $ true $ true
    ≡⟨ {!   !} ⟩
  true
    ∎

-- Exercise 3.3 (compulsory). Prove
apply2x-ex : apply2x $ add2 $ num 3 ≡ num 7
apply2x-ex =
  apply2x $ add2 $ num 3
    ≡⟨ {!   !} ⟩
  num 7
    ∎

-- Exercise 3.5 (compulsory). Define the identity function and function
-- composition, and show the following equations relating them.
id : ∀{Γ A} → Tm Γ (A ⇒ A)
id = {!   !}

comp : ∀{Γ A B C} → Tm Γ ((B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
comp = {!   !}

idl : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → comp $ id $ t ≡ t
idl {t = t} =
  comp $ id $ t
    ≡⟨ {!   !} ⟩
  t
    ∎

-- Exercise 3.8 (compulsory). How many different elements does
-- Tm ⋄ (Bool ⇒ Bool) have? Define them!

-- Exercise 3.9 (compulsory).
-- Compare the sets Tm ⋄ (Bool ⇒ Bool) and (Tm ⋄ Bool → Tm ⋄ Bool).

-- Exercise 3.10 (compulsory). Internalise isZero, that is, define isZero' as
-- below. Similarly, internalise the functions _+o_ and ite.
isZero' : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
isZero' = {!   !}

add' : ∀{Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
add' = {!   !}

ite' : ∀{Γ A} → Tm Γ (Bool ⇒ A ⇒ A ⇒ A)
ite' = {!   !}

-- Externalise `neg` and `and`.
neg' : ∀{Γ} → Tm Γ Bool → Tm Γ Bool
neg' = {!   !}

and' : ∀{Γ} → Tm Γ Bool → Tm Γ Bool → Tm Γ Bool
and' = {!   !}
