{-# OPTIONS --prop --rewriting #-}

module gy04-5 where

open import Lib

module ZH where
  open import Nat

  double : ℕ → ℕ
  double zero = zero
  double (suc n) = suc (suc (double n))

  D : DepModel {lzero}
  D = record
    { Nat∙ = λ n → double n ≡ n + n
    ; Zero∙ = refl
    ; Suc∙ = λ {n} e →
      suc (suc (double n))
                            ≡⟨ cong (λ x → suc (suc x)) e ⟩
      suc (suc (n + n))
                            ≡⟨ cong suc (+suc ⁻¹) ⟩
      suc (n + suc n)
                            ∎
    }
  module D = DepModel D

  double=n+n : (n : ℕ) → double n ≡ n + n
  double=n+n n = D.⟦ n ⟧

---------------------------------------
-- RazorAST
---------------------------------------

module AST where
  open import RazorAST hiding (D)

  {-
  A szintaxisban (num 2 +o num 3) ≠ (num 3 +o num 2).
  Nincs olyan modell, melyben Tm az üres halmaz.
  Van olyan modell, melyben Tm-nek végtelen sok eleme van.
  Van olyan modell, melyben num 2 = num 3.
  Van olyan modell, melyben Tm-nek 6 eleme van.
  Van olyan M modell, melyre van olyan x : M.Tm, hogy nincs t : I.Tm, melyre M.⟦ t ⟧ = x.
  Van olyan modell, melyben true = false.
  Van olyan modell, melyben Tm-nek egy eleme van.
  -}

  M1 : Model {lzero}
  M1 = record
    { Tm = 𝟚
    ; true = ff
    ; false = ff
    ; ite = λ _ _ _ → ff
    ; num = λ n → n == 2
    ; isZero = λ _ → ff
    ; _+o_ = λ x y → x
    }
  module M1 = Model M1

  2+3≠3+2 : ¬ (I.num 2 I.+o I.num 3 ≡ I.num 3 I.+o I.num 2)
  2+3≠3+2 e = tt≠ff (cong (λ t → M1.⟦ t ⟧) e)

  noEmptyTm : (M : Model) → ¬ (Model.Tm M ≡ Lift ⊥)
  noEmptyTm M refl = un (Model.true M)

  module isZeroInjectivity where

    D : DepModel {lzero}
    D = record
      { Tm∙     = λ _ → I.Tm
      ; true∙   = I.true
      ; false∙  = I.true
      ; ite∙    = λ _ _ _ → I.true
      ; num∙    = λ _ → I.true
      ; isZero∙ = λ {t} _ → t
      ; _+o∙_   = λ _ _ → I.true
      }
    module D = DepModel D

    isZeroInj : ∀{t t'} → I.isZero t ≡ I.isZero t' → t ≡ t'
    isZeroInj e = cong (λ x → D.⟦ x ⟧) e

  module isZeroInjective where

    -- Az iniciális modellben most már tudjuk, hogy az isZero injektív. Adj meg egy másik modellt, amelyben szintén injektív!
    M : Model {lzero}
    M = record
      { Tm     = ℕ
      ; true   = zero
      ; false  = zero
      ; ite    = λ _ _ _ → zero
      ; num    = λ _ → zero
      ; isZero = suc
      ; _+o_   = λ _ _ → zero
      }
    open Model M

    inj : ∀ t t' → isZero t ≡ isZero t' → t ≡ t'
    inj t .t refl = refl

  module isZeroNotInjective where

    -- Adj meg egy modellt, amelyben az isZero operátor nem injektív!
    M : Model {lzero}
    M = record
      { Tm = 𝟚
      ; true = ff
      ; false = ff
      ; ite = λ _ _ _ → ff
      ; num = λ _ → ff
      ; isZero = λ _ → ff
      ; _+o_ = λ _ _ → ff
      }
    open Model M

    notInj : ¬ (∀ t t' → isZero t ≡ isZero t' → t ≡ t')
    notInj inj = tt≠ff (inj ff tt refl ⁻¹) -- \^- \^1

----------------------------------------------------
-- RazorWT
----------------------------------------------------

{-

Exercise 1.33 (compulsory). Draw the derivation trees of the following terms.
isZero (num 1 + ite true (num 2) (num 3))
isZero (num 1 + (num 2 + num 3))
isZero ((num 1 + num 2) + num 3)
ite (isZero (num 0)) (num 1 + num 2) (num 3)


Exercise 1.35 (compulsory). Here are some lists of lexical elements (written as strings). They
are all accepted by the parser. Which ones are rejected by type inference? For the ones which are
accepted, write down the RazorWT terms which are produced.
if true then true else num 0
if true then num 0 else num 0
if num 0 then num 0 else num 0
if num 0 then num 0 else true
true + zero
true + num 1
true + isZero false
true + isZero (num 0)


Exercise 1.36 (compulsory). Here are some RazorAST terms. Which ones are rejected by type
inference? For the ones which are accepted, write down the RazorWT terms which are produced.
ite true true (num 0)
ite true (num 0) (num 0)
ite (num 0) (num 0) (num 0)
true +o zero
true +o num 1
true +o isZero (num 1)
isZero (num 1) +o num 0
-}

-- experiment with type inference using infer in the code
module WT where
  open import RazorWT

  ex : I.Tm I.Nat
  ex = I.ite (I.isZero (I.num 3)) (I.num 3) (I.num 5)

---------------------------------------
-- Razor
---------------------------------------

module Eq where
  open import Razor

-- equational consistency: I.true ≠ I.false

  Std : Model
  Std = record
    { Ty = Set
    ; Tm = λ A → A
    ; Nat = ℕ
    ; Bool = 𝟚
    ; true = tt
    ; false = ff
    ; ite = λ b x y → if b then x else y
    ; num = λ n → n
    ; isZero = λ n → n == 0
    ; _+o_ = λ x y → x + y
    ; iteβ₁ = refl
    ; iteβ₂ = refl
    ; isZeroβ₁ = refl
    ; isZeroβ₂ = refl
    ; +β = refl
    }
  module Std = Model Std

  true≠false : ¬ (I.true ≡ I.false)
  true≠false e = tt≠ff (cong (λ n → Std.⟦ n ⟧t) e)
{-
Exercise 1.39 (compulsory). Show that if true = false in a model, then any two u, v : Tm Nat are
equal in that model. A consequence is that if a compiler compiles true and false to the same code,
then it compiles all natural numbers to the same code
-}
  p :
    (M : Model {lzero} {lzero}) →
    Model.true M ≡ Model.false M →
    (u v : Model.Tm M (Model.Nat M)) → u ≡ v
  p M e u v =
    u
                        ≡⟨ M.iteβ₁ ⁻¹ ⟩
    M.ite M.true u v
                        ≡⟨ cong (λ b → M.ite b u v) e ⟩
    M.ite M.false u v
                        ≡⟨ M.iteβ₂ ⟩
    v
                        ∎
    where module M = Model M

{-
In Razor.I, you cannot count the leaves!
-}
