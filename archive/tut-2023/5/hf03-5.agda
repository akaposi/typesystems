{-# OPTIONS --prop --rewriting #-}

module hf03-5 where

open import Lib hiding (idr+; ass+; +suc; +comm)
open import Nat hiding (M)

-- a *2+1 Nat modell
-- Definiáld azt a modellt, amely az "értelemszerűen" ábrázolt számot megszorozza 2-vel,
-- majd hozzáad 1-et.
M : Model {lzero}
M = record
  { Nat  = {!   !}
  ; Zero = {!   !}
  ; Suc  = {!   !}
  }
module M = Model M

-- néhány teszteset
testM : M.⟦ 3 ⟧ ≡ 3 * 2 + 1
testM = refl
testM' : M.⟦ 5 ⟧ ≡ 5 * 2 + 1
testM' = refl
testM'' : M.⟦ 10 ⟧ ≡ 21
testM'' = refl

-- Bizonyítsd be, hogy az ebbe a modellbe való kiértékelés tényleg beszoroz 2-vel és hozzáad egyet.
D : DepModel {lzero}
D = record
  { Nat∙ = {!   !}
  ; Zero∙ = {!   !}
  ; Suc∙ = {!   !}
  }
module D = DepModel D
M=*2+1 : (n : ℕ) → M.⟦ n ⟧ ≡ n * 2 + 1
M=*2+1 n = {!   !}

-- összeadás asszociatív
D'' : DepModel {lzero}
D'' = record
  { Nat∙ = λ m → ((n o : ℕ) → (m + n) + o ≡ m + (n + o))
  ; Zero∙ = λ n o → {!   !}
  ; Suc∙ = λ {m} e n o → {!   !}
  }
module D'' = DepModel D''
ass+ : (m n o : ℕ) → (m + n) + o ≡ m + (n + o)
ass+ m n o = D''.⟦ m ⟧ n o

-- suc a második tagból kiemelhető.
D''' : DepModel {lzero}
D''' = record
  { Nat∙ = λ m → (∀ n → m + suc n ≡ suc m + n)
  ; Zero∙ = {!   !}
  ; Suc∙ = {!   !}
  }
module D''' = DepModel D'''
+suc : (m n : ℕ) → m + suc n ≡ suc m + n
+suc m n = {!   !}

-- összeadás kommutatív
-- használd "idr+"-t, "+suc"-ot, "_⁻¹"-t
D'''' : DepModel
D'''' = record
  { Nat∙ = λ m → ∀ n → m + n ≡ n + m
  ; Zero∙ = {!   !}
  ; Suc∙ = λ {m} e n →
    suc (m + n)
                    ≡⟨ {!   !} ⟩
    suc (n + m)
                    ≡⟨ {!   !} ⟩
    suc n + m
                    ≡⟨ {!   !} ⟩
    n + suc m
                    ∎
  }
module D'''' = DepModel D''''
+comm : (m n : ℕ) → m + n ≡ n + m
+comm m n = {!   !}

-- valami egyenlőség
D''''' : DepModel {lzero}
D''''' = {!   !}
module D''''' = DepModel D'''''
+3=1++2 : (n : ℕ) → n + 3 ≡ 1 + n + 2
+3=1++2 n = {!   !}

D'''''' : DepModel {lzero}
D'''''' = {!   !}
module D'''''' = DepModel D''''''
*-distr-+ : (m n o : ℕ) → (m + n) * o ≡ (m * o) + (n * o)
*-distr-+ = {!   !}
