{-# OPTIONS --prop --rewriting #-}

module gy08-5 where

open import Lib

module Eqs where
  open import Def.Syntax

  t : Tm (◇ ▹ Bool) Nat
  t = ite v0 (num 1) (num 0)
  -- ite x (num 1) (num 0)

  γ : Sub ◇ (◇ ▹ Bool)
  γ = ⟨ true ⟩

  t[γ] : t [ γ ] ≡ num 1
  t[γ] =
    t [ γ ]
      ≡⟨ refl ⟩
    ite v0 (num 1) (num 0) [ ⟨ true ⟩ ]
      ≡⟨ ite[] ⟩
    ite (v0 [ ⟨ true ⟩ ]) (num 1 [ ⟨ true ⟩ ]) (num 0 [ ⟨ true ⟩ ])
      ≡⟨ refl ⟩
    ite (var vz [ ⟨ true ⟩ ]) (num 1 [ ⟨ true ⟩ ]) (num 0 [ ⟨ true ⟩ ])
      ≡⟨ cong (λ x → ite x (num 1 [ ⟨ true ⟩ ]) (num 0 [ ⟨ true ⟩ ])) vz[⟨⟩] ⟩
    ite true (num 1 [ ⟨ true ⟩ ]) (num 0 [ ⟨ true ⟩ ])
      ≡⟨ iteβ₁ ⟩
    num 1 [ ⟨ true ⟩ ]
      ≡⟨ num[] ⟩
    num 1
      ∎

  δ' : Sub (◇ ▹ Bool) (◇ ▹ Bool ▹ Nat)
  δ' = ⟨ ite v0 (num 1) (num 2) ⟩

  δ : Sub (◇ ▹ Bool ▹ Nat) (◇ ▹ Bool ▹ Nat ▹ Nat)
  δ = ⟨ num 2 ⟩

  u : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Nat
  u = v0 +o v1

  u[δ][δ'] : u [ δ ] [ δ' ] ≡ num 2 +o ite v0 (num 1) (num 2)
  u[δ][δ'] =
    u [ δ ] [ δ' ]
      ≡⟨ refl ⟩
    v0 +o v1 [ ⟨ num 2 ⟩ ] [ ⟨ ite v0 (num 1) (num 2) ⟩ ]
      ≡⟨ cong (λ x → x [ ⟨ ite v0 (num 1) (num 2) ⟩ ]) +[] ⟩
    (v0 [ ⟨ num 2 ⟩ ]) +o (v1 [ ⟨ num 2 ⟩ ]) [ ⟨ ite v0 (num 1) (num 2) ⟩ ]
      ≡⟨ cong (λ x → x +o (v1 [ ⟨ num 2 ⟩ ]) [ ⟨ ite v0 (num 1) (num 2) ⟩ ]) vz[⟨⟩] ⟩
    num 2 +o (v1 [ ⟨ num 2 ⟩ ]) [ ⟨ ite v0 (num 1) (num 2) ⟩ ]
      ≡⟨ cong (λ x → num 2 +o x [ ⟨ ite v0 (num 1) (num 2) ⟩ ]) vs[⟨⟩] ⟩
    num 2 +o v0 [ ⟨ ite v0 (num 1) (num 2) ⟩ ]
      ≡⟨ +[] ⟩
    (num 2 [ ⟨ ite v0 (num 1) (num 2) ⟩ ]) +o (v0 [ ⟨ ite v0 (num 1) (num 2) ⟩ ])
      ≡⟨ cong (λ x → x +o (v0 [ ⟨ ite v0 (num 1) (num 2) ⟩ ])) num[] ⟩
    num 2 +o (v0 [ ⟨ ite v0 (num 1) (num 2) ⟩ ])
      ≡⟨ cong (λ x → num 2 +o x) vz[⟨⟩] ⟩
    num 2 +o ite v0 (num 1) (num 2)
      ∎

  tm-1 : Tm ◇ Bool
  tm-1 = def true v0

  eq-1 : tm-1 ≡ true
  eq-1 =
    def true v0
      ≡⟨ refl ⟩
    v0 [ ⟨ true ⟩ ]
      ≡⟨ vz[⟨⟩] ⟩
    true
      ∎

  tm-2 : Tm ◇ Bool
  tm-2 = def true (def false v0)

  eq-2 : tm-2 ≡ false
  eq-2 =
    def true (def false v0)
      ≡⟨ refl ⟩
    v0 [ ⟨ false ⟩ ] [ ⟨ true ⟩ ]
      ≡⟨ cong (λ x → x [ ⟨ true ⟩ ]) vz[⟨⟩] ⟩
    false [ ⟨ true ⟩ ]
      ≡⟨ false[] ⟩
    false
      ∎

  tm-3 : Tm ◇ Bool
  tm-3 = def true (def false v1)

  eq-3 : tm-3 ≡ true
  eq-3 =
    def true (def false v1)
      ≡⟨ {!   !} ⟩
    true
      ∎

  tm-4 : Tm ◇ Nat
  tm-4 = def (num 4) (v0 +o v0)

  eq-4 : tm-4 ≡ num 8
  eq-4 = {!   !}

  tm-5 : Tm (◇ ▹ Nat) Nat
  tm-5 = def v0 (v0 +o v1)

  eq-5 : tm-5 ≡ v0 +o v0
  eq-5 = {!   !}

  tm-6 : Tm (◇ ▹ Bool ▹ Nat) Nat
  tm-6 = ite v1 (v0 +o v0) v0

  eq-6 : tm-6 [ {!   !} ] [ {!   !} ] ≡ num 2
  eq-6 = {!   !}

  tm-7 : Tm ◇ Nat
  tm-7 = def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))

  eq-7 : tm-7 ≡ num 3
  eq-7 = {!   !}

  tm-8 : Tm ◇ Nat
  tm-8 = def (def (num 1) (v0 +o num 2)) (def (num 2 +o v0) (v1 +o v0))

  eq-8 : tm-8 ≡ num 5
  eq-8 = {!   !}

  tm-9 : Tm ◇ Bool
  tm-9 = def (num 0) (def (isZero v0) (ite v0 false true))

  eq-9 : tm-9 ≡ false
  eq-9 = {!   !}

  tm-10 : Tm (◇ ▹ Nat ▹ Nat ▹ Nat) Nat
  tm-10 = v0 +o v1 +o v2

  eq-10 : tm-10 [ ⟨ num 1 ⟩ ⁺ ] ≡ v0 +o num 1 +o v1
  eq-10 =
    tm-10 [ ⟨ num 1 ⟩ ⁺ ]
      ≡⟨ refl ⟩
    v0 +o v1 +o v2 [ ⟨ num 1 ⟩ ⁺ ]
      ≡⟨ +[] ⟩
    (v0 +o v1 [ ⟨ num 1 ⟩ ⁺ ]) +o (v2 [ ⟨ num 1 ⟩ ⁺ ])
      ≡⟨ cong (λ x → x +o (v2 [ ⟨ num 1 ⟩ ⁺ ])) +[] ⟩
    (v0 [ ⟨ num 1 ⟩ ⁺ ]) +o (v1 [ ⟨ num 1 ⟩ ⁺ ]) +o (v2 [ ⟨ num 1 ⟩ ⁺ ])
      ≡⟨ cong (λ x → (x +o (v1 [ ⟨ num 1 ⟩ ⁺ ])) +o (v2 [ ⟨ num 1 ⟩ ⁺ ])) vz[⁺] ⟩
    v0 +o (v1 [ ⟨ num 1 ⟩ ⁺ ]) +o (v2 [ ⟨ num 1 ⟩ ⁺ ])
      ≡⟨ cong (λ x → v0 +o x +o (v2 [ ⟨ num 1 ⟩ ⁺ ])) vs[⁺] ⟩
    v0 +o (v0 [ ⟨ num 1 ⟩ ] [ p ]) +o (v2 [ ⟨ num 1 ⟩ ⁺ ])
      ≡⟨ cong (λ x → v0 +o (x [ p ]) +o (v2 [ ⟨ num 1 ⟩ ⁺ ])) vz[⟨⟩] ⟩
    v0 +o (num 1 [ p ]) +o (v2 [ ⟨ num 1 ⟩ ⁺ ])
      ≡⟨ cong (λ x → v0 +o x +o (v2 [ ⟨ num 1 ⟩ ⁺ ])) num[] ⟩
    v0 +o num 1 +o (v2 [ ⟨ num 1 ⟩ ⁺ ])
      ≡⟨ cong (λ x → v0 +o num 1 +o x) vs[⁺] ⟩
    v0 +o num 1 +o (v1 [ ⟨ num 1 ⟩ ] [ p ])
      ≡⟨ cong (λ x → v0 +o num 1 +o (x [ p ])) vs[⟨⟩] ⟩
    v0 +o num 1 +o (v0 [ p ])
      ≡⟨ cong (λ x → v0 +o num 1 +o x) [p] ⟩
    v0 +o num 1 +o v1
      ∎

module Std where
  open import Def.Model

  Standard : Model {lsuc lzero} {lzero}
  Standard = record
    { Ty = Set
    ; Nat = ℕ
    ; Bool = 𝟚
    ; Con = Set
    ; ◇ = Lift ⊤
    ; _▹_ = λ Γ A → Γ × A
    ; Var = λ Γ A → Γ → A
    ; vz = λ (γ , a) → a
    ; vs = λ a (γ , b) → a γ
    ; Tm = λ Γ A → Γ → A
    ; Sub = λ Δ Γ → Δ → Γ
    ; p = λ (γ , a) → γ
    ; ⟨_⟩ = λ a γ → γ , a γ
    ; _⁺ = λ γ (δ , a) → γ δ , a
    ; var = λ a → a
    ; _[_] = λ a γ δ → a (γ δ)
    ; [p] = refl
    ; vz[⟨⟩] = refl
    ; vz[⁺] = refl
    ; vs[⟨⟩] = refl
    ; vs[⁺] = refl
    ; true = λ γ → tt
    ; false = λ γ → ff
    ; ite = λ a b c γ → if a γ then b γ else c γ
    ; iteβ₁ = refl
    ; iteβ₂ = refl
    ; true[] = refl
    ; false[] = refl
    ; ite[] = refl
    ; num = λ n γ → n
    ; isZero = λ a γ → a γ == 0
    ; _+o_ = λ a b γ → a γ + b γ
    ; isZeroβ₁ = refl
    ; isZeroβ₂ = refl
    ; +β = refl
    ; num[] = refl
    ; isZero[] = refl
    ; +[] = refl
    }

  open Model Standard

  tm-7 : Tm ◇ Nat
  tm-7 = def false (def (num 2) (ite (var (vs vz)) (num 0) (num 1 +o var vz)))

  eq-7 : tm-7 ≡ num 3
  eq-7 = refl

module Norm where
  open import Def
  open import Def.Syntax
