{-# OPTIONS --prop --rewriting #-}

module gy06-5 where

open import Lib

module injectivity where

  module Razor where
    open import Razor hiding (St)

    -- standard model
    St : Model {lsuc lzero} {lzero}
    St = record
      { Ty = Set -- Set : Set₁
      ; Tm = λ A → A
      ; Nat = ℕ
      ; Bool = 𝟚
      ; true = tt
      ; false = ff
      ; ite = if_then_else_
      ; num = λ n → n
      ; isZero = λ n → n == 0
      ; _+o_ = _+_
      ; iteβ₁ = refl
      ; iteβ₂ = refl
      ; isZeroβ₁ = refl
      ; isZeroβ₂ = refl
      ; +β = refl
      }

    module St = Model St

    module equations where
      open St

      -- a standard modellben az eq-0...eq-12 egyenlosegek definicio szerint teljesulnek (a metaelmedefbol kovetkeznek)
      -- pl.
      eq-4 : isZero (num 3 +o num 1) ≡ false
      eq-4 =  refl

      eq-6 : ite true (isZero (num 0)) false ≡ true
      eq-6 = refl

      eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
      eq-7 = refl

      eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
      eq-8 = refl

      eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
      eq-9 = refl

    open I

    -- bizonyitsd be, hogy Razor.I.num injektiv!
    numInj : ∀{m n} → num m ≡ num n → m ≡ n
    numInj e =  cong St.⟦_⟧t e   -- hasznald az St-be valo kiertekelest!

    1≠2 : 1 ≢ 2 -- \==n    x ≢ y = ¬ (x ≡ y)
    1≠2 ()

    -- bizonyitsd be, hogy Razor.I.isZero nem injektiv!
    notInj : ¬ ((t t' : Tm Nat) → isZero t ≡ isZero t' → t ≡ t')
    notInj e = 1≠2 (numInj (e (num 1) (num 2) (isZeroβ₂ ◾ isZeroβ₂ ⁻¹))) -- hasznald a num injektivitasat ill. az isZeroβ₂-t!
    -- ◾ \sq5

    -- isZeroβ₂ : isZero (num 1) ≡ false
    -- isZeroβ₂ ⁻¹ : false ≡ isZero (num 2)

module normalisation where

  open import Razor
  open I

  eq-4 : isZero (num 3 +o num 1) ≡ false
  eq-4 = comp {t = isZero (num 3 +o num 1)} ⁻¹

  eq-6 : ite true (isZero (num 0)) false ≡ true
  eq-6 = comp ⁻¹

  eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
  eq-7 = comp ⁻¹

  eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
  eq-8 = comp ⁻¹

  eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  eq-9 = comp ⁻¹

module ABT where
  open import DefABT
  open I

  {- Rewrite the following expressions with De Bruijn notation -}

  private
    v' : (n : ℕ) → ∀{m} → Var (suc n + m)
    v' zero = vz
    v' (suc n) = vs (v' n)

  v : (n : ℕ) → ∀{m} → Tm (suc n + m)
  v n = var (v' n)

  -- (def x:=num 1 in x +o x)
  tm-0 : Tm 0
  tm-0 = def (num 1) (v 0 +o v 0)

  -- def x:=num 1 in
  --  x +o def y:=x +o num 1 in
  --    y +o def z:=x +o y in
  --      (x +o z) +o (y +o x)
  tm-1 : Tm 0
  tm-1 =
    def (num 1)
      (v 0 +o def (v 0 +o num 1) (v 0 +o def (v 1 +o v 0) ((v 2 +o v 0) +o (v 1 +o v 2))))

  -- (def x:=num 1 in x) +o def y:=num 1 in
  --   y +o def z:=x +o y in
  --     (x +o z) +o (y +o x)
  tm-2 : Tm 1
  tm-2 =
    def (num 1) (v 0) +o def (num 1) (v 0 +o def (v 1 +o v 0) ((v 2 +o v 0) +o (v 1 +o v 2)))

  -- (def x:=num 1 in
  --      x +o def y:=x +o num 1 in x) +o
  --    def z:=num 1 in z +o z
  tm-3 : Tm {!   !}
  tm-3 = {!   !}

  -- ((def x:=num 1 in x) +o (def y:=num 1 in y)) +o def z:=num 1 in z +o z
  tm-4 : Tm {!   !}
  tm-4 = {!   !}

  -- def x:=(isZero true) in (ite x 0 x)
  tm-5 : Tm {!   !}
  tm-5 = {!   !}


  {- Rewrite the following expressions with variable names -}

  t-1 : Tm 0
  t-1 = def (num 1 +o num 2) (v0 +o v0) +o def (num 3 +o num 4) (v0 +o v0)

  t-1' : Tm 0
  t-1' = def (num 1 +o num 2) ((v0 +o v0) +o def (num 3 +o num 4) (v1 +o v0))

  t-2 : Tm 0
  t-2 = def true (v0 +o def v0 (v0 +o v1))

  t-3 : Tm 0
  t-3 = def true (def false (ite v0 v0 v1))

  t-4 : Tm 0
  t-4 = true +o def true (false +o def v0 (v1 +o v0))

  t-5 : Tm 0
  t-5 = def true (def false (def true (def false ((v0 +o v1) +o (v2 +o v3)))))

  -- exercise 2.6

  zeros : ∀{n} → Vec ℕ n
  zeros {zero} = []
  zeros {suc n} = 0 :: zeros {n}

  zip+ : ∀{n} → Vec ℕ n → Vec ℕ n → Vec ℕ n
  zip+ [] [] = []
  zip+ (m :: ms) (n :: ns) = (m + n) :: zip+ ms ns

  countVarsv : ∀{n} → Var n → Vec ℕ n
  countVarsv vz = 1 :: zeros
  countVarsv (vs x) = 0 :: countVarsv x

  tail : ∀{n} → Vec ℕ (suc n) → Vec ℕ n
  tail (m :: ms) = ms

  countVars' : ∀{n} → Tm n → Vec ℕ n -- var esetén tudni kell, hogy hova kell számolni.
  countVars' = {!   !}

  ttt : Tm 3
  --  ttt = v0
  ttt = (v0 +o v0) +o def v0 (v1 +o v2)

  alma = {!countVars' ttt!}
