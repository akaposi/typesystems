{-# OPTIONS --prop --rewriting #-}

module hf12-5 where

open import Lib

module Fins where
  open import Fin
  open import Fin.Syntax

  postulate
    fst[] : ∀{Γ A B}{t : Tm Γ (A ×o B)}{Δ}{γ : Sub Δ Γ} →
      fst t [ γ ] ≡ fst (t [ γ ])

  ×-comm : ∀ {A B} → A ×o B ≅ B ×o A
  ×-comm = record
    { f = {!   !}
    ; g = {!   !}
    ; fg = {!   !}
    ; gf = {!   !}
    }

  -- Define a function that swaps the two sides of _+o_
  swap-+ : ∀ {Γ A B} → Tm Γ ((A +o B) ⇒ (B +o A))
  swap-+ = {!   !}

  -- Define a function that distributes product type over sum type
  -- This corresponds to the fact that a*(b + c) = a*b + a*c for numbers
  dist : ∀ {Γ A B C} → Tm Γ (A ×o (B +o C) ⇒ (A ×o B +o A ×o C))
  dist = {!   !}

  -- Define the inverse of the previous function
  undist : ∀ {Γ A B C} → Tm Γ ((A ×o B +o A ×o C) ⇒ A ×o (B +o C))
  undist = {!   !}

  -- Define a function that splits a function with sum type codomain
  -- This corresponds to the fact that c^(a + b) = c^a * c^b for numbers
  splitFunc : ∀ {Γ A B C} → Tm Γ (((A +o B) ⇒ C) ⇒ (A ⇒ C) ×o (B ⇒ C))
  splitFunc = {!   !}

  -- Define the inverse of the previous function
  unsplitFunc : ∀ {Γ A B C} → Tm Γ ((A ⇒ C) ×o (B ⇒ C) ⇒ ((A +o B) ⇒ C))
  unsplitFunc = {!   !}

module Inds where
  open import Ind
  open import Ind.Syntax
  open import Ind.Model
  open Standard using (St; []; _∷_)
  module S = Standard

  -- for testing
  eval : {A : Ty} → Tm ◇ A → St.⟦ A ⟧T
  eval t = St.⟦ t ⟧t (mk triv)

  num : ∀ {Γ} → ℕ → Tm Γ Nat
  num zero = zeroo
  num (suc n) = suco (num n)

  plus : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
  plus = lam (lam (iteNat v0 (suco v0) v1))

  plus-test1 : eval plus 0 3 ≡ 3
  plus-test1 = refl

  plus-test2 : eval plus 3 0 ≡ 3
  plus-test2 = refl

  plus-test3 : eval plus 3 3 ≡ 6
  plus-test3 = refl

  plus-test4 : eval plus 2 5 ≡ 7
  plus-test4 = refl

  plus-test5 : eval plus 5 2 ≡ 7
  plus-test5 = refl

  isZero : ∀ {Γ} → Tm Γ (Nat ⇒ Bool)
  isZero = lam (iteNat true false v0)

  ------------------------------------------------------------------------
  -- Prove isZeroβ₂ for inductive natural numbers
  isZeroβ₂ : ∀ {Γ n} → isZero {Γ} $ suco n ≡ false
  isZeroβ₂ {n = n} = {!   !}

  ------------------------------------------------------------------------
  -- Prove num[] by pattern matching
  num[] : ∀ {Γ Δ n} {γ : Sub Δ Γ} → num n [ γ ] ≡ num n
  num[] {n = zero} {γ = γ} = {!   !}
  num[] {n = suc n} {γ = γ} = {!   !}

  ------------------------------------------------------------------------
  -- Define multiplication for inductive natural numbers, use plus
  times : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
  times = {!   !}

  times-test1 : eval times 0 3 ≡ 0
  times-test1 = refl

  times-test2 : eval times 3 0 ≡ 0
  times-test2 = refl

  times-test3 : eval times 3 3 ≡ 9
  times-test3 = refl

  times-test4 : eval times 2 5 ≡ 10
  times-test4 = refl

  times-test5 : eval times 5 2 ≡ 10
  times-test5 = refl

  ------------------------------------------------------------------------
  -- Define exponentiation for inductive natural numbers, use times
  -- Let 0^0 be 1
  pow : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
  pow = {!   !}

  pow-test1 : eval pow 0 0 ≡ 1
  pow-test1 = refl

  pow-test2 : eval pow 3 0 ≡ 1
  pow-test2 = refl

  pow-test3 : eval pow 0 3 ≡ 0
  pow-test3 = refl

  pow-test4 : eval pow 3 3 ≡ 27
  pow-test4 = refl

  pow-test5 : eval pow 5 2 ≡ 25
  pow-test5 = refl

  pow-test6 : eval pow 2 5 ≡ 32
  pow-test6 = refl

  ------------------------------------------------------------------------
  -- Define concatenation for lists
  concat : ∀ {Γ A} → Tm Γ (List A ⇒ List A ⇒ List A)
  concat = {!   !}

  -- ([] is nil, _∷_ is cons)
  concat-test1 : eval (concat {A = Nat}) [] (1 ∷ 2 ∷ 3 ∷ []) ≡ 1 ∷ 2 ∷ 3 ∷ []
  concat-test1 = refl

  concat-test2 : eval (concat {A = Nat}) (1 ∷ 2 ∷ 3 ∷ []) [] ≡ 1 ∷ 2 ∷ 3 ∷ []
  concat-test2 = refl

  concat-test3 :
    eval (concat {A = Nat}) (1 ∷ 2 ∷ 3 ∷ []) (4 ∷ 5 ∷ 6 ∷ []) ≡
    1 ∷ 2 ∷ 3 ∷ 4 ∷ 5 ∷ 6 ∷ []
  concat-test3 = refl

  concat-test4 :
    eval (concat {A = Nat}) (1 ∷ 2 ∷ []) (3 ∷ 4 ∷ 5 ∷ 6 ∷ 7 ∷ []) ≡
    1 ∷ 2 ∷ 3 ∷ 4 ∷ 5 ∷ 6 ∷ 7 ∷ []
  concat-test4 = refl

  concat-test5 :
    eval (concat {A = Nat}) (1 ∷ 2 ∷ 3 ∷ 4 ∷ 5 ∷ []) (6 ∷ 7 ∷ []) ≡
    1 ∷ 2 ∷ 3 ∷ 4 ∷ 5 ∷ 6 ∷ 7 ∷ []
  concat-test5 = refl

  ------------------------------------------------------------------------
  -- Complete the following proof in the syntax, you can use num[]
  concat-proof :
    concat {Γ = ◇} $ cons (num 1) nil $ cons (num 2) nil ≡
    cons (num 1) (cons (num 2) nil)
  concat-proof =
    concat $ cons (num 1) nil $ cons (num 2) nil
      ≡⟨ {!   !} ⟩

    lam (iteList v0 (cons v1 v0) v1) [ ⟨ cons (num 1) nil ⟩ ] $ cons (num 2) nil
      ≡⟨
        cong (λ x → x $ cons (num 2) nil)
          ( lam[] ◾
            cong lam
              ( iteList[] ◾
                cong₃ iteList
                  vz[⁺]
                  ( cons[] ◾
                    cong₂ cons (vs[⁺] ◾ cong (λ x → x [ p ]) vz[⁺] ◾ [p]) vz[⁺])
                  ( vs[⁺] ◾
                    cong (λ x → x [ p ]) vz[⟨⟩] ◾
                    cons[] ◾
                    cong₂ cons num[] nil[]))) ⟩

    lam (iteList v0 (cons v1 v0) (cons (num 1) nil)) $ cons (num 2) nil
      ≡⟨ {!   !} ⟩

    lam (cons (num 1) v0) $ cons (num 2) nil
      ≡⟨ {!   !} ⟩

    cons (num 1) (cons (num 2) nil)
      ∎

  ------------------------------------------------------------------------
  -- Define a function that adds all numbers in a list
  sum : ∀ {Γ} → Tm Γ (List Nat ⇒ Nat)
  sum = {!   !}

  sum-test1 : eval sum [] ≡ 0
  sum-test1 = refl

  sum-test2 : eval sum (1 ∷ 2 ∷ 3 ∷ []) ≡ 6
  sum-test2 = refl

  sum-test3 : eval sum (1 ∷ 2 ∷ 3 ∷ 4 ∷ 5 ∷ []) ≡ 15
  sum-test3 = refl

  ------------------------------------------------------------------------
  -- Define a function that converts a tree to a list
  treeToList : ∀ {Γ A} → Tm Γ (Tree A ⇒ List A)
  treeToList = {!   !}

  treeToList-test1 : eval (treeToList {A = Nat}) (S.leaf 1) ≡ 1 ∷ []
  treeToList-test1 = refl

  treeToList-test2 :
    eval (treeToList {A = Nat}) (S.node (S.leaf 1) (S.leaf 2)) ≡ 1 ∷ 2 ∷ []
  treeToList-test2 = refl

  treeToList-test3 :
    eval (treeToList {A = Nat})
      (S.node (S.node (S.leaf 1) (S.leaf 2)) (S.node (S.leaf 3) (S.leaf 4))) ≡
    1 ∷ 2 ∷ 3 ∷ 4 ∷ []
  treeToList-test3 = refl
