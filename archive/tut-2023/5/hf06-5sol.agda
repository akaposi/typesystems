{-# OPTIONS --prop --rewriting #-}

module hf06-5 where

open import Lib
open import DefABT

open I

-- for testing
size : ∀ {n} → Tm n → ℕ
size (var i) = 1
size (def t u) = 1 + size t + size u
size true = 1
size false = 1
size (ite t u v) = 1 + size t + size u + size v
size (num n) = 1
size (isZero t) = 1 + size t
size (t +o u) = 1 + size t + size u

-- also for testing
eval : ∀ {n} → Tm n → Vec ℕ n → ℕ
eval (var vz) (n :: env) = n
eval (var (vs i)) (n :: env) = eval (var i) env
eval (def t u) env = eval u (eval t env :: env)
eval true env = 1
eval false env = 0
eval (ite t u v) env = if eval t env == 0 then eval v env else eval u env
eval (num n) env = n
eval (isZero t) env = if eval t env == 0 then 1 else 0
eval (t +o u) env = eval t env + eval u env

-- Use this function for variables in De Bruijn notation
-- You can also use v0, v1, v2, v3 defined in DefABT
-- v0 = v 0
-- v1 = v 1
-- ...
v : (n : ℕ) → ∀ {m} → Tm (suc n + m)
v n = var (v' n) where
  v' : (n : ℕ) → ∀ {m} → Var (suc n + m)
  v' zero = vz
  v' (suc n) = vs (v' n)

-- Write down the following terms with De Bruijn notation
-- For free variables, use the lowest possible numbers to make the tests
-- typecheck

-- let a := num 1 in let y := isZero a in ite y a (num 1 +o a)
q1 : Tm 0
q1 = def (num 1) (def (isZero (v 0)) (ite (v 0) (v 1) (num 1 +o v 1)))

_ : size q1 ≡ 11
_ = refl

_ : ∀ {env} → eval q1 env ≡ 2
_ = refl

-- let a := num 1 in let a := a +o a in a +o a
q2 : Tm 0
q2 = def (num 1) (def (v0 +o v0) (v0 +o v0))

_ : size q2 ≡ 9
_ = refl

_ : ∀ {env} → eval q2 env ≡ 4
_ = refl

-- let a := num 1 +o b in b +o let c := a +o num 1 in b +o c
q3 : Tm 1
q3 = def (num 1 +o v0) (v1 +o def (v0 +o num 1) (v2 +o v0))

_ : size q3 ≡ 13
_ = refl

_ : ∀ {env} → eval q3 (1 :: env) ≡ 5
_ = refl

_ : ∀ {env} → eval q3 (2 :: env) ≡ 8
_ = refl

-- ite (isZero a)
--   (let a := num 1 +o a in a +o a)
--   (let b := a +o num 3 in let b := a +o b in b)
q4 : Tm 1
q4 =
  ite (isZero v0)
    (def (num 1 +o v0) (v0 +o v0))
    (def (v0 +o num 3) (def (v1 +o v0) v0))

_ : size q4 ≡ 19
_ = refl

_ : ∀ {env} → eval q4 (0 :: env) ≡ 2
_ = refl

_ : ∀ {env} → eval q4 (1 :: env) ≡ 5
_ = refl

_ : ∀ {env} → eval q4 (2 :: env) ≡ 7
_ = refl

-- let a := num 3 in let b := isZero c in ite b c (let c := a +o c in c)
q5 : Tm 1
q5 = def (num 3) (def (isZero v1) (ite v0 v2 (def (v1 +o v2) v0)))

_ : size q5 ≡ 13
_ = refl

_ : ∀ {env} → eval q5 (0 :: env) ≡ 0
_ = refl

_ : ∀ {env} → eval q5 (1 :: env) ≡ 4
_ = refl

_ : ∀ {env} → eval q5 (2 :: env) ≡ 5
_ = refl

-- let a := num 3 +o num 2 in a +o b +o let b := num 1 in b +o c
q6 : Tm 2
q6 = def (num 3 +o num 2) (v0 +o v1 +o def (num 1) (v0 +o v3))

_ : size q6 ≡ 13
_ = refl

_ : ∀ {env} → eval q6 (4 :: 3 :: env) ≡ 13
_ = refl

_ : ∀ {env} → eval q6 (6 :: 9 :: env) ≡ 21
_ = refl

-- a +o (let a := num 2 in a +o a +o b)
--   +o (let b := a +o b +o num 3 in a +o b) +o b
q7 : Tm 2
q7 =
  v0 +o def (num 2) (v0 +o v0 +o v2)
     +o def (v0 +o v1 +o num 3) (v1 +o v0) +o v1

_ : size q7 ≡ 21
_ = refl

_ : ∀ {env} → eval q7 (4 :: 1 :: env) ≡ 22
_ = refl

_ : ∀ {env} → eval q7 (5 :: 6 :: env) ≡ 40
_ = refl
