{-# OPTIONS --prop --rewriting #-}

module gy11-5 where

open import Lib

module St where
  open import STT

  St' : Model
  St' = record
    { Ty = Set
    ; Con = Set
    ; Var = λ Γ A → (Γ → A)
    ; Tm = λ Γ A → (Γ → A)
    ; Sub = λ Δ Γ → (Δ → Γ)
    ; ◇ = Lift ⊤
    ; _▹_ = _×_
    ; p = π₁
    ; ⟨_⟩ = λ a γ → (γ , a γ)
    ; _⁺ = λ γ (δ , a) → (γ δ , a)
    ; vz = π₂
    ; vs = λ a (γ , b) → a γ
    ; var = λ a → a
    ; _[_] = λ a γ δ → a (γ δ)
    ; [p] = refl
    ; vz[⟨⟩] = refl
    ; vz[⁺] = refl
    ; vs[⟨⟩] = refl
    ; vs[⁺] = refl
    ; Bool = 𝟚
    ; true = λ _ → tt
    ; false = λ _ → ff
    ; ite = λ b t f γ → if b γ then t γ else f γ
    ; iteβ₁ = refl
    ; iteβ₂ = refl
    ; true[] = refl
    ; false[] = refl
    ; ite[] = refl
    ; Nat = ℕ
    ; num = λ n _ → n
    ; isZero = λ n γ → n γ == 0
    ; _+o_ = λ a b γ → a γ + b γ
    ; isZeroβ₁ = refl
    ; isZeroβ₂ = refl
    ; +β = refl
    ; num[] = refl
    ; isZero[] = refl
    ; +[] = refl
    ; _⇒_ = {!   !}
    ; lam = {!   !}
    ; _$_ = {!   !}
    ; ⇒β = {!   !}
    ; ⇒η = {!   !}
    ; lam[] = {!   !}
    ; $[] = {!   !}
    }

module Nf where
  open import STT
  open import STT.Syntax

  -- Exercise 3.12. What are the normal form of the following terms?
  t-2 : Σsp (Nf (◇ ▹ Bool) Bool) λ v → ⌜ v ⌝Nf ≡ v0
  t-2 = {!   !}

  t-3 : Σsp (Nf (◇ ▹ Nat) Nat) λ v → ⌜ v ⌝Nf ≡ v0
  t-3 = {!   !}

  t-8 : Σsp (Nf ◇ Bool) λ v → ⌜ v ⌝Nf ≡ lam (isZero v0) $ num 0
  t-8 = {!   !}

  t-9 : Σsp (Nf (◇ ▹ Nat) Bool) λ v → ⌜ v ⌝Nf ≡ lam (isZero v0) $ v0
  t-9 = {!   !}

  t-10 : Σsp (Nf (◇ ▹ Nat) Bool) λ v → ⌜ v ⌝Nf ≡ lam (isZero v0) $ (v0 +o num 1)
  t-10 = {!   !}

  t-11 : Σsp (Nf (◇ ▹ Nat ⇒ Nat ⇒ Nat) (Nat ⇒ Nat ⇒ Nat)) λ v → ⌜ v ⌝Nf ≡ v0
  t-11 = {!   !}

  t-12 : Σsp (Nf (◇ ▹ Nat ⇒ Nat ⇒ Nat) (Nat ⇒ Nat)) λ v → ⌜ v ⌝Nf ≡ v0 $ (num 1 +o num 2)
  t-12 = {!   !}

module F where
  open import Fin hiding (snd[])
  open import Fin.Syntax

  swap : ∀ {Γ A B} → Tm Γ (A ×o B ⇒ B ×o A)
  swap = {!   !}

  curry : ∀ {Γ A B C} → Tm Γ ((A ×o B ⇒ C) ⇒ A ⇒ B ⇒ C)
  curry = {!   !}

  uncurry : ∀ {Γ A B C} → Tm Γ ((A ⇒ B ⇒ C) ⇒ A ×o B ⇒ C)
  uncurry = {!   !}

  snd[] : ∀{Γ A B}{t : Tm Γ (A ×o B)}{Δ}{γ : Sub Δ Γ} → snd t [ γ ] ≡ snd (t [ γ ])
  snd[] = {!   !}

  ×-idr : ∀ {A} → A ×o Unit ≅ A
  ×-idr = {!   !}

  ×-comm : ∀ {A B} → A ×o B ≅ B ×o A
  ×-comm = {!   !}
