{-# OPTIONS --prop --rewriting #-}

module gy11-5 where

open import Lib

module ZH where
  open import STT.Syntax

  const : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
  const = lam (lam v1)

  e : const $ false ≡ lam false
  e =
    lam (lam v1) $ false
      ≡⟨ ⇒β ⟩
    lam v1 [ ⟨ false ⟩ ]
      ≡⟨ lam[] ⟩
    lam (var (vs vz) [ ⟨ false ⟩ ⁺ ])
      ≡⟨ cong lam vs[⁺] ⟩
    lam (var vz [ ⟨ false ⟩ ] [ p ])
      ≡⟨ cong (λ x → lam (x [ p ])) vz[⟨⟩] ⟩
    lam (false [ p ])
      ≡⟨ cong lam false[] ⟩
    lam false
      ∎

module St where
  open import STT

  St' : Model
  St' = record
    { Ty = Set
    ; Con = Set
    ; Var = λ Γ A → (Γ → A)
    ; Tm = λ Γ A → (Γ → A)
    ; Sub = λ Δ Γ → (Δ → Γ)
    ; ◇ = Lift ⊤
    ; _▹_ = _×_
    ; p = π₁
    ; ⟨_⟩ = λ a γ → (γ , a γ)
    ; _⁺ = λ γ (δ , a) → (γ δ , a)
    ; vz = π₂
    ; vs = λ a (γ , b) → a γ
    ; var = λ a → a
    ; _[_] = λ a γ δ → a (γ δ)
    ; [p] = refl
    ; vz[⟨⟩] = refl
    ; vz[⁺] = refl
    ; vs[⟨⟩] = refl
    ; vs[⁺] = refl
    ; Bool = 𝟚
    ; true = λ _ → tt
    ; false = λ _ → ff
    ; ite = λ b t f γ → if b γ then t γ else f γ
    ; iteβ₁ = refl
    ; iteβ₂ = refl
    ; true[] = refl
    ; false[] = refl
    ; ite[] = refl
    ; Nat = ℕ
    ; num = λ n _ → n
    ; isZero = λ n γ → n γ == 0
    ; _+o_ = λ a b γ → a γ + b γ
    ; isZeroβ₁ = refl
    ; isZeroβ₂ = refl
    ; +β = refl
    ; num[] = refl
    ; isZero[] = refl
    ; +[] = refl
    ; _⇒_ = λ A B → (A → B)
    ; lam = λ f γ a → f (γ , a)
    ; _$_ = λ f g γ → f γ (g γ)
    ; ⇒β = refl
    ; ⇒η = refl
    ; lam[] = refl
    ; $[] = refl
    }

module Nf where
  open import STT
  open import STT.Syntax

  -- Exercise 3.12. What are the normal form of the following terms?
  t-2 : Σsp (Nf (◇ ▹ Bool) Bool) λ v → ⌜ v ⌝Nf ≡ v0
  t-2 = neuBool (varNe vz) , refl

  t-3 : Σsp (Nf (◇ ▹ Nat) Nat) λ v → ⌜ v ⌝Nf ≡ v0
  t-3 = neuNat (varNe vz) , refl

  t-8 : Σsp (Nf ◇ Bool) λ v →
        ⌜ v ⌝Nf ≡ lam (isZero v0) $ num 0
  t-8 = trueNf , {!   !}

  t-9 : Σsp (Nf (◇ ▹ Nat) Bool) λ v →
        ⌜ v ⌝Nf ≡ lam (isZero v0) $ v0
  t-9 = neuBool (isZeroNe (varNe vz)) , {!   !}

  t-10 : Σsp (Nf (◇ ▹ Nat) Bool) λ v →
        ⌜ v ⌝Nf ≡ lam (isZero v0) $ (v0 +o num 1)
  t-10 = neuBool (isZeroNe (varNe vz +NeNf 1)) , {!   !}

  t-11 :
    Σsp (Nf (◇ ▹ Nat ⇒ Nat ⇒ Nat) (Nat ⇒ Nat ⇒ Nat)) λ v →
    ⌜ v ⌝Nf ≡ v0
  t-11 = lamNf (lamNf
    (neuNat (varNe (vs (vs vz))
      $Ne neuNat (varNe (vs vz))
      $Ne neuNat (varNe vz)))) ,
    (lam (lam (v2 $ v1 $ v0))
      ≡⟨ cong₂ (λ x y → lam (lam (x $ y $ v0))) ([p] ⁻¹) ([p] ⁻¹) ⟩
    lam (lam ((v1 [ p ]) $ (v0 [ p ]) $ v0))
      ≡⟨ cong (λ x → lam (lam (x $ v0))) ($[] ⁻¹) ⟩
    lam (lam ((v1 $ v0) [ p ] $ v0))
      ≡⟨ cong lam (⇒η ⁻¹) ⟩
    lam (v1 $ v0)
      ≡⟨ cong (λ x → lam (x $ v0)) ([p] ⁻¹) ⟩
    lam (v0 [ p ] $ v0)
      ≡⟨ ⇒η ⁻¹ ⟩
    v0
      ∎)
  -- f = λ x. λ y. f $ x $ y

  t-12 :
    Σsp (Nf (◇ ▹ Nat ⇒ Nat ⇒ Nat) (Nat ⇒ Nat)) λ v →
    ⌜ v ⌝Nf ≡ v0 $ (num 1 +o num 2)
  t-12 = lamNf
    (neuNat (varNe (vs vz)
      $Ne numNf 3
      $Ne neuNat (varNe vz))) , {!   !}
  -- f $ (num 1 +o num 2)
  -- λ x. f $ (num 1 +o num 2) $ x

module F where
  open import Fin hiding (trivial[]; snd[])
  open import Fin.Syntax

  swap : ∀ {Γ A B} → Tm Γ (A ×o B ⇒ B ×o A)
  swap = lam (snd v0 ,o fst v0)

  curry : ∀ {Γ A B C} → Tm Γ ((A ×o B ⇒ C) ⇒ A ⇒ B ⇒ C)
  curry = lam (lam (lam (v2 $ (v1 ,o v0))))

  uncurry : ∀ {Γ A B C} → Tm Γ ((A ⇒ B ⇒ C) ⇒ A ×o B ⇒ C)
  uncurry = lam (lam (v1 $ fst v0 $ snd v0))

  -- Exercise 4.2 (compulsory). Prove the substitution rule for the constructor
  -- of the unit type.
  trivial[] : ∀{Γ Δ}{γ : Sub Δ Γ} → trivial [ γ ] ≡ trivial
  trivial[] = Unitη

  -- Exercise 4.1 (recommended). Prove the substitution rule for the other
  -- destructor for products.
  snd[] : ∀{Γ A B}{t : Tm Γ (A ×o B)}{Δ}{γ : Sub Δ Γ} → snd t [ γ ] ≡ snd (t [ γ ])
  snd[] {t = t} {γ = γ} =
    snd t [ γ ]
      ≡⟨ ×β₂ ⁻¹ ⟩
    snd ((fst t [ γ ]) ,o (snd t [ γ ]))
      ≡⟨ cong snd (,[] ⁻¹) ⟩
    snd ((fst t ,o snd t) [ γ ])
      ≡⟨ cong (λ x → snd (x [ γ ])) (×η ⁻¹) ⟩
    snd (t [ γ ])
      ∎

  -- Exercise 4.11 (compulsory). Show that for any type A, A ×o Unit is
  -- isormophic to A.
  ×-idr : ∀ {A} → A ×o Unit ≅ A
  ×-idr = record
    { f = fst v0
    ; g = v0 ,o trivial
    ; fg = {!   !}
    ; gf = {!   !} }

  -- Exercise 4.12 (compulsory). Show that for any two types A, B, there is an
  -- isomorphism between A +o B and B +o A.
  ×-comm : ∀ {A B} → A ×o B ≅ B ×o A
  ×-comm = {!   !}
