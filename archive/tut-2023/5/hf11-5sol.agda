{-# OPTIONS --prop --rewriting #-}

module hf11-5 where

open import Lib

module FuncNf where
  open import STT
  open import STT.Syntax

  -- Give the normal forms for the following terms

  nf1 :
    Σsp (Nf (◇ ▹ Nat) (Bool ⇒ Nat)) λ v →
    ⌜ v ⌝Nf ≡ lam (ite v0 (num 0) v1)
  nf1 =
    lamNf (neuNat (iteNe (varNe vz) (numNf 0) (neuNat (varNe (vs vz))))) ,
    refl

  nf2 :
    Σsp (Nf (◇ ▹ Nat ⇒ Bool ⇒ Nat ▹ Bool) Nat) λ v →
    ⌜ v ⌝Nf ≡ v1 $ (num 1 +o num 2) $ v0
  nf2 =
    neuNat (varNe (vs vz) $Ne numNf 3 $Ne neuBool (varNe vz)) ,
    cong (λ x → v1 $ x $ v0) (+β ⁻¹)

  nf3 :
    Σsp (Nf (◇ ▹ Nat ⇒ Bool ⇒ Nat ▹ Nat) (Bool ⇒ Nat)) λ v →
    ⌜ v ⌝Nf ≡ v1 $ v0
  nf3 =
    lamNf (neuNat (varNe (vs (vs vz)) $Ne neuNat (varNe (vs vz)) $Ne neuBool (varNe vz))) ,
    ( lam (v2 $ v1 $ v0)
        ≡⟨ cong₂ (λ x y → lam ((x $ y) $ v0)) ([p] ⁻¹) ([p] ⁻¹) ⟩

      lam ((v1 [ p ]) $ (v0 [ p ]) $ v0)
        ≡⟨ cong (λ x → lam (x $ v0)) ($[] ⁻¹) ⟩

      lam (((v1 $ v0) [ p ]) $ v0)
        ≡⟨ ⇒η ⁻¹ ⟩

      v1 $ v0
        ∎)

module Product where
  open import Fin.Syntax

  -- Define the following functions based on their types

  -- Interestingly, every function here only has one possible definition
  -- (up to equality)

  dup : ∀ {Γ A} → Tm Γ (A ⇒ A ×o A)
  dup = lam (v0 ,o v0)

  assocl : ∀ {Γ A B C} → Tm Γ (A ×o (B ×o C) ⇒ (A ×o B) ×o C)
  assocl = lam ((fst v0 ,o fst (snd v0)) ,o snd (snd v0))

  mapBoth : ∀ {Γ A B C D} → Tm Γ ((A ⇒ B) ⇒ (C ⇒ D) ⇒ A ×o C ⇒ B ×o D)
  mapBoth = lam (lam (lam ((v2 $ fst v0) ,o (v1 $ snd v0))))

  distFunc : ∀ {Γ A B C} → Tm Γ ((A ⇒ B ×o C) ⇒ (A ⇒ B) ×o (A ⇒ C))
  distFunc = lam (lam (fst (v1 $ v0)) ,o lam (snd (v1 $ v0)))

  ev : ∀ {Γ A B} → Tm Γ ((A ⇒ B) ×o A ⇒ B)
  ev = lam (fst v0 $ snd v0)
