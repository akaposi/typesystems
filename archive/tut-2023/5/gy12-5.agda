{-# OPTIONS --prop --rewriting #-}

module gy12-5 where

open import Lib

module F where
  open import Fin
  open import Fin.Syntax

  postulate
    fst[] : ∀{Γ A B}{t : Tm Γ (A ×o B)}{Δ}{γ : Sub Δ Γ} →
      fst t [ γ ] ≡ fst (t [ γ ])

  -- Exercise 4.11 (compulsory). Show that for any type A, A ×o Unit is
  -- isormophic to A.
  ×-idr : ∀ {A} → A ×o Unit ≅ A
  ×-idr = record
    { f = fst v0
    ; g = v0 ,o trivial
    ; fg =
      fst v0 [ p ⁺ ] [ ⟨ v0 ,o trivial ⟩ ]
        ≡⟨ cong (λ x → x [ ⟨ v0 ,o trivial ⟩ ]) fst[] ⟩
      fst (v0 [ p ⁺ ]) [ ⟨ v0 ,o trivial ⟩ ]
        ≡⟨ cong (λ x → fst x [ ⟨ v0 ,o trivial ⟩ ]) vz[⁺] ⟩
      fst v0 [ ⟨ v0 ,o trivial ⟩ ]
        ≡⟨ fst[] ⟩
      fst (v0 [ ⟨ v0 ,o trivial ⟩ ])
        ≡⟨ cong fst vz[⟨⟩] ⟩
      fst (v0 ,o trivial)
        ≡⟨ ×β₁ ⟩
      v0
        ∎
    ; gf =
      (v0 ,o trivial) [ p ⁺ ] [ ⟨ fst v0 ⟩ ]
        ≡⟨ {!   !} ⟩
      ((v0 [ p ⁺ ]) ,o (trivial [ p ⁺ ])) [ ⟨ fst v0 ⟩ ]
        ≡⟨ {!   !} ⟩
      (v0 ,o trivial) [ ⟨ fst v0 ⟩ ]
        ≡⟨ {!   !} ⟩
      ((v0 [ ⟨ fst v0 ⟩ ]) ,o (trivial [ ⟨ fst v0 ⟩ ]))
        ≡⟨ {!   !} ⟩
      (fst v0 ,o trivial)
        ≡⟨ cong (λ x → (fst v0 ,o x)) (Unitη ⁻¹) ⟩
      (fst v0 ,o snd v0)
        ≡⟨ ×η ⁻¹ ⟩
      v0
        ∎
    }

  -- Show that for any type A, A +o Empty is isormophic to A.
  +-idr : ∀ {A} → A +o Empty ≅ A
  +-idr = record
    { f = caseo v0 (absurd v0) v0
    ; g = inl v0
    ; fg =
      caseo v0 (absurd v0) v0 [ p ⁺ ] [ ⟨ inl v0 ⟩ ]
        ≡⟨ {!   !} ⟩
      caseo (v0 [ p ⁺ ⁺ ]) (absurd v0 [ p ⁺ ⁺ ]) (v0 [ p ⁺ ]) [ ⟨ inl v0 ⟩ ]
        ≡⟨ {!   !} ⟩
      caseo v0 (absurd v0) v0 [ ⟨ inl v0 ⟩ ]
        ≡⟨ {!   !} ⟩
      caseo (v0 [ ⟨ inl v0 ⟩ ⁺ ]) (absurd v0 [ ⟨ inl v0 ⟩ ⁺ ]) (v0 [ ⟨ inl v0 ⟩ ])
        ≡⟨ {!   !} ⟩
      caseo v0 (absurd v0) (inl v0)
      -- case Left x of
      --   Left x -> ...
        ≡⟨ +β₁ ⟩
      v0 [ ⟨ v0 ⟩ ]
        ≡⟨ vz[⟨⟩] ⟩
      v0
        ∎
    ; gf =
      inl v0 [ p ⁺ ] [ ⟨ caseo v0 (absurd v0) v0 ⟩ ]
        ≡⟨ {!   !} ⟩
      inl v0 [ ⟨ caseo v0 (absurd v0) v0 ⟩ ]
        ≡⟨ {!   !} ⟩
      inl (caseo v0 (absurd v0) v0)
        ≡⟨ +η ⟩
      caseo
        (inl (caseo v0 (absurd v0) v0) [ p ⁺ ] [ ⟨ inl (var vz) ⟩ ] [ p ⁺ ])
        (inl (caseo v0 (absurd v0) v0) [ p ⁺ ] [ ⟨ inr (var vz) ⟩ ] [ p ⁺ ])
        (var vz)
        ≡⟨ {!   !} ⟩
      caseo
        (v0 [ p ⁺ ] [ ⟨ inl (var vz) ⟩ ] [ p ⁺ ])
        (v0 [ p ⁺ ] [ ⟨ inr (var vz) ⟩ ] [ p ⁺ ])
        (var vz)
        ≡⟨ +η ⁻¹ ⟩
      v0
        ∎
    }

module _ where
  open import Ind
  open import Ind.Syntax

  -- Exercise 5.3 (compulsory). Define num as syntactic sugar
  num : ∀ {Γ} → ℕ → Tm Γ Nat
  num zero = zeroo
  num (suc n) = suco (num n)

  -- Define the follwing functions using iterators
  isZero : ∀ {Γ} → Tm Γ (Nat ⇒ Bool)
  isZero = lam (iteNat true false v0)

  isZeroβ₁ : ∀ {Γ} → isZero {Γ} $ num 0 ≡ true
  isZeroβ₁ =
    lam (iteNat true false v0) $ num 0
      ≡⟨ refl ⟩
    lam (iteNat true false v0) $ zeroo
      ≡⟨ ⇒β ⟩
    iteNat true false v0 [ ⟨ zeroo ⟩ ]
      ≡⟨ iteNat[] ⟩
    iteNat (true [ ⟨ zeroo ⟩ ]) (false [ ⟨ zeroo ⟩ ⁺ ]) (v0 [ ⟨ zeroo ⟩ ])
      ≡⟨ cong₃ iteNat true[] false[] vz[⟨⟩] ⟩
    iteNat true false zeroo
      ≡⟨ Natβ₁ ⟩
    true
      ∎

  plus : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
  plus = lam (lam (iteNat v1 (suco v0) v0))
  -- plus $ x $ zeroo = x
  -- plus $ x $ suco y = suc (plus $ x $ y)

  length : ∀ {Γ A} → Tm Γ (List A ⇒ Nat)
  length = lam (iteList zeroo (suco v0) v0)

  length-test : length {◇} $ (cons false nil) ≡ num 1
  length-test =
    length $ cons false nil
      ≡⟨ refl ⟩
    lam (iteList zeroo (suco v0) v0) $ cons false nil
      ≡⟨ ⇒β ⟩
    iteList zeroo (suco v0) v0 [ ⟨ cons false nil ⟩ ]
      ≡⟨ {!   !} ⟩
    iteList zeroo (suco v0) (cons false nil)
      ≡⟨ Listβ₂ ⟩
    suco v0 [ ⟨ false ⟩ ⁺ ] [ ⟨ iteList zeroo (suco v0) nil ⟩ ]
      ≡⟨ {!   !} ⟩
    suco v0 [ ⟨ iteList zeroo (suco v0) nil ⟩ ]
      ≡⟨ {!   !} ⟩
    suco (iteList zeroo (suco v0) nil)
      ≡⟨ cong suco Listβ₁ ⟩
    suco zeroo
      ≡⟨ refl ⟩
    num 1
      ∎

  -- Exercise 5.4 (compulsory). List the rules for inductively defined trees
  -- with ternary branching, a boolean at each node and a natural number at each
  -- leaf.
  postulate
    Tree1 : Ty
    leaf1 : ∀ {Γ} → Tm Γ Nat → Tm Γ Tree1
    node1 : ∀ {Γ} → Tm Γ Bool → Tm Γ Tree1 → Tm Γ Tree1 → Tm Γ Tree1 → Tm Γ Tree1
    iteTree1 :
      ∀ {Γ A} → Tm (Γ ▹ Nat) A → Tm (Γ ▹ Bool ▹ A ▹ A ▹ A) A → Tm Γ Tree1 → Tm Γ A
    Tree1β₁ : ∀ {Γ A u v n} → iteTree1 {Γ} {A} u v (leaf1 n) ≡ (u [ ⟨ n ⟩ ])
    Tree1β₂ : ∀ {Γ A u v b t1 t2 t3} →
      iteTree1 {Γ} {A} u v (node1 b t1 t2 t3) ≡
      ( v [ ⟨ b ⟩ ⁺ ⁺ ⁺ ]
          [ ⟨ iteTree1 u v t1 ⟩ ⁺ ⁺ ]
          [ ⟨ iteTree1 u v t2 ⟩ ⁺ ]
          [ ⟨ iteTree1 u v t3 ⟩ ])
    iteTree1[] : ∀ {Γ Δ} {γ : Sub Δ Γ} {A u v t} →
      iteTree1 {Γ} {A} u v t [ γ ] ≡ iteTree1 (u [ γ ⁺ ]) (v [ γ ⁺ ⁺ ⁺ ⁺ ]) (t [ γ ])

  -- Exercise 5.5 (compulsory). List the rules for inductively defined trees
  -- with two kinds of nullary, one kind of binary and three kinds of ternary
  -- branching. There is no extra information at leaves or nodes.

  -- Exercise 5.6 (compulsory). List the rules for inductively defined trees no
  -- information at the leaves and infinity (Nat-ary) branching.

  -- Exercise 5.7 (compulsory). For types A, B, list the rules for the inductive
  -- type with only leaves (and no nodes) that contain an A and a B. Derive all
  -- its rules from binary products of the previous section. Which rule of
  -- binary products cannot be derived from this inductive type?

  -- Exercise 5.8 (compulsory). For types A, B, list the rules for the inductive
  -- type with two kinds of leaves and no nodes. One kind of leaf contains an A,
  -- the other kind of leaf contains a B. Derive all the rules from binary sums
  -- of the previous section. Which rule of binary sums cannot be derived from
  -- this inductive type?

  -- Exercise 5.9 (compulsory). How many elements are in the inductive type with
  -- only the following constructors?

  -- SpecialTree : Ty
  -- node1 : Tm Γ SpecialTree → Tm Γ SpecialTree
  -- node2 : Tm Γ SpecialTree → Tm Γ SpecialTree → Tm Γ SpecialTree
