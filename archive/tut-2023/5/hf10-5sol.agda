{-# OPTIONS --prop --rewriting #-}

module hf10-5 where

open import Lib
open import STT.Syntax

-- Internalized _+o_
add' : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
add' = lam (lam (v1 +o v0))

+β' : ∀ {Γ} (m : ℕ) (n : ℕ) → add' {Γ} $ num m $ num n ≡ num (m + n)
+β' m n =
  add' $ num m $ num n
    ≡⟨ refl ⟩

  lam (lam (v1 +o v0)) $ num m $ num n
    ≡⟨ cong (_$ num n) ⇒β ⟩

  lam (v1 +o v0) [ ⟨ num m ⟩ ] $ num n
    ≡⟨ cong (_$ num n) lam[] ⟩

  lam (v1 +o v0 [ ⟨ num m ⟩ ⁺ ]) $ num n
    ≡⟨ cong (λ x → lam x $ num n) +[] ⟩

  lam ((v1 [ ⟨ num m ⟩ ⁺ ]) +o (v0 [ ⟨ num m ⟩ ⁺ ])) $ num n
    ≡⟨ cong₂ (λ x y → lam (x +o y) $ num n) vs[⁺] vz[⁺] ⟩

  lam ((v0 [ ⟨ num m ⟩ ] [ p ]) +o v0) $ num n
    ≡⟨ cong (λ x → lam ((x [ p ]) +o v0) $ num n) vz[⟨⟩] ⟩

  lam ((num m [ p ]) +o v0) $ num n
    ≡⟨ cong (λ x → lam (x +o v0) $ num n) num[] ⟩

  lam (num m +o v0) $ num n
    ≡⟨ ⇒β ⟩

  num m +o v0 [ ⟨ num n ⟩ ]
    ≡⟨ +[] ⟩

  (num m [ ⟨ num n ⟩ ]) +o (v0 [ ⟨ num n ⟩ ])
    ≡⟨ cong₂ _+o_ num[] vz[⟨⟩] ⟩

  num m +o num n
    ≡⟨ +β ⟩

  num (m + n)
    ∎

t1 : Tm (◇ ▹ Bool) (Nat ⇒ Nat)
t1 = lam (lam (v0 +o (v1 $ num 1))) $ lam (ite v1 v0 (num 0))

e1 : t1 ≡ lam (v0 +o ite v1 (num 1) (num 0))
e1 =
  -- (λ x. λ y. y +o (x $ num 1)) $ (λ z. ite b z (num 0))
  lam (lam (v0 +o (v1 $ num 1))) $ lam (ite v1 v0 (num 0))
    ≡⟨ ⇒β ⟩

  -- (λ y. y +o (x $ num 1)) [x ↦ λ z. ite b z (num 0)]
  lam (v0 +o (v1 $ num 1)) [ ⟨ lam (ite v1 v0 (num 0)) ⟩ ]
    ≡⟨ lam[] ⟩

  lam (v0 +o (v1 $ num 1) [ ⟨ lam (ite v1 v0 (num 0)) ⟩ ⁺ ])
    ≡⟨ cong lam +[] ⟩

  lam ((v0 [ ⟨ lam (ite v1 v0 (num 0)) ⟩ ⁺ ]) +o ((v1 $ num 1) [ ⟨ lam (ite v1 v0 (num 0)) ⟩ ⁺ ]))
    ≡⟨ cong₂ (λ x y → lam (x +o y)) vz[⁺] $[] ⟩

  lam (v0 +o ((v1 [ ⟨ lam (ite v1 v0 (num 0)) ⟩ ⁺ ]) $ (num 1 [ ⟨ lam (ite v1 v0 (num 0)) ⟩ ⁺ ])))
    ≡⟨ cong₂ (λ x y → lam (v0 +o (x $ y))) vs[⁺] num[] ⟩

  lam (v0 +o ((v0 [ ⟨ lam (ite v1 v0 (num 0)) ⟩ ] [ p ]) $ num 1))
    ≡⟨ cong (λ x → lam (v0 +o ((x [ p ]) $ num 1))) vz[⟨⟩] ⟩

  lam (v0 +o ((lam (ite v1 v0 (num 0)) [ p ]) $ num 1))
    ≡⟨ cong (λ x → lam (v0 +o (x $ num 1))) lam[] ⟩

  lam (v0 +o (lam (ite v1 v0 (num 0) [ p ⁺ ]) $ num 1))
    ≡⟨ cong (λ x → lam (v0 +o (lam x $ num 1))) ite[] ⟩

  lam (v0 +o (lam (ite (v1 [ p ⁺ ]) (v0 [ p ⁺ ]) (num 0 [ p ⁺ ])) $ num 1))
    ≡⟨ cong₃ (λ x y z → lam (v0 +o (lam (ite x y z) $ num 1))) vs[⁺] vz[⁺] num[] ⟩

  lam (v0 +o (lam (ite (v0 [ p ] [ p ]) v0 (num 0)) $ num 1))
    ≡⟨ cong (λ x → lam (v0 +o (lam (ite (x [ p ]) v0 (num 0)) $ num 1))) [p] ⟩

  lam (v0 +o (lam (ite (v1 [ p ]) v0 (num 0)) $ num 1))
    ≡⟨ cong (λ x → lam (v0 +o (lam (ite x v0 (num 0)) $ num 1))) [p] ⟩

  -- (λ y. y +o ((λ z. ite b z (num 0)) $ num 1))
  lam (v0 +o (lam (ite v2 v0 (num 0)) $ num 1))
    ≡⟨ cong (λ x → lam (v0 +o x)) ⇒β ⟩

  -- (λ y. y +o (ite b z (num 0) [z ↦ num 1]))
  lam (v0 +o (ite v2 v0 (num 0) [ ⟨ num 1 ⟩ ]))
    ≡⟨ cong (λ x → lam (v0 +o x)) ite[] ⟩

  lam (v0 +o ite (v2 [ ⟨ num 1 ⟩ ]) (v0 [ ⟨ num 1 ⟩ ]) (num 0 [ ⟨ num 1 ⟩ ]))
    ≡⟨ cong₃ (λ x y z → lam (v0 +o ite x y z)) vs[⟨⟩] vz[⟨⟩] num[] ⟩

  -- (λ y. y +o ite b (num 1) (num 0))
  lam (v0 +o ite v1 (num 1) (num 0))
    ∎

t2 : Tm (◇ ▹ Bool ⇒ Bool ⇒ Bool) (Bool ⇒ Bool ⇒ Bool)
t2 = lam (lam (v2 $ v1 $ v0))

-- Use ⇒η
e2 : v0 ≡ t2   -- f = (λ x. λ y. f $ x $ y)
e2 =
  v0
    ≡⟨ ⇒η ⟩

  lam (v0 [ p ] $ v0)
    ≡⟨ cong (λ x → lam (x $ v0)) [p] ⟩

  lam (v1 $ v0)
    ≡⟨ cong lam ⇒η ⟩

  lam (lam (((v1 $ v0) [ p ]) $ v0))
    ≡⟨ cong (λ x → lam (lam (x $ v0))) $[] ⟩

  lam (lam ((v1 [ p ]) $ (v0 [ p ]) $ v0))
    ≡⟨ cong₂ (λ x y → lam (lam (x $ y $ v0))) [p] [p] ⟩

  lam (lam (v2 $ v1 $ v0))
    ∎
