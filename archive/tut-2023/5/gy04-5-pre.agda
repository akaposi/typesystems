{-# OPTIONS --prop --rewriting #-}

module gy04-5 where

open import Lib

---------------------------------------
-- RazorAST
---------------------------------------

module AST where
  open import RazorAST hiding (D)

  {-
  A szintaxisban (num 2 +o num 3) ≠ (num 3 +o num 2).
  Nincs olyan modell, melyben Tm az üres halmaz.
  Van olyan modell, melyben Tm-nek végtelen sok eleme van.
  Van olyan modell, melyben num 2 = num 3.
  Van olyan modell, melyben Tm-nek 6 eleme van.
  Van olyan M modell, melyre van olyan x : M.Tm, hogy nincs t : I.Tm, melyre M.⟦ t ⟧ = x.
  Van olyan modell, melyben true = false.
  Van olyan modell, melyben Tm-nek egy eleme van.
  -}

  module isZeroInjectivity where

    D : DepModel {lzero}
    D = record
      { Tm∙     = {!!}
      ; true∙   = {!!}
      ; false∙  = {!!}
      ; ite∙    = {!!}
      ; num∙    = {!!}
      ; isZero∙ = {!!}
      ; _+o∙_   = {!!}
      }
    module D = DepModel D

    isZeroInj : ∀{t t'} → I.isZero t ≡ I.isZero t' → t ≡ t'
    isZeroInj e = cong D.⟦_⟧ e

  module isZeroInjective where

    -- Az iniciális modellben most már tudjuk, hogy az isZero injektív. Adj meg egy másik modellt, amelyben szintén injektív!
    M : Model {lzero}
    M = record
      { Tm     = ℕ
      ; true   = {!!}
      ; false  = {!!}
      ; ite    = {!!}
      ; num    = {!!}
      ; isZero = {!!}
      ; _+o_   = {!!}
      }
    open Model M

    inj : ∀ t t' → isZero t ≡ isZero t' → t ≡ t'
    inj = {!!}

  module isZeroNotInjective where

    -- Adj meg egy modellt, amelyben az isZero operátor nem injektív!
    M : Model {lzero}
    M = {!   !}
    open Model M

    notInj : ¬ (∀ t t' → isZero t ≡ isZero t' → t ≡ t')
    notInj inj = {!!}

----------------------------------------------------
-- RazorWT
----------------------------------------------------

{-

Exercise 1.33 (compulsory). Draw the derivation trees of the following terms.
isZero (num 1 + ite true (num 2) (num 3))
isZero (num 1 + (num 2 + num 3))
isZero ((num 1 + num 2) + num 3)
ite (isZero (num 0)) (num 1 + num 2) (num 3)


Exercise 1.35 (compulsory). Here are some lists of lexical elements (written as strings). They
are all accepted by the parser. Which ones are rejected by type inference? For the ones which are
accepted, write down the RazorWT terms which are produced.
if true then true else num 0
if true then num 0 else num 0
if num 0 then num 0 else num 0
if num 0 then num 0 else true
true + zero
true + num 1
true + isZero false
true + isZero (num 0)


Exercise 1.36 (compulsory). Here are some RazorAST terms. Which ones are rejected by type
inference? For the ones which are accepted, write down the RazorWT terms which are produced.
ite true true (num 0)
ite true (num 0) (num 0)
ite (num 0) (num 0) (num 0)
true +o zero
true +o num 1
true +o isZero (num 1)
isZero (num 1) +o num 0
-}

-- experiment with type inference using infer in the code

---------------------------------------
-- Razor
---------------------------------------

-- equational consistency: I.true ≠ I.false

{-
Exercise 1.39 (compulsory). Show that if true = false in a model, then any two u, v : Tm Nat are
equal in that model. A consequence is that if a compiler compiles true and false to the same code,
then it compiles all natural numbers to the same code
-}


{-
In Razor.I, you cannot count the leaves!
-}
