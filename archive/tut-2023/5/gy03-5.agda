{-# OPTIONS --prop --rewriting #-}

module gy03-5 where

open import Lib hiding (n)
open import RazorAST hiding (D)
open import Nat hiding (Model; DepModel)

-- mutasd meg, hogy true ≠ isZero (num 0) a szintaxisban! ehhez adj
-- meg egy modellt, amiben a true tt-re, az isZero (num 0) ff-re
-- ertekelodik!
TN : Model {lzero}
TN = record
  { Tm = 𝟚
  ; true = tt
  ; false = tt
  ; ite = λ _ _ _ → tt
  ; num = λ _ → tt
  ; isZero = λ _ → ff
  ; _+o_ = λ _ _ → tt
  }
true≠isZeronum0 : ¬ (I.true ≡ I.isZero (I.num 0))
true≠isZeronum0 e = tt≠ff (cong (λ n → TN.⟦ n ⟧) e) -- ≠ \ne
  where module TN = Model TN

-- nemsztenderd modell (a szintaxis ertelmezese nem rakepzes)
NS : Model {lzero}
NS = record
       { Tm = ℕ
       ; true = 2
       ; false = 0
       ; ite = λ { zero a b → b ; (suc _) a b → a }
       ; num = λ n → n + n
       ; isZero = λ { zero → 2 ; (suc _) → 0 }
       ; _+o_ = _+_
       }
module testNS where
  module NS = Model NS

  -- adj meg egy ℕ-t, amire nem kepez egyik term sem
  n : ℕ
  n = 1

  -- bizonyitsd be, hogy minden szintaktikus term ertelmezese paros szam
  ps : (t : I.Tm) → Σsp ℕ λ m → NS.⟦ t ⟧ ≡ m + m
  ps I.true = 1 , refl -- C-u C-u C-c C-, or C-y C-,
  ps I.false = {!   !}
  ps (I.ite t u v) = {!   !}
  ps (I.num x) = {!   !}
  ps (I.isZero t) with NS.⟦ t ⟧
  ... | zero = 1 , refl
  ... | suc n = 0 , refl
  ps (t I.+o u) =
    let x , e1 = ps t
        y , e2 = ps u in
    x + y , (
      NS.⟦ t ⟧ + NS.⟦ u ⟧
                          ≡⟨ cong (λ n → n + NS.⟦ u ⟧) e1 ⟩ -- \== \< \>
      (x + x) + NS.⟦ u ⟧
                          ≡⟨ cong (λ n → (x + x) + n) e2 ⟩
      (x + x) + (y + y)
                          ≡⟨ ass+ {x + x} {y} {y} ⁻¹ ⟩ -- \^- \^1
      ((x + x) + y) + y
                          ≡⟨ cong (λ n → n + y) (ass+ {x} {x} {y}) ⟩
      (x + (x + y)) + y
                          ≡⟨ cong (λ n → (x + n) + y) (+comm {x} {y}) ⟩
      (x + (y + x)) + y
                          ≡⟨ cong (λ n → n + y) (ass+ {x} {y} {x} ⁻¹) ⟩
      ((x + y) + x) + y
                          ≡⟨ ass+ {x + y} {x} {y} ⟩
      (x + y) + (x + y)
                          ∎) -- \qed
    -- \pi \_1
  -- pattern match: C-c C-c
  -- (x + x) + (y + y) = (x + y) + (x + y)

-- FEL: add meg a legegyszerubb nemsztenderd modellt!
NS' : Model {lzero}
NS' = record
  { Tm = 𝟚
  ; true = ff
  ; false = ff
  ; ite = λ _ _ _ → ff
  ; num = λ _ → ff
  ; isZero = λ _ → ff
  ; _+o_ = λ _ _ → ff
  }
module testNS' where
  module NS' = Model NS'
  b : 𝟚
  b = tt

  -- indukcio
  D : DepModel {lzero}
  D = record
    { Tm∙ = λ t → Lift (NS'.⟦ t ⟧ ≡ ff)
    ; true∙ = mk refl
    ; false∙ = mk refl
    ; ite∙ = λ _ _ _ → mk refl
    ; num∙ = λ _ → mk refl
    ; isZero∙ = λ _ → mk refl
    ; _+o∙_ = λ _ _ → mk refl
    }
  module D = DepModel D

  ∀ff : (t : I.Tm) → NS'.⟦ t ⟧ ≡ ff
  ∀ff t = un D.⟦ t ⟧

  ns : (Σsp I.Tm λ t → NS'.⟦ t ⟧ ≡ tt) → ⊥
  ns e = tt≠ff (π₂ e ⁻¹ ◾ ∀ff (π₁ e))

Idr : Nat.DepModel {lzero}
Idr = record { Nat∙ = λ n → n + 0 ≡ n ; Zero∙ = refl ; Suc∙ = λ e → cong suc e }
module Idr = Nat.DepModel Idr

+-idr : (n : ℕ) → n + 0 ≡ n
+-idr n = Idr.⟦ n ⟧

L1' : Model
L1' = record
  { Tm     = ℕ
  ; true   = 1
  ; false  = 1
  ; ite    = λ t t' t'' → t + t' + t''
  ; num    = λ _ → 1
  ; isZero = λ t → t
  ; _+o_   = _+_
  }
module L1' = Model L1'
L2' : Model
L2' = record
  { Tm     = ℕ
  ; true   = 0
  ; false  = 0
  ; ite    = λ t t' t'' → 2 + t + t' + t''
  ; num    = λ _ → 0
  ; isZero = λ t → t
  ; _+o_   = λ t t' → 1 + t + t'
  }
module L2' = Model L2'

L1L2' : DepModel {lzero}
L1L2' = record
  { Tm∙ = λ t → Lift (L1'.⟦ t ⟧ ≡ suc L2'.⟦ t ⟧)
  ; true∙ = mk refl
  ; false∙ = {!   !}
  ; ite∙ = {!   !}
  ; num∙ = {!   !}
  ; isZero∙ = {!   !}
  ; _+o∙_ = λ {u} e1 {v} e2 → mk (
    L1'.⟦ u ⟧ + L1'.⟦ v ⟧
                                      ≡⟨ cong (λ n → n + L1'.⟦ v ⟧) (un e1) ⟩
    suc L2'.⟦ u ⟧ + L1'.⟦ v ⟧
                                      ≡⟨ refl ⟩
    suc (L2'.⟦ u ⟧ + L1'.⟦ v ⟧)
                                      ≡⟨ cong (λ x → suc (L2'.⟦ u ⟧ + x)) (un e2) ⟩
    suc (L2'.⟦ u ⟧ + suc L2'.⟦ v ⟧)
                                      ≡⟨ cong suc +suc ⟩
    suc (suc (L2'.⟦ u ⟧ + L2'.⟦ v ⟧))
                                      ∎)
  } -- mk / un
module L1L2' = DepModel L1L2'

twolengths : ∀ t → L1'.⟦ t ⟧ ≡ suc L2'.⟦ t ⟧
twolengths t = un L1L2'.⟦ t ⟧
