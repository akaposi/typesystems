{-# OPTIONS --prop --rewriting #-}

module hf09-5 where

open import Lib
open import STT.Syntax
open import STT.DepModel

------------------------------------------------------------------------
-- Give the most general type for the following terms

term1 : {!   !}
term1 = lam (lam (lam (ite v0 (v1 $ num 1) (isZero v2))))

term2 : {!   !}
term2 = lam (lam (v0 $ v1))

term3 : {!   !}
term3 = lam (lam (ite (isZero v2) (v1 $ false $ v0) v0))

term4 : {!   !}
term4 = lam (v0 $ lam v0)

term5 : {!   !}
term5 = lam (lam (v0 $ v1 $ lam (isZero (v2 $ v0))))

------------------------------------------------------------------------
-- (for testing)
St' : DepModel
St' = record
  { Ty∙ = λ _ → Set ; Nat∙ = ℕ ; Bool∙ = 𝟚
  ; Con∙ = λ _ → Set ; ◇∙ = Lift ⊤ ; _▹∙_ = _×_
  ; Var∙ = λ Γ A _ → Γ → A ; vz∙ = π₂ ; vs∙ = λ a (γ , b) → a γ
  ; Tm∙ = λ Γ A _ → Γ → A ; Sub∙ = λ Δ Γ _ → Δ → Γ
  ; p∙ = π₁ ; ⟨_⟩∙ = λ a γ → γ , a γ ; _⁺∙ = λ γ (δ , a) → γ δ , a
  ; var∙ = λ a → a ; _[_]∙ = λ a γ δ → a (γ δ) ; [p]∙ = refl
  ; vz[⟨⟩]∙ = refl ; vz[⁺]∙ = refl ; vs[⟨⟩]∙ = refl ; vs[⁺]∙ = refl
  ; true∙ = λ _ → tt ; false∙ = λ _ → ff
  ; ite∙ = λ x y z γ → if x γ then y γ else z γ ; iteβ₁∙ = refl ; iteβ₂∙ = refl
  ; true[]∙ = refl ; false[]∙ = refl ; ite[]∙ = refl
  ; num∙ = λ n _ → n ; isZero∙ = λ x γ → x γ == 0 ; _+o∙_ = λ x y γ → x γ + y γ
  ; isZeroβ₁∙ = refl ; isZeroβ₂∙ = refl ; +β∙ = refl
  ; num[]∙ = refl ; isZero[]∙ = refl ; +[]∙ = refl
  ; _⇒∙_ = λ Δ Γ → Δ → Γ
  ; lam∙ = λ b γ a → b (γ , a) ; _$∙_ = λ f a γ → f γ (a γ)
  ; ⇒β∙ = refl ; ⇒η∙ = refl ; lam[]∙ = refl ; $[]∙ = refl
  }
module St = DepModel St'
------------------------------------------------------------------------
-- Define the following functions

-- Exclusive or: Return true when one side is true, return false when both are
-- true or both are false

-- Equations that should hold:
-- xor $ true  $ true  ≡ false
-- xor $ true  $ false ≡ true
-- xor $ false $ true  ≡ true
-- xor $ false $ false ≡ false

xor : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
xor = {!   !}

xor-test1 : St.⟦ xor $ true $ true ⟧t (mk triv) ≡ ff
xor-test1 = refl

xor-test2 : St.⟦ xor $ true $ false ⟧t (mk triv) ≡ tt
xor-test2 = refl

xor-test3 : St.⟦ xor $ false $ true ⟧t (mk triv) ≡ tt
xor-test3 = refl

xor-test4 : St.⟦ xor $ false $ false ⟧t (mk triv) ≡ ff
xor-test4 = refl

------------------------------------

-- Apply the function to the last parameter if given true, return the
-- last parameter unchanged if given false

-- applyWhen $ true  $ lam (num 1 +o v0) $ num 10 ≡ num 11
-- applyWhen $ false $ lam (num 1 +o v0) $ num 10 ≡ num 10

applyWhen : ∀ {A} → Tm ◇ (Bool ⇒ (A ⇒ A) ⇒ A ⇒ A)
applyWhen = {!   !}

applyWhen-test1 :
  St.⟦ applyWhen $ true $ lam (num 1 +o v0) $ num 10 ⟧t (mk triv) ≡ 11
applyWhen-test1 = refl

applyWhen-test2 :
  St.⟦ applyWhen $ false $ lam (num 1 +o v0) $ num 10 ⟧t (mk triv) ≡ 10
applyWhen-test2 = refl

------------------------------------

-- Given a function with two parameters, return a function with the parameters
-- swapped

-- flip $ lam (lam (v1 +o v1 +o v0)) $ num 10 $ num  1 ≡ num 12
-- flip $ lam (lam (v1 +o v1 +o v0)) $ num  1 $ num 10 ≡ num 21

flip : ∀ {A B C} → Tm ◇ ((A ⇒ B ⇒ C) ⇒ (B ⇒ A ⇒ C))
flip = {!   !}

flip-test1 :
  St.⟦ flip $ lam (lam (v1 +o v1 +o v0)) $ num 10 $ num 1 ⟧t (mk triv) ≡ 12
flip-test1 = refl

flip-test2 :
  St.⟦ flip $ lam (lam (v1 +o v1 +o v0)) $ num 1 $ num 10 ⟧t (mk triv) ≡ 21
flip-test2 = refl

------------------------------------

-- Multiplication with one external parameter, define it by pattern matching and
-- recursion

-- mul 2 $ num 2 ≡ num  4
-- mul 2 $ num 5 ≡ num 10
-- mul 5 $ num 2 ≡ num 10
-- mul 5 $ num 5 ≡ num 25

mul : ∀ {Γ} → ℕ → Tm Γ (Nat ⇒ Nat)
mul = {!   !}

mul-test1 : St.⟦ mul {◇} 2 $ num 2 ⟧t (mk triv) ≡ 4
mul-test1 = refl

mul-test2 : St.⟦ mul {◇} 2 $ num 5 ⟧t (mk triv) ≡ 10
mul-test2 = refl

mul-test3 : St.⟦ mul {◇} 5 $ num 2 ⟧t (mk triv) ≡ 10
mul-test3 = refl

mul-test4 : St.⟦ mul {◇} 5 $ num 5 ⟧t (mk triv) ≡ 25
mul-test4 = refl
