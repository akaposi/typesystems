{-# OPTIONS --prop --rewriting #-}

module gy13-5 where

open import Lib

module Ind where
  open import Ind
  open import Ind.Syntax
  open Norm

  -- Exercise 5.6 (compulsory). List the rules for inductively defined trees no
  -- information at the leaves and infinity (Nat-ary) branching.
  postulate
    Tree1 : Ty
    leaf1 : ∀ {Γ} → Tm Γ Tree1
    node1 : ∀ {Γ} → Tm Γ (Nat ⇒ Tree1) → Tm Γ Tree1
    -- A ×o A ≅ (Bool ⇒ A)   -- a*a = a^2
    -- (A ×o A) ⇒ (Bool ⇒ A)
    -- (Bool ⇒ A) ⇒ A ×o A
    -- Tree1 ×o Tree1 ×o ... ×o Tree1
    -- Nat ⇒ Tree1

    iteTree1 : ∀ {Γ A} → Tm Γ A → Tm (Γ ▹ (Nat ⇒ A)) A → Tm Γ Tree1 → Tm Γ A
    Tree1β₁ : ∀ {Γ A l n} → iteTree1 {Γ} {A} l n leaf1 ≡ l
    Tree1β₂ : ∀ {Γ A l n f} →
      iteTree1 {Γ} {A} l n (node1 f) ≡
      n [ ⟨ lam (iteTree1 (l [ p ]) (n [ p ⁺ ]) (f [ p ] $ v0)) ⟩ ]

  -- Exercise 5.7 (compulsory). For types A, B, list the rules for the inductive
  -- type with only leaves (and no nodes) that contain an A and a B.
  postulate
    Prod : Ty → Ty → Ty
    pair : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (Prod A B)
    iteProd : ∀ {Γ A B C} → Tm (Γ ▹ A ▹ B) C → Tm Γ (Prod A B) → Tm Γ C
    Prodβ : ∀ {Γ A B C a b c} →
      iteProd {Γ}{A}{B}{C} c (pair a b) ≡ c [ ⟨ a ⟩ ⁺ ] [ ⟨ b ⟩ ]

  {-
  _×o_ , _,o_ , fst , snd

  Prod A B = A ×o B
  pair a b = a ,o b
  iteProd c t = c [ ⟨ fst t ⟩ ⁺ ] [ ⟨ snd t ⟩ ]
  -}
  {-
  Prod, pair, iteProd

  _×o_ = Prod
  _,_ = pair

  fst : Tm Γ (A ×o B) → Tm Γ A
  fst t = iteProd v1 t

  snd t = iteProd v0 t
  -}

  -- Exercise 5.8 (compulsory). For types A, B, list the rules for the inductive
  -- type with two kinds of leaves and no nodes. One kind of leaf contains an A,
  -- the other kind of leaf contains a B.

  -- Exercise 5.9 (compulsory). How many elements are in the inductive type with
  -- only the following constructors?

  postulate
    SpecialTree : Ty
    node1' : ∀ {Γ} → Tm Γ SpecialTree → Tm Γ SpecialTree
    node2' : ∀ {Γ} → Tm Γ SpecialTree → Tm Γ SpecialTree → Tm Γ SpecialTree

  test : Tm ◇ SpecialTree
  test = node1' (node1' (node1' {!   !}))
  -- SpecialTree ≅ ⊥

  -- Exercise 5.14. What are the normal form of the following terms?
  t9' : Σsp (Nf (◇ ▹ Nat) Bool) λ v → ⌜ v ⌝Nf ≡ lam (iteNat true false v0) $ v0 -- (v0 [ ⟨ v0 ⟩ ])
  t9' = neuBool (iteNatNe trueNf falseNf (varNe vz)) , {!   !}

module Coind where
  open import Coind.Syntax

  numbers : Tm ◇ (Stream Nat)
  numbers = genStream v0 (suco v0) zeroo

  numbers-test : head (tail numbers) ≡ suco zeroo
  numbers-test =
    head (tail numbers)
      ≡⟨ refl ⟩

    head (tail (genStream v0 (suco v0) zeroo))
      ≡⟨ cong head Streamβ₂ ⟩

    head (genStream v0 (suco v0) (suco v0 [ ⟨ zeroo ⟩ ]))
      ≡⟨ Streamβ₁ ⟩

    v0 [ ⟨ suco v0 [ ⟨ zeroo ⟩ ] ⟩ ]
      ≡⟨ vz[⟨⟩] ⟩

    suco v0 [ ⟨ zeroo ⟩ ]
      ≡⟨ suc[] ⟩

    suco (v0 [ ⟨ zeroo ⟩ ])
      ≡⟨ cong suco vz[⟨⟩] ⟩

    suco zeroo
      ∎

  repeat : ∀ {Γ A} → Tm Γ (A ⇒ Stream A)
  repeat = lam (genStream v0 v0 v0)

  repeat-test : head (tail (tail (repeat {◇} $ true))) ≡ true
  repeat-test = {!   !}

  postulate
    plus : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
    num : ∀ {Γ} → ℕ → Tm Γ Nat

  adder : Tm ◇ Machine
  adder = genMachine (plus $ v1 $ v0) zeroo v0 zeroo

  adder-example : Tm ◇ Nat
  adder-example = get (put (set (put (put adder (num 2)) (num 4))) (num 3))

  -- Exercise 6.2 (compulsory). For types A, B, define the coinductive type of
  -- machines from which we can get out an A and a B. List all the rules.
  postulate
    Prod : Ty → Ty → Ty
    first : ∀ {Γ A B} → Tm Γ (Prod A B) → Tm Γ A
    second : ∀ {Γ A B} → Tm Γ (Prod A B) → Tm Γ B
    genProd : ∀ {Γ A B C} → Tm (Γ ▹ C) A → Tm (Γ ▹ C) B → Tm Γ C → Tm Γ (Prod A B)
    -- first (genProd a b c) ≡ a [ ⟨ c ⟩ ]
    -- second (genProd a b c) ≡ b [ ⟨ c ⟩ ]

  -- Exercise 6.4 (compulsory). For types A, B, define the coinductive type of
  -- machines which has one operation: it receives an input of type A, and
  -- outputs something of type B. List all the rules.
  -- Fun : Ty → Ty → Ty
  -- app : Tm Γ (Fun A B) → Tm Γ A → Tm Γ B
  -- genFun : Tm (Γ ▹ C ▹ A) B → Tm Γ C → Tm Γ (Fun A B)

  -- Exercise 6.6 (compulsory). Define the coinductive types of machines for
  -- which there are two different ways to input a natural number and if we
  -- press a button, they output a natural number. Implement a machine which has
  -- two numbers in its inner state and outputs one or the other alternating. If
  -- we input a number in the first way, the machine adds that to its first
  -- inner number. If we input a number in the second way, the machine adds that
  -- to its second inner number.
