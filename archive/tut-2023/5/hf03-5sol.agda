{-# OPTIONS --prop --rewriting #-}

module hf03-5sol where

open import Lib hiding (ass+; +suc; +comm)
open import Nat hiding (M)

-- a *2+1 Nat modell
-- Definiáld azt a modellt, amely az "értelemszerűen" ábrázolt számot megszorozza 2-vel,
-- majd hozzáad 1-et.
M : Model {lzero}
M = record
  { Nat  = ℕ
  ; Zero = 1
  ; Suc  = λ n → suc (suc n)
  }
module M = Model M

-- néhány teszteset
testM : M.⟦ 3 ⟧ ≡ 3 * 2 + 1
testM = refl
testM' : M.⟦ 5 ⟧ ≡ 5 * 2 + 1
testM' = refl
testM'' : M.⟦ 10 ⟧ ≡ 21
testM'' = refl

-- Bizonyítsd be, hogy az ebbe a modellbe való kiértékelés tényleg beszoroz 2-vel és hozzáad egyet.
D : DepModel {lzero}
D = record
  { Nat∙ = λ n → M.⟦ n ⟧ ≡ n * 2 + 1
  ; Zero∙ = refl
  ; Suc∙ = λ e → cong (λ n → suc (suc n)) e
  }
module D = DepModel D
M=*2+1 : (n : ℕ) → M.⟦ n ⟧ ≡ n * 2 + 1
M=*2+1 n = D.⟦ n ⟧

-- összeadás asszociatív
D'' : DepModel {lzero}
D'' = record
  { Nat∙ = λ m → ((n o : ℕ) → (m + n) + o ≡ m + (n + o))
  ; Zero∙ = λ n o → refl
  ; Suc∙ = λ {m} e n o → cong suc (e n o)
  }
module D'' = DepModel D''
ass+ : (m n o : ℕ) → (m + n) + o ≡ m + (n + o)
ass+ m n o = D''.⟦ m ⟧ n o

-- suc a második tagból kiemelhető.
D''' : DepModel {lzero}
D''' = record
  { Nat∙ = λ m → (∀ n → m + suc n ≡ suc m + n)
  ; Zero∙ = λ n → refl
  ; Suc∙ = λ e n → cong suc (e n)
  }
module D''' = DepModel D'''
+suc : (m n : ℕ) → m + suc n ≡ suc m + n
+suc m n = D'''.⟦ m ⟧ n

-- összeadás kommutatív
-- használd "idr+"-t, "+suc"-ot, "_⁻¹"-t
D'''' : DepModel
D'''' = record
  { Nat∙ = λ m → ∀ n → m + n ≡ n + m
  ; Zero∙ = λ n → idr+ ⁻¹
  ; Suc∙ = λ {m} e n →
    suc (m + n)
                    ≡⟨ cong suc (e n) ⟩
    suc (n + m)
                    ≡⟨ refl ⟩
    suc n + m
                    ≡⟨ +suc n m ⁻¹ ⟩
    n + suc m
                    ∎
  }
module D'''' = DepModel D''''
+comm : (m n : ℕ) → m + n ≡ n + m
+comm m n = D''''.⟦ m ⟧ n

-- valami egyenlőség
D''''' : DepModel {lzero}
D''''' = record
  { Nat∙ = λ n → n + 3 ≡ 1 + n + 2
  ; Zero∙ = refl
  ; Suc∙ = λ e → cong suc e
  }
module D''''' = DepModel D'''''
+3=1++2 : (n : ℕ) → n + 3 ≡ 1 + n + 2
+3=1++2 n = D'''''.⟦ n ⟧

D'''''' : DepModel {lzero}
D'''''' = record
  { Nat∙ = λ m → ∀ n o → (m + n) * o ≡ (m * o) + (n * o)
  ; Zero∙ = λ n o → refl
  ; Suc∙ = λ {m} e n o →
    o + ((m + n) * o)
                            ≡⟨ cong (λ n → o + n) (e n o) ⟩
    o + ((m * o) + (n * o))
                            ≡⟨ ass+ o (m * o) (n * o) ⁻¹ ⟩
    (o + (m * o)) + (n * o)
                            ∎
  }
module D'''''' = DepModel D''''''
*-distr-+ : (m n o : ℕ) → (m + n) * o ≡ (m * o) + (n * o)
*-distr-+ m n o = D''''''.⟦ m ⟧ n o
