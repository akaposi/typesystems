{-# OPTIONS --prop --rewriting #-}

module gy12-5 where

open import Lib

module F where
  open import Fin
  open import Fin.Syntax

  -- Exercise 4.11 (compulsory). Show that for any type A, A ×o Unit is
  -- isormophic to A.
  ×-idr : ∀ {A} → A ×o Unit ≅ A
  ×-idr = record
    { f = fst v0
    ; g = v0 ,o trivial
    ; fg = {!   !}
    ; gf = {!   !} }

  -- Show that for any type A, A +o Empty is isormophic to A.
  +-idr : ∀ {A} → A +o Empty ≅ A
  +-idr = {!   !}

module _ where
  open import Ind
  open import Ind.Syntax

  -- Exercise 5.3 (compulsory). Define num as syntactic sugar
  num : ∀ {Γ} → ℕ → Tm Γ Nat
  num = {!   !}

  -- Define the follwing functions using iterators
  isZero : ∀ {Γ} → Tm Γ (Nat ⇒ Bool)
  isZero = {!   !}

  isZeroβ₁ : ∀ {Γ} → isZero {Γ} $ num 0 ≡ true
  isZeroβ₁ = {!   !}

  plus : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
  plus = {!   !}

  length : ∀ {Γ A} → Tm Γ (List A ⇒ Nat)
  length = {!   !}

  length-test : length {◇} $ (cons false nil) ≡ num 1
  length-test = {!   !}

  -- Exercise 5.4 (compulsory). List the rules for inductively defined trees
  -- with ternary branching, a boolean at each node and a natural number at each
  -- leaf.

  -- Exercise 5.5 (compulsory). List the rules for inductively defined trees
  -- with two kinds of nullary, one kind of binary and three kinds of ternary
  -- branching. There is no extra information at leaves or nodes.

  -- Exercise 5.6 (compulsory). List the rules for inductively defined trees no
  -- information at the leaves and infinity (Nat-ary) branching.

  -- Exercise 5.7 (compulsory). For types A, B, list the rules for the inductive
  -- type with only leaves (and no nodes) that contain an A and a B. Derive all
  -- its rules from binary products of the previous section. Which rule of
  -- binary products cannot be derived from this inductive type?

  -- Exercise 5.8 (compulsory). For types A, B, list the rules for the inductive
  -- type with two kinds of leaves and no nodes. One kind of leaf contains an A,
  -- the other kind of leaf contains a B. Derive all the rules from binary sums
  -- of the previous section. Which rule of binary sums cannot be derived from
  -- this inductive type?

  -- Exercise 5.9 (compulsory). How many elements are in the inductive type with
  -- only the following constructors?

  -- SpecialTree : Ty
  -- node1 : Tm Γ SpecialTree → Tm Γ SpecialTree
  -- node2 : Tm Γ SpecialTree → Tm Γ SpecialTree → Tm Γ SpecialTree
