{-# OPTIONS --prop --rewriting #-}

module gy08-5 where

open import Lib

module Eqs where
  open import Def.Syntax

  t : Tm (◇ ▹ Bool) Nat
  t = ite v0 (num 1) (num 0)

  γ : Sub ◇ (◇ ▹ Bool)
  γ = ⟨ true ⟩

  t[γ] : t [ γ ] ≡ num 1
  t[γ] =
    t [ γ ]
      ≡⟨ refl ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 1
      ∎

  δ' : Sub (◇ ▹ Bool) (◇ ▹ Bool ▹ Nat)
  δ' = ⟨ ite v0 (num 1) (num 2) ⟩

  δ : Sub (◇ ▹ Bool ▹ Nat) (◇ ▹ Bool ▹ Nat ▹ Nat)
  δ = ⟨ num 2 ⟩

  u : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Nat
  u = v0 +o v1

  u[δ][δ'] : u [ δ ] [ δ' ] ≡ num 2 +o ite v0 (num 1) (num 2)
  u[δ][δ'] =
    u [ δ ] [ δ' ]
      ≡⟨ refl ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 2 +o ite v0 (num 1) (num 2)
      ∎

  tm-1 : Tm ◇ Bool
  tm-1 = def true v0

  eq-1 : tm-1 ≡ true
  eq-1 =
    def true v0
      ≡⟨ {!   !} ⟩
    true
      ∎

  tm-2 : Tm ◇ Bool
  tm-2 = def true (def false v0)

  eq-2 : tm-2 ≡ false
  eq-2 =
    def true (def false v0)
      ≡⟨ {!   !} ⟩
    false
      ∎

  tm-3 : Tm ◇ Bool
  tm-3 = def true (def false v1)

  eq-3 : tm-3 ≡ true
  eq-3 =
    def true (def false v1)
      ≡⟨ {!   !} ⟩
    true
      ∎

  tm-4 : Tm ◇ Nat
  tm-4 = def (num 4) (v0 +o v0)

  eq-4 : tm-4 ≡ num 8
  eq-4 = {!   !}

  tm-5 : Tm (◇ ▹ Nat) Nat
  tm-5 = def v0 (v0 +o v1)

  eq-5 : tm-5 ≡ v0 +o v0
  eq-5 = {!   !}

  tm-6 : Tm (◇ ▹ Nat ▹ Nat ▹ Nat) Nat
  tm-6 = v0 +o v1 +o v2

  eq-6 : tm-6 [ ⟨ num 1 ⟩ ⁺ ] ≡ v0 +o num 1 +o v1
  eq-6 = {!   !}

  tm-7 : Tm (◇ ▹ Bool ▹ Nat) Nat
  tm-7 = ite v1 (v0 +o v0) v0

  eq-7 : tm-7 [ {!   !} ] [ {!   !} ] ≡ num 2
  eq-7 = {!   !}

  tm-8 : Tm ◇ Nat
  tm-8 = def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))

  eq-8 : tm-8 ≡ num 3
  eq-8 = {!   !}

  tm-9 : Tm ◇ Nat
  tm-9 = def (def (num 1) (v0 +o num 2)) (def (num 2 +o v0) (v1 +o v0))

  eq-9 : tm-9 ≡ num 5
  eq-9 = {!   !}

  tm-10 : Tm ◇ Bool
  tm-10 = def (num 0) (def (isZero v0) (ite v0 false true))

  eq-10 : tm-10 ≡ false
  eq-10 = {!   !}

module Std where
  open import Def.Model

  Standard : Model {lsuc lzero} {lzero}
  Standard = {!   !}

  open Model Standard

module Norm where
  open import Def
  open import Def.Syntax
