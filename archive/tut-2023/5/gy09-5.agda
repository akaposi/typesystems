{-# OPTIONS --prop --rewriting #-}

module gy09-5 where

open import Lib

module Std where
  open import Def.Model

  Standard : Model {lsuc lzero} {lzero}
  Standard = record
    { Ty = Set
    ; Nat = ℕ
    ; Bool = 𝟚
    ; Con = Set
    ; ◇ = Lift ⊤
    ; _▹_ = λ Γ A → Γ × A
    ; Var = λ Γ A → Γ → A
    ; vz = λ (γ , a) → a
    ; vs = λ a (γ , b) → a γ
    ; Tm = λ Γ A → Γ → A
    ; Sub = λ Δ Γ → Δ → Γ
    ; p = λ (γ , a) → γ
    ; ⟨_⟩ = λ a γ → γ , a γ
    ; _⁺ = λ γ (δ , a) → γ δ , a
    ; var = λ a → a
    ; _[_] = λ a γ δ → a (γ δ)
    ; [p] = refl
    ; vz[⟨⟩] = refl
    ; vz[⁺] = refl
    ; vs[⟨⟩] = refl
    ; vs[⁺] = refl
    ; true = λ γ → tt
    ; false = λ γ → ff
    ; ite = λ a b c γ → if a γ then b γ else c γ
    ; iteβ₁ = refl
    ; iteβ₂ = refl
    ; true[] = refl
    ; false[] = refl
    ; ite[] = refl
    ; num = λ n γ → n
    ; isZero = λ a γ → a γ == 0
    ; _+o_ = λ a b γ → a γ + b γ
    ; isZeroβ₁ = refl
    ; isZeroβ₂ = refl
    ; +β = refl
    ; num[] = refl
    ; isZero[] = refl
    ; +[] = refl
    }

  open Model Standard

  tm-7 : Tm ◇ Nat
  tm-7 = def false (def (num 2) (ite (var (vs vz)) (num 0) (num 1 +o var vz)))

  eq-7 : tm-7 ≡ num 3
  eq-7 = refl

module Norm where
  open import Def hiding (t1; t2; t3; t4; t5; t6)
  open import Def.Syntax

  {-
  Exercise 2.12. What is the normal form of the following terms?
  -}
  t6 : Σsp (Nf (◇ ▹ Nat) Bool) λ v → ⌜ v ⌝Nf ≡ isZero (num 1 +o num 2)
  t6 = falseNf , (cong isZero +β ◾ isZeroβ₂) ⁻¹

  t1 : Σsp (Nf (◇ ▹ Nat) Nat) λ v → ⌜ v ⌝Nf ≡ v0 +o (num 1 +o num 2)
  t1 = neu (varNe vz +NeNf 3) , cong (λ x → var vz +o x) +β ⁻¹

  -- check what eval, norm and ⌜norm⌝ gives for this term
  t : Tm ◇ Bool
  t = isZero (ite true (num 1) (num 0))

  evalt : Lift ⊤ → 𝟚
  evalt = St.⟦ t ⟧t

  normt : Nf ◇ Bool
  normt = norm t

  ⌜norm⌝t : Tm ◇ Bool
  ⌜norm⌝t = ⌜ norm t ⌝Nf

  complt : ⌜ norm t ⌝Nf ≡ t
  complt = compl t

  -- check what eval, norm and ⌜norm⌝ gives for this term
  t' : Tm (◇ ▹ Nat) Bool
  t' = isZero (ite (isZero (num 2 +o num 3)) v0 (v0 +o num 2))

  evalt1 : Lift ⊤ × ℕ → 𝟚
  evalt1 = St.⟦ t' ⟧t

  normt1 : Nf (◇ ▹ Nat) Bool
  normt1 = norm t'

  ⌜norm⌝t1 : Tm (◇ ▹ Nat) Bool
  ⌜norm⌝t1 = ⌜ norm t' ⌝Nf

  complt1 : ⌜ norm t' ⌝Nf ≡ t'
  complt1 = compl t'

  -- How to use completeness
  t2' t3' : Tm ◇ Bool
  t2' = def true (def false v1)
  t3' = true

  var-eq-3' : t2' ≡ t3'
  var-eq-3' = compl t2' ⁻¹

  t4' t5' : Tm ◇ Nat
  t4' = def (false) (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
  t5' = ite true (num 3) (num 0)

  qwe : def {◇} (false) (def (num 2) (ite v1 (num 0) (num 1 +o v0))) ≡ ite true (num 3) (num 0)
  qwe = {!   !}

  -- prove that two terms are equal using compl
  var-eq-5' : t4' ≡ t5'
  var-eq-5' =
    def (false) (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
      ≡⟨ compl t4' ⁻¹ ⟩
    num 3
      ≡⟨ compl t5' ⟩
    ite true (num 3) (num 0)
      ∎

module Func where
  open import STT hiding (and; or; neg; isZero'; add'; ite'; id; comp; idl; idr; ass)
  open Model I

    -- ⇒η     : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → t ≡ lam (t [ p ] $ var vz) -- t ≡ (λ x. t x)
  {-
  Exercise 3.1 (compulsory). Derive the typing of
  apply2x : Tm ⋄ ((Nat ⇒ Nat) ⇒ Nat ⇒ Nat)  -- ~ (Nat ⇒ Nat) × Nat ⇒ Nat
  apply2x = lam (lam (v1 $ (v1 $ v0))) -- λf.λx. f(f(x))
  -}

  {-
  Exercise 3.2 (compulsory). What is the type of the following terms?
  lam (isZero (ite v0 (num 1) (num 0)))
  lam (lam (ite v0 (isZero v1) false))
  lam (lam (v1 +o (v0 $ v1)))
  lam (lam (num 1 +o (v1 $ v0)))
  -}

  e1 : ∀{Γ} → Tm Γ (Bool ⇒ Bool)
  e1 = lam (isZero (ite v0 (num 1) (num 0)))

  e2 : ∀{Γ} → Tm Γ (Nat ⇒ Bool ⇒ Bool)
  e2 = lam (lam (ite v0 (isZero v1) false))

  e3 : ∀{Γ} → Tm Γ (Nat ⇒ (Nat ⇒ Nat) ⇒ Nat)
  e3 = lam (lam (v1 +o (v0 $ v1)))

  e4 : ∀{Γ A} → Tm Γ ((A ⇒ Nat) ⇒ A ⇒ Nat)
  e4 = lam (lam (num 1 +o (v1 $ v0)))

  -- Exercise 3.4 (compulsory). Define the following functions on booleans.
  and : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
  and = lam (lam (ite v1 v0 false))

  or : Tm ◇ (Bool ⇒ Bool ⇒ Bool)
  or = lam (lam (ite v1 true v0))

  neg : Tm ◇ (Bool ⇒ Bool)
  neg = lam (ite v0 false true)

  -- Exercise 3.8 (compulsory). How many different elements does
  -- Tm ◇ (Bool ⇒ Bool) have? Define them!
  f1 : Tm ◇ (Bool ⇒ Bool)
  f1 = lam {!   !}

  -- Exercise 3.9 (compulsory).
  -- Compare the sets Tm ⋄ (Bool ⇒ Bool) and (Tm ⋄ Bool → Tm ⋄ Bool).

  -- Exercise 3.10 (compulsory). Internalise isZero, that is, define isZero' as
  -- below. Similarly, internalise the functions _+o_ and ite.
  isZero' : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
  isZero' = {!   !}

  add' : ∀{Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
  add' = {!   !}

  ite' : ∀{Γ A} → Tm Γ (Bool ⇒ A ⇒ A ⇒ A)
  ite' = {!   !}

  -- Externalise `neg` and `and`.
  neg' : ∀{Γ} → Tm Γ Bool → Tm Γ Bool
  neg' = {!   !}

  and' : ∀{Γ} → Tm Γ Bool → Tm Γ Bool → Tm Γ Bool
  and' = {!   !}

  -- Exercise 3.3 (compulsory). Prove
  apply2x-ex : apply2x $ add2 $ num 3 ≡ num 7
  apply2x-ex =
    apply2x $ add2 $ num 3
      ≡⟨ refl ⟩
    (lam (lam (v1 $ (v1 $ v0))) $ lam (v0 +o num 2)) $ num 3
      ≡⟨ cong (_$ num 3) ⇒β ⟩
    (lam (v1 $ (v1 $ v0)) [ ⟨ lam (v0 +o num 2) ⟩ ]) $ num 3
      ≡⟨ cong (_$ num 3) lam[] ⟩
    (lam ((v1 $ (v1 $ v0)) [ ⟨ lam (v0 +o num 2) ⟩ ⁺ ])) $ num 3
      ≡⟨ {!   !} ⟩
    {!   !}
      ∎

  -- Exercise 3.5 (compulsory). Define the identity function and function
  -- composition, and show the following equations relating them.
  id : ∀{Γ A} → Tm Γ (A ⇒ A)
  id = {!   !}

  comp : ∀{Γ A B C} → Tm Γ ((B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  comp = {!   !}

  idl : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → comp $ id $ t ≡ t
  idl = {!   !}

  idr : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → comp $ t $ id ≡ t
  idr = {!   !}

  assoc : ∀{Γ A B C D}{t : Tm Γ (A ⇒ B)}{t' : Tm Γ (B ⇒ C)}{t'' : Tm Γ (C ⇒ D)} →
          comp $ (comp $ t'' $ t') $ t ≡ comp $ t'' $ (comp $ t' $ t)
  assoc = {!   !}
