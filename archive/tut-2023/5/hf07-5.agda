{-# OPTIONS --prop --rewriting #-}

module hf07-5 where

open import Lib
open import DefWT

open I

-- Write down the terms as well-typed abstract binding trees
-- Give the most general type

-- ite a b (let x := ite a (num 0) (num 1) in b +o x)
q1 : {!   !}
q1 = {!   !}

-- (let x := x +o y in x +o y) +o (let y := x +o y in x +o y)
q2 : {!   !}
q2 = {!   !}

-- isZero (x +o let x := (let x := x in x +o x) in x +o x)
q3 : {!   !}
q3 = {!   !}

-- isZero (y +o let x := (let x := y in x +o y) in x +o y)
q4 : {!   !}
q4 = {!   !}

-- let a := ite c a (num 0) in ite (isZero a) b b
q5 : {!   !}
q5 = {!   !}

-- let z := isZero z in let x := y in ite z false true
q6 : {!   !}
q6 = {!   !}

-- let a := true in let b := isZero x in ite b (ite a c d) d
q7 : {!   !}
q7 = {!   !}
