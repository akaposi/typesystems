{-# OPTIONS --prop --rewriting #-}

module gy05-5 where

open import Lib

module ZH where
  open import RazorAST

  -- Prove that I.ite is injective in its 2nd parameter by giving a dependent
  -- model.
  M : DepModel {lzero}
  M = record
    { Tm∙ = λ _ → I.Tm
    ; true∙ = I.true
    ; false∙ = I.true
    ; ite∙ = λ {t} x {u} y {v} z → u
    ; num∙ = λ _ → I.true
    ; isZero∙ = λ _ → I.true
    ; _+o∙_ = λ _ _ → I.true
    }
  module M = DepModel M

  ite-inj2 : (x y y' z : I.Tm) → I.ite x y z ≡ I.ite x y' z → y ≡ y'
  ite-inj2 x y y' z e = cong M.⟦_⟧ e

  -- M.⟦ I.ite x y z ⟧ ≡ M.ite· {x} M.⟦ x ⟧ {y} M.⟦ y ⟧ {z} M.⟦ z ⟧

module type-inference where

  import RazorAST
  open import RazorWT
  open RazorWT.I

  -- tipuskikovetkeztetes primitiv hibauzenetekkel

  data Result (A : Set) : Set where
    Ok : (r : A) → Result A
    Err : (e : ℕ) → Result A

  {-
    Error codes:
    · 1: Non boolean condition
    · 2: Differing branch types
    · 3: Non numeric isZero parameter
    · 4: Non numeric addition parameter
  -}

  Infer : RazorAST.Model {lzero}
  Infer = record
    { Tm      = Result (Σ Ty (λ A → Tm A)) -- WT-beli Ty, WT-beli Tm
    ; true    = Ok (Bool , true)
    ; false   = Ok (Bool , false)
    ; ite     =
      λ { (Ok (Nat , x)) (Ok y) (Ok z) → Err 1
        ; (Ok (Bool , x)) (Ok (Nat , y)) (Ok (Nat , z)) → Ok (Nat , ite x y z)
        ; (Ok (Bool , x)) (Ok (Nat , y)) (Ok (Bool , z)) → Err 2
        ; (Ok (Bool , x)) (Ok (Bool , y)) (Ok (Nat , z)) → Err 2
        ; (Ok (Bool , x)) (Ok (Bool , y)) (Ok (Bool , z)) → Ok (Bool , ite x y z)
        ; (Ok x) (Ok y) (Err e) → Err e
        ; (Ok x) (Err e) z → Err e
        ; (Err e) y z → Err e
        }
    ; num     = λ n → Ok (Nat , num n)
    ; isZero  =
      λ { (Ok (Nat , x)) → Ok (Bool , isZero x)
        ; (Ok (Bool , x)) → Err 3
        ; (Err e) → Err e }
    ; _+o_    =
      λ { (Ok (Nat , x)) (Ok (Nat , y)) → Ok (Nat , x +o y)
        ; (Ok (Nat , x)) (Ok (Bool , y)) → Err 4
        ; (Ok (Bool , x)) (Ok (yt , y)) → Err 4
        ; (Ok r) (Err e) → Err e
        ; (Err e) y → Err e }
    }

  module INF = RazorAST.Model Infer

  inf-test : Result (Σ Ty (λ T → Tm T))
  inf-test = {! INF.⟦ ite (isZero (num 1)) false (num 5 +o num 4) ⟧  !} -- itt lehet tesztelni
    where open RazorAST.I
    -- C-c C-n

  -- Standard model

  Standard : Model {lsuc lzero} {lzero}
  Standard = record
    { Ty      = Set
    ; Tm      = λ A → A
    ; Nat     = ℕ
    ; Bool    = 𝟚
    ; true    = tt
    ; false   = ff
    ; ite     = if_then_else_
    ; num     = λ n → n
    ; isZero  = λ n → n == 0
    ; _+o_    = _+_
    }
  module STD = Model Standard

  eval : {A : Ty} → Tm A → STD.⟦ A ⟧T
  eval t = STD.⟦ t ⟧t

  _ = {! eval (ite (isZero (num 1)) (num 3) (num 5 +o num 4))  !}

  typeOfINF : Result (Σ Ty (λ A → Tm A)) → Set
  typeOfINF (Ok (A , t)) = STD.⟦ A ⟧T
  typeOfINF (Err e) = Lift ⊤ -- \top

  run : (t : RazorAST.I.Tm) → typeOfINF (INF.⟦ t ⟧)
  run t with INF.⟦ t ⟧
  ... | Ok (A , t) = eval t
  ... | Err e = mk triv

  run-test = {! run (ite (isZero (num 1)) (num 3) (num 5 +o num 4))  !}
    where open RazorAST.I

module Razor-with-equational-theory where

  open import Razor
  open I

  eq-0 : true ≡ true
  eq-0 = refl

  eq-1 : isZero (num 0) ≡ true
  eq-1 = isZeroβ₁

  eq-2 : isZero (num 3 +o num 1) ≡ isZero (num 4)
  eq-2 = cong isZero +β

  eq-3 : isZero (num 4) ≡ false
  eq-3 = isZeroβ₂

  eq-4 : isZero (num 3 +o num 1) ≡ false
  eq-4 =
    isZero (num 3 +o num 1)
      ≡⟨ cong isZero +β ⟩ -- \== \< \>
    isZero (num 4)
      ≡⟨ isZeroβ₂ ⟩
    false
      ∎ -- \qed

  eq-4' : isZero (num 3 +o num 1) ≡ false
  eq-4' = cong isZero +β ◾ isZeroβ₂ -- \sq5

  eq-5 : ite false (num 2) (num 5) ≡ num 5
  eq-5 = {!   !}

  eq-6 : ite true (isZero (num 0)) false ≡ true
  eq-6 = {!   !}

  eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
  eq-7 = {!   !}

  eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
  eq-8 =
    ite (isZero (num 0)) (num 1 +o num 1) (num 0)
                                        ≡⟨ cong (λ x → ite x (num 1 +o num 1) (num 0)) isZeroβ₁ ⟩
    ite true (num 1 +o num 1) (num 0)
                                        ≡⟨ iteβ₁ ⟩
    num 1 +o num 1
                                        ≡⟨ +β ⟩
    num 2
                                        ∎

  eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  eq-9 = {!   !}

  eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  eq-10 = {!   !}

  eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  eq-11 = {!   !}

  eq-12 : num 3 +o num 2 ≡ ite true (num 5) (num 1)
  eq-12 = {!   !}

module injectivity where

  -- bizonyitsd be, hogy RazorWT.I.isZero injektiv!
  module RazorWT where
    open import RazorWT
    open I

    D : DepModel
    D = record
      { Ty∙     = λ _ → ℕ -- or Lift ⊤
      ; Bool∙   = 0
      ; Nat∙    = 0
      ; Tm∙     = λ _ _ → Tm Nat
      ; true∙   = num 0
      ; false∙  = num 0
      ; ite∙    = λ _ _ _ → num 0
      ; num∙    = λ _ → num 0
      ; isZero∙ = λ {t} _ → t
      ; _+o∙_   = λ _ _ → num 0
      }
    module D = DepModel D

    isZeroInj : ∀{t t' : Tm Nat} → isZero t ≡ isZero t' → t ≡ t'
    isZeroInj e = cong D.⟦_⟧t e

  module Razor where
    open import Razor hiding (St)

    -- standard model
    St : Model
    St = record
      { Ty       = Set
      ; Tm       = λ X → X
      ; Nat      = ℕ
      ; Bool     = 𝟚
      ; true     = tt
      ; false    = ff
      ; ite      = λ b a a' → if b then a else a'
      ; num      = {!!}
      ; isZero   = {!!}
      ; _+o_     = {!!}
      ; iteβ₁    = {!!}
      ; iteβ₂    = {!!}
      ; isZeroβ₁ = {!!}
      ; isZeroβ₂ = {!!}
      ; +β       = {!!}
      }
    module St = Model St

    module equations where
      open St

      -- a standard modellben az eq-0...eq-12 egyenlosegek definicio szerint teljesulnek (a metaelmeletbol kovetkeznek)
      -- pl.
      eq-4 : isZero (num 3 +o num 1) ≡ false
      eq-4 = {!!}

      eq-6 : ite true (isZero (num 0)) false ≡ true
      eq-6 = {!!}

      eq-7 : (num 3 +o num 0) +o num 1 ≡ num 4
      eq-7 = {!!}

      eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
      eq-8 = {!!}

      eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
      eq-9 = {!!}

      -- stb.

    open I

    -- bizonyitsd be, hogy Razor.I.num injektiv!
    numInj : ∀{m n} → num m ≡ num n → m ≡ n
    numInj e = {!!} -- hasznald az St-be valo kiertekelest!

    -- bizonyitsd be, hogy Razor.I.isZero nem injektiv!
    notInj : ¬ ((t t' : Tm Nat) → isZero t ≡ isZero t' → t ≡ t')
    notInj e = {!!} -- hasznald a num injektivitasat ill. az isZeroβ₂-t!
