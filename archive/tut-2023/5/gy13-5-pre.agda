{-# OPTIONS --prop --rewriting #-}

module gy13-5 where

open import Lib

module Ind where
  open import Ind
  open import Ind.Syntax
  open Norm

  -- Exercise 5.6 (compulsory). List the rules for inductively defined trees no
  -- information at the leaves and infinity (Nat-ary) branching.

  -- Exercise 5.7 (compulsory). For types A, B, list the rules for the inductive
  -- type with only leaves (and no nodes) that contain an A and a B.

  -- Exercise 5.8 (compulsory). For types A, B, list the rules for the inductive
  -- type with two kinds of leaves and no nodes. One kind of leaf contains an A,
  -- the other kind of leaf contains a B.

  -- Exercise 5.9 (compulsory). How many elements are in the inductive type with
  -- only the following constructors?

  -- SpecialTree : Ty
  -- node1 : Tm Γ SpecialTree → Tm Γ SpecialTree
  -- node2 : Tm Γ SpecialTree → Tm Γ SpecialTree → Tm Γ SpecialTree

  -- Exercise 5.14. What are the normal form of the following terms?
  t9' : Σsp (Nf (◇ ▹ Nat) Bool) λ v → ⌜ v ⌝Nf ≡ lam (iteNat true false v0) $ v0
  t9' = {!   !}

module Coind where
  open import Coind.Syntax

  numbers : Tm ◇ (Stream Nat)
  numbers = {!   !}

  numbers-test : head (tail numbers) ≡ suco zeroo
  numbers-test = {!   !}

  repeat : ∀ {Γ A} → Tm Γ (A ⇒ Stream A)
  repeat = {!   !}

  repeat-test : head (tail (tail (repeat {◇} $ true))) ≡ true
  repeat-test = {!   !}

  adder : Tm ◇ Machine
  adder = {!   !}

  adder-example : Tm ◇ Nat
  adder-example = ?

  -- Exercise 6.2 (compulsory). For types A, B, define the coinductive type of
  -- machines from which we can get out an A and a B. List all the rules.

  -- Exercise 6.4 (compulsory). For types A, B, define the coinductive type of
  -- machines which has one operation: it receives an input of type A, and
  -- outputs something of type B. List all the rules.

  -- Exercise 6.6 (compulsory). Define the coinductive types of machines for
  -- which there are two different ways to input a natural number and if we
  -- press a button, they output a natural number. Implement a machine which has
  -- two numbers in its inner state and outputs one or the other alternating. If
  -- we input a number in the first way, the machine adds that to its first
  -- inner number. If we input a number in the second way, the machine adds that
  -- to its second inner number.
