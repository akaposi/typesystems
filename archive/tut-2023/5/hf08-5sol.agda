{-# OPTIONS --prop --rewriting #-}

module hf08-5 where

open import Lib
open import Def.Syntax

-- Tip:
--   You can use "cong₂" and "cong₃" to make proofs shorter
-- Example:
t : Tm ◇ Bool
t = isZero (num 1 +o v0) [ ⟨ num 2 ⟩ ]

ex : t ≡ false
ex =
  isZero (num 1 +o v0) [ ⟨ num 2 ⟩ ]
                              ≡⟨ isZero[] ⟩
  isZero (num 1 +o v0 [ ⟨ num 2 ⟩ ])
                              ≡⟨ cong isZero +[] ⟩
  isZero ((num 1 [ ⟨ num 2 ⟩ ]) +o (v0 [ ⟨ num 2 ⟩ ]))
                              ≡⟨ cong₂ (λ x y → isZero (x +o y)) num[] vz[⟨⟩] ⟩
  isZero (num 1 +o num 2)
                              ≡⟨ cong isZero +β ◾ isZeroβ₂ ⟩
  false
    ∎

------------------------------------------------------------------------

tm1 : Tm ◇ Nat
tm1 = def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))

eq1 : tm1 ≡ num 2
eq1 =
  def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
    ≡⟨ refl ⟩
  (v0 +o num 1) +o (ite (isZero v0) (num 1) (num 0) [ ⟨ v0 ⟩ ]) [ ⟨ num 1 ⟩ ]
    ≡⟨ cong (λ x → (v0 +o num 1) +o x [ ⟨ num 1 ⟩ ]) ite[] ⟩
  (v0 +o num 1) +o (ite (isZero v0 [ ⟨ v0 ⟩ ]) (num 1 [ ⟨ v0 ⟩ ]) (num 0 [ ⟨ v0 ⟩ ])) [ ⟨ num 1 ⟩ ]
    ≡⟨ cong₃ (λ x y z → (v0 +o num 1) +o (ite x y z) [ ⟨ num 1 ⟩ ]) (isZero[] ◾ cong isZero vz[⟨⟩]) num[] num[] ⟩
  (v0 +o num 1) +o (ite (isZero v0) (num 1) (num 0)) [ ⟨ num 1 ⟩ ]
    ≡⟨ +[] ⟩
  (v0 +o num 1 [ ⟨ num 1 ⟩ ]) +o (ite (isZero v0) (num 1) (num 0) [ ⟨ num 1 ⟩ ])
    ≡⟨ cong (λ x → x +o (ite (isZero v0) (num 1) (num 0) [ ⟨ num 1 ⟩ ])) +[] ⟩
  ((v0 [ ⟨ num 1 ⟩ ]) +o (num 1 [ ⟨ num 1 ⟩ ])) +o (ite (isZero v0) (num 1) (num 0) [ ⟨ num 1 ⟩ ])
    ≡⟨ cong₂ (λ x y → x +o y +o (ite (isZero v0) (num 1) (num 0) [ ⟨ num 1 ⟩ ])) vz[⟨⟩] num[] ⟩
  (num 1 +o num 1) +o (ite (isZero v0) (num 1) (num 0) [ ⟨ num 1 ⟩ ])
    ≡⟨ cong (λ x → x +o (ite (isZero v0) (num 1) (num 0) [ ⟨ num 1 ⟩ ])) +β ⟩
  num 2 +o (ite (isZero v0) (num 1) (num 0) [ ⟨ num 1 ⟩ ])
    ≡⟨ cong (λ x → num 2 +o x) ite[] ⟩
  num 2 +o ite (isZero v0 [ ⟨ num 1 ⟩ ]) (num 1 [ ⟨ num 1 ⟩ ]) (num 0 [ ⟨ num 1 ⟩ ])
    ≡⟨ cong₃ (λ x y z → num 2 +o ite x y z) (isZero[] ◾ cong isZero vz[⟨⟩]) num[] num[] ⟩
  num 2 +o ite (isZero (num 1)) (num 1) (num 0)
    ≡⟨ cong (λ x → num 2 +o ite x (num 1) (num 0)) isZeroβ₂ ⟩
  num 2 +o ite false (num 1) (num 0)
    ≡⟨ cong (λ x → num 2 +o x) iteβ₂ ⟩
  num 2 +o num 0
    ≡⟨ +β ⟩
  num 2
    ∎

------------------------------------------------------------------------

tm2 : Tm ◇ Nat
tm2 = def (num 1) (def (def (num 2 +o v0) (isZero v0)) (ite v0 (num 0) v1))

eq2 : tm2 ≡ num 1
eq2 =
  def (num 1) (def (def (num 2 +o v0) (isZero v0)) (ite v0 (num 0) v1))
    ≡⟨ refl ⟩
  ite v0 (num 0) v1 [ ⟨ isZero v0 [ ⟨ num 2 +o v0 ⟩ ] ⟩ ] [ ⟨ num 1 ⟩ ]
    ≡⟨ cong (λ x → ite v0 (num 0) v1 [ ⟨ x ⟩ ] [ ⟨ num 1 ⟩ ]) isZero[] ⟩
  ite v0 (num 0) v1 [ ⟨ isZero (v0 [ ⟨ num 2 +o v0 ⟩ ]) ⟩ ] [ ⟨ num 1 ⟩ ]
    ≡⟨ cong (λ x → ite v0 (num 0) v1 [ ⟨ isZero x ⟩ ] [ ⟨ num 1 ⟩ ]) vz[⟨⟩] ⟩
  ite v0 (num 0) v1 [ ⟨ isZero (num 2 +o v0) ⟩ ] [ ⟨ num 1 ⟩ ]
    ≡⟨ cong (λ x → x [ ⟨ num 1 ⟩ ]) ite[] ⟩
  ite (v0 [ ⟨ isZero (num 2 +o v0) ⟩ ]) (num 0 [ ⟨ isZero (num 2 +o v0) ⟩ ]) (v1 [ ⟨ isZero (num 2 +o v0) ⟩ ]) [ ⟨ num 1 ⟩ ]
    ≡⟨ cong₃ (λ x y z → ite x y z [ ⟨ num 1 ⟩ ]) vz[⟨⟩] num[] vs[⟨⟩] ⟩
  ite (isZero (num 2 +o v0)) (num 0) v0 [ ⟨ num 1 ⟩ ]
    ≡⟨ ite[] ⟩
  ite (isZero (num 2 +o v0) [ ⟨ num 1 ⟩ ]) (num 0 [ ⟨ num 1 ⟩ ]) (v0 [ ⟨ num 1 ⟩ ])
    ≡⟨ cong₃ ite (isZero[] ◾ cong isZero +[]) num[] vz[⟨⟩] ⟩
  ite (isZero ((num 2 [ ⟨ num 1 ⟩ ]) +o (v0 [ ⟨ num 1 ⟩ ]))) (num 0) (num 1)
    ≡⟨ cong₂ (λ x y → ite (isZero (x +o y)) (num 0) (num 1)) num[] vz[⟨⟩] ⟩
  ite (isZero (num 2 +o num 1)) (num 0) (num 1)
    ≡⟨ cong (λ x → ite (isZero x) (num 0) (num 1)) +β ⟩
  ite (isZero (num 3)) (num 0) (num 1)
    ≡⟨ cong (λ x → ite x (num 0) (num 1)) isZeroβ₂ ⟩
  ite false (num 0) (num 1)
    ≡⟨ iteβ₂ ⟩
  num 1
    ∎

------------------------------------------------------------------------

tm3 : Tm (◇ ▹ Nat) Nat
tm3 = ite v0 v1 (num 0) [ ⟨ num 1 +o v0 ⟩ ⁺ ] [ ⟨ true ⟩ ]

eq3 : tm3 ≡ num 1 +o v0
eq3 =
  ite v0 v1 (num 0) [ ⟨ num 1 +o v0 ⟩ ⁺ ] [ ⟨ true ⟩ ]
    ≡⟨ cong (λ x → x [ ⟨ true ⟩ ]) ite[] ⟩
  ite (v0 [ ⟨ num 1 +o v0 ⟩ ⁺ ]) (v1 [ ⟨ num 1 +o v0 ⟩ ⁺ ]) (num 0 [ ⟨ num 1 +o v0 ⟩ ⁺ ]) [ ⟨ true ⟩ ]
    ≡⟨ cong₃ (λ x y z → ite x y z [ ⟨ true ⟩ ]) vz[⁺] vs[⁺] num[] ⟩
  ite v0 (v0 [ ⟨ num 1 +o v0 ⟩ ] [ p ]) (num 0) [ ⟨ true ⟩ ]
    ≡⟨ cong (λ x → ite v0 (x [ p ]) (num 0) [ ⟨ true ⟩ ]) vz[⟨⟩] ⟩
  ite v0 (num 1 +o v0 [ p ]) (num 0) [ ⟨ true ⟩ ]
    ≡⟨ cong (λ x → ite v0 x (num 0) [ ⟨ true ⟩ ]) +[] ⟩
  ite v0 ((num 1 [ p ]) +o (v0 [ p ])) (num 0) [ ⟨ true ⟩ ]
    ≡⟨ cong₂ (λ x y → ite v0 (x +o y) (num 0) [ ⟨ true ⟩ ]) num[] [p] ⟩
  ite v0 (num 1 +o v1) (num 0) [ ⟨ true ⟩ ]
    ≡⟨ ite[] ⟩
  ite (v0 [ ⟨ true ⟩ ]) (num 1 +o v1 [ ⟨ true ⟩ ]) (num 0 [ ⟨ true ⟩ ])
    ≡⟨ cong₃ ite vz[⟨⟩] +[] num[] ⟩
  ite true ((num 1 [ ⟨ true ⟩ ]) +o (v1 [ ⟨ true ⟩ ])) (num 0)
    ≡⟨ cong₂ (λ x y → ite true (x +o y) (num 0)) num[] vs[⟨⟩] ⟩
  ite true (num 1 +o v0) (num 0)
    ≡⟨ iteβ₁ ⟩
  num 1 +o v0
    ∎

------------------------------------------------------------------------

tm4 : Tm (◇ ▹ Nat ▹ Nat) Nat
tm4 = v0 +o num 1 [ ⟨ v0 ⟩ ]

tm4' : Tm (◇ ▹ Nat ▹ Nat) Nat
tm4' = v0 [ ⟨ v0 +o num 1 ⟩ ]

var-eq-12 : tm4 ≡ tm4'
var-eq-12 =
  v0 +o num 1 [ ⟨ v0 ⟩ ]
    ≡⟨ +[] ⟩
  (v0 [ ⟨ v0 ⟩ ]) +o (num 1 [ ⟨ v0 ⟩ ])
    ≡⟨ cong₂ _+o_ vz[⟨⟩] num[] ⟩
  v0 +o num 1
    ≡⟨ vz[⟨⟩] ⁻¹ ⟩
  v0 [ ⟨ v0 +o num 1 ⟩ ]
    ∎
