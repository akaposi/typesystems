{-# OPTIONS --prop --rewriting #-}

module hf07-5 where

open import Lib
open import DefWT

open I

-- Write down the terms as well-typed abstract binding trees
-- Give the most general type

-- ite a b (let x := ite a (num 0) (num 1) in b +o x)
q1 : ∀ {Γ} → Tm (Γ ▹ Bool ▹ Nat) Nat
q1 = ite v1 v0 (def (ite v1 (num 0) (num 1)) (v1 +o v0))

-- Another solution
q1' : ∀ {Γ} → Tm (Γ ▹ Nat ▹ Bool) Nat
q1' = ite v0 v1 (def (ite v0 (num 0) (num 1)) (v2 +o v0))

-- A third solution
q1'' : ∀ {Γ A} → Tm (Γ ▹ Nat ▹ Bool ▹ A) Nat
q1'' = ite v1 v2 (def (ite v1 (num 0) (num 1)) (v3 +o v0))

-- (let x := x +o y in x +o y) +o (let y := x +o y in x +o y)
q2 : ∀ {Γ} → Tm (Γ ▹ Nat ▹ Nat) Nat
q2 = (def (v0 +o v1) (v0 +o v2)) +o (def (v0 +o v1) (v1 +o v0))

-- isZero (x +o let x := (let x := x in x +o x) in x +o x)
q3 : ∀ {Γ} → Tm (Γ ▹ Nat) Bool
q3 = isZero (v0 +o def (def v0 (v0 +o v0)) (v0 +o v0))

-- isZero (y +o let x := (let x := y in x +o y) in x +o y)
q4 : ∀ {Γ} → Tm (Γ ▹ Nat) Bool
q4 = isZero (v0 +o def (def v0 (v0 +o v1)) (v0 +o v1))

-- let a := ite c a (num 0) in ite (isZero a) b b
q5 : ∀ {Γ A} → Tm (Γ ▹ Nat ▹ A ▹ Bool) A
q5 = def (ite v0 v2 (num 0)) (ite (isZero v0) v2 v2)

-- let z := isZero z in let x := y in ite z false true
q6 : ∀ {Γ A} → Tm (Γ ▹ A ▹ Nat) Bool
q6 = def (isZero v0) (def v2 (ite v1 false true))

-- let a := true in let b := isZero x in ite b (ite a c d) d
q7 : ∀ {Γ A} → Tm (Γ ▹ A ▹ A ▹ Nat) A
q7 = def true (def (isZero v1) (ite v0 (ite v1 v3 v4) v4))
