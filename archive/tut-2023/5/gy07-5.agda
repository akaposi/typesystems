{-# OPTIONS --prop --rewriting #-}

module gy07-5 where

open import Lib

module ABT where
  open import DefABT
  open I

  {- Rewrite the following expressions with variable names -}

  t-1 : Tm 0
  t-1 = def (num 1 +o num 2) (v0 +o v0) +o def (num 3 +o num 4) (v0 +o v0)
  -- (let a := num 1 +o num 2 in a +o a) +o (let b := num 3 +o num 4 in b +o b)
  -- (let x := num 1 +o num 2 in x +o x) +o (let y := num 3 +o num 4 in y +o y)

  t-1' : Tm 0
  t-1' = def (num 1 +o num 2) ((v0 +o v0) +o def (num 3 +o num 4) (v1 +o v0))
  -- let a := num 1 +o num 2 in (a +o a) +o (let b := num 3 +o num 4 in a +o b)

  t-2 : Tm 2
  t-2 = v1 +o def true (v0 +o v1 +o def v0 (v0 +o v1 +o v2))
  -- d +o let a := true in a +o c +o (let b := a in b +o a +o c)

  -- v1 : Tm 2
  -- def true (v0 +o v1 +o def v0 (v0 +o v1 +o v2)) : Tm 2
  -- true : Tm 2
  -- v0 +o v1 +o def v0 (v0 +o v1 +o v2) : Tm 3

  t-3 : Tm 0
  t-3 = def true (def false (ite v0 v0 v1))

  t-4 : Tm 0
  t-4 = true +o def true (false +o def v0 (v1 +o v0))

  t-5 : Tm 0
  t-5 = def true (def false (def true (def false ((v0 +o v1) +o (v2 +o v3)))))

module DefWT where

  {-
  Exercise 2.7 (compulsory). Derive def (var vz) true : Tm (⋄ ▹ Nat) Bool!
  Exercise 2.8 (compulsory). Derive def (var (vs vz)) (var vz) : Tm (⋄ ▹ Bool ▹ Nat) Bool!
  Exercise 2.9 (compulsory). Derive def (num 1) (var vz + var (vs vz)) : Tm (⋄ ▹ Nat) Nat!
  Exercise 2.10 (compulsory). What can be t such that def t (def true (ite v0 v1 v1)) : Tm ⋄ Nat?
  -}
  open import DefWT
  open I
  -- ◇ \diw or \di2 or \Diamond
  -- ▹ \t6 or \tw2 or \triangeright
  tm-0 : Tm (◇ ▹ Bool ▹ Nat ▹ Nat) Bool
  tm-0 = isZero (ite v2 (v1 +o v0) v1)

  tm-0' : {Γ : Con} → Tm (Γ ▹ Bool ▹ Nat ▹ Nat) Bool
  tm-0' = isZero (ite v2 (v1 +o v0) v1)

  tm-1 : Tm (◇ ▹ Nat ▹ Nat) Nat
  tm-1 = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  tm-1' : {Γ : Con} {A : Ty} → Tm (Γ ▹ Nat ▹ A) Nat
  tm-1' = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  -- ite (isZero v0) v2 (num 0) : Tm (Γ ▹ Nat ▹ A ▹ Nat) Nat

  tm-2 : {!   !}
  tm-2 = ite v1 (isZero v0) (isZero v2)

  tm-3 : ∀ {Γ A} → Tm (Γ ▹ Bool ▹ A ▹ Nat) A -- \all
  tm-3 = ite (ite v2 (isZero v0) v2) v1 v1

  tm-4 : {!   !}
  tm-4 = ite v1 (ite v1 v2 v2) (isZero v0)

  tm-5 : {!   !}
  tm-5 = def (v1 +o num 10) (ite (isZero v0) (v2 +o num 20) v0)

  tm-6 : {!   !}
  tm-6 = isZero (def (isZero v1) (ite v0 v1 v2))

  tm-7 : {!   !}
  tm-7 = v1 +o def (def (num 2 +o v0) (v0 +o v1 +o v2)) (v2 +o v3) +o num 10

  tm-8 : {!   !}
  tm-8 = def (v1 +o num 10) (ite (isZero v0) (def (isZero v1) (v2 +o v1 +o v4)) (def v0 (v0 +o v1 +o v2) +o v3))


module DefEq where
  open import Def.Syntax

  tm-1 : Tm ◇ Nat
  tm-1 = def (num 1) (v0 +o v0)

  eq-1 : tm-1 ≡ {!   !}
  eq-1 = {!   !}

  tm-2 : Tm ◇ Nat
  tm-2 = def (num 1) (def (num 2 +o v0) (v0 +o v1))

  eq-2 : tm-2 ≡ {!   !}
  eq-2 = {!   !}

  tm-3 : Tm (◇ ▹ Nat) Nat
  tm-3 = def v0 (v0 +o v1)

  eq-3 : tm-3 ≡ {!   !}
  eq-3 = {!   !}
