{-# OPTIONS --prop --rewriting #-}

module hf11-5 where

open import Lib

module FuncNf where
  open import STT
  open import STT.Syntax

  -- Give the normal forms for the following terms

  nf1 :
    Σsp (Nf (◇ ▹ Nat) (Bool ⇒ Nat)) λ v →
    ⌜ v ⌝Nf ≡ lam (ite v0 (num 0) v1)
  nf1 = {!   !} , {!   !}

  nf2 :
    Σsp (Nf (◇ ▹ Nat ⇒ Bool ⇒ Nat ▹ Bool) Nat) λ v →
    ⌜ v ⌝Nf ≡ v1 $ (num 1 +o num 2) $ v0
  nf2 = {!   !}

  nf3 :
    Σsp (Nf (◇ ▹ Nat ⇒ Bool ⇒ Nat ▹ Nat) (Bool ⇒ Nat)) λ v →
    ⌜ v ⌝Nf ≡ v1 $ v0
  nf3 = {!   !}

module Product where
  open import Fin.Syntax

  -- Define the following functions based on their types

  -- Interestingly, every function here only has one possible definition
  -- (up to equality)

  dup : ∀ {Γ A} → Tm Γ (A ⇒ A ×o A)
  dup = {!   !}

  assocl : ∀ {Γ A B C} → Tm Γ (A ×o (B ×o C) ⇒ (A ×o B) ×o C)
  assocl = {!   !}

  mapBoth : ∀ {Γ A B C D} → Tm Γ ((A ⇒ B) ⇒ (C ⇒ D) ⇒ A ×o C ⇒ B ×o D)
  mapBoth = {!   !}

  distFunc : ∀ {Γ A B C} → Tm Γ ((A ⇒ B ×o C) ⇒ (A ⇒ B) ×o (A ⇒ C))
  distFunc = {!   !}

  ev : ∀ {Γ A B} → Tm Γ ((A ⇒ B) ×o A ⇒ B)
  ev = {!   !}
