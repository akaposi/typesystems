{-# OPTIONS --prop --rewriting #-}

module hf04-5 where

open import Lib

module AST where
  open import RazorAST

  -- Give a model to prove that in the syntax
  -- isZero (false +o num 2) ≠ isZero (false +o num 3)
  Neq : Model {lzero}
  Neq = record
    { Tm = 𝟚
    ; true = ff
    ; false = ff
    ; ite = λ _ _ _ → ff
    ; num = λ n → n == 2
    ; isZero = λ x → x
    ; _+o_ = λ x y → y
    }
  module Neq = Model Neq

  neq : ¬ (I.isZero (I.false I.+o I.num 2) ≡ I.isZero (I.false I.+o I.num 3))
  neq e = tt≠ff (cong Neq.⟦_⟧ e) -- use tt≠ff

  ------------------------------------------------------------------------
  -- Prove that _+o_ is injective in both its parameters in the syntax.
  -- There are two ways prove this:
  -- * Give two dependent models, one for the injectivity in the left side of
  --   _+o_, the other one for the right side
  -- * Alternatively, only one dependent model is needed, where we return a pair
  --   of I.Tm's, using the product type _×_ (\times or \x). The product type
  --   has the constructor _,_ and destructors π₁ and π₂
  Inj : DepModel {lzero}
  Inj = record
    { Tm∙ = λ t → I.Tm × I.Tm
    ; true∙ = I.true , I.true
    ; false∙ = I.true , I.true
    ; ite∙ = λ _ _ _ → I.true , I.true
    ; num∙ = λ _ → I.true , I.true
    ; isZero∙ = λ _ → I.true , I.true
    ; _+o∙_ = λ {t} _ {u} _ → t , u
    }
  module Inj = DepModel Inj

  {- uncomment if needed
  Inj2 : DepModel {lzero}
  Inj2 = ?
  module Inj2 = DepModel Inj2
  -}

  +o-inj :
    (u v u' v' : I.Tm) → u I.+o v ≡ u' I.+o v' → Lift (u ≡ u') × Lift (v ≡ v')
  +o-inj u v u' v' e =
    mk (cong (λ t → π₁ Inj.⟦ t ⟧) e) , mk (cong (λ t → π₂ Inj.⟦ t ⟧) e)

  ------------------------------------------------------------------------
  -- Given the following model:
  M : Model {lzero}
  M = record
    { Tm = ℕ
    ; true = 1
    ; false = 1
    ; ite = λ x y z → 1 + x + y + z
    ; num = λ _ → 1
    ; isZero = suc
    ; _+o_ = λ x y → 1 + x + y
    }
  module M = Model M

  -- Prove that for any term, there exists a natural number, such that adding 1
  -- to it equals the interpretation of the term with the model M.
  -- We use the dependent pair type Σsp for existence.
  -- The first parameter of Σsp is the type of what exists, the second is a
  -- function returning a Prop.
  MD : DepModel {lzero}
  MD = record
    { Tm∙ = λ t → Σsp ℕ (λ x → M.⟦ t ⟧ ≡ 1 + x)
    ; true∙ = 0 , refl
    ; false∙ = 0 , refl
    ; ite∙ = λ {t1} (n1 , e1) {t2} (n2 , e2) {t3} (n3 , e3) →
      3 + n1 + n2 + n3 ,
      ( suc ((M.⟦ t1 ⟧ + M.⟦ t2 ⟧) + M.⟦ t3 ⟧)
                                                ≡⟨ cong₃ (λ x y z → suc ((x + y) + z)) e1 e2 e3 ⟩ -- copy for more steps
        suc (((suc n1 + suc n2) + suc n3))
                                                ≡⟨ refl ⟩
        suc (suc ((n1 + suc n2) + suc n3))
                                                ≡⟨ cong (λ x → suc (suc (x + suc n3))) +suc ⟩
        suc (suc (suc ((n1 + n2) + suc n3)))
                                                ≡⟨ cong (λ x → suc (suc (suc x))) +suc ⟩
        suc (suc (suc (suc ((n1 + n2) + n3))))
                                                ∎
      )
    ; num∙ = λ n → 0 , refl
    ; isZero∙ = λ (n , e) → 1 + n , cong suc e
    ; _+o∙_ = λ {t1} (n1 , e1) {t2} (n2 , e2) →
      2 + n1 + n2 ,
      ( suc (M.⟦ t1 ⟧ + M.⟦ t2 ⟧)
                                    ≡⟨ cong₂ (λ x y → suc (x + y)) e1 e2 ⟩
        suc (suc n1 + suc n2)
                                    ≡⟨ refl ⟩
        suc (suc (n1 + suc n2))
                                    ≡⟨ cong (λ x → suc (suc x)) +suc ⟩
        suc (suc (suc (n1 + n2)))
                                    ∎
      )
    }
  module MD = DepModel MD

  md : (t : I.Tm) → Σsp ℕ (λ x → M.⟦ t ⟧ ≡ 1 + x)
  md t = MD.⟦ t ⟧

module Eq where
  open import Razor
  -- We work in the syntax, so by opening the module I, there is no need to
  -- prefix things with `I.`
  open I

  {- available equations
  iteβ₁     : ∀{A}{u v : Tm A} → ite true u v ≡ u
  iteβ₂     : ∀{A}{u v : Tm A} → ite false u v ≡ v
  isZeroβ₁  : isZero (num 0) ≡ true
  isZeroβ₂  : {n : ℕ} → isZero (num (1 + n)) ≡ false
  +β        : {m n : ℕ} → num m +o num n ≡ num (m + n)
  -}

  eq1 : ite (isZero (num 2)) (num 0) (num 1 +o num 2) ≡ num 3
  eq1 =
    ite (isZero (num 2)) (num 0) (num 1 +o num 2)
                                                  ≡⟨ cong (λ t → ite t (num 0) (num 1 +o num 2)) isZeroβ₂ ⟩
    ite false (num 0) (num 1 +o num 2)
                                                  ≡⟨ iteβ₂ ⟩ -- copy for more steps
    num 1 +o num 2
                                                  ≡⟨ +β ⟩
    num 3
          ∎

  eq2 : num 3 +o num 4 ≡ num 2 +o num 5
  eq2 =
    num 3 +o num 4
                    ≡⟨ +β ⟩
    num 7
                    ≡⟨ +β ⁻¹ ⟩
    num 2 +o num 5
                    ∎

  eq3 : ite true (isZero (num 1 +o num 2)) false ≡ isZero (num 4)
  eq3 =
    ite true (isZero (num 1 +o num 2)) false
                                              ≡⟨ iteβ₁ ⟩
    isZero (num 1 +o num 2)
                                              ≡⟨ cong isZero +β ⟩
    isZero (num 3)
                                              ≡⟨ isZeroβ₂ ⟩
    false
                                              ≡⟨ isZeroβ₂ ⁻¹ ⟩
    isZero (num 4)
                                              ∎
