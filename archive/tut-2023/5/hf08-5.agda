{-# OPTIONS --prop --rewriting #-}

module hf08-5 where

open import Lib
open import Def.Syntax

-- Tip:
--   You can use "cong₂" and "cong₃" to make proofs shorter
-- Example:
t : Tm ◇ Bool
t = isZero (num 1 +o v0) [ ⟨ num 2 ⟩ ]

ex : t ≡ false
ex =
  isZero (num 1 +o v0) [ ⟨ num 2 ⟩ ]
                              ≡⟨ isZero[] ⟩
  isZero (num 1 +o v0 [ ⟨ num 2 ⟩ ])
                              ≡⟨ cong isZero +[] ⟩
  isZero ((num 1 [ ⟨ num 2 ⟩ ]) +o (v0 [ ⟨ num 2 ⟩ ]))
                              ≡⟨ cong₂ (λ x y → isZero (x +o y)) num[] vz[⟨⟩] ⟩
  isZero (num 1 +o num 2)
                              ≡⟨ cong isZero +β ◾ isZeroβ₂ ⟩
  false
    ∎

------------------------------------------------------------------------

tm1 : Tm ◇ Nat
tm1 = def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))

eq1 : tm1 ≡ num 2
eq1 =
  def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
    ≡⟨ {!   !} ⟩
  num 2
    ∎

------------------------------------------------------------------------

tm2 : Tm ◇ Nat
tm2 = def (num 1) (def (def (num 2 +o v0) (isZero v0)) (ite v0 (num 0) v1))

eq2 : tm2 ≡ num 1
eq2 = {!   !}

------------------------------------------------------------------------

tm3 : Tm (◇ ▹ Nat) Nat
tm3 = ite v0 v1 (num 0) [ ⟨ num 1 +o v0 ⟩ ⁺ ] [ ⟨ true ⟩ ]

eq3 : tm3 ≡ num 1 +o v0
eq3 = {!   !}

------------------------------------------------------------------------

tm4 : Tm (◇ ▹ Nat ▹ Nat) Nat
tm4 = v0 +o num 1 [ ⟨ v0 ⟩ ]

tm4' : Tm (◇ ▹ Nat ▹ Nat) Nat
tm4' = v0 [ ⟨ v0 +o num 1 ⟩ ]

var-eq-12 : tm4 ≡ tm4'
var-eq-12 = {!   !}
