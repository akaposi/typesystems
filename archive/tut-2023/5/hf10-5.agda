{-# OPTIONS --prop --rewriting #-}

module hf10-5 where

open import Lib
open import STT.Syntax

-- Internalized _+o_
add' : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
add' = lam (lam (v1 +o v0))

+β' : ∀ {Γ} (m : ℕ) (n : ℕ) → add' {Γ} $ num m $ num n ≡ num (m + n)
+β' m n =
  add' $ num m $ num n
    ≡⟨ refl ⟩

  lam (lam (v1 +o v0)) $ num m $ num n
    ≡⟨ {!   !} ⟩

  num (m + n)
    ∎

t1 : Tm (◇ ▹ Bool) (Nat ⇒ Nat)
t1 = lam (lam (v0 +o (v1 $ num 1))) $ lam (ite v1 v0 (num 0))

e1 : t1 ≡ lam (v0 +o ite v1 (num 1) (num 0))
e1 = {!   !}

t2 : Tm (◇ ▹ Bool ⇒ Bool ⇒ Bool) (Bool ⇒ Bool ⇒ Bool)
t2 = lam (lam (v2 $ v1 $ v0))

-- Use ⇒η
e2 : v0 ≡ t2   -- f = (λ x. λ y. f $ x $ y)
e2 =
  v0
    ≡⟨ {!   !} ⟩ -- You can add more steps

  lam (v1 $ v0)
    ≡⟨ {!   !} ⟩

  lam (lam (((v1 $ v0) [ p ]) $ v0))
    ≡⟨ {!   !} ⟩

  lam (lam (v2 $ v1 $ v0))
    ∎
