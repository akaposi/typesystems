{-# OPTIONS --prop --rewriting #-}

module gy02-5 where

open import Lib

comm-suc : (m n : ℕ) → m + suc n ≡ suc m + n
comm-suc zero n = refl
comm-suc (suc m) n = cong (λ x → suc x) (comm-suc m n) -- \Gl or \lambda

-- e : x ≡ y
-- cong f e : f x ≡ f y

-- Razor nyelv AST szinten

open import RazorAST

  {-
          ite
        /  |  \
       /   |   \
     true  +o   isZero
          /\        |
         /  \       |
    num 1  num 3  isZero
                    |
                    |
                  false
  -}
tm1 : I.Tm
tm1 = {!!}
  where open I

-- more exercises from the book

t1 : I.Tm
t1 = {!!}

t1-test : height t1 ≡ 3
t1-test = refl

t2 : I.Tm
t2 = {!!}

t2-test-1 : height t2 ≡ 3
t2-test-1 = refl

t2-test-2 : ¬ (t1 ≡ t2)
t2-test-2 ()

-- ird le az alabbi nyelveket data-val!

-- T ::= op0 | op1 T | op2 T T | op3 T T T | op4 T T T T

-- A ::= a | fb B
-- B ::= fa A

-- V ::= vzero | vsuc V
-- E ::= num N | E < E | E = E | var V
-- C ::= V := E | while E S | if E then S else S
-- S ::= empty | C colon S

-- ujradefinialni height-ot modell segitsegevel

Height' : Model {lzero}
Height' = record
           { Tm = ℕ
           ; true = 0
           ; false = 0
           ; ite = λ x y z → 1 + max x (max y z)
           ; num = λ x → 0
           ; isZero = λ x → 1 + x
           ; _+o_ = λ x y → 1 + max x y
           }

module HH = Model Height'

-- \[[ and \]]
-- C-c C-n: HH.⟦ I.true ⟧



-- modell: Count the number of trues in a term

Trues : Model {lzero}
Trues = record
         { Tm = ℕ
         ; true = 1
         ; false = 0
         ; ite = λ x y z → x + y + z
         ; num = λ x → 0
         ; isZero = λ x → x
         ; _+o_ = λ x y → x + y
         }

module testTrues where
  module M = Model Trues

  -- kulonbseg modell-beli es szintaktikus termek kozott
  t : M.Tm
  t = M.true

  t' : M.Tm
  t' = 10

  test1 : M.⟦ I.false I.+o (I.num 1) ⟧ ≡ 0
  test1 = refl
  test2 : M.⟦ tm1 ⟧ ≡ 1
  test2 = refl
  test3 : M.⟦ I.ite I.true I.true I.true ⟧ ≡ 3
  test3 = refl


-- C stilusu interpreter

C : Model {lzero}
C = record
     { Tm = ℕ
     ; true = 1
     ; false = 0
     ; ite = λ x y z → if x == 0 then z else y
     ; num = λ x → x
     ; isZero = λ x → if x == 0 then 1 else 0
     ; _+o_ = λ x y → x + y
     }

module testC where
  module M = Model C
  open I

  test1 : M.⟦ false ⟧ ≡ 0
  test1 = refl
  test2 : M.⟦ true ⟧ ≡ 1
  test2 = refl
  test3 : M.⟦ ite (num 100) (num 3) (num 2) ⟧ ≡ 3
  test3 = refl
  test4 : M.⟦ ite (num 0) (num 3) (num 2) +o num 3 ⟧ ≡ 5
  test4 = refl



data ⊥' : Set where

Empty : Model
Empty = record
      { Tm = ⊥'
      ; true = {!!}
      ; false = {!!}
      ; ite = λ ()
      ; num = {!!}
      ; isZero = λ ()
      ; _+o_ = λ ()
      }

e : ⊥' → ℕ
e ()

Product : Model {lzero} → Model {lzero} → Model {lzero}
Product M N = record
               { Tm = M.Tm × N.Tm
               ; true = M.true , N.true
               ; false = M.false , N.false
               ; ite = λ (x1 , x2) (y1 , y2) (z1 , z2) → (M.ite x1 y1 z1) , (N.ite x2 y2 z2)
               ; num = λ n → M.num n , N.num n
               ; isZero = λ (x1 , x2) → M.isZero x1 , N.isZero x2
               ; _+o_ = λ (x1 , x2) (y1 , y2) → (x1 M.+o y1) , (x2 N.+o y2)
               }
  where
  module M = Model M
  module N = Model N
