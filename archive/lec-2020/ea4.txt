Ezen az órán: összefoglaló: NatBool nyelvet teljesen; 1 új dolog: normalizálás NatBool nyelvről (t : I.Tm I.Bool) → (t = I.true) ⊎ (t = I.false)

NatBool egyszerű kifejezésnyelv

 - illusztrálás: algebra, dependens algebra, rekurzió, indukció
 - nyelv tárgyalásának absztrakciós szintjei: teljes leírás: NatBool
 
A nyelv definíciója: az egy algebrai struktúra:

Ty : Set
Tm : Ty → Set
Nat : Ty
Bool : Ty
true, false : Tm Bool
ite : Tm Bool → Tm A → Tm A → Tm A
zero : Tm Nat
suc : Tm Nat → Tm Nat
isZero : Tm Nat → Tm Bool
_+_ : Tm Nat → Tm Nat → Tm Nat
Boolβ₁ : ite true  u v = u
Boolβ₂ : ite false u v = v
isZeroβ₁ : isZero zero = true
isZeroβ₂ : isZero (suc u) = false
+β₁ : zero + t = t
+β₂ : (suc u) + t = suc (u + t)

tulajdonságok: (1) csak jóltípusozott termek (programok), (2) ha két program "futtatása"
ugyanazt adja, akkor azok egyenlőek [(1) típusrendszer, (2) operációs szemantika]

nyelv szintaxisa: iniciális NatBool-algebra (⟦_⟧).
absztrakt szintaxisfák, csak jóltípuzottak

   _+_
   / \          <-  ilyen nincs benne
  /   \
true   false

  _+_                     suc              suc               _+_
  / \       ==             |         ==     |       ==       / \
 /   \                    _+_             zero              /   \
suc  zero                 / \                             zero  suc
 |                       /   \                                   |
 zero                 zero   zero                              zero

                  +β₂                  +β₁         +β₁ ⁻¹
(suc zero) + zero  =  suc (zero + zero) = suc zero =  zero + (suc zero)




ℤ := ℕ × ℕ / ~
(a,b) ~ (a',b') := a+b' = a'+b

==============================================================

NatBool
  ^                                             |
  | egyesítünk dolgokat: isZero zero = true     | ennek a képe true, false, suc...(zero)
  |                                             v
NatBoolWT (jóltípusozott, de a programok futtatásának információja nincs benne, pl.
           isZero zero ≠ true)                                               Tm Bool : Set
  ^                                  |                                       Tm Nat  : Set
  | type INFERence                   | típusinformáció elfelejtése
  |                                  v
NatBoolAST (szintaxisfák, de nem jól típusozott dolgok is, pl. isZero true is)   Tm : Set
  ^                                  |
  | parsing (szintaktikus elemzés)   | flattening of the tree
  |                                  v
lexikális elemek sorozata (benne vannak a rosszul zárójelezett kifejezések)
  ^                                  |
  | lexikális elemzés (lexing)       | print (belerak space-eket)
  |                                  v
sztring (space-ek is vannak benne)                                   hány space van benne

===============================================================

NatBool-beli programunk: I.Tm A  (ahol A=I.Bool vagy A=I.Nat)

Minden ilyen termből tudunk ún. normál formát képezni. Normál formák:
true, false, zero, suc zero, suc (suc zero), ..., suc (...suc(..suc zero))

A program futtatásának végeredménye egy normál forma (normalizálás).

Nat  típusú normálformák St.⟦ I.Nat  ⟧ = ℕ
Bool típusú normálformák St.⟦ I.Bool ⟧ = 𝟚

Normál formákból mindig tudunk a szintaxisba képezni:

⌜_⌝ : St.⟦ I.Bool ⟧ → I.Tm I.Bool
⌜ O ⌝ := I.false
⌜ I ⌝ := I.true

        = ℕ
      /----------\
⌜_⌝ : St.⟦ I.Nat ⟧ → I.Tm I.Nat
⌜ O ⌝ := I.zero
⌜ S n ⌝ := I.suc ⌜ n ⌝

norm : I.Tm A → St.⟦ A ⟧       <- normalizálás (futtatás)
norm t := St.⟦ t ⟧

Két tulajdonság:

teljesség: ⌜ norm t ⌝ = t              ⌜ norm (I.isZero I.zero) ⌝ = I.true = I.isZero I.zero
  "van elég egyenlőségem a nyelvemben ahhoz, hogy futtatni tudjam a programokat"

norm (I.isZero (I.ite ...)) = ... = I.false

stabilitás: norm ⌜ n ⌝ = n
  "nincs szemét a normál formákban" -- csak olyan dolgok vannak, amik a termekben is szerepelnek

=========================================================

Teljesség: indukícióval, ehhez megadunk egy függő algebrát:

NatBoolAST, minden t-re trues t ≤ 3^(height t)

  indukció NatBool-ra                                               indukció ℕ-ra

Comp : DepAlgebra
Comp.Ty A := 𝟙
Comp.Tm A t := ⌜ norm t ⌝ = t                                       Idr.N n := (n + 0 = n)
Comp.true : ⌜ norm I.true ⌝ = I.true                                Idr.z : (I.z + 0 = I.z)
Comp.false : ⌜ norm I.false ⌝ = I.false
Comp.isZero : ⌜ norm t ⌝ = t → ⌜ norm (I.isZero t) ⌝ = I.isZero t   Idr.s : (n + 0 = n) →
                                                                            I.s n + 0 = I.s n
...

Comp.true : ⌜ norm I.true ⌝ = ⌜ St.⟦ I.true ⟧ ⌝ = ⌜ St.true ⌝ = ⌜ I ⌝ = I.true
Comp.isZero : tudjuk, hogy e : (⌜ norm t ⌝ = t)   ⌜ St.⟦ t ⟧ ⌝ = t
   ⌜ norm (I.isZero t) ⌝ = ⌜ St.⟦ I.isZero t ⟧ ⌝ = ⌜ St.isZero St.⟦ t ⟧ ⌝ =
   ⌜ is0 St.⟦ t ⟧ ⌝ = ?                     = I.isZero t
         \______/   (1) f : St.⟦ t ⟧ = 0 = O                
           : ℕ           ⌜ is0 O ⌝ = ⌜ I ⌝ =
                         I.true =
                                                  isZeroβ₁
                         I.isZero I.zero =
                                                  defin
                         I.isZero ⌜ O ⌝ =
                                                   f
                         I.isZero ⌜ St.⟦ t ⟧ ⌝ =
                                                  e
                         I.isZero t

                    (2) f : St.⟦ t ⟧ = S n
                        ⌜ is0 (S n) ⌝ =
                                                 def
                        ⌜ O ⌝ =
                                                 def
                        I.false                =
                                                  isZeroβ₂
                        I.isZero (I.suc ⌜ n ⌝) =
                                                  def
                        I.isZero ⌜ S n ⌝ =
                                                  f
                        I.isZero ⌜ St.⟦ t ⟧ ⌝ =
                                                  e
                        I.isZero t
is0 : ℕ → 𝟚
is0 O := I
is0 _ := O




Comp.Tm A t := ⌜ norm t ⌝ = t

completeness : (t : I.Tm A) → ⌜ norm t ⌝ = t
completeness t := Comp.⟦ t ⟧

stability ℕ ill. 𝟚 szerinti indukcióval belátható
