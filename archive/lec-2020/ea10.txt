eddig: Nat, Bool <- egyszerű változat

ma: függvény típus
még jön: szorzat, összeg, üres, egyelemű, Nat igazi változata, induktív típusok, koinduktív típusok

Beadandó: mi megadjuk: Algebra, DepAlgebra, I : Algebra, ⟦_⟧ interpretáció I-ből tetszőleges (dependens) algebrábra, normál formák:
   I.Tm ∙ Nat  normál formája ℕ
   I.Tm ∙ Bool normál formája 𝟚
   ⌜_⌝ : ℕ → I.Tm ∙ Nat
   ⌜_⌝ : 𝟚 → I.Tm ∙ Bool

kell: normN : I.Tm ∙ Nat  → ℕ       (interpretáció a standard algebrába)
      normB : I.Tm ∙ Bool → 𝟚
      compl : ⌜ norm t ⌝ = t        (dependens algebra kell hozzá -- indukció a szintaxison)
      stab  : norm ⌜ n ⌝ = n        (normál formákon indukció)

-------------------------------------------------------------------------

Függvény típus

Def = substitution calculus + Nat + Bool + függvény

Nat : Ty, Bool : Ty, _⇒_ : Ty → Ty → Ty

iniciális algebrában a típusok bináris fák:

   _⇒_
   / \
  Nat Bool

Nat ⇒ Bool   bemenet: Nat, kimenet: Bool


   _⇒_
   / \
 Nat  _⇒_
      / \
   Bool  Nat

Nat ⇒ (Bool ⇒ Nat)   bemenet: Nat, kimenet: függvény, aminek a bemenete Bool,
                                                               kimenete Nat

"kétparaméteres függvény: két bemenet: Nat, Bool"

    _⇒_
    / \
  _⇒_  Nat
  / \
Bool Nat

(Bool ⇒ Nat) ⇒ Nat   bemenet: függvény, kimenet: Nat

magasabbrendű függvény - másodrendű függvény

t : I.Tm ∙ (((Bool ⇒ Nat) ⇒ Nat) ⇒ Nat)


termeken új operátorok:

röviden: Tm (Γ▹A) B ≅ Tm Γ (A⇒B)        (bijekció, izomorfizmus)

_⇒_                                     típuslétrehozó
lam : Tm (Γ▹A) B → Tm Γ (A⇒B)           bevezető operátor (intro, constructor)
app : Tm Γ (A⇒B) → Tm (Γ▹A) B           eliminációs operátor (destructor)
⇒β  : app (lam t) = t                   számítási szabály, β szabály - elim(bev)
⇒η  : lam (app t) = t                   egyediség szabály, η szabály - bev(elim)
lam[] : (lam {Γ} t)[σ] = lam {Θ} (t[σ∘p,q])
           t : Tm (Γ▹A) B
           lam {Γ}{A}{B} t : Tm Γ (A ⇒ B)
           σ : Sub Θ Γ
           (lam t)[σ] : Tm Θ (A ⇒ B)
           t[?] : Tm (Θ▹A) B
           σ∘p : Sub (Θ▹A) Γ
           (σ∘p,q) : Sub (Θ▹A) (Γ▹A)
def[] hasonló módon
(app[] : app (t[σ]) = (app t)[σ∘p,q]        <- bizonyítható)

Szokásos applikáció (modus ponens):
_$_ : Tm Γ (A⇒B) → Tm Γ A → Tm Γ B
t$u := (app t)[id,u]
         (id,u) : Sub Γ (Γ▹A)

(def t u := u[id,t] hasonló)

Példák:

lam (isZero v⁰) : Tm ∙ (Nat ⇒ Bool)          v⁰=q, v¹=q[p], v²=q[p][p], ...
    ^ : Tm (∙▹Nat) Bool

lam true : Tm ∙ (Nat ⇒ Bool)

lam true $ suc (suc zero) =
           ^ : Tm ∙ Nat

(app (lam true))[id,suc (suc zero)] =  (⇒β)
true[id,suc (suc zero)]             =  (true[])
true

lam true $ u = true (tetszőleges u-ra)

                           Bool f(Nat x) { t }    <- itt van neve a függvénynek
                           \x.t
szokásos jelölés lam-ra:   λx.t  helyett lam t  (x helyett v⁰-t írunk)
                           t u   helyett t $ u = app t [id,u]

konstans függvény tényleg mindig konstans:

                                         ⇒β            [∘]          ▹β₁     [id]
lam (t[p]) $ u = (app (lam (t[p])))[id,u] = t[p][id,u] = t[p∘(id,u)] = t[id] = t
  t    : Tm Γ     B
  t[p] : Tm (Γ▹A) B       <- ez egy olyan term, ami nem hivatkozik v⁰-ra
  u    : Tm Γ A
  lam t $ u : Tm Γ B

Nat-hoz hozzáad egyet vagy kettőt a Bool-tól függően
(n,true)  ↦ n+1
(n,false) ↦ n+2
lam (lam (v¹ + if v⁰ then suc zero else (suc (suc zero))))
                                           : Tm ∙ (Nat ⇒ (Bool ⇒ Nat))
         ^ : Tm (∙▹Nat▹Bool) Nat
    ^ : Tm (∙▹Nat) (Bool ⇒ Nat)

lam (lam (if v⁰ then v¹+suc zero else (v¹+suc (suc zero)))) : Tm ∙ (Nat ⇒ (Bool ⇒ Nat))

lam (v⁰ $ true) : Tm ∙ ((Bool ⇒ Nat) ⇒ Nat)
          ^ : Tm (∙ ▹ Bool⇒Nat) Bool
     ^ : Tm (∙ ▹ Bool⇒Nat) (Bool⇒Nat)
    ^ : Tm (∙ ▹ Bool⇒Nat) Nat

lam (v⁰ $ isZero (v⁰ $ true)) : Tm ∙ ((Bool ⇒ Nat) ⇒ Nat)

lam (v⁰ $ (lam (if v⁰ then zero else suc zero)))
                                    : Tm ∙ (((Bool ⇒ Nat) ⇒ Nat) ⇒ Nat)
               ^ : Tm (∙ ▹ (Bool⇒Nat)⇒Nat ▹ Bool) Nat
          ^ : Tm (∙ ▹ (Bool⇒Nat)⇒Nat) (Bool⇒Nat)
    ^ : Tm (∙ ▹ (Bool⇒Nat)⇒Nat) Nat

lam : Tm (Γ▹A) B → Tm Γ (A⇒B)
app : Tm Γ (A⇒B) → Tm (Γ▹A) B

lam (app t) = t

t     : Tm Γ (A⇒B)
app t : Tm (Γ▹A) B
lam (app t) : Tm Γ (A⇒B)

v⁰ : Tm (∙▹Bool⇒Nat) (Bool⇒Nat)
        ^ =: Γ
app v⁰ =
app q = 
(app q)[id] = 
(app q)[p,q] = 
(app q)[p∘id,q] = 
(app q)[(p∘p,q)∘(id,q)] = 
(app q)[p∘p,q][id,q] = app[]
(app (q[p]))[id,q] =
(app v¹)[id,q] =
v¹ $ v⁰ : Tm (∙▹ Bool⇒Nat ▹ Bool) Nat

lam (v¹ $ v⁰) = lam (app v⁰) = v⁰
                            ⇒η

egyediség működik minden függvényre: (t : Tm Γ (A ⇒ B))

egyediség szerint t lam-al jött létre: t = lam (app t)

Minden függvény lambda.

eddig a függvények definíciója

-----------------------------------------------------------------------

szemantika: normalizálás függvényeket tartalmazó nyelven

normN : I.Tm ∙ Nat  → ℕ
normB : I.Tm ∙ Bool → 𝟚

norm t := St.⟦ t ⟧ *
  St.⟦ t ⟧ : St.⟦∙⟧ → St.⟦A⟧

standard algebra kiegészíthető függvényekkel:

Ty := Set
Bool := 𝟚
A ⇒ B := A → B
Tm Γ A := Γ → A
Γ▹A := Γ × A
lam t : Γ → (A → B) := λ γ α → t (γ , α)            lam := curry
    ^ : (Γ × A) → B
app t (γ,α) := t γ α 
⇒β := refl
⇒η := refl

------------------------------------------------------------------------

Teljesség:

  t' : I.Tm Γ' A'
Tm Γ A t' := ∀(ν':I.Sub ∙ A') → ⌜ norm ν' ⌝ = ν' → 
  → ⌜ norm (t'[ν']) ⌝ = t'[ν']

Nem működik függvény típusra, ⌜_⌝ nem adható meg.

Függényekre nincs ⌜_⌝ : (ℕ → 𝟚) → I.Tm ∙ (Nat ⇒ Bool)
  megj. a szintaxisban megszámlálhatóan sok term van, a metaelméletben megszámlálhatatlanul sok ℕ→𝟚 függvény van

logikai predikátumok módszere, Tait módszer, reducibility candidates módszer

Nem közvetlenül bizonyítjuk, amit szeretnénk, hanem egy általánosabb
dolgot bizonyítunk: minden típusra megadunk egy predikátumot. Termekre
azt látjuk be, hogy megőrzik a predikátumot.

Comp dependens algebra:

Ty  A' := I.Tm  ∙ A' → Set
Con Γ' := I.Sub ∙ Γ' → Set
Tm Γ A t' := (ν' : I.Sub ∙ Γ') → Γ ν' → A (t'[ν'])
  t' : I.Tm Γ' A'
Sub Γ Δ σ' := (ν' : I.Sub ∙ Γ') → Γ ν' → Δ (σ'∘ν')
Nat t' := (⌜ norm t' ⌝ = t')
  t' : I.Tm ∙ I.Nat
(A ⇒ B) t' := (u' : I.Tm ∙ A') → A u' → B (t' $ u')
  t' : I.Tm ∙ (A' I.⇒ B')

A' : I.Ty, akkor Comp.⟦A'⟧ : I.Tm ∙ A' → Set

alkalmazás:

t' : I.Tm ∙ Nat

∙ σ' := ⊤

Comp.⟦t'⟧ : (ν' : I.Sub ∙ ∙) → ∙ ν' → Nat (t'[id])
                                      ^ =  ⌜ norm t' ⌝ = t'
completeness t' := Comp.⟦t'⟧ id *
