jóltípusozott leírás:

isZero false

NatBoolWT-algebra:

Ty : Set        -- Ty egy halmaz
Tm : Ty → Set   -- Tm az egy Ty felett indexelt halmaz
Bool : Ty
Nat  : Ty           Tm Bool : Set, Tm Nat : Set
zero : Tm Nat
suc  : Tm Nat → Tm Nat
isZero : Tm Nat → Tm Bool
_+_    : Tm Nat → Tm Nat → Tm Nat                          "suc true" le se tudom írni
true  : Tm Bool                                            "(true" le se tudom írni
false : Tm Bool
ite   : Tm Bool → Tm A → Tm A → Tm A                       "ite false true zero" nem tudom leirni

{-
TmBool : Set
TmNat  : Set
_⇒_ : Ty → Ty → Ty
-}

algebrai struktúra: csoport
C : Set
egy : C
_⊗_ : C -> C -> C
_⁻¹ : C -> C
idl : egy ⊗ t = t
idr : t × egy = t
ass : (t ⊗ u) ⊗ v = t ⊗ (u ⊗ v)
invr : t ⊗ (t ⁻¹) = egy
invl : (t ⁻¹) ⊗ t = egy

Tm : Set
true : Tm
suc : Tm → Tm
_+_ : Tm → Tm → Tm

jóltípusozott NatBool reprzentáció: algebra, szintaxis (I iniciális algebra), dependens algebra, rekuzor: kiértékelés egy tetszőleges algebrába, indukció: kiértékelés egy dependens algebrába


NatBoolWT: 1 .típuskikövetkeztetés, 2. standard algebra, standard interpretáció (evaluátor, kiértékelés)

1.

  infer : NatBoolAST.I.Tm → Maybe ((A : NatBoolWT.I.Ty) × (NatBoolWT.I.Tm A))
                            Σ NatBoolWT.I.Ty NatBoolWT.I.Tm
                            (n : ℕ) × (n ≡ 3)
                            (n : ℕ) × (z : List 𝟚) × (length z ≡ n)
  Nothing : Maybe A
  Just a : Maybe A, ha a : A

  Inf : NatBoolAST.Algebra
  Inf.Tm = Maybe ((A : I.Ty) × (I.Tm A))
  Inf.true := Just (I.Bool, I.true)            I.true : I.Tm I.Bool
  Inf.false := Just (I.Bool, I.false)
  Inf.ite : Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm) → Maybe (Σ I.Ty I.Tm)
  Inf.ite (Just (I.Bool, t)) (Just (A , u)) (Just (A' , v)) := if A ≟ A' then
    Just (A , ite t u v) else Nothing
  Inf.ite _ _ _ = Nothing
  Inf.zero := Just (Nat , zero)
  Inf.suc (Just (Nat , t)) := Just (Nat , suc t)
  Inf.isZero (Just (Nat , t)) := Just (Bool , isZero t)
  Inf.isZero _ := Nothing
  (Just (Nat , u)) Inf.+ Just (Nat , v) := Just (Nat , u I.+ v)
  _ Inf.+ _ := Nothing

  infer (ite true zero false) := Nothing
  infer (ite true zero zero)  := (Nat, ite true zero zero)
  infer (ite (isZero (suc zero)) (suc zero) (suc (suc zero))) :=
    (Nat , ite (isZero (suc zero)) (suc zero) (suc (suc zero)))
  infer (true) := (Bool, true)

  Inf.⟦_⟧ := infer


típusellenőrzés:

infer : NatBoolAST.I.Tm -> Maybe (Σ A (NatBoolWT.I.Tm A))
check : NatBoolAST.I.Tm → (A : NatBoolWT.I.Ty) → Maybe (I.Tm A)

check t A := ha ((infer t  ≟ Just (A' , t')) akkor (ha A ≟ A' akkor t' különben Nothing) különben Nothing

1. ez volt a típuskikövetkeztetés (AST -> WT)

2. standard algebra: metaelméleti megfelelő konstrukciókkal adunk egy algebrát

Ty := Set
Bool := 𝟚 : Set
Nat := ℕ
Tm A := A            Tm Bool = 𝟚, Tm Nat = ℕ
true := I  : Tm Bool = 𝟚
false := O : Tm Bool = 𝟚
zero := O
suc t := S t
u + v := u +ℕ v
isZero O     := I
isZero (S _) := O
       ^ : Tm Nat = ℕ
ite O u v := v
ite I u v := u
    ^ : Tm Bool = 𝟚

NatBoolAST rekurzio: M algebra M.⟦_⟧ : I.Tm → M.Tm
NatBoolWT  rekurzio: M algebra
  M.⟦_⟧T : I.Ty → M.Ty
  M.⟦_⟧t : I.Tm A → M.Tm M.⟦ A ⟧T

St.⟦_⟧t : I.Tm A      → St.Tm St.⟦ A ⟧T
St.⟦_⟧t : I.Tm I.Bool → 𝟚
St.⟦_⟧t : I.Tm I.Nat  → ℕ

interpreter, evaluator

eval := St.⟦_⟧t

denotációs szemantika
operációs  szemantika

-----------------------------------------------------------------------

NatBoolAST, NatBoolWT, *NatBool*

Well-typed syntax with equations

NatBool-algebra:

Ty : Set
Tm : Ty → Set
Bool : Ty
Nat  : Ty
zero : Tm Nat
suc  : Tm Nat → Tm Nat
isZero : Tm Nat → Tm Bool
_+_    : Tm Nat → Tm Nat → Tm Nat
true  : Tm Bool                  
false : Tm Bool
ite   : Tm Bool → Tm A → Tm A → Tm A
isZeroβ₁ : isZero zero = true
isZeroβ₂ : isZero (suc n) = false
+β₁      : zero + n = n
+β₂      : (suc m) + n = suc (m + n)
                                               NEM: (+β₁'     : m + zero = m)
iteβ₁ : ite true u v = u
ite‌β₂ : ite false u v = v

szintaxis := iniciális algebra

NatBoolAST algebra példa volt a term magassága

Tm A := ℕ
true := 0
false := 0
ite t u v := 1 + max(t,u,v)
...
zero := 0
suc t := 1 + t\
iteβ₁ : 1 + u = 1+max(u,v) = ite true u v = u

Magasság nem őrzi meg a NatBool-egyenlőségeket, ezért nem kapható meg egy term magassága.
Nem adható meg egy height : I.Tm A -> ℕ függvény, amelyre
  height (I.ite t u v) = 1 + max(height t,height u,height v)

Milyen NatBool-algebrák vannak?

St jó.

isZero (suc zero) = false

isZeroβ₁ : I = isZero zero = true = I
isZeroβ₂ : O = isZero (suc n) = false = O
+β₁      : O +ℕ n = zero + n = n
+β₂      : (S m) +ℕ n = (suc m) + n = suc (m + n) = S (m +ℕ n)
                                               NEM: (+β₁'     : m + zero = m)
iteβ₁ : u = if I then u else v = ite true u v = u
ite‌β₂ : v = if O then u else v = ite false u v = v

-- standard algebra teljes-e?

St.⟦_⟧ : I.Tm Bool → 𝟚
Minden Bool típusú termből kapunk true-t vagy false-ot.

Minden Nat típusú termből kapunk 0,1,2,3,4....-t kapunk

Kérdés : igaz-e, hogy t : I.Tm Bool -re t = I.true vagy t = I.false?

isZero (suc zero) = isZeroβ₁
false

ite (isZero zero) (zero + suc zero) zero = isZeroβ₁
ite true          (zero + suc zero) zero = iteβ₁
(zero + suc zero)                        = +β₁
suc zero

Kanonikus alakja a termeknek az olyan, hogy csak true, false, suc, zero szerepelhet benne

⌜_⌝ : ℕ → I.Tm I.Nat
⌜ O ⌝ := I.zero
⌜ S n ⌝ := I.suc ⌜ n ⌝

⌜_⌝ : 𝟚 → I.Tm I.Bool
⌜ O ⌝ := I.false
⌜ I ⌝ := I.true

Következő óra elején bebizonyítjuk, hogy:

kanonicitás természetes számokra:
(t : I.Tm I.Nat) → Σ (n : ℕ) → t = ⌜ n ⌝
            bool-okra:
(t : I.Tm I.Bool) → (t = true) ⊎ (t = false)
