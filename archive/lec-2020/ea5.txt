Van olyan NatBool-algebra, melyben isZero zero = false.

* : 𝟙

Ty := Set
Nat := 𝟙
Bool := 𝟙
Tm A := 𝟙
true := *
false := *
ite t u v := *
isZero t := *
...

* = true = isZero zero = isZero * = * = false

==============================================================

Ha egy NatBool-algebrában true = false, akkor ebben az algebrában
bármely u, v : Tm Nat-ra u = v.

u = ite true u v = ite false u v = v
  ^iteβ₁         ^feltétel         ^iteβ₂

Minden programozási nyelvben, amiben van if-then-else, abban ha true =
false, akkor minden egyenlő mindennel

===============================================================

Összefoglaló:

NatBool kifejezésnyelv
- többféle reprezentáció, az igazi a NatBool
  - értelmes termek csak (jóltípusozott)
  - az ugyolyan végeredményű termek egyenlőek
- kiértékelő: I.Tm I.Nat  -> ℕ
              I.Tm I.Bool -> 𝟚
  ⌜_⌝ : ℕ → I.Tm I.Nat
  ⌜_⌝ : 𝟚 → I.Tm I.Bool
  (standard algebrába kiértékelés, normalizálás, norm)
  - teljesség:  (t : I.Tm A) → ⌜ norm t ⌝ = t
  - stabilitás: (n : ℕ) → norm ⌜ n ⌝ = n
                (b : 𝟚) → norm ⌜ b ⌝ = b

  - norm megadható NatBoolWT-re is, nincs teljesség, van stabilitás
  - teljesség elromlik, ha kiveszünk egy egyenlőséget.
    pl. isZero (suc t) = false
       I.true, I.false, I.isZero (I.suc I.zero)  : I.Tm I.Bool

Normalizálás: egész számok

ℤ := (ℕ × ℕ) / ~
(a,b) ~ (a',b') := (a+b' = a'+b)

-2  := (0,2), ...
-1  := (0,1), (10,11)
0   := (0,0), (1,1), (2,2), ...
1   := (1,0), (2,1), ...
2   := (2,0), (3,1), (4,2), ...

algebrai struktúra:                                    
Z    : Set                                             Ty : Set
pair : ℕ → ℕ → Z                                       Nat : Ty
eq   : (a + b' = a' + b) → (pair a b = pair a' b')     Tm : Ty → Set
                                                       zero : Tm Nat
                                                       _+_ : Tm Nat → Tm Nat → Tm Nat
                                                       +β₁ : zero + t = t

normálforma:                                           normálforma:
vagy negatív nemnulla, vagy nulla, vagy                Tm Nat    ℕ
pozitív nemnulla                                       0,1,2,3,4,...

Nf := ℕ ⊎ (𝟙 ⊎ ℕ)
ι₁ 0 =: -1  negativ
ι₁ 1 =: -2
ι₁ 2 =: -3
...
ι₂ (ι₁ *) =: 0   null
ι₂ (ι₂ 0) =: 1
ι₂ (ι₂ 1) =: 2
ι₂ (ι₂ 2) =: 3
...


ℤ := I.Z

norm : I.Z → Nf
⌜_⌝ : Nf → I.Z
teljesseg: ⌜ norm z ⌝ = z
stabilitas: norm ⌜ n ⌝ = n

norm := M.⟦_⟧ : I.Z → M.Z

M.Z    := Nf
M.pair : ℕ → ℕ → Nf
M.pair   a  b :=
M.pair   10 12 := ι₁ 1
M.pair   0   0 := ι₂ (ι₁ *) = "0"
M.pair   1   0 := ι₂ (ι₂ 0) = "1"
M.pair   2   0 := ι₂ (ι₂ 1) = "2"
M.pair   2   1 := ι₂ (ι₂ 0) = "1"
M.pair   3   2 := ι₂ (ι₂ 0) = "1"
M.eq   : (a + b' = a' + b) → (M.pair a b = M.pair a' b')

ugyanúgy normalizálás

======================================================================

normalizálás: NatBool, ℤ, szabad egységelemes félcsoport (monoid) A felett

C   : Set
u   : C
_*_ : C → C → C
ass : (x * y) * z = x * (y * z)
idl : u * x = x
idr : x * u = x
inj : A → C

Mik a normálformák?

norm : I.C → Nf

iniciális algebra:

 u      *           *             *               *           *      *
       / \         / \           / \             / \         / \    / \ = x
      u   u       u   inj       *   inj         *   z   =   x   *  u   x   
                       |       / \   |         / \             / \
                       a      u  inj a         x  y           y   z     *     
                                  |                                    / \ = x
                                  a'                                  x   u   

                                   *       
                                  / \      
                                inj inj   
                                 |   |    
                                 a'  a    
1. u-k eltüntethetők
2. ass-el át tudom rendezni a fákat, hogy

   *
  /  \
inj   *
 |   /  \
 a₁ inj  *
    |    / \                a₁ ∷ a₂ ∷ a₃ ∷ ... ∷ aₙ ∷ []
    a₂ inj   *              [a₁,a₂,a₃,...,aₙ]
        |   /\
        a₃   ....
              / \
            inj  u
             |
             aₙ

tetszőleges n-re van n db A-beli elemünk

A-k listája = iniciális A feletti monoid
normálforma
 [a]
<A>Array

nil, cons
[], _:_
    _∷_

norm : I.C → List A
norm := St.⟦_⟧
St.⟦_⟧ : I.C → St.C

St.C := List A
St.u := []
xs St.* ys := _++_ (listák összefűzése)
St.inj : A → List A
St.inj a := [a] = (a ∷ []) (csak a-t tartalmazó lista)
St.ass : (xs ++ ys) ++ zs = xs + (ys ++ zs)
St.idl : [] ++ xs = xs
St.idr : xs ++ [] = xs

normálformák beágyazhatók a szintaxisba:
⌜_⌝ : List A → I.C
⌜ [] ⌝ := I.u
⌜ a ∷ as ⌝ := (I.inj a) I.* ⌜ as ⌝

              *
            /  \
         inj   ⌜as⌝
          |
          a

pl. A = ℕ
⌜ [1,5,3] ⌝ = I.inj 1 I.* (I.inj 5 I.* (I.inj 3 I.* I.u))

norm = St.⟦_⟧   (standard: listák, St.u = [], St._*_ = _++_, St.inj = [_])

teljesség: (t : I.C) → ⌜ norm t ⌝ = t
helyesség: (as : List A) → norm ⌜ as ⌝ = as

teljességet függő A feletti monoid algebrát kell megadni (A feletti monoid indukció)

helyesség as szerinti indukcióval:
- norm ⌜ [] ⌝ = norm I.u = St.⟦ I.u ⟧ = []
- (e : norm ⌜ as ⌝ = as)
  norm ⌜ a ∷ as ⌝ =
  norm ((I.inj a) I.* ⌜ as ⌝) =
  St.⟦ (I.inj a) I.* ⌜ as ⌝ ⟧ =
  St.⟦ (I.inj a) ⟧ ++ St.⟦ ⌜ as ⌝ ⟧ =
  [a] ++ St.⟦ ⌜ as ⌝ ⟧ = (ind.hip: e-t)
  [a] ++ as =
  a ∷ as

[1,3,2,3] ++ [5,5,4] = [1,3,2,3,5,5,4]

  ([1,2,0] ++ [2,3]) ++ [6,8,7]
= ([1,2,0,2,3]) ++ [6,8,7]
= [1,2,0,2,3,6,8,7]
= [1,2,0,2,3,6,8,7]
= [1,2,0] ++ ([2,3,6,8,7])
= [1,2,0] ++ ([2,3] ++ [6,8,7])


============================================================================

Volt normalizálás NatBool nyelvre, ugyanazt megadtuk ℤ-ra és A feletti
monoidokra is.

