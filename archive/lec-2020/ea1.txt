Kaposi Ambrus

akaposi@inf.elte.hu

Nyelvek típusrendszere
Gyakorlat, Donkó István

bitbucket.org/akaposi/typesystems/

ide fogok írni; mint egy táblát
interaktívak lehetnek

minden előadás után Canvas kvíz canvas.elte.hu
50% fölötti átlagra van szükség
teszt jellegű kérdések, előadás anyagának megértését ellenőrzi
következő előadásig lehet beadni
időre megy, 20 perc kb (előtte érdemes átnézni az előadás anyagát)
az első kvíz nagyon egyszerű lesz
csak az előadáshoz lesz köze, de gondolkodtató
a vizsga eredményébe nem számít bele

vizsgához kötelező a gyakorlati jegy (mikrozh-k óra elején, beadandó)
gyakorlat: Agda programozási nyelv (hasonló a Haskell-hez), Coq hasonló
Coq: van egy nyelv a funkcionális programozásra; van egy másik nyelv a bizonyításokra (taktikák)
Agda: egy nyelv van, a programozás és bizonyítás ugyanaz
Agda-ban van, Coq-ban nincs: Prop univerzum, definíció szerint proof irrelevant; rewrite pragmák

vizsga: valószínűleg: Canvas kvíz, hosszabb és gondolkodtatóbb; Agda feladatok

Tantárgy története: Csörnyei Zoltán oktatta, van egy könyve:
Zoltán Csörnyei.Bevezetés a típusrendszerek elméletébe.  ELTE EötvösKiadó, 2012
Zoltán Csörnyei.Lambda-kalkulus. A funkcionális programozás alapjai. Ty-potex, 2007
Zoltán Csörnyei.Fordítóprogramok. Az informatika alkalmazásai. Typotex,2009.

Matematikaibb módszer: Robert Harper: Practical foundations of programming languages. Nagyon jó könyv (egy kis részét).

Benjamin Pierce: Software foundations (ehhez tartozik Coq kód is)
https://softwarefoundations.cis.upenn.edu/plf-current/index.html

Philip Wadler, Wen Kokke, Programming language foundations in Agda.
https://plfa.github.io/

Simon Castellan, Pierre Clairambault, and Peter Dybjer: Categories with Families: Unityped, Simply Typed, and Dependently Typed

Kocsis Bálint és Széles Márk
Agda formalizálás, jegyzet: https://bitbucket.org/akaposi/typesystems/src/master/main.pdf

Előismeretek:
funkcionális programozás (Haskell) (szigorú előfeltétel)
formális szemantika (WHILE nyelv)
típuselmélet (Agda)
funkcionális nyelvek (Haskell)
formális nyelvek és automaták
logika és számításelmélet
diszkrét matek (algebra, Peano axiómák)

TÉNYLEGES TANANYAG

típuselmélet:
1. programozási nyelvek típusrendszereinek tudománya (programozási nyelvek tanulmányozása)
2. egy konkrét programozási nyelv, Martin-Löf's type theory, erre épül az Agda, Coq, Lean, Idris, Epigram.

2.-hoz kapcsolódik: halmazelmélet (Zermelo-Frankel) 

ebben a tárgyban:
1. kifejezésnyelv (Nat-Bool)
2. változók, definíciók
3. függvénytér (simply typed lambda calculus, simple type theory)
4. szorzat és összeg típusok
5. induktív és koinduktív típusok (Gödel System T)
6. fixpont operátorok (PCF nyelv, típus nélküli lambda kalkulus)
7. polimorf típusok (System F, erre épül a Haskell, ML típusrendszere)
8. altípusok
+. függő típusok (Martin-Löf's type theory, típuselmélet)

Metaelméletünk/metanyelv: Agda, type theory, magyar/angol nyelv, matematika
Objektumelmélet/objektumnyelv/tárgynyelv: 1-8.

1. NatBool nyelv. Mi az, hogy programozási nyelv? Sokféle absztrakciós szinten lehet válaszolni.

kifejezésnyelv, amiben logikai értékek és számok vannak

"if isZero (zero + suc zero) then false else isZero zero"

1.0. Egy program egy természetes szám (Gödel kódolás)

1.1. Programok halmaza, egy program egy sztring

"isZero (suc zero)"
"isZero   (suc zero)"

"trua"

túlságosan tág, két probléma:
- vannak értelmetlen programok (trua)
- vannak feleslegesen megkülönböztetett programok

1.2. Lexikális elemek sorozata

(, ), true, false, if, then, else, zero, suc, isZero, +

[ isZero, (, suc, zero, ) ]

fordítóprogramok: lexing, lexical analysis: string --> lexikális elemek sorozata

Két probléma:
- vannak értelmetlen programok +, ++, isZero if, ((
- vannak feleslegesen megkülönböztetett programok [zero], [(,zero,)] [(,(,zero,),)]


1.3 Absztrakt szintaxisfa

BNF definíció:

T ::= true | false | if T then T else T | zero | suc T | isZero T | T + T

"if isZero (zero + suc zero) then false else isZero zero"
"(if   isZero (  zero +   suc zero) then false else isZero zero)"


       if_then_else_
          /  |   \
         /   |    \
        /    |     \
    isZero  false  isZero
      |              |
      +             zero
     / \
    /   \
  zero   suc
          |
          |
        zero

1.2 --> 1.3: parsing, szintaktikus elemezés

(((zero + zero) + zero))         zero + (zero + zero)  
                                                   
      +                            +               
     / \                          / \              
    /   \                        /   \             
   +     zero                 zero    +
  / \                                / \
 /   \                              /   \
zero  zero                       zero   zero


mint szintaxisfa, 5+0 és az 5 különböznek

      +           5
     / \
    5   0

két probléma:
- túl sok minden megkülönböztetünk: zero, zero + zero
- vannak értelmetlen programok: isZero true, 

"(false" 1.1 és 1.2 ok. 1.3 szinten nincs


1.4 well typed syntax



[5,+,0] = [5]
