-- {-# OPTIONS --prop --rewriting #-}
{-# OPTIONS --prop #-}
module gy04 where

open import Lib

import NatBoolAST as AST
import NatBoolWT as WT
import NatBool as NB

module WT' where
  open WT
  open WT.I

  -- Normalization in the well typed standard algebra

  p : Tm Bool
  p =
    isZero (
      ite
        (isZero (suc zero))
        (zero + zero)
        (ite true zero (suc (suc zero)))
      )

  q : Tm Bool
  q = ite (isZero (zero + suc zero)) false (isZero (suc zero))

  r : Tm Nat
  r = (zero + (suc zero)) + (zero + (suc (suc zero) + zero))

  s : Tm Nat
  s = (zero + zero) + ((zero + zero) + zero)

  canBeNormalized : ∀ {ty : Ty} → Tm ty → Set
  canBeNormalized {ty = Nat} tm =
    ↑p (St.⟦ tm ⟧t ≡ O) ⊎ Σ ℕ (λ n → ↑p (St.⟦ tm ⟧t ≡ S n))
  canBeNormalized {ty = Bool} tm =
    ↑p (St.⟦ tm ⟧t ≡ O) ⊎ ↑p (St.⟦ tm ⟧t ≡ I)

  np : canBeNormalized p
  np = {!!}

  nq : canBeNormalized q
  nq = {!!}

  nr : canBeNormalized r
  nr = {!!}

  ns : canBeNormalized s
  ns = {!!}

  nn : canBeNormalized {!!}
  nn = {!!}

module NB' where

  open NB.I

  -- Many previously different terms are now equal

  eq-1 : isZero (suc zero) ≡ false
  eq-1 = {!!}

  eq-2 : ite true (isZero zero) false ≡ true
  eq-2 = {!!}

  eq-2' : ite true (isZero zero) false ≡ true
  eq-2' = {!!}

  eq-3 : ite (isZero zero) (suc zero + suc zero) zero
         ≡
         suc (suc zero)
  eq-3 = {!!}

  eq-4 : (ite false (suc zero + suc zero) zero + zero) ≡ zero
  eq-4 = {!!}

  -- But still, there are terms that can be distinguished

  Diff : NB.Algebra
  Diff = record
    { Ty       = {!!}
    ; Tm       = {!!}
    ; Nat      = {!!}
    ; Bool     = {!!}
    ; zero     = {!!}
    ; suc      = {!!}
    ; isZero   = {!!}
    ; _+_      = {!!}
    ; true     = {!!}
    ; false    = {!!}
    ; ite      = {!!}
    ; isZeroβ₁ = {!!}
    ; isZeroβ₂ = {!!}
    ; +β₁      = {!!}
    ; +β₂      = {!!}
    ; iteβ₁    = {!!}
    ; iteβ₂    = {!!}
    }
  module Diff = NB.Algebra Diff

  neq' : ¬ I ≡ O
  neq' eq = {!!}

  neq'' : ¬ Diff.⟦ true ⟧t ≡ Diff.⟦ false ⟧t
  neq'' eq = {!!}

  neq : ¬ true ≡ false
  neq eq = {!!}

  -- Product of two NatBool algebras

  prod : ∀ {i} → (M N : NB.Algebra {i}) → NB.Algebra
  prod {i} M N = record
    { Ty       = {!!}
    ; Tm       = {!!}
    ; Nat      = {!!}
    ; Bool     = {!!}
    ; zero     = {!!}
    ; suc      = {!!}
    ; isZero   = {!!}
    ; _+_      = {!!}
    ; true     = {!!}
    ; false    = {!!}
    ; ite      = {!!}
    ; isZeroβ₁ = {!!}
    ; isZeroβ₂ = {!!}
    ; +β₁      = {!!}
    ; +β₂      = {!!}
    ; iteβ₁    = {!!}
    ; iteβ₂    = {!!}
    }
    where
      module M = NB.Algebra M
      module N = NB.Algebra N

-- Conversions between levels

-- AST→WT

AST→WT : AST.Algebra
AST→WT = record
  { Tm     = {!!}
  ; zero   = {!!}
  ; suc    = {!!}
  ; isZero = {!!}
  ; _+_    = {!!}
  ; true   = {!!}
  ; false  = {!!}
  ; ite    = {!!}
  }
  where open WT.I
module AST→WT = AST.Algebra AST→WT

-- WT→AST

WT→AST : WT.Algebra
WT→AST = record
  { Ty       = {!!}
  ; Tm       = {!!}
  ; Nat      = {!!}
  ; Bool     = {!!}
  ; zero     = {!!}
  ; suc      = {!!}
  ; isZero   = {!!}
  ; _+_      = {!!}
  ; true     = {!!}
  ; false    = {!!}
  ; ite      = {!!}
  }
  where open AST.I
module WT→AST = WT.Algebra WT→AST

-- WT→NB

WT→NB : WT.Algebra
WT→NB = record
  { Ty       = {!!}
  ; Tm       = {!!}
  ; Nat      = {!!}
  ; Bool     = {!!}
  ; zero     = {!!}
  ; suc      = {!!}
  ; isZero   = {!!}
  ; _+_      = {!!}
  ; true     = {!!}
  ; false    = {!!}
  ; ite      = {!!}
  }
  where open NB.I
module WT→NB = WT.Algebra WT→NB

-- NB→WT

NB→WT : NB.Algebra
NB→WT = record
  { Ty       = {!!}
  ; Tm       = {!!}
  ; Nat      = {!!}
  ; Bool     = {!!}
  ; zero     = {!!}
  ; suc      = {!!}
  ; isZero   = {!!}
  ; _+_      = {!!}
  ; true     = {!!}
  ; false    = {!!}
  ; ite      = {!!}
  ; isZeroβ₁ = {!!}
  ; isZeroβ₂ = {!!}
  ; +β₁      = {!!}
  ; +β₂      = {!!}
  ; iteβ₁    = {!!}
  ; iteβ₂    = {!!}
  }
  where open WT.I
module NB→WT = NB.Algebra NB→WT
