{-# OPTIONS --prop --rewriting #-}
module gy07 where

open import Lib

open import DefABT
open import DefWT

module ABT where
  open DefABT.I
  -- Exercise 2.4. Rewrite the following (closed) terms with De Bruijn notation.
  -- (page 28.)

  -- let x:=1 in x + let y:=x+1 in y + let z:=x+y in (x+z)+(y+x)
  a : Tm 0
  a = {!!}

  -- (let x:=1 in x) + let y:=1 in y + let z:=x+y in (x+z)+(y+x)
  b : Tm 1
  b = {!!}

  -- (let x:=1 in x + let y:=x+1 in y) + let z:=1 in z+z
  c : Tm 0
  c = {!!}

  -- (let x:=1 in x) + (let y:=1 in y) + let z:=1 in z+z
  d : Tm 0
  d = {!!}

  -- let x:=(isZero true) in (ite x zero x)
  e : Tm 0
  e = {!!}

  -- CountFree

  zeroVec : (n : ℕ) → Vec ℕ n
  zeroVec O = []
  zeroVec (S n) = O :: zeroVec n

  incVec : {n : ℕ} → Var n → Vec ℕ n → Vec ℕ n
  incVec vz (x :: vec) = S x :: vec
  incVec (vs v) (x :: vec) = x :: incVec v vec

  unitVec : {n : ℕ} → Var n → Vec ℕ n
  unitVec {n} v = incVec v (zeroVec n)

  addVec : {n : ℕ} → Vec ℕ n → Vec ℕ n → Vec ℕ n
  addVec [] [] = []
  addVec (m :: v₁) (n :: v₂) = (m +ℕ n) :: (addVec v₁ v₂)

  tail : {n : ℕ} → Vec ℕ (S n) → Vec ℕ n
  tail (x :: v) = v

  countFree : {n : ℕ} → Tm n → Vec ℕ n
  countFree tm = {!!}

  testCountFree = {!countFree {3} ((v0 + v1) + (v2 + v0))!}

  -- Weakening

  subTm-1 : Tm 1
  subTm-1 = isZero v0

  subTm-2 : Tm 2
  subTm-2 = isZero v0

  wkVar : {n : ℕ} → Var n → Var (S n)
  wkVar v = {!!}

  wkVar-n : {n : ℕ} → (m : ℕ) → Var n → Var (m +ℕ n)
  wkVar-n n v = {!!}

  wk : {n : ℕ} → Tm n → Tm (S n)
  wk tm = {!!}

  subTm-≡ : wk subTm-1 ≡ subTm-2
  subTm-≡ = refl

  wk-n : {n : ℕ} → (m : ℕ) → Tm n → Tm (m +ℕ n)
  wk-n m tm = {!!}

  subTm-n : (n : ℕ) → Tm (S n)
  subTm-n n = {!!}

module WT where
  open DefWT.I

  -- let x:=1 in x + let y:=x+1 in y + let z:=x+y in (x+z)+(y+x)
  a : Tm ∙ Nat
  a = {!!}

  -- (let x:=1 in x) + let y:=1 in y + let z:=x+y in (x+z)+(y+x)
  b : Tm (∙ ▹ Nat) Nat
  b = {!!}

  -- (let x:=false in (if x then y else (isZero zero)))
  c : Tm (∙ ▹ Bool) Bool
  c = {!!}

  -- (let x:=1 in (if (isZero x) then (let y:=true in y) else y)
  d : Tm ∙ Bool
  d = {!!}

  -- let x:=(isZero zero) in (ite x false x)
  e : Tm ∙ Bool
  e = {!!}

  --

  swapVar : {Γ : Con} → {A B C : Ty} → Var (Γ ▹ A ▹ B) C -> Var (Γ ▹ B ▹ A) C
  swapVar v = {!!}

  wk : {Γ Δ : Con} → {A B : Ty} → Tm (Γ ++ Δ) A -> Tm ((Γ ▹ B) ++ Δ) A
  wk = {!!}

  Std : Algebra
  Std = record
    { Ty     = Set
    ; Nat    = ℕ
    ; Bool   = 𝟚
    ; Con    = Set
    ; ∙      = ↑p 𝟙
    ; _▹_    = _×_
    ; Var    = λ Γ A → Γ → A
    ; vz     = π₂
    ; vs     = λ x γ → x (π₁ γ)
    ; Tm     = λ Γ A → Γ → A
    ; var    = {!!}
    ; def    = {!!}
    ; zero   = {!!}
    ; suc    = {!!}
    ; isZero = {!!}
    ; _+_    = {!!}
    ; true   = {!!}
    ; false  = {!!}
    ; ite    = {!!}
    }
