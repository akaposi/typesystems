{-# OPTIONS --prop #-}
module gy09 where

open import Lib renaming (_∘_ to _∘f_; _,_ to _,Σ_)

open import Def

open I

sub-eq-3 :
  {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
  (σ , t) ∘ δ ≡ (σ ∘ δ , t [ δ ])
sub-eq-3 {σ = σ} {δ = δ} {t = t} =
  (σ , t) ∘ δ
    ≡⟨ ▹η ⁻¹ ⟩
  p ∘ ((σ , t) ∘ δ) , q [ (σ , t) ∘ δ ]
    ≡⟨ cong-2 _,_ (ass ⁻¹) ([∘] ⁻¹) ⟩
  p ∘ (σ , t) ∘ δ , q [ σ , t ] [ δ ]
    ≡⟨ cong-2 _,_ (cong (_∘ δ) ▹β₁) (cong (_[ δ ]) ▹β₂) ⟩
  σ ∘ δ , t [ δ ]
    ∎

sub-eq-4 :
  {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
  (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
sub-eq-4 {σ = σ} {t = t} {u = u} =
  (def t u) [ σ ]
    ≡⟨ refl ⟩
  u [ id , t ] [ σ ]
    ≡⟨ [∘] ⟩
  u [ (id , t) ∘ σ ]
    ≡⟨ cong (u [_]) (
      (id , t) ∘ σ
        ≡⟨ sub-eq-3 ⟩
      id ∘ σ , t [ σ ]
        ≡⟨ cong-2 _,_ (
          id ∘ σ
            ≡⟨ idl ◾ idr ⁻¹ ⟩
          σ ∘ id
            ≡⟨ cong (σ ∘_) (▹β₁ ⁻¹) ⟩
          σ ∘ (p ∘ (id , t [ σ ]))
            ≡⟨ ass ⁻¹ ⟩
          σ ∘ p ∘ (id , t [ σ ])
            ∎
        ) (▹β₂ ⁻¹) ⟩
      σ ∘ p ∘ (id , t [ σ ]) , q [ id , t [ σ ] ]
        ≡⟨ sub-eq-3 ⁻¹ ⟩
      (σ ∘ p , q) ∘ (id , t [ σ ])
        ∎
    ) ⟩
  u [ (σ ∘ p , q) ∘ (id , t [ σ ]) ]
    ≡⟨ [∘] ⁻¹ ⟩
  u [ σ ∘ p , q ] [ id , t [ σ ] ]
    ≡⟨ refl ⟩
  def (t [ σ ]) (u [ σ ∘ p , q ])
    ∎

sub-eq-5 :
  {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} →
  (σ ∘ p , q {A = A}) ∘ (δ ∘ p , q) ≡ (σ ∘ δ) ∘ p , q
sub-eq-5 {σ = σ} {δ = δ} =
  (σ ∘ p , q) ∘ (δ ∘ p , q)
    ≡⟨ sub-eq-3 ⟩
  σ ∘ p ∘ (δ ∘ p , q) , q [ δ ∘ p , q ]
    ≡⟨ cong-2 _,_ (
      σ ∘ p ∘ (δ ∘ p , q)
        ≡⟨ ass ⟩
      σ ∘ (p ∘ (δ ∘ p , q))
        ≡⟨ cong (σ ∘_) ▹β₁ ⟩
      σ ∘ (δ ∘ p)
        ≡⟨ ass ⁻¹ ⟩
      σ ∘ δ ∘ p
        ∎
    ) ▹β₂ ⟩
  (σ ∘ δ) ∘ p , q
    ∎
