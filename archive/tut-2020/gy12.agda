{-# OPTIONS --prop --rewriting #-}
module gy12 where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)

module I where
  data Ty      : Set where
    -- Bool       : Ty
    Prod       : Ty → Ty → Ty

  data Con     : Set where
    ∙          : Con
    _▹_        : Con → Ty → Con

  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_

  postulate
    Sub        : Con → Con → Set
    Tm         : Con → Ty → Set

    _∘_        : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id         : ∀{Γ} → Sub Γ Γ
    ass        : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                 (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl        : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr        : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]       : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]       : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]        : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                 t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _,_        : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η         : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    -- true       : ∀{Γ} → Tm Γ Bool
    -- false      : ∀{Γ} → Tm Γ Bool
    -- ite        : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    -- iteβ₁      : ∀{Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    -- iteβ₂      : ∀{Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    -- true[]     : ∀{Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    -- false[]    : ∀{Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    -- ite[]      : ∀{Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
    --              (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])

    pair       : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (Prod A B)
    recProd    : ∀{Γ A B C} → Tm (Γ ▹ A ▹ B) C → Tm Γ (Prod A B) → Tm Γ C
    Prodβ      : ∀{Γ A B C}{t : Tm (Γ ▹ A ▹ B) C}{u : Tm Γ A}{v : Tm Γ B} →
                 recProd t (pair u v) ≡ (t [ id , u , v ])
    pair[]     : ∀{Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
                 pair u v [ σ ] ≡ pair (u [ σ ]) (v [ σ ])
    recProd[]  : ∀{Γ Δ A B C}{t : Tm (Δ ▹ A ▹ B) C}{u : Tm Δ (Prod A B)}{σ : Sub Γ Δ} →
                 recProd t u [ σ ] ≡ recProd (t [ (σ ∘ p , q) ∘ p , q ]) (u [ σ ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  -- {-# REWRITE iteβ₁ iteβ₂ true[] false[] ite[] #-}
  {-# REWRITE Prodβ pair[] recProd[] #-}

module DepAlgebra where

  record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
    infixl 6 _∘_
    infixl 6 _[_]
    infixl 5 _▹_
    infixl 5 _,_

    field
      Con : I.Con → Set i
      Ty  : I.Ty → Set j
      Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
      Tm  : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

      _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
        Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
      id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
      ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
        {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
        {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
        (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
      idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
        id ∘ σ ≡ σ
      idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
        σ ∘ id ≡ σ

      _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
        Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
      [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
        t [ id ] ≡ t
      [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
        {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
        {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
        t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

      ∙ : Con I.∙
      ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
      ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
        σ =[ ap (Sub Γ ∙) I.∙η ]= ε

      _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')
      _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
        Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
      p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p
      q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
      ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
        {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
      ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
        {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
      ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
        {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ

      -- Bool : Ty I.Bool
      -- true : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.true
      -- false : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.false
      -- ite : ∀ {Γ' A' b' u' v'}  {Γ : Con Γ'}{A : Ty A'} →
      --   Tm Γ Bool b' → Tm Γ A u' → Tm Γ A v' → Tm Γ A (I.ite b' u' v')
      -- true[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      --   true [ σ ] ≡ true
      -- false[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      --   false [ σ ] ≡ false
      -- ite[] : ∀ {Γ' Δ' A' b' u' v' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      --   {b : Tm Δ Bool b'}{u : Tm Δ A u'}{v : Tm Δ A v'}
      --   {σ : Sub Γ Δ σ'} →
      --   (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
      -- iteβ₁ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      --   {u : Tm Γ A u'}{v : Tm Γ A v'} → ite true u v ≡ u
      -- iteβ₂ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      --   {u : Tm Γ A u'}{v : Tm Γ A v'} → ite false u v ≡ v

      Prod       : ∀ {A' B'} → Ty A' → Ty B' → Ty (I.Prod A' B')
      pair       : ∀{Γ' A' B' u' v'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm Γ A u' → Tm Γ B v' → Tm Γ (Prod A B) (I.pair u' v')
      recProd    : ∀{Γ' A' B' C' u' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'} → Tm (Γ ▹ A ▹ B) C u' → Tm Γ (Prod A B) t' → Tm Γ C (I.recProd u' t')
      Prodβ      : ∀{Γ' A' B' C' u' v' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}{t : Tm (Γ ▹ A ▹ B) C t'}{u : Tm Γ A u'}{v : Tm Γ B v'} →
                  recProd t (pair u v) ≡ (t [ id , u , v ])
      pair[]     : ∀{Γ' Δ' A' B' u' v' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{u : Tm Δ A u'}{v : Tm Δ B v'}{σ : Sub Γ Δ σ'} →
                  pair u v [ σ ] ≡ pair (u [ σ ]) (v [ σ ])
      recProd[]  : ∀{Γ' Δ' A' B' C' u' t' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}{t : Tm (Δ ▹ A ▹ B) C t'}{u : Tm Δ (Prod A B) u'}{σ : Sub Γ Δ σ'} →
                  recProd t u [ σ ] ≡ recProd (t [ (σ ∘ p , q) ∘ p , q ]) (u [ σ ])

    def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
    def t u = u [ id , t ]

    -------------------------------------------
    -- eliminator
    -------------------------------------------

    ⟦_⟧T : (A : I.Ty) → Ty A
    -- ⟦ I.Bool ⟧T = Bool
    ⟦ I.Prod A B ⟧T = Prod ⟦ A ⟧T ⟦ B ⟧T

    ⟦_⟧C : (Γ : I.Con) → Con Γ
    ⟦ I.∙ ⟧C = ∙
    ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

    postulate
      ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
      ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t

      ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
        ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
      ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
      ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
      ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
        ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
      ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
      {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

      ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
      ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
        ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
      {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

      -- ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
      -- ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
      -- ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      --   ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
      -- {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

      ⟦pair⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
        ⟦ I.pair u v ⟧t ≡ pair ⟦ u ⟧t ⟦ v ⟧t
      ⟦recProd⟧ : ∀{Γ A B C}{u : I.Tm (Γ I.▹ A I.▹ B) C}{t : I.Tm Γ (I.Prod A B)} →
        ⟦ I.recProd u t ⟧t ≡ recProd ⟦ u ⟧t ⟦ t ⟧t
      {-# REWRITE ⟦pair⟧ ⟦recProd⟧ #-}

    ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
      ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
    ⟦def⟧ = refl

module Canonicity where

  open I
  open DepAlgebra

  -- data PBool : Tm ∙ Bool → Set where
  --   Ptrue  : PBool true
  --   Pfalse : PBool false

  -- Pite :
  --   {A' : Ty}{t' : Tm ∙ Bool}{u' v' : Tm ∙ A'}(A : Tm ∙ A' → Set) →
  --   PBool t' → A u' → A v' → A (ite t' u' v')
  -- Pite = ?

  -- PcanBool : ∀{t'} → PBool t' → ↑p (t' ≡ true) ⊎ ↑p (t' ≡ false)
  -- PcanBool = ?

  -- Prod

  data PProd {A' B' : Ty}(A : Tm ∙ A' → Set)(B : Tm ∙ B' → Set) : Tm ∙ (Prod A' B') → Set where
    Ppair : {a' : Tm ∙ A'}{b' : Tm ∙ B'} → A a' → B b' → PProd A B (pair a' b')

  PrecProd :
    {A' B' C' : Ty}{u' : Tm (∙ ▹ A' ▹ B') C'}{t' : Tm ∙ (Prod A' B')}
    {A : Tm ∙ A' → Set}{B : Tm ∙ B' → Set}(C : Tm ∙ C' → Set)
    (u : ∀{a'}{b'} → A a' → B b' → C (u' [ id , a' , b' ]))
    (t : PProd A B t') → C (recProd u' t')
  PrecProd = ?

  PcanProd :
    {A' B' : Ty} → {t' : Tm ∙ (Prod A' B')} → {A : Tm ∙ A' → Set} → {B : Tm ∙ B' → Set} → PProd A B t' →
    Σ (Tm ∙ A') λ u' → Σ (Tm ∙ B') λ v' → ↑p (t' ≡ pair u' v')
  PcanProd = ?

  Can : DepAlgebra
  Can = record
        { Con        = λ Γ' → Sub ∙ Γ' → Set
        ; Sub        = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
        ; Ty         = λ A' → Tm ∙ A' → Set
        ; Tm         = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
        ; _∘_        = λ σ δ x → σ (δ x)
        ; id         = idf
        ; ass        = refl
        ; idl        = refl
        ; idr        = refl
        ; _[_]       = λ t σ x → t (σ x)
        ; [id]       = refl
        ; [∘]        = refl
        ; ∙          = λ _ → ↑p 𝟙
        ; ε          = λ _ → *↑
        ; ∙η         = refl
        ; _▹_        = λ Γ A σ' → Γ (p ∘ σ') ⊗ A (q [ σ' ])
        ; _,_        = λ σ t x → σ x ,Σ t x
        ; p          = π₁
        ; q          = π₂
        ; ▹β₁        = refl
        ; ▹β₂        = refl
        ; ▹η         = refl

        -- ; Bool       = ?
        -- ; true       = ?
        -- ; false      = ?
        -- ; ite        = ?
        -- ; iteβ₁      = ?
        -- ; iteβ₂      = ?
        -- ; true[]     = ?
        -- ; false[]    = ?
        -- ; ite[]      = ?

        ; Prod       = ?
        ; pair       = ?
        ; recProd    = ?
        ; Prodβ      = ?
        ; pair[]     = ?
        ; recProd[]  = ?
        }
  module Can = DepAlgebra.DepAlgebra Can

  open I

  -- canBool : (t : Tm ∙ Bool) → ↑p (t ≡ true) ⊎ ↑p (t ≡ false)
  -- canBool t = ?

  canProd : ∀{A B}(t : Tm ∙ (Prod A B)) → Σ (Tm ∙ A) λ u → Σ (Tm ∙ B) λ v → ↑p (t ≡ pair u v)
  canProd t = ?
