{-# OPTIONS --prop --rewriting #-}
module gy11 where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_)

open import Fun.Algebra
open I

St : Algebra
St = record
       { Con      = Set
       ; STy      = ?
       ; Ty       = Set
       ; Sub      = λ Γ Δ → (Γ → Δ)
       ; Tm       = λ Γ A → (Γ → A)
       ; ∙        = ↑p 𝟙
       ; _▹_      = _×_ -- λ Γ A → Γ × A
       ; Nat      = ℕ
       ; Bool     = 𝟚
       ; sty      = ?
       ; _⇒_      = ?
       ; _∘_      = _∘f_ -- λ σ δ γ → σ (δ γ)
       ; id       = idf -- λ γ → γ
       ; ε        = const *↑ -- λ _ → *↑
       ; _,_      = λ σ t γ → (σ γ) ,Σ (t γ)
       ; p        = π₁ -- λ { (γ ,Σ a) → γ }
       ; q        = π₂ -- λ { (γ ,Σ a) → a }
       ; _[_]     = _∘f_ -- λ t σ γ → t (σ γ)
       ; lam      = ?
       ; app      = ?
       ; zero     = const O -- λ _ → O
       ; suc      = S ∘f_ -- λ t γ → S (t γ)
       ; isZero   = λ t γ → 0 ≟ℕ (t γ)
       ; _+_      = λ m n γ → m γ +ℕ n γ
       ; true     = const I
       ; false    = const O
       ; ite      = λ b t f γ → if b γ then t γ else f γ
       ; ass      = refl
       ; idl      = refl
       ; idr      = refl
       ; ∙η       = refl
       ; ▹β₁      = refl
       ; ▹β₂      = refl
       ; ▹η       = refl
       ; [id]     = refl
       ; [∘]      = refl
       ; ⇒β       = refl
       ; ⇒η       = refl
       ; lam[]    = refl
       ; zero[]   = refl
       ; suc[]    = refl
       ; isZero[] = refl
       ; +[]      = refl
       ; true[]   = refl
       ; false[]  = refl
       ; ite[]    = refl
       ; isZeroβ₁ = refl
       ; isZeroβ₂ = refl
       ; +β₁      = refl
       ; +β₂      = refl
       ; iteβ₁    = refl
       ; iteβ₂    = refl
       }
open Algebra St using (⟦_⟧ST ; ⟦_⟧T ; ⟦_⟧t)

norm : {A : Ty} → Tm ∙ A → ⟦ A ⟧T
norm t = ?

⌜_⌝ : {A : STy} → ⟦ A ⟧ST → Tm ∙ (sty A)
⌜_⌝ {A} t = ?

isZero-⌜⌝ : {n : ℕ} → ⌜ 0 ≟ℕ n ⌝ ≡ isZero ⌜ n ⌝
isZero-⌜⌝ {n} = ?

+-⌜⌝ : {m n : ℕ} → ⌜ m +ℕ n ⌝ ≡ ⌜ m ⌝ + ⌜ n ⌝
+-⌜⌝ {m} {n} = ?

-- ite-⌜⌝ : ∀ {A b t f} → ⌜ if b then t else f ⌝ ≡ ite {A = sty A} ⌜ b ⌝ ⌜ t ⌝ ⌜ f ⌝
-- ite-⌜⌝ {b = O} = refl
-- ite-⌜⌝ {b = I} = refl

P-ite : {A : Ty} → {P : Tm ∙ A → Set} →
        {b : Tm ∙ (sty Bool)} → {t f : Tm ∙ A} →
        ⌜ norm b ⌝ ≡ b → P t → P f →
        P (ite b t f)
P-ite {A} {P} {b} {t} {f} eq Pt Pf = ?

Comp : DepAlgebra
Comp = record
         { Con      = ?
         ; STy      = ?
         ; Ty       = ?
         ; Sub      = ?
         ; Tm       = ?
         ; ∙        = ?
         ; _▹_      = ?
         ; Nat      = ?
         ; Bool     = ?
         ; sty      = ?
         ; _⇒_      = ?
         ; _∘_      = ?
         ; id       = ?
         ; ε        = ?
         ; _,_      = ?
         ; p        = ?
         ; q        = ?
         ; _[_]     = ?
         ; lam      = ?
         ; app      = ?
         ; zero     = ?
         ; suc      = ?
         ; isZero   = ?
         ; _+_      = ?
         ; true     = ?
         ; false    = ?
         ; ite      = ?
         ; ass      = refl
         ; idl      = refl
         ; idr      = refl
         ; ∙η       = refl
         ; ▹β₁      = refl
         ; ▹β₂      = refl
         ; ▹η       = refl
         ; [id]     = refl
         ; [∘]      = refl
         ; ⇒β       = refl
         ; ⇒η       = refl
         ; lam[]    = refl
         ; zero[]   = refl
         ; suc[]    = refl
         ; isZero[] = refl
         ; +[]      = refl
         ; true[]   = refl
         ; false[]  = refl
         ; ite[]    = refl
         ; isZeroβ₁ = refl
         ; isZeroβ₂ = refl
         ; +β₁      = refl
         ; +β₂      = refl
         ; iteβ₁    = refl
         ; iteβ₂    = refl
         }
module Comp = DepAlgebra Comp

completeness : {A : STy} → {t : Tm ∙ (sty A)} → ⌜ norm t ⌝ ≡ t
completeness {A} {t} = ?

stability : {A : STy} → {t : ⟦ sty A ⟧T} → norm {sty A} ⌜ t ⌝ ≡ t
stability {A} {t} = ?
