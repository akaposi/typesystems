{-# OPTIONS --prop #-}
module gy08 where

open import Lib hiding (_,_; _∘_)

open import Def

open I

v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
v0 = {!!}
v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
v1 = {!!}
v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = {!!}
v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = {!!}

var-test-1 : def true v0 ≡ true
var-test-1 =
  def true v0
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  true
    ∎

var-test-2 : def true (def false v0) ≡ false
var-test-2 =
  def true (def false v0)
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  false
    ∎

var-test-3 : def true (def false v1) ≡ true
var-test-3 =
  def true (def false v1)
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  true
    ∎

sub-1 : Sub (∙ ▹ Nat) (∙ ▹ Bool ▹ Nat)
sub-1 = {!!}

sub-2 : {Γ : Con} → {A : Ty} → Sub (Γ ▹ Bool ▹ A ▹ A) (Γ ▹ A)
sub-2 = {!!}

sub-swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
sub-swap = {!!}

sub-3 : Sub (∙ ▹ Nat) (∙ ▹ Bool)
sub-3 = {!!}

tm-3 : Tm (∙ ▹ Bool) Nat
tm-3 = ite v0 (suc zero) zero

sub-3-test : tm-3 [ sub-3 ] ≡ {!!}
sub-3-test =
  tm-3 [ sub-3 ]
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ∎

-- let x:=1 in x + x = 2
eq-1 : def (suc zero) (v0 + v0) ≡ suc (suc zero)
eq-1 =
  def (suc zero) (v0 + v0)
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  suc (suc zero)
    ∎

sub-eq-1 : {Γ : Con} → {σ δ : Sub Γ ∙} → σ ≡ δ
sub-eq-1 = {!!}

sub-eq-2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → ε ∘ σ ≡ ε
sub-eq-2 = {!!}

sub-eq-3 :
  {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
  (σ , t) ∘ δ ≡ (σ ∘ δ , t [ δ ])
sub-eq-3 {σ = σ} {δ = δ} {t = t} =
  (σ , t) ∘ δ
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  {!!}
    ≡⟨ {!!} ⟩
  (σ ∘ δ , t [ δ ])
    ∎

sub-eq-4 :
  {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
  (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
sub-eq-4 = {!!}
