{-# OPTIONS --prop --rewriting #-}
module gy10 where

open import Lib renaming (_∘_ to _∘f_; _,_ to _,Σ_)
import Lemmas

open import Def

open I

Std : Algebra
Std = record
       { Con      = ?
       ; Ty       = ?
       ; Sub      = ?
       ; Tm       = ?
       ; ∙        = ?
       ; _▹_      = ?
       ; Nat      = ?
       ; Bool     = ?
       ; _∘_      = ?
       ; id       = ?
       ; ε        = ?
       ; _,_      = ?
       ; p        = ?
       ; q        = ?
       ; _[_]     = ?
       ; zero     = ?
       ; suc      = ?
       ; isZero   = ?
       ; _+_      = ?
       ; true     = ?
       ; false    = ?
       ; ite      = ?
       ; ass      = ?
       ; idl      = ?
       ; idr      = ?
       ; ∙η       = ?
       ; ▹β₁      = ?
       ; ▹β₂      = ?
       ; ▹η       = ?
       ; [id]     = ?
       ; [∘]      = ?
       ; zero[]   = ?
       ; suc[]    = ?
       ; isZero[] = ?
       ; +[]      = ?
       ; true[]   = ?
       ; false[]  = ?
       ; ite[]    = ?
       ; isZeroβ₁ = ?
       ; isZeroβ₂ = ?
       ; +β₁      = ?
       ; +β₂      = ?
       ; iteβ₁    = ?
       ; iteβ₂    = ?
       }
module St = Algebra St

normBool : Tm ∙ Bool → 𝟚
normBool t = ?

normNat : Tm ∙ Nat → ℕ
normNat t = ?

norm : ?
norm t = ?

norm-test = ?
norm-test-2 = ?

--

VarDiff : Algebra
VarDiff = record
       { Con      = ?
       ; Ty       = ?
       ; Sub      = ?
       ; Tm       = ?
       ; ∙        = ?
       ; _▹_      = ?
       ; Nat      = ?
       ; Bool     = ?
       ; _∘_      = ?
       ; id       = ?
       ; ε        = ?
       ; _,_      = ?
       ; p        = ?
       ; q        = ?
       ; _[_]     = ?
       ; zero     = ?
       ; suc      = ?
       ; isZero   = ?
       ; _+_      = ?
       ; true     = ?
       ; false    = ?
       ; ite      = ?
       ; ass      = ?
       ; idl      = ?
       ; idr      = ?
       ; ∙η       = ?
       ; ▹β₁      = ?
       ; ▹β₂      = ?
       ; ▹η       = ?
       ; [id]     = ?
       ; [∘]      = ?
       ; zero[]   = ?
       ; suc[]    = ?
       ; isZero[] = ?
       ; +[]      = ?
       ; true[]   = ?
       ; false[]  = ?
       ; ite[]    = ?
       ; isZeroβ₁ = ?
       ; isZeroβ₂ = ?
       ; +β₁      = ?
       ; +β₂      = ?
       ; iteβ₁    = ?
       ; iteβ₂    = ?
       }
module VarDiff = Algebra VarDiff

v0≢v1 : ¬ v0 ≡ v1
v0≢v1 = ?

--

ConDiff : Algebra
ConDiff = record
       { Con      = ?
       ; Ty       = ?
       ; Sub      = ?
       ; Tm       = ?
       ; ∙        = ?
       ; _▹_      = ?
       ; Nat      = ?
       ; Bool     = ?
       ; _∘_      = ?
       ; id       = ?
       ; ε        = ?
       ; _,_      = ?
       ; p        = ?
       ; q        = ?
       ; _[_]     = ?
       ; zero     = ?
       ; suc      = ?
       ; isZero   = ?
       ; _+_      = ?
       ; true     = ?
       ; false    = ?
       ; ite      = ?
       ; ass      = ?
       ; idl      = ?
       ; idr      = ?
       ; ∙η       = ?
       ; ▹β₁      = ?
       ; ▹β₂      = ?
       ; ▹η       = ?
       ; [id]     = ?
       ; [∘]      = ?
       ; zero[]   = ?
       ; suc[]    = ?
       ; isZero[] = ?
       ; +[]      = ?
       ; true[]   = ?
       ; false[]  = ?
       ; ite[]    = ?
       ; isZeroβ₁ = ?
       ; isZeroβ₂ = ?
       ; +β₁      = ?
       ; +β₂      = ?
       ; iteβ₁    = ?
       ; iteβ₂    = ?
       }
module ConDiff = Algebra ConDiff

∙≢▹ : {Γ : Con} → {A : Ty} → (¬ ∙ ≡ (Γ ▹ A))
∙≢▹ = ?

--

ConEq : Algebra
ConEq = record
       { Con      = ?
       ; Ty       = ?
       ; Sub      = ?
       ; Tm       = ?
       ; ∙        = ?
       ; _▹_      = ?
       ; Nat      = ?
       ; Bool     = ?
       ; _∘_      = ?
       ; id       = ?
       ; ε        = ?
       ; _,_      = ?
       ; p        = ?
       ; q        = ?
       ; _[_]     = ?
       ; zero     = ?
       ; suc      = ?
       ; isZero   = ?
       ; _+_      = ?
       ; true     = ?
       ; false    = ?
       ; ite      = ?
       ; ass      = ?
       ; idl      = ?
       ; idr      = ?
       ; ∙η       = ?
       ; ▹β₁      = ?
       ; ▹β₂      = ?
       ; ▹η       = ?
       ; [id]     = ?
       ; [∘]      = ?
       ; zero[]   = ?
       ; suc[]    = ?
       ; isZero[] = ?
       ; +[]      = ?
       ; true[]   = ?
       ; false[]  = ?
       ; ite[]    = ?
       ; isZeroβ₁ = ?
       ; isZeroβ₂ = ?
       ; +β₁      = ?
       ; +β₂      = ?
       ; iteβ₁    = ?
       ; iteβ₂    = ?
       }
module ConEq = Algebra ConEq

∙≡▹ : {Γ : Con} → {A : Ty} → (ConEq.⟦ ∙ ⟧C ≡ ConEq.⟦ (Γ ▹ A) ⟧C)
∙≡▹ = ?

--

SUC-INJ : Prop
SUC-INJ = ?

suc-inj : SUC-INJ
suc-inj = ?

--

ISZERO-INJ : Prop
ISZERO-INJ = ?

1≢2 : ¬ 1 ≡ 2
1≢2 = ?

one-neq-two : ¬ (suc zero) ≡ (suc (suc zero))
one-neq-two = ?

not-isZero-inj : ¬ ISZERO-INJ
not-isZero-inj = ?

--

decEqℕ : (m n : ℕ) → (m ≡ n ⊎p (¬ m ≡ n))
decEqℕ m n = ?

decEq𝟚 : (a b : 𝟚) → (a ≡ b ⊎p (¬ a ≡ b))
decEq𝟚 a b = ?

decEqTm : {A : Ty} → (t t' : Tm ∙ A) → (t ≡ t' ⊎p (¬ t ≡ t'))
decEqTm {A} t t' = ?
