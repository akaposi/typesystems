{-# OPTIONS --prop #-}
module gy01 where

-- ketelemu halmaz

data 𝟚 : Set where -- \b2
  O I : 𝟚

boolean : 𝟚
boolean = O

boolean' : 𝟚
boolean' = {!!}

idb : 𝟚 → 𝟚
idb x = x

neg : 𝟚 → 𝟚
neg O = I
neg I = O

-- C-c C-n

boolean'' : 𝟚
boolean'' = neg (neg (neg (idb O)))

boolean''' : 𝟚
boolean''' = idb (neg (neg (neg (idb O))))

idb' : 𝟚 → 𝟚 -- mi a kulonbseg idb es idb' kozott?
idb' O = O
idb' I = I

or : 𝟚 → 𝟚 → 𝟚
or = {!!}

-- termeszetes szamok

data ℕ : Set where
  O : ℕ
  S : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}

_*2+1 : ℕ → ℕ
O *2+1 = 1
S n *2+1 = S (S (n *2+1))

anatural : ℕ
anatural = 3 *2+1

-- C-c C-n

anatural' : ℕ
anatural' = 7 *2+1

add5 : ℕ → ℕ
add5 n = {!!}

-- egyenloseg

data Id (A : Set)(a : A) : A → Prop where
  refl : Id A a a

-- unit teszteket tudunk irni:

test0 : Id 𝟚 boolean'' I
test0 = refl

test1 : Id ℕ anatural 7
test1 = refl

test2 : Id ℕ (add5 5) 10
test2 = {!!}

test3 : Id ℕ (add5 10) 15
test3 = {!!}

-- extra feladatok:

constO : 𝟚 → 𝟚
constO = {!!}

constI : 𝟚 → 𝟚
constI = {!!}

-- hany darab 𝟚 → 𝟚 fuggveny van?

and : 𝟚 → 𝟚 → 𝟚
and = {!!}

xor : 𝟚 → 𝟚 → 𝟚
xor = {!!}

plus : ℕ → ℕ → ℕ
plus = {!!}

testplus : Id ℕ (plus 23 12) 35
testplus = {!!}

isEq : ℕ → ℕ → 𝟚
isEq a b = {!!}

testIsEq : Id 𝟚 (isEq 23 100) O
testIsEq = {!!}

testIsEq' : Id 𝟚 (isEq 23 23) I
testIsEq' = {!!}

idl-plus : (a : ℕ) → Id ℕ (plus 0 a) a
idl-plus = {!!}

idr-plus : (a : ℕ) → Id ℕ (plus a 0) a
idr-plus = {!!}
