\section{Canonicity}

\begin{code}[hide]
open I

-- Bool

data PBool : Tm ∙ Bool → Set where
  Ptrue  : PBool true
  Pfalse : PBool false

Pite :
  {A' : Ty}{t' : Tm ∙ Bool}{u' v' : Tm ∙ A'}(A : Tm ∙ A' → Set) →
  PBool t' → A u' → A v' → A (ite t' u' v')
Pite {u' = u'} {v'} A Ptrue  u v = u
Pite {u' = u'} {v'} A Pfalse u v = v

PcanBool : ∀{t'} → PBool t' → ↑p (t' ≡ true) ⊎ ↑p (t' ≡ false)
PcanBool Ptrue  = ι₁ ↑[ refl ]↑
PcanBool Pfalse = ι₂ ↑[ refl ]↑

-- Nat

\end{code}

To prove canonicity for natural numbers as, the predicate for natural
numbers \verb$PNat$ is given inductively saying that the preicate
holds for \verb$zero$ and if it holds for a number, it holds for its
successor.
\begin{code}
data PNat : Tm ∙ Nat → Set where
  Pzero : PNat zero
  Psuc : ∀{n'} → PNat n' → PNat (suc n')
\end{code}
To define \verb$iteNat$ in the canonicity dependent algebra, we will need
that if the terms \verb$u'$ and \verb$v'$ preserve the predicate for a type
\verb$A'$, then if \verb$PNat$ holds for \verb$t'$, then the predicate for \verb$A'$
holds for \verb$iteNat u' v' t'$:
\begin{code}
PiteNat :
  {A' : Ty}{u' : Tm ∙ A'}{v' : Tm (∙ ▹ A') A'}{t' : Tm ∙ Nat}
  (A : Tm ∙ A' → Set)(u : A u')(v : ∀{w'} → A w' → A (v' [ id , w' ]))
  (t : PNat t') →
  A (iteNat u' v' t')
PiteNat {A'}{u'}{v'}{.zero}    A u v Pzero    = u
PiteNat {A'}{u'}{v'}{.(suc _)} A u v (Psuc t) = v (PiteNat A u v t)
\end{code}
If the predicate \verb$PNat$ holds for a term, it is either zero or the successor of
another term.
\begin{code}
PcanNat : ∀{t'} → PNat t' → ↑p (t' ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t' ≡ suc u)
PcanNat Pzero    = ι₁ ↑[ refl ]↑
PcanNat (Psuc t) with PcanNat t
... | ι₁ e = ι₂ (zero ,Σ ↑[ ap suc ↓[ e ]↓ ]↑)
... | ι₂ (t' ,Σ e) = ι₂ (suc t' ,Σ ↑[ ap suc ↓[ e ]↓ ]↑)
\end{code}
\begin{code}[hide]
-- This is simpler ;)
PcanNat' : ∀{t'} → PNat t' → ↑p (t' ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t' ≡ suc u)
PcanNat' Pzero         = ι₁ refl↑
PcanNat' (Psuc {n'} t) = ι₂ (n' ,Σ refl↑)
\end{code}

\begin{code}[hide]

-- List

\end{code}
We have a similar predicate for lists and a function which shows that the recursor
preserves this predicate.
\begin{code}
data PList {A'}(A : Tm ∙ A' → Set) : Tm ∙ (List A') → Set where
  Pnil : PList A nil
  Pcons : ∀{u' t'} → A u' → PList A t' → PList A (cons u' t')

PiteList :
  {A' B' : Ty}{u' : Tm ∙ B'}{v' : Tm (∙ ▹ A' ▹ B') B'}{t' : Tm ∙ (List A')}
  {A : Tm ∙ A' → Set}(B : Tm ∙ B' → Set)(u : B u')(v : ∀{w₁' w₂'} → A w₁' → B w₂' → B (v' [ id , w₁' , w₂' ]))(t : PList A t') →
  B (iteList u' v' t')
PiteList B u v Pnil = u
PiteList B u v (Pcons w t) = v w (PiteList B u v t)
\end{code}
Canonicity for lists follows from the predicate;
\begin{code}
PcanList :
  {A' : Ty} → {t' : Tm ∙ (List A')} → {A : Tm ∙ A' → Set} → PList A t' →
  ↑p (t' ≡ nil) ⊎ Σ (Tm ∙ A') λ a' → Σ (Tm ∙ (List A')) λ as' → ↑p (t' ≡ cons a' as')
PcanList Pnil = ι₁ refl↑
PcanList (Pcons {a'} {as'} a as) = ι₂ (a' ,Σ (as' ,Σ refl↑))
\end{code}
\begin{code}[hide]

-- Tree

\end{code}
The same for trees:
\begin{code}
data PTree {A' : Ty} (A : Tm ∙ A' → Set) : Tm ∙ (Tree A') → Set where
  Pleaf : {a' : Tm ∙ A'} → A a' → PTree A (leaf a')
  Pnode : {ll' rr' : Tm ∙ (Tree A')} →
           PTree A ll' → PTree A rr' → PTree A (node ll' rr')

PiteTree :
  {A' B' : Ty} → {l' : Tm (∙ ▹ A') B'} → {n' : Tm (∙ ▹ B' ▹ B') B'} → {t' : Tm ∙ (Tree A')} →
  {A : Tm ∙ A' → Set} → (B : Tm ∙ B' → Set) →
  (l : {a' : Tm ∙ A'} → A a' → B (l' [ id , a' ])) →
  (n : {ll rr : Tm ∙ B'} → B ll → B rr → B (n' [ id , ll , rr ])) →
  (t : PTree A t') → B (iteTree l' n' t')
PiteTree B l n (Pleaf a) = l a
PiteTree B l n (Pnode ll rr) = n (PiteTree B l n ll) (PiteTree B l n rr)

PcanTree :
  {A' : Ty} → {t' : Tm ∙ (Tree A')} → {A : Tm ∙ A' → Set} → PTree A t' →
  (Σ (Tm ∙ A') λ a' → ↑p (t' ≡ leaf a')) ⊎
  Σ (Tm ∙ (Tree A')) λ ll' → Σ (Tm ∙ (Tree A')) λ rr' → ↑p (t' ≡ node ll' rr')
PcanTree (Pleaf {a'} a) = ι₁ (a' ,Σ refl↑)
PcanTree (Pnode {ll'} {rr'} ll rr) = ι₂ (ll' ,Σ (rr' ,Σ refl↑))
\end{code}
Now we can define the new components of the canonicity dependent algebra:
\begin{code}
Can : DepAlgebra
Can = iteord
       { Con        = λ Γ' → Sub ∙ Γ' → Set
       ; Sub        = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
       ; Ty         = λ A' → Tm ∙ A' → Set
       ; Tm         = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
       ; _∘_        = λ σ δ x → σ (δ x)
       ; id         = idf
       ; ass        = refl
       ; idl        = refl
       ; idr        = refl
       ; _[_]       = λ t σ x → t (σ x)
       ; [id]       = refl
       ; [∘]        = refl
       ; ∙          = λ _ → ↑p 𝟙
       ; ε          = λ _ → *↑
       ; ∙η         = refl
       ; _▹_        = λ Γ A σ' → Γ (p ∘ σ') ×ₘ A (q [ σ' ])
       ; _,_        = λ σ t x → σ x ,Σ t x
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = refl
       ; ▹β₂        = refl
       ; ▹η         = refl
\end{code}
\begin{code}[hide]
       ; Bool       = PBool
       ; true       = λ _ → Ptrue
       ; false      = λ _ → Pfalse
       ; ite        = λ where {A = A} t u v {ν'} ν̂ → Pite A (t ν̂) (u ν̂) (v ν̂)
       ; iteβ₁      = refl
       ; iteβ₂      = refl
       ; true[]     = refl
       ; false[]    = refl
       ; ite[]      = refl
\end{code}
\begin{code}
       ; Nat        = PNat
       ; zero       = λ _ → Pzero
       ; suc        = λ t ν̂ → Psuc (t ν̂)
       ; iteNat     = λ where {A = A} u v t {ν'} ν̂ → PiteNat A (u ν̂) (λ ŵ → v (ν̂ ,Σ ŵ)) (t ν̂)
       ; Natβ₁      = refl
       ; Natβ₂      = refl
       ; zero[]     = refl
       ; suc[]      = refl
       ; iteNat[]   = refl
\end{code}
\begin{code}[hide]
       ; _⇒_        = λ {A'}{B'} A B t' → (u' : Tm ∙ A') → A u' → B (t' $ u')
       ; lam        = λ t {ν'} ν̂ u' û → t {ν' , u'} (ν̂ ,Σ û)
       ; app        = λ t {ν'} ν̂ → t {p ∘ ν'} (π₁ ν̂) (q [ ν' ]) (π₂ ν̂)
       ; ⇒β         = refl
       ; ⇒η         = refl
       ; lam[]      = refl

       ; Unit       = λ _ → ↑p 𝟙
       ; tt         = _
       ; Unitη      = refl

       ; _×_        = λ A B t' → A (proj₁ t') ×ₘ B (proj₂ t')
       ; ⟨_,_⟩      = λ u v ν̂ → u ν̂ ,Σ v ν̂
       ; proj₁      = λ t ν̂ → π₁ (t ν̂)
       ; proj₂      = λ t ν̂ → π₂ (t ν̂)
       ; ×β₁        = refl
       ; ×β₂        = refl
       ; ×η         = refl
       ; ⟨,⟩[]      = refl
\end{code}
\begin{code}
       ; List       = PList
       ; nil        = λ _ → Pnil
       ; cons       = λ u t ν̂ → Pcons (u ν̂) (t ν̂)
       ; iteList    = λ where {B = B} u v t ν̂ → PiteList B (u ν̂) (λ w₁ w₂ → v (ν̂ ,Σ w₁ ,Σ w₂)) (t ν̂)
       ; Listβ₁     = refl
       ; Listβ₂     = refl
       ; nil[]      = refl
       ; cons[]     = refl
       ; iteList[]  = refl

       ; Tree       = PTree
       ; leaf       = λ a ν̂ → Pleaf (a ν̂)
       ; node       = λ ll rr ν̂ → Pnode (ll ν̂) (rr ν̂)
       ; iteTree    = λ where {B = B} l n t ν̂ → PiteTree B (λ b → l (ν̂  ,Σ b)) (λ ll rr → n (ν̂ ,Σ ll ,Σ rr)) (t ν̂ )
       ; Treeβ₁     = refl
       ; Treeβ₂     = refl
       ; leaf[]     = refl
       ; node[]     = refl
       ; iteTree[]  = refl
\end{code}
\begin{code}[hide]
       }
module Can = DepAlgebra Can
\end{code}
Interpretating into the canonicity dependent algebra gives actual canonicity for
terms:
\begin{code}
canNat : (t : Tm ∙ Nat) → ↑p (t ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t ≡ suc u)
canNat t = PcanNat (Can.⟦ t ⟧t {id} ↑[ * ]↑)

canList : ∀{A}(t : Tm ∙ (List A)) →
  ↑p (t ≡ nil) ⊎ Σ (Tm ∙ A) λ u → Σ (Tm ∙ (List A)) λ v → ↑p (t ≡ cons u v)
canList t = PcanList (Can.⟦ t ⟧t {id} *↑)

canTree : ∀{A}(t : Tm ∙ (Tree A)) →
  (Σ (Tm ∙ A) λ a → ↑p (t ≡ leaf a)) ⊎
  Σ (Tm ∙ (Tree A)) λ ll → Σ (Tm ∙ (Tree A)) λ rr → ↑p (t ≡ node ll rr)
canTree t = PcanTree (Can.⟦ t ⟧t {id} *↑)
-}
\end{code}
