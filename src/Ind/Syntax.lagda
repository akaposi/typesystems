\begin{code}
{-# OPTIONS --prop --rewriting #-}
module Ind.Syntax where

open import Lib

infixl 6 _[_]
infixl 5 _▹_
infixr 6 _⇒_
infixl 5 _$_

data Ty   : Set where
  _⇒_     : Ty → Ty → Ty
  Unit    : Ty
  Bool    : Ty
  Nat     : Ty
  List    : Ty → Ty
  Tree    : Ty → Ty

data Con  : Set where
  ◇       : Con
  _▹_     : Con → Ty → Con

data Var : Con → Ty → Set where
  vz        : ∀{Γ A} → Var (Γ ▹ A) A
  vs        : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

postulate
  Tm        : Con → Ty → Set

data Sub : Con → Con → Set where
  p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
  ⟨_⟩       : ∀{Γ A} → Tm Γ A → Sub Γ (Γ ▹ A)
  _⁺        : ∀{Γ Δ A} → (σ : Sub Δ Γ) → Sub (Δ ▹ A) (Γ ▹ A)

postulate
  var       : ∀{Γ A} → Var Γ A → Tm Γ A
  _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
  [p]       : ∀{Γ A B x} → var {Γ}{A} x [ p {A = B} ] ≡ var (vs x)
  vz[⟨⟩]    : ∀{Γ A t} → var (vz {Γ}{A}) [ ⟨ t ⟩ ] ≡ t
  vz[⁺]     : ∀{Γ Δ A σ} → var (vz {Γ}{A}) [ σ ⁺ ] ≡ var (vz {Δ}{A})
  vs[⟨⟩]    : ∀{Γ A B x t} → var (vs {Γ}{A}{B} x) [ ⟨ t ⟩ ] ≡ var x
  vs[⁺]     : ∀{Γ Δ A B x σ} → var (vs {Γ}{A}{B} x) [ σ ⁺ ] ≡ var x [ σ ] [ p {Δ} ]

  lam    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  _$_    : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  ⇒β     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ ⟨ u ⟩ ]
  ⇒η     : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → t ≡ lam (t [ p ] $ var vz)
  lam[]  : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{σ : Sub Δ Γ} → lam t [ σ ] ≡ lam (t [ σ ⁺ ])
  $[]    : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{σ : Sub Δ Γ} →
           (t $ u) [ σ ] ≡ (t [ σ ]) $ (u [ σ ])

  true       : ∀{Γ} → Tm Γ Bool
  false      : ∀{Γ} → Tm Γ Bool
  iteBool    : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
  Boolβ₁     : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
  Boolβ₂     : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
  true[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
  false[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
  iteBool[]  : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
               iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])

  zeroo      : ∀{Γ} → Tm Γ Nat
  suco       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
  iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
  Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
  Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
               iteNat u v (suco t) ≡ v [ ⟨ iteNat u v t ⟩ ]
  zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
  suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
  iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
               iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⁺ ]) (t [ γ ])

  trivial    : ∀{Γ} → Tm Γ Unit
  iteUnit    : ∀{Γ A} → Tm Γ A → Tm Γ Unit → Tm Γ A
  Unitβ      : ∀{Γ A t} → iteUnit {Γ}{A} t trivial ≡ t
  trivial[]  : ∀{Γ Δ}{γ : Sub Δ Γ} → trivial [ γ ] ≡ trivial
  iteUnit[]  : ∀{Γ A t u Δ}{γ : Sub Δ Γ} →
               iteUnit {Γ}{A} u t [ γ ] ≡ iteUnit (u [ γ ]) (t [ γ ])

  nil        : ∀{Γ A} → Tm Γ (List A)
  cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
  iteList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
  Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → iteList u v nil ≡ u
  Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
               iteList u v (cons t₁ t) ≡ (v [ ⟨ t₁ ⟩ ⁺ ] [ ⟨ iteList u v t ⟩ ])
  nil[]      : ∀{Γ A Δ}{γ : Sub Δ Γ} → nil {Γ}{A} [ γ ] ≡ nil {Δ}{A}
  cons[]     : ∀{Γ A}{t₁ : Tm Γ A}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
               (cons t₁ t) [ γ ] ≡ cons (t₁ [ γ ]) (t [ γ ])
  iteList[]  : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
               iteList u v t [ γ ] ≡ iteList (u [ γ ]) (v [ γ ⁺ ⁺  ]) (t [ γ ])

  leaf       : ∀{Γ A} → Tm Γ A → Tm Γ (Tree A)
  node       : ∀{Γ A} → Tm Γ (Tree A) → Tm Γ (Tree A) → Tm Γ (Tree A)
  iteTree    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm (Γ ▹ B ▹ B) B → Tm Γ (Tree A) → Tm Γ B
  Treeβ₁     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{a : Tm Γ A} → iteTree l n (leaf a) ≡ l [ ⟨ a ⟩ ]
  Treeβ₂     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{ll rr : Tm Γ (Tree A)} →
               iteTree l n (node ll rr) ≡ n [ ⟨ iteTree l n ll ⟩ ⁺ ] [ ⟨ iteTree l n rr ⟩ ]
  leaf[]     : ∀{Γ A}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (leaf a) [ γ ] ≡ leaf (a [ γ ])
  node[]     : ∀{Γ A}{ll rr : Tm Γ (Tree A)}{Δ}{γ : Sub Δ Γ} →
               (node ll rr) [ γ ] ≡ node (ll [ γ ]) (rr [ γ ])
  iteTree[]  : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{t : Tm Γ (Tree A)}{Δ}{γ : Sub Δ Γ} →
               iteTree l n t [ γ ] ≡ iteTree (l [ γ ⁺ ]) (n [ γ ⁺ ⁺ ]) (t [ γ ])


-- these are provable
postulate
  [p][⁺]     : ∀{Γ A}{a : Tm Γ A}{B}{Δ}{γ : Sub Δ Γ} → a [ p {Γ}{B} ] [ γ ⁺ ] ≡ a [ γ ] [ p ]
  [p⁺][⁺⁺]   : ∀{Γ C A}{a : Tm (Γ ▹ C) A}{B}{Δ}{γ : Sub Δ Γ} → a [ p {Γ}{B} ⁺ ] [ γ ⁺ ⁺ ] ≡ a [ γ ⁺ ] [ p ⁺ ]
  [p][⟨⟩]    : ∀{Γ A}{a : Tm Γ A}{B}{b : Tm Γ B} → a [ p ] [ ⟨ b ⟩ ] ≡ a
  [p⁺][⟨⟩⁺]  : ∀{Γ A C}{a : Tm (Γ ▹ C) A}{B}{b : Tm Γ B} → a [ p ⁺ ] [ ⟨ b ⟩ ⁺ ] ≡ a
  [⟨⟩][]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → t [ ⟨ a ⟩ ] [ γ ] ≡ t [ γ ⁺ ] [ ⟨ a [ γ ] ⟩ ]
  [p⁺][⟨vz⟩] : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → t [ p ⁺ ] [ ⟨ var vz ⟩ ] ≡ t

def : {Γ : Con}{A B : Ty} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
def t u = u [ ⟨ t ⟩ ]

private
  Var' : ℕ → Con → Ty → Set
  Var' zero Γ A = Var Γ A
  Var' (suc n) Γ A = ∀ {B} → Var' n (Γ ▹ B) A

  Tm' : ℕ → Con → Ty → Set
  Tm' zero Γ A = Tm Γ A
  Tm' (suc n) Γ A = ∀ {B} → Tm' n (Γ ▹ B) A

  vs' : (n : ℕ) {Γ : Con} {A B : Ty} → Var' n Γ A → Var' n (Γ ▹ B) A
  vs' zero x = vs x
  vs' (suc n) x = vs' n x

  var' : (n : ℕ) {Γ : Con} {A : Ty} → Var' n Γ A → Tm' n Γ A
  var' zero x = var x
  var' (suc n) x = var' n x

  v' : (n : ℕ) {Γ : Con} {A : Ty} → Var' n (Γ ▹ A) A
  v' zero = vz
  v' (suc n) = vs' n (v' n)

v : (n : ℕ) {Γ : Con} {A : Ty} → Tm' n (Γ ▹ A) A
v n = var' n (v' n)

v0 : ∀{Γ A} → Tm (Γ ▹ A) A
v0 = var vz
v1 : ∀{Γ A B} → Tm (Γ ▹ A ▹ B) A
v1 = var (vs vz)
v2 : ∀{Γ A B C} → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = var (vs (vs vz))
v3 : ∀{Γ A B C D} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = var (vs (vs (vs vz)))
v4 : ∀{Γ A B C D E} → Tm (Γ ▹ A ▹ B ▹ C ▹ D ▹ E) A
v4 = var (vs (vs (vs (vs vz))))
\end{code}
