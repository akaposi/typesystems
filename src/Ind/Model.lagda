\begin{code}
{-# OPTIONS --prop --rewriting #-}
module Ind.Model where

open import Lib
import Ind.Syntax as I

record Model {i j} : Set (lsuc (i ⊔ j)) where
  infixl 6 _[_]
  infixl 5 _▹_
  infixr 6 _⇒_
  infixl 5 _$_

  field
    Ty        : Set i

    Con       : Set i
    ◇         : Con
    _▹_       : Con → Ty → Con

    Var       : Con → Ty → Set j
    vz        : ∀{Γ A} → Var (Γ ▹ A) A
    vs        : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

    Tm        : Con → Ty → Set j

    Sub       : Con → Con → Set j
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    ⟨_⟩       : ∀{Γ A} → Tm Γ A → Sub Γ (Γ ▹ A)
    _⁺        : ∀{Γ Δ A} → (σ : Sub Δ Γ) → Sub (Δ ▹ A) (Γ ▹ A)

    var       : ∀{Γ A} → Var Γ A → Tm Γ A
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [p]       : ∀{Γ A B x} → var {Γ}{A} x [ p {A = B} ] ≡ var (vs x)
    vz[⟨⟩]    : ∀{Γ A t} → var (vz {Γ}{A}) [ ⟨ t ⟩ ] ≡ t
    vz[⁺]     : ∀{Γ Δ A σ} → var (vz {Γ}{A}) [ σ ⁺ ] ≡ var (vz {Δ}{A})
    vs[⟨⟩]    : ∀{Γ A B x t} → var (vs {Γ}{A}{B} x) [ ⟨ t ⟩ ] ≡ var x
    vs[⁺]     : ∀{Γ Δ A B x σ} → var (vs {Γ}{A}{B} x) [ σ ⁺ ] ≡ var x [ σ ] [ p {Δ} ]
    
    _⇒_    : Ty → Ty → Ty
    lam    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_    : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ ⟨ u ⟩ ]
    ⇒η     : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → t ≡ lam (t [ p ] $ var vz)
    lam[]  : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{σ : Sub Δ Γ} → lam t [ σ ] ≡ lam (t [ σ ⁺ ])
    $[]    : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{σ : Sub Δ Γ} →
             (t $ u) [ σ ] ≡ (t [ σ ]) $ (u [ σ ])

    Bool       : Ty
    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    iteBool    : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
    Boolβ₁     : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
    Boolβ₂     : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
    true[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    iteBool[]  : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                 iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])
    
    Nat        : Ty
    zeroo      : ∀{Γ} → Tm Γ Nat
    suco       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                 iteNat u v (suco t) ≡ v [ ⟨ iteNat u v t ⟩ ]
    zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
    suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
    iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                 iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⁺ ]) (t [ γ ])

    Unit       : Ty
    trivial    : ∀{Γ} → Tm Γ Unit
    iteUnit    : ∀{Γ A} → Tm Γ A → Tm Γ Unit → Tm Γ A
    Unitβ      : ∀{Γ A t} → iteUnit {Γ}{A} t trivial ≡ t
    trivial[]  : ∀{Γ Δ}{γ : Sub Δ Γ} → trivial [ γ ] ≡ trivial
    iteUnit[]  : ∀{Γ A t u Δ}{γ : Sub Δ Γ} →
                 iteUnit {Γ}{A} u t [ γ ] ≡ iteUnit (u [ γ ]) (t [ γ ])

    List       : Ty → Ty
    nil        : ∀{Γ A} → Tm Γ (List A)
    cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
    iteList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
    Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → iteList u v nil ≡ u
    Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                 iteList u v (cons t₁ t) ≡ (v [ ⟨ t₁ ⟩ ⁺ ] [ ⟨ iteList u v t ⟩ ])
    nil[]      : ∀{Γ A Δ}{γ : Sub Δ Γ} → nil {Γ}{A} [ γ ] ≡ nil {Δ}{A}
    cons[]     : ∀{Γ A}{t₁ : Tm Γ A}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                 (cons t₁ t) [ γ ] ≡ cons (t₁ [ γ ]) (t [ γ ])
    iteList[]  : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                 iteList u v t [ γ ] ≡ iteList (u [ γ ]) (v [ γ ⁺ ⁺  ]) (t [ γ ])

    Tree       : Ty → Ty
    leaf       : ∀{Γ A} → Tm Γ A → Tm Γ (Tree A)
    node       : ∀{Γ A} → Tm Γ (Tree A) → Tm Γ (Tree A) → Tm Γ (Tree A)
    iteTree    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm (Γ ▹ B ▹ B) B → Tm Γ (Tree A) → Tm Γ B
    Treeβ₁     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{a : Tm Γ A} → iteTree l n (leaf a) ≡ l [ ⟨ a ⟩ ]
    Treeβ₂     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{ll rr : Tm Γ (Tree A)} →
                 iteTree l n (node ll rr) ≡ n [ ⟨ iteTree l n ll ⟩ ⁺ ] [ ⟨ iteTree l n rr ⟩ ]
    leaf[]     : ∀{Γ A}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (leaf a) [ γ ] ≡ leaf (a [ γ ])
    node[]     : ∀{Γ A}{ll rr : Tm Γ (Tree A)}{Δ}{γ : Sub Δ Γ} →
                 (node ll rr) [ γ ] ≡ node (ll [ γ ]) (rr [ γ ])
    iteTree[]  : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{t : Tm Γ (Tree A)}{Δ}{γ : Sub Δ Γ} →
                 iteTree l n t [ γ ] ≡ iteTree (l [ γ ⁺ ]) (n [ γ ⁺ ⁺ ]) (t [ γ ])

  ⟦_⟧T : (A : I.Ty) → Ty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Unit ⟧T = Unit
  ⟦ I.List A ⟧T = List ⟦ A ⟧T
  ⟦ I.Tree A ⟧T = Tree ⟦ A ⟧T

  ⟦_⟧C : I.Con → Con
  ⟦ I.◇ ⟧C = ◇
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  ⟦_⟧v : ∀{Γ A} → I.Var Γ A  → Var ⟦ Γ ⟧C ⟦ A ⟧T
  ⟦ I.vz ⟧v = vz
  ⟦ I.vs x ⟧v = vs ⟦ x ⟧v

  postulate
    ⟦_⟧t : ∀{Γ A} → I.Tm Γ A  → Tm  ⟦ Γ ⟧C ⟦ A ⟧T

  ⟦_⟧s : ∀{Γ Δ} → I.Sub Δ Γ → Sub ⟦ Δ ⟧C  ⟦ Γ ⟧C
  ⟦ I.p ⟧s = p
  ⟦ I.⟨ t ⟩ ⟧s = ⟨ ⟦ t ⟧t ⟩
  ⟦ σ I.⁺ ⟧s = ⟦ σ ⟧s ⁺

  postulate
    ⟦var⟧     : ∀{Γ A}  {x : I.Var Γ A}              → ⟦ I.var x   ⟧t ≈ var ⟦ x ⟧v
    ⟦[]⟧      : ∀{Γ Δ A}{t :  I.Tm Γ A}{σ : I.Sub Δ Γ} → ⟦ t I.[ σ ] ⟧t ≈ ⟦ t ⟧t [ ⟦ σ ⟧s ]
    {-# REWRITE ⟦var⟧ ⟦[]⟧ #-}

    ⟦lam⟧     : ∀{Γ A B t} → ⟦ I.lam {Γ}{A}{B} t ⟧t ≈ lam ⟦ t ⟧t
    ⟦$⟧       : ∀{Γ A B}{t : I.Tm Γ (A I.⇒ B)}{u} → ⟦ t I.$ u ⟧t ≈ ⟦ t ⟧t $ ⟦ u ⟧t
    {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}
    
    ⟦true⟧    : ∀{Γ} → ⟦ I.true  {Γ} ⟧t ≈ true
    ⟦false⟧   : ∀{Γ} → ⟦ I.false {Γ} ⟧t ≈ false
    ⟦iteBool⟧ : ∀{Γ A}{t : I.Tm Γ I.Bool}{u v : I.Tm Γ A} → 
                ⟦ I.iteBool u v t ⟧t ≈ iteBool ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t 
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦iteBool⟧ #-}

    ⟦zero⟧    : ∀{Γ} → ⟦ I.zeroo {Γ} ⟧t ≈ zeroo
    ⟦suc⟧     : ∀{Γ t} → ⟦ I.suco {Γ} t ⟧t ≈ suco ⟦ t ⟧t
    ⟦iteNat⟧  : ∀{Γ A t u v} → ⟦ I.iteNat {Γ}{A} t u v ⟧t ≈ iteNat ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦iteNat⟧ #-}

    ⟦trivial⟧    : ∀{Γ} → ⟦ I.trivial {Γ} ⟧t ≈ trivial
    ⟦iteUnit⟧  : ∀{Γ A u t} → ⟦ I.iteUnit {Γ}{A} u t ⟧t ≈ iteUnit ⟦ u ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦trivial⟧ ⟦iteUnit⟧ #-}
    
    ⟦nil⟧      : ∀{Γ A} → ⟦ I.nil {Γ}{A} ⟧t ≈ nil
    ⟦cons⟧     : ∀{Γ A t ts} → ⟦ I.cons {Γ}{A} t ts ⟧t ≈ cons ⟦ t ⟧t ⟦ ts ⟧t
    ⟦iteList⟧  : ∀{Γ A C ts u v} → ⟦ I.iteList {Γ}{A}{C} u v ts ⟧t ≈ iteList ⟦ u ⟧t ⟦ v ⟧t ⟦ ts ⟧t
    {-# REWRITE ⟦nil⟧ ⟦cons⟧ ⟦iteList⟧ #-}

    ⟦leaf⟧     : ∀{Γ A t} → ⟦ I.leaf {Γ}{A} t ⟧t ≈ leaf ⟦ t ⟧t
    ⟦node⟧     : ∀{Γ A t1 t2} → ⟦ I.node {Γ}{A} t1 t2 ⟧t ≈ node ⟦ t1 ⟧t ⟦ t2 ⟧t
    ⟦iteTree⟧  : ∀{Γ A C l n t} → ⟦ I.iteTree {Γ}{A}{C} l n t ⟧t ≈ iteTree ⟦ l ⟧t ⟦ n ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf⟧ ⟦node⟧ ⟦iteTree⟧ #-}
\end{code}
