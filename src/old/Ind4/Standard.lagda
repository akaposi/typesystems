\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Ind4.Standard where

open import Lib renaming (_∘_ to _∘ₘ_ ; _×_ to _×ₘ_ ; _,_ to _⸴_)
open import Ind4.Algebra
\end{code}
\begin{code}
module M where
  Ty : Set₁
  Ty = Set

  Con : Set₁
  Con = Set

  Sub : Con → Con → Set
  Sub Γ Δ = Γ → Δ

  Tm : Con → Ty → Set
  Tm Γ A = Γ → A

  Unit : Ty
  Unit = 𝟙↑

  Empty : Ty
  Empty = 𝟘↑

  _⇒_ : Ty → Ty → Ty
  A ⇒ B = A → B

  _×_ : Ty → Ty → Ty
  _×_ = _×ₘ_

  _+_ : Ty → Ty → Ty
  _+_ = _⊎_

  ∙ : Con
  ∙ = 𝟙↑

  _▻_ : Con → Ty → Con
  _▻_ = _×ₘ_

  _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
  _∘_ = _∘ₘ_

  id : ∀ {Γ} → Sub Γ Γ
  id = idf

  ε : ∀ {Γ} → Sub Γ ∙
  ε = _

  _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▻ A)
  (σ , t) γ = σ γ ⸴ t γ

  p : ∀ {Γ A} → Sub (Γ ▻ A) Γ
  p = π₁

  q : ∀ {Γ A} → Tm (Γ ▻ A) A
  q = π₂

  _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
  _[_] = _∘ₘ_

  lam : ∀ {Γ A B} → Tm (Γ ▻ A) B → Tm Γ (A ⇒ B)
  (lam t) γ a = t (γ ⸴ a)

  app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▻ A) B
  (app t) (γ ⸴ a) = t γ a

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = (app t) [ id , u ]

  tt : ∀ {Γ} → Tm Γ Unit
  tt = _

  ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
  ⟨ u , v ⟩ γ = u γ ⸴ v γ

  proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
  (proj₁ t) γ = π₁ (t γ)

  proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B
  (proj₂ t) γ = π₂ (t γ)

  absurd : ∀ {Γ} A → Tm Γ Empty → Tm Γ A
  (absurd A t) γ = ⟦ ↓[ t γ ]↓ ⟧𝟘

  inl : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
  (inl t) γ = ι₁ (t γ)

  inr : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
  (inr t) γ = ι₂ (t γ)

  case : ∀ {Γ A B C} → Tm Γ (A + B) → Tm (Γ ▻ A) C → Tm (Γ ▻ B) C → Tm Γ C
  (case t) u v γ = ind⊎ _ (λ a → u (γ ⸴ a)) (λ b → v (γ ⸴ b)) (t γ)

  -- inductive types

  data Ar (n : ℕ) : Set where
    X   : Ar n
    _⇛_ : Fin n → Ar n → Ar n
    X⇒_ : Ar n → Ar n

  data Sign (n : ℕ) : Set where
    ◆   : Sign n
    _▷_ : Sign n → Ar n → Sign n

  _ᴬA : ∀ {n} → Ar n → 𝕍 Ty n → Ty → Ty
  (X       ᴬA) P A = A
  ((k ⇛ R) ᴬA) P A = (P ‼ k) ⇒ (R ᴬA) P A
  ((X⇒ R)  ᴬA) P A = A ⇒ (R ᴬA) P A
  
  _ᴬS : ∀ {n} → Sign n → 𝕍 Ty n → Ty → Ty
  (◆       ᴬS) P A = Unit
  ((Ω ▷ R) ᴬS) P A = (Ω ᴬS) P A × (R ᴬA) P A

  data IVar {n} : Sign n → Ar n → Set where
    vz : ∀ {Ω R} → IVar (Ω ▷ R) R
    vs : ∀ {Ω R S} → IVar Ω R → IVar (Ω ▷ S) R
  
  data ITmᴹ {n}(P : 𝕍 Ty n)(Ω : Sign n) : Ar n → Set where
      varᴹ : ∀ {R} → IVar Ω R → ITmᴹ P Ω R
      _$̂ᴹ_ : ∀ {k R} → ITmᴹ P Ω (k ⇛ R) → (P ‼ k) → ITmᴹ P Ω R
      _$̇ᴹ_ : ∀ {R} → ITmᴹ P Ω (X⇒ R) → ITmᴹ P Ω X → ITmᴹ P Ω R

  _ᴬᴹV : ∀ {n P Ω R A} →
    IVar {n} Ω R → (Ω ᴬS) P A → (R ᴬA) P A
  (vz     ᴬᴹV) (ω ⸴ α) = α
  ((vs x) ᴬᴹV) (ω ⸴ α) = (x ᴬᴹV) ω

  _ᴬᴹT : ∀ {n P Ω R A} →
    ITmᴹ {n} P Ω R → (Ω ᴬS) P A → (R ᴬA) P A
  ((varᴹ x) ᴬᴹT) ω = (x ᴬᴹV) ω
  ((t $̂ᴹ u) ᴬᴹT) ω = (t ᴬᴹT) ω u
  ((t $̇ᴹ u) ᴬᴹT) ω = (t ᴬᴹT) ω ((u ᴬᴹT) ω)

  ITm : ∀ {n} → Con → 𝕍 Ty n → Sign n → Ar n → Set
  ITm Γ P Ω R = Γ → ITmᴹ P Ω R

  ind : ∀ {n} → Sign n → 𝕍 Ty n → Ty
  ind Ω P = ITmᴹ P Ω X

  var : ∀ {n Γ P Ω R} → IVar {n} Ω R → ITm Γ P Ω R
  (var x) γ = varᴹ x

  _$̂_ : ∀ {n Γ P Ω k R} →
    ITm {n} Γ P Ω (k ⇛ R) → Tm Γ (P ‼ k) → ITm Γ P Ω R
  (t $̂ u) γ = t γ $̂ᴹ u γ

  _$̇_ : ∀ {n Γ P Ω R} →
    ITm {n} Γ P Ω (X⇒ R) → Tm Γ (ind Ω P) → ITm Γ P Ω R
  (t $̇ u) γ = t γ $̇ᴹ u γ

  _[_]I : ∀ {n Γ Δ P Ω R} → ITm {n} Δ P Ω R → Sub Γ Δ → ITm Γ P Ω R
  _[_]I = _∘ₘ_

  _ᴬV : ∀ {n Γ P Ω R A} →
    IVar {n} Ω R → Tm Γ ((Ω ᴬS) P A) → Tm Γ ((R ᴬA) P A)
  (x ᴬV) ω γ = (x ᴬᴹV) (ω γ)

  _ᴬT : ∀ {n Γ P Ω R A} →
    ITm {n} Γ P Ω R → Tm Γ ((Ω ᴬS) P A) → Tm Γ ((R ᴬA) P A)
  (t ᴬT) ω γ = (t γ ᴬᴹT) (ω γ)

  con : ∀ {n Γ P Ω} → ITm {n} Γ P Ω X → Tm Γ (ind Ω P)
  (con t) γ = t γ

  rec : ∀ {n Γ P}(Ω : Sign n){A} →
    Tm Γ ((Ω ᴬS) P A) → Tm Γ (ind Ω P) → Tm Γ A
  (rec Ω ω t) γ = (t γ ᴬᴹT) (ω γ)

St : Algebra
St = record
       { Ty = M.Ty
       ; Con = M.Con
       ; Ar = M.Ar
       ; Sign = M.Sign
       ; Sub = M.Sub
       ; Tm = M.Tm
       ; IVar = M.IVar
       ; ITm = M.ITm
       ; Unit = M.Unit
       ; Empty = M.Empty
       ; _⇒_ = M._⇒_
       ; _×_ = M._×_
       ; _+_ = M._+_
       ; ind = M.ind
       ; ∙ = M.∙
       ; _▻_ = M._▻_
       ; X = M.X
       ; _⇛_ = M._⇛_
       ; X⇒_ = M.X⇒_
       ; ◆ = M.◆
       ; _▷_ = M._▷_
       ; _ᴬA = M._ᴬA
       ; _ᴬS = M._ᴬS
       ; _∘_ = M._∘_
       ; id = M.id
       ; ε = M.ε
       ; _,_ = M._,_
       ; p = M.p
       ; q = M.q
       ; _[_] = M._[_]
       ; _[_]I = M._[_]I
       ; lam = M.lam
       ; app = M.app
       ; tt = M.tt
       ; ⟨_,_⟩ = M.⟨_,_⟩
       ; proj₁ = M.proj₁
       ; proj₂ = M.proj₂
       ; absurd = M.absurd
       ; inl = M.inl
       ; inr = M.inr
       ; case = M.case
       ; vz = M.vz
       ; vs = M.vs
       ; var = M.var
       ; _$̂_ = M._$̂_
       ; _$̇_ = M._$̇_
       ; _ᴬV = M._ᴬV
       ; _ᴬT = M._ᴬT
       ; con = M.con
       ; rec = M.rec
       ; ᴬAX = refl
       ; ᴬA⇛ = refl
       ; ᴬAX⇒ = refl
       ; ᴬS◆ = refl
       ; ᴬS▷ = refl
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▻β₁ = refl
       ; ▻β₂ = refl
       ; ▻η = refl
       ; [id] = refl
       ; [∘] = refl
       ; [id]I = refl
       ; [∘]I = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; lam[] = refl
       ; Unitη = refl
       ; tt[] = refl
       ; ×β₁ = refl
       ; ×β₂ = refl
       ; ×η = refl
       ; ⟨,⟩[] = refl
       ; absurd[] = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; inl[] = refl
       ; inr[] = refl
       ; case[] = refl
       ; var[]I = refl
       ; $̂[]I = refl
       ; $[]I = refl
       ; ᴬVvz = refl
       ; ᴬVvs = refl
       ; ᴬTvar = refl
       ; ᴬT$̂ = refl
       ; ᴬT$̇ = refl
       ; indβ = refl
       ; con[] = refl
       ; rec[] = refl
       }
module St = Algebra St
\end{code}
