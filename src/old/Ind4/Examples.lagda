\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Ind4.Examples where

open import Lib hiding (_∘_; _×_; _,_; _≤_)
open import Ind4.Algebra
open import Ind4.Standard

open I
open St using (⟦_⟧T; ⟦_⟧C; ⟦_⟧P; ⟦_⟧A; ⟦_⟧S; ⟦_⟧σ ; ⟦_⟧t; ⟦_⟧V; ⟦_⟧I)

q1 : ∀ {Γ A B} → Tm (Γ ▻ A ▻ B) A
q1 = q [ p ]

q2 : ∀ {Γ A B C} → Tm (Γ ▻ A ▻ B ▻ C) A
q2 = q [ p ∘ p ]

_$2_$_ : ∀ {Γ A B C} → Tm Γ (A ⇒ B ⇒ C) → Tm Γ A → Tm Γ B → Tm Γ C
f $2 a $ b = app (app f) [ id , a , b ]

_$3_$_$_ : ∀ {Γ A B C D} →
  Tm Γ (A ⇒ B ⇒ C ⇒ D) → Tm Γ A → Tm Γ B → Tm Γ C → Tm Γ D
f $3 a $ b $ c = app (app (app f)) [ id , a , b , c ]
\end{code}
\begin{code}
N : Sign 0
N = ◆ ▷ X ▷ X⇒ X

Nat : Ty
Nat = ind N □

zero : ∀ {Γ} → Tm Γ Nat
zero = con' N (fin 1)

suc : ∀ {Γ} → Tm Γ (Nat ⇒ Nat)
suc = con' N (fin 0)

⌜_⌝N : ∀ {Γ} → ℕ → Tm Γ Nat
⌜ O ⌝N = zero
⌜ S n ⌝N = suc $ ⌜ n ⌝N

ωdouble : ∀ {Γ} → Tm Γ ((N ᴬS) □ Nat)
ωdouble = ⟨ ⟨ tt , zero ⟩ , lam (suc $ (suc $ q)) ⟩

double : ∀ {Γ} → Tm Γ (Nat ⇒ Nat)
double = lam (rec N ωdouble q)

ωplus : ∀ {Γ} → Tm Γ ((N ᴬS) □ (Nat ⇒ Nat))
ωplus = ⟨ ⟨ tt , lam q ⟩ , lam (lam (suc $ (q1 $ q))) ⟩

plus : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
plus = lam (rec N ωplus q)

ωtimes : ∀ {Γ} → Tm Γ ((N ᴬS) □ (Nat ⇒ Nat))
ωtimes = ⟨ ⟨ tt , lam zero ⟩ , lam (lam (plus $2 q $ (q1 $ q))) ⟩

times : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
times = lam (rec N ωtimes q)

L : Sign 1
L = ◆ ▷ X ▷ z ⇛ X⇒ X

<_> : Ty → Ty
< A > = ind L (A ◁ □)

nil : ∀ {Γ A} → Tm Γ < A >
nil = con' L (fin 1)

cons : ∀ {Γ A} → Tm Γ (A ⇒ < A > ⇒ < A >)
cons = con' L (fin 0)

⌜_⌝L : ∀ {Γ n} → 𝕍 ℕ n → Tm Γ < Nat >
⌜ □ ⌝L = nil
⌜ x ◁ xs ⌝L = cons $2 ⌜ x ⌝N $ ⌜ xs ⌝L

ωconcat : ∀ {Γ A} → Tm Γ ((L ᴬS) (A ◁ □) (< A > ⇒ < A >))
ωconcat = ⟨ ⟨ tt , lam q ⟩ , lam (lam (lam (cons $2 q2 $ (q1 $ q)))) ⟩

concat : ∀ {Γ A} → Tm Γ (< A > ⇒ < A > ⇒ < A >)
concat {A = A} = lam (rec L ωconcat q)

ωsum : ∀ {Γ} → Tm Γ ((L ᴬS) (Nat ◁ □) Nat)
ωsum = ⟨ ⟨ tt , zero ⟩ , plus ⟩

sum : ∀ {Γ} → Tm Γ (< Nat > ⇒ Nat)
sum = lam (rec L ωsum q)

T : Sign 2
T = ◆ ▷ s z ⇛ X ▷ X⇒ z ⇛ X⇒ X

Tree : Ty → Ty → Ty
Tree A B = ind T (A ◁ B ◁ □)

leaf : ∀ {Γ A B} → Tm Γ (B ⇒ Tree A B)
leaf = con' T (fin 1)

node : ∀ {Γ A B} → Tm Γ (Tree A B ⇒ A ⇒ Tree A B ⇒ Tree A B)
node = con' T (fin 0)

⌜_⌝T : ∀ {Γ} → 𝕋2 ℕ ℕ → Tm Γ (Tree Nat Nat)
⌜ Leaf n ⌝T = leaf $ ⌜ n ⌝N
⌜ Node l n r ⌝T = node $3 ⌜ l ⌝T $ ⌜ n ⌝N $ ⌜ r ⌝T

ωsumLeaves : ∀ {Γ A} → Tm Γ ((T ᴬS) (A ◁ Nat ◁ □) Nat)
ωsumLeaves = ⟨ ⟨ tt , lam q ⟩ , lam (lam (lam (plus $2 q2 $ q))) ⟩

sumLeaves : ∀ {Γ A} → Tm Γ (Tree A Nat ⇒ Nat)
sumLeaves = lam (rec T ωsumLeaves q)

ωflattenLeaves : ∀ {Γ A B} → Tm Γ ((T ᴬS) (A ◁ B ◁ □) < B >)
ωflattenLeaves = ⟨ ⟨ tt , lam (cons $2 q $ nil) ⟩
                 , lam (lam (lam (concat $2 q2 $ q))) ⟩

flattenLeaves : ∀ {Γ A B} → Tm Γ (Tree A B ⇒ < B >)
flattenLeaves = lam (rec T ωflattenLeaves q)
\end{code}
