\chapter{Inductive types}

\begin{tcolorbox}[title=Learning goals of this chapter]
  
\end{tcolorbox}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module Ind4.Algebra where

open import Lib hiding (_∘_ ; _×_ ; _,_)
\end{code}
\begin{code}
module I where
  infixr 6 _⇛_
  infixr 6 X⇒_
  infixl 5 _▷_
  infixl 5 _▻_
  infixr 6 _⇒_
  infixl 8 _×_
  infixl 7 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 7 _↑_
  infixl 6 _[_]
  infixl 5 _[_]I
  infixl 5 _$_
  infixl 5 _$̂_
  infixl 5 _$̇_
  
  data Ty  : Set
  data Con : Set
  data Ar   (n : ℕ) : Set
  data Sign (n : ℕ) : Set

  data Ty where
    Unit  : Ty
    Empty : Ty
    _⇒_   : Ty → Ty → Ty
    _×_   : Ty → Ty → Ty
    _+_   : Ty → Ty → Ty
    ind   : ∀ {n} → Sign n → 𝕍 Ty n → Ty

  data Con where
    ∙   : Con
    _▻_ : Con → Ty → Con

  data Ar n where
    X   : Ar n
    _⇛_ : Fin n → Ar n → Ar n
    X⇒_ : Ar n → Ar n

  data Sign n where
    ◆   : Sign n
    _▷_ : Sign n → Ar n → Sign n
  
  postulate
    Sub : Con → Con → Set
    Tm  : Con → Ty → Set

  _ᴬA : ∀ {n} → Ar n → 𝕍 Ty n → Ty → Ty
  (X       ᴬA) P A = A
  ((k ⇛ R) ᴬA) P A = (P ‼ k) ⇒ (R ᴬA) P A
  ((X⇒ R)  ᴬA) P A = A ⇒ (R ᴬA) P A

  _ᴬS : ∀ {n} → Sign n → 𝕍 Ty n → Ty → Ty
  (◆       ᴬS) P A = Unit
  ((Ω ▷ R) ᴬS) P A = (Ω ᴬS) P A × (R ᴬA) P A

  postulate
    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▻ A)
    p : ∀ {Γ A} → Sub (Γ ▻ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▻ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    
    lam : ∀ {Γ A B} → Tm (Γ ▻ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▻ A) B
    
    tt : ∀ {Γ} → Tm Γ Unit
    ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B

    absurd : ∀ {Γ} A → Tm Γ Empty → Tm Γ A
    inl : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
    case : ∀ {Γ A B C} →
      Tm Γ (A + B) → Tm (Γ ▻ A) C → Tm (Γ ▻ B) C → Tm Γ C

  _↑_ : ∀ {Γ Δ}(σ : Sub Γ Δ) A → Sub (Γ ▻ A) (Δ ▻ A)
  σ ↑ A = σ ∘ p , q
  
  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▻ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  data IVar {n} : Sign n → Ar n → Set where
    vz : ∀ {Ω R} → IVar (Ω ▷ R) R
    vs : ∀ {Ω R S} → IVar Ω R → IVar (Ω ▷ S) R
  
  data ITm {n}(Γ : Con)(P : 𝕍 Ty n)(Ω : Sign n) : Ar n → Set where
    var : ∀ {R} → IVar Ω R → ITm Γ P Ω R
    _$̂_ : ∀ {k R} → ITm Γ P Ω (k ⇛ R) → Tm Γ (P ‼ k) → ITm Γ P Ω R
    _$̇_ : ∀ {R} → ITm Γ P Ω (X⇒ R) → Tm Γ (ind Ω P) → ITm Γ P Ω R

  _[_]I : ∀ {n Γ Δ P Ω R} → ITm {n} Δ P Ω R → Sub Γ Δ → ITm Γ P Ω R
  (var x) [ σ ]I = var x
  (t $̂ u) [ σ ]I = t [ σ ]I $̂ u [ σ ]
  (t $̇ u) [ σ ]I = t [ σ ]I $̇ u [ σ ]
  
  postulate
    con : ∀ {n Γ P Ω} → ITm {n} Γ P Ω X → Tm Γ (ind Ω P)
    rec : ∀ {n Γ P}(Ω : Sign n){A} →
      Tm Γ ((Ω ᴬS) P A) → Tm Γ (ind Ω P) → Tm Γ A

  _ᴬV : ∀ {n Γ P Ω R A} →
    IVar {n} Ω R → Tm Γ ((Ω ᴬS) P A) → Tm Γ ((R ᴬA) P A)
  (vz     ᴬV) ω = proj₂ ω
  ((vs x) ᴬV) ω = (x ᴬV) (proj₁ ω)

  _ᴬT : ∀ {n Γ P Ω R A} →
    ITm {n} Γ P Ω R → Tm Γ ((Ω ᴬS) P A) → Tm Γ ((R ᴬA) P A)
  ((var x) ᴬT) ω = (x ᴬV) ω
  ((t $̂ u) ᴬT) ω = (t ᴬT) ω $ u
  ((t $̇ u) ᴬT) ω = (t ᴬT) ω $ rec _ ω u

  -- helper functions for inductive types

  conᴳ : ∀ {n Γ P} Ω {R} → ITm {n} Γ P Ω R → Tm Γ ((R ᴬA) P (ind Ω P))
  conᴳ Ω {X}     t = con t
  conᴳ Ω {k ⇛ R} t = lam (conᴳ Ω (t [ p ]I $̂ q))
  conᴳ Ω {X⇒ R}  t = lam (conᴳ Ω (t [ p ]I $̇ q))

  ∣_∣ : ∀ {n} → Sign n → ℕ
  ∣ ◆     ∣ = O
  ∣ Ω ▷ R ∣ = S ∣ Ω ∣

  _!_ : ∀ {n}(Ω : Sign n) → Fin ∣ Ω ∣ → Ar n
  (Ω ▷ R) ! z   = R
  (Ω ▷ R) ! s k = Ω ! k

  _⟩_ : ∀ {n}(Ω : Sign n)(k : Fin ∣ Ω ∣) → IVar Ω (Ω ! k)
  (Ω ▷ R) ⟩ z     = vz
  (Ω ▷ R) ⟩ (s k) = vs (Ω ⟩ k)

  con' : ∀ {n Γ P}
    (Ω : Sign n)(k : Fin ∣ Ω ∣) → Tm Γ (((Ω ! k) ᴬA) P (ind Ω P))
  con' Ω k = conᴳ Ω (var (Ω ⟩ k))

  -- equalities

  ᴬAX : ∀ {n P A} → (X {n} ᴬA) P A ≡ A
  ᴬAX = refl

  ᴬA⇛ : ∀ {n P k}{R : Ar n}{A} → ((k ⇛ R) ᴬA) P A ≡ (P ‼ k) ⇒ (R ᴬA) P A
  ᴬA⇛ = refl
  
  ᴬAX⇒ : ∀ {n P}{R : Ar n}{A} → ((X⇒ R) ᴬA) P A ≡ A ⇒ (R ᴬA) P A
  ᴬAX⇒ = refl

  ᴬS◆ : ∀ {n P A} → (◆ {n} ᴬS) P A ≡ Unit
  ᴬS◆ = refl
  
  ᴬS▷ : ∀ {n P Ω}{R : Ar n}{A} →
    ((Ω ▷ R) ᴬS) P A ≡ (Ω ᴬS) P A × (R ᴬA) P A
  ᴬS▷ = refl

  postulate
    ass : ∀ {Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ}{σ : Sub Γ ∙} → σ ≡ ε
    
    ▻β₁ : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▻β₂ : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▻η : ∀ {Γ Δ A}{σ : Sub Γ (Δ ▻ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A}{t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    
    ⇒β : ∀ {Γ A B}{t : Tm (Γ ▻ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[] : ∀ {Γ Δ A B}{t : Tm (Δ ▻ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ↑ A ])

    Unitη : ∀ {Γ}{t : Tm Γ Unit} → t ≡ tt
    tt[] : ∀ {Γ Δ}{σ : Sub Γ Δ} → tt [ σ ] ≡ tt
    
    ×β₁ : ∀ {Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ A B}{t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[] : ∀ {Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ Δ A}{t : Tm Δ Empty}{σ : Sub Γ Δ} →
      (absurd A t) [ σ ] ≡ absurd A (t [ σ ])
    
    +β₁ : ∀ {Γ A B C}{t : Tm Γ A}{u : Tm (Γ ▻ A) C}{v : Tm (Γ ▻ B) C} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ A B C}{t : Tm Γ B}{u : Tm (Γ ▻ A) C}{v : Tm (Γ ▻ B) C} →
      case (inr t) u v ≡ v [ id , t ]
    inl[] : ∀ {Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ Δ A B}{t : Tm Δ B}{σ : Sub Γ Δ} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ Δ A B C}{t : Tm Δ (A + B)}
      {u : Tm (Δ ▻ A) C}{v : Tm (Δ ▻ B) C}{σ : Sub Γ Δ} →
      (case t u v) [ σ ] ≡ case (t [ σ ]) (u [ σ ↑ A ]) (v [ σ ↑ B ])

  [id]I : ∀ {n Γ P Ω R}{t : ITm {n} Γ P Ω R} → t [ id ]I ≡ t
  [id]I {t = var x} = refl
  [id]I {t = t $̂ u} = _$̂_ & [id]I {t = t} ⊛ [id] {t = u}
  [id]I {t = t $̇ u} = _$̇_ & [id]I {t = t} ⊛ [id] {t = u}

  [∘]I : ∀ {n Γ Δ Θ P Ω R}{t : ITm {n} Θ P Ω R}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
    t [ σ ]I [ δ ]I ≡ t [ σ ∘ δ ]I
  [∘]I {t = var x} = refl
  [∘]I {t = t $̂ u} = _$̂_ & [∘]I {t = t} ⊛ [∘] {t = u}
  [∘]I {t = t $̇ u} = _$̇_ & [∘]I {t = t} ⊛ [∘] {t = u}

  var[]I : ∀ {n Γ Δ P Ω R}{x : IVar {n} Ω R}{σ : Sub Γ Δ} →
    (var {P = P} x) [ σ ]I ≡ var x
  var[]I = refl

  $̂[]I : ∀ {n Γ Δ P Ω k R}{t : ITm {n} Δ P Ω (k ⇛ R)}{u : Tm Δ (P ‼ k)}
    {σ : Sub Γ Δ} → (t $̂ u) [ σ ]I ≡ t [ σ ]I $̂ u [ σ ]
  $̂[]I = refl

  $̇[]I : ∀ {n Γ Δ P Ω R}{t : ITm {n} Δ P Ω (X⇒ R)}{u : Tm Δ (ind Ω P)}
    {σ : Sub Γ Δ} → (t $̇ u) [ σ ]I ≡ t [ σ ]I $̇ u [ σ ]
  $̇[]I = refl

  ᴬVvz : ∀ {n Γ P Ω}{R : Ar n}{A}{ω : Tm Γ (((Ω ▷ R) ᴬS) P A)} →
    (vz ᴬV) ω ≡ proj₂ (coe (Tm Γ & ᴬS▷) ω)
  ᴬVvz = refl

  ᴬVvs : ∀ {n Γ P Ω R S A}{x : IVar {n} Ω R}{ω : Tm Γ (((Ω ▷ S) ᴬS) P A)} →
    ((vs x) ᴬV) ω ≡ (x ᴬV) (proj₁ (coe (Tm Γ & ᴬS▷) ω))
  ᴬVvs = refl

  ᴬTvar : ∀ {n Γ P Ω R A}{x : IVar {n} Ω R}{ω : Tm Γ ((Ω ᴬS) P A)} →
    ((var x) ᴬT) ω ≡ (x ᴬV) ω
  ᴬTvar = refl

  ᴬT$̂ : ∀ {n Γ P Ω k R A}{t : ITm {n} Γ P Ω (k ⇛ R)}{u : Tm Γ (P ‼ k)}
    {ω : Tm Γ ((Ω ᴬS) P A)} →
    ((t $̂ u) ᴬT) ω ≡ coe (Tm Γ & ᴬA⇛ {A = A}) ((t ᴬT) ω) $ u
  ᴬT$̂ = refl

  ᴬT$̇ : ∀ {n Γ P Ω R A}{t : ITm {n} Γ P Ω (X⇒ R)}{u : Tm Γ (ind Ω P)}
    {ω : Tm Γ ((Ω ᴬS) P A)} →
    ((t $̇ u) ᴬT) ω ≡ coe (Tm Γ & ᴬAX⇒) ((t ᴬT) ω) $ rec Ω ω u
  ᴬT$̇ = refl

  postulate
    indβ : ∀ {n Γ P Ω A}{ω : Tm Γ ((Ω ᴬS) P A)}{t : ITm {n} Γ P Ω X} →
      rec Ω ω (con t) ≡ coe (Tm Γ & ᴬAX {P = P}) ((t ᴬT) ω)
    con[] : ∀ {n Γ Δ P Ω}{t : ITm {n} Δ P Ω X}{σ : Sub Γ Δ} →
      (con t) [ σ ] ≡ con (t [ σ ]I)
    rec[] : ∀ {n Γ Δ P}{Ω : Sign n}{A}{ω : Tm Δ ((Ω ᴬS) P A)}
      {t : Tm Δ (ind Ω P)}{σ : Sub Γ Δ} →
      (rec Ω ω t) [ σ ] ≡ rec Ω (ω [ σ ]) (t [ σ ])

  ▻η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▻η' = ap2 _,_ idr [id] ⁻¹ ◾ ▻η

  ,∘ : ∀ {Γ Δ Θ A}{σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▻η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▻β₁) (ap (_[ δ ]) ▻β₂)

  app[] : ∀ {Γ Δ A B}{t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  proj₁[] : ∀ {Γ Δ A B}{t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {σ = σ} = ×β₁ ⁻¹
                  ◾ ap proj₁ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀ {Γ Δ A B}{t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {σ = σ} = ×β₂ ⁻¹
                  ◾ ap proj₂ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₂ (u [ σ ])) ×η

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▻β₁ ▻β₂ ▻η ▻η' ,∘ [id] [∘] #-}
  {-# REWRITE ⇒β ⇒η lam[] app[] #-}
  {-# REWRITE tt[] ×β₁ ×β₂ ×η ⟨,⟩[] proj₁[] proj₂[] #-}
  {-# REWRITE absurd[] +β₁ +β₂ inl[] inr[] case[] #-}
  {-# REWRITE [id]I [∘]I indβ con[] rec[] #-}

record Algebra {a b c d e f g h}
       : Set (lsuc (a ⊔ b ⊔ c ⊔ d ⊔ e ⊔ f ⊔ g ⊔ h)) where
  infixr 6 _⇛_
  infixr 6 X⇒_
  infixl 5 _▷_
  infixl 5 _▻_
  infixr 6 _⇒_
  infixl 8 _×_
  infixl 7 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 7 _↑_
  infixl 6 _[_]
  infixl 5 _[_]I
  infixl 5 _$_
  infixl 5 _$̂_
  infixl 5 _$̇_
  
  field
    Ty       : Set a
    Con      : Set b
    Ar       : ℕ → Set c
    Sign     : ℕ → Set d
    Sub      : Con → Con → Set e
    Tm       : Con → Ty → Set f
    IVar     : ∀ {n} → Sign n → Ar n → Set g
    ITm      : ∀ {n} → Con → 𝕍 Ty n → Sign n → Ar n → Set h

    Unit     : Ty
    Empty    : Ty
    _⇒_      : Ty → Ty → Ty
    _×_      : Ty → Ty → Ty
    _+_      : Ty → Ty → Ty
    ind      : ∀ {n} → Sign n → 𝕍 Ty n → Ty

    ∙        : Con
    _▻_      : Con → Ty → Con
    
    X        : ∀ {n} → Ar n
    _⇛_      : ∀ {n} → Fin n → Ar n → Ar n
    X⇒_      : ∀ {n} → Ar n → Ar n
    
    ◆        : ∀ {n} → Sign n
    _▷_      : ∀ {n} → Sign n → Ar n → Sign n

    _ᴬA      : ∀ {n} → Ar n → 𝕍 Ty n → Ty → Ty
    _ᴬS      : ∀ {n} → Sign n → 𝕍 Ty n → Ty → Ty

    _∘_      : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id       : ∀ {Γ} → Sub Γ Γ
    ε        : ∀ {Γ} → Sub Γ ∙
    _,_      : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▻ A)
    p        : ∀ {Γ A} → Sub (Γ ▻ A) Γ
    
    q        : ∀ {Γ A} → Tm (Γ ▻ A) A
    _[_]     : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    _[_]I    : ∀ {n Γ Δ P Ω R} → ITm {n} Δ P Ω R → Sub Γ Δ → ITm Γ P Ω R
    
    lam      : ∀ {Γ A B} → Tm (Γ ▻ A) B → Tm Γ (A ⇒ B)
    app      : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▻ A) B
    
    tt       : ∀ {Γ} → Tm Γ Unit
    ⟨_,_⟩    : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁    : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂    : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B

    absurd   : ∀ {Γ} A → Tm Γ Empty → Tm Γ A
    inl      : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr      : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
    case     : ∀ {Γ A B C} →
      Tm Γ (A + B) → Tm (Γ ▻ A) C → Tm (Γ ▻ B) C → Tm Γ C
    
    vz       : ∀ {n Ω R} → IVar {n} (Ω ▷ R) R
    vs       : ∀ {n Ω R S} → IVar {n} Ω R → IVar (Ω ▷ S) R

    var      : ∀ {n Γ P Ω R} → IVar {n} Ω R → ITm Γ P Ω R
    _$̂_      : ∀ {n Γ P Ω k R} →
      ITm {n} Γ P Ω (k ⇛ R) → Tm Γ (P ‼ k) → ITm Γ P Ω R
    _$̇_      : ∀ {n Γ P Ω R} →
      ITm {n} Γ P Ω (X⇒ R) → Tm Γ (ind Ω P) → ITm Γ P Ω R

    _ᴬV      : ∀ {n Γ P Ω R A} →
      IVar {n} Ω R → Tm Γ ((Ω ᴬS) P A) → Tm Γ ((R ᴬA) P A)
    _ᴬT      : ∀ {n Γ P Ω R A} →
      ITm {n} Γ P Ω R → Tm Γ ((Ω ᴬS) P A) → Tm Γ ((R ᴬA) P A)

    con      : ∀ {n Γ P Ω} → ITm {n} Γ P Ω X → Tm Γ (ind Ω P)
    rec      : ∀ {n Γ P}(Ω : Sign n){A} →
      Tm Γ ((Ω ᴬS) P A) → Tm Γ (ind Ω P) → Tm Γ A

  _↑_ : ∀ {Γ Δ}(σ : Sub Γ Δ) A → Sub (Γ ▻ A) (Δ ▻ A)
  σ ↑ A = σ ∘ p , q

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▻ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  field
    ᴬAX      : ∀ {n P A} → (X {n} ᴬA) P A ≡ A
    ᴬA⇛      : ∀ {n P k}{R : Ar n}{A} →
      ((k ⇛ R) ᴬA) P A ≡ (P ‼ k) ⇒ (R ᴬA) P A
    ᴬAX⇒     : ∀ {n P}{R : Ar n}{A} → ((X⇒ R) ᴬA) P A ≡ A ⇒ (R ᴬA) P A

    ᴬS◆      : ∀ {n P A} → (◆ {n} ᴬS) P A ≡ Unit
    ᴬS▷      : ∀ {n P Ω}{R : Ar n}{A} →
      ((Ω ▷ R) ᴬS) P A ≡ (Ω ᴬS) P A × (R ᴬA) P A

    ass      : ∀ {Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl      : ∀ {Γ Δ}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr      : ∀ {Γ Δ}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η       : ∀ {Γ}{σ : Sub Γ ∙} → σ ≡ ε

    ▻β₁      : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▻β₂      : ∀ {Γ Δ A}{σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▻η       : ∀ {Γ Δ A}{σ : Sub Γ (Δ ▻ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id]     : ∀ {Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    [∘]      : ∀ {Γ Δ Θ A}{t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    [id]I    : ∀ {n Γ P Ω R}{t : ITm {n} Γ P Ω R} → t [ id ]I ≡ t
    [∘]I     : ∀ {n Γ Δ Θ P Ω R}{t : ITm {n} Θ P Ω R}
      {σ : Sub Δ Θ}{δ : Sub Γ Δ} → t [ σ ]I [ δ ]I ≡ t [ σ ∘ δ ]I
    
    ⇒β       : ∀ {Γ A B}{t : Tm (Γ ▻ A) B} → app (lam t) ≡ t
    ⇒η       : ∀ {Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[]    : ∀ {Γ Δ A B}{t : Tm (Δ ▻ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ↑ A ])

    Unitη    : ∀ {Γ}{t : Tm Γ Unit} → t ≡ tt
    tt[]     : ∀ {Γ Δ}{σ : Sub Γ Δ} → tt [ σ ] ≡ tt

    ×β₁      : ∀ {Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂      : ∀ {Γ A B}{u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η       : ∀ {Γ A B}{t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[]    : ∀ {Γ Δ A B}{u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ Δ A}{t : Tm Δ Empty}{σ : Sub Γ Δ} →
      (absurd A t) [ σ ] ≡ absurd A (t [ σ ])

    +β₁      : ∀ {Γ A B C}{t : Tm Γ A}{u : Tm (Γ ▻ A) C}{v : Tm (Γ ▻ B) C} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂      : ∀ {Γ A B C}{t : Tm Γ B}{u : Tm (Γ ▻ A) C}{v : Tm (Γ ▻ B) C} →
      case (inr t) u v ≡ v [ id , t ]
    inl[]    : ∀ {Γ Δ A B}{t : Tm Δ A}{σ : Sub Γ Δ} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[]    : ∀ {Γ Δ A B}{t : Tm Δ B}{σ : Sub Γ Δ} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[]   : ∀ {Γ Δ A B C}{t : Tm Δ (A + B)}
      {u : Tm (Δ ▻ A) C}{v : Tm (Δ ▻ B) C}{σ : Sub Γ Δ} →
      (case t u v) [ σ ] ≡ case (t [ σ ]) (u [ σ ↑ A ]) (v [ σ ↑ B ])

    var[]I   : ∀ {n Γ Δ P Ω R}{x : IVar {n} Ω R}{σ : Sub Γ Δ} →
      (var {P = P} x) [ σ ]I ≡ var x
    $̂[]I     : ∀ {n Γ Δ P Ω k R}{t : ITm {n} Δ P Ω (k ⇛ R)}
      {u : Tm Δ (P ‼ k)}{σ : Sub Γ Δ} → (t $̂ u) [ σ ]I ≡ t [ σ ]I $̂ u [ σ ]
    $[]I     : ∀ {n Γ Δ P Ω R}{t : ITm {n} Δ P Ω (X⇒ R)}{u : Tm Δ (ind Ω P)}
      {σ : Sub Γ Δ} → (t $̇ u) [ σ ]I ≡ t [ σ ]I $̇ u [ σ ]

    ᴬVvz     : ∀ {n Γ P Ω}{R : Ar n}{A}{ω : Tm Γ (((Ω ▷ R) ᴬS) P A)} →
      (vz ᴬV) ω ≡ proj₂ (coe (Tm Γ & ᴬS▷) ω)
    ᴬVvs     : ∀ {n Γ P Ω R S A}{x : IVar {n} Ω R}
      {ω : Tm Γ (((Ω ▷ S) ᴬS) P A)} →
      ((vs x) ᴬV) ω ≡ (x ᴬV) (proj₁ (coe (Tm Γ & ᴬS▷) ω))

    ᴬTvar    : ∀ {n Γ P Ω R A}{x : IVar {n} Ω R}{ω : Tm Γ ((Ω ᴬS) P A)} →
      ((var x) ᴬT) ω ≡ (x ᴬV) ω
    ᴬT$̂      : ∀ {n Γ P Ω k R A}{t : ITm {n} Γ P Ω (k ⇛ R)}
      {u : Tm Γ (P ‼ k)}{ω : Tm Γ ((Ω ᴬS) P A)} →
      ((t $̂ u) ᴬT) ω ≡ coe (Tm Γ & ᴬA⇛) ((t ᴬT) ω) $ u
    ᴬT$̇      : ∀ {n Γ P Ω R A}{t : ITm {n} Γ P Ω (X⇒ R)}
      {u : Tm Γ (ind Ω P)}{ω : Tm Γ ((Ω ᴬS) P A)} →
      ((t $̇ u) ᴬT) ω ≡ coe (Tm Γ & ᴬAX⇒) ((t ᴬT) ω) $ rec Ω ω u

    indβ     : ∀ {n Γ P Ω A}{ω : Tm Γ ((Ω ᴬS) P A)}{t : ITm {n} Γ P Ω X} →
      rec Ω ω (con t) ≡ coe (Tm Γ & ᴬAX) ((t ᴬT) ω)
    con[]    : ∀ {n Γ Δ P Ω}{t : ITm {n} Δ P Ω X}{σ : Sub Γ Δ} →
      (con t) [ σ ] ≡ con (t [ σ ]I)
    rec[]    : ∀ {n Γ Δ P}{Ω : Sign n}{A}{ω : Tm Δ ((Ω ᴬS) P A)}
      {t : Tm Δ (ind Ω P)}{σ : Sub Γ Δ} →
      (rec Ω ω t) [ σ ] ≡ rec Ω (ω [ σ ]) (t [ σ ])

  ▻η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▻η' = ap2 _,_ idr [id] ⁻¹ ◾ ▻η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▻η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▻β₁) (ap (_[ δ ]) ▻β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {σ = σ} = ×β₁ ⁻¹
                  ◾ ap proj₁ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {σ = σ} = ×β₂ ⁻¹
                  ◾ ap proj₂ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₂ (u [ σ ])) ×η

  def⇒ : ∀ {Γ A B} {t : Tm Γ A}{u : Tm (Γ ▻ A) B} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▻β₁)
                    ( ▻β₂
                    ◾ [id] ⁻¹)
                  ◾ ▻η)
                ◾ [id])
              ◾ ⇒η
  
  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▻ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▻β₁
            ◾ idr)
          ▻β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦_⟧C : I.Con → Con
  ⟦_⟧P : ∀ {n} → 𝕍 I.Ty n → 𝕍 Ty n
  ⟦_⟧A : ∀ {n} → I.Ar n → Ar n
  ⟦_⟧S : ∀ {n} → I.Sign n → Sign n

  ⟦ I.Unit ⟧T    = Unit
  ⟦ I.Empty ⟧T   = Empty
  ⟦ A I.⇒ B ⟧T   = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ A I.× B ⟧T   = ⟦ A ⟧T × ⟦ B ⟧T
  ⟦ A I.+ B ⟧T   = ⟦ A ⟧T + ⟦ B ⟧T
  ⟦ I.ind Ω P ⟧T = ind ⟦ Ω ⟧S ⟦ P ⟧P

  ⟦ I.∙ ⟧C     = ∙
  ⟦ Γ I.▻ A ⟧C = ⟦ Γ ⟧C ▻ ⟦ A ⟧T

  ⟦ □ ⟧P     = □
  ⟦ C ◁ P ⟧P = ⟦ C ⟧T ◁ ⟦ P ⟧P
  
  ⟦ I.X ⟧A     = X
  ⟦ k I.⇛ R ⟧A = k ⇛ ⟦ R ⟧A
  ⟦ I.X⇒ R ⟧A  = X⇒ ⟦ R ⟧A
  
  ⟦ I.◆ ⟧S     = ◆
  ⟦ Ω I.▷ R ⟧S = ⟦ Ω ⟧S ▷ ⟦ R ⟧A

  ⟦‼⟧P : ∀ {n P}{k : Fin n} → ⟦ P ‼ k ⟧T ≡ ⟦ P ⟧P ‼ k
  ⟦‼⟧P {P = C ◁ P}{z}   = refl
  ⟦‼⟧P {P = C ◁ P}{s k} = ⟦‼⟧P {P = P}{k}

  postulate
    ⟦_⟧σ : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T

  ⟦_⟧V : ∀ {n Ω R} → I.IVar {n} Ω R → IVar ⟦ Ω ⟧S ⟦ R ⟧A
  ⟦ I.vz   ⟧V = vz
  ⟦ I.vs x ⟧V = vs ⟦ x ⟧V

  ⟦_⟧I : ∀ {n Γ P Ω R} → I.ITm {n} Γ P Ω R → ITm ⟦ Γ ⟧C ⟦ P ⟧P ⟦ Ω ⟧S ⟦ R ⟧A
  ⟦ I.var x ⟧I = var ⟦ x ⟧V
  ⟦_⟧I {P = P} (t I.$̂ u) = ⟦ t ⟧I $̂ coe (Tm _ & ⟦‼⟧P {P = P}) ⟦ u ⟧t
  ⟦ t I.$̇ u ⟧I = ⟦ t ⟧I $̇ ⟦ u ⟧t

  postulate
    ⟦ᴬA⟧     : ∀ {n P}{R : I.Ar n}{A} →
      ⟦ (R I.ᴬA) P A ⟧T ≡ (⟦ R ⟧A ᴬA) ⟦ P ⟧P ⟦ A ⟧T
    ⟦ᴬS⟧     : ∀ {n P}{Ω : I.Sign n}{A} →
      ⟦ (Ω I.ᴬS) P A ⟧T ≡ (⟦ Ω ⟧S ᴬS) ⟦ P ⟧P ⟦ A ⟧T
    {-# REWRITE ⟦ᴬA⟧ ⟦ᴬS⟧ #-}

    ⟦∘⟧      : ∀ {Γ Δ Θ}{σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧σ ≡ ⟦ σ ⟧σ ∘ ⟦ δ ⟧σ
    ⟦id⟧     : ∀ {Γ} → ⟦ I.id {Γ} ⟧σ ≡ id
    ⟦ε⟧      : ∀ {Γ} → ⟦ I.ε {Γ} ⟧σ ≡ ε
    ⟦,⟧      : ∀ {Γ Δ A}{σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧σ ≡ ⟦ σ ⟧σ , ⟦ t ⟧t
    ⟦p⟧      : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧σ ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧      : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧     : ∀ {Γ Δ A}{t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧σ ]
    ⟦[]I⟧    : ∀ {n Γ Δ P Ω R}{t : I.ITm {n} Δ P Ω R}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ]I ⟧I ≡ ⟦ t ⟧I [ ⟦ σ ⟧σ ]I
    {-# REWRITE ⟦q⟧ ⟦[]⟧ ⟦[]I⟧ #-}
    
    ⟦lam⟧    : ∀ {Γ A B}{t : I.Tm (Γ I.▻ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧    : ∀ {Γ A B}{t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦tt⟧     : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    ⟦⟨,⟩⟧    : ∀ {Γ A B}{u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧  : ∀ {Γ A B}{t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧  : ∀ {Γ A B}{t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦tt⟧ ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}

    ⟦absurd⟧ : ∀ {Γ A}{t : I.Tm Γ I.Empty} →
      ⟦ I.absurd A t ⟧t ≡ absurd ⟦ A ⟧T ⟦ t ⟧t
    ⟦inl⟧    : ∀ {Γ A B}{t : I.Tm Γ A} →
      ⟦ I.inl {B = B} t ⟧t ≡ inl ⟦ t ⟧t
    ⟦inr⟧    : ∀ {Γ A B}{t : I.Tm Γ B} →
      ⟦ I.inr {A = A} t ⟧t ≡ inr ⟦ t ⟧t
    ⟦case⟧   : ∀ {Γ A B C}{t : I.Tm Γ (A I.+ B)}
      {u : I.Tm (Γ I.▻ A) C}{v : I.Tm (Γ I.▻ B) C} →
      ⟦ I.case t u v ⟧t ≡ case ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦absurd⟧ ⟦inl⟧ ⟦inr⟧ ⟦case⟧ #-}

    ⟦ᴬV⟧     : ∀ {n Γ P Ω R A}{x : I.IVar {n} Ω R}
      {ω : I.Tm Γ ((Ω I.ᴬS) P A)} →
      ⟦ (x I.ᴬV) ω ⟧t ≡ (⟦ x ⟧V ᴬV) ⟦ ω ⟧t
    ⟦ᴬT⟧     : ∀ {n Γ P Ω R A}{t : I.ITm {n} Γ P Ω R}
      {ω : I.Tm Γ ((Ω I.ᴬS) P A)} →
      ⟦ (t I.ᴬT) ω ⟧t ≡ (⟦ t ⟧I ᴬT) ⟦ ω ⟧t
    {-# REWRITE ⟦ᴬV⟧ ⟦ᴬT⟧ #-}

    ⟦con⟧    : ∀ {n Γ P Ω}{t : I.ITm {n} Γ P Ω I.X} →
      ⟦ I.con t ⟧t ≡ con ⟦ t ⟧I
    ⟦rec⟧    : ∀ {n Γ P}{Ω : I.Sign n}{A}{ω : I.Tm Γ ((Ω I.ᴬS) P A)}
      {t : I.Tm Γ (I.ind Ω P)} →
      ⟦ I.rec Ω ω t ⟧t ≡ rec ⟦ Ω ⟧S ⟦ ω ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦con⟧ ⟦rec⟧ #-}
  
  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▻ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B}{t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
\end{code}
