\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module MLTT.PostSyntax  where

open import Lib hiding (_∘_; Σ; _,_)
                renaming (refl to reflₘ)
\end{code}
\begin{code}
infixl 5 _▻_
infixl 6 _+_
infixr 5 _⇒_
infixl 6 _∘_
infixl 5 _,_
infixl 7 _↑_
infixl 8 _[_]T
infixl 8 _[_]t
infixl 5 _$_

postulate
  Con      : Level → Set
  Ty       : {i : Level} → Level → Con i → Set
  Sub      : {i j : Level} → Con i → Con j → Set
  Tm       : {i j : Level} (Γ : Con i) → Ty j Γ → Set

  id       : ∀ {i}{Γ : Con i} → Sub Γ Γ
  _∘_      : ∀ {i j k}{Γ : Con i}{Δ : Con j}{Θ : Con k} →
    Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
  ass      : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{Λ : Con l}
    {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  idl      : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
  idr      : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
  {-# REWRITE ass idl idr #-}

  _[_]T    : ∀ {i j k}{Γ : Con i}{Δ : Con j} → Ty k Δ → Sub Γ Δ → Ty k Γ
  _[_]t    : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ} →
    Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
  [id]T    : ∀ {i j}{Γ : Con i}{A : Ty j Γ} → A [ id ]T ≡ A
  [∘]T     : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : Ty l Θ}
    {σ : Sub Δ Θ}{δ : Sub Γ Δ} → A [ σ ]T [ δ ]T ≡ A [ σ ∘ δ ]T
  {-# REWRITE [id]T [∘]T #-}
  [id]t    : ∀ {i j}{Γ : Con i}{A : Ty j Γ}{t : Tm Γ A} → t [ id ]t ≡ t
  [∘]t     : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : Ty l Θ}
    {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} → t [ σ ]t [ δ ]t ≡ t [ σ ∘ δ ]t
  {-# REWRITE [id]t [∘]t #-}

  ∙        : Con lzero
  ε        : ∀ {i}{Γ : Con i} → Sub Γ ∙
  ∙η       : ∀ {i}{Γ : Con i}{σ : Sub Γ ∙} → σ ≡ ε

  _▻_      : ∀ {i j}(Γ : Con i) → Ty j Γ → Con (i ⊔ j)
  _,_      : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}
    (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▻ A)
  p        : ∀ {i j}{Γ : Con i}{A : Ty j Γ} → Sub (Γ ▻ A) Γ
  q        : ∀ {i j}{Γ : Con i}{A : Ty j Γ} → Tm (Γ ▻ A) (A [ p ]T)
  ▻β₁      : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}
    {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → p {Γ = Δ}{A} ∘ (σ , t) ≡ σ
  {-# REWRITE ▻β₁ #-}
  ▻β₂      : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}
    {σ : Sub Γ Δ}{t : Tm Γ (A [ σ ]T)} → q {Γ = Δ}{A} [ σ , t ]t ≡ t
  ▻η       : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}
    {σ : Sub Γ (Δ ▻ A)} → p ∘ σ , q [ σ ]t ≡ σ
  ,∘       : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : Ty l Θ}
    {σ : Sub Δ Θ}{t : Tm Δ (A [ σ ]T)}{δ : Sub Γ Δ} →
    (_,_ {A = A} σ t) ∘ δ ≡ σ ∘ δ , t [ δ ]t

_↑_ : ∀ {i j k}{Γ : Con i}{Δ : Con j}(σ : Sub Γ Δ)(A : Ty k Δ) →
  Sub (Γ ▻ A [ σ ]T) (Δ ▻ A)
σ ↑ A = σ ∘ p , q

postulate
  ↑∘       : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{Θ : Con k}{A : Ty l Θ}
    {σ : Sub Δ Θ}{δ : Sub Γ Δ}{t : Tm Γ ( A [ σ ]T [ δ ]T)} →
    (σ ↑ A) ∘ (δ , t) ≡ σ ∘ δ , t
  {-# REWRITE ▻β₂ ▻η ,∘ ↑∘ #-}

  Π        : ∀ {i j k}{Γ : Con i}(A : Ty j Γ) → Ty k (Γ ▻ A) → Ty (j ⊔ k) Γ
  lam      : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)} →
    Tm (Γ ▻ A) B → Tm Γ (Π A B)
  app      : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)} →
    Tm Γ (Π A B) → Tm (Γ ▻ A) B
  Πβ       : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
    {t : Tm (Γ ▻ A) B} → app (lam t) ≡ t
  Πη       : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
    {t : Tm Γ (Π A B)} → lam (app t) ≡ t
  Π[]      : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
    {σ : Sub Γ Δ} → (Π A B) [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ↑ A ]T)
  {-# REWRITE Πβ Πη Π[] #-}
  lam[]    : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
    {t : Tm (Δ ▻ A) B}{σ : Sub Γ Δ} → (lam t) [ σ ]t ≡ lam (t [ σ ↑ A ]t)

_⇒_ : ∀ {i j k}{Γ : Con i} → Ty j Γ → Ty k Γ → Ty (j ⊔ k) Γ
A ⇒ B = Π A (B [ p ]T)

_$_ : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)} →
  Tm Γ (Π A B) → (u : Tm Γ A) → Tm Γ (B [ id , u ]T)
t $ u = (app t) [ id , u ]t

⇒[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
  {σ : Sub Γ Δ} → (A ⇒ B) [ σ ]T ≡ A [ σ ]T ⇒ B [ σ ]T
⇒[] = reflₘ

postulate
  app[]    : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
    {t : Tm Δ (Π A B)}{σ : Sub Γ Δ} → app (t [ σ ]t) ≡ (app t) [ σ ↑ A ]t
  {-# REWRITE lam[] app[] #-}

$[] : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
  {t : Tm Δ (Π A B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
  (t $ u) [ σ ]t ≡ t [ σ ]t $ u [ σ ]t
$[] = reflₘ

postulate
  Σ        : ∀ {i j k}{Γ : Con i}(A : Ty j Γ) → Ty k (Γ ▻ A) → Ty (j ⊔ k) Γ
  ⟨_,_⟩    : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
    (u : Tm Γ A) → Tm Γ (B [ id , u ]T) → Tm Γ (Σ A B)
  proj₁    : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)} →
    Tm Γ (Σ A B) → Tm Γ A
  proj₂    : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
    (t : Tm Γ (Σ A B)) → Tm Γ (B [ id , proj₁ t ]T)
  Σβ₁      : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
    {u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} → proj₁ {B = B} ⟨ u , v ⟩ ≡ u
  {-# REWRITE Σβ₁ #-}
  Σβ₂      : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
    {u : Tm Γ A}{v : Tm Γ (B [ id , u ]T)} → proj₂ {B = B} ⟨ u , v ⟩ ≡ v
  Ση       : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k (Γ ▻ A)}
    {t : Tm Γ (Σ A B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
  Σ[]      : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
    {σ : Sub Γ Δ} → (Σ A B) [ σ ]T ≡ Σ (A [ σ ]T) (B [ σ ↑ A ]T)
  {-# REWRITE Σβ₂ Ση Σ[] #-}
  ⟨,⟩[]    : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
    {u : Tm Δ A}{v : Tm Δ (B [ id , u ]T)}{σ : Sub Γ Δ} →
    (⟨_,_⟩ {B = B} u v) [ σ ]t ≡ ⟨ u [ σ ]t , v [ σ ]t ⟩
  proj₁[]  : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
    {t : Tm Δ (Σ A B)}{σ : Sub Γ Δ} → (proj₁ t) [ σ ]t ≡ proj₁ (t [ σ ]t)
  {-# REWRITE ⟨,⟩[] proj₁[] #-}
  proj₂[]  : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l (Δ ▻ A)}
    {t : Tm Δ (Σ A B)}{σ : Sub Γ Δ} → (proj₂ t) [ σ ]t ≡ proj₂ (t [ σ ]t)
  {-# REWRITE proj₂[] #-}

  ⊤        : ∀ {i}{Γ : Con i} → Ty lzero Γ
  tt       : ∀ {i}{Γ : Con i} → Tm Γ ⊤
  ⊤η       : ∀ {i}{Γ : Con i}{t : Tm Γ ⊤} → t ≡ tt
  ⊤[]      : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → ⊤ [ σ ]T ≡ ⊤
  {-# REWRITE ⊤[] #-}
  tt[]     : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → tt [ σ ]t ≡ tt
  {-# REWRITE tt[] #-}

  _+_      : ∀ {i j k}{Γ : Con i} → Ty j Γ → Ty k Γ → Ty (j ⊔ k) Γ
  inj₁     : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ} →
    Tm Γ A → Tm Γ (A + B)
  inj₂     : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ} →
    Tm Γ B → Tm Γ (A + B)
  +[]      : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
    {σ : Sub Γ Δ} → (A + B) [ σ ]T ≡ A [ σ ]T + B [ σ ]T
  {-# REWRITE +[] #-}
  inj₁[]   : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
    {t : Tm Δ A}{σ : Sub Γ Δ} → (inj₁ {B = B} t) [ σ ]t ≡ inj₁ (t [ σ ]t)
  inj₂[]   : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
    {t : Tm Δ B}{σ : Sub Γ Δ} → (inj₂ {A = A} t) [ σ ]t ≡ inj₂ (t [ σ ]t)
  {-# REWRITE inj₁[] inj₂[] #-}
  ind+     : ∀ {i j k l}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ}
    (P : Ty l (Γ ▻ A + B)) →
    Tm (Γ ▻ A) (P [ p , inj₁ q ]T) → Tm (Γ ▻ B) (P [ p , inj₂ q ]T) →
    (t : Tm Γ (A + B)) → Tm Γ (P [ id , t ]T)
  ind+[]   : ∀ {i j k l m}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{B : Ty l Δ}
    {P : Ty m (Δ ▻ A + B)}
    {u : Tm (Δ ▻ A) (P [ p , inj₁ q ]T)}
    {v : Tm (Δ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Δ (A + B)}{σ : Sub Γ Δ} →
    (ind+ P u v t) [ σ ]t ≡
      {!ind+ (P [ σ ↑ (A + B) ]T) (u [ σ ↑ A ]t) (v [ σ ↑ B ]t) (t [ σ ]t)!}
  +β₁      : ∀ {i j k l}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ}
    {P : Ty l (Γ ▻ A + B)}
    {u : Tm (Γ ▻ A) (P [ p , inj₁ q ]T)}
    {v : Tm (Γ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Γ A} →
    ind+ P u v (inj₁ t) ≡ u [ id , t ]t
  +β₂      : ∀ {i j k l}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ}
    {P : Ty l (Γ ▻ A + B)}
    {u : Tm (Γ ▻ A) (P [ p , inj₁ q ]T)}
    {v : Tm (Γ ▻ B) (P [ p , inj₂ q ]T)}{t : Tm Γ B} →
    ind+ P u v (inj₂ t) ≡ v [ id , t ]t
  {-# REWRITE +β₁ +β₂ #-}

case : ∀ {i j k l}{Γ : Con i}{A : Ty j Γ}{B : Ty k Γ}{C : Ty l Γ} →
  Tm Γ (A + B) → Tm (Γ ▻ A) (C [ p ]T) → Tm (Γ ▻ B) (C [ p ]T) → Tm Γ C
case {C = C} t u v = ind+ (C [ p ]T) u v t

postulate
  ⊥        : ∀ {i}{Γ : Con i} → Ty lzero Γ
  absurd   : ∀ {i j}{Γ : Con i}(A : Ty j Γ) → Tm Γ ⊥ → Tm Γ A
  ⊥[]      : ∀ {i j}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} → ⊥ [ σ ]T ≡ ⊥
  {-# REWRITE ⊥[] #-}
  absurd[] : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{t : Tm Δ ⊥}
    {σ : Sub Γ Δ} → (absurd A t) [ σ ]t ≡ absurd (A [ σ ]T) (t [ σ ]t)
  {-# REWRITE absurd[] #-}

  U        : ∀ {i}{Γ : Con i}(j : Level) → Ty (lsuc j) Γ
  c        : ∀ {i j}{Γ : Con i} → Ty j Γ → Tm Γ (U j)
  El       : ∀ {i j}{Γ : Con i} → Tm Γ (U j) → Ty j Γ
  Uβ       : ∀ {i j}{Γ : Con i}{A : Ty j Γ} → El (c A) ≡ A
  Uη       : ∀ {i j}{Γ : Con i}{a : Tm Γ (U j)} → c (El a) ≡ a
  U[]      : ∀ {i j k}{Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ} →
    (U k) [ σ ]T ≡ U k
  {-# REWRITE Uβ Uη U[] #-}
  c[]      : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{σ : Sub Γ Δ} →
    (c A) [ σ ]t ≡ c (A [ σ ]T)
  El[]     : ∀ {i j k}{Γ : Con i}{Δ : Con j}{a : Tm Δ (U k)}{σ : Sub Γ Δ} →
    (El a) [ σ ]T ≡ El (a [ σ ]t)
  {-# REWRITE c[] El[] #-}

  Id       : ∀ {i j}{Γ : Con i}(A : Ty j Γ) → Tm Γ A → Tm Γ A → Ty j Γ
  refl     : ∀ {i j}{Γ : Con i}{A : Ty j Γ}(u : Tm Γ A) → Tm Γ (Id A u u)
  Id[]     : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}
    {u : Tm Δ A}{v : Tm Δ A}{σ : Sub Γ Δ} →
    (Id A u v) [ σ ]T ≡ Id (A [ σ ]T) (u [ σ ]t) (v [ σ ]t)
  {-# REWRITE Id[] #-}
  refl[]   : ∀ {i j k}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{u : Tm Δ A}
    {σ : Sub Γ Δ} → (refl u) [ σ ]t ≡ refl (u [ σ ]t)
  {-# REWRITE refl[] #-}
  J        : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{u : Tm Γ A}
    (P : Ty k (Γ ▻ A ▻ Id (A [ p ]T) (u [ p ]t) q)) →
    Tm Γ (P [ id , u , refl u ]T) →
    {v : Tm Γ A}(e : Tm Γ (Id A u v)) → Tm Γ (P [ id , v , e ]T)
  J[]      : ∀ {i j k l}{Γ : Con i}{Δ : Con j}{A : Ty k Δ}{u : Tm Δ A}
    {P : Ty l (Δ ▻ A ▻ Id (A [ p ]T) (u [ p ]t) q)}
    {w : Tm Δ (P [ id , u , refl u ]T)}{v : Tm Δ A}{e : Tm Δ (Id A u v)}
    {σ : Sub Γ Δ} →
    (J P w e) [ σ ]t ≡ {!J (P [ σ ↑ A ↑ _ ]T) (w [ σ ]t) (e [ σ ]t)!}
  Jβ       : ∀ {i j k}{Γ : Con i}{A : Ty j Γ}{u : Tm Γ A}
    {P : Ty k (Γ ▻ A ▻ Id (A [ p ]T) (u [ p ]t) q)}
    {w : Tm Γ (P [ id , u , refl u ]T)} → J P w (refl u) ≡ w
  {-# REWRITE Jβ #-}
\end{code}
