\chapter{Inductive types}

\begin{tcolorbox}[title=Learning goals of this chapter]
  
\end{tcolorbox}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module Ind2.Algebra where

open import Lib hiding (_∘_ ; _×_ ; _,_)
\end{code}
\begin{code}
module I where
  infixr 6 X⇒_
  infixr 6 C⇒_
  infixl 5 _▷_
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 7 _×_
  infixl 6 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  data Ty : Set
  data Con : Set
  data Ar (C : Ty) : Set
  data Sign (C : Ty) : Set

  data Ty where
    Unit : Ty
    Empty : Ty
    _⇒_ : Ty → Ty → Ty
    _×_ : Ty → Ty → Ty
    _+_ : Ty → Ty → Ty
    ind : ∀ {C} → Sign C → Ty

  data Con where
    ∙ : Con
    _▹_ : Con → Ty → Con

  data Ar C where
    X : Ar C
    X⇒_ : Ar C → Ar C
    C⇒_ : Ar C → Ar C

  data Sign C where
    ◆ : Sign C
    _▷_ : Sign C → Ar C → Sign C
  
  postulate
    Sub : Con → Con → Set
    Tm : Con → Ty → Set

  _ᴬA : ∀ {C} → Ar C → Ty → Ty
  (X      ᴬA) A = A
  ((X⇒ R) ᴬA) A = A ⇒ (R ᴬA) A
  _ᴬA {C} (C⇒ R) A = C ⇒ (R ᴬA) A

  _ᴬ : ∀ {C} → Sign C → Ty → Ty
  (◆       ᴬ) A = Unit
  ((Ω ▷ R) ᴬ) A = (Ω ᴬ) A × (R ᴬA) A

  postulate
    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A

    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    
    tt : ∀ {Γ} → Tm Γ Unit
    ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B

    absurd : ∀ {Γ A} → Tm Γ Empty → Tm Γ A
    inl : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
    case : ∀ {Γ A B C} →
      Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C

    con : ∀ {Γ C} (Ω : Sign C) → Tm Γ ((Ω ᴬ) (ind Ω))
    rec : ∀ {Γ A C} (Ω : Sign C) → Tm Γ ((Ω ᴬ) A) → Tm Γ (ind Ω ⇒ A)

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  _ᴹA : ∀ {Γ A B C} (R : Ar C) →
    Tm Γ (A ⇒ B) → Tm Γ ((R ᴬA) A) → Tm Γ ((R ᴬA) B) → Prop
  (X      ᴹA) F t u = F $ t ≡ u
  ((X⇒ R) ᴹA) F f g = ∀ t → (R ᴹA) F (f $ t) (g $ (F $ t))
  ((C⇒ R) ᴹA) F f g = ∀ t → (R ᴹA) F (f $ t) (g $ t)

  _ᴹ : ∀ {Γ A B C} (Ω : Sign C) →
    Tm Γ (A ⇒ B) → Tm Γ ((Ω ᴬ) A) → Tm Γ ((Ω ᴬ) B) → Prop
  (◆       ᴹ) F γ δ = 𝟙
  ((Ω ▷ R) ᴹ) F γ δ =
    (Ω ᴹ) F (proj₁ γ) (proj₁ δ) ×p (R ᴹA) F (proj₂ γ) (proj₂ δ)

  postulate
    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε
    
    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t

    uη : ∀ {Γ} {t : Tm Γ Unit} → t ≡ tt
    ×β₁ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ A B} {t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t

    +β₁ : ∀ {Γ A B C} {t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ A B C} {t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inr t) u v ≡ v [ id , t ]

    indβ : ∀ {Γ A C} {Ω : Sign C}{ω : Tm Γ ((Ω ᴬ) A)} →
      (Ω ᴹ) (rec Ω ω) (con Ω) ω

    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    tt[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → tt [ σ ] ≡ tt
    ⟨,⟩[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ Δ A} {t : Tm Δ Empty}{σ : Sub Γ Δ} →
      (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])
    inl[] : ∀ {Γ Δ A B} {t : Tm Δ A}{σ : Sub Γ Δ} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ Δ A B} {t : Tm Δ B}{σ : Sub Γ Δ} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ Δ A B C} {t : Tm Δ (A + B)}
      {u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
      (case t u v) [ σ ] ≡
        case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

    con[] : ∀ {Γ Δ C} {Ω : Sign C}{σ : Sub Γ Δ} → (con Ω) [ σ ] ≡ con Ω
    rec[] : ∀ {Γ Δ A C} {Ω : Sign C}{u : Tm Δ ((Ω ᴬ) A)}{σ : Sub Γ Δ} →
      (rec Ω u) [ σ ] ≡ rec Ω (u [ σ ])

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {σ = σ} = ×β₁ ⁻¹
                  ◾ ap proj₁ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {σ = σ} = ×β₂ ⁻¹
                  ◾ ap proj₂ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₂ (u [ σ ])) ×η

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE ⇒β ⇒η #-}
  {-# REWRITE ×β₁ ×β₂ ×η #-}
  {-# REWRITE +β₁ +β₂ #-}
  {-# REWRITE lam[] app[] #-}
  {-# REWRITE tt[] ⟨,⟩[] proj₁[] proj₂[] #-}
  {-# REWRITE absurd[] inl[] inr[] case[] #-}
  {-# REWRITE con[] rec[] #-}

record Algebra {i j k l m n} : Set (lsuc (i ⊔ j ⊔ k ⊔ l ⊔ m ⊔ n)) where
  infixr 6 X⇒_
  infixr 6 C⇒_
  infixl 5 _▷_
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 7 _×_
  infixl 6 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Ty : Set i
    Con : Set j
    Ar : Ty → Set k
    Sign : Ty → Set l
    Sub : Con → Con → Set m
    Tm : Con → Ty → Set n

    Unit : Ty
    Empty : Ty
    _⇒_ : Ty → Ty → Ty
    _×_ : Ty → Ty → Ty
    _+_ : Ty → Ty → Ty
    ind : ∀ {C} → Sign C → Ty

    ∙ : Con
    _▹_ : Con → Ty → Con
    
    X : ∀ {C} → Ar C              -- returner
    X⇒_ : ∀ {C} → Ar C → Ar C     -- a carrier argument
    C⇒_ : ∀ {C} → Ar C → Ar C     -- a C parameter -- I would have used _⇒_ : Ty → Ar → Ar instead
    -- e.g. C⇒ X⇒ X is the arity of cons for lists of type C
    
    ◆ : ∀ {C} → Sign C
    _▷_ : ∀ {C} → Sign C → Ar C → Sign C

  private
    M : ∀ {Γ A B} → Ty → Ty → Set (lsuc n)
    M {Γ}{A}{B} Aᴬ Bᴬ = Tm Γ (A ⇒ B) → Tm Γ Aᴬ → Tm Γ Bᴬ → Prop n

  field
    _ᴬA : ∀ {C} → Ar C → Ty → Ty     -- (R ᴬA) X is the type of R-operator with carrier X
    _ᴬ : ∀ {C} → Sign C → Ty → Ty    -- (Ω ᴬ) X is the type of Ω-algebras with carrier X

    _ᴹA : ∀ {Γ A B C} (R : Ar C) → M {Γ}{A}{B} ((R ᴬA) A) ((R ᴬA) B)
    _ᴹ : ∀ {Γ A B C} (Ω : Sign C) → M {Γ}{A}{B} ((Ω ᴬ) A) ((Ω ᴬ) B)

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ
    
    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    
    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    
    tt : ∀ {Γ} → Tm Γ Unit
    ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B

    absurd : ∀ {Γ A} → Tm Γ Empty → Tm Γ A
    inl : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
    case : ∀ {Γ A B C} →
      Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C 

    con : ∀ {Γ C} (Ω : Sign C) → Tm Γ ((Ω ᴬ) (ind Ω))
    rec : ∀ {Γ A C} (Ω : Sign C) → Tm Γ ((Ω ᴬ) A) → Tm Γ (ind Ω ⇒ A)

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  field
    ᴬAX : ∀ {A C} → (X {C} ᴬA) A ≡ A
    ᴬAX⇒ : ∀ {A C} {R : Ar C} → ((X⇒ R) ᴬA) A ≡ A ⇒ (R ᴬA) A
    ᴬAC⇒ : ∀ {A C} {R : Ar C} → ((C⇒ R) ᴬA) A ≡ C ⇒ (R ᴬA) A

    ᴬ◆ : ∀ {A C} → (◆ {C} ᴬ) A ≡ Unit
    ᴬ▷ : ∀ {A C} {Ω : Sign C}{R : Ar C} → ((Ω ▷ R) ᴬ) A ≡ (Ω ᴬ) A × (R ᴬA) A

    ᴹAX : ∀ {Γ A B C} {F : Tm Γ (A ⇒ B)}{t : Tm Γ A}{u : Tm Γ B} →
      coe (ap2 M ᴬAX ᴬAX) (X {C} ᴹA) F t u ≡ (F $ t ≡ u)
    ᴹAX⇒ : ∀ {Γ A B C} {R : Ar C}{F : Tm Γ (A ⇒ B)}
      {f : Tm Γ (A ⇒ (R ᴬA) A)}{g : Tm Γ (B ⇒ (R ᴬA) B)} →
      coe (ap2 M ᴬAX⇒ ᴬAX⇒) ((X⇒ R) ᴹA) F f g ≡
        (∀ t → (R ᴹA) F (f $ t) (g $ (F $ t)))
    ᴹAC⇒ : ∀ {Γ A B C} {R : Ar C}{F : Tm Γ (A ⇒ B)}
      {f : Tm Γ (C ⇒ (R ᴬA) A)}{g : Tm Γ (C ⇒ (R ᴬA) B)} →
      coe (ap2 M ᴬAC⇒ ᴬAC⇒) ((C⇒ R) ᴹA) F f g ≡
        (∀ t → (R ᴹA) F (f $ t) (g $ t))

    ᴹ◆ : ∀ {Γ A B C} {F : Tm Γ (A ⇒ B)}{t u : Tm Γ Unit} →
      coe (ap2 M ᴬ◆ ᴬ◆) (◆ {C} ᴹ) F t u ≡ 𝟙'
    ᴹ▷ : ∀ {Γ A B C} {Ω : Sign C}{R : Ar C}{F : Tm Γ (A ⇒ B)}
      {t : Tm Γ ((Ω ᴬ) A × (R ᴬA) A)}{u : Tm Γ ((Ω ᴬ) B × (R ᴬA) B)} →
      coe (ap2 M ᴬ▷ ᴬ▷) ((Ω ▷ R) ᴹ) F t u ≡
        ((Ω ᴹ) F (proj₁ t) (proj₁ u) ×p (R ᴹA) F (proj₂ t) (proj₂ u))
    
    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    
    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t

    uη : ∀ {Γ} {t : Tm Γ Unit} → t ≡ tt
    ×β₁ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ A B} {t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t

    +β₁ : ∀ {Γ A B C} {t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ A B C} {t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inr t) u v ≡ v [ id , t ]

    indβ : ∀ {Γ A C} {Ω : Sign C}{ω : Tm Γ ((Ω ᴬ) A)} →
      (Ω ᴹ) (rec Ω ω) (con Ω) ω

    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    tt[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → tt [ σ ] ≡ tt
    ⟨,⟩[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ Δ A} {t : Tm Δ Empty}{σ : Sub Γ Δ} →
      (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])
    inl[] : ∀ {Γ Δ A B} {t : Tm Δ A}{σ : Sub Γ Δ} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ Δ A B} {t : Tm Δ B}{σ : Sub Γ Δ} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ Δ A B C} {t : Tm Δ (A + B)}
      {u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
      (case t u v) [ σ ] ≡
            case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

    con[] : ∀ {Γ Δ C} {Ω : Sign C}{σ : Sub Γ Δ} → (con Ω) [ σ ] ≡ con Ω
    rec[] : ∀ {Γ Δ A C} {Ω : Sign C}{u : Tm Δ ((Ω ᴬ) A)}{σ : Sub Γ Δ} →
      (rec Ω u) [ σ ] ≡ rec Ω (u [ σ ])

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {σ = σ} = ×β₁ ⁻¹
                  ◾ ap proj₁ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {σ = σ} = ×β₂ ⁻¹
                  ◾ ap proj₂ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₂ (u [ σ ])) ×η

  def⇒ : ∀ {Γ A B} {t : Tm Γ A}{u : Tm (Γ ▹ A) B} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η
  
  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦_⟧C : I.Con → Con
  ⟦_⟧A : ∀ {C} → I.Ar C → Ar ⟦ C ⟧T
  ⟦_⟧S : ∀ {C} → I.Sign C → Sign ⟦ C ⟧T

  ⟦ I.Unit ⟧T = Unit
  ⟦ I.Empty ⟧T = Empty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T
  ⟦ A I.+ B ⟧T = ⟦ A ⟧T + ⟦ B ⟧T
  ⟦ I.ind Ω ⟧T = ind ⟦ Ω ⟧S

  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T
  
  ⟦ I.X ⟧A = X
  ⟦ I.X⇒ R ⟧A = X⇒ ⟦ R ⟧A
  ⟦ I.C⇒ R ⟧A = C⇒ ⟦ R ⟧A
  
  ⟦ I.◆ ⟧S = ◆
  ⟦ Ω I.▷ R ⟧S = ⟦ Ω ⟧S ▷ ⟦ R ⟧A

  postulate
    ⟦_⟧σ : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T

    ⟦ᴬA⟧ : ∀ {A C} {R : I.Ar C} → ⟦ (R I.ᴬA) A ⟧T ≡ (⟦ R ⟧A ᴬA) ⟦ A ⟧T
    ⟦ᴬ⟧ : ∀ {A C} {Ω : I.Sign C} → ⟦ (Ω I.ᴬ) A ⟧T ≡ (⟦ Ω ⟧S ᴬ) ⟦ A ⟧T
    {-# REWRITE ⟦ᴬA⟧ ⟦ᴬ⟧ #-}

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧σ ≡ ⟦ σ ⟧σ ∘ ⟦ δ ⟧σ
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧σ ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧σ ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧σ ≡ ⟦ σ ⟧σ , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧σ ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧σ ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦tt⟧ ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}

    ⟦absurd⟧ : ∀ {Γ A} {t : I.Tm Γ I.Empty} →
      ⟦ I.absurd {A = A} t ⟧t ≡ absurd ⟦ t ⟧t
    ⟦inl⟧ : ∀ {Γ A B} {t : I.Tm Γ A} →
      ⟦ I.inl {B = B} t ⟧t ≡ inl ⟦ t ⟧t
    ⟦inr⟧ : ∀ {Γ A B} {t : I.Tm Γ B} →
      ⟦ I.inr {A = A} t ⟧t ≡ inr ⟦ t ⟧t
    ⟦case⟧ : ∀ {Γ A B C} {t : I.Tm Γ (A I.+ B)}
      {u : I.Tm (Γ I.▹ A) C}{v : I.Tm (Γ I.▹ B) C} →
      ⟦ I.case t u v ⟧t ≡ case ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦absurd⟧ ⟦inl⟧ ⟦inr⟧ ⟦case⟧ #-}

    ⟦con⟧ : ∀ {Γ C} {Ω : I.Sign C} → ⟦ I.con {Γ} Ω ⟧t ≡ con ⟦ Ω ⟧S
    ⟦rec⟧ : ∀ {Γ A C} {Ω : I.Sign C}
      {u : I.Tm Γ ((Ω I.ᴬ) A)} →
      ⟦ I.rec Ω u ⟧t ≡ rec ⟦ Ω ⟧S ⟦ u ⟧t
    {-# REWRITE ⟦con⟧ ⟦rec⟧ #-}
  
  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
{-
record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 7 _×_
  infixl 6 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Con : I.Con → Set i
    Ty : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    ∙ : Con I.∙
    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')

    Unit : Ty I.Unit
    Empty : Ty I.Empty
    _⇒_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.⇒ B')
    _×_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.× B')
    _+_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.+ B')

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p

    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    
    lam : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm (Γ ▹ A) B t' → Tm Γ (A ⇒ B) (I.lam t')
    app : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A ⇒ B) t' → Tm (Γ ▹ A) B (I.app t')

    tt : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Unit I.tt
    ⟨_,_⟩ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ A u' → Tm Γ B v' → Tm Γ (A × B) I.⟨ u' , v' ⟩
    proj₁ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ A (I.proj₁ t')
    proj₂ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ B (I.proj₂ t')

    absurd : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ Empty t' → Tm Γ A (I.absurd t')
    inl : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ A t' → Tm Γ (A + B) (I.inl t')
    inr : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ B t' → Tm Γ (A + B) (I.inr t')
    case : ∀ {Γ' A' B' C' t' u' v'}
      {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'} →
      Tm Γ (A + B) t' → Tm (Γ ▹ A) C u' → Tm (Γ ▹ B) C v' →
      Tm Γ C (I.case t' u' v')

    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Γ ▹ A) B t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A ⇒ B) t'} → lam (app t) ≡ t

    uη : ∀ {Γ' t'} {Γ : Con Γ'} {t : Tm Γ Unit t'} →
      t =[ ap (Tm Γ Unit) I.uη ]= tt
    ×β₁ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A × B) t'} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t

    +β₁ : ∀ {Γ' A' B' C' t' u' v'}
      {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}
      {t : Tm Γ A t'}{u : Tm (Γ ▹ A) C u'}{v : Tm (Γ ▹ B) C v'} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ' A' B' C' t' u' v'}
      {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}
      {t : Tm Γ B t'}{u : Tm (Γ ▹ A) C u'}{v : Tm (Γ ▹ B) C v'} →
      case (inr t) u v ≡ v [ id , t ]
    
    lam[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Δ ▹ A) B t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    tt[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      tt [ σ ] ≡ tt
    ⟨,⟩[] : ∀ {Γ' Δ' A' B' u' v' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Δ A u'}{v : Tm Δ B v'}{σ : Sub Γ Δ σ'} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {t : Tm Δ Empty t'}{σ : Sub Γ Δ σ'} →
      (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])
    inl[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Δ A t'}{σ : Sub Γ Δ σ'} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Δ B t'}{σ : Sub Γ Δ σ'} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ' Δ' A' B' C' t' u' v' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{C : Ty C'}
      {t : Tm Δ (A + B) t'}{u : Tm (Δ ▹ A) C u'}{v : Tm (Δ ▹ B) C v'}
      {σ : Sub Γ Δ σ'} →
      (case t u v) [ σ ] ≡
        case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  _$_ : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ (A ⇒ B) t' → Tm Γ A u' → Tm Γ B (t' I.$ u')
  t $ u = def u (app t)

  -------------------------------------------
  -- eliminator
  -------------------------------------------
  
  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Unit ⟧T = Unit
  ⟦ I.Empty ⟧T = Empty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T
  ⟦ A I.+ B ⟧T = ⟦ A ⟧T + ⟦ B ⟧T

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t
    
    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦tt⟧ ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}

    ⟦absurd⟧ : ∀ {Γ A} {t : I.Tm Γ I.Empty} →
      ⟦ I.absurd {A = A} t ⟧t ≡ absurd ⟦ t ⟧t
    ⟦inl⟧ : ∀ {Γ A B} {t : I.Tm Γ A} →
      ⟦ I.inl {B = B} t ⟧t ≡ inl ⟦ t ⟧t
    ⟦inr⟧ : ∀ {Γ A B} {t : I.Tm Γ B} →
      ⟦ I.inr {A = A} t ⟧t ≡ inr ⟦ t ⟧t
    ⟦case⟧ : ∀ {Γ A B C} {t : I.Tm Γ (A I.+ B)}
      {u : I.Tm (Γ I.▹ A) C}{v : I.Tm (Γ I.▹ B) C} →
      ⟦ I.case t u v ⟧t ≡ case ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦absurd⟧ ⟦inl⟧ ⟦inr⟧ ⟦case⟧ #-}
  
  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl-}
\end{code}
