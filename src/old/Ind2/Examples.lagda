\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Ind2.Examples where

open import Lib renaming (_∘_ to _∘f_ ; _×_ to _×m_ ; _,_ to _,Σ_)
open import Ind2.Algebra
open import Ind2.Standard

open I
open St using (⟦_⟧A ; ⟦_⟧S ; ⟦_⟧T ; ⟦_⟧C ; ⟦_⟧σ ; ⟦_⟧t)

q1 : ∀ {Γ A B} → Tm (Γ ▹ A ▹ B) A
q1 = q [ p ]

q2 : ∀ {Γ A B C} → Tm (Γ ▹ A ▹ B ▹ C) A
q2 = q [ p ∘ p ]

_$2_$_ : ∀ {Γ A B C} → Tm Γ (A ⇒ B ⇒ C) → Tm Γ A → Tm Γ B → Tm Γ C
f $2 a $ b = app (app f) [ id , a , b ]
\end{code}
\begin{code}
N : Sign Empty
N = ◆ ▷ X ▷ X⇒ X

Nat : Ty
Nat = ind N

zero : ∀ {Γ} → Tm Γ Nat
zero = proj₂ (proj₁ (con N))

suc : ∀ {Γ} → Tm Γ (Nat ⇒ Nat)
suc = proj₂ (con N)

⟦conN⟧ : ∀ {Γ} → ⟦ con {Γ} N ⟧t ≡ St.con ⟦ N ⟧S
⟦conN⟧ {Γ} = St.⟦con⟧ {Γ}{Ω = N}
{-# REWRITE ⟦conN⟧ #-}

conN[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → (con N) [ σ ] ≡ con N
conN[] {σ = σ} = con[] {Ω = N}{σ}
{-# REWRITE conN[] #-}

Natβ : ∀ {Γ A} {ω : Tm Γ ((N ᴬ) A)} →
  (⟦ N ⟧S St.ᴹ) ⟦ rec N ω ⟧t ⟦ con {Γ} N ⟧t ⟦ ω ⟧t
Natβ = * ,p refl ,p (λ _ → refl)

⌜_⌝N : ∀ {Γ} → ℕ → Tm Γ Nat
⌜ O ⌝N = zero
⌜ S n ⌝N = suc $ ⌜ n ⌝N

ωdouble : ∀ {Γ} → Tm Γ ((N ᴬ) Nat)
ωdouble = ⟨ ⟨ tt , zero ⟩ , lam (suc $ (suc $ q)) ⟩

double : ∀ {Γ} → Tm Γ (Nat ⇒ Nat)
double = lam (rec N ωdouble $ q)

ωplus : ∀ {Γ} → Tm Γ ((N ᴬ) (Nat ⇒ Nat))
ωplus = ⟨ ⟨ tt , lam q ⟩ , lam (lam (suc $ (q1 $ q))) ⟩

plus : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
plus = lam (rec N ωplus $ q)

ωtimes : ∀ {Γ} → Tm Γ ((N ᴬ) (Nat ⇒ Nat))
ωtimes = ⟨ ⟨ tt , lam zero ⟩ , lam (lam (plus $2 q $ (q1 $ q))) ⟩

times : ∀ {Γ} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
times = lam (rec N ωtimes $ q)

L : (A : Ty) → Sign A
L A  = ◆ ▷ X ▷ C⇒ X⇒ X

<_> : Ty → Ty
< A > = ind (L A)

nil : ∀ {Γ A} → Tm Γ < A >
nil {A = A} = proj₂ (proj₁ (con (L A)))

cons : ∀ {Γ A} → Tm Γ (A ⇒ < A > ⇒ < A >)
cons {A = A} = proj₂ (con (L A))

⟦conL⟧ : ∀ {Γ A} → ⟦ con {Γ} (L A) ⟧t ≡ St.con ⟦ L A ⟧S
⟦conL⟧ {Γ}{A} = St.⟦con⟧ {Γ}{Ω = L A}
{-# REWRITE ⟦conL⟧ #-}

conL[] : ∀ {Γ Δ A} {σ : Sub Γ Δ} → (con (L A)) [ σ ] ≡ con (L A)
conL[] {A = A}{σ} = con[] {Ω = L A}{σ}
{-# REWRITE conL[] #-}

<>β : ∀ {Γ A B} {ω : Tm Γ (((L A) ᴬ) B)} →
  (⟦ L A ⟧S St.ᴹ) ⟦ rec (L A) ω ⟧t ⟦ con {Γ} (L A) ⟧t ⟦ ω ⟧t
<>β = * ,p refl ,p (λ a xs → refl)

⌜_⌝L : ∀ {Γ n} → 𝕍 ℕ n → Tm Γ < Nat >
⌜ □ ⌝L = nil
⌜ x ◁ xs ⌝L = cons $2 ⌜ x ⌝N $ ⌜ xs ⌝L

ωconcat : ∀ {Γ A} → Tm Γ (((L A) ᴬ) (< A > ⇒ < A >))
ωconcat = ⟨ ⟨ tt , lam q ⟩ , lam (lam (lam (cons $2 q2 $ (q1 $ q)))) ⟩

concat : ∀ {Γ A} → Tm Γ (< A > ⇒ < A > ⇒ < A >)
concat {A = A} = lam (rec (L A) ωconcat $ q)

ωsum : ∀ {Γ} → Tm Γ (((L Nat) ᴬ) Nat)
ωsum = ⟨ ⟨ tt , zero ⟩ , plus ⟩

sum : ∀ {Γ} → Tm Γ (< Nat > ⇒ Nat)
sum = lam (rec (L Nat) ωsum $ q)
\end{code}
