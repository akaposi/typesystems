{-# OPTIONS --prop #-}

module NatBool where

-- open import Lib
open import Agda.Primitive
open import Agda.Builtin.Nat renaming (Nat to ℕ)
open import Agda.Builtin.Bool renaming (Bool to 𝟚; true to tt; false to ff)

infixl 6 _+o_

infix 4 _≡_
data _≡_ {a} {A : Set a} (x : A) : A → Prop a where
  instance refl : x ≡ x
S≡ : ∀{ℓ}{A : Set ℓ}{a a' : A} → a ≡ a' → a' ≡ a
S≡ refl = refl
cong : ∀{ℓ ℓ'}{A : Set ℓ}{B : Set ℓ'}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl

module Model where

  record Model : Set₁ where
    field
      Ty         : Set
      Nat        : Ty
      Bool       : Ty
      Tm         : Ty → Set
      true       : Tm Bool
      false      : Tm Bool
      ite        : ∀{A} → Tm Bool → Tm A → Tm A → Tm A
      num        : ℕ → Tm Nat
      isZero     : Tm Nat → Tm Bool
      _+o_       : Tm Nat → Tm Nat → Tm Nat
      iteβtrue   : ∀{A}{u v : Tm A} → ite true u v ≡ u
      iteβfalse  : ∀{A}{u v : Tm A} → ite false u v ≡ v
      isZeroβ0   : isZero (num 0) ≡ true
      isZeroβsuc : {n : ℕ} → isZero (num (1 + n)) ≡ false
      +β         : {m n : ℕ} → num m +o num n ≡ num (m + n)

data Ty     : Set where
  Nat       : Ty
  Bool      : Ty
  
data Tm : Ty → Set where
  true       : Tm Bool
  false      : Tm Bool
  ite        : ∀{A} → Tm Bool → Tm A → Tm A → Tm A
  num        : ℕ → Tm Nat
  isZero     : Tm Nat → Tm Bool
  _+o_       : Tm Nat → Tm Nat → Tm Nat

data Tm= : {A : Ty} → Tm A → Tm A → Prop where
  RTm : ∀{A a} → Tm= {A} a a
  STm : ∀{A a a'} → Tm= {A} a a' → Tm= a' a
  TTm : ∀{A a a' a''} → Tm= {A} a a' → Tm= a' a'' → Tm= a a''

  true=      : Tm= true true
  false=     : Tm= false false
  ite=       : ∀{A b₀ b₁ u₀ u₁ v₀ v₁} → Tm= b₀ b₁ → Tm= u₀ u₁ → Tm= v₀ v₁ → Tm= (ite {A} b₀ u₀ v₀) (ite {A} b₁ u₁ v₁)
  num=       : (n : ℕ) → Tm= (num n) (num n)
  isZero=    : ∀{t₀ t₁} → Tm= t₀ t₁ → Tm= (isZero t₀) (isZero t₁)
  _+o=_      : ∀{u₀ u₁ v₀ v₁} → Tm= u₀ u₁ → Tm= v₀ v₁ → Tm= (u₀ +o v₀) (u₁ +o v₁)

  iteβtrue   : ∀{A}{u v : Tm A} → Tm= (ite true u v) u
  iteβfalse  : ∀{A}{u v : Tm A} → Tm= (ite false u v) v
  isZeroβ0   : Tm= (isZero (num 0)) true
  isZeroβsuc : {n : ℕ} → Tm= (isZero (num (1 + n))) false
  +β         : {m n : ℕ} → Tm= (num m +o num n) (num (m + n))

infix  3 _∎
infixr 2 _≡⟨_⟩_
_∎ : ∀{A}(x : Tm A) → Tm= x x
x ∎ = RTm
_≡⟨_⟩_ : ∀{A}(x : Tm A){y z : Tm A} → Tm= x y → Tm= y z → Tm= x z
x ≡⟨ x≡y ⟩ y≡z = TTm x≡y y≡z

ex : Tm= (ite (isZero (num 0 +o num 1)) false (isZero (num 0))) true
ex = 
  ite (isZero (num 0 +o num 1)) false (isZero (num 0))
    ≡⟨ ite= (isZero= +β) RTm RTm ⟩
  ite (isZero (num 1)) false (isZero (num zero)) 
    ≡⟨ ite= isZeroβsuc RTm RTm ⟩
  ite false false (isZero (num zero))
    ≡⟨ iteβfalse ⟩ 
  isZero (num 0)
    ≡⟨ isZeroβ0 ⟩ 
  true
    ∎

⟦_⟧T : Ty → Set
⟦ Nat ⟧T = ℕ
⟦ Bool ⟧T = 𝟚

isZeroℕ : ℕ → 𝟚
isZeroℕ zero = tt
isZeroℕ (suc _) = ff

⟦_⟧t : ∀{A} → Tm A → ⟦ A ⟧T
⟦ true ⟧t = tt
⟦ false ⟧t = ff
⟦ ite {A} t u v ⟧t = {!!}
⟦ num n ⟧t = n
⟦ isZero t ⟧t = isZeroℕ ⟦ t ⟧t
⟦ u +o v ⟧t = ⟦ u ⟧t + ⟦ v ⟧t

⟦_⟧t= : ∀{A}{t₀ t₁ : Tm A} → Tm= t₀ t₁ → ⟦ t₀ ⟧t ≡ ⟦ t₁ ⟧t
⟦ RTm ⟧t= = refl
⟦ STm t= ⟧t= = S≡ ⟦ t= ⟧t=
⟦ TTm t= t=' ⟧t= = {!!}
⟦ true= ⟧t= = {!!}
⟦ false= ⟧t= = {!!}
⟦ ite= t= t=₁ t=₂ ⟧t= = {!!}
⟦ num= n ⟧t= = {!!}
⟦ isZero= t= ⟧t= = cong isZeroℕ ⟦ t= ⟧t=
⟦ t= +o= t=₁ ⟧t= = {!!}
⟦ iteβtrue ⟧t= = {!!}
⟦ iteβfalse ⟧t= = {!!}
⟦ isZeroβ0 ⟧t= = refl
⟦ isZeroβsuc ⟧t= = refl
⟦ +β ⟧t= = refl
{-
⟦ iteβtrue {_} {u} {v} i ⟧t = St⟦ u ⟧t
⟦ iteβfalse {_} {u} {v} i ⟧t = St⟦ v ⟧t
⟦ isZeroβ0 i ⟧t = tt
⟦ isZeroβsuc i ⟧t = ff
⟦ +β {m} {n} i ⟧t = m + n
-}
