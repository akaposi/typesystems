{-# OPTIONS --prop --rewriting #-}

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)
open import Full.I

module Full.DepAlgebra where

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_

  field
    Con : I.Con → Set i
    Ty  : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm  : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ

    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙ : Con I.∙
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ ap (Sub Γ ∙) I.∙η ]= ε

    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p
    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ

    Bool : Ty I.Bool
    true : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.true
    false : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.false
    ite : ∀ {Γ' A' b' u' v'}  {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ Bool b' → Tm Γ A u' → Tm Γ A v' → Tm Γ A (I.ite b' u' v')
    true[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      true [ σ ] ≡ true
    false[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      false [ σ ] ≡ false
    ite[] : ∀ {Γ' Δ' A' b' u' v' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {b : Tm Δ Bool b'}{u : Tm Δ A u'}{v : Tm Δ A v'}
      {σ : Sub Γ Δ σ'} →
      (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])
    iteβ₁ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite true u v ≡ u
    iteβ₂ : ∀ {Γ' A' u' v'} {Γ : Con Γ'}{A : Ty A'}
      {u : Tm Γ A u'}{v : Tm Γ A v'} → ite false u v ≡ v

    Tree1       : ∀{A'} → Ty A' → Ty (I.Tree1 A')
    leaf1       : ∀{Γ' A'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ (Tree1 A) I.leaf1
    node1       : ∀{Γ' A' u' v' w'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ (Tree1 A) u' → Tm Γ A w' → Tm Γ (Tree1 A) v' → Tm Γ (Tree1 A) (I.node1 u' w' v')
    recTree1    : ∀{Γ' A' B' u' v' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm Γ B u' → Tm (Γ ▹ B ▹ A ▹ B) B v' → Tm Γ (Tree1 A) t' → Tm Γ B (I.recTree1 u' v' t')
    Tree1β₁     : ∀{Γ' A' B' u' v'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{u : Tm Γ B u'}{v : Tm (Γ ▹ B ▹ A ▹ B) B v'} → recTree1 u v leaf1 ≡ u
    Tree1β₂     : ∀{Γ' A' B' u' v' t₁' t₂' w'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{u : Tm Γ B u'}{v : Tm (Γ ▹ B ▹ A ▹ B) B v'}{t₁ : Tm Γ (Tree1 A) t₁'}{t₂ : Tm Γ (Tree1 A) t₂'}{w : Tm Γ A w'} →
                 recTree1 u v (node1 t₁ w t₂) ≡ v [ id , recTree1 u v t₁ , w , recTree1 u v t₂ ]
    leaf1[]     : ∀{Γ' Δ' A' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{σ : Sub Γ Δ σ'} → leaf1 {A = A} [ σ ] ≡ leaf1
    node1[]     : ∀{Γ' Δ' A' t₁' t₂' w' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{t₁ : Tm Δ (Tree1 A) t₁'}{t₂ : Tm Δ (Tree1 A) t₂'}{w : Tm Δ A w'}{σ : Sub Γ Δ σ'} →
                 (node1 t₁ w t₂) [ σ ] ≡ node1 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree1[]  : ∀{Γ' Δ' A' B' u' v' t' σ'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{Δ : Con Δ'}{u : Tm Δ B u'}{v : Tm (Δ ▹ B ▹ A ▹ B) B v'}{t : Tm Δ (Tree1 A) t'}{σ : Sub Γ Δ σ'} →
                 recTree1 u v t [ σ ] ≡ recTree1 (u [ σ ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  -------------------------------------------
  -- eliminator
  -------------------------------------------

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.Tree1 A ⟧T = Tree1 ⟦ A ⟧T

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦leaf1⟧ : ∀ {Γ A} → ⟦ I.leaf1 {Γ}{A} ⟧t ≡ leaf1
    ⟦node1⟧ : ∀ {Γ A} {t t' : I.Tm Γ (I.Tree1 A)}{t'' : I.Tm Γ A} →
      ⟦ I.node1 t t'' t' ⟧t ≡ node1 ⟦ t ⟧t ⟦ t'' ⟧t ⟦ t' ⟧t
    ⟦recTree1⟧ : ∀{Γ A B}{u : I.Tm Γ B}{v : I.Tm (Γ I.▹ B I.▹ A I.▹ B) B}{t : I.Tm Γ (I.Tree1 A)} →
      ⟦ I.recTree1 u v t ⟧t ≡ recTree1 ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf1⟧ ⟦node1⟧ ⟦recTree1⟧ #-}

  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl
