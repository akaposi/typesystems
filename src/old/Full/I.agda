{-# OPTIONS --prop --rewriting #-}

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)

module Full.I where

module I where
  data Ty      : Set where
    Bool       : Ty
    Tree1      : Ty → Ty

  data Con     : Set where
    ∙          : Con
    _▹_        : Con → Ty → Con

  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_

  postulate
    Sub        : Con → Con → Set
    Tm         : Con → Ty → Set

    _∘_        : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id         : ∀{Γ} → Sub Γ Γ
    ass        : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                 (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl        : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr        : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]       : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]       : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]        : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                 t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _,_        : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η         : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    ite        : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁      : ∀{Γ A} {u v : Tm Γ A} → ite true u v ≡ u
    iteβ₂      : ∀{Γ A} {u v : Tm Γ A} → ite false u v ≡ v
    true[]     : ∀{Γ Δ} {σ : Sub Γ Δ} → true [ σ ] ≡ true
    false[]    : ∀{Γ Δ} {σ : Sub Γ Δ} → false [ σ ] ≡ false
    ite[]      : ∀{Γ Δ A} {b : Tm Δ Bool}{u v : Tm Δ A}{σ : Sub Γ Δ} →
                 (ite b u v) [ σ ] ≡ ite (b [ σ ]) (u [ σ ]) (v [ σ ])

    leaf1      : ∀{Γ A} → Tm Γ (Tree1 A)
    node1      : ∀{Γ A} → Tm Γ (Tree1 A) → Tm Γ A → Tm Γ (Tree1 A) → Tm Γ (Tree1 A)
    recTree1   : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ B ▹ A ▹ B) B → Tm Γ (Tree1 A) → Tm Γ B
    Tree1β₁    : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A ▹ B) B} → recTree1 u v leaf1 ≡ u
    Tree1β₂    : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ B ▹ A ▹ B) B}{t₁ t₂ : Tm Γ (Tree1 A)}{w : Tm Γ A} →
                 recTree1 u v (node1 t₁ w t₂) ≡ v [ id , recTree1 u v t₁ , w , recTree1 u v t₂ ]
    leaf1[]    : ∀{Γ Δ A}{σ : Sub Γ Δ} → leaf1 {Δ}{A} [ σ ] ≡ leaf1
    node1[]    : ∀{Γ Δ A}{t₁ t₂ : Tm Δ (Tree1 A)}{w : Tm Δ A}{σ : Sub Γ Δ} →
                 (node1 t₁ w t₂) [ σ ] ≡ node1 (t₁ [ σ ]) (w [ σ ]) (t₂ [ σ ])
    recTree1[] : ∀{Γ Δ A B}{u : Tm Δ B}{v : Tm (Δ ▹ B ▹ A ▹ B) B}{t : Tm Δ (Tree1 A)}{σ : Sub Γ Δ} →
                 recTree1 u v t [ σ ] ≡ recTree1 (u [ σ ]) (v [ ((σ ∘ p , q) ∘ p , q) ∘ p , q ]) (t [ σ ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE iteβ₁ iteβ₂ true[] false[] ite[] #-}
  {-# REWRITE Tree1β₁ Tree1β₂ leaf1[] node1[] recTree1[] #-}
