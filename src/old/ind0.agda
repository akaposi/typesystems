module ind where

open import Agda.Primitive

data SP : Set₁ where
  X : SP
  ty : Set → SP
  _×_ : SP → SP → SP
  _⇒_ : Set → SP → SP

record _×c_ {i j} (A : Set i)(B : Set j) : Set (i ⊔ j) where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B

_$_ : SP → Set → Set
X $ A = A
ty A $ B = A
(F × G) $ A = (F $ A) ×c (G $ A)
(A ⇒ F) $ B = A → F $ B

fmap : {A B : Set}{F : SP} → (A → B) → (F $ A) → (F $ B)
fmap {F = X} f = f
fmap {F = ty A} f x = x
fmap {F = F × G} f (x , y) = fmap {F = F} f x , fmap {F = G} f y
fmap {F = A ⇒ F} f x t = fmap {F = F} f (x t)

data ind (F : SP) : Set where
  con : (F $ ind F) → ind F

rec : {A : Set}{F : SP} → (F $ A → A) → ind F → A
rec {F = F} f (con x) = f (fmap {F = F} (rec f) x)

record coind (F : SP) : Set where
  coinductive
  field
    des : F $ coind F
open coind

gen : {A : Set}{F : SP} → (A → F $ A) → A → coind F
des (gen {F = F} f x) = fmap {F = F} (gen f) (f x)
