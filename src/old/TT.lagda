\chapter{Dependent types}

\begin{code}
{-# OPTIONS --prop --rewriting #-}
module TT where

open import Lib

record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _⊚_
  infixl 6 _[_]T
  infixl 6 _[_]t
  infixl 5 _▹_
  infixl 5 _,o_
  infixl 5 _,'_
  
  field
    Con        : Set i
    Sub        : Con → Con → Set j
    _⊚_        : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass        : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} →
                 (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id         : ∀{Γ} → Sub Γ Γ
    idl        : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr        : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ◇          : Con
    ε          : ∀{Γ} → Sub Γ ◇
    ◇η         : ∀{Γ}{σ : Sub Γ ◇} → σ ≡ ε

    Ty         : Con → Set k
    _[_]T      : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
    [∘]T       : ∀{Γ A Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} →
                 A [ γ ⊚ δ ]T ≡ A [ γ ]T [ δ ]T
    [id]T      : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A

    Tm         : (Γ : Con) → Ty Γ → Set l
    _[_]t      : ∀{Γ A} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
    [∘]t       : ∀{Γ A}{t : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} →
                 (Tm Θ ~) [∘]T (t [ γ ⊚ δ ]t) (t [ γ ]t [ δ ]t)
    [id]t      : ∀{Γ A}{t : Tm Γ A} → (Tm Γ ~) [id]T (t [ id ]t) t
    _▹_        : (Γ : Con) → Ty Γ → Con
    _^_        : ∀{Γ Δ}(γ : Sub Δ Γ)(A : Ty Γ) → Sub (Δ ▹ A [ γ ]T) (Γ ▹ A)
    _,o_       : ∀{Γ Δ}(γ : Sub Δ Γ) → ∀{A} → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
    π₁         : ∀{Γ A Δ} → Sub Δ (Γ ▹ A) → Sub Δ Γ
    π₂         : ∀{Γ A Δ}(γa : Sub Δ (Γ ▹ A)) → Tm Δ (A [ π₁ γa ]T)
    ▹β₁        : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → π₁ (γ ,o a) ≡ γ
    ▹β₂        : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → (Tm Δ ~) (cong (A [_]T) ▹β₁) (π₂ (γ ,o a)) a
    ▹η         : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → π₁ γa ,o π₂ γa ≡ γa
    -- TODO : _^_ rules
    
    Π          : ∀{Γ}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
    Π[]        : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → Π A B [ γ ]T ≡ Π (A [ γ ]T) (B [ γ ^ A ]T)
    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (Π A B)
    app        : ∀{Γ A B} → Tm Γ (Π A B) → Tm (Γ ▹ A) B
    Πβ         : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    Πη         : ∀{Γ A B}{t : Tm Γ (Π A B)} → lam (app t) ≡ t
    lam[]      : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} → (Tm Δ ~) Π[] (lam t [ γ ]t) (lam (t [ γ ^ A ]t))

    Σ'         : ∀{Γ}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
    Σ[]        : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → Σ' A B [ γ ]T ≡ Σ' (A [ γ ]T) (B [ γ ^ A ]T)
    _,'_       : ∀{Γ A B}(a : Tm Γ A) → Tm Γ (B [ id ,o a [ id ]t ]T) → Tm Γ (Σ' A B)
    proj₁      : ∀{Γ A B} → Tm Γ (Σ' A B) → Tm Γ A
    proj₂      : ∀{Γ A B}(ab : Tm Γ (Σ' A B)) → Tm Γ (B [ id ,o (proj₁ ab [ id ]t) ]T)
    Σβ₁        : ∀{Γ A B}{a : Tm Γ A}{b : Tm Γ (B [ id ,o a [ id ]t ]T)} → proj₁ (a ,' b) ≡ a
    Σβ₂        : ∀{Γ A B}{a : Tm Γ A}{b : Tm Γ (B [ id ,o a [ id ]t ]T)} → (Tm Γ ~) (cong (λ z → B [ id ,o z [ id ]t ]T) Σβ₁) (proj₂ (a ,' b)) b
    Ση         : ∀{Γ A B}{ab : Tm Γ (Σ' A B)} → (proj₁ ab ,' proj₂ ab) ≡ ab
    ,[]        : ∀{Γ A B}{a : Tm Γ A}{b : Tm Γ (B [ id ,o a [ id ]t ]T)}{Δ}{γ : Sub Δ Γ} → (Tm Δ ~) Σ[] ((a ,' b) [ γ ]t) (a [ γ ]t ,' transp (Tm Δ) {!!} (b [ γ ]t))

    {-
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) (A [ p ]T)
    ▹β₁        : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{t : Tm Δ (A [ γ ]T)} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂        : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{t : Tm Δ (A [ γ ]T)} → (Tm Δ ~) ([∘]T ⁻¹ ◾ cong (A [_]T) ▹β₁) (q [ γ ,o t ]t) t
    ▹η         : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o transp (Tm Δ) ([∘]T ⁻¹) (q [ γa ]t) ≡ γa

    U          : Ty ◇
    El         : Ty (◇ ▹ U)

    Π          : ∀{Γ}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
    Π[]        : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → Π A B [ γ ]T ≡ Π (A [ γ ]T) (B [ γ ⊚ p ,o transp (Tm (Δ ▹ A [ γ ]T)) ([∘]T ⁻¹) q ]T)
    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (Π A B)
    app        : ∀{Γ A B} → Tm Γ (Π A B) → Tm (Γ ▹ A) B
    Πβ         : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    Πη         : ∀{Γ A B}{t : Tm Γ (Π A B)} → lam (app t) ≡ t
    lam[]      : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} → (Tm Δ ~) Π[] (lam t [ γ ]t) (lam (t [ γ ⊚ p ,o transp (Tm (Δ ▹ A [ γ ]T)) ([∘]T ⁻¹) q ]t))

    Σ'         : ∀{Γ}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
    Σ[]        : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → Σ' A B [ γ ]T ≡ Σ' (A [ γ ]T) (B [ γ ⊚ p ,o transp (Tm (Δ ▹ A [ γ ]T)) ([∘]T ⁻¹) q ]T)
    _,'_       : ∀{Γ A B}(a : Tm Γ A) → Tm Γ (B [ id ,o a [ id ]t ]T) → Tm Γ (Σ' A B)
    proj₁      : ∀{Γ A B} → Tm Γ (Σ' A B) → Tm Γ A
    proj₂      : ∀{Γ A B}(ab : Tm Γ (Σ' A B)) → Tm Γ (B [ id ,o (proj₁ ab [ id ]t) ]T)
    Σβ₁        : ∀{Γ A B}{a : Tm Γ A}{b : Tm Γ (B [ id ,o a [ id ]t ]T)} → proj₁ (a ,' b) ≡ a
    Σβ₂        : ∀{Γ A B}{a : Tm Γ A}{b : Tm Γ (B [ id ,o a [ id ]t ]T)} → (Tm Γ ~) (cong (λ z → B [ id ,o z [ id ]t ]T) Σβ₁) (proj₂ (a ,' b)) b
    Ση         : ∀{Γ A B}{ab : Tm Γ (Σ' A B)} → (proj₁ ab ,' proj₂ ab) ≡ ab
    ,[]        : ∀{Γ A B}{a : Tm Γ A}{b : Tm Γ (B [ id ,o a [ id ]t ]T)}{Δ}{γ : Sub Δ Γ} → (Tm Δ ~) Σ[] ((a ,' b) [ γ ]t) (a [ γ ]t ,' transp (Tm Δ) ([∘]T ⁻¹ ◾ cong (B [_]T) {!!} ◾ [∘]T) (b [ γ ]t))
-}
\end{code}
