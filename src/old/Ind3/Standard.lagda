\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Ind3.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _×_ to _×m_ ; _,_ to _⸴_)
open import Ind3.Algebra
\end{code}
\begin{code}
module M where
  Ty : Set₁
  Ty = Set

  Con : Set₁
  Con = Set

  Sub : Con → Con → Set
  Sub Γ Δ = Γ → Δ

  Tm : Con → Ty → Set
  Tm Γ A = Γ → A

  Unit : Ty
  Unit = 𝟙↑

  Empty : Ty
  Empty = 𝟘↑

  _⇒_ : Ty → Ty → Ty
  A ⇒ B = A → B

  _×_ : Ty → Ty → Ty
  _×_ = _×m_

  _+_ : Ty → Ty → Ty
  _+_ = _⊎_

  ∙ : Con
  ∙ = 𝟙↑

  _▹_ : Con → Ty → Con
  _▹_ = _×m_

  _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
  _∘_ = _∘f_

  id : ∀ {Γ} → Sub Γ Γ
  id = idf

  ε : ∀ {Γ} → Sub Γ ∙
  ε = _

  _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
  σ , t = λ γ → σ γ ⸴ t γ

  p : ∀ {Γ A} → Sub (Γ ▹ A) Γ
  p = π₁

  q : ∀ {Γ A} → Tm (Γ ▹ A) A
  q = π₂

  _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
  _[_] = _∘f_

  lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  lam t γ a = t (γ ⸴ a)

  app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
  app t γa = t (π₁ γa) (π₂ γa)

  tt : ∀ {Γ} → Tm Γ Unit
  tt = _

  ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
  ⟨ u , v ⟩ γ = u γ ⸴ v γ

  proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
  proj₁ t γ = π₁ (t γ)

  proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B
  proj₂ t γ = π₂ (t γ)

  absurd : ∀ {Γ A} → Tm Γ Empty → Tm Γ A
  absurd t γ = ⟦ ↓[ t γ ]↓ ⟧𝟘

  inl : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
  inl t γ = ι₁ (t γ)

  inr : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
  inr t γ = ι₂ (t γ)

  case : ∀ {Γ A B C} → Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C
  case t u v γ = ind⊎ _ (λ a → u (γ ⸴ a)) (λ b → v (γ ⸴ b)) (t γ)

  -- inductive types

  data Ar (n : ℕ) : Set where
    X   : Ar n
    X⇒_ : Ar n → Ar n
    _⇛_ : Fin n → Ar n → Ar n

  data Sign (n : ℕ) : Set where
    ◆   : Sign n
    _▷_ : Sign n → Ar n → Sign n

  data VarI {n} : Sign n → Ar n → Set where
    vvz : ∀ {Ω R} → VarI (Ω ▷ R) R
    vvs : ∀ {Ω R S} → VarI Ω R → VarI (Ω ▷ S) R

  data TmI {n} (P : 𝕍 Ty n)(Ω : Sign n) : Ar n → Set where
    var : ∀ {R} → VarI Ω R → TmI P Ω R
    _$_ : ∀ {R} → TmI P Ω (X⇒ R) → TmI P Ω X → TmI P Ω R
    _$̂_ : ∀ {R k} → TmI P Ω (k ⇛ R) → (P ‼ k) → TmI P Ω R

  data SubI {n} (P : 𝕍 Ty n)(Ω : Sign n) : Sign n → Set where
    εₛ   : SubI P Ω ◆
    _,ₛ_ : ∀ {Σ R} → SubI P Ω Σ → TmI P Ω R → SubI P Ω (Σ ▷ R)

  vz : ∀ {n P Ω R} → TmI {n} P (Ω ▷ R) R
  vz = var vvz

  vs : ∀ {n P Ω R S} → TmI {n} P Ω R → TmI P (Ω ▷ S) R
  vs (var x) = var (vvs x)
  vs (t $ u) = vs t $ vs u
  vs (t $̂ u) = vs t $̂ u

  wk : ∀ {n P Ω Σ S} → SubI {n} P Ω Σ → SubI P (Ω ▷ S) Σ
  wk εₛ       = εₛ
  wk (σ ,ₛ t) = wk σ ,ₛ vs t

  idₛ : ∀ {n P Ω} → SubI {n} P Ω Ω
  idₛ {Ω = ◆}     = εₛ
  idₛ {Ω = Ω ▷ R} = wk idₛ ,ₛ vz

  _ᴬA : ∀ {n} → Ar n → 𝕍 Ty n → Ty → Ty
  (X       ᴬA) P A = A
  ((X⇒ R)  ᴬA) P A = A ⇒ (R ᴬA) P A
  ((k ⇛ R) ᴬA) P A = (P ‼ k) ⇒ (R ᴬA) P A

  _ᴬ : ∀ {n} → Sign n → 𝕍 Ty n → Ty → Ty
  (◆       ᴬ) P A = Unit
  ((Ω ▷ R) ᴬ) P A = (Ω ᴬ) P A × (R ᴬA) P A

  _ᴬv : ∀ {n P Ω R A} → VarI {n} Ω R → (Ω ᴬ) P A → (R ᴬA) P A
  (vvz   ᴬv) (ω ⸴ α) = α
  (vvs x ᴬv) (ω ⸴ β) = (x ᴬv) ω

  _ᴬt : ∀ {n P Ω R A} → TmI {n} P Ω R → (Ω ᴬ) P A → (R ᴬA) P A
  ((var x) ᴬt) ω = (x ᴬv) ω
  ((t $ u) ᴬt) ω = (t ᴬt) ω ((u ᴬt) ω)
  ((t $̂ u) ᴬt) ω = (t ᴬt) ω u

  _ᴬs : ∀ {n P Ω Σ A} → SubI {n} P Ω Σ → (Ω ᴬ) P A → (Σ ᴬ) P A
  (εₛ       ᴬs) ω = *↑
  ((σ ,ₛ t) ᴬs) ω = (σ ᴬs) ω ⸴ (t ᴬt) ω

  vsᴬ : ∀ {n P Ω R S A} (t : TmI {n} P Ω R)
    {ω : (Ω ᴬ) P A}{α : (S ᴬA) P A} →
    (vs {S = S} t ᴬt) (ω ⸴ α) ≡ (t ᴬt) ω
  vsᴬ (var x) = refl
  vsᴬ (t $ u) = vsᴬ t ⊛ vsᴬ u
  vsᴬ (t $̂ u) = vsᴬ t &♯ u

  wkᴬ : ∀ {n P Ω Σ S A} (σ : SubI {n} P Ω Σ)
    {ω : (Ω ᴬ) P A}{α : (S ᴬA) P A} →
    (wk {S = S} σ ᴬs) (ω ⸴ α) ≡ (σ ᴬs) ω
  wkᴬ εₛ       = refl
  wkᴬ (σ ,ₛ t) = _⸴_ & wkᴬ σ ⊛ vsᴬ t

  idᴬ : ∀ {n P A} {Ω : Sign n}{ω : (Ω ᴬ) P A} → (idₛ ᴬs) ω ≡ ω
  idᴬ {Ω = ◆}            = refl
  idᴬ {Ω = Ω ▷ R}{ω ⸴ α} = (_⸴ α) & (wkᴬ idₛ ◾ idᴬ)

  -- morphisms between algebras of the metatheoretic inductive types

  _ᴵᴹA : ∀ {A B n} (R : Ar n) {P} →
    (A → B) → ((R ᴬA) P A) → ((R ᴬA) P B) → Prop
  (X       ᴵᴹA) F t u = F t ≡ u
  ((X⇒ R)  ᴵᴹA) F f g = ∀ t → (R ᴵᴹA) F (f t) (g (F t))
  ((k ⇛ R) ᴵᴹA) F f g = ∀ t → (R ᴵᴹA) F (f t) (g t)

  _ᴵᴹ : ∀ {A B n} (Ω : Sign n) {P} →
    (A → B) → ((Ω ᴬ) P A) → ((Ω ᴬ) P B) → Prop
  (◆       ᴵᴹ) F .*↑     .*↑       = 𝟙'
  ((Ω ▷ R) ᴵᴹ) F (ω ⸴ α) (ω' ⸴ α') = (Ω ᴵᴹ) F ω ω' ×p (R ᴵᴹA) F α α'

  -- "internal" morphisms

  _ᴹA : ∀ {Γ A B n} (R : Ar n) {P} →
    Tm Γ (A ⇒ B) → Tm Γ ((R ᴬA) P A) → Tm Γ ((R ᴬA) P B) → Prop
  (X       ᴹA) F t u = F ⊗ t ≡ u
  ((X⇒ R)  ᴹA) F f g = ∀ t → (R ᴹA) F (f ⊗ t) (g ⊗ (F ⊗ t))
  ((k ⇛ R) ᴹA) F f g = ∀ t → (R ᴹA) F (f ⊗ t) (g ⊗ t)

  _ᴹ : ∀ {Γ A B n} (Ω : Sign n) {P} →
    Tm Γ (A ⇒ B) → Tm Γ ((Ω ᴬ) P A) → Tm Γ ((Ω ᴬ) P B) → Prop
  (◆       ᴹ) F t u = 𝟙'
  ((Ω ▷ R) ᴹ) F t u =
    (Ω ᴹ) F (proj₁ t) (proj₁ u) ×p (R ᴹA) F (proj₂ t) (proj₂ u)

  ind : ∀ {n} → Sign n → 𝕍 Ty n → Ty
  ind Ω P = TmI P Ω X

  module _ {n} (P : 𝕍 Ty n)(Ω : Sign n) where
    -- constructor

    conᵗ : ∀ {R} → TmI P Ω R → (R ᴬA) P (ind Ω P)
    conᵗ {X}     t = t
    conᵗ {X⇒ R}  t = λ u → conᵗ (t $ u)
    conᵗ {k ⇛ R} t = λ u → conᵗ (t $̂ u)

    conˢ : ∀ {Σ} → SubI P Ω Σ → (Σ ᴬ) P (ind Ω P)
    conˢ εₛ       = *↑
    conˢ (σ ,ₛ t) = conˢ σ ⸴ conᵗ t

    conᴵ : (Ω ᴬ) P (ind Ω P)
    conᴵ = conˢ idₛ

    module _ {A : Ty}(ω : ((Ω ᴬ) P A)) where
      -- recursor
      
      recᴵ : ind Ω P → A
      recᴵ t = (t ᴬt) ω

      indβᵗ : ∀ {R} (t : TmI P Ω R) → (R ᴵᴹA) recᴵ (conᵗ t) ((t ᴬt) ω)
      indβᵗ {X}     t = refl
      indβᵗ {X⇒ R}  t = λ u → indβᵗ (t $ u)
      indβᵗ {k ⇛ R} t = λ u → indβᵗ (t $̂ u)

      indβˢ : ∀ {Σ} (σ : SubI P Ω Σ) → (Σ ᴵᴹ) recᴵ (conˢ σ) ((σ ᴬs) ω)
      indβˢ εₛ       = *
      indβˢ (σ ,ₛ t) = indβˢ σ ,p indβᵗ t

      indβᴵ : (Ω ᴵᴹ) recᴵ conᴵ ω
      indβᴵ = transportp ((Ω ᴵᴹ) recᴵ conᴵ) idᴬ (indβˢ idₛ)

  con : ∀ {Γ n} (Ω : Sign n) P → Tm Γ ((Ω ᴬ) P (ind Ω P))
  con Ω P γ = conᴵ P Ω

  rec : ∀ {Γ A n P} (Ω : Sign n) → Tm Γ ((Ω ᴬ) P A) → Tm Γ (ind Ω P ⇒ A)
  rec Ω ω γ t = recᴵ _ Ω (ω γ) t

  gen-indβᵗ : ∀ {Γ n P A B} (R : Ar n)(F : Tm Γ (A ⇒ B))
    (t : Tm Γ ((R ᴬA) P A))(u : Tm Γ ((R ᴬA) P B)) →
    (∀ γ → (R ᴵᴹA) (F γ) (t γ) (u γ)) → (R ᴹA) F t u
  gen-indβᵗ X       F t u p = funext p
  gen-indβᵗ (X⇒ R)  F f g p =
    λ t → gen-indβᵗ R F (f ⊗ t) (g ⊗ (F ⊗ t)) (λ γ → p γ (t γ))
  gen-indβᵗ (k ⇛ R) F f g p =
    λ t → gen-indβᵗ R F (f ⊗ t) (g ⊗ t) (λ γ → p γ (t γ))

  gen-indβˢ : ∀ {Γ n P A B} (Ω : Sign n)(F : Tm Γ (A ⇒ B))
    (t : Tm Γ ((Ω ᴬ) P A))(u : Tm Γ ((Ω ᴬ) P B)) →
    (∀ γ → (Ω ᴵᴹ) (F γ) (t γ) (u γ)) → (Ω ᴹ) F t u
  gen-indβˢ ◆       F t u p = *
  gen-indβˢ (Ω ▷ R) F t u p =
       gen-indβˢ Ω F (proj₁ t) (proj₁ u) (λ γ → π₁ (p γ))
    ,p gen-indβᵗ R F (proj₂ t) (proj₂ u) (λ γ → π₂ (p γ))

  indβ : ∀ {Γ A n P} (Ω : Sign n)(ω : Tm Γ ((Ω ᴬ) P A)) →
    (Ω ᴹ) (rec Ω ω) (con Ω P) ω
  indβ Ω ω = gen-indβˢ Ω (rec Ω ω) (con Ω _) ω (λ γ → indβᴵ _ Ω (ω γ))

St : Algebra
St = record
       { Ty = M.Ty
       ; Con = M.Con
       ; Ar = M.Ar
       ; Sign = M.Sign
       ; Sub = M.Sub
       ; Tm = M.Tm
       ; Unit = M.Unit
       ; Empty = M.Empty
       ; _⇒_ = M._⇒_
       ; _×_ = M._×_
       ; _+_ = M._+_
       ; ind = M.ind
       ; ∙ = M.∙
       ; _▹_ = M._▹_
       ; X = M.X
       ; X⇒_ = M.X⇒_
       ; _⇛_ = M._⇛_
       ; ◆ = M.◆
       ; _▷_ = M._▷_
       ; _ᴬA = M._ᴬA
       ; _ᴬ = M._ᴬ
       ; _ᴹA = M._ᴹA
       ; _ᴹ = M._ᴹ
       ; _∘_ = M._∘_
       ; id = M.id
       ; ε = M.ε
       ; _,_ = M._,_
       ; p = M.p
       ; q = M.q
       ; _[_] = M._[_]
       ; lam = M.lam
       ; app = M.app
       ; tt = M.tt
       ; ⟨_,_⟩ = M._,_
       ; proj₁ = M.proj₁
       ; proj₂ = M.proj₂
       ; absurd = M.absurd
       ; inl = M.inl
       ; inr = M.inr
       ; case = M.case
       ; con = M.con
       ; rec = M.rec
       ; ᴬAX = refl
       ; ᴬAX⇒ = refl
       ; ᴬA⇛ = refl
       ; ᴬ◆ = refl
       ; ᴬ▷ = refl
       ; ᴹAX = refl
       ; ᴹAX⇒ = refl
       ; ᴹA⇛ = refl
       ; ᴹ◆ = refl
       ; ᴹ▷ = refl
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; uη = refl
       ; ×β₁ = refl
       ; ×β₂ = refl
       ; ×η = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; indβ = M.indβ
       ; lam[] = refl
       ; tt[] = refl
       ; ⟨,⟩[] = refl
       ; absurd[] = refl
       ; inl[] = refl
       ; inr[] = refl
       ; case[] = refl
       ; con[] = refl
       ; rec[] = refl
       }
module St = Algebra St
\end{code}
