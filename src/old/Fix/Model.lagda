\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Fix.Model where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_ )
open import Fix.Algebra
open import Fix.OCPO

open Fix.Algebra.I
open PartialityMonad
\end{code}
\begin{code}

apΣ : ∀{i j} {A : Set i} {B : A → Set j} {u v : Σ A B}
  (e : π₁ u ≡ π₁ v) → π₂ u =[ ap B e ]= π₂ v → v ≡ u
apΣ refl refl = refl

ap∐ : ∀ {Ω : ωCPO {lzero}{lzero} (↑p 𝟘) } {f f' p p'} →
  f ≡ f' → ωCPO.∐ Ω f p ≡ ωCPO.∐ Ω f' p'  
ap∐ refl = refl

isO : ℕ → 𝟚
isO O = I
isO (S n) = O

f' : ∀ {Γ A} → ωCPOMor (Γ ×ω A) A → ωCPO.D Γ → ℕ → ωCPO.D A
f' {Γ}{A} t γ O = ωCPO.⊥ A 
f' {Γ}{A} t γ (S n) = ωCPOMor.D t (γ ,Σ f'{Γ} t γ n)

p' : ∀ {Γ A} (t : ωCPOMor (Γ ×ω A) A) (γ : ωCPO.D Γ) →
  (n : ℕ) → ωCPO._⊑_ A  (f'{Γ} t γ n) (ωCPOMor.D t (γ ,Σ f'{Γ} t γ n))
p' {Γ}{A} t γ O = ωCPO.inf⊑ A
p' {Γ}{A} t γ (S n) = ωCPOMor._⊑_ t ((ωCPO.refl⊑ Γ) ,p (p' t γ n))


∐-const-lem : ∀{Ω : ωCPO {lzero}{lzero} (↑p 𝟘)}{ω : ωCPO.D Ω} → ω ≡ ωCPO.∐ Ω (λ _ → ω) λ _ → ωCPO.refl⊑ Ω
∐-const-lem {Ω}{ω} = let module Ω = ωCPO Ω
                     in Ω.antisym⊑ (Ω.out {f = λ _ → ω}{p = λ _ → Ω.refl⊑} Ω.refl⊑ O)
                                          (Ω.ιn λ _ → Ω.refl⊑)

-- function extensionality needed for our model
private
  postulate
    funext : ∀ {i j} {A : Set i} {B : Set j} {f g : A → B} →
      (∀ {a} → f a ≡ g a) → f ≡ g

M : Algebra
M = record
      { Con = ωCPO {lzero}{lzero} (↑p 𝟘)
      ; Ty = ωCPO {lzero}{lzero} (↑p 𝟘)
      ; Sub = ωCPOMor
      ; Tm = ωCPOMor
      ; ∙ = record
              { D = ↑p 𝟙
              ; _⊑_ = λ _ _ → 𝟙
              ; η = λ _ → *↑
              ; ⊥ = *↑
              ; ∐ = λ  _ _ → *↑
              ; refl⊑ = *
              ; trans⊑ = λ _ _ → *
              ; antisym⊑ = λ _ _ → refl
              ; inf⊑ = *
              ; ιn = λ _ → *
              ; out = λ _ _ → *
              }
      ; _▹_ = _×ω_
      ; Nat = I𝟘 ℕ
      ; Bool = I𝟘 𝟚
      ; _⇒_ = λ A B → let module A = ωCPO A
                          module B = ωCPO B
                          open ωCPOMor
                      in record
                           { D = ωCPOMor A B
                           ; _⊑_ = λ F G → ∀{a} → D F a B.⊑ D G a
                           ; η = λ b  → ⟦ ↓[ b ]↓ ⟧𝟘
                           ; ⊥ = record { D = λ _ → B.⊥
                                        ; _⊑_ = λ _ → B.refl⊑
                                        ; ∐ = B.antisym⊑ B.inf⊑ (B.ιn λ _ → B.refl⊑) }
                           ; ∐ = λ f p → record { D = λ a → B.∐ (λ n → D (f n) a) (λ n → p n)
                                                ; _⊑_ = λ {γ}{γ'}u → B.ιn λ n → B.trans⊑ (f n ⊑ u )
                                                                                         (B.out {λ m → D (f m) γ'}{λ n → p n}
                                                                                                B.refl⊑ n)
                                                ; ∐ = λ {f₁}{p₁} → ap∐ {B}{f  = (λ n → D (f n) (A.∐ f₁ p₁))}
                                                              {f' = (λ x → B.∐ (λ n → D (f n) (f₁ x)) (λ n → p n))}
                                                              {p  = (λ n → p n) }
                                                              {p' = (λ n → B.ιn (λ n₁ → B.trans⊑ (f n₁ ⊑ p₁ n)
                                                                                                 ((B.out B.refl⊑ n₁))))}
                                                              (funext λ {a} → ∐ (f a)
                                                                ◾ ap∐ {B}{f = (λ x → D (f a) (f₁ x))}
                                                                  {f' = (λ n → D (f n) (f₁ a))}
                                                                  {p = (λ n → f a ⊑ p₁ n)}
                                                                  {p' = (λ n → p n)}
                                                                  {!!}) }
                           ; refl⊑ = B.refl⊑
                           ; trans⊑ = λ u v → B.trans⊑ u v
                           ; antisym⊑ = λ u v → ≡mor (funext (B.antisym⊑ u v)) (B.antisym⊑ u v)
                           ; inf⊑ = B.inf⊑
                           ; ιn = λ u → B.ιn λ n → u n
                           ; out = λ u n → B.out u n 
                           }
      ; _∘_ = _∘ₘ_
      ; id = idm
      ; ε = record { D = λ _ → *↑ ; _⊑_ = λ _ → * ; ∐ = refl }
      ; _,_ = λ σ t → let open ωCPOMor in record
                   { D = λ γ → (D σ γ) ,Σ D t γ
                   ; _⊑_ = λ u → (σ ⊑ u) ,p (t ⊑ u)
                   ; ∐ = ap2 _,Σ_ (∐ σ) (∐ t) }
      ; p = record { D = π₁ ; _⊑_ = π₁ ; ∐ = refl }
      ; q = record { D = π₂ ; _⊑_ = π₂ ; ∐ = refl }
      ; _[_] = _∘ₘ_
      ; fix = λ {Γ}{A} t → let module A = ωCPO A
                               module Γ = ωCPO Γ
                               module t = ωCPOMor t
                           in record
                  { D = λ γ → A.∐ (f'{Γ} t γ) (p' t γ) 
                  ; _⊑_ = λ {γ}{γ'}u →
                        let lem : ∀ {γ γ'} → γ Γ.⊑ γ' → (n : ℕ) →  f'{Γ} t γ n A.⊑ f'{Γ} t γ' n
                            lem = λ {γ γ'}h → indℕp _ A.refl⊑ λ x → ωCPOMor._⊑_ t (h ,p x)
                        in A.ιn λ n → A.trans⊑ (lem u n) (A.out {f' t γ'}{λ n → p' t γ' n} A.refl⊑  n)
                  ; ∐ = λ {f}{p₁} → ap∐ {A}{(f' t (Γ.∐ f p₁))}{(λ x → A.∐ (f' t (f x)) (p' t (f x)))}
                                            {(p' t (Γ.∐ f p₁))}{(λ n →
                                            A.ιn
         (λ n₁ →
            A.trans⊑
            (indℕp (λ z → f' t (f n) z A.⊑ f' t (f (S n)) z) A.refl⊑
             (λ {n = n₂} x → t._⊑_ (p₁ n ,p x)) n₁)
            (A.out A.refl⊑ n₁)))}
              (funext λ {a} → {!!}) } 
      ; lam = λ {Γ}{A}t → let module t = ωCPOMor t
                              module Γ = ωCPO Γ
                              module A = ωCPO A
                              open ωCPO
                          in record { D = λ γ → record { D = λ a → t.D (γ ,Σ a)
                                                       ; _⊑_ = λ u → t._⊑_ (Γ.refl⊑ ,p u)
                                                       ; ∐ = λ{f}{p} → ap t.D (ap2 _,Σ_ (∐-const-lem {Γ}) refl)
                                                                       ◾ t.∐ }
                                    ; _⊑_ = λ {γ}{γ'} u {a} → t._⊑_ (u ,p A.refl⊑) 
                                    ; ∐ = λ {f}{p} → ≡mor (funext (λ {a} → ap t.D (ap2 _,Σ_ refl (∐-const-lem {A}))
                                                                     ◾ t.∐))
                                                             (ap t.D (ap2 _,Σ_ refl (∐-const-lem {A}))
                                                                     ◾ t.∐) }
      ; app = λ {Γ}{A}{B}t → let module t = ωCPOMor t
                                 module Γ = ωCPO Γ
                                 module A = ωCPO A
                                 module B = ωCPO B
                                 open ωCPOMor
                                 open ωCPO
                             in record { D = λ γa → D (t.D (π₁ γa)) (π₂ γa) 
                                       ; _⊑_ = λ {γ}{γ'} u → B.trans⊑ (t.D (π₁ γ) ⊑ π₂ u) (t._⊑_ (π₁ u) )
                                       ; ∐ = λ {f}{p} → ∐ (t.D (π₁ (∐ (Γ ×ω A) f p))) {(λ x → π₂ (f x))}{(λ n → π₂ (p n))}
                                                    ◾ ap∐ {B}
                                                    {λ x → D (t.D (Γ.∐ (λ x₁ → π₁ (f x₁)) (λ n → π₁ (p n)))) (π₂ (f x))}
                                                    {λ x → D (t.D (π₁ (f x))) (π₂ (f x))}
                                                    {λ n → t.D (Γ.∐ (λ x → π₁ (f x)) (λ n₁ → π₁ (p n₁))) ⊑ π₂ (p n)}
                                                    {λ n → B.trans⊑ (t.D (π₁ (f n)) ⊑ π₂ (p n)) (t._⊑_ (π₁ (p n)))}
                                                    {!!}}
      -- we need : t.D (Γ.∐ (λ x₁ → π₁ (f x₁)) (λ n → π₁ (p n))) ≡ t.D (π₁ (f a))
      ; zero = let module I' = Fix.OCPO.I ℕ in record
               { D = λ _ → I'.η O
               ; _⊑_ = λ _ → I'.refl⊑
               ; ∐ = I'.antisym⊑ (I'.out I'.refl⊑ O) (I'.ιn λ _ → I'.refl⊑) }
      ; suc = λ {Γ}n → let module I' = Fix.OCPO.I ℕ
                           module Γ = ωCPO Γ
                           open ωCPOMor in record
               { D = λ γ → fmap S (D n γ)
               ; _⊑_ = λ u → fmap⊑ (n ⊑ u)
                     ; ∐ = λ {f}{p} → ap (fmap S) (∐ n) ◾ fmap∐ {_}{_}{_}{_}{D n Γ.⊥}{D n Γ.⊥}
                                                          {f = D n ∘f f}{g = S}{p = λ m → n ⊑ p m} }
      ; isZero = λ {Γ}n → let open ωCPOMor
                              module Γ = ωCPO Γ
                          in record
               { D = λ γ → fmap isO (D n γ)
               ; _⊑_ = λ u → fmap⊑ (n ⊑ u)
               ; ∐ = λ {f}{p} → ap (fmap isO) (∐ n) ◾ fmap∐ {_}{_}{_}{_}{D n Γ.⊥}{D n Γ.⊥}
                                                      {f = D n ∘f f}{g = isO}{p = λ m → n ⊑ p m} }
      ; pred = {!!}
      ; true = let module I' = Fix.OCPO.I 𝟚 in record
               { D = λ _ → I'.η I
               ; _⊑_ = λ _ → I'.refl⊑
               ; ∐ = I'.antisym⊑ (I'.out I'.refl⊑ O) (I'.ιn λ _ → I'.refl⊑) }
      ; false = let module I' = Fix.OCPO.I 𝟚 in record
               { D = λ _ → I'.η O
               ; _⊑_ = λ _ → I'.refl⊑
               ; ∐ = I'.antisym⊑ (I'.out I'.refl⊑ O) (I'.ιn λ _ → I'.refl⊑) }
      ; ite = λ {Γ}{A} b u v → let open ωCPOMor
                                   module A = ωCPO A
                                   X : ωCPO.D Γ → ωCPO 𝟚
                                   X γ = record
                                      { D = A.D
                                      ; _⊑_ = A._⊑_
                                      ; η = λ b → if b then (D u γ) else (D v γ)
                                      ; ⊥ = A.⊥
                                      ; ∐ = A.∐
                                      ; refl⊑ = A.refl⊑
                                      ; trans⊑ = A.trans⊑
                                      ; antisym⊑ = A.antisym⊑
                                      ; inf⊑ = A.inf⊑
                                      ; ιn = A.ιn
                                      ; out = A.out
                                      }
                                in record
               { D = λ γ → let module X = ωCPO (X γ) in X.⟦ D b γ ⟧
               ; _⊑_ = λ {γ} {γ'} x → let module X = ωCPO (X γ)
                                          module X' = ωCPO (X γ')
                                      in A.trans⊑ (X.⟦ b ⊑ x ⟧⊑) {!!}  
               ; ∐ = {!!} }
      ; ass = refl
      ; idl = refl
      ; idr = refl
      ; ∙η = refl
      ; ▹β₁ = refl
      ; ▹β₂ = refl
      ; ▹η = refl
      ; [id] = refl
      ; [∘] = refl
      ; fixβ = {!!}
      ; ⇒β = refl
      ; ⇒η = {!!}
      ; isZeroβ₁ = ≡mor (funext fmapη) fmapη
      ; isZeroβ₂ = λ {Γ}{n} → let module n = ωCPOMor n in
                                  ≡mor (funext λ {a} → {!!}) {!!}
      ; predβ₁ = {!!}
      ; predβ₂ = {!!}
      ; iteβ₁ = {!!}
      ; iteβ₂ = {!!}
      ; lam[] = {!!}
      ; zero[] = refl
      ; suc[] = refl
      ; isZero[] = refl
      ; pred[] = {!!}
      ; true[] = refl
      ; false[] = refl
      ; ite[] = {!!}
      ; fix[] = {!!}
      }

\end{code}
