{-# OPTIONS --prop --rewriting #-}

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)
open import Full.I
open import Full.Algebra
open import Full.DepAlgebra
open import Full.Standard

module Full.Completeness where

open I
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

-- P-recNat : ∀ {Γ'} {A'} {u' : Tm ∙ A'} {v' : Tm (∙ ▹ A') A'}
--              {t' : Tm ∙ Nat} {Γ : Sub ∙ ∙ → Set} {A : Tm ∙ A' → Set} →
--            (A u') →
--            (Γ p ⊗ A q → A (v')) →
--            (↑p (⌜ norm (t') ⌝N ≡ t')) →
--            A (recNat u' v' t')
-- P-recNat = {!!}

Comp : DepAlgebra
Comp = record
         { Con        = λ Γ' → Sub ∙ Γ' → Set
         ; Sub        = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Ty         = λ A' → Tm ∙ A' → Set
         ; Tm         = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; _∘_        = λ σ δ x → σ (δ x)
         ; id         = idf
         ; ass        = refl
         ; idl        = refl
         ; idr        = refl
         ; _[_]       = λ t σ x → t (σ x)
         ; [id]       = refl
         ; [∘]        = refl
         ; ∙          = λ _ → ↑p 𝟙
         ; ε          = λ _ → *↑
         ; ∙η         = refl
         ; _▹_        = λ Γ A σ' → Γ (p ∘ σ') ⊗ A (q [ σ' ])
         ; _,_        = λ σ t x → σ x ,Σ t x
         ; p          = π₁
         ; q          = π₂
         ; ▹β₁        = refl
         ; ▹β₂        = refl
         ; ▹η         = refl
         ; Bool       = λ b → ↑p (⌜ norm b ⌝B ≡ b)
         ; true       = λ _ → refl↑
         ; false      = λ _ → refl↑
         ; ite        = λ {_ _ b' u' v' _ A} b u v {ν'} x → P-ite {P = A} ↓[ b x ]↓ (u x) (v x)
         ; true[]     = refl
         ; false[]    = refl
         ; ite[]      = refl
         ; iteβ₁      = refl
         ; iteβ₂      = refl
         ; Nat        = λ n → ↑p (⌜ norm n ⌝N ≡ n)
         ; zero       = λ _ → refl↑
         ; suc        = λ n x → ↑[ cong suc ↓[ n x ]↓ ]↑
         ; recNat     = {!!}
         ; Natβ₁      = refl
         ; Natβ₂      = refl
         ; zero[]     = refl
         ; suc[]      = refl
         ; recNat[]   = {!!}
         ; _⇒_        = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; lam        = λ t x y → t (x ,Σ y)
         ; app        = λ t xy → t (π₁ xy) (π₂ xy)
         ; ⇒β         = refl
         ; ⇒η         = refl
         ; lam[]      = {!refl!}
         ; Unit       = {!!}
         ; tt         = {!!}
         ; uη         = {!!}
         ; tt[]       = {!!}
         ; _×_        = {!!}
         ; ⟨_,_⟩      = {!!}
         ; proj₁      = {!!}
         ; proj₂      = {!!}
         ; ×β₁        = {!!}
         ; ×β₂        = {!!}
         ; ×η         = {!!}
         ; ⟨,⟩[]      = {!!}
         ; Prod       = {!!}
         ; pair       = {!!}
         ; recProd    = {!!}
         ; Prodβ      = {!!}
         ; pair[]     = {!!}
         ; recProd[]  = {!!}
         ; Empty      = {!!}
         ; absurd     = {!!}
         ; absurd[]   = {!!}
         ; _+_        = {!!}
         ; inl        = {!!}
         ; inr        = {!!}
         ; case       = {!!}
         ; +β₁        = {!!}
         ; +β₂        = {!!}
         ; inl[]      = {!!}
         ; inr[]      = {!!}
         ; case[]     = {!!}
         ; List       = {!!}
         ; nil        = {!!}
         ; cons       = {!!}
         ; recList    = {!!}
         ; Listβ₁     = {!!}
         ; Listβ₂     = {!!}
         ; nil[]      = {!!}
         ; cons[]     = {!!}
         ; recList[]  = {!!}
         ; Tree       = λ t → ↑p (⌜ norm t ⌝T ≡ t)
         ; leaf       = λ _ → refl↑
         ; node       = λ l r x → ↑[ cong-2 node ↓[ l x ]↓ ↓[ r x ]↓ ]↑
         ; recTree    = {!!}
         ; Treeβ₁     = {!!}
         ; Treeβ₂     = {!!}
         ; leaf[]     = {!!}
         ; node[]     = {!!}
         ; recTree[]  = {!!}
         ; Tree1      = {!!}
         ; leaf1      = {!!}
         ; node1      = {!!}
         ; recTree1   = {!!}
         ; Tree1β₁    = {!!}
         ; Tree1β₂    = {!!}
         ; leaf1[]    = {!!}
         ; node1[]    = {!!}
         ; recTree1[] = {!!}
         ; Tree2      = {!!}
         ; leaf2      = {!!}
         ; node2      = {!!}
         ; recTree2   = {!!}
         ; Tree2β₁    = {!!}
         ; Tree2β₂    = {!!}
         ; leaf2[]    = {!!}
         ; node2[]    = {!!}
         ; recTree2[] = {!!}
         }

compB : {b : Tm ∙ Bool} → ⌜ norm b ⌝B ≡ b
compB {b} = {!!}

compN : {n : Tm ∙ Nat} → ⌜ norm n ⌝N ≡ n
compN {n} = {!!}

compT : {t : Tm ∙ Tree} → ⌜ norm t ⌝T ≡ t
compT {t} = {!!}
