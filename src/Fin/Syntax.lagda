\begin{code}
{-# OPTIONS --prop --rewriting #-}
module Fin.Syntax where

open import Lib

infixl 6 _[_]
infixl 5 _▹_
infixr 6 _⇒_
infixl 5 _$_
infixl 7 _×o_
infixl 5 _,o_
infixl 6 _+o_

data Ty   : Set where
  _⇒_     : Ty → Ty → Ty
  _×o_    : Ty → Ty → Ty
  Unit    : Ty
  _+o_    : Ty → Ty → Ty
  Empty   : Ty

data Con  : Set where
  ◇       : Con
  _▹_     : Con → Ty → Con

data Var : Con → Ty → Set where
  vz        : ∀{Γ A} → Var (Γ ▹ A) A
  vs        : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

postulate
  Tm        : Con → Ty → Set

data Sub : Con → Con → Set where
  p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
  ⟨_⟩       : ∀{Γ A} → Tm Γ A → Sub Γ (Γ ▹ A)
  _⁺        : ∀{Γ Δ A} → (σ : Sub Δ Γ) → Sub (Δ ▹ A) (Γ ▹ A)

postulate
  var       : ∀{Γ A} → Var Γ A → Tm Γ A
  _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
  [p]       : ∀{Γ A B x} → var {Γ}{A} x [ p {A = B} ] ≡ var (vs x)
  vz[⟨⟩]    : ∀{Γ A t} → var (vz {Γ}{A}) [ ⟨ t ⟩ ] ≡ t
  vz[⁺]     : ∀{Γ Δ A σ} → var (vz {Γ}{A}) [ σ ⁺ ] ≡ var (vz {Δ}{A})
  vs[⟨⟩]    : ∀{Γ A B x t} → var (vs {Γ}{A}{B} x) [ ⟨ t ⟩ ] ≡ var x
  vs[⁺]     : ∀{Γ Δ A B x σ} → var (vs {Γ}{A}{B} x) [ σ ⁺ ] ≡ var x [ σ ] [ p {Δ} ]

  lam    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  _$_    : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  ⇒β     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ ⟨ u ⟩ ]
  ⇒η     : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → t ≡ lam (t [ p ] $ var vz)
  lam[]  : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{σ : Sub Δ Γ} → lam t [ σ ] ≡ lam (t [ σ ⁺ ])
  $[]    : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{σ : Sub Δ Γ} →
           (t $ u) [ σ ] ≡ (t [ σ ]) $ (u [ σ ])

  _,o_      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
  fst       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
  snd       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
  ×β₁       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst (u ,o v) ≡ u
  ×β₂       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd (u ,o v) ≡ v
  ×η        : ∀{Γ A B}{t : Tm Γ (A ×o B)} → t ≡ (fst t ,o snd t)
  ,[]       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
              (u ,o v) [ γ ] ≡ (u [ γ ] ,o v [ γ ])

  trivial   : ∀{Γ} → Tm Γ Unit
  Unitη     : ∀{Γ}{u : Tm Γ Unit} → u ≡ trivial

  inl       : ∀{Γ A B} → Tm Γ A → Tm Γ (A +o B)
  inr       : ∀{Γ A B} → Tm Γ B → Tm Γ (A +o B)
  caseo     : ∀{Γ A B C} → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ (A +o B) → Tm Γ C
  +β₁       : ∀{Γ A B C}{c₁ : Tm (Γ ▹ A) C}{c₂ : Tm (Γ ▹ B) C}{a : Tm Γ A} →
              caseo c₁ c₂ (inl a) ≡ c₁ [ ⟨ a ⟩ ]
  +β₂       : ∀{Γ A B C}{c₁ : Tm (Γ ▹ A) C}{c₂ : Tm (Γ ▹ B) C}{b : Tm Γ B} →
              caseo c₁ c₂ (inr b) ≡ c₂ [ ⟨ b ⟩ ]
  +η        : ∀{Γ A B C}{c : Tm (Γ ▹ A +o B) C} →
              c ≡ caseo (c [ p ⁺ ] [ ⟨ inl (var vz) ⟩ ] [ p ⁺ ]) (c [ p ⁺ ] [ ⟨ inr (var vz) ⟩ ] [ p ⁺ ]) (var vz)
  inl[]     : ∀{Γ A B}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → inl {B = B} a [ γ ] ≡ inl (a [ γ ])
  inr[]     : ∀{Γ A B}{b : Tm Γ B}{Δ}{γ : Sub Δ Γ} → inr {A = A} b [ γ ] ≡ inr (b [ γ ])
  case[]    : ∀{Γ A B C}{c₁ : Tm (Γ ▹ A) C}{c₂ : Tm (Γ ▹ B) C}{ab : Tm Γ (A +o B)}{Δ}{γ : Sub Δ Γ} →
              (caseo c₁ c₂ ab) [ γ ] ≡ caseo (c₁ [ γ ⁺ ]) (c₂ [ γ ⁺ ]) (ab [ γ ])

  absurd    : ∀{Γ A} → Tm Γ Empty → Tm Γ A
  Emptyη    : ∀{Γ A}{t : Tm (Γ ▹ Empty) A} → t ≡ absurd (var vz)
  absurd[]  : ∀{Γ A}{t : Tm Γ Empty}{Δ}{γ : Sub Δ Γ} → absurd {A = A} t [ γ ] ≡ absurd (t [ γ ])

-- these are provable
postulate
  [p][⁺]     : ∀{Γ A}{a : Tm Γ A}{B}{Δ}{γ : Sub Δ Γ} → a [ p {Γ}{B} ] [ γ ⁺ ] ≡ a [ γ ] [ p ]
  [p⁺][⁺⁺]   : ∀{Γ C A}{a : Tm (Γ ▹ C) A}{B}{Δ}{γ : Sub Δ Γ} → a [ p {Γ}{B} ⁺ ] [ γ ⁺ ⁺ ] ≡ a [ γ ⁺ ] [ p ⁺ ]
  [p][⟨⟩]    : ∀{Γ A}{a : Tm Γ A}{B}{b : Tm Γ B} → a [ p ] [ ⟨ b ⟩ ] ≡ a
  [p⁺][⟨⟩⁺]  : ∀{Γ A C}{a : Tm (Γ ▹ C) A}{B}{b : Tm Γ B} → a [ p ⁺ ] [ ⟨ b ⟩ ⁺ ] ≡ a
  [⟨⟩][]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → t [ ⟨ a ⟩ ] [ γ ] ≡ t [ γ ⁺ ] [ ⟨ a [ γ ] ⟩ ]
  [p⁺][⟨vz⟩] : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → t [ p ⁺ ] [ ⟨ var vz ⟩ ] ≡ t

def : {Γ : Con}{A B : Ty} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
def t u = u [ ⟨ t ⟩ ]

private
  Var' : ℕ → Con → Ty → Set
  Var' zero Γ A = Var Γ A
  Var' (suc n) Γ A = ∀ {B} → Var' n (Γ ▹ B) A

  Tm' : ℕ → Con → Ty → Set
  Tm' zero Γ A = Tm Γ A
  Tm' (suc n) Γ A = ∀ {B} → Tm' n (Γ ▹ B) A

  vs' : (n : ℕ) {Γ : Con} {A B : Ty} → Var' n Γ A → Var' n (Γ ▹ B) A
  vs' zero x = vs x
  vs' (suc n) x = vs' n x

  var' : (n : ℕ) {Γ : Con} {A : Ty} → Var' n Γ A → Tm' n Γ A
  var' zero x = var x
  var' (suc n) x = var' n x

  v' : (n : ℕ) {Γ : Con} {A : Ty} → Var' n (Γ ▹ A) A
  v' zero = vz
  v' (suc n) = vs' n (v' n)

v : (n : ℕ) {Γ : Con} {A : Ty} → Tm' n (Γ ▹ A) A
v n = var' n (v' n)

v0 : ∀{Γ A} → Tm (Γ ▹ A) A
v0 = var vz
v1 : ∀{Γ A B} → Tm (Γ ▹ A ▹ B) A
v1 = var (vs vz)
v2 : ∀{Γ A B C} → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = var (vs (vs vz))
v3 : ∀{Γ A B C D} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = var (vs (vs (vs vz)))
v4 : ∀{Γ A B C D E} → Tm (Γ ▹ A ▹ B ▹ C ▹ D ▹ E) A
v4 = var (vs (vs (vs (vs vz))))
\end{code}
