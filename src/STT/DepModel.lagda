\begin{code}
{-# OPTIONS --prop --rewriting #-}
module STT.DepModel where

open import Lib
open import STT.Syntax

record DepModel {i j} : Set (lsuc (i ⊔ j)) where
  infixl 6 _[_]∙
  infixl 5 _▹∙_
  infixl 7 _+o∙_
  infixr 6 _⇒∙_
  infixl 5 _$∙_

  field
    Ty∙        : Ty → Set i
    Nat∙       : Ty∙ Nat
    Bool∙      : Ty∙ Bool

    Con∙       : Con → Set i
    ◇∙         : Con∙ ◇
    _▹∙_       : ∀{Γ A} → Con∙ Γ → Ty∙ A → Con∙ (Γ ▹ A)

    Var∙       : ∀{Γ A} → Con∙ Γ → Ty∙ A → Var Γ A → Set j
    vz∙        : ∀{Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A} → Var∙ {Γ ▹ A} (Γ∙ ▹∙ A∙) A∙ vz
    vs∙        : ∀{Γ A B}{x : Var Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A}{B∙ : Ty∙ B} →
                 Var∙ Γ∙ A∙ x → Var∙ {Γ ▹ B} (Γ∙ ▹∙ B∙) A∙ (vs x)

    Tm∙        : ∀{Γ A} → Con∙ Γ → Ty∙ A → Tm Γ A → Set j

    Sub∙       : ∀{Δ Γ} → Con∙ Δ → Con∙ Γ → Sub Δ Γ → Set j
    p∙         : ∀{Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A} → Sub∙ (Γ∙ ▹∙ A∙) Γ∙ p
    ⟨_⟩∙       : ∀{Γ A}{t : Tm Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A} → Tm∙ Γ∙ A∙ t → Sub∙ Γ∙ (Γ∙ ▹∙ A∙) ⟨ t ⟩
    _⁺∙        : ∀{Γ Δ A}{σ : Sub Δ Γ}{Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}{A∙ : Ty∙ A}
                 (σ∙ : Sub∙ Δ∙ Γ∙ σ) → Sub∙ (Δ∙ ▹∙ A∙) (Γ∙ ▹∙ A∙) (σ ⁺)

    var∙       : ∀{Γ A}{x : Var Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A} → Var∙ Γ∙ A∙ x → Tm∙ Γ∙ A∙ (var x)
    _[_]∙      : ∀{Γ Δ A}{t : Tm Γ A}{σ : Sub Δ Γ}{Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}{A∙ : Ty∙ A} → 
                 Tm∙ Γ∙ A∙ t → Sub∙ Δ∙ Γ∙ σ → Tm∙ Δ∙ A∙ (t [ σ ])
    [p]∙       : ∀{Γ A B}{x : Var Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A}{B∙ : Ty∙ B}{x∙ : Var∙ Γ∙ A∙ x} →
                 (Tm∙ (Γ∙ ▹∙ B∙) A∙ ~) [p] (var∙ x∙ [ p∙ ]∙) (var∙ (vs∙ x∙))
    vz[⟨⟩]∙    : ∀{Γ A}{t : Tm Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A}{t∙ : Tm∙ Γ∙ A∙ t} →
                 (Tm∙ Γ∙ A∙ ~) vz[⟨⟩] (var∙ vz∙ [ ⟨ t∙ ⟩∙ ]∙) t∙
    vz[⁺]∙     : ∀{Γ Δ A}{σ : Sub Δ Γ}{Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}{A∙ : Ty∙ A}{σ∙ : Sub∙ Δ∙ Γ∙ σ} →
                 (Tm∙ (Δ∙ ▹∙ A∙) A∙ ~) vz[⁺] (var∙ vz∙ [ σ∙ ⁺∙ ]∙) (var∙ vz∙)
    vs[⟨⟩]∙    : ∀{Γ A B}{x : Var Γ A}{t : Tm Γ B}
                 {Γ∙ : Con∙ Γ}{A∙ : Ty∙ A}{B∙ : Ty∙ B}{x∙ : Var∙ Γ∙ A∙ x}{t∙ : Tm∙ Γ∙ B∙ t} →
                 (Tm∙ Γ∙ A∙ ~) vs[⟨⟩] (var∙ (vs∙ x∙) [ ⟨ t∙ ⟩∙ ]∙) (var∙ x∙)
    vs[⁺]∙     : ∀{Γ Δ A B}{x : Var Γ A}{σ : Sub Δ Γ}
                 {Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}{A∙ : Ty∙ A}{B∙ : Ty∙ B}{x∙ : Var∙ Γ∙ A∙ x}{σ∙ : Sub∙ Δ∙ Γ∙ σ} →
                 (Tm∙ (Δ∙ ▹∙ B∙) A∙ ~) vs[⁺] (var∙ (vs∙ x∙) [ σ∙ ⁺∙ ]∙) (var∙ x∙ [ σ∙ ]∙ [ p∙ ]∙)

    true∙      : ∀{Γ}{Γ∙ : Con∙ Γ} → Tm∙ Γ∙ Bool∙ true
    false∙     : ∀{Γ}{Γ∙ : Con∙ Γ} → Tm∙ Γ∙ Bool∙ false
    ite∙       : ∀{Γ A}{t : Tm Γ Bool}{u v : Tm Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A} →
                 Tm∙ Γ∙ Bool∙ t → Tm∙ Γ∙ A∙ u → Tm∙ Γ∙ A∙ v → Tm∙ Γ∙ A∙ (ite t u v)
    iteβ₁∙     : ∀{Γ A}{u v : Tm Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A}{u∙ : Tm∙ Γ∙ A∙ u}{v∙ : Tm∙ Γ∙ A∙ v} →
                 (Tm∙ Γ∙ A∙ ~) iteβ₁ (ite∙ true∙ u∙ v∙) u∙
    iteβ₂∙     : ∀{Γ A}{u v : Tm Γ A}{Γ∙ : Con∙ Γ}{A∙ : Ty∙ A}{u∙ : Tm∙ Γ∙ A∙ u}{v∙ : Tm∙ Γ∙ A∙ v} →
                 (Tm∙ Γ∙ A∙ ~) iteβ₂ (ite∙ false∙ u∙ v∙) v∙
    true[]∙    : ∀{Γ Δ}{σ : Sub Δ Γ}{Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}{σ∙ : Sub∙ Δ∙ Γ∙ σ} →
                 (Tm∙ Δ∙ Bool∙ ~) true[] (true∙ [ σ∙ ]∙) true∙
    false[]∙   : ∀{Γ Δ}{σ : Sub Δ Γ}{Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}{σ∙ : Sub∙ Δ∙ Γ∙ σ} →
                 (Tm∙ Δ∙ Bool∙ ~) false[] (false∙ [ σ∙ ]∙) false∙
    ite[]∙     : ∀{Γ Δ A}{t : Tm Γ Bool}{u v : Tm Γ A}{σ : Sub Δ Γ}
                 {Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}{A∙ : Ty∙ A}{t∙ : Tm∙ Γ∙ Bool∙ t}
                 {u∙ : Tm∙ Γ∙ A∙ u}{v∙ : Tm∙ Γ∙ A∙ v}{σ∙ : Sub∙ Δ∙ Γ∙ σ} →
                 (Tm∙ Δ∙ A∙ ~) ite[] (ite∙ t∙ u∙ v∙ [ σ∙ ]∙) (ite∙ (t∙ [ σ∙ ]∙) (u∙ [ σ∙ ]∙) (v∙ [ σ∙ ]∙))
    
    num∙       : ∀{Γ}{Γ∙ : Con∙ Γ}(n : ℕ) → Tm∙ Γ∙ Nat∙ (num n)
    isZero∙    : ∀{Γ}{t : Tm Γ Nat}{Γ∙ : Con∙ Γ} → Tm∙ Γ∙ Nat∙ t → Tm∙ Γ∙ Bool∙ (isZero t)
    _+o∙_      : ∀{Γ}{u v : Tm Γ Nat}{Γ∙ : Con∙ Γ} → Tm∙ Γ∙ Nat∙ u → Tm∙ Γ∙ Nat∙ v → Tm∙ Γ∙ Nat∙ (u +o v)
    isZeroβ₁∙  : ∀{Γ}{Γ∙ : Con∙ Γ} → (Tm∙ Γ∙ Bool∙ ~) isZeroβ₁ (isZero∙ (num∙ 0)) true∙
    isZeroβ₂∙  : ∀{Γ}{Γ∙ : Con∙ Γ}{n : ℕ} → (Tm∙ Γ∙ Bool∙ ~) isZeroβ₂ (isZero∙ (num∙ (1 + n))) false∙
    +β∙        : ∀{Γ}{Γ∙ : Con∙ Γ}{m n : ℕ} → (Tm∙ Γ∙ Nat∙ ~) +β (num∙ m +o∙ num∙ n) (num∙ (m + n))
    num[]∙     : ∀{Γ Δ}{σ : Sub Δ Γ}{Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}{σ∙ : Sub∙ Δ∙ Γ∙ σ}{n : ℕ} →
                 (Tm∙ Δ∙ Nat∙ ~) num[] (num∙ n [ σ∙ ]∙) (num∙ n)
    isZero[]∙  : ∀{Γ Δ}{t : Tm Γ Nat}{σ : Sub Δ Γ}{Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}{t∙ : Tm∙ Γ∙ Nat∙ t}{σ∙ : Sub∙ Δ∙ Γ∙ σ} →
                 (Tm∙ Δ∙ Bool∙ ~) isZero[] (isZero∙ t∙ [ σ∙ ]∙) (isZero∙ (t∙ [ σ∙ ]∙))
    +[]∙       : ∀{Γ Δ}{u v : Tm Γ Nat}{σ : Sub Δ Γ}{Γ∙ : Con∙ Γ}{Δ∙ : Con∙ Δ}
                 {u∙ : Tm∙ Γ∙ Nat∙ u}{v∙ : Tm∙ Γ∙ Nat∙ v}{σ∙ : Sub∙ Δ∙ Γ∙ σ} →
                 (Tm∙ Δ∙ Nat∙ ~) +[] ((u∙ +o∙ v∙) [ σ∙ ]∙) ((u∙ [ σ∙ ]∙) +o∙ (v∙ [ σ∙ ]∙))
                 
    _⇒∙_       : ∀{A} → Ty∙ A → ∀{B} → Ty∙ B → Ty∙ (A ⇒ B)
    lam∙       : ∀{Γ}{Γ∙ : Con∙ Γ}{A}{A∙ : Ty∙ A}{B}{B∙ : Ty∙ B}{t : Tm (Γ ▹ A) B} → Tm∙ (Γ∙ ▹∙ A∙) B∙ t →
                 Tm∙ Γ∙  (A∙ ⇒∙ B∙) (lam t)
    _$∙_       : ∀{Γ}{Γ∙ : Con∙ Γ}{A}{A∙ : Ty∙ A}{B}{B∙ : Ty∙ B}{t : Tm Γ (A ⇒ B)} → Tm∙ Γ∙ (A∙ ⇒∙ B∙) t →
                 {u : Tm Γ A} → Tm∙ Γ∙ A∙ u → Tm∙ Γ∙ B∙ (t $ u)
    ⇒β∙        : ∀{Γ}{Γ∙ : Con∙ Γ}{A}{A∙ : Ty∙ A}{B}{B∙ : Ty∙ B}{t : Tm (Γ ▹ A) B}{t∙ : Tm∙ (Γ∙ ▹∙ A∙) B∙ t}
                 {u : Tm Γ A}{u∙ : Tm∙ Γ∙ A∙ u} → (Tm∙ Γ∙ B∙ ~) ⇒β (lam∙ t∙ $∙ u∙) (t∙ [ ⟨ u∙ ⟩∙ ]∙)
    ⇒η∙        : ∀{Γ}{Γ∙ : Con∙ Γ}{A}{A∙ : Ty∙ A}{B}{B∙ : Ty∙ B}{t : Tm Γ (A ⇒ B)}{t∙ : Tm∙ Γ∙ (A∙ ⇒∙ B∙) t} →
                 (Tm∙ Γ∙ (A∙ ⇒∙ B∙) ~) ⇒η t∙ (lam∙ (t∙ [ p∙ ]∙ $∙ var∙ vz∙))
    lam[]∙     : ∀{Γ}{Γ∙ : Con∙ Γ}{A}{A∙ : Ty∙ A}{B}{B∙ : Ty∙ B}{t : Tm (Γ ▹ A) B}{t∙ : Tm∙ (Γ∙ ▹∙ A∙) B∙ t}
                 {Δ}{Δ∙ : Con∙ Δ}{σ : Sub Δ Γ}{σ∙ : Sub∙ Δ∙ Γ∙ σ} →
                 (Tm∙ Δ∙ (A∙ ⇒∙ B∙) ~) lam[] (lam∙ t∙ [ σ∙ ]∙) (lam∙ (t∙ [ σ∙ ⁺∙ ]∙))
    $[]∙       : ∀{Γ}{Γ∙ : Con∙ Γ}{A}{A∙ : Ty∙ A}{B}{B∙ : Ty∙ B}{t : Tm Γ (A ⇒ B)}{t∙ : Tm∙ Γ∙ (A∙ ⇒∙ B∙) t}
                 {u : Tm Γ A}{u∙ : Tm∙ Γ∙ A∙ u}{Δ}{Δ∙ : Con∙ Δ}{σ : Sub Δ Γ}{σ∙ : Sub∙ Δ∙ Γ∙ σ} →
                 (Tm∙ Δ∙ B∙ ~) $[] ((t∙ $∙ u∙) [ σ∙ ]∙) ((t∙ [ σ∙ ]∙) $∙ (u∙ [ σ∙ ]∙))

  ⟦_⟧T : (A : Ty) → Ty∙ A
  ⟦ Nat ⟧T = Nat∙
  ⟦ Bool ⟧T = Bool∙
  ⟦ A ⇒ B ⟧T = ⟦ A ⟧T ⇒∙ ⟦ B ⟧T 

  ⟦_⟧C : (Γ : Con) → Con∙ Γ
  ⟦ ◇ ⟧C = ◇∙
  ⟦ Γ ▹ A ⟧C = ⟦ Γ ⟧C ▹∙ ⟦ A ⟧T

  ⟦_⟧v : ∀{Γ A}(x : Var Γ A)  → Var∙ ⟦ Γ ⟧C ⟦ A ⟧T x
  ⟦ vz ⟧v = vz∙
  ⟦ vs x ⟧v = vs∙ ⟦ x ⟧v

  postulate
    ⟦_⟧t : ∀{Γ A}(t : Tm Γ A)  → Tm∙  ⟦ Γ ⟧C ⟦ A ⟧T t

  ⟦_⟧s : ∀{Γ Δ}(σ : Sub Δ Γ) → Sub∙ ⟦ Δ ⟧C  ⟦ Γ ⟧C  σ
  ⟦ p ⟧s = p∙
  ⟦ ⟨ t ⟩ ⟧s = ⟨ ⟦ t ⟧t ⟩∙
  ⟦ σ ⁺ ⟧s = ⟦ σ ⟧s ⁺∙

  postulate
    ⟦var⟧     : ∀{Γ A}  {x : Var Γ A}              → ⟦ var x   ⟧t ≈ var∙ ⟦ x ⟧v
    ⟦[]⟧      : ∀{Γ Δ A}{t :  Tm Γ A}{σ : Sub Δ Γ} → ⟦ t [ σ ] ⟧t ≈ ⟦ t ⟧t [ ⟦ σ ⟧s ]∙
    {-# REWRITE ⟦var⟧ ⟦[]⟧ #-}

    ⟦true⟧    : ∀{Γ} → ⟦ true  {Γ} ⟧t ≈ true∙
    ⟦false⟧   : ∀{Γ} → ⟦ false {Γ} ⟧t ≈ false∙
    ⟦ite⟧     : ∀{Γ A}{t : Tm Γ Bool}{u v : Tm Γ A} → 
                ⟦ ite {Γ} t u v ⟧t ≈ ite∙ ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦num⟧     : ∀{Γ n}               → ⟦ num {Γ} n ⟧t ≈ num∙ n
    ⟦isZero⟧  : ∀{Γ}{  t : Tm Γ Nat} → ⟦ isZero t  ⟧t ≈ isZero∙ ⟦ t ⟧t
    ⟦+⟧       : ∀{Γ}{u v : Tm Γ Nat} → ⟦ u +o v    ⟧t ≈ ⟦ u ⟧t +o∙ ⟦ v ⟧t
    {-# REWRITE ⟦num⟧ ⟦isZero⟧ ⟦+⟧ #-}

    ⟦lam⟧     : ∀{Γ A B t} → ⟦ lam {Γ}{A}{B} t ⟧t ≈ lam∙ ⟦ t ⟧t
    ⟦$⟧       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u} → ⟦ t $ u ⟧t ≈ ⟦ t ⟧t $∙ ⟦ u ⟧t
    {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}
\end{code}
