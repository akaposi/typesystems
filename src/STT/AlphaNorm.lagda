The following are the $\alpha$-normal forms.

\begin{code}
{-# OPTIONS --prop --rewriting --no-termination-check #-}
module STT.AlphaNorm where

open import Lib
open import STT.Syntax

data Tm*     : (Γ : Con)(A : Ty) → Tm Γ A → Prop where
  var*       : ∀{Γ A}(x : Var Γ A) → Tm* Γ A (var x)

  true*      : ∀{Γ} → Tm* Γ Bool true
  false*     : ∀{Γ} → Tm* Γ Bool false
  ite*       : ∀{Γ A t u v} → Tm* Γ Bool t → Tm* Γ A u → Tm* Γ A v → Tm* Γ A (ite t u v)

  num*       : ∀{Γ}(n : ℕ) → Tm* Γ Nat (num n)
  isZero*    : ∀{Γ}{t : Tm Γ Nat} → Tm* Γ Nat t → Tm* Γ Bool (isZero t)
  _+o*_      : ∀{Γ}{u v : Tm Γ Nat} → Tm* Γ Nat u → Tm* Γ Nat v → Tm* Γ Nat (u +o v)

  lam*       : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → Tm* (Γ ▹ A) B t → Tm* Γ  (A ⇒ B) (lam t)
  _$*_       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → Tm* Γ (A ⇒ B) t →
               {u : Tm Γ A} → Tm* Γ A u → Tm* Γ B (t $ u)

data Wk  : (Δ Γ : Con) → Sub Δ Γ → Set where
  p      : ∀{Γ A} → Wk (Γ ▹ A) Γ p
  _⁺     : ∀{Δ Γ γ A} → Wk Δ Γ γ → Wk (Δ ▹ A) (Γ ▹ A) (γ ⁺)

_[_]vWk : ∀{Γ Δ A γ} → Var Γ A → Wk Δ Γ γ → Var Δ A
x [ p ]vWk = vs x
vz [ γ* ⁺ ]vWk = vz
vs x [ γ* ⁺ ]vWk = x [ γ* ]vWk [ p ]vWk

var[] : ∀{Γ Δ A γ}{x : Var Γ A}{γ* : Wk Δ Γ γ} → var x [ γ ] ≡ var (x [ γ* ]vWk)
var[] {x = x} {p} = [p]
var[] {x = vz} {γ* ⁺} = vz[⁺]
var[] {x = vs x} {γ* ⁺} = vs[⁺] ◾ cong (_[ p ]) (var[] {x = x}{γ*}) ◾ [p]

_[_]Wk : ∀{Γ A a} → Tm* Γ A a → ∀{Δ γ} → Wk Δ Γ γ → Tm* Δ A (a [ γ ])
var* x [ γ* ]Wk = transpP (Tm* _ _) (var[] {x = x}{γ*} ⁻¹) (var* (x [ γ* ]vWk))
true*  [ γ* ]Wk = transpP (Tm* _ Bool) (true[] ⁻¹) true*
false* [ γ* ]Wk = transpP (Tm* _ Bool) (false[] ⁻¹) false*
ite* t* u* v* [ γ* ]Wk = transpP (Tm* _ _) (ite[] ⁻¹) (ite* (t* [ γ* ]Wk) (u* [ γ* ]Wk) (v* [ γ* ]Wk))
num* n [ γ* ]Wk = transpP (Tm* _ Nat) (num[] ⁻¹) (num* n)
isZero* t* [ γ* ]Wk = transpP (Tm* _ _) (isZero[] ⁻¹) (isZero* (t* [ γ* ]Wk))
(u* +o* v*) [ γ* ]Wk = transpP (Tm* _ _) (+[] ⁻¹) ((u* [ γ* ]Wk) +o* (v* [ γ* ]Wk))
lam* t* [ γ* ]Wk = transpP (Tm* _ _) (lam[] ⁻¹) (lam* (t* [ γ* ⁺ ]Wk))
(t* $* u*) [ γ* ]Wk = transpP (Tm* _ _) ($[] ⁻¹) ((t* [ γ* ]Wk) $* (u* [ γ* ]Wk))

data Sub* : (Δ Γ : Con) → Sub Δ Γ → Set where
  ⟨_⟩     : ∀{Γ A a} → Tm* Γ A a → Sub* Γ (Γ ▹ A) ⟨ a ⟩
  _⁺      : ∀{Δ Γ γ A} → Sub* Δ Γ γ → Sub* (Δ ▹ A) (Γ ▹ A) (γ ⁺)

_[_]v* : ∀{Γ Δ A γ}(x : Var Γ A) → Sub* Δ Γ γ → Tm* Δ A (var x [ γ ])
vz [ ⟨ u* ⟩ ]v* = transpP (Tm* _ _) (vz[⟨⟩] ⁻¹) u*
vz [ γ ⁺ ]v* = transpP (Tm* _ _) (vz[⁺] ⁻¹) (var* vz)
vs x [ ⟨ u* ⟩ ]v* = transpP (Tm* _ _) (vs[⟨⟩] ⁻¹) (var* x)
vs x [ γ ⁺ ]v* = transpP (Tm* _ _) (vs[⁺] ⁻¹) (x [ γ ]v* [ p ]Wk)

_[_]* : ∀{Γ A a} → Tm* Γ A a → ∀{Δ γ} → Sub* Δ Γ γ → Tm* Δ A (a [ γ ])
var* x [ γ* ]* = x [ γ* ]v*
true* [ γ* ]* = transpP (Tm* _ _) (true[] ⁻¹) true*
false* [ γ* ]* = transpP (Tm* _ _) (false[] ⁻¹) false*
ite* b* a₀* a₁* [ γ* ]* = {!!}
num* n [ γ* ]* = {!!}
isZero* t* [ γ* ]* = {!!}
(t* +o* t*₁) [ γ* ]* = {!!}
lam* t* [ γ* ]* = {!!}
(t* $* a*) [ γ* ]* = {!!}

-- D : DepModel
-- D = ?

norm : ∀{Γ}{A}(t : Tm Γ A) → Tm* Γ A t
norm t = {!!}

-- norms : Sub-ot normalizalni, csak nem Sub*-ra, hanem vagy Wk-ra, vagy Sub*-ra

_++_ : Con → Con → Con
Γ ++ ◇ = Γ
Γ ++ (Θ ▹ A) = (Γ ++ Θ) ▹ A
_⁺^_ : ∀{Δ Γ} → Sub Δ Γ → (Θ : Con) → Sub (Δ ++ Θ) (Γ ++ Θ)
γ ⁺^ ◇       = γ
γ ⁺^ (Θ ▹ A) = (γ ⁺^ Θ) ⁺
-- todo infix

[p][⁺]Wk : ∀{Γ Θ A}{a : Tm (Γ ++ Θ) A}(a* : Tm* (Γ ++ Θ) A a){B}{Δ}{γ : Sub Δ Γ}(γwk : Wk Δ Γ γ) → a [ p {Γ}{B} ⁺^ Θ ] [ (γ ⁺) ⁺^ Θ ] ≡ a [ γ ⁺^ Θ ] [ p ⁺^ Θ ]
[p][⁺]Wk (var* x) γwk = {!!}
[p][⁺]Wk true* γwk = {!!}
[p][⁺]Wk false* γwk = {!!}
[p][⁺]Wk (ite* a* a*₁ a*₂) γwk = {!!}
[p][⁺]Wk (num* n) γwk = {!!}
[p][⁺]Wk (isZero* n*) γwk = {!!}
[p][⁺]Wk (a* +o* a*₁) γwk = {!!}
[p][⁺]Wk (lam* b*) γwk = cong (_[ (_ ⁺) ⁺^ _ ]) lam[] ◾ lam[] ◾ cong lam ([p][⁺]Wk b* γwk) ◾ lam[] ⁻¹ ◾ cong (_[ p ⁺^ _ ]) (lam[] ⁻¹)
[p][⁺]Wk (a* $* a*₁) γwk = {!!}

[p][⁺]* : ∀{Γ A}{a : Tm Γ A}(a* : Tm* Γ A a){B}{Δ}{γ : Sub Δ Γ}(γwk : Sub* Δ Γ γ) → a [ p {Γ}{B} ] [ γ ⁺ ] ≡ a [ γ ] [ p ]
[p][⁺]* = {!!}

-- ezekbol kihozni:

[p][⁺]' : ∀{Γ A}{a : Tm Γ A}{B}{Δ}{γ : Sub Δ Γ} → a [ p {Γ}{B} ] [ γ ⁺ ] ≡ a [ γ ] [ p ]
[p][⁺]' = {!!}

-- [p⁺][⁺⁺], [p⁺⁺][⁺⁺⁺]

-- and the other equations

-- ∀ γ γ' → (∀ t. t[γ] = t[γ']) → ∀ t . t[γ⁺] = t[γ'⁺]
-- ∀γ,δ,γ',δ'.(∀ t . t[γ][δ] = t[γ'][δ']) → ∀ t. t[γ⁺][δ⁺] = t[γ'⁺][δ'⁺]
-- lehet ezt altalanositani akarmilyen hosszu Sub-sorozatra

\end{code}
