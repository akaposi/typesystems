\begin{code}
{-# OPTIONS --prop --rewriting #-}
module STT.Canon where

open import Lib
open import STT.Syntax
open import STT.DepModel

Tms : Con → Con → Set
Tms Δ ◇       = Lift ⊤
Tms Δ (Γ ▹ A) = Tms Δ Γ × Tm Δ A

wk : ∀{Γ Δ} → Tms Δ Γ → ∀{A} → Tms (Δ ▹ A) Γ
wk {◇}     γ       = _
wk {Γ ▹ A} (γ , a) = wk γ , a [ p ]

_⁺s : ∀{Γ Δ} → Tms Δ Γ → ∀{A} → Tms (Δ ▹ A) (Γ ▹ A)
γ ⁺s = wk γ , var vz

_[p^_] : ∀{A} → Tm ◇ A → (Δ : Con) → Tm Δ A
a [p^ ◇ ]     = a
a [p^ Δ ▹ B ] = a [p^ Δ ] [ p ]

data Sub* : Con → Con → Set where
  nil : ∀{Γ} → Sub* Γ Γ
  _∷_ : ∀{Γ Δ Θ} → Sub* Δ Γ → Sub Θ Δ → Sub* Θ Γ

_⁺* : ∀{Γ Δ} → Sub* Δ Γ → ∀{A} → Sub* (Δ ▹ A) (Γ ▹ A)
nil      ⁺* = nil
(γ* ∷ δ) ⁺* = (γ* ⁺*) ∷ (δ ⁺)

p^_ : (Γ : Con) → Sub* Γ ◇
p^ ◇ = nil
p^ (Γ ▹ A) = (p^ Γ) ∷ p

⌜_⌝ : ∀{Γ Δ} → Tms Δ Γ → Sub* Δ Γ
⌜_⌝ {◇}     _       = p^ _
⌜_⌝ {Γ ▹ A} (γ , a) = (⌜ γ ⌝ ⁺*) ∷ ⟨ a ⟩

_[_]* : ∀{Γ A} → Tm Γ A → ∀{Δ} → Sub* Δ Γ → Tm Δ A
a [ nil    ]* = a
a [ γ* ∷ δ ]* = a [ γ* ]* [ δ ]



-- _∘_
-- wk
-- id
-- p
-- [∘]s
-- [id]s
-- ass
-- idl
-- idr


{-
data Noc : Set where
  ◆   : Noc
  _◃_ : Ty → Noc → Noc


_++_ : Con → Noc → Con
Γ ++ ◆       = Γ
Γ ++ (A ◃ Ω) = (Γ ▹ A) ++ Ω

_⁺^_ : ∀{Γ Δ} → Sub Δ Γ → (Ω : Noc) → Sub (Δ ++ Ω) (Γ ++ Ω)
γ ⁺^ ◆       = γ
γ ⁺^ (A ◃ Ω) = (γ ⁺) ⁺^ Ω 

[p^] : ∀ Ω {A} → Tm (◇ ++ Ω) A → (Δ : Con) → Tm (Δ ++ Ω) A
[p^] Ω t ◇       = t
[p^] Ω t (Δ ▹ A) = [p^] (A ◃ Ω) (t [ p ⁺^ Ω ]) Δ
syntax [p^] Ω t Δ = t [ p^ Δ ⁺^ Ω ]

[]s : ∀{Γ} Ω {Δ A} → Tm (Γ ++ Ω) A → Tms Δ Γ → Tm (Δ ++ Ω) A
[]s {◇}     Ω t _       = t [ p^ _ ⁺^ Ω ]
[]s {Γ ▹ A} Ω t (γ , a) = []s {Γ} (A ◃ Ω) t γ [ ⟨ a ⟩ ⁺^ Ω ]
syntax []s Ω t γ = t [ γ ⁺^ Ω ]s

vz[]s : ∀{Γ A Δ}{γ : Tms Δ Γ}{a : Tm Δ A} → var vz [ γ , a ⁺^ ◆ ]s ≡ a
vz[]s {◇}     = {!!}
vz[]s {Γ ▹ B} = {!!}

true[p^] : ∀{Γ Ω} → true [ p^ Γ ⁺^ Ω ] ≡ true
true[p^] {◇} = refl
true[p^] {Γ ▹ A}{Ω} = cong (λ z → [p^] (A ◃ Ω) z Γ) true[] ◾ true[p^] {Γ}{A ◃ Ω}
true[]s : ∀{Γ Ω Δ}{γ : Tms Δ Γ} → true [ γ ⁺^ Ω ]s ≡ true
true[]s {◇}{Ω}{Δ} = true[p^] {Δ}{Ω}
true[]s {Γ ▹ A}{Ω}{Δ}{γ , a} = cong (_[ ⟨ a ⟩ ⁺^ Ω ]) (true[]s {Γ}{A ◃ Ω}{Δ}{γ}) ◾ true[]

false[p^] : ∀{Γ Ω} → false [ p^ Γ ⁺^ Ω ] ≡ false
false[p^] {◇} = refl
false[p^] {Γ ▹ A}{Ω} = cong (λ z → [p^] (A ◃ Ω) z Γ) false[] ◾ false[p^] {Γ}{A ◃ Ω}
false[]s : ∀{Γ Ω Δ}{γ : Tms Δ Γ} → false [ γ ⁺^ Ω ]s ≡ false
false[]s {◇}{Ω}{Δ} = false[p^] {Δ}{Ω}
false[]s {Γ ▹ A}{Ω}{Δ}{γ , a} = cong (_[ ⟨ a ⟩ ⁺^ Ω ]) (false[]s {Γ}{A ◃ Ω}{Δ}{γ}) ◾ false[]

ite[p^] : ∀{Γ Ω B t u}{v : Tm (◇ ++ Ω) B} → (ite t u v) [ p^ Γ ⁺^ Ω ] ≡ ite (t [ p^ Γ ⁺^ Ω ]) (u [ p^ Γ ⁺^ Ω ]) (v [ p^ Γ ⁺^ Ω ])
ite[p^] {◇} = refl
ite[p^] {Γ ▹ A}{Ω} = cong (λ z → [p^] (A ◃ Ω) z Γ) ite[] ◾ ite[p^] {Γ}{A ◃ Ω}
ite[]s : ∀{Γ Ω B t u}{v : Tm (Γ ++ Ω) B}{Δ}{γ : Tms Δ Γ} → (ite t u v) [ γ ⁺^ Ω ]s ≡ ite (t [ γ ⁺^ Ω ]s) (u [ γ ⁺^ Ω ]s) (v [ γ ⁺^ Ω ]s)
ite[]s {◇}{Ω}{_}{_}{_}{_}{Δ} = ite[p^] {Δ}{Ω}
ite[]s {Γ ▹ A}{Ω}{_}{_}{_}{_}{Δ}{γ , a} = cong (_[ ⟨ a ⟩ ⁺^ Ω ]) (ite[]s {Γ}{A ◃ Ω}{_}{_}{_}{_}{Δ}{γ}) ◾ ite[]

num[p^] : ∀{Γ Ω n} → num n [ p^ Γ ⁺^ Ω ] ≡ num n
num[p^] {◇} = refl
num[p^] {Γ ▹ A}{Ω} = cong (λ z → [p^] (A ◃ Ω) z Γ) num[] ◾ num[p^] {Γ}{A ◃ Ω}
num[]s : ∀{Γ Ω n Δ}{γ : Tms Δ Γ} → num n [ γ ⁺^ Ω ]s ≡ num n
num[]s {◇}{Ω}{n}{Δ} = num[p^] {Δ}{Ω}
num[]s {Γ ▹ A}{Ω}{n}{Δ}{γ , a} = cong (_[ ⟨ a ⟩ ⁺^ Ω ]) (num[]s {Γ}{A ◃ Ω}{n}{Δ}{γ}) ◾ num[]

isZero[p^] : ∀{Γ Ω t} → isZero t [ p^ Γ ⁺^ Ω ] ≡ isZero (t [ p^ Γ ⁺^ Ω ])
isZero[p^] {◇} = refl
isZero[p^] {Γ ▹ A}{Ω} = cong (λ z → [p^] (A ◃ Ω) z Γ) isZero[] ◾ isZero[p^] {Γ}{A ◃ Ω}
isZero[]s : ∀{Γ Ω t Δ}{γ : Tms Δ Γ} → isZero t [ γ ⁺^ Ω ]s ≡ isZero (t [ γ ⁺^ Ω ]s)
isZero[]s {◇}{Ω}{_}{Δ} = isZero[p^] {Δ}{Ω}
isZero[]s {Γ ▹ A}{Ω}{_}{Δ}{γ , a} = cong (_[ ⟨ a ⟩ ⁺^ Ω ]) (isZero[]s {Γ}{A ◃ Ω}{_}{Δ}{γ}) ◾ isZero[]

+[p^] : ∀{Γ Ω u v} → (u +o v) [ p^ Γ ⁺^ Ω ] ≡ (u [ p^ Γ ⁺^ Ω ]) +o (v [ p^ Γ ⁺^ Ω ])
+[p^] {◇} = refl
+[p^] {Γ ▹ A}{Ω} = cong (λ z → [p^] (A ◃ Ω) z Γ) +[] ◾ +[p^] {Γ}{A ◃ Ω}
+[]s : ∀{Γ Ω u v Δ}{γ : Tms Δ Γ} → (u +o v) [ γ ⁺^ Ω ]s ≡ (u [ γ ⁺^ Ω ]s) +o (v [ γ ⁺^ Ω ]s)
+[]s {◇}{Ω}{_}{_}{Δ} = +[p^] {Δ}{Ω}
+[]s {Γ ▹ A}{Ω}{_}{_}{Δ}{γ , a} = cong (_[ ⟨ a ⟩ ⁺^ Ω ]) (+[]s {Γ}{A ◃ Ω}{_}{_}{Δ}{γ}) ◾ +[]

$[p^] : ∀{Γ Ω A B}{t : Tm (◇ ++ Ω) (A ⇒ B)}{u} → (t $ u) [ p^ Γ ⁺^ Ω ] ≡ (t [ p^ Γ ⁺^ Ω ]) $ (u [ p^ Γ ⁺^ Ω ])
$[p^] {◇} = refl
$[p^] {Γ ▹ A}{Ω} = cong (λ z → [p^] (A ◃ Ω) z Γ) $[] ◾ $[p^] {Γ}{A ◃ Ω}
$[]s : ∀{Γ Ω A B}{t : Tm (Γ ++ Ω) (A ⇒ B)}{u}{Δ}{γ : Tms Δ Γ} → (t $ u) [ γ ⁺^ Ω ]s ≡ (t [ γ ⁺^ Ω ]s) $ (u [ γ ⁺^ Ω ]s)
$[]s {◇}{Ω}{_}{_}{_}{_}{Δ} = $[p^] {Δ}{Ω}
$[]s {Γ ▹ A}{Ω}{_}{_}{_}{_}{Δ}{γ , a} = cong (_[ ⟨ a ⟩ ⁺^ Ω ]) ($[]s {Γ}{A ◃ Ω}{_}{_}{_}{_}{Δ}{γ}) ◾ $[]

lam[p^] : ∀{Γ Ω A B}{t : Tm ((◇ ++ Ω) ▹ A) B} → (lam t) [ p^ Γ ⁺^ Ω ] ≡ lam {!t [ p^ Γ ⁺^ Ω ]!}
lam[p^] {◇} = {!!}
lam[p^] {Γ ▹ A}{Ω} = cong (λ z → [p^] (A ◃ Ω) z Γ) lam[] ◾ {!!} -- lam[p^] {Γ}{A ◃ Ω}
-}
{-
Canon : DepModel
Canon = record
  { Ty∙ = λ A → Σ Set λ A∙ → A∙ → Tm ◇ A -- equivalent to PA : λ A → Tm ◇ A → Set
  ; Nat∙ = ℕ , num
  ; Bool∙ = 𝟚 , λ b → if b then true else false
  ; Con∙ = λ Γ → Σ Set λ Γ∙ → Γ∙ → Tms ◇ Γ -- equivalent to PΓ : λ Γ → Tms ◇ Γ → Set
  ; ◇∙ = Lift ⊤ , _
  ; _▹∙_ = λ (Γ∙ , πΓ) (A∙ , πA) → (Γ∙ × A∙) , λ (γ , a) → πΓ γ , πA a
  ; Var∙ = λ {Γ}{A} (Γ∙ , πΓ) (A∙ , πA) x → (γ∙ : Γ∙) → Σsp A∙ λ a∙ → πA a∙ ≡ (var x [ πΓ γ∙ ⁺^ ◆ ]s) -- PΓ γ → PA (x [ γ ])
  ; vz∙ = λ { {Γ}{A}{Γ∙ , πΓ}{A∙ , πA}(γ∙ , a∙) → a∙ , {!!} }
  ; vs∙ = {!!}
  ; Tm∙ = λ {Γ}{A} (Γ∙ , πΓ) (A∙ , πA) a → (γ∙ : Γ∙) → Σsp A∙ λ a∙ → πA a∙ ≡ (a [ πΓ γ∙ ⁺^ ◆ ]s) -- PΓ γ → PA (a [ γ ])
  ; Sub∙ = {!!}
  ; p∙ = {!!}
  ; ⟨_⟩∙ = {!!}
  ; _⁺∙ = {!!}
  ; var∙ = {!!}
  ; _[_]∙ = {!!}
  ; [p]∙ = {!!}
  ; vz[⟨⟩]∙ = {!!}
  ; vz[⁺]∙ = {!!}
  ; vs[⟨⟩]∙ = {!!}
  ; vs[⁺]∙ = {!!}
  ; true∙ = λ { {Γ}{Γ∙ , πΓ} γ∙ → tt , true[]s {_}{◆}{◇}{πΓ γ∙} ⁻¹ }
  ; false∙ = λ { {Γ}{Γ∙ , πΓ} γ∙ → ff , false[]s {_}{◆}{◇}{πΓ γ∙} ⁻¹ }
  ; ite∙ = λ { {Γ}{A}{t}{u}{v}{Γ∙ , πΓ}{A∙ , πA} t∙ u∙ v∙ γ∙ → {!!} , {!!} }
  ; iteβ₁∙ = {!!}
  ; iteβ₂∙ = {!!}
  ; true[]∙ = {!!}
  ; false[]∙ = {!!}
  ; ite[]∙ = {!!}
  ; num∙ = λ { {Γ}{Γ∙ , πΓ} n γ∙ → n , num[]s {_}{◆}{n}{◇}{πΓ γ∙} ⁻¹ }
  ; isZero∙ = {!!}
  ; _+o∙_ = {!!}
  ; isZeroβ₁∙ = {!!}
  ; isZeroβ₂∙ = {!!}
  ; +β∙ = {!!}
  ; num[]∙ = {!!}
  ; isZero[]∙ = {!!}
  ; +[]∙ = {!!}
  ; _⇒∙_ = {!!}
  ; lam∙ = {!!}
  ; _$∙_ = {!!}
  ; ⇒β∙ = {!!}
  ; ⇒η∙ = {!!}
  ; lam[]∙ = {!!}
  ; $[]∙ = {!!}
  }
-}
\end{code}
