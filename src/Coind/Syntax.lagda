\begin{code}
{-# OPTIONS --prop --rewriting #-}
module Coind.Syntax where

open import Lib

infixl 6 _[_]
infixl 5 _▹_
infixr 6 _⇒_
infixl 5 _$_
infixl 7 _×o_
infixl 5 _,o_

data Ty   : Set where
  _⇒_     : Ty → Ty → Ty
  _×o_    : Ty → Ty → Ty
  Bool    : Ty
  Nat     : Ty
  Stream  : Ty → Ty
  Machine : Ty

data Con  : Set where
  ◇       : Con
  _▹_     : Con → Ty → Con

data Var : Con → Ty → Set where
  vz        : ∀{Γ A} → Var (Γ ▹ A) A
  vs        : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

postulate
  Tm        : Con → Ty → Set

data Sub : Con → Con → Set where
  p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
  ⟨_⟩       : ∀{Γ A} → Tm Γ A → Sub Γ (Γ ▹ A)
  _⁺        : ∀{Γ Δ A} → (σ : Sub Δ Γ) → Sub (Δ ▹ A) (Γ ▹ A)

postulate
  var       : ∀{Γ A} → Var Γ A → Tm Γ A
  _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
  [p]       : ∀{Γ A B x} → var {Γ}{A} x [ p {A = B} ] ≡ var (vs x)
  vz[⟨⟩]    : ∀{Γ A t} → var (vz {Γ}{A}) [ ⟨ t ⟩ ] ≡ t
  vz[⁺]     : ∀{Γ Δ A σ} → var (vz {Γ}{A}) [ σ ⁺ ] ≡ var (vz {Δ}{A})
  vs[⟨⟩]    : ∀{Γ A B x t} → var (vs {Γ}{A}{B} x) [ ⟨ t ⟩ ] ≡ var x
  vs[⁺]     : ∀{Γ Δ A B x σ} → var (vs {Γ}{A}{B} x) [ σ ⁺ ] ≡ var x [ σ ] [ p {Δ} ]

  lam    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
  _$_    : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  ⇒β     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ ⟨ u ⟩ ]
  ⇒η     : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → t ≡ lam (t [ p ] $ var vz)
  lam[]  : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{σ : Sub Δ Γ} → lam t [ σ ] ≡ lam (t [ σ ⁺ ])
  $[]    : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{σ : Sub Δ Γ} →
           (t $ u) [ σ ] ≡ (t [ σ ]) $ (u [ σ ])

  _,o_      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
  fst       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
  snd       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
  ×β₁       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst (u ,o v) ≡ u
  ×β₂       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd (u ,o v) ≡ v
  ×η        : ∀{Γ A B}{t : Tm Γ (A ×o B)} → t ≡ (fst t ,o snd t)
  ,[]       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
              (u ,o v) [ γ ] ≡ (u [ γ ] ,o v [ γ ])
                
  true       : ∀{Γ} → Tm Γ Bool
  false      : ∀{Γ} → Tm Γ Bool
  iteBool    : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
  Boolβ₁     : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
  Boolβ₂     : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
  true[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
  false[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
  iteBool[]  : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
               iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])

  zeroo      : ∀{Γ} → Tm Γ Nat
  suco       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
  iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
  Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
  Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
               iteNat u v (suco t) ≡ v [ ⟨ iteNat u v t ⟩ ]
  zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
  suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
  iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
               iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⁺ ]) (t [ γ ])

  head          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ A
  tail          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ (Stream A)
  genStream     : ∀{Γ A C} → Tm (Γ ▹ C) A → Tm (Γ ▹ C) C → Tm Γ C → Tm Γ (Stream A)
  Streamβ₁      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                  head (genStream u v t) ≡ u [ ⟨ t ⟩ ]
  Streamβ₂      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                  tail (genStream u v t) ≡ genStream u v (v [ ⟨ t ⟩ ])
  head[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} →
                  head as [ γ ] ≡ head (as [ γ ])
  tail[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} → tail as [ γ ] ≡ tail (as [ γ ])
  genStream[]   : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C}{Δ}
                  {γ : Sub Δ Γ} →
                  genStream u v t [ γ ] ≡ genStream (u [ γ ⁺ ]) (v [ γ ⁺ ]) (t [ γ ])

  put           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat → Tm Γ Machine
  set           : ∀{Γ} → Tm Γ Machine → Tm Γ Machine
  get           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat
  genMachine    : ∀{Γ C} → Tm (Γ ▹ C ▹ Nat) C → Tm (Γ ▹ C) C → Tm (Γ ▹ C) Nat →
                  Tm Γ C → Tm Γ Machine
  Machineβ₁     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                  {t : Tm Γ C}{t' : Tm Γ Nat} →
                  put (genMachine u v w t) t' ≡ genMachine u v w (u [ ⟨ t ⟩ ⁺ ] [ ⟨ t' ⟩ ])
  Machineβ₂     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                  {t : Tm Γ C} →
                  set (genMachine u v w t) ≡ genMachine u v w (v [ ⟨ t ⟩ ])
  Machineβ₃     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                  {t : Tm Γ C} →
                  get (genMachine u v w t) ≡ w [ ⟨ t ⟩ ]
  put[]         : ∀{Γ}{t : Tm Γ Machine}{u : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                  put t u [ γ ] ≡ put (t [ γ ]) (u [ γ ])
  set[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → set t [ γ ] ≡ set (t [ γ ])
  get[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → get t [ γ ] ≡ get (t [ γ ])
  genMachine[]  : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                  {t : Tm Γ C}{Δ}{γ : Sub Δ Γ} →
                  genMachine u v w t [ γ ] ≡
                  genMachine (u [ γ ⁺ ⁺ ]) (v [ γ ⁺ ]) (w [ γ ⁺ ]) (t [ γ ])

-- these are provable
postulate
  [p][⁺]     : ∀{Γ A}{a : Tm Γ A}{B}{Δ}{γ : Sub Δ Γ} → a [ p {Γ}{B} ] [ γ ⁺ ] ≡ a [ γ ] [ p ]
  [p⁺][⁺⁺]   : ∀{Γ C A}{a : Tm (Γ ▹ C) A}{B}{Δ}{γ : Sub Δ Γ} → a [ p {Γ}{B} ⁺ ] [ γ ⁺ ⁺ ] ≡ a [ γ ⁺ ] [ p ⁺ ]
  [p][⟨⟩]    : ∀{Γ A}{a : Tm Γ A}{B}{b : Tm Γ B} → a [ p ] [ ⟨ b ⟩ ] ≡ a
  [p⁺][⟨⟩⁺]  : ∀{Γ A C}{a : Tm (Γ ▹ C) A}{B}{b : Tm Γ B} → a [ p ⁺ ] [ ⟨ b ⟩ ⁺ ] ≡ a
  [⟨⟩][]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → t [ ⟨ a ⟩ ] [ γ ] ≡ t [ γ ⁺ ] [ ⟨ a [ γ ] ⟩ ]
  [p⁺][⟨vz⟩] : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → t [ p ⁺ ] [ ⟨ var vz ⟩ ] ≡ t

def : {Γ : Con}{A B : Ty} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
def t u = u [ ⟨ t ⟩ ]

private
  Var' : ℕ → Con → Ty → Set
  Var' zero Γ A = Var Γ A
  Var' (suc n) Γ A = ∀ {B} → Var' n (Γ ▹ B) A

  Tm' : ℕ → Con → Ty → Set
  Tm' zero Γ A = Tm Γ A
  Tm' (suc n) Γ A = ∀ {B} → Tm' n (Γ ▹ B) A

  vs' : (n : ℕ) {Γ : Con} {A B : Ty} → Var' n Γ A → Var' n (Γ ▹ B) A
  vs' zero x = vs x
  vs' (suc n) x = vs' n x

  var' : (n : ℕ) {Γ : Con} {A : Ty} → Var' n Γ A → Tm' n Γ A
  var' zero x = var x
  var' (suc n) x = var' n x

  v' : (n : ℕ) {Γ : Con} {A : Ty} → Var' n (Γ ▹ A) A
  v' zero = vz
  v' (suc n) = vs' n (v' n)

v : (n : ℕ) {Γ : Con} {A : Ty} → Tm' n (Γ ▹ A) A
v n = var' n (v' n)

v0 : ∀{Γ A} → Tm (Γ ▹ A) A
v0 = var vz
v1 : ∀{Γ A B} → Tm (Γ ▹ A ▹ B) A
v1 = var (vs vz)
v2 : ∀{Γ A B C} → Tm (Γ ▹ A ▹ B ▹ C) A
v2 = var (vs (vs vz))
v3 : ∀{Γ A B C D} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
v3 = var (vs (vs (vs vz)))
v4 : ∀{Γ A B C D E} → Tm (Γ ▹ A ▹ B ▹ C ▹ D ▹ E) A
v4 = var (vs (vs (vs (vs vz))))
\end{code}
