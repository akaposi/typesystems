\begin{code}
{-# OPTIONS --prop --rewriting #-}
module Coind.Model where

open import Lib
import Coind.Syntax as I

record Model {i j} : Set (lsuc (i ⊔ j)) where
  infixl 6 _[_]
  infixl 5 _▹_
  infixr 6 _⇒_
  infixl 5 _$_
  infixl 7 _×o_
  infixl 5 _,o_

  field
    Ty        : Set i

    Con       : Set i
    ◇         : Con
    _▹_       : Con → Ty → Con

    Var       : Con → Ty → Set j
    vz        : ∀{Γ A} → Var (Γ ▹ A) A
    vs        : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

    Tm        : Con → Ty → Set j

    Sub       : Con → Con → Set j
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    ⟨_⟩       : ∀{Γ A} → Tm Γ A → Sub Γ (Γ ▹ A)
    _⁺        : ∀{Γ Δ A} → (σ : Sub Δ Γ) → Sub (Δ ▹ A) (Γ ▹ A)

    var       : ∀{Γ A} → Var Γ A → Tm Γ A
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [p]       : ∀{Γ A B x} → var {Γ}{A} x [ p {A = B} ] ≡ var (vs x)
    vz[⟨⟩]    : ∀{Γ A t} → var (vz {Γ}{A}) [ ⟨ t ⟩ ] ≡ t
    vz[⁺]     : ∀{Γ Δ A σ} → var (vz {Γ}{A}) [ σ ⁺ ] ≡ var (vz {Δ}{A})
    vs[⟨⟩]    : ∀{Γ A B x t} → var (vs {Γ}{A}{B} x) [ ⟨ t ⟩ ] ≡ var x
    vs[⁺]     : ∀{Γ Δ A B x σ} → var (vs {Γ}{A}{B} x) [ σ ⁺ ] ≡ var x [ σ ] [ p {Δ} ]
    
    _⇒_    : Ty → Ty → Ty
    lam    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_    : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ ⟨ u ⟩ ]
    ⇒η     : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → t ≡ lam (t [ p ] $ var vz)
    lam[]  : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{σ : Sub Δ Γ} → lam t [ σ ] ≡ lam (t [ σ ⁺ ])
    $[]    : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{σ : Sub Δ Γ} →
             (t $ u) [ σ ] ≡ (t [ σ ]) $ (u [ σ ])

    _×o_      : Ty → Ty → Ty
    _,o_      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
    fst       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
    snd       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
    ×β₁       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst (u ,o v) ≡ u
    ×β₂       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd (u ,o v) ≡ v
    ×η        : ∀{Γ A B}{t : Tm Γ (A ×o B)} → t ≡ (fst t ,o snd t)
    ,[]       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                (u ,o v) [ γ ] ≡ (u [ γ ] ,o v [ γ ])
              
    Bool       : Ty
    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    iteBool    : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
    Boolβ₁     : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
    Boolβ₂     : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
    true[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    iteBool[]  : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                 iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])
    
    Nat        : Ty
    zeroo      : ∀{Γ} → Tm Γ Nat
    suco       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                 iteNat u v (suco t) ≡ v [ ⟨ iteNat u v t ⟩ ]
    zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
    suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
    iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                 iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⁺ ]) (t [ γ ])

    Stream        : Ty → Ty
    head          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ A
    tail          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ (Stream A)
    genStream     : ∀{Γ A C} → Tm (Γ ▹ C) A → Tm (Γ ▹ C) C → Tm Γ C → Tm Γ (Stream A)
    Streamβ₁      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                    head (genStream u v t) ≡ u [ ⟨ t ⟩ ]
    Streamβ₂      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                    tail (genStream u v t) ≡ genStream u v (v [ ⟨ t ⟩ ])
    head[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} →
                    head as [ γ ] ≡ head (as [ γ ])
    tail[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} → tail as [ γ ] ≡ tail (as [ γ ])
    genStream[]   : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C}{Δ}
                    {γ : Sub Δ Γ} →
                    genStream u v t [ γ ] ≡ genStream (u [ γ ⁺ ]) (v [ γ ⁺ ]) (t [ γ ])

    Machine       : Ty
    put           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat → Tm Γ Machine
    set           : ∀{Γ} → Tm Γ Machine → Tm Γ Machine
    get           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat
    genMachine    : ∀{Γ C} → Tm (Γ ▹ C ▹ Nat) C → Tm (Γ ▹ C) C → Tm (Γ ▹ C) Nat →
                    Tm Γ C → Tm Γ Machine
    Machineβ₁     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C}{t' : Tm Γ Nat} →
                    put (genMachine u v w t) t' ≡ genMachine u v w (u [ ⟨ t ⟩ ⁺ ] [ ⟨ t' ⟩ ])
    Machineβ₂     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C} →
                    set (genMachine u v w t) ≡ genMachine u v w (v [ ⟨ t ⟩ ])
    Machineβ₃     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C} →
                    get (genMachine u v w t) ≡ w [ ⟨ t ⟩ ]
    put[]         : ∀{Γ}{t : Tm Γ Machine}{u : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                    put t u [ γ ] ≡ put (t [ γ ]) (u [ γ ])
    set[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → set t [ γ ] ≡ set (t [ γ ])
    get[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → get t [ γ ] ≡ get (t [ γ ])
    genMachine[]  : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C}{Δ}{γ : Sub Δ Γ} →
                    genMachine u v w t [ γ ] ≡
                    genMachine (u [ γ ⁺ ⁺ ]) (v [ γ ⁺ ]) (w [ γ ⁺ ]) (t [ γ ])


  ⟦_⟧T : (A : I.Ty) → Ty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ A I.×o B ⟧T = ⟦ A ⟧T ×o ⟦ B ⟧T
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Stream A ⟧T = Stream ⟦ A ⟧T
  ⟦ I.Machine ⟧T = Machine

  ⟦_⟧C : I.Con → Con
  ⟦ I.◇ ⟧C = ◇
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  ⟦_⟧v : ∀{Γ A} → I.Var Γ A  → Var ⟦ Γ ⟧C ⟦ A ⟧T
  ⟦ I.vz ⟧v = vz
  ⟦ I.vs x ⟧v = vs ⟦ x ⟧v

  postulate
    ⟦_⟧t : ∀{Γ A} → I.Tm Γ A  → Tm  ⟦ Γ ⟧C ⟦ A ⟧T

  ⟦_⟧s : ∀{Γ Δ} → I.Sub Δ Γ → Sub ⟦ Δ ⟧C  ⟦ Γ ⟧C
  ⟦ I.p ⟧s = p
  ⟦ I.⟨ t ⟩ ⟧s = ⟨ ⟦ t ⟧t ⟩
  ⟦ σ I.⁺ ⟧s = ⟦ σ ⟧s ⁺

  postulate
    ⟦var⟧     : ∀{Γ A}  {x : I.Var Γ A}              → ⟦ I.var x   ⟧t ≈ var ⟦ x ⟧v
    ⟦[]⟧      : ∀{Γ Δ A}{t :  I.Tm Γ A}{σ : I.Sub Δ Γ} → ⟦ t I.[ σ ] ⟧t ≈ ⟦ t ⟧t [ ⟦ σ ⟧s ]
    {-# REWRITE ⟦var⟧ ⟦[]⟧ #-}

    ⟦lam⟧     : ∀{Γ A B t} → ⟦ I.lam {Γ}{A}{B} t ⟧t ≈ lam ⟦ t ⟧t
    ⟦$⟧       : ∀{Γ A B}{t : I.Tm Γ (A I.⇒ B)}{u} → ⟦ t I.$ u ⟧t ≈ ⟦ t ⟧t $ ⟦ u ⟧t
    {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}

    ⟦,⟧     : ∀{Γ A B u v} → ⟦ I._,o_ {Γ}{A}{B} u v ⟧t ≈ ⟦ u ⟧t ,o ⟦ v ⟧t
    ⟦fst⟧   : ∀{Γ A B}{t : I.Tm Γ (A I.×o B)} → ⟦ I.fst t ⟧t ≈ fst ⟦ t ⟧t
    ⟦snd⟧   : ∀{Γ A B}{t : I.Tm Γ (A I.×o B)} → ⟦ I.snd t ⟧t ≈ snd ⟦ t ⟧t
    {-# REWRITE ⟦,⟧ ⟦fst⟧ ⟦snd⟧ #-}

    ⟦true⟧    : ∀{Γ} → ⟦ I.true  {Γ} ⟧t ≈ true
    ⟦false⟧   : ∀{Γ} → ⟦ I.false {Γ} ⟧t ≈ false
    ⟦iteBool⟧ : ∀{Γ A}{t : I.Tm Γ I.Bool}{u v : I.Tm Γ A} → 
                ⟦ I.iteBool u v t ⟧t ≈ iteBool ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t 
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦iteBool⟧ #-}

    ⟦zero⟧    : ∀{Γ} → ⟦ I.zeroo {Γ} ⟧t ≈ zeroo
    ⟦suc⟧     : ∀{Γ t} → ⟦ I.suco {Γ} t ⟧t ≈ suco ⟦ t ⟧t
    ⟦iteNat⟧  : ∀{Γ A t u v} → ⟦ I.iteNat {Γ}{A} t u v ⟧t ≈ iteNat ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦iteNat⟧ #-}

    -- TODO: ⟦head⟧, ...
\end{code}
