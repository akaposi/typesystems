{-# OPTIONS --prop --rewriting --guardedness #-}

module Everything.Model where

open import Lib
import Everything.Syntax as I

record Model {i j} : Set (lsuc (i ⊔ j)) where
  infixl 6 _[_]
  infixl 5 _▹_
  infixr 6 _⇒_
  infixl 5 _$_
  infixl 8 _×o_
  infixl 5 _,o_
  infixl 7 _+o_

  field
    Ty : Set i

    Con : Set i
    ◇         : Con
    _▹_       : Con → Ty → Con

    Var       : Con → Ty → Set j
    vz        : ∀{Γ A} → Var (Γ ▹ A) A
    vs        : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

    Tm        : Con → Ty → Set j

    Sub       : Con → Con → Set j
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    ⟨_⟩       : ∀{Γ A} → Tm Γ A → Sub Γ (Γ ▹ A)
    _⁺        : ∀{Γ Δ A} → (σ : Sub Δ Γ) → Sub (Δ ▹ A) (Γ ▹ A)
    
    var       : ∀{Γ A} → Var Γ A → Tm Γ A
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [p]       : ∀{Γ A B x} → var {Γ}{A} x [ p {A = B} ] ≡ var (vs x)
    vz[⟨⟩]    : ∀{Γ A t} → var (vz {Γ}{A}) [ ⟨ t ⟩ ] ≡ t
    vz[⁺]     : ∀{Γ Δ A σ} → var (vz {Γ}{A}) [ σ ⁺ ] ≡ var (vz {Δ}{A})
    vs[⟨⟩]    : ∀{Γ A B x t} → var (vs {Γ}{A}{B} x) [ ⟨ t ⟩ ] ≡ var x
    vs[⁺]     : ∀{Γ Δ A B x σ} → var (vs {Γ}{A}{B} x) [ σ ⁺ ] ≡ var x [ σ ] [ p {Δ} ]

  def : {Γ : Con}{A B : Ty} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ ⟨ t ⟩ ]

  field
    _⇒_     : Ty → Ty → Ty
    lam    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_    : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ ⟨ u ⟩ ]
    ⇒η     : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → t ≡ lam (t [ p ] $ var vz)
    lam[]  : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{σ : Sub Δ Γ} → lam t [ σ ] ≡ lam (t [ σ ⁺ ])
    $[]    : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{σ : Sub Δ Γ} →
             (t $ u) [ σ ] ≡ (t [ σ ]) $ (u [ σ ])
    
    Unit    : Ty
    trivial    : ∀{Γ} → Tm Γ Unit
    iteUnit    : ∀{Γ A} → Tm Γ A → Tm Γ Unit → Tm Γ A
    Unitβ      : ∀{Γ A t} → iteUnit {Γ}{A} t trivial ≡ t
    Unitη      : ∀{Γ}{u : Tm Γ Unit} → u ≡ trivial

    Bool       : Ty
    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    iteBool    : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
    Boolβ₁     : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
    Boolβ₂     : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
    true[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    iteBool[]  : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                 iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])

    Nat        : Ty
    zeroo      : ∀{Γ} → Tm Γ Nat
    suco       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                 iteNat u v (suco t) ≡ v [ ⟨ iteNat u v t ⟩ ]
    zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
    suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
    iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                 iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⁺ ]) (t [ γ ])

    _×o_    : Ty → Ty → Ty
    _,o_      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
    fst       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
    snd       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
    ×β₁       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst (u ,o v) ≡ u
    ×β₂       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd (u ,o v) ≡ v
    ×η        : ∀{Γ A B}{t : Tm Γ (A ×o B)} → (fst t ,o snd t) ≡ t
    ,[]       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                (u ,o v) [ γ ] ≡ ((u [ γ ]) ,o (v [ γ ]))

    _+o_      : Ty → Ty → Ty
    inl       : ∀{Γ A B} → Tm Γ A → Tm Γ (A +o B)
    inr       : ∀{Γ A B} → Tm Γ B → Tm Γ (A +o B)
    caseo     : ∀{Γ A B C} → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ (A +o B) → Tm Γ C
    +β₁       : ∀{Γ A B C}{c₁ : Tm (Γ ▹ A) C}{c₂ : Tm (Γ ▹ B) C}{a : Tm Γ A} →
                caseo c₁ c₂ (inl a) ≡ c₁ [ ⟨ a ⟩ ]
    +β₂       : ∀{Γ A B C}{c₁ : Tm (Γ ▹ A) C}{c₂ : Tm (Γ ▹ B) C}{b : Tm Γ B} →
                caseo c₁ c₂ (inr b) ≡ c₂ [ ⟨ b ⟩ ]
    +η        : ∀{Γ A B C}{c : Tm (Γ ▹ A +o B) C} →
                c ≡ caseo (c [ p ⁺ ] [ ⟨ inl (var vz) ⟩ ] [ p ⁺ ]) (c [ p ⁺ ] [ ⟨ inr (var vz) ⟩ ] [ p ⁺ ]) (var vz)
    inl[]     : ∀{Γ A B}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → inl {B = B} a [ γ ] ≡ inl (a [ γ ])
    inr[]     : ∀{Γ A B}{b : Tm Γ B}{Δ}{γ : Sub Δ Γ} → inr {A = A} b [ γ ] ≡ inr (b [ γ ])
    case[]    : ∀{Γ A B C}{c₁ : Tm (Γ ▹ A) C}{c₂ : Tm (Γ ▹ B) C}{ab : Tm Γ (A +o B)}{Δ}{γ : Sub Δ Γ} →
                (caseo c₁ c₂ ab) [ γ ] ≡ caseo (c₁ [ γ ⁺ ]) (c₂ [ γ ⁺ ]) (ab [ γ ])

    Empty     : Ty
    absurd    : ∀{Γ A} → Tm Γ Empty → Tm Γ A
    Emptyη    : ∀{Γ A}{t : Tm (Γ ▹ Empty) A} → t ≡ absurd (var vz)
    absurd[]  : ∀{Γ A}{t : Tm Γ Empty}{Δ}{γ : Sub Δ Γ} → absurd {A = A} t [ γ ] ≡ absurd (t [ γ ])
    
    List       : Ty → Ty
    nil        : ∀{Γ A} → Tm Γ (List A)
    cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
    iteList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
    Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → iteList u v nil ≡ u
    Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                 iteList u v (cons t₁ t) ≡ (v [ ⟨ t₁ ⟩ ⁺ ] [ ⟨ iteList u v t ⟩ ])
    nil[]      : ∀{Γ A Δ}{γ : Sub Δ Γ} → nil {Γ}{A} [ γ ] ≡ nil {Δ}{A}
    cons[]     : ∀{Γ A}{t₁ : Tm Γ A}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                 (cons t₁ t) [ γ ] ≡ cons (t₁ [ γ ]) (t [ γ ])
    iteList[]  : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                 iteList u v t [ γ ] ≡ iteList (u [ γ ]) (v [ γ ⁺ ⁺  ]) (t [ γ ])
    
    Tree       : Ty → Ty
    leaf       : ∀{Γ A} → Tm Γ A → Tm Γ (Tree A)
    node       : ∀{Γ A} → Tm Γ (Tree A) → Tm Γ (Tree A) → Tm Γ (Tree A)
    iteTree    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm (Γ ▹ B ▹ B) B → Tm Γ (Tree A) → Tm Γ B
    Treeβ₁     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{a : Tm Γ A} → iteTree l n (leaf a) ≡ l [ ⟨ a ⟩ ]
    Treeβ₂     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{ll rr : Tm Γ (Tree A)} →
                 iteTree l n (node ll rr) ≡ n [ ⟨ iteTree l n ll ⟩ ⁺ ] [ ⟨ iteTree l n rr ⟩ ]
    leaf[]     : ∀{Γ A}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (leaf a) [ γ ] ≡ leaf (a [ γ ])
    node[]     : ∀{Γ A}{ll rr : Tm Γ (Tree A)}{Δ}{γ : Sub Δ Γ} →
                 (node ll rr) [ γ ] ≡ node (ll [ γ ]) (rr [ γ ])
    iteTree[]  : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{t : Tm Γ (Tree A)}{Δ}{γ : Sub Δ Γ} →
                 iteTree l n t [ γ ] ≡ iteTree (l [ γ ⁺ ]) (n [ γ ⁺ ⁺ ]) (t [ γ ])

    Stream        : Ty → Ty
    head          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ A
    tail          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ (Stream A)
    genStream     : ∀{Γ A C} → Tm (Γ ▹ C) A → Tm (Γ ▹ C) C → Tm Γ C → Tm Γ (Stream A)
    Streamβ₁      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                    head (genStream u v t) ≡ u [ ⟨ t ⟩ ]
    Streamβ₂      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                    tail (genStream u v t) ≡ genStream u v (v [ ⟨ t ⟩ ])
    head[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} →
                    head as [ γ ] ≡ head (as [ γ ])
    tail[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} → tail as [ γ ] ≡ tail (as [ γ ])
    genStream[]   : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C}{Δ}
                    {γ : Sub Δ Γ} →
                    genStream u v t [ γ ] ≡ genStream (u [ γ ⁺ ]) (v [ γ ⁺ ]) (t [ γ ])

    Machine       : Ty
    put           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat → Tm Γ Machine
    set           : ∀{Γ} → Tm Γ Machine → Tm Γ Machine
    get           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat
    genMachine    : ∀{Γ C} → Tm (Γ ▹ C ▹ Nat) C → Tm (Γ ▹ C) C → Tm (Γ ▹ C) Nat →
                    Tm Γ C → Tm Γ Machine
    Machineβ₁     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C}{t' : Tm Γ Nat} →
                    put (genMachine u v w t) t' ≡ genMachine u v w (u [ ⟨ t ⟩ ⁺ ] [ ⟨ t' ⟩ ])
    Machineβ₂     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C} →
                    set (genMachine u v w t) ≡ genMachine u v w (v [ ⟨ t ⟩ ])
    Machineβ₃     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C} →
                    get (genMachine u v w t) ≡ w [ ⟨ t ⟩ ]
    put[]         : ∀{Γ}{t : Tm Γ Machine}{u : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                    put t u [ γ ] ≡ put (t [ γ ]) (u [ γ ])
    set[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → set t [ γ ] ≡ set (t [ γ ])
    get[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → get t [ γ ] ≡ get (t [ γ ])
    genMachine[]  : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C}{Δ}{γ : Sub Δ Γ} →
                    genMachine u v w t [ γ ] ≡
                    genMachine (u [ γ ⁺ ⁺ ]) (v [ γ ⁺ ]) (w [ γ ⁺ ]) (t [ γ ])
  
  trivial[] : ∀{Γ Δ}{γ : Sub Δ Γ} → trivial [ γ ] ≡ trivial
  trivial[] = Unitη

  iteUnit[] : ∀{Γ A t u Δ}{γ : Sub Δ Γ} → iteUnit {Γ}{A} u t [ γ ] ≡ iteUnit (u [ γ ]) (t [ γ ])
  iteUnit[] {t = t} {u} {_} {γ} = cong (λ x → iteUnit u x [ γ ]) Unitη ◾ cong _[ γ ] Unitβ ◾ Unitβ ⁻¹ ◾ cong (iteUnit (u [ γ ])) (Unitη ⁻¹)
    
  fst[] : ∀{Γ A B}{t : Tm Γ (A ×o B)}{Δ}{σ : Sub Δ Γ} → fst t [ σ ] ≡ fst (t [ σ ])
  fst[] {t = t} {_} {σ} =
    (fst t [ σ ])
    ≡⟨ ×β₁ ⁻¹ ⟩
    fst (fst t [ σ ] ,o (snd t [ σ ]))
    ≡⟨ cong fst (,[] ⁻¹) ⟩
    fst ((fst t ,o snd t) [ σ ])
    ≡⟨ cong (λ x → fst (x [ σ ])) ×η ⟩
    fst (t [ σ ]) ∎

  snd[] : ∀{Γ A B}{t : Tm Γ (A ×o B)}{Δ}{σ : Sub Δ Γ} → snd t [ σ ] ≡ snd (t [ σ ])
  snd[] {t = t} {_} {σ} =
    (snd t [ σ ])
    ≡⟨ ×β₂ ⁻¹ ⟩
    snd (fst t [ σ ] ,o (snd t [ σ ]))
    ≡⟨ cong snd (,[] ⁻¹) ⟩
    snd ((fst t ,o snd t) [ σ ])
    ≡⟨ cong (λ x → snd (x [ σ ])) ×η ⟩
    snd (t [ σ ]) ∎
  
  ⟦_⟧T : (A : I.Ty) → Ty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Unit ⟧T = Unit
  ⟦ I.List A ⟧T = List ⟦ A ⟧T
  ⟦ I.Tree A ⟧T = Tree ⟦ A ⟧T
  ⟦ A I.×o B ⟧T = ⟦ A ⟧T ×o ⟦ B ⟧T
  ⟦ A I.+o B ⟧T = ⟦ A ⟧T +o ⟦ B ⟧T
  ⟦ I.Empty ⟧T = Empty
  ⟦ I.Stream A ⟧T = Stream ⟦ A ⟧T
  ⟦ I.Machine ⟧T = Machine

  ⟦_⟧C : I.Con → Con
  ⟦ I.◇ ⟧C = ◇
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  ⟦_⟧v : ∀{Γ A} → I.Var Γ A  → Var ⟦ Γ ⟧C ⟦ A ⟧T
  ⟦ I.vz ⟧v = vz
  ⟦ I.vs x ⟧v = vs ⟦ x ⟧v

  postulate
    ⟦_⟧t : ∀{Γ A} → I.Tm Γ A  → Tm  ⟦ Γ ⟧C ⟦ A ⟧T

  ⟦_⟧s : ∀{Γ Δ} → I.Sub Δ Γ → Sub ⟦ Δ ⟧C  ⟦ Γ ⟧C
  ⟦ I.p ⟧s = p
  ⟦ I.⟨ t ⟩ ⟧s = ⟨ ⟦ t ⟧t ⟩
  ⟦ σ I.⁺ ⟧s = ⟦ σ ⟧s ⁺

  postulate
    ⟦var⟧     : ∀{Γ A}  {x : I.Var Γ A}              → ⟦ I.var x   ⟧t ≈ var ⟦ x ⟧v
    ⟦[]⟧      : ∀{Γ Δ A}{t :  I.Tm Γ A}{σ : I.Sub Δ Γ} → ⟦ t I.[ σ ] ⟧t ≈ ⟦ t ⟧t [ ⟦ σ ⟧s ]
    {-# REWRITE ⟦var⟧ ⟦[]⟧ #-}

    ⟦lam⟧     : ∀{Γ A B t} → ⟦ I.lam {Γ}{A}{B} t ⟧t ≈ lam ⟦ t ⟧t
    ⟦$⟧       : ∀{Γ A B}{t : I.Tm Γ (A I.⇒ B)}{u} → ⟦ t I.$ u ⟧t ≈ ⟦ t ⟧t $ ⟦ u ⟧t
    {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}
    
    ⟦true⟧    : ∀{Γ} → ⟦ I.true  {Γ} ⟧t ≈ true
    ⟦false⟧   : ∀{Γ} → ⟦ I.false {Γ} ⟧t ≈ false
    ⟦iteBool⟧ : ∀{Γ A}{t : I.Tm Γ I.Bool}{u v : I.Tm Γ A} → 
                ⟦ I.iteBool u v t ⟧t ≈ iteBool ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t 
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦iteBool⟧ #-}

    ⟦zero⟧    : ∀{Γ} → ⟦ I.zeroo {Γ} ⟧t ≈ zeroo
    ⟦suc⟧     : ∀{Γ t} → ⟦ I.suco {Γ} t ⟧t ≈ suco ⟦ t ⟧t
    ⟦iteNat⟧  : ∀{Γ A t u v} → ⟦ I.iteNat {Γ}{A} t u v ⟧t ≈ iteNat ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦iteNat⟧ #-}

    ⟦trivial⟧  : ∀{Γ} → ⟦ I.trivial {Γ} ⟧t ≈ trivial
    ⟦iteUnit⟧  : ∀{Γ A u t} → ⟦ I.iteUnit {Γ}{A} u t ⟧t ≈ iteUnit ⟦ u ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦trivial⟧ ⟦iteUnit⟧ #-}

    ⟦absurd⟧ : ∀{Γ A t} → ⟦ I.absurd {Γ}{A} t ⟧t ≈ absurd ⟦ t ⟧t 
    {-# REWRITE ⟦absurd⟧ #-}
    
    ⟦nil⟧      : ∀{Γ A} → ⟦ I.nil {Γ}{A} ⟧t ≈ nil
    ⟦cons⟧     : ∀{Γ A t ts} → ⟦ I.cons {Γ}{A} t ts ⟧t ≈ cons ⟦ t ⟧t ⟦ ts ⟧t
    ⟦iteList⟧  : ∀{Γ A C ts u v} → ⟦ I.iteList {Γ}{A}{C} u v ts ⟧t ≈ iteList ⟦ u ⟧t ⟦ v ⟧t ⟦ ts ⟧t
    {-# REWRITE ⟦nil⟧ ⟦cons⟧ ⟦iteList⟧ #-}

    ⟦leaf⟧     : ∀{Γ A t} → ⟦ I.leaf {Γ}{A} t ⟧t ≈ leaf ⟦ t ⟧t
    ⟦node⟧     : ∀{Γ A t1 t2} → ⟦ I.node {Γ}{A} t1 t2 ⟧t ≈ node ⟦ t1 ⟧t ⟦ t2 ⟧t
    ⟦iteTree⟧  : ∀{Γ A C l n t} → ⟦ I.iteTree {Γ}{A}{C} l n t ⟧t ≈ iteTree ⟦ l ⟧t ⟦ n ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf⟧ ⟦node⟧ ⟦iteTree⟧ #-}

    ⟦,⟧     : ∀{Γ A B u v} → ⟦ I._,o_ {Γ}{A}{B} u v ⟧t ≈ ⟦ u ⟧t ,o ⟦ v ⟧t
    ⟦fst⟧   : ∀{Γ A B}{t : I.Tm Γ (A I.×o B)} → ⟦ I.fst t ⟧t ≈ fst ⟦ t ⟧t
    ⟦snd⟧   : ∀{Γ A B}{t : I.Tm Γ (A I.×o B)} → ⟦ I.snd t ⟧t ≈ snd ⟦ t ⟧t
    {-# REWRITE ⟦,⟧ ⟦fst⟧ ⟦snd⟧ #-}

    ⟦inl⟧   : ∀{Γ A B t} → ⟦ I.inl {Γ}{A}{B} t ⟧t ≈ inl ⟦ t ⟧t
    ⟦inr⟧   : ∀{Γ A B t} → ⟦ I.inr {Γ}{A}{B} t ⟧t ≈ inr ⟦ t ⟧t
    ⟦caseo⟧ : ∀{Γ A B C u v t} → ⟦ I.caseo {Γ}{A}{B}{C} u v t ⟧t ≈ caseo ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦inl⟧ ⟦inr⟧ ⟦caseo⟧ #-}

    ⟦head⟧      : ∀{Γ A t} → ⟦ I.head {Γ}{A} t ⟧t ≈ head ⟦ t ⟧t
    ⟦tail⟧      : ∀{Γ A t} → ⟦ I.tail {Γ}{A} t ⟧t ≈ tail ⟦ t ⟧t
    ⟦genStream⟧ : ∀{Γ A C t u v} → ⟦ I.genStream {Γ}{A}{C} u v t ⟧t ≈ genStream ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦head⟧ ⟦tail⟧ ⟦genStream⟧ #-}

    ⟦put⟧        : ∀{Γ t u} → ⟦ I.put {Γ} t u ⟧t ≈ put ⟦ t ⟧t ⟦ u ⟧t
    ⟦set⟧        : ∀{Γ t}   → ⟦ I.set {Γ} t ⟧t ≈ set ⟦ t ⟧t
    ⟦get⟧        : ∀{Γ t}   → ⟦ I.get {Γ} t ⟧t ≈ get ⟦ t ⟧t
    ⟦genMachine⟧ : ∀{Γ C u v w t} → ⟦ I.genMachine {Γ}{C} u v w t ⟧t ≈ genMachine ⟦ u ⟧t ⟦ v ⟧t ⟦ w ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦put⟧ ⟦set⟧ ⟦get⟧ ⟦genMachine⟧ #-}

module Standard where
  data List (A : Set) : Set where
    []  : List A
    _∷_ : A → List A → List A
  infixr 5 _∷_
  
  iteList : {A B : Set} → B → (A → B → B) → List A → B
  iteList b f [] = b
  iteList b f (a ∷ as) = f a (iteList b f as)

  data Tree (A : Set) : Set where
    leaf : A → Tree A
    node : Tree A → Tree A → Tree A
  
  iteTree : {A B : Set} → (A → B) → (B → B → B) → Tree A → B
  iteTree f g (leaf a) = f a
  iteTree f g (node t t') = g (iteTree f g t) (iteTree f g t')
  
  record Stream (A : Set) : Set where
    coinductive
    field
      head : A
      tail : Stream A
  open Stream

  genStream : {A C : Set} → (C → A) → (C → C) → C → Stream A
  head (genStream f g c) = f c
  tail (genStream f g c) = genStream f g (g c)

  take : {A : Set} → ℕ → Stream A → List A
  take zero s = []
  take (suc n) s = head s ∷ take n (tail s)

  record Machine : Set where
    coinductive
    field
      put : ℕ → Machine
      set : Machine
      get : ℕ
  open Machine
  
  genMachine : {C : Set} → (C → ℕ → C) → (C → C) → (C → ℕ) → C → Machine
  put (genMachine f g h c) n = genMachine f g h (f c n)
  set (genMachine f g h c) = genMachine f g h (g c)
  get (genMachine f g h c) = h c

  St : Model {lsuc lzero} {lzero}
  St = record
    { Ty        = Set
    
    ; Con       = Set
    ; ◇         = Lift ⊤
    ; _▹_       = _×_
    
    ; Var       = λ Γ A → Γ → A
    ; vz        = π₂
    ; vs        = λ σ → σ ∘ π₁
    ; Tm        = λ Γ A → Γ → A
    
    ; Sub       = λ Δ Γ → Δ → Γ
    ; p         = π₁
    ; ⟨_⟩       = λ t γ → (γ , t γ)
    ; _⁺        = λ σ δa → σ (π₁ δa) , π₂ δa
    
    ; var       = λ x → x
    ; _[_]      = λ t σ → t ∘ σ
    ; [p]       = refl
    ; vz[⟨⟩]    = refl
    ; vz[⁺]     = refl
    ; vs[⟨⟩]    = refl
    ; vs[⁺]     = refl
    
    ; _⇒_       = λ A B → A → B
    ; lam       = λ t γ → λ a → t (γ , a)
    ; _$_       = λ t u γ → t γ (u γ)
    ; ⇒β        = refl
    ; ⇒η        = refl
    ; lam[]     = refl
    ; $[]       = refl  
    
    ; Unit      = Lift ⊤
    ; trivial   = λ _ → mk triv
    ; iteUnit   = λ z _ → z
    ; Unitβ     = refl
    ; Unitη     = refl
    
    ; Bool      = 𝟚
    ; true      = λ _ → tt
    ; false     = λ _ → ff
    ; iteBool   = λ u v t γ* → if t γ* then u γ* else v γ*
    ; Boolβ₁    = refl
    ; Boolβ₂    = refl
    ; true[]    = refl
    ; false[]   = refl
    ; iteBool[] = refl
    
    ; Nat        = ℕ
    ; zeroo      = λ _ → zero
    ; suco       = λ t γ* → suc (t γ*)
    ; iteNat     = λ u v t γ* → iteℕ (u γ*) (λ x → v (γ* , x)) (t γ*)
    ; Natβ₁      = refl
    ; Natβ₂      = refl
    ; zero[]     = refl
    ; suc[]      = refl
    ; iteNat[]   = refl
    
    ; _×o_      = _×_
    ; _,o_      = λ a b γ → a γ , b γ
    ; fst       = λ ab γ → π₁ (ab γ)
    ; snd       = λ ab γ → π₂ (ab γ)
    ; ×β₁       = refl
    ; ×β₂       = refl
    ; ×η        = refl
    ; ,[]       = refl
    
    ; _+o_      = _⊎_
    ; inl       = λ a γ → ι₁ (a γ)
    ; inr       = λ b γ → ι₂ (b γ)
    ; caseo     = λ c₁ c₂ ab γ → case (ab γ) (λ a → c₁ (γ , a)) (λ b → c₂ (γ , b))
    ; +β₁       = refl
    ; +β₂       = refl
    ; +η        = funext λ { (_ , ι₁ _) → refl ; (_ , ι₂ _) → refl }
    ; inl[]     = refl
    ; inr[]     = refl
    ; case[]    = refl
    
    ; Empty     = Lift ⊥
    ; absurd    = λ t γ → exfalso (un (t γ))
    ; Emptyη    = funext λ ()
    ; absurd[]  = refl
    
    ; List       = List
    ; nil        = λ _ → []
    ; cons       = λ u v γ* → u γ* ∷ v γ*
    ; iteList    = λ u v t γ* → iteList (u γ*) (λ x y → v ((γ* , x) , y)) (t γ*)
    ; Listβ₁     = refl
    ; Listβ₂     = refl
    ; nil[]      = refl
    ; cons[]     = refl
    ; iteList[]  = refl

    ; Tree       = Tree
    ; leaf       = λ t γ* → leaf (t γ*)
    ; node       = λ u v γ* → node (u γ*) (v γ*)
    ; iteTree    = λ {Γ}{A}{B} u v t γ* → iteTree (λ x → u (γ* , x)) (λ x y → v ((γ* , x) , y)) (t γ*)
    ; Treeβ₁     = refl
    ; Treeβ₂     = refl
    ; leaf[]     = refl
    ; node[]     = refl
    ; iteTree[]  = refl
    
    ; Stream        = Stream
    ; head          = λ t γ* → head (t γ*)
    ; tail          = λ t γ* → tail (t γ*)
    ; genStream     = λ u v t γ* → genStream (λ x → u (γ* , x)) (λ x → v (γ* , x)) (t γ*)
    ; Streamβ₁      = refl
    ; Streamβ₂      = refl
    ; head[]        = refl
    ; tail[]        = refl
    ; genStream[]   = refl

    ; Machine       = Machine
    ; put           = λ t u γ* → put (t γ*) (u γ*) 
    ; set           = λ t γ* → set (t γ*)
    ; get           = λ t γ* → get (t γ*)
    ; genMachine    = λ u v w t γ* → genMachine (λ x y → u ((γ* , x) , y))
                      (λ x → v (γ* , x)) (λ x → w (γ* , x)) (t γ*)
    ; Machineβ₁     = refl
    ; Machineβ₂     = refl
    ; Machineβ₃     = refl
    ; put[]         = refl
    ; set[]         = refl
    ; get[]         = refl
    ; genMachine[]  = refl
    }

module St = Model Standard.St

eval : {A : I.Ty} → I.Tm I.◇ A → St.⟦ A ⟧T
eval t = St.⟦ t ⟧t (mk triv)