# Nyelvek típusrendszere tantárgy, 2023. ősz, ELTE-IK, Programtervező informatikus MSc #

Fontos, hogy megfelelő kóddal vedd fel a tárgyat:

 * MSc nappali, 2022-es tanterv: IPM-22sztNYTR[E|G]
 * MSc esti, 2022-es tanterv: IPM-22EsztNYTR[E|G]
 * MSc nappali, 2018-as tanterv: IPM-18sztNYTR[E|G]
 * MSc esti, 2018-as tanterv: IPM-18EsztNYTR[E|G]

#### Időpontok

- Előadás (Kaposi Ambrus): Kedd 16:00-17:30, Déli Tömb 2-502
- Gyakorlat, 1. kurzus (Bense Viktor): Szerda 17:45-19:15, Északi Tömb 7.15 (PC 11)
- Gyakorlat, 2. kurzus (Bense Viktor): Szerda 19:30-21:00, Északi Tömb 7.15 (PC 11)
- Gyakorlat, 3. kurzus (angol nyelvű) (Szumi Xie): Csütörtök 19:30-21:00, Északi Tömb 7.15 (PC 11)


#### Elérhetőségek

- Kaposi Ambrus, email: akaposi @ inf.elte.hu (szóköz nélkül).
- Bense Viktor, email: qils07 @ inf.elte.hu (szóköz nélkül).
- Szumi Xie, email: szumi @ inf.elte.hu (szóköz nélkül).


#### Helyszínek az interneten

- Microsoft Teams: a tananyag online megbeszélésének helyszíne. Itt
  érdemes a tananyaggal kapcsolatos kérdéseket feltenni, nem pedig az
  oktatóknak emailben. A csoport kódja: ndim1j6 (ezzel lehet csatlakozni).

- Canvas: az első előadáson kívül minden előadásra meg kell csinálni
  egy Canvas kvízt, ami az előző előadásról szól. Neptun azonosítóval
  lehet belépni. Az elsőn kívül minden gyakorlatra lesz egy beadandó
  és minden gyakorlat elején lesz egy 10 perces teszt, amit a Canvasba
  kell feltölteni.

- Előadás jegyzetek ezen az oldalon a [lec](lec) mappában. A
  gyakorlati anyagok a [tut](tut) mappában.

#### Technikai követelmények

Az [Agda programozási
nyelvet](https://wiki.portal.chalmers.se/agda/pmwiki.php) fel kell
telepíteni, ezt fogjuk használni gyakorlaton és részben előadáson is.


#### Előzetes ismeretek

A BSc-s "Funkcionális programozás" tárgy előkövetelmény, aki nem az
ELTE-re járt BSc-re, az felveheti.


#### Követelmények

- A gyakorlati jegy a gyakorlatok elején történő feladatokból jön
  össze. A várható ponthatárok az alábbi táblázatban láthatóak:

  | Pont     | Jegy |
  |---------:|------|
  | 6-7.49   | 2    |
  | 7.5-8.99 | 3    |
  | 9-10.49  | 4    |
  | 10.5-12  | 5    |

- Vizsgára az jöhet, akinek van nem-egyes gyakorlati jegye és az
  előadásokhoz tartozó Canvas kvízek Canvas átlaga 50% fölött van.

- A vizsga valószínűleg csak írásbeli teszt-jellegű lesz. Vizsgán
  tetszőleges segédeszköz/internet használható, kommunikáció más
  személyekkel viszont nem engedélyezett.


#### Irodalom

Folyamatosan frissülő [jegyzet](src/main.pdf) (angol nyelvű). Ez tartalmazza a teljes előadás-anyagot, további példákkal, gyakorló feladatokkal (közöttük kötelezők). Az előadásokon írt szöveg a [lec](lec), a gyakorlati anyagok a [tut](tut) mappában találhatók.

Zahorán Barnabás elaborátora: [forráskód](https://github.com/Seeker04/stlc-agda-elab) és [leírás](https://github.com/Seeker04/stlc-agda-elab/blob/main/paper/STLC-elaboration-in-Agda.pdf)

[Korábbi évek jegyzete](https://bitbucket.org/akaposi/tipusrendszerek/raw/HEAD/jegyzet.pdf?at=master&fileviewer=file-view-default)

[Régi példa-vizsga](https://bitbucket.org/akaposi/tipusrendszerek/raw/master/peldaVizsga.pdf).

[Robert Harper: Practical foundations of programming languages](https://www.cs.cmu.edu/~rwh/pfpl/2nded.pdf)

[Simon Castellan, Pierre Clairambault, and Peter Dybjer: Categories with Families: Unityped, Simply Typed, and Dependently Typed](https://arxiv.org/pdf/1904.00827.pdf)

[Philip Wadler, Wen Kokke, and Jeremy G. Siek. Programming Language Foundations in Agda](https://plfa.github.io/)
